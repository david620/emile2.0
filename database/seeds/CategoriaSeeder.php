<?php

use Illuminate\Database\Seeder;
use App\Categoria;

class CategoriasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Categoria::create([
            'nombre_categoria' => 'Introducción al Forex',
            'plataforma_categoría'     => '1'
        ]);
        Categoria::create([
            'nombre_categoria' => 'Como comenzar a operar',
            'plataforma_categoría'     => '1'
        ]);
        Categoria::create([
            'nombre_categoria' => 'Psicología',
            'plataforma_categoría'     => '1'
        ]);
        Categoria::create([
            'nombre_categoria' => 'Fase 1',
            'plataforma_categoría'     => '1'
        ]);
        Categoria::create([
            'nombre_categoria' => 'Fase 2',
            'plataforma_categoría'     => '1'
        ]);
        Categoria::create([
            'nombre_categoria' => 'Fase 3',
            'plataforma_categoría'     => '1'
        ]);
        Categoria::create([
            'nombre_categoria' => 'Fase 4',
            'plataforma_categoría'     => '1'
        ]);
        Categoria::create([
            'nombre_categoria' => 'Clases en Vivo',
            'plataforma_categoría'     => '1'
        ]);
        Categoria::create([
            'nombre_categoria' => 'Extras',
            'plataforma_categoría'     => '1'
        ]);

        Categoria::create([
            'nombre_categoria' => 'Introducción',
            'plataforma_categoría'     => '2'
        ]);
        Categoria::create([
            'nombre_categoria' => 'Capítulo 1',
            'plataforma_categoría'     => '2'
        ]);
        Categoria::create([
            'nombre_categoria' => 'Capítulo 2',
            'plataforma_categoría'     => '2'
        ]);
        Categoria::create([
            'nombre_categoria' => 'Capítulo 3',
            'plataforma_categoría'     => '2'
        ]);
        Categoria::create([
            'nombre_categoria' => 'Capítulo 4',
            'plataforma_categoría'     => '2'
        ]);
        Categoria::create([
            'nombre_categoria' => 'ICO',
            'plataforma_categoría'     => '2'
        ]);
        Categoria::create([
            'nombre_categoria' => 'Clases en Vivo',
            'plataforma_categoría'     => '2'
        ]);
        Categoria::create([
            'nombre_categoria' => 'Extras',
            'plataforma_categoría'     => '2'
        ]);


    }
}
