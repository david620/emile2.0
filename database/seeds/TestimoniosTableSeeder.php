<?php

use Illuminate\Database\Seeder;

class TestimoniosTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('testimonios')->delete();
        
        \DB::table('testimonios')->insert(array (
            0 => 
            array (
                'id' => 6,
                'texto_descriptivo' => 'Resultados',
                'user_id' => 1552,
                'url_imagen' => 'imagenes/testimonio1573325706.png',
            ),
            1 => 
            array (
                'id' => 7,
                'texto_descriptivo' => 'Profitcalls',
                'user_id' => 1552,
                'url_imagen' => 'imagenes/testimonio1573839924.png',
            ),
            2 => 
            array (
                'id' => 8,
                'texto_descriptivo' => 'Profitcalls',
                'user_id' => 1552,
                'url_imagen' => 'imagenes/testimonio1573839961.png',
            ),
            3 => 
            array (
                'id' => 9,
                'texto_descriptivo' => NULL,
                'user_id' => 1552,
                'url_imagen' => 'imagenes/testimonio1573945881.png',
            ),
            4 => 
            array (
                'id' => 10,
                'texto_descriptivo' => 'Profitcalls',
                'user_id' => 1552,
                'url_imagen' => 'imagenes/testimonio1573945906.png',
            ),
            5 => 
            array (
                'id' => 11,
                'texto_descriptivo' => 'Profitcalls',
                'user_id' => 1552,
                'url_imagen' => 'imagenes/testimonio1573945983.png',
            ),
        ));
        
        
    }
}