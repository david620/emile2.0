<?php

use Illuminate\Database\Seeder;
use App\Servicio;

class ServicioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Servicio::create([
            'nombre_servicio' => 'PROFIT 4 LIFE',
            'descripcion_servicio' => 'Te damos la bienvenida a PROFIT 4 LIFE, la primera escuela de forex abierta en español, cuyo fin es facilitar el uso de las herramientas y brindar los conocimientos necesarios para poder entrar al gran mundo de forex.',
            'imagen_servicio'     => 'img/servicios/1.png',
            'precio_servicio'     => '750'
        ]);
        Servicio::create([
            'nombre_servicio' => 'CRIPTO 4 LIFE',
            'descripcion_servicio' => 'En tu academia Cripto 4 Life aprenderas a tomar ventaja de ese cambio de precio causado por el crecimiento de la demanda para generar Profits y asi comenzar en el Trading de las Criptomonedas, esto para verlo desde el punto de vista de un inversionista, desarrollando habilidades en el análisis tanto fundamental como técnico, para tomar ventaja de las oportunidades que ofrece el Mercado.',
            'imagen_servicio'     => 'img/servicios/2.png',
            'precio_servicio'     => '500'
        ]); 
        Servicio::create([
            'nombre_servicio' => 'PROFIT CALLS',
            'descripcion_servicio' => 'Nuestra nueva APP te llevara a cumplir tus sueños.',
            'imagen_servicio'     => 'img/servicios/3.png',
            'precio_servicio'     => '99'
        ]);        
        Servicio::create([
            'nombre_servicio' => 'LIBRO: SCALPING',
            'descripcion_servicio' => 'En este libro explicaremos de la manera más sencilla como entender la estructura del mercado, para así aplicar estos conocimientos a nuestra estrategia de “scalping”, es importante destacar que para identificar la estructura del mercado debemos de hacer uso de Fibonacci, además debemos identificar la estructura del mercado la cual nos ayuda a identificar la tendencia de la misma.',
            'imagen_servicio'     => 'img/servicios/4.jpg',
            'precio_servicio'     => '350'
        ]);
        Servicio::create([
            'nombre_servicio' => 'LIBRO: CRIPTOMONEDAS',
            'descripcion_servicio' => 'En este libro encontraran una nueva estrategia que diseñe para operar criptomonedas con apalancamiento, teniendo un 75% de efectividad, para ser utilizada en pares como ETH/USD, XRP/USD y BTC/USD siendo de estos tres el más recomendado para operar ETH/USD.',
            'imagen_servicio'     => 'img/servicios/5.jpg',
            'precio_servicio'     => '350'
        ]);
        Servicio::create([
            'nombre_servicio' => 'APP: TECHNICAL PROSPERITY',
            'descripcion_servicio' => 'Apliación Technical Prosperity',
            'imagen_servicio'     => 'img/servicios/3.jpg',
            'precio_servicio'     => '99'
        ]);
    }
}
