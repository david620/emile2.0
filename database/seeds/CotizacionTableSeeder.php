<?php

use Illuminate\Database\Seeder;

class CotizacionTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('cotizacion')->delete();
        
        \DB::table('cotizacion')->insert(array (
            0 => 
            array (
                'id' => 2,
                'bid' => 0.22136,
                'ask' => 0.22187,
                'symbol' => 'XRPUSD',
                'decimales' => 4,
                'factor' => 1.0,
            ),
            1 => 
            array (
                'id' => 3,
                'bid' => 7248.01,
                'ask' => 7264.09,
                'symbol' => 'BTCUSD',
                'decimales' => 0,
                'factor' => 1.0,
            ),
            2 => 
            array (
                'id' => 4,
                'bid' => 29.91,
                'ask' => 30.01,
                'symbol' => 'ZECUSD',
                'decimales' => 1,
                'factor' => 1.0,
            ),
            3 => 
            array (
                'id' => 5,
                'bid' => 145.45,
                'ask' => 145.8,
                'symbol' => 'ETHUSD',
                'decimales' => 1,
                'factor' => 1.0,
            ),
            4 => 
            array (
                'id' => 6,
                'bid' => 108.739,
                'ask' => 108.745,
                'symbol' => 'USDJPY',
                'decimales' => 2,
                'factor' => 0.1057082,
            ),
            5 => 
            array (
                'id' => 7,
                'bid' => 201.332,
                'ask' => 201.368,
                'symbol' => 'GBPNZD',
                'decimales' => 4,
                'factor' => 0.1533742,
            ),
            6 => 
            array (
                'id' => 8,
                'bid' => 193.456,
                'ask' => 193.475,
                'symbol' => 'GBPAUD',
                'decimales' => 4,
                'factor' => 0.1474926,
            ),
            7 => 
            array (
                'id' => 9,
                'bid' => 27910.67,
                'ask' => 27914.34,
                'symbol' => 'US30',
                'decimales' => 1,
                'factor' => 1.0,
            ),
            8 => 
            array (
                'id' => 10,
                'bid' => 1464.55,
                'ask' => 1464.65,
                'symbol' => 'XAUUSD',
                'decimales' => 1,
                'factor' => 1.0,
            ),
            9 => 
            array (
                'id' => 11,
                'bid' => 174.411,
                'ask' => 174.437,
                'symbol' => 'GBPCAD',
                'decimales' => 4,
                'factor' => 1.0,
            ),
            10 => 
            array (
                'id' => 12,
                'bid' => 132.317,
                'ask' => 13.233,
                'symbol' => 'USDCAD',
                'decimales' => 1,
                'factor' => 0.1328,
            ),
            11 => 
            array (
                'id' => 13,
                'bid' => 0.65465,
                'ask' => 0.65474,
                'symbol' => 'NZDUSD',
                'decimales' => 1,
                'factor' => 0.1,
            ),
            12 => 
            array (
                'id' => 14,
                'bid' => 71.181,
                'ask' => 71.195,
                'symbol' => 'NZDJPY',
                'decimales' => 1,
                'factor' => 0.10204,
            ),
            13 => 
            array (
                'id' => 15,
                'bid' => 143.334,
                'ask' => 143.348,
                'symbol' => 'GBPJPY',
                'decimales' => 1,
                'factor' => 0.10204,
            ),
            14 => 
            array (
                'id' => 16,
                'bid' => 120.659,
                'ask' => 120.673,
                'symbol' => 'EURJPY',
                'decimales' => 1,
                'factor' => 0.10204,
            ),
            15 => 
            array (
                'id' => 17,
                'bid' => 110.962,
                'ask' => 110.967,
                'symbol' => 'EURUSD',
                'decimales' => 4,
                'factor' => 0.1,
            ),
            16 => 
            array (
                'id' => 18,
                'bid' => 0.98412,
                'ask' => 0.98421,
                'symbol' => 'USDCHF',
                'decimales' => 1,
                'factor' => 0.1,
            ),
            17 => 
            array (
                'id' => 19,
                'bid' => 169.487,
                'ask' => 169.519,
                'symbol' => 'EURNZD',
                'decimales' => 1,
                'factor' => 0.15974,
            ),
            18 => 
            array (
                'id' => 20,
                'bid' => 131.815,
                'ask' => 131.822,
                'symbol' => 'GBPUSD',
                'decimales' => 4,
                'factor' => 1.0,
            ),
        ));
        
        
    }
}