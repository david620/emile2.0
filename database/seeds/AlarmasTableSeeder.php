<?php

use Illuminate\Database\Seeder;

class AlarmasTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('alarmas')->delete();
        
        \DB::table('alarmas')->insert(array (
            0 => 
            array (
                'id_alarmas' => 9,
                'id_cotizacion' => 4,
                'precio' => 50.0,
                'inicio' => 49.9,
                'id_usuario' => 1552,
                'fecha' => '2019-08-16 21:42:00',
                'ejecutado' => 1,
            ),
            1 => 
            array (
                'id_alarmas' => 12,
                'id_cotizacion' => 3,
                'precio' => 10500.0,
                'inicio' => 10786.86,
                'id_usuario' => 1552,
                'fecha' => '2019-08-20 17:59:00',
                'ejecutado' => 1,
            ),
            2 => 
            array (
                'id_alarmas' => 14,
                'id_cotizacion' => 3,
                'precio' => 10720.0,
                'inicio' => 10746.82,
                'id_usuario' => 1409,
                'fecha' => '2019-08-21 02:16:00',
                'ejecutado' => 1,
            ),
            3 => 
            array (
                'id_alarmas' => 16,
                'id_cotizacion' => 3,
                'precio' => 10000.0,
                'inicio' => 10211.22,
                'id_usuario' => 1409,
                'fecha' => '2019-08-21 05:01:00',
                'ejecutado' => 1,
            ),
            4 => 
            array (
                'id_alarmas' => 17,
                'id_cotizacion' => 5,
                'precio' => 195.0,
                'inicio' => 195.18,
                'id_usuario' => 1522,
                'fecha' => '2019-08-23 23:41:00',
                'ejecutado' => 1,
            ),
            5 => 
            array (
                'id_alarmas' => 19,
                'id_cotizacion' => 6,
                'precio' => 105.403,
                'inicio' => 105.403,
                'id_usuario' => 822,
                'fecha' => '2019-08-24 01:01:00',
                'ejecutado' => 0,
            ),
            6 => 
            array (
                'id_alarmas' => 21,
                'id_cotizacion' => 5,
                'precio' => 193.7,
                'inicio' => 193.91,
                'id_usuario' => 1445,
                'fecha' => '2019-08-24 01:11:00',
                'ejecutado' => 1,
            ),
            7 => 
            array (
                'id_alarmas' => 23,
                'id_cotizacion' => 5,
                'precio' => 193.7,
                'inicio' => 193.53,
                'id_usuario' => 1445,
                'fecha' => '2019-08-24 01:13:00',
                'ejecutado' => 1,
            ),
            8 => 
            array (
                'id_alarmas' => 24,
                'id_cotizacion' => 2,
                'precio' => 0.27669,
                'inicio' => 0.27667,
                'id_usuario' => 864,
                'fecha' => '2019-08-24 01:34:00',
                'ejecutado' => 1,
            ),
            9 => 
            array (
                'id_alarmas' => 25,
                'id_cotizacion' => 5,
                'precio' => 193.71,
                'inicio' => 193.71,
                'id_usuario' => 1445,
                'fecha' => '2019-08-24 01:42:00',
                'ejecutado' => 0,
            ),
            10 => 
            array (
                'id_alarmas' => 29,
                'id_cotizacion' => 5,
                'precio' => 193.44,
                'inicio' => 193.5,
                'id_usuario' => 1199,
                'fecha' => '2019-08-24 02:27:00',
                'ejecutado' => 1,
            ),
            11 => 
            array (
                'id_alarmas' => 36,
                'id_cotizacion' => 6,
                'precio' => 105.403,
                'inicio' => 105.403,
                'id_usuario' => 55,
                'fecha' => '2019-08-24 03:55:00',
                'ejecutado' => 0,
            ),
            12 => 
            array (
                'id_alarmas' => 39,
                'id_cotizacion' => 6,
                'precio' => 106.238,
                'inicio' => 105.403,
                'id_usuario' => 1134,
                'fecha' => '2019-08-24 04:43:00',
                'ejecutado' => 1,
            ),
            13 => 
            array (
                'id_alarmas' => 40,
                'id_cotizacion' => 5,
                'precio' => 192.6,
                'inicio' => 192.56,
                'id_usuario' => 1134,
                'fecha' => '2019-08-24 04:46:00',
                'ejecutado' => 1,
            ),
            14 => 
            array (
                'id_alarmas' => 43,
                'id_cotizacion' => 5,
                'precio' => 182.19,
                'inicio' => 193.36,
                'id_usuario' => 1134,
                'fecha' => '2019-08-24 05:29:00',
                'ejecutado' => 1,
            ),
            15 => 
            array (
                'id_alarmas' => 44,
                'id_cotizacion' => 5,
                'precio' => 203.34,
                'inicio' => 193.33,
                'id_usuario' => 1134,
                'fecha' => '2019-08-24 05:29:00',
                'ejecutado' => 1,
            ),
            16 => 
            array (
                'id_alarmas' => 45,
                'id_cotizacion' => 6,
                'precio' => 105.309,
                'inicio' => 105.403,
                'id_usuario' => 1134,
                'fecha' => '2019-08-24 05:34:00',
                'ejecutado' => 1,
            ),
            17 => 
            array (
                'id_alarmas' => 65,
                'id_cotizacion' => 2,
                'precio' => 0.27002,
                'inicio' => 0.27069,
                'id_usuario' => 380,
                'fecha' => '2019-08-24 17:55:00',
                'ejecutado' => 1,
            ),
            18 => 
            array (
                'id_alarmas' => 67,
                'id_cotizacion' => 2,
                'precio' => 0.27134,
                'inicio' => 0.271,
                'id_usuario' => 1394,
                'fecha' => '2019-08-25 03:23:00',
                'ejecutado' => 1,
            ),
            19 => 
            array (
                'id_alarmas' => 71,
                'id_cotizacion' => 2,
                'precio' => 0.2696,
                'inicio' => 0.27524,
                'id_usuario' => 1509,
                'fecha' => '2019-08-25 11:31:00',
                'ejecutado' => 1,
            ),
            20 => 
            array (
                'id_alarmas' => 73,
                'id_cotizacion' => 5,
                'precio' => 187.34,
                'inicio' => 188.6,
                'id_usuario' => 1395,
                'fecha' => '2019-08-25 15:46:00',
                'ejecutado' => 1,
            ),
            21 => 
            array (
                'id_alarmas' => 74,
                'id_cotizacion' => 5,
                'precio' => 187.54,
                'inicio' => 188.45,
                'id_usuario' => 321,
                'fecha' => '2019-08-25 16:23:00',
                'ejecutado' => 1,
            ),
            22 => 
            array (
                'id_alarmas' => 75,
                'id_cotizacion' => 2,
                'precio' => 0.26787,
                'inicio' => 0.2709,
                'id_usuario' => 969,
                'fecha' => '2019-08-25 18:18:00',
                'ejecutado' => 1,
            ),
            23 => 
            array (
                'id_alarmas' => 77,
                'id_cotizacion' => 6,
                'precio' => 105.654,
                'inicio' => 105.403,
                'id_usuario' => 969,
                'fecha' => '2019-08-25 18:20:00',
                'ejecutado' => 1,
            ),
            24 => 
            array (
                'id_alarmas' => 78,
                'id_cotizacion' => 6,
                'precio' => 105.94,
                'inicio' => 105.95,
                'id_usuario' => 1247,
                'fecha' => '2019-08-26 12:42:00',
                'ejecutado' => 1,
            ),
            25 => 
            array (
                'id_alarmas' => 85,
                'id_cotizacion' => 6,
                'precio' => 105.891,
                'inicio' => 106.084,
                'id_usuario' => 969,
                'fecha' => '2019-08-26 20:39:00',
                'ejecutado' => 1,
            ),
            26 => 
            array (
                'id_alarmas' => 86,
                'id_cotizacion' => 6,
                'precio' => 105.905,
                'inicio' => 106.107,
                'id_usuario' => 1525,
                'fecha' => '2019-08-26 22:05:00',
                'ejecutado' => 1,
            ),
            27 => 
            array (
                'id_alarmas' => 88,
                'id_cotizacion' => 10,
                'precio' => 1532.07,
                'inicio' => 1526.94,
                'id_usuario' => 179,
                'fecha' => '2019-08-26 22:23:00',
                'ejecutado' => 1,
            ),
            28 => 
            array (
                'id_alarmas' => 89,
                'id_cotizacion' => 10,
                'precio' => 1530.0,
                'inicio' => 1527.4,
                'id_usuario' => 962,
                'fecha' => '2019-08-26 23:31:00',
                'ejecutado' => 1,
            ),
            29 => 
            array (
                'id_alarmas' => 90,
                'id_cotizacion' => 10,
                'precio' => 1530.0,
                'inicio' => 1527.4,
                'id_usuario' => 962,
                'fecha' => '2019-08-26 23:31:00',
                'ejecutado' => 1,
            ),
            30 => 
            array (
                'id_alarmas' => 91,
                'id_cotizacion' => 6,
                'precio' => 106.046,
                'inicio' => 106.1,
                'id_usuario' => 962,
                'fecha' => '2019-08-26 23:32:00',
                'ejecutado' => 1,
            ),
            31 => 
            array (
                'id_alarmas' => 93,
                'id_cotizacion' => 5,
                'precio' => 189.29,
                'inicio' => 189.28,
                'id_usuario' => 1552,
                'fecha' => '2019-08-26 23:35:00',
                'ejecutado' => 1,
            ),
            32 => 
            array (
                'id_alarmas' => 96,
                'id_cotizacion' => 5,
                'precio' => 186.35,
                'inicio' => 186.3,
                'id_usuario' => 0,
                'fecha' => '2019-08-27 02:36:00',
                'ejecutado' => 1,
            ),
            33 => 
            array (
                'id_alarmas' => 98,
                'id_cotizacion' => 5,
                'precio' => 189.3,
                'inicio' => 186.3,
                'id_usuario' => 0,
                'fecha' => '2019-08-27 02:37:00',
                'ejecutado' => 1,
            ),
            34 => 
            array (
                'id_alarmas' => 100,
                'id_cotizacion' => 6,
                'precio' => 104.0,
                'inicio' => 105.759,
                'id_usuario' => 329,
                'fecha' => '2019-08-27 03:13:00',
                'ejecutado' => 0,
            ),
            35 => 
            array (
                'id_alarmas' => 101,
                'id_cotizacion' => 10,
                'precio' => 1528.47,
                'inicio' => 1528.4,
                'id_usuario' => 1169,
                'fecha' => '2019-08-27 04:31:00',
                'ejecutado' => 1,
            ),
            36 => 
            array (
                'id_alarmas' => 102,
                'id_cotizacion' => 7,
                'precio' => 192.362,
                'inicio' => 19.188,
                'id_usuario' => 1427,
                'fecha' => '2019-08-27 04:53:00',
                'ejecutado' => 1,
            ),
            37 => 
            array (
                'id_alarmas' => 103,
                'id_cotizacion' => 7,
                'precio' => 191.852,
                'inicio' => 191.847,
                'id_usuario' => 1503,
                'fecha' => '2019-08-27 05:33:00',
                'ejecutado' => 1,
            ),
            38 => 
            array (
                'id_alarmas' => 106,
                'id_cotizacion' => 7,
                'precio' => 191.634,
                'inicio' => 191.913,
                'id_usuario' => 1323,
                'fecha' => '2019-08-27 06:33:00',
                'ejecutado' => 1,
            ),
            39 => 
            array (
                'id_alarmas' => 107,
                'id_cotizacion' => 7,
                'precio' => 191.634,
                'inicio' => 191.967,
                'id_usuario' => 380,
                'fecha' => '2019-08-27 06:37:00',
                'ejecutado' => 1,
            ),
            40 => 
            array (
                'id_alarmas' => 108,
                'id_cotizacion' => 10,
                'precio' => 1540.0,
                'inicio' => 1532.11,
                'id_usuario' => 1498,
                'fecha' => '2019-08-27 08:33:00',
                'ejecutado' => 1,
            ),
            41 => 
            array (
                'id_alarmas' => 110,
                'id_cotizacion' => 5,
                'precio' => 186.5,
                'inicio' => 186.5,
                'id_usuario' => 289,
                'fecha' => '2019-08-27 10:56:00',
                'ejecutado' => 0,
            ),
            42 => 
            array (
                'id_alarmas' => 111,
                'id_cotizacion' => 7,
                'precio' => 192.332,
                'inicio' => 19.241,
                'id_usuario' => 1292,
                'fecha' => '2019-08-27 12:38:00',
                'ejecutado' => 1,
            ),
            43 => 
            array (
                'id_alarmas' => 112,
                'id_cotizacion' => 7,
                'precio' => 192.297,
                'inicio' => 192.415,
                'id_usuario' => 1292,
                'fecha' => '2019-08-27 13:03:00',
                'ejecutado' => 1,
            ),
            44 => 
            array (
                'id_alarmas' => 114,
                'id_cotizacion' => 6,
                'precio' => 106.078,
                'inicio' => 105.991,
                'id_usuario' => 992,
                'fecha' => '2019-08-27 13:26:00',
                'ejecutado' => 1,
            ),
            45 => 
            array (
                'id_alarmas' => 115,
                'id_cotizacion' => 6,
                'precio' => 105.847,
                'inicio' => 105.951,
                'id_usuario' => 969,
                'fecha' => '2019-08-27 13:40:00',
                'ejecutado' => 1,
            ),
            46 => 
            array (
                'id_alarmas' => 117,
                'id_cotizacion' => 2,
                'precio' => 0.2691,
                'inicio' => 0.2691,
                'id_usuario' => 437,
                'fecha' => '2019-08-27 14:02:00',
                'ejecutado' => 0,
            ),
            47 => 
            array (
                'id_alarmas' => 118,
                'id_cotizacion' => 3,
                'precio' => 10202.22,
                'inicio' => 10202.22,
                'id_usuario' => 437,
                'fecha' => '2019-08-27 14:02:00',
                'ejecutado' => 0,
            ),
            48 => 
            array (
                'id_alarmas' => 119,
                'id_cotizacion' => 6,
                'precio' => 106.014,
                'inicio' => 106.03,
                'id_usuario' => 1247,
                'fecha' => '2019-08-27 14:03:00',
                'ejecutado' => 1,
            ),
            49 => 
            array (
                'id_alarmas' => 120,
                'id_cotizacion' => 6,
                'precio' => 106.027,
                'inicio' => 106.024,
                'id_usuario' => 1180,
                'fecha' => '2019-08-27 14:05:00',
                'ejecutado' => 1,
            ),
            50 => 
            array (
                'id_alarmas' => 121,
                'id_cotizacion' => 7,
                'precio' => 192.976,
                'inicio' => 192.993,
                'id_usuario' => 1180,
                'fecha' => '2019-08-27 14:05:00',
                'ejecutado' => 1,
            ),
            51 => 
            array (
                'id_alarmas' => 122,
                'id_cotizacion' => 2,
                'precio' => 0.26868,
                'inicio' => 0.26874,
                'id_usuario' => 961,
                'fecha' => '2019-08-27 14:51:00',
                'ejecutado' => 1,
            ),
            52 => 
            array (
                'id_alarmas' => 123,
                'id_cotizacion' => 3,
                'precio' => 10176.19,
                'inicio' => 10176.19,
                'id_usuario' => 961,
                'fecha' => '2019-08-27 14:51:00',
                'ejecutado' => 0,
            ),
            53 => 
            array (
                'id_alarmas' => 124,
                'id_cotizacion' => 2,
                'precio' => 0.26868,
                'inicio' => 0.26866,
                'id_usuario' => 961,
                'fecha' => '2019-08-27 14:51:00',
                'ejecutado' => 1,
            ),
            54 => 
            array (
                'id_alarmas' => 125,
                'id_cotizacion' => 2,
                'precio' => 0.26868,
                'inicio' => 0.26866,
                'id_usuario' => 961,
                'fecha' => '2019-08-27 14:51:00',
                'ejecutado' => 1,
            ),
            55 => 
            array (
                'id_alarmas' => 126,
                'id_cotizacion' => 2,
                'precio' => 0.26868,
                'inicio' => 0.26871,
                'id_usuario' => 961,
                'fecha' => '2019-08-27 14:51:00',
                'ejecutado' => 1,
            ),
            56 => 
            array (
                'id_alarmas' => 127,
                'id_cotizacion' => 5,
                'precio' => 188.14,
                'inicio' => 188.14,
                'id_usuario' => 961,
                'fecha' => '2019-08-27 14:51:00',
                'ejecutado' => 0,
            ),
            57 => 
            array (
                'id_alarmas' => 128,
                'id_cotizacion' => 6,
                'precio' => 106.027,
                'inicio' => 106.024,
                'id_usuario' => 961,
                'fecha' => '2019-08-27 14:51:00',
                'ejecutado' => 1,
            ),
            58 => 
            array (
                'id_alarmas' => 129,
                'id_cotizacion' => 7,
                'precio' => 192.851,
                'inicio' => 192.873,
                'id_usuario' => 961,
                'fecha' => '2019-08-27 14:52:00',
                'ejecutado' => 1,
            ),
            59 => 
            array (
                'id_alarmas' => 130,
                'id_cotizacion' => 2,
                'precio' => 0.26868,
                'inicio' => 0.2688,
                'id_usuario' => 961,
                'fecha' => '2019-08-27 14:52:00',
                'ejecutado' => 1,
            ),
            60 => 
            array (
                'id_alarmas' => 131,
                'id_cotizacion' => 2,
                'precio' => 0.26868,
                'inicio' => 0.2688,
                'id_usuario' => 961,
                'fecha' => '2019-08-27 14:53:00',
                'ejecutado' => 1,
            ),
            61 => 
            array (
                'id_alarmas' => 132,
                'id_cotizacion' => 7,
                'precio' => 193.043,
                'inicio' => 193.136,
                'id_usuario' => 1247,
                'fecha' => '2019-08-27 16:12:00',
                'ejecutado' => 1,
            ),
            62 => 
            array (
                'id_alarmas' => 133,
                'id_cotizacion' => 2,
                'precio' => 0.27026,
                'inicio' => 0.27024,
                'id_usuario' => 289,
                'fecha' => '2019-08-27 16:51:00',
                'ejecutado' => 1,
            ),
            63 => 
            array (
                'id_alarmas' => 135,
                'id_cotizacion' => 6,
                'precio' => 105.693,
                'inicio' => 105.705,
                'id_usuario' => 1180,
                'fecha' => '2019-08-27 16:54:00',
                'ejecutado' => 1,
            ),
            64 => 
            array (
                'id_alarmas' => 136,
                'id_cotizacion' => 11,
                'precio' => 163.406,
                'inicio' => 163.409,
                'id_usuario' => 1180,
                'fecha' => '2019-08-27 16:54:00',
                'ejecutado' => 1,
            ),
            65 => 
            array (
                'id_alarmas' => 137,
                'id_cotizacion' => 7,
                'precio' => 193.383,
                'inicio' => 193.386,
                'id_usuario' => 1180,
                'fecha' => '2019-08-27 16:54:00',
                'ejecutado' => 1,
            ),
            66 => 
            array (
                'id_alarmas' => 140,
                'id_cotizacion' => 3,
                'precio' => 10584.2,
                'inicio' => 10184.2,
                'id_usuario' => 1512,
                'fecha' => '2019-08-27 19:22:00',
                'ejecutado' => 1,
            ),
            67 => 
            array (
                'id_alarmas' => 141,
                'id_cotizacion' => 2,
                'precio' => 0.2692,
                'inicio' => 0.2692,
                'id_usuario' => 289,
                'fecha' => '2019-08-27 19:50:00',
                'ejecutado' => 0,
            ),
            68 => 
            array (
                'id_alarmas' => 142,
                'id_cotizacion' => 7,
                'precio' => 193.105,
                'inicio' => 193.077,
                'id_usuario' => 1180,
                'fecha' => '2019-08-27 19:50:00',
                'ejecutado' => 1,
            ),
            69 => 
            array (
                'id_alarmas' => 143,
                'id_cotizacion' => 9,
                'precio' => 25781.64,
                'inicio' => 25786.64,
                'id_usuario' => 872,
                'fecha' => '2019-08-27 19:53:00',
                'ejecutado' => 1,
            ),
            70 => 
            array (
                'id_alarmas' => 144,
                'id_cotizacion' => 6,
                'precio' => 105.749,
                'inicio' => 105.748,
                'id_usuario' => 1307,
                'fecha' => '2019-08-27 20:00:00',
                'ejecutado' => 1,
            ),
            71 => 
            array (
                'id_alarmas' => 145,
                'id_cotizacion' => 6,
                'precio' => 105.785,
                'inicio' => 105.773,
                'id_usuario' => 1307,
                'fecha' => '2019-08-27 20:25:00',
                'ejecutado' => 1,
            ),
            72 => 
            array (
                'id_alarmas' => 146,
                'id_cotizacion' => 7,
                'precio' => 193.152,
                'inicio' => 193.168,
                'id_usuario' => 1307,
                'fecha' => '2019-08-27 20:28:00',
                'ejecutado' => 1,
            ),
            73 => 
            array (
                'id_alarmas' => 147,
                'id_cotizacion' => 7,
                'precio' => 19.116,
                'inicio' => 193.219,
                'id_usuario' => 1307,
                'fecha' => '2019-08-27 20:50:00',
                'ejecutado' => 1,
            ),
            74 => 
            array (
                'id_alarmas' => 148,
                'id_cotizacion' => 6,
                'precio' => 105.71,
                'inicio' => 105.712,
                'id_usuario' => 1307,
                'fecha' => '2019-08-27 23:39:00',
                'ejecutado' => 1,
            ),
            75 => 
            array (
                'id_alarmas' => 149,
                'id_cotizacion' => 5,
                'precio' => 181.3,
                'inicio' => 185.85,
                'id_usuario' => 875,
                'fecha' => '2019-08-28 00:46:00',
                'ejecutado' => 1,
            ),
            76 => 
            array (
                'id_alarmas' => 150,
                'id_cotizacion' => 6,
                'precio' => 106.2,
                'inicio' => 105.804,
                'id_usuario' => 873,
                'fecha' => '2019-08-28 01:13:00',
                'ejecutado' => 1,
            ),
            77 => 
            array (
                'id_alarmas' => 151,
                'id_cotizacion' => 5,
                'precio' => 186.19,
                'inicio' => 186.2,
                'id_usuario' => 365,
                'fecha' => '2019-08-28 05:33:00',
                'ejecutado' => 1,
            ),
            78 => 
            array (
                'id_alarmas' => 152,
                'id_cotizacion' => 5,
                'precio' => 186.19,
                'inicio' => 186.32,
                'id_usuario' => 365,
                'fecha' => '2019-08-28 05:33:00',
                'ejecutado' => 1,
            ),
            79 => 
            array (
                'id_alarmas' => 154,
                'id_cotizacion' => 6,
                'precio' => 105.747,
                'inicio' => 105.752,
                'id_usuario' => 1580,
                'fecha' => '2019-08-28 11:05:00',
                'ejecutado' => 1,
            ),
            80 => 
            array (
                'id_alarmas' => 155,
                'id_cotizacion' => 6,
                'precio' => 105.747,
                'inicio' => 105.753,
                'id_usuario' => 1580,
                'fecha' => '2019-08-28 11:05:00',
                'ejecutado' => 1,
            ),
            81 => 
            array (
                'id_alarmas' => 156,
                'id_cotizacion' => 6,
                'precio' => 105.747,
                'inicio' => 105.757,
                'id_usuario' => 1580,
                'fecha' => '2019-08-28 11:05:00',
                'ejecutado' => 1,
            ),
            82 => 
            array (
                'id_alarmas' => 157,
                'id_cotizacion' => 6,
                'precio' => 105.747,
                'inicio' => 105.752,
                'id_usuario' => 1580,
                'fecha' => '2019-08-28 11:06:00',
                'ejecutado' => 1,
            ),
            83 => 
            array (
                'id_alarmas' => 158,
                'id_cotizacion' => 3,
                'precio' => 10167.18,
                'inicio' => 10167.18,
                'id_usuario' => 1580,
                'fecha' => '2019-08-28 11:08:00',
                'ejecutado' => 0,
            ),
            84 => 
            array (
                'id_alarmas' => 159,
                'id_cotizacion' => 3,
                'precio' => 10167.18,
                'inicio' => 10167.18,
                'id_usuario' => 1580,
                'fecha' => '2019-08-28 11:08:00',
                'ejecutado' => 0,
            ),
            85 => 
            array (
                'id_alarmas' => 160,
                'id_cotizacion' => 3,
                'precio' => 10167.18,
                'inicio' => 10164.17,
                'id_usuario' => 1580,
                'fecha' => '2019-08-28 11:09:00',
                'ejecutado' => 1,
            ),
            86 => 
            array (
                'id_alarmas' => 161,
                'id_cotizacion' => 2,
                'precio' => 0.26757,
                'inicio' => 0.26757,
                'id_usuario' => 961,
                'fecha' => '2019-08-28 11:30:00',
                'ejecutado' => 0,
            ),
            87 => 
            array (
                'id_alarmas' => 162,
                'id_cotizacion' => 6,
                'precio' => 105.712,
                'inicio' => 105.714,
                'id_usuario' => 961,
                'fecha' => '2019-08-28 11:31:00',
                'ejecutado' => 1,
            ),
            88 => 
            array (
                'id_alarmas' => 163,
                'id_cotizacion' => 6,
                'precio' => 105.712,
                'inicio' => 105.729,
                'id_usuario' => 961,
                'fecha' => '2019-08-28 11:32:00',
                'ejecutado' => 1,
            ),
            89 => 
            array (
                'id_alarmas' => 164,
                'id_cotizacion' => 4,
                'precio' => 49.91,
                'inicio' => 49.86,
                'id_usuario' => 961,
                'fecha' => '2019-08-28 11:33:00',
                'ejecutado' => 1,
            ),
            90 => 
            array (
                'id_alarmas' => 165,
                'id_cotizacion' => 9,
                'precio' => 25694.64,
                'inicio' => 25694.64,
                'id_usuario' => 961,
                'fecha' => '2019-08-28 11:38:00',
                'ejecutado' => 0,
            ),
            91 => 
            array (
                'id_alarmas' => 166,
                'id_cotizacion' => 7,
                'precio' => 19.239,
                'inicio' => 192.388,
                'id_usuario' => 961,
                'fecha' => '2019-08-28 11:43:00',
                'ejecutado' => 1,
            ),
            92 => 
            array (
                'id_alarmas' => 167,
                'id_cotizacion' => 5,
                'precio' => 186.02,
                'inicio' => 186.02,
                'id_usuario' => 1515,
                'fecha' => '2019-08-28 11:45:00',
                'ejecutado' => 0,
            ),
            93 => 
            array (
                'id_alarmas' => 168,
                'id_cotizacion' => 5,
                'precio' => 186.02,
                'inicio' => 186.02,
                'id_usuario' => 1515,
                'fecha' => '2019-08-28 11:45:00',
                'ejecutado' => 0,
            ),
            94 => 
            array (
                'id_alarmas' => 169,
                'id_cotizacion' => 5,
                'precio' => 186.02,
                'inicio' => 186.02,
                'id_usuario' => 1515,
                'fecha' => '2019-08-28 11:45:00',
                'ejecutado' => 0,
            ),
            95 => 
            array (
                'id_alarmas' => 170,
                'id_cotizacion' => 10,
                'precio' => 1545.76,
                'inicio' => 1545.16,
                'id_usuario' => 961,
                'fecha' => '2019-08-28 11:56:00',
                'ejecutado' => 1,
            ),
            96 => 
            array (
                'id_alarmas' => 171,
                'id_cotizacion' => 11,
                'precio' => 162.658,
                'inicio' => 162.609,
                'id_usuario' => 961,
                'fecha' => '2019-08-28 11:56:00',
                'ejecutado' => 1,
            ),
            97 => 
            array (
                'id_alarmas' => 172,
                'id_cotizacion' => 8,
                'precio' => 181.195,
                'inicio' => 181.049,
                'id_usuario' => 961,
                'fecha' => '2019-08-28 11:56:00',
                'ejecutado' => 1,
            ),
            98 => 
            array (
                'id_alarmas' => 173,
                'id_cotizacion' => 10,
                'precio' => 1534.7,
                'inicio' => 1537.45,
                'id_usuario' => 1179,
                'fecha' => '2019-08-28 14:03:00',
                'ejecutado' => 1,
            ),
            99 => 
            array (
                'id_alarmas' => 174,
                'id_cotizacion' => 10,
                'precio' => 1534.7,
                'inicio' => 1537.47,
                'id_usuario' => 1179,
                'fecha' => '2019-08-28 14:03:00',
                'ejecutado' => 1,
            ),
            100 => 
            array (
                'id_alarmas' => 176,
                'id_cotizacion' => 7,
                'precio' => 193.294,
                'inicio' => 193.128,
                'id_usuario' => 1358,
                'fecha' => '2019-08-28 16:04:00',
                'ejecutado' => 1,
            ),
            101 => 
            array (
                'id_alarmas' => 177,
                'id_cotizacion' => 7,
                'precio' => 193.294,
                'inicio' => 193.085,
                'id_usuario' => 825,
                'fecha' => '2019-08-28 16:06:00',
                'ejecutado' => 1,
            ),
            102 => 
            array (
                'id_alarmas' => 178,
                'id_cotizacion' => 7,
                'precio' => 193.294,
                'inicio' => 193.227,
                'id_usuario' => 1292,
                'fecha' => '2019-08-28 16:34:00',
                'ejecutado' => 1,
            ),
            103 => 
            array (
                'id_alarmas' => 179,
                'id_cotizacion' => 7,
                'precio' => 193.008,
                'inicio' => 192.995,
                'id_usuario' => 1545,
                'fecha' => '2019-08-28 18:23:00',
                'ejecutado' => 1,
            ),
            104 => 
            array (
                'id_alarmas' => 180,
                'id_cotizacion' => 7,
                'precio' => 193.008,
                'inicio' => 192.998,
                'id_usuario' => 1545,
                'fecha' => '2019-08-28 18:23:00',
                'ejecutado' => 1,
            ),
            105 => 
            array (
                'id_alarmas' => 181,
                'id_cotizacion' => 7,
                'precio' => 192.111,
                'inicio' => 192.957,
                'id_usuario' => 1545,
                'fecha' => '2019-08-28 18:25:00',
                'ejecutado' => 1,
            ),
            106 => 
            array (
                'id_alarmas' => 182,
                'id_cotizacion' => 10,
                'precio' => 1543.15,
                'inicio' => 1539.18,
                'id_usuario' => 644,
                'fecha' => '2019-08-28 20:02:00',
                'ejecutado' => 1,
            ),
            107 => 
            array (
                'id_alarmas' => 183,
                'id_cotizacion' => 10,
                'precio' => 1546.15,
                'inicio' => 1539.21,
                'id_usuario' => 644,
                'fecha' => '2019-08-28 20:02:00',
                'ejecutado' => 1,
            ),
            108 => 
            array (
                'id_alarmas' => 184,
                'id_cotizacion' => 7,
                'precio' => 192.856,
                'inicio' => 192.865,
                'id_usuario' => 477,
                'fecha' => '2019-08-28 20:49:00',
                'ejecutado' => 1,
            ),
            109 => 
            array (
                'id_alarmas' => 185,
                'id_cotizacion' => 7,
                'precio' => 192.856,
                'inicio' => 192.865,
                'id_usuario' => 477,
                'fecha' => '2019-08-28 20:49:00',
                'ejecutado' => 1,
            ),
            110 => 
            array (
                'id_alarmas' => 186,
                'id_cotizacion' => 7,
                'precio' => 192.744,
                'inicio' => 192.708,
                'id_usuario' => 806,
                'fecha' => '2019-08-28 21:15:00',
                'ejecutado' => 1,
            ),
            111 => 
            array (
                'id_alarmas' => 187,
                'id_cotizacion' => 7,
                'precio' => 192.744,
                'inicio' => 192.723,
                'id_usuario' => 806,
                'fecha' => '2019-08-28 21:15:00',
                'ejecutado' => 1,
            ),
            112 => 
            array (
                'id_alarmas' => 188,
                'id_cotizacion' => 7,
                'precio' => 19.329,
                'inicio' => 192.776,
                'id_usuario' => 262,
                'fecha' => '2019-08-28 21:45:00',
                'ejecutado' => 1,
            ),
            113 => 
            array (
                'id_alarmas' => 189,
                'id_cotizacion' => 7,
                'precio' => 192.857,
                'inicio' => 192.776,
                'id_usuario' => 1525,
                'fecha' => '2019-08-28 22:10:00',
                'ejecutado' => 1,
            ),
            114 => 
            array (
                'id_alarmas' => 190,
                'id_cotizacion' => 7,
                'precio' => 1.93,
                'inicio' => 1.928,
                'id_usuario' => 1005,
                'fecha' => '2019-08-28 22:14:00',
                'ejecutado' => 1,
            ),
            115 => 
            array (
                'id_alarmas' => 191,
                'id_cotizacion' => 7,
                'precio' => 1.93,
                'inicio' => 1.928,
                'id_usuario' => 1005,
                'fecha' => '2019-08-28 22:14:00',
                'ejecutado' => 1,
            ),
            116 => 
            array (
                'id_alarmas' => 192,
                'id_cotizacion' => 7,
                'precio' => 193.057,
                'inicio' => 192.775,
                'id_usuario' => 1525,
                'fecha' => '2019-08-29 08:30:00',
                'ejecutado' => 1,
            ),
            117 => 
            array (
                'id_alarmas' => 193,
                'id_cotizacion' => 7,
                'precio' => 192.638,
                'inicio' => 192.805,
                'id_usuario' => 1525,
                'fecha' => '2019-08-29 08:30:00',
                'ejecutado' => 1,
            ),
            118 => 
            array (
                'id_alarmas' => 194,
                'id_cotizacion' => 7,
                'precio' => 19.263,
                'inicio' => 192.773,
                'id_usuario' => 1307,
                'fecha' => '2019-08-29 12:16:00',
                'ejecutado' => 1,
            ),
            119 => 
            array (
                'id_alarmas' => 195,
                'id_cotizacion' => 7,
                'precio' => 1.923,
                'inicio' => 192.584,
                'id_usuario' => 320,
                'fecha' => '2019-08-29 12:33:00',
                'ejecutado' => 1,
            ),
            120 => 
            array (
                'id_alarmas' => 197,
                'id_cotizacion' => 6,
                'precio' => 106.13,
                'inicio' => 106.251,
                'id_usuario' => 493,
                'fecha' => '2019-08-29 13:15:00',
                'ejecutado' => 1,
            ),
            121 => 
            array (
                'id_alarmas' => 198,
                'id_cotizacion' => 7,
                'precio' => 193.294,
                'inicio' => 193.092,
                'id_usuario' => 262,
                'fecha' => '2019-08-29 15:09:00',
                'ejecutado' => 1,
            ),
            122 => 
            array (
                'id_alarmas' => 199,
                'id_cotizacion' => 7,
                'precio' => 193.118,
                'inicio' => 19.312,
                'id_usuario' => 528,
                'fecha' => '2019-08-29 17:49:00',
                'ejecutado' => 1,
            ),
            123 => 
            array (
                'id_alarmas' => 200,
                'id_cotizacion' => 7,
                'precio' => 193.118,
                'inicio' => 19.312,
                'id_usuario' => 528,
                'fecha' => '2019-08-29 17:49:00',
                'ejecutado' => 1,
            ),
            124 => 
            array (
                'id_alarmas' => 201,
                'id_cotizacion' => 7,
                'precio' => 19.289,
                'inicio' => 193.042,
                'id_usuario' => 1427,
                'fecha' => '2019-08-29 18:03:00',
                'ejecutado' => 1,
            ),
            125 => 
            array (
                'id_alarmas' => 202,
                'id_cotizacion' => 7,
                'precio' => 19.289,
                'inicio' => 193.042,
                'id_usuario' => 1427,
                'fecha' => '2019-08-29 18:03:00',
                'ejecutado' => 1,
            ),
            126 => 
            array (
                'id_alarmas' => 205,
                'id_cotizacion' => 2,
                'precio' => 0.2595,
                'inicio' => 0.25949,
                'id_usuario' => 321,
                'fecha' => '2019-08-29 20:24:00',
                'ejecutado' => 1,
            ),
            127 => 
            array (
                'id_alarmas' => 206,
                'id_cotizacion' => 10,
                'precio' => 1528.35,
                'inicio' => 1527.54,
                'id_usuario' => 321,
                'fecha' => '2019-08-29 20:25:00',
                'ejecutado' => 1,
            ),
            128 => 
            array (
                'id_alarmas' => 207,
                'id_cotizacion' => 10,
                'precio' => 1529.64,
                'inicio' => 1527.25,
                'id_usuario' => 1522,
                'fecha' => '2019-08-29 20:33:00',
                'ejecutado' => 1,
            ),
            129 => 
            array (
                'id_alarmas' => 208,
                'id_cotizacion' => 10,
                'precio' => 1525.65,
                'inicio' => 1527.15,
                'id_usuario' => 1522,
                'fecha' => '2019-08-29 20:33:00',
                'ejecutado' => 1,
            ),
            130 => 
            array (
                'id_alarmas' => 210,
                'id_cotizacion' => 10,
                'precio' => 1528.64,
                'inicio' => 1526.72,
                'id_usuario' => 1249,
                'fecha' => '2019-08-29 20:49:00',
                'ejecutado' => 1,
            ),
            131 => 
            array (
                'id_alarmas' => 213,
                'id_cotizacion' => 10,
                'precio' => 1528.64,
                'inicio' => 1526.78,
                'id_usuario' => 1249,
                'fecha' => '2019-08-29 20:52:00',
                'ejecutado' => 1,
            ),
            132 => 
            array (
                'id_alarmas' => 215,
                'id_cotizacion' => 10,
                'precio' => 1528.64,
                'inicio' => 1526.84,
                'id_usuario' => 1249,
                'fecha' => '2019-08-29 20:53:00',
                'ejecutado' => 1,
            ),
            133 => 
            array (
                'id_alarmas' => 217,
                'id_cotizacion' => 10,
                'precio' => 1528.64,
                'inicio' => 1527.14,
                'id_usuario' => 1358,
                'fecha' => '2019-08-29 22:00:00',
                'ejecutado' => 1,
            ),
            134 => 
            array (
                'id_alarmas' => 220,
                'id_cotizacion' => 6,
                'precio' => 106.475,
                'inicio' => 106.526,
                'id_usuario' => 1594,
                'fecha' => '2019-08-29 22:15:00',
                'ejecutado' => 1,
            ),
            135 => 
            array (
                'id_alarmas' => 221,
                'id_cotizacion' => 6,
                'precio' => 106.459,
                'inicio' => 106.537,
                'id_usuario' => 1594,
                'fecha' => '2019-08-29 22:35:00',
                'ejecutado' => 1,
            ),
            136 => 
            array (
                'id_alarmas' => 222,
                'id_cotizacion' => 6,
                'precio' => 106.459,
                'inicio' => 106.537,
                'id_usuario' => 1594,
                'fecha' => '2019-08-29 22:35:00',
                'ejecutado' => 1,
            ),
            137 => 
            array (
                'id_alarmas' => 223,
                'id_cotizacion' => 10,
                'precio' => 1527.5,
                'inicio' => 1528.03,
                'id_usuario' => 1288,
                'fecha' => '2019-08-29 23:55:00',
                'ejecutado' => 1,
            ),
            138 => 
            array (
                'id_alarmas' => 224,
                'id_cotizacion' => 10,
                'precio' => 1528.5,
                'inicio' => 1528.14,
                'id_usuario' => 1288,
                'fecha' => '2019-08-29 23:56:00',
                'ejecutado' => 1,
            ),
            139 => 
            array (
                'id_alarmas' => 225,
                'id_cotizacion' => 10,
                'precio' => 1528.97,
                'inicio' => 1525.06,
                'id_usuario' => 1249,
                'fecha' => '2019-08-30 00:24:00',
                'ejecutado' => 1,
            ),
            140 => 
            array (
                'id_alarmas' => 226,
                'id_cotizacion' => 10,
                'precio' => 1528.64,
                'inicio' => 1525.55,
                'id_usuario' => 1249,
                'fecha' => '2019-08-30 00:24:00',
                'ejecutado' => 1,
            ),
            141 => 
            array (
                'id_alarmas' => 227,
                'id_cotizacion' => 10,
                'precio' => 1543.02,
                'inicio' => 1525.33,
                'id_usuario' => 787,
                'fecha' => '2019-08-30 01:22:00',
                'ejecutado' => 1,
            ),
            142 => 
            array (
                'id_alarmas' => 228,
                'id_cotizacion' => 10,
                'precio' => 1543.02,
                'inicio' => 1525.46,
                'id_usuario' => 787,
                'fecha' => '2019-08-30 01:22:00',
                'ejecutado' => 1,
            ),
            143 => 
            array (
                'id_alarmas' => 229,
                'id_cotizacion' => 6,
                'precio' => 106.35,
                'inicio' => 106.441,
                'id_usuario' => 104,
                'fecha' => '2019-08-30 01:55:00',
                'ejecutado' => 1,
            ),
            144 => 
            array (
                'id_alarmas' => 230,
                'id_cotizacion' => 3,
                'precio' => 9000.0,
                'inicio' => 9496.94,
                'id_usuario' => 1389,
                'fecha' => '2019-08-30 02:09:00',
                'ejecutado' => 1,
            ),
            145 => 
            array (
                'id_alarmas' => 231,
                'id_cotizacion' => 3,
                'precio' => 10000.0,
                'inicio' => 9551.4,
                'id_usuario' => 671,
                'fecha' => '2019-08-30 06:28:00',
                'ejecutado' => 1,
            ),
            146 => 
            array (
                'id_alarmas' => 232,
                'id_cotizacion' => 9,
                'precio' => 26540.0,
                'inicio' => 26510.14,
                'id_usuario' => 1538,
                'fecha' => '2019-08-30 09:27:00',
                'ejecutado' => 1,
            ),
            147 => 
            array (
                'id_alarmas' => 233,
                'id_cotizacion' => 10,
                'precio' => 1526.95,
                'inicio' => 1525.87,
                'id_usuario' => 311,
                'fecha' => '2019-08-30 11:06:00',
                'ejecutado' => 1,
            ),
            148 => 
            array (
                'id_alarmas' => 234,
                'id_cotizacion' => 10,
                'precio' => 1524.06,
                'inicio' => 1525.86,
                'id_usuario' => 311,
                'fecha' => '2019-08-30 11:07:00',
                'ejecutado' => 1,
            ),
            149 => 
            array (
                'id_alarmas' => 237,
                'id_cotizacion' => 5,
                'precio' => 169.65,
                'inicio' => 169.65,
                'id_usuario' => 411,
                'fecha' => '2019-08-30 16:30:00',
                'ejecutado' => 0,
            ),
            150 => 
            array (
                'id_alarmas' => 238,
                'id_cotizacion' => 6,
                'precio' => 106.337,
                'inicio' => 106.336,
                'id_usuario' => 411,
                'fecha' => '2019-08-30 16:31:00',
                'ejecutado' => 1,
            ),
            151 => 
            array (
                'id_alarmas' => 239,
                'id_cotizacion' => 9,
                'precio' => 26401.64,
                'inicio' => 26397.64,
                'id_usuario' => 411,
                'fecha' => '2019-08-30 16:31:00',
                'ejecutado' => 1,
            ),
            152 => 
            array (
                'id_alarmas' => 240,
                'id_cotizacion' => 10,
                'precio' => 1525.44,
                'inicio' => 1525.78,
                'id_usuario' => 411,
                'fecha' => '2019-08-30 16:31:00',
                'ejecutado' => 1,
            ),
            153 => 
            array (
                'id_alarmas' => 241,
                'id_cotizacion' => 6,
                'precio' => 106.0,
                'inicio' => 106.333,
                'id_usuario' => 1552,
                'fecha' => '2019-08-30 16:38:00',
                'ejecutado' => 1,
            ),
            154 => 
            array (
                'id_alarmas' => 245,
                'id_cotizacion' => 2,
                'precio' => 0.23,
                'inicio' => 0.25557,
                'id_usuario' => 1504,
                'fecha' => '2019-08-30 20:55:00',
                'ejecutado' => 1,
            ),
            155 => 
            array (
                'id_alarmas' => 246,
                'id_cotizacion' => 10,
                'precio' => 1520.21,
                'inicio' => 1521.21,
                'id_usuario' => 1469,
                'fecha' => '2019-08-30 22:12:00',
                'ejecutado' => 1,
            ),
            156 => 
            array (
                'id_alarmas' => 247,
                'id_cotizacion' => 10,
                'precio' => 1513.0,
                'inicio' => 1521.21,
                'id_usuario' => 964,
                'fecha' => '2019-08-31 13:27:00',
                'ejecutado' => 1,
            ),
            157 => 
            array (
                'id_alarmas' => 248,
                'id_cotizacion' => 5,
                'precio' => 167.0,
                'inicio' => 168.87,
                'id_usuario' => 964,
                'fecha' => '2019-08-31 13:27:00',
                'ejecutado' => 1,
            ),
            158 => 
            array (
                'id_alarmas' => 249,
                'id_cotizacion' => 2,
                'precio' => 0.25679,
                'inicio' => 0.25683,
                'id_usuario' => 86,
                'fecha' => '2019-09-01 09:48:00',
                'ejecutado' => 1,
            ),
            159 => 
            array (
                'id_alarmas' => 250,
                'id_cotizacion' => 10,
                'precio' => 1525.56,
                'inicio' => 1530.43,
                'id_usuario' => 1133,
                'fecha' => '2019-09-01 22:15:00',
                'ejecutado' => 1,
            ),
            160 => 
            array (
                'id_alarmas' => 251,
                'id_cotizacion' => 9,
                'precio' => 26306.7,
                'inicio' => 26264.14,
                'id_usuario' => 1585,
                'fecha' => '2019-09-01 23:04:00',
                'ejecutado' => 1,
            ),
            161 => 
            array (
                'id_alarmas' => 252,
                'id_cotizacion' => 10,
                'precio' => 1528.93,
                'inicio' => 1529.71,
                'id_usuario' => 1585,
                'fecha' => '2019-09-01 23:04:00',
                'ejecutado' => 1,
            ),
            162 => 
            array (
                'id_alarmas' => 253,
                'id_cotizacion' => 7,
                'precio' => 19.357,
                'inicio' => 193.006,
                'id_usuario' => 834,
                'fecha' => '2019-09-02 00:33:00',
                'ejecutado' => 1,
            ),
            163 => 
            array (
                'id_alarmas' => 254,
                'id_cotizacion' => 3,
                'precio' => 9790.0,
                'inicio' => 9794.87,
                'id_usuario' => 0,
                'fecha' => '2019-09-02 01:25:00',
                'ejecutado' => 1,
            ),
            164 => 
            array (
                'id_alarmas' => 255,
                'id_cotizacion' => 9,
                'precio' => 26300.0,
                'inicio' => 26329.14,
                'id_usuario' => 1538,
                'fecha' => '2019-09-02 01:43:00',
                'ejecutado' => 1,
            ),
            165 => 
            array (
                'id_alarmas' => 258,
                'id_cotizacion' => 3,
                'precio' => 9920.0,
                'inicio' => 9796.57,
                'id_usuario' => 1236,
                'fecha' => '2019-09-02 05:45:00',
                'ejecutado' => 1,
            ),
            166 => 
            array (
                'id_alarmas' => 262,
                'id_cotizacion' => 6,
                'precio' => 106.216,
                'inicio' => 106.274,
                'id_usuario' => 1594,
                'fecha' => '2019-09-02 15:31:00',
                'ejecutado' => 1,
            ),
            167 => 
            array (
                'id_alarmas' => 263,
                'id_cotizacion' => 8,
                'precio' => 17.982,
                'inicio' => 179.732,
                'id_usuario' => 1307,
                'fecha' => '2019-09-02 17:05:00',
                'ejecutado' => 1,
            ),
            168 => 
            array (
                'id_alarmas' => 264,
                'id_cotizacion' => 8,
                'precio' => 179.822,
                'inicio' => 179.667,
                'id_usuario' => 262,
                'fecha' => '2019-09-02 22:45:00',
                'ejecutado' => 1,
            ),
            169 => 
            array (
                'id_alarmas' => 265,
                'id_cotizacion' => 2,
                'precio' => 0.26006,
                'inicio' => 0.26215,
                'id_usuario' => 644,
                'fecha' => '2019-09-02 23:23:00',
                'ejecutado' => 1,
            ),
            170 => 
            array (
                'id_alarmas' => 266,
                'id_cotizacion' => 2,
                'precio' => 0.26406,
                'inicio' => 0.26259,
                'id_usuario' => 644,
                'fecha' => '2019-09-02 23:24:00',
                'ejecutado' => 1,
            ),
            171 => 
            array (
                'id_alarmas' => 267,
                'id_cotizacion' => 7,
                'precio' => 191.561,
                'inicio' => 191.569,
                'id_usuario' => 262,
                'fecha' => '2019-09-03 01:06:00',
                'ejecutado' => 1,
            ),
            172 => 
            array (
                'id_alarmas' => 268,
                'id_cotizacion' => 7,
                'precio' => 191.561,
                'inicio' => 191.787,
                'id_usuario' => 1485,
                'fecha' => '2019-09-03 02:26:00',
                'ejecutado' => 1,
            ),
            173 => 
            array (
                'id_alarmas' => 269,
                'id_cotizacion' => 7,
                'precio' => 191.561,
                'inicio' => 191.787,
                'id_usuario' => 1485,
                'fecha' => '2019-09-03 02:26:00',
                'ejecutado' => 1,
            ),
            174 => 
            array (
                'id_alarmas' => 270,
                'id_cotizacion' => 7,
                'precio' => 191.561,
                'inicio' => 19.173,
                'id_usuario' => 1108,
                'fecha' => '2019-09-03 02:34:00',
                'ejecutado' => 1,
            ),
            175 => 
            array (
                'id_alarmas' => 271,
                'id_cotizacion' => 7,
                'precio' => 191.561,
                'inicio' => 191.727,
                'id_usuario' => 1108,
                'fecha' => '2019-09-03 02:34:00',
                'ejecutado' => 1,
            ),
            176 => 
            array (
                'id_alarmas' => 273,
                'id_cotizacion' => 7,
                'precio' => 191.561,
                'inicio' => 191.837,
                'id_usuario' => 57,
                'fecha' => '2019-09-03 03:39:00',
                'ejecutado' => 1,
            ),
            177 => 
            array (
                'id_alarmas' => 275,
                'id_cotizacion' => 10,
                'precio' => 1536.0,
                'inicio' => 1531.15,
                'id_usuario' => 1236,
                'fecha' => '2019-09-03 08:19:00',
                'ejecutado' => 1,
            ),
            178 => 
            array (
                'id_alarmas' => 279,
                'id_cotizacion' => 2,
                'precio' => 1524.0,
                'inicio' => 0.2613,
                'id_usuario' => 496,
                'fecha' => '2019-09-03 11:31:00',
                'ejecutado' => 0,
            ),
            179 => 
            array (
                'id_alarmas' => 281,
                'id_cotizacion' => 5,
                'precio' => 179.36,
                'inicio' => 179.2,
                'id_usuario' => 1545,
                'fecha' => '2019-09-03 13:53:00',
                'ejecutado' => 1,
            ),
            180 => 
            array (
                'id_alarmas' => 282,
                'id_cotizacion' => 5,
                'precio' => 179.38,
                'inicio' => 179.2,
                'id_usuario' => 1545,
                'fecha' => '2019-09-03 13:53:00',
                'ejecutado' => 1,
            ),
            181 => 
            array (
                'id_alarmas' => 283,
                'id_cotizacion' => 5,
                'precio' => 179.4,
                'inicio' => 179.2,
                'id_usuario' => 1545,
                'fecha' => '2019-09-03 13:54:00',
                'ejecutado' => 1,
            ),
            182 => 
            array (
                'id_alarmas' => 284,
                'id_cotizacion' => 9,
                'precio' => 26150.0,
                'inicio' => 26065.64,
                'id_usuario' => 493,
                'fecha' => '2019-09-03 18:53:00',
                'ejecutado' => 1,
            ),
            183 => 
            array (
                'id_alarmas' => 285,
                'id_cotizacion' => 2,
                'precio' => 0.26452,
                'inicio' => 0.26454,
                'id_usuario' => 437,
                'fecha' => '2019-09-03 21:58:00',
                'ejecutado' => 1,
            ),
            184 => 
            array (
                'id_alarmas' => 290,
                'id_cotizacion' => 7,
                'precio' => 190.931,
                'inicio' => 190.853,
                'id_usuario' => 1444,
                'fecha' => '2019-09-04 03:32:00',
                'ejecutado' => 1,
            ),
            185 => 
            array (
                'id_alarmas' => 291,
                'id_cotizacion' => 10,
                'precio' => 1543.64,
                'inicio' => 1543.65,
                'id_usuario' => 1615,
                'fecha' => '2019-09-04 04:02:00',
                'ejecutado' => 1,
            ),
            186 => 
            array (
                'id_alarmas' => 292,
                'id_cotizacion' => 10,
                'precio' => 1543.64,
                'inicio' => 1543.65,
                'id_usuario' => 1615,
                'fecha' => '2019-09-04 04:02:00',
                'ejecutado' => 1,
            ),
            187 => 
            array (
                'id_alarmas' => 293,
                'id_cotizacion' => 10,
                'precio' => 1543.64,
                'inicio' => 1543.65,
                'id_usuario' => 1615,
                'fecha' => '2019-09-04 04:02:00',
                'ejecutado' => 1,
            ),
            188 => 
            array (
                'id_alarmas' => 294,
                'id_cotizacion' => 7,
                'precio' => 1.92,
                'inicio' => 191.072,
                'id_usuario' => 787,
                'fecha' => '2019-09-04 04:57:00',
                'ejecutado' => 1,
            ),
            189 => 
            array (
                'id_alarmas' => 295,
                'id_cotizacion' => 5,
                'precio' => 180.16,
                'inicio' => 179.35,
                'id_usuario' => 1439,
                'fecha' => '2019-09-04 04:58:00',
                'ejecutado' => 1,
            ),
            190 => 
            array (
                'id_alarmas' => 296,
                'id_cotizacion' => 5,
                'precio' => 180.16,
                'inicio' => 179.35,
                'id_usuario' => 1439,
                'fecha' => '2019-09-04 04:58:00',
                'ejecutado' => 1,
            ),
            191 => 
            array (
                'id_alarmas' => 297,
                'id_cotizacion' => 7,
                'precio' => 1.916,
                'inicio' => 191.477,
                'id_usuario' => 380,
                'fecha' => '2019-09-04 06:46:00',
                'ejecutado' => 1,
            ),
            192 => 
            array (
                'id_alarmas' => 298,
                'id_cotizacion' => 10,
                'precio' => 1533.75,
                'inicio' => 1536.89,
                'id_usuario' => 1565,
                'fecha' => '2019-09-04 08:00:00',
                'ejecutado' => 1,
            ),
            193 => 
            array (
                'id_alarmas' => 301,
                'id_cotizacion' => 10,
                'precio' => 1530.8,
                'inicio' => 1537.04,
                'id_usuario' => 1565,
                'fecha' => '2019-09-04 08:01:00',
                'ejecutado' => 1,
            ),
            194 => 
            array (
                'id_alarmas' => 302,
                'id_cotizacion' => 10,
                'precio' => 1538.7,
                'inicio' => 1538.05,
                'id_usuario' => 1538,
                'fecha' => '2019-09-04 08:25:00',
                'ejecutado' => 1,
            ),
            195 => 
            array (
                'id_alarmas' => 305,
                'id_cotizacion' => 10,
                'precio' => 1539.4,
                'inicio' => 1539.06,
                'id_usuario' => 1538,
                'fecha' => '2019-09-04 09:44:00',
                'ejecutado' => 1,
            ),
            196 => 
            array (
                'id_alarmas' => 306,
                'id_cotizacion' => 10,
                'precio' => 1538.95,
                'inicio' => 1538.64,
                'id_usuario' => 1538,
                'fecha' => '2019-09-04 09:58:00',
                'ejecutado' => 1,
            ),
            197 => 
            array (
                'id_alarmas' => 308,
                'id_cotizacion' => 10,
                'precio' => 1537.26,
                'inicio' => 1537.03,
                'id_usuario' => 1538,
                'fecha' => '2019-09-04 10:24:00',
                'ejecutado' => 1,
            ),
            198 => 
            array (
                'id_alarmas' => 309,
                'id_cotizacion' => 2,
                'precio' => 1541.88,
                'inicio' => 0.25845,
                'id_usuario' => 388,
                'fecha' => '2019-09-04 13:08:00',
                'ejecutado' => 0,
            ),
            199 => 
            array (
                'id_alarmas' => 310,
                'id_cotizacion' => 10,
                'precio' => 1541.88,
                'inicio' => 1542.27,
                'id_usuario' => 1108,
                'fecha' => '2019-09-04 13:19:00',
                'ejecutado' => 1,
            ),
            200 => 
            array (
                'id_alarmas' => 311,
                'id_cotizacion' => 10,
                'precio' => 1541.88,
                'inicio' => 1542.31,
                'id_usuario' => 1108,
                'fecha' => '2019-09-04 13:19:00',
                'ejecutado' => 1,
            ),
            201 => 
            array (
                'id_alarmas' => 312,
                'id_cotizacion' => 10,
                'precio' => 1541.88,
                'inicio' => 1542.31,
                'id_usuario' => 1108,
                'fecha' => '2019-09-04 13:19:00',
                'ejecutado' => 1,
            ),
            202 => 
            array (
                'id_alarmas' => 314,
                'id_cotizacion' => 2,
                'precio' => 0.31,
                'inicio' => 0.2598,
                'id_usuario' => 671,
                'fecha' => '2019-09-05 04:32:00',
                'ejecutado' => 1,
            ),
            203 => 
            array (
                'id_alarmas' => 320,
                'id_cotizacion' => 8,
                'precio' => 178.761,
                'inicio' => 179.379,
                'id_usuario' => 1236,
                'fecha' => '2019-09-05 07:20:00',
                'ejecutado' => 1,
            ),
            204 => 
            array (
                'id_alarmas' => 321,
                'id_cotizacion' => 5,
                'precio' => 172.0,
                'inicio' => 174.89,
                'id_usuario' => 1236,
                'fecha' => '2019-09-05 07:21:00',
                'ejecutado' => 1,
            ),
            205 => 
            array (
                'id_alarmas' => 322,
                'id_cotizacion' => 7,
                'precio' => 192.025,
                'inicio' => 192.345,
                'id_usuario' => 380,
                'fecha' => '2019-09-05 08:26:00',
                'ejecutado' => 1,
            ),
            206 => 
            array (
                'id_alarmas' => 323,
                'id_cotizacion' => 8,
                'precio' => 179.845,
                'inicio' => 18.002,
                'id_usuario' => 380,
                'fecha' => '2019-09-05 08:26:00',
                'ejecutado' => 1,
            ),
            207 => 
            array (
                'id_alarmas' => 324,
                'id_cotizacion' => 8,
                'precio' => 180.215,
                'inicio' => 180.095,
                'id_usuario' => 179,
                'fecha' => '2019-09-05 09:25:00',
                'ejecutado' => 1,
            ),
            208 => 
            array (
                'id_alarmas' => 325,
                'id_cotizacion' => 8,
                'precio' => 180.215,
                'inicio' => 180.119,
                'id_usuario' => 179,
                'fecha' => '2019-09-05 09:26:00',
                'ejecutado' => 1,
            ),
            209 => 
            array (
                'id_alarmas' => 326,
                'id_cotizacion' => 10,
                'precio' => 1548.0,
                'inicio' => 1542.53,
                'id_usuario' => 1347,
                'fecha' => '2019-09-05 11:21:00',
                'ejecutado' => 0,
            ),
            210 => 
            array (
                'id_alarmas' => 327,
                'id_cotizacion' => 10,
                'precio' => 1537.0,
                'inicio' => 1531.62,
                'id_usuario' => 1347,
                'fecha' => '2019-09-05 13:42:00',
                'ejecutado' => 0,
            ),
            211 => 
            array (
                'id_alarmas' => 328,
                'id_cotizacion' => 10,
                'precio' => 1543.0,
                'inicio' => 1530.86,
                'id_usuario' => 1347,
                'fecha' => '2019-09-05 13:43:00',
                'ejecutado' => 0,
            ),
            212 => 
            array (
                'id_alarmas' => 329,
                'id_cotizacion' => 6,
                'precio' => 106.693,
                'inicio' => 106.973,
                'id_usuario' => 1186,
                'fecha' => '2019-09-05 15:20:00',
                'ejecutado' => 1,
            ),
            213 => 
            array (
                'id_alarmas' => 330,
                'id_cotizacion' => 6,
                'precio' => 106.693,
                'inicio' => 106.973,
                'id_usuario' => 1186,
                'fecha' => '2019-09-05 15:20:00',
                'ejecutado' => 1,
            ),
            214 => 
            array (
                'id_alarmas' => 331,
                'id_cotizacion' => 10,
                'precio' => 1527.0,
                'inicio' => 1518.35,
                'id_usuario' => 1347,
                'fecha' => '2019-09-05 20:05:00',
                'ejecutado' => 1,
            ),
            215 => 
            array (
                'id_alarmas' => 332,
                'id_cotizacion' => 10,
                'precio' => 1527.0,
                'inicio' => 1518.36,
                'id_usuario' => 1347,
                'fecha' => '2019-09-05 20:05:00',
                'ejecutado' => 1,
            ),
            216 => 
            array (
                'id_alarmas' => 333,
                'id_cotizacion' => 2,
                'precio' => 0.25582,
                'inicio' => 0.25582,
                'id_usuario' => 0,
                'fecha' => '2019-09-05 21:54:00',
                'ejecutado' => 0,
            ),
            217 => 
            array (
                'id_alarmas' => 334,
                'id_cotizacion' => 2,
                'precio' => 0.25582,
                'inicio' => 0.25582,
                'id_usuario' => 0,
                'fecha' => '2019-09-05 21:54:00',
                'ejecutado' => 0,
            ),
            218 => 
            array (
                'id_alarmas' => 335,
                'id_cotizacion' => 2,
                'precio' => 0.25582,
                'inicio' => 0.25582,
                'id_usuario' => 0,
                'fecha' => '2019-09-05 21:54:00',
                'ejecutado' => 0,
            ),
            219 => 
            array (
                'id_alarmas' => 336,
                'id_cotizacion' => 5,
                'precio' => 173.74,
                'inicio' => 174.24,
                'id_usuario' => 456,
                'fecha' => '2019-09-05 23:58:00',
                'ejecutado' => 1,
            ),
            220 => 
            array (
                'id_alarmas' => 337,
                'id_cotizacion' => 5,
                'precio' => 173.74,
                'inicio' => 174.24,
                'id_usuario' => 456,
                'fecha' => '2019-09-05 23:58:00',
                'ejecutado' => 1,
            ),
            221 => 
            array (
                'id_alarmas' => 338,
                'id_cotizacion' => 5,
                'precio' => 173.0,
                'inicio' => 174.23,
                'id_usuario' => 1347,
                'fecha' => '2019-09-06 00:16:00',
                'ejecutado' => 1,
            ),
            222 => 
            array (
                'id_alarmas' => 339,
                'id_cotizacion' => 9,
                'precio' => 26776.34,
                'inicio' => 26777.34,
                'id_usuario' => 1639,
                'fecha' => '2019-09-06 10:54:00',
                'ejecutado' => 1,
            ),
            223 => 
            array (
                'id_alarmas' => 340,
                'id_cotizacion' => 5,
                'precio' => 171.2,
                'inicio' => 177.24,
                'id_usuario' => 1639,
                'fecha' => '2019-09-06 15:30:00',
                'ejecutado' => 1,
            ),
            224 => 
            array (
                'id_alarmas' => 342,
                'id_cotizacion' => 5,
                'precio' => 176.11,
                'inicio' => 176.61,
                'id_usuario' => 1108,
                'fecha' => '2019-09-06 16:22:00',
                'ejecutado' => 1,
            ),
            225 => 
            array (
                'id_alarmas' => 343,
                'id_cotizacion' => 3,
                'precio' => 10149.0,
                'inicio' => 10368.4,
                'id_usuario' => 1654,
                'fecha' => '2019-09-06 20:57:00',
                'ejecutado' => 1,
            ),
            226 => 
            array (
                'id_alarmas' => 344,
                'id_cotizacion' => 3,
                'precio' => 10300.0,
                'inicio' => 10368.4,
                'id_usuario' => 1205,
                'fecha' => '2019-09-07 05:21:00',
                'ejecutado' => 1,
            ),
            227 => 
            array (
                'id_alarmas' => 347,
                'id_cotizacion' => 3,
                'precio' => 10400.0,
                'inicio' => 10381.41,
                'id_usuario' => 1205,
                'fecha' => '2019-09-07 05:46:00',
                'ejecutado' => 1,
            ),
            228 => 
            array (
                'id_alarmas' => 348,
                'id_cotizacion' => 3,
                'precio' => 10369.0,
                'inicio' => 10388.42,
                'id_usuario' => 1205,
                'fecha' => '2019-09-07 06:06:00',
                'ejecutado' => 1,
            ),
            229 => 
            array (
                'id_alarmas' => 352,
                'id_cotizacion' => 2,
                'precio' => 0.3,
                'inicio' => 0.26278,
                'id_usuario' => 81,
                'fecha' => '2019-09-07 19:37:00',
                'ejecutado' => 1,
            ),
            230 => 
            array (
                'id_alarmas' => 353,
                'id_cotizacion' => 2,
                'precio' => 0.2,
                'inicio' => 0.26278,
                'id_usuario' => 81,
                'fecha' => '2019-09-07 19:37:00',
                'ejecutado' => 0,
            ),
            231 => 
            array (
                'id_alarmas' => 354,
                'id_cotizacion' => 3,
                'precio' => 10487.0,
                'inicio' => 10517.56,
                'id_usuario' => 0,
                'fecha' => '2019-09-07 21:10:00',
                'ejecutado' => 1,
            ),
            232 => 
            array (
                'id_alarmas' => 355,
                'id_cotizacion' => 3,
                'precio' => 10490.0,
                'inicio' => 10521.57,
                'id_usuario' => 0,
                'fecha' => '2019-09-07 21:11:00',
                'ejecutado' => 1,
            ),
            233 => 
            array (
                'id_alarmas' => 356,
                'id_cotizacion' => 7,
                'precio' => 191.233,
                'inicio' => 191.233,
                'id_usuario' => 1710,
                'fecha' => '2019-09-07 21:36:00',
                'ejecutado' => 0,
            ),
            234 => 
            array (
                'id_alarmas' => 357,
                'id_cotizacion' => 7,
                'precio' => 191.233,
                'inicio' => 191.233,
                'id_usuario' => 1710,
                'fecha' => '2019-09-08 11:50:00',
                'ejecutado' => 0,
            ),
            235 => 
            array (
                'id_alarmas' => 358,
                'id_cotizacion' => 2,
                'precio' => 0.2644,
                'inicio' => 0.26349,
                'id_usuario' => 1545,
                'fecha' => '2019-09-08 15:13:00',
                'ejecutado' => 1,
            ),
            236 => 
            array (
                'id_alarmas' => 359,
                'id_cotizacion' => 9,
                'precio' => 26805.84,
                'inicio' => 26805.84,
                'id_usuario' => 1723,
                'fecha' => '2019-09-08 15:20:00',
                'ejecutado' => 0,
            ),
            237 => 
            array (
                'id_alarmas' => 360,
                'id_cotizacion' => 9,
                'precio' => 26805.84,
                'inicio' => 26805.84,
                'id_usuario' => 1723,
                'fecha' => '2019-09-08 15:20:00',
                'ejecutado' => 0,
            ),
            238 => 
            array (
                'id_alarmas' => 361,
                'id_cotizacion' => 9,
                'precio' => 26805.84,
                'inicio' => 26805.84,
                'id_usuario' => 1723,
                'fecha' => '2019-09-08 15:20:00',
                'ejecutado' => 0,
            ),
            239 => 
            array (
                'id_alarmas' => 362,
                'id_cotizacion' => 2,
                'precio' => 0.2644,
                'inicio' => 0.26381,
                'id_usuario' => 1545,
                'fecha' => '2019-09-08 15:49:00',
                'ejecutado' => 1,
            ),
            240 => 
            array (
                'id_alarmas' => 363,
                'id_cotizacion' => 2,
                'precio' => 0.2657,
                'inicio' => 0.26319,
                'id_usuario' => 1133,
                'fecha' => '2019-09-08 17:02:00',
                'ejecutado' => 1,
            ),
            241 => 
            array (
                'id_alarmas' => 364,
                'id_cotizacion' => 3,
                'precio' => 10500.0,
                'inicio' => 10409.44,
                'id_usuario' => 1630,
                'fecha' => '2019-09-08 17:47:00',
                'ejecutado' => 0,
            ),
            242 => 
            array (
                'id_alarmas' => 368,
                'id_cotizacion' => 2,
                'precio' => 0.25812,
                'inicio' => 0.26057,
                'id_usuario' => 445,
                'fecha' => '2019-09-08 20:32:00',
                'ejecutado' => 1,
            ),
            243 => 
            array (
                'id_alarmas' => 369,
                'id_cotizacion' => 7,
                'precio' => 191.254,
                'inicio' => 191.251,
                'id_usuario' => 406,
                'fecha' => '2019-09-08 23:27:00',
                'ejecutado' => 1,
            ),
            244 => 
            array (
                'id_alarmas' => 377,
                'id_cotizacion' => 2,
                'precio' => 0.2619,
                'inicio' => 0.26159,
                'id_usuario' => 1575,
                'fecha' => '2019-09-09 02:42:00',
                'ejecutado' => 1,
            ),
            245 => 
            array (
                'id_alarmas' => 378,
                'id_cotizacion' => 6,
                'precio' => 106.554,
                'inicio' => 106.91,
                'id_usuario' => 198,
                'fecha' => '2019-09-09 02:43:00',
                'ejecutado' => 1,
            ),
            246 => 
            array (
                'id_alarmas' => 379,
                'id_cotizacion' => 6,
                'precio' => 106.554,
                'inicio' => 106.91,
                'id_usuario' => 1702,
                'fecha' => '2019-09-09 02:43:00',
                'ejecutado' => 1,
            ),
            247 => 
            array (
                'id_alarmas' => 380,
                'id_cotizacion' => 6,
                'precio' => 106.554,
                'inicio' => 106.912,
                'id_usuario' => 1702,
                'fecha' => '2019-09-09 02:43:00',
                'ejecutado' => 1,
            ),
            248 => 
            array (
                'id_alarmas' => 382,
                'id_cotizacion' => 7,
                'precio' => 1.916,
                'inicio' => 191.382,
                'id_usuario' => 179,
                'fecha' => '2019-09-09 08:12:00',
                'ejecutado' => 1,
            ),
            249 => 
            array (
                'id_alarmas' => 383,
                'id_cotizacion' => 7,
                'precio' => 19.165,
                'inicio' => 191.453,
                'id_usuario' => 179,
                'fecha' => '2019-09-09 08:59:00',
                'ejecutado' => 1,
            ),
            250 => 
            array (
                'id_alarmas' => 386,
                'id_cotizacion' => 2,
                'precio' => 0.26146,
                'inicio' => 0.25853,
                'id_usuario' => 1470,
                'fecha' => '2019-09-09 12:08:00',
                'ejecutado' => 1,
            ),
            251 => 
            array (
                'id_alarmas' => 389,
                'id_cotizacion' => 3,
                'precio' => 10000.0,
                'inicio' => 10440.48,
                'id_usuario' => 1650,
                'fecha' => '2019-09-09 12:25:00',
                'ejecutado' => 1,
            ),
            252 => 
            array (
                'id_alarmas' => 393,
                'id_cotizacion' => 10,
                'precio' => 1508.0,
                'inicio' => 1513.76,
                'id_usuario' => 1707,
                'fecha' => '2019-09-09 13:08:00',
                'ejecutado' => 1,
            ),
            253 => 
            array (
                'id_alarmas' => 394,
                'id_cotizacion' => 10,
                'precio' => 1520.0,
                'inicio' => 1513.74,
                'id_usuario' => 1707,
                'fecha' => '2019-09-09 13:08:00',
                'ejecutado' => 1,
            ),
            254 => 
            array (
                'id_alarmas' => 395,
                'id_cotizacion' => 10,
                'precio' => 1502.5,
                'inicio' => 1506.18,
                'id_usuario' => 1707,
                'fecha' => '2019-09-09 14:40:00',
                'ejecutado' => 1,
            ),
            255 => 
            array (
                'id_alarmas' => 396,
                'id_cotizacion' => 10,
                'precio' => 1502.42,
                'inicio' => 1501.94,
                'id_usuario' => 1683,
                'fecha' => '2019-09-09 15:50:00',
                'ejecutado' => 1,
            ),
            256 => 
            array (
                'id_alarmas' => 397,
                'id_cotizacion' => 2,
                'precio' => 0.257,
                'inicio' => 0.25989,
                'id_usuario' => 1666,
                'fecha' => '2019-09-09 18:12:00',
                'ejecutado' => 1,
            ),
            257 => 
            array (
                'id_alarmas' => 398,
                'id_cotizacion' => 2,
                'precio' => 0.257,
                'inicio' => 0.25989,
                'id_usuario' => 1666,
                'fecha' => '2019-09-09 18:12:00',
                'ejecutado' => 1,
            ),
            258 => 
            array (
                'id_alarmas' => 400,
                'id_cotizacion' => 2,
                'precio' => 0.257,
                'inicio' => 0.25989,
                'id_usuario' => 1666,
                'fecha' => '2019-09-09 18:12:00',
                'ejecutado' => 1,
            ),
            259 => 
            array (
                'id_alarmas' => 402,
                'id_cotizacion' => 10,
                'precio' => 1497.1,
                'inicio' => 1500.56,
                'id_usuario' => 1755,
                'fecha' => '2019-09-09 19:57:00',
                'ejecutado' => 1,
            ),
            260 => 
            array (
                'id_alarmas' => 403,
                'id_cotizacion' => 2,
                'precio' => 0.2563,
                'inicio' => 0.25937,
                'id_usuario' => 1627,
                'fecha' => '2019-09-09 20:09:00',
                'ejecutado' => 1,
            ),
            261 => 
            array (
                'id_alarmas' => 404,
                'id_cotizacion' => 2,
                'precio' => 0.263,
                'inicio' => 0.25937,
                'id_usuario' => 1627,
                'fecha' => '2019-09-09 20:09:00',
                'ejecutado' => 1,
            ),
            262 => 
            array (
                'id_alarmas' => 405,
                'id_cotizacion' => 6,
                'precio' => 106.45,
                'inicio' => 107.241,
                'id_usuario' => 1633,
                'fecha' => '2019-09-09 20:32:00',
                'ejecutado' => 0,
            ),
            263 => 
            array (
                'id_alarmas' => 406,
                'id_cotizacion' => 9,
                'precio' => 26842.24,
                'inicio' => 26842.24,
                'id_usuario' => 1633,
                'fecha' => '2019-09-09 22:02:00',
                'ejecutado' => 0,
            ),
            264 => 
            array (
                'id_alarmas' => 407,
                'id_cotizacion' => 9,
                'precio' => 26674.18,
                'inicio' => 26842.24,
                'id_usuario' => 1633,
                'fecha' => '2019-09-09 22:02:00',
                'ejecutado' => 1,
            ),
            265 => 
            array (
                'id_alarmas' => 408,
                'id_cotizacion' => 5,
                'precio' => 180.0,
                'inicio' => 182.24,
                'id_usuario' => 1545,
                'fecha' => '2019-09-09 23:29:00',
                'ejecutado' => 1,
            ),
            266 => 
            array (
                'id_alarmas' => 409,
                'id_cotizacion' => 5,
                'precio' => 180.2,
                'inicio' => 182.24,
                'id_usuario' => 1545,
                'fecha' => '2019-09-09 23:29:00',
                'ejecutado' => 1,
            ),
            267 => 
            array (
                'id_alarmas' => 410,
                'id_cotizacion' => 10,
                'precio' => 1496.0,
                'inicio' => 1497.9,
                'id_usuario' => 1755,
                'fecha' => '2019-09-09 23:45:00',
                'ejecutado' => 1,
            ),
            268 => 
            array (
                'id_alarmas' => 411,
                'id_cotizacion' => 7,
                'precio' => 191.626,
                'inicio' => 192.055,
                'id_usuario' => 1755,
                'fecha' => '2019-09-09 23:47:00',
                'ejecutado' => 1,
            ),
            269 => 
            array (
                'id_alarmas' => 412,
                'id_cotizacion' => 5,
                'precio' => 178.8,
                'inicio' => 181.87,
                'id_usuario' => 1627,
                'fecha' => '2019-09-09 23:49:00',
                'ejecutado' => 1,
            ),
            270 => 
            array (
                'id_alarmas' => 413,
                'id_cotizacion' => 5,
                'precio' => 187.0,
                'inicio' => 181.87,
                'id_usuario' => 1627,
                'fecha' => '2019-09-09 23:49:00',
                'ejecutado' => 1,
            ),
            271 => 
            array (
                'id_alarmas' => 414,
                'id_cotizacion' => 5,
                'precio' => 183.9,
                'inicio' => 183.99,
                'id_usuario' => 445,
                'fecha' => '2019-09-10 02:08:00',
                'ejecutado' => 1,
            ),
            272 => 
            array (
                'id_alarmas' => 415,
                'id_cotizacion' => 5,
                'precio' => 183.6,
                'inicio' => 184.01,
                'id_usuario' => 445,
                'fecha' => '2019-09-10 02:08:00',
                'ejecutado' => 1,
            ),
            273 => 
            array (
                'id_alarmas' => 416,
                'id_cotizacion' => 2,
                'precio' => 0.26372,
                'inicio' => 0.26286,
                'id_usuario' => 1470,
                'fecha' => '2019-09-10 02:31:00',
                'ejecutado' => 1,
            ),
            274 => 
            array (
                'id_alarmas' => 418,
                'id_cotizacion' => 7,
                'precio' => 1.931,
                'inicio' => 192.536,
                'id_usuario' => 1347,
                'fecha' => '2019-09-10 12:24:00',
                'ejecutado' => 1,
            ),
            275 => 
            array (
                'id_alarmas' => 419,
                'id_cotizacion' => 2,
                'precio' => 0.26484,
                'inicio' => 0.26289,
                'id_usuario' => 1549,
                'fecha' => '2019-09-10 12:48:00',
                'ejecutado' => 1,
            ),
            276 => 
            array (
                'id_alarmas' => 420,
                'id_cotizacion' => 10,
                'precio' => 1488.0,
                'inicio' => 1496.02,
                'id_usuario' => 1347,
                'fecha' => '2019-09-10 12:56:00',
                'ejecutado' => 1,
            ),
            277 => 
            array (
                'id_alarmas' => 421,
                'id_cotizacion' => 2,
                'precio' => 10.0,
                'inicio' => 0.26004,
                'id_usuario' => 987,
                'fecha' => '2019-09-10 15:08:00',
                'ejecutado' => 0,
            ),
            278 => 
            array (
                'id_alarmas' => 428,
                'id_cotizacion' => 6,
                'precio' => 107.479,
                'inicio' => 107.482,
                'id_usuario' => 1779,
                'fecha' => '2019-09-10 18:39:00',
                'ejecutado' => 1,
            ),
            279 => 
            array (
                'id_alarmas' => 429,
                'id_cotizacion' => 2,
                'precio' => 0.32,
                'inicio' => 0.25927,
                'id_usuario' => 1776,
                'fecha' => '2019-09-10 18:48:00',
                'ejecutado' => 1,
            ),
            280 => 
            array (
                'id_alarmas' => 430,
                'id_cotizacion' => 10,
                'precio' => 1490.78,
                'inicio' => 1490.21,
                'id_usuario' => 1108,
                'fecha' => '2019-09-10 19:08:00',
                'ejecutado' => 1,
            ),
            281 => 
            array (
                'id_alarmas' => 431,
                'id_cotizacion' => 10,
                'precio' => 1490.78,
                'inicio' => 1490.18,
                'id_usuario' => 1108,
                'fecha' => '2019-09-10 19:08:00',
                'ejecutado' => 1,
            ),
            282 => 
            array (
                'id_alarmas' => 432,
                'id_cotizacion' => 10,
                'precio' => 1490.78,
                'inicio' => 1490.18,
                'id_usuario' => 1108,
                'fecha' => '2019-09-10 19:08:00',
                'ejecutado' => 1,
            ),
            283 => 
            array (
                'id_alarmas' => 433,
                'id_cotizacion' => 10,
                'precio' => 1490.78,
                'inicio' => 1489.98,
                'id_usuario' => 1108,
                'fecha' => '2019-09-10 19:11:00',
                'ejecutado' => 1,
            ),
            284 => 
            array (
                'id_alarmas' => 437,
                'id_cotizacion' => 10,
                'precio' => 1491.72,
                'inicio' => 1488.63,
                'id_usuario' => 1549,
                'fecha' => '2019-09-10 19:19:00',
                'ejecutado' => 1,
            ),
            285 => 
            array (
                'id_alarmas' => 438,
                'id_cotizacion' => 10,
                'precio' => 1491.72,
                'inicio' => 1488.59,
                'id_usuario' => 1549,
                'fecha' => '2019-09-10 19:19:00',
                'ejecutado' => 1,
            ),
            286 => 
            array (
                'id_alarmas' => 439,
                'id_cotizacion' => 10,
                'precio' => 1490.78,
                'inicio' => 1488.66,
                'id_usuario' => 1630,
                'fecha' => '2019-09-10 19:39:00',
                'ejecutado' => 1,
            ),
            287 => 
            array (
                'id_alarmas' => 443,
                'id_cotizacion' => 3,
                'precio' => 10200.0,
                'inicio' => 10125.13,
                'id_usuario' => 493,
                'fecha' => '2019-09-10 23:27:00',
                'ejecutado' => 1,
            ),
            288 => 
            array (
                'id_alarmas' => 444,
                'id_cotizacion' => 10,
                'precio' => 1497.71,
                'inicio' => 1488.46,
                'id_usuario' => 1247,
                'fecha' => '2019-09-11 00:28:00',
                'ejecutado' => 1,
            ),
            289 => 
            array (
                'id_alarmas' => 445,
                'id_cotizacion' => 10,
                'precio' => 1490.78,
                'inicio' => 1491.75,
                'id_usuario' => 1659,
                'fecha' => '2019-09-11 01:08:00',
                'ejecutado' => 1,
            ),
            290 => 
            array (
                'id_alarmas' => 446,
                'id_cotizacion' => 10,
                'precio' => 1490.78,
                'inicio' => 1491.79,
                'id_usuario' => 1108,
                'fecha' => '2019-09-11 01:10:00',
                'ejecutado' => 1,
            ),
            291 => 
            array (
                'id_alarmas' => 447,
                'id_cotizacion' => 10,
                'precio' => 1490.78,
                'inicio' => 1491.76,
                'id_usuario' => 1108,
                'fecha' => '2019-09-11 01:10:00',
                'ejecutado' => 1,
            ),
            292 => 
            array (
                'id_alarmas' => 448,
                'id_cotizacion' => 10,
                'precio' => 1490.78,
                'inicio' => 1491.76,
                'id_usuario' => 1108,
                'fecha' => '2019-09-11 01:10:00',
                'ejecutado' => 1,
            ),
            293 => 
            array (
                'id_alarmas' => 449,
                'id_cotizacion' => 10,
                'precio' => 1490.78,
                'inicio' => 1491.76,
                'id_usuario' => 1108,
                'fecha' => '2019-09-11 01:10:00',
                'ejecutado' => 1,
            ),
            294 => 
            array (
                'id_alarmas' => 450,
                'id_cotizacion' => 10,
                'precio' => 1487.19,
                'inicio' => 1491.16,
                'id_usuario' => 1470,
                'fecha' => '2019-09-11 03:22:00',
                'ejecutado' => 1,
            ),
            295 => 
            array (
                'id_alarmas' => 451,
                'id_cotizacion' => 10,
                'precio' => 1496.64,
                'inicio' => 1491.05,
                'id_usuario' => 1470,
                'fecha' => '2019-09-11 03:24:00',
                'ejecutado' => 1,
            ),
            296 => 
            array (
                'id_alarmas' => 452,
                'id_cotizacion' => 10,
                'precio' => 1495.0,
                'inicio' => 1491.27,
                'id_usuario' => 1627,
                'fecha' => '2019-09-11 04:21:00',
                'ejecutado' => 1,
            ),
            297 => 
            array (
                'id_alarmas' => 453,
                'id_cotizacion' => 10,
                'precio' => 1480.0,
                'inicio' => 1491.26,
                'id_usuario' => 1627,
                'fecha' => '2019-09-11 04:21:00',
                'ejecutado' => 1,
            ),
            298 => 
            array (
                'id_alarmas' => 454,
                'id_cotizacion' => 10,
                'precio' => 1500.0,
                'inicio' => 1491.17,
                'id_usuario' => 1627,
                'fecha' => '2019-09-11 04:21:00',
                'ejecutado' => 1,
            ),
            299 => 
            array (
                'id_alarmas' => 455,
                'id_cotizacion' => 5,
                'precio' => 182.6,
                'inicio' => 179.87,
                'id_usuario' => 1627,
                'fecha' => '2019-09-11 04:22:00',
                'ejecutado' => 1,
            ),
            300 => 
            array (
                'id_alarmas' => 456,
                'id_cotizacion' => 5,
                'precio' => 174.0,
                'inicio' => 179.87,
                'id_usuario' => 1627,
                'fecha' => '2019-09-11 04:22:00',
                'ejecutado' => 1,
            ),
            301 => 
            array (
                'id_alarmas' => 458,
                'id_cotizacion' => 5,
                'precio' => 181.0,
                'inicio' => 180.15,
                'id_usuario' => 1705,
                'fecha' => '2019-09-11 04:42:00',
                'ejecutado' => 1,
            ),
            302 => 
            array (
                'id_alarmas' => 459,
                'id_cotizacion' => 10,
                'precio' => 1489.0,
                'inicio' => 1490.77,
                'id_usuario' => 1311,
                'fecha' => '2019-09-11 04:42:00',
                'ejecutado' => 1,
            ),
            303 => 
            array (
                'id_alarmas' => 460,
                'id_cotizacion' => 10,
                'precio' => 1495.0,
                'inicio' => 1493.12,
                'id_usuario' => 1627,
                'fecha' => '2019-09-11 06:28:00',
                'ejecutado' => 1,
            ),
            304 => 
            array (
                'id_alarmas' => 461,
                'id_cotizacion' => 10,
                'precio' => 1485.0,
                'inicio' => 1493.3,
                'id_usuario' => 1627,
                'fecha' => '2019-09-11 06:28:00',
                'ejecutado' => 1,
            ),
            305 => 
            array (
                'id_alarmas' => 462,
                'id_cotizacion' => 10,
                'precio' => 1488.0,
                'inicio' => 1492.74,
                'id_usuario' => 1627,
                'fecha' => '2019-09-11 06:32:00',
                'ejecutado' => 1,
            ),
            306 => 
            array (
                'id_alarmas' => 463,
                'id_cotizacion' => 10,
                'precio' => 1484.0,
                'inicio' => 1490.43,
                'id_usuario' => 590,
                'fecha' => '2019-09-11 08:23:00',
                'ejecutado' => 1,
            ),
            307 => 
            array (
                'id_alarmas' => 464,
                'id_cotizacion' => 7,
                'precio' => 193.468,
                'inicio' => 192.579,
                'id_usuario' => 590,
                'fecha' => '2019-09-11 08:24:00',
                'ejecutado' => 1,
            ),
            308 => 
            array (
                'id_alarmas' => 467,
                'id_cotizacion' => 7,
                'precio' => 19.263,
                'inicio' => 192.533,
                'id_usuario' => 1108,
                'fecha' => '2019-09-11 10:35:00',
                'ejecutado' => 1,
            ),
            309 => 
            array (
                'id_alarmas' => 468,
                'id_cotizacion' => 7,
                'precio' => 19.263,
                'inicio' => 192.533,
                'id_usuario' => 1108,
                'fecha' => '2019-09-11 10:35:00',
                'ejecutado' => 1,
            ),
            310 => 
            array (
                'id_alarmas' => 469,
                'id_cotizacion' => 7,
                'precio' => 19.263,
                'inicio' => 192.533,
                'id_usuario' => 1108,
                'fecha' => '2019-09-11 10:35:00',
                'ejecutado' => 1,
            ),
            311 => 
            array (
                'id_alarmas' => 470,
                'id_cotizacion' => 7,
                'precio' => 19.263,
                'inicio' => 192.533,
                'id_usuario' => 1108,
                'fecha' => '2019-09-11 10:35:00',
                'ejecutado' => 1,
            ),
            312 => 
            array (
                'id_alarmas' => 471,
                'id_cotizacion' => 7,
                'precio' => 19.263,
                'inicio' => 192.533,
                'id_usuario' => 1108,
                'fecha' => '2019-09-11 10:35:00',
                'ejecutado' => 1,
            ),
            313 => 
            array (
                'id_alarmas' => 472,
                'id_cotizacion' => 7,
                'precio' => 19.263,
                'inicio' => 192.533,
                'id_usuario' => 1108,
                'fecha' => '2019-09-11 10:35:00',
                'ejecutado' => 1,
            ),
            314 => 
            array (
                'id_alarmas' => 473,
                'id_cotizacion' => 7,
                'precio' => 19.263,
                'inicio' => 192.533,
                'id_usuario' => 1108,
                'fecha' => '2019-09-11 10:35:00',
                'ejecutado' => 1,
            ),
            315 => 
            array (
                'id_alarmas' => 474,
                'id_cotizacion' => 7,
                'precio' => 19.263,
                'inicio' => 19.235,
                'id_usuario' => 1108,
                'fecha' => '2019-09-11 11:44:00',
                'ejecutado' => 1,
            ),
            316 => 
            array (
                'id_alarmas' => 475,
                'id_cotizacion' => 7,
                'precio' => 19.263,
                'inicio' => 19.235,
                'id_usuario' => 1108,
                'fecha' => '2019-09-11 11:44:00',
                'ejecutado' => 1,
            ),
            317 => 
            array (
                'id_alarmas' => 476,
                'id_cotizacion' => 7,
                'precio' => 19.263,
                'inicio' => 19.235,
                'id_usuario' => 1108,
                'fecha' => '2019-09-11 11:44:00',
                'ejecutado' => 1,
            ),
            318 => 
            array (
                'id_alarmas' => 477,
                'id_cotizacion' => 7,
                'precio' => 19.263,
                'inicio' => 19.235,
                'id_usuario' => 1108,
                'fecha' => '2019-09-11 11:44:00',
                'ejecutado' => 1,
            ),
            319 => 
            array (
                'id_alarmas' => 478,
                'id_cotizacion' => 7,
                'precio' => 19.263,
                'inicio' => 19.235,
                'id_usuario' => 1108,
                'fecha' => '2019-09-11 11:44:00',
                'ejecutado' => 1,
            ),
            320 => 
            array (
                'id_alarmas' => 479,
                'id_cotizacion' => 7,
                'precio' => 19.263,
                'inicio' => 19.235,
                'id_usuario' => 1108,
                'fecha' => '2019-09-11 11:44:00',
                'ejecutado' => 1,
            ),
            321 => 
            array (
                'id_alarmas' => 480,
                'id_cotizacion' => 10,
                'precio' => 1485.0,
                'inicio' => 1489.26,
                'id_usuario' => 1774,
                'fecha' => '2019-09-11 13:24:00',
                'ejecutado' => 1,
            ),
            322 => 
            array (
                'id_alarmas' => 481,
                'id_cotizacion' => 7,
                'precio' => 19.267,
                'inicio' => 192.368,
                'id_usuario' => 1734,
                'fecha' => '2019-09-11 15:03:00',
                'ejecutado' => 1,
            ),
            323 => 
            array (
                'id_alarmas' => 482,
                'id_cotizacion' => 7,
                'precio' => 192.628,
                'inicio' => 1.923,
                'id_usuario' => 1777,
                'fecha' => '2019-09-11 15:16:00',
                'ejecutado' => 1,
            ),
            324 => 
            array (
                'id_alarmas' => 484,
                'id_cotizacion' => 10,
                'precio' => 1491.0,
                'inicio' => 1496.08,
                'id_usuario' => 1777,
                'fecha' => '2019-09-11 15:16:00',
                'ejecutado' => 1,
            ),
            325 => 
            array (
                'id_alarmas' => 485,
                'id_cotizacion' => 7,
                'precio' => 192.497,
                'inicio' => 192.301,
                'id_usuario' => 1470,
                'fecha' => '2019-09-11 15:19:00',
                'ejecutado' => 1,
            ),
            326 => 
            array (
                'id_alarmas' => 486,
                'id_cotizacion' => 7,
                'precio' => 19.263,
                'inicio' => 192.211,
                'id_usuario' => 1720,
                'fecha' => '2019-09-11 15:26:00',
                'ejecutado' => 1,
            ),
            327 => 
            array (
                'id_alarmas' => 487,
                'id_cotizacion' => 10,
                'precio' => 1496.76,
                'inicio' => 1495.94,
                'id_usuario' => 1779,
                'fecha' => '2019-09-11 17:31:00',
                'ejecutado' => 1,
            ),
            328 => 
            array (
                'id_alarmas' => 494,
                'id_cotizacion' => 10,
                'precio' => 1494.57,
                'inicio' => 1496.28,
                'id_usuario' => 1709,
                'fecha' => '2019-09-11 18:46:00',
                'ejecutado' => 1,
            ),
            329 => 
            array (
                'id_alarmas' => 500,
                'id_cotizacion' => 10,
                'precio' => 1497.5,
                'inicio' => 1496.68,
                'id_usuario' => 1709,
                'fecha' => '2019-09-11 18:47:00',
                'ejecutado' => 1,
            ),
            330 => 
            array (
                'id_alarmas' => 501,
                'id_cotizacion' => 7,
                'precio' => 192.499,
                'inicio' => 192.295,
                'id_usuario' => 1660,
                'fecha' => '2019-09-11 18:53:00',
                'ejecutado' => 1,
            ),
            331 => 
            array (
                'id_alarmas' => 502,
                'id_cotizacion' => 10,
                'precio' => 1499.0,
                'inicio' => 1497.62,
                'id_usuario' => 1709,
                'fecha' => '2019-09-11 18:55:00',
                'ejecutado' => 1,
            ),
            332 => 
            array (
                'id_alarmas' => 503,
                'id_cotizacion' => 7,
                'precio' => 192.499,
                'inicio' => 192.221,
                'id_usuario' => 1108,
                'fecha' => '2019-09-11 19:58:00',
                'ejecutado' => 1,
            ),
            333 => 
            array (
                'id_alarmas' => 504,
                'id_cotizacion' => 7,
                'precio' => 192.499,
                'inicio' => 192.221,
                'id_usuario' => 1108,
                'fecha' => '2019-09-11 19:58:00',
                'ejecutado' => 1,
            ),
            334 => 
            array (
                'id_alarmas' => 505,
                'id_cotizacion' => 7,
                'precio' => 192.499,
                'inicio' => 192.216,
                'id_usuario' => 1108,
                'fecha' => '2019-09-11 19:58:00',
                'ejecutado' => 1,
            ),
            335 => 
            array (
                'id_alarmas' => 506,
                'id_cotizacion' => 7,
                'precio' => 192.499,
                'inicio' => 192.216,
                'id_usuario' => 1108,
                'fecha' => '2019-09-11 19:58:00',
                'ejecutado' => 1,
            ),
            336 => 
            array (
                'id_alarmas' => 507,
                'id_cotizacion' => 7,
                'precio' => 192.499,
                'inicio' => 192.216,
                'id_usuario' => 1108,
                'fecha' => '2019-09-11 19:58:00',
                'ejecutado' => 1,
            ),
            337 => 
            array (
                'id_alarmas' => 508,
                'id_cotizacion' => 7,
                'precio' => 192.499,
                'inicio' => 192.216,
                'id_usuario' => 1108,
                'fecha' => '2019-09-11 19:58:00',
                'ejecutado' => 1,
            ),
            338 => 
            array (
                'id_alarmas' => 509,
                'id_cotizacion' => 7,
                'precio' => 192.499,
                'inicio' => 192.221,
                'id_usuario' => 1108,
                'fecha' => '2019-09-11 19:58:00',
                'ejecutado' => 1,
            ),
            339 => 
            array (
                'id_alarmas' => 510,
                'id_cotizacion' => 7,
                'precio' => 192.499,
                'inicio' => 192.219,
                'id_usuario' => 1108,
                'fecha' => '2019-09-11 19:58:00',
                'ejecutado' => 1,
            ),
            340 => 
            array (
                'id_alarmas' => 511,
                'id_cotizacion' => 7,
                'precio' => 192.499,
                'inicio' => 192.219,
                'id_usuario' => 1108,
                'fecha' => '2019-09-11 19:58:00',
                'ejecutado' => 1,
            ),
            341 => 
            array (
                'id_alarmas' => 513,
                'id_cotizacion' => 5,
                'precio' => 175.6,
                'inicio' => 178.71,
                'id_usuario' => 1133,
                'fecha' => '2019-09-12 00:39:00',
                'ejecutado' => 1,
            ),
            342 => 
            array (
                'id_alarmas' => 525,
                'id_cotizacion' => 10,
                'precio' => 1490.78,
                'inicio' => 1495.41,
                'id_usuario' => 1640,
                'fecha' => '2019-09-12 04:53:00',
                'ejecutado' => 1,
            ),
            343 => 
            array (
                'id_alarmas' => 526,
                'id_cotizacion' => 10,
                'precio' => 1500.0,
                'inicio' => 1503.78,
                'id_usuario' => 1626,
                'fecha' => '2019-09-12 07:23:00',
                'ejecutado' => 1,
            ),
            344 => 
            array (
                'id_alarmas' => 527,
                'id_cotizacion' => 10,
                'precio' => 1501.0,
                'inicio' => 1503.87,
                'id_usuario' => 1626,
                'fecha' => '2019-09-12 07:23:00',
                'ejecutado' => 1,
            ),
            345 => 
            array (
                'id_alarmas' => 528,
                'id_cotizacion' => 7,
                'precio' => 192.615,
                'inicio' => 192.032,
                'id_usuario' => 1691,
                'fecha' => '2019-09-12 14:15:00',
                'ejecutado' => 1,
            ),
            346 => 
            array (
                'id_alarmas' => 529,
                'id_cotizacion' => 7,
                'precio' => 192.615,
                'inicio' => 192.066,
                'id_usuario' => 1691,
                'fecha' => '2019-09-12 14:15:00',
                'ejecutado' => 1,
            ),
            347 => 
            array (
                'id_alarmas' => 530,
                'id_cotizacion' => 7,
                'precio' => 192.615,
                'inicio' => 192.052,
                'id_usuario' => 1691,
                'fecha' => '2019-09-12 14:15:00',
                'ejecutado' => 1,
            ),
            348 => 
            array (
                'id_alarmas' => 532,
                'id_cotizacion' => 7,
                'precio' => 19.219,
                'inicio' => 192.037,
                'id_usuario' => 1752,
                'fecha' => '2019-09-12 15:06:00',
                'ejecutado' => 1,
            ),
            349 => 
            array (
                'id_alarmas' => 534,
                'id_cotizacion' => 5,
                'precio' => 180.66,
                'inicio' => 179.6,
                'id_usuario' => 1798,
                'fecha' => '2019-09-12 17:40:00',
                'ejecutado' => 1,
            ),
            350 => 
            array (
                'id_alarmas' => 535,
                'id_cotizacion' => 5,
                'precio' => 180.66,
                'inicio' => 179.6,
                'id_usuario' => 1798,
                'fecha' => '2019-09-12 17:40:00',
                'ejecutado' => 1,
            ),
            351 => 
            array (
                'id_alarmas' => 536,
                'id_cotizacion' => 5,
                'precio' => 180.66,
                'inicio' => 179.6,
                'id_usuario' => 1798,
                'fecha' => '2019-09-12 17:40:00',
                'ejecutado' => 1,
            ),
            352 => 
            array (
                'id_alarmas' => 537,
                'id_cotizacion' => 5,
                'precio' => 180.66,
                'inicio' => 179.6,
                'id_usuario' => 1798,
                'fecha' => '2019-09-12 17:40:00',
                'ejecutado' => 1,
            ),
            353 => 
            array (
                'id_alarmas' => 538,
                'id_cotizacion' => 5,
                'precio' => 180.66,
                'inicio' => 179.6,
                'id_usuario' => 1798,
                'fecha' => '2019-09-12 17:40:00',
                'ejecutado' => 1,
            ),
            354 => 
            array (
                'id_alarmas' => 539,
                'id_cotizacion' => 5,
                'precio' => 180.66,
                'inicio' => 179.6,
                'id_usuario' => 1798,
                'fecha' => '2019-09-12 17:40:00',
                'ejecutado' => 1,
            ),
            355 => 
            array (
                'id_alarmas' => 540,
                'id_cotizacion' => 5,
                'precio' => 180.66,
                'inicio' => 179.6,
                'id_usuario' => 1798,
                'fecha' => '2019-09-12 17:40:00',
                'ejecutado' => 1,
            ),
            356 => 
            array (
                'id_alarmas' => 541,
                'id_cotizacion' => 5,
                'precio' => 180.66,
                'inicio' => 179.6,
                'id_usuario' => 1798,
                'fecha' => '2019-09-12 17:40:00',
                'ejecutado' => 1,
            ),
            357 => 
            array (
                'id_alarmas' => 542,
                'id_cotizacion' => 5,
                'precio' => 180.66,
                'inicio' => 179.6,
                'id_usuario' => 1798,
                'fecha' => '2019-09-12 17:40:00',
                'ejecutado' => 1,
            ),
            358 => 
            array (
                'id_alarmas' => 543,
                'id_cotizacion' => 5,
                'precio' => 180.66,
                'inicio' => 179.6,
                'id_usuario' => 1798,
                'fecha' => '2019-09-12 17:40:00',
                'ejecutado' => 1,
            ),
            359 => 
            array (
                'id_alarmas' => 544,
                'id_cotizacion' => 5,
                'precio' => 180.66,
                'inicio' => 179.6,
                'id_usuario' => 1798,
                'fecha' => '2019-09-12 17:40:00',
                'ejecutado' => 1,
            ),
            360 => 
            array (
                'id_alarmas' => 545,
                'id_cotizacion' => 5,
                'precio' => 180.62,
                'inicio' => 180.45,
                'id_usuario' => 1792,
                'fecha' => '2019-09-12 18:33:00',
                'ejecutado' => 1,
            ),
            361 => 
            array (
                'id_alarmas' => 550,
                'id_cotizacion' => 5,
                'precio' => 178.5,
                'inicio' => 180.43,
                'id_usuario' => 1427,
                'fecha' => '2019-09-12 18:36:00',
                'ejecutado' => 1,
            ),
            362 => 
            array (
                'id_alarmas' => 551,
                'id_cotizacion' => 5,
                'precio' => 179.0,
                'inicio' => 180.03,
                'id_usuario' => 1627,
                'fecha' => '2019-09-12 18:58:00',
                'ejecutado' => 1,
            ),
            363 => 
            array (
                'id_alarmas' => 552,
                'id_cotizacion' => 5,
                'precio' => 182.6,
                'inicio' => 180.02,
                'id_usuario' => 1627,
                'fecha' => '2019-09-12 18:58:00',
                'ejecutado' => 1,
            ),
            364 => 
            array (
                'id_alarmas' => 553,
                'id_cotizacion' => 5,
                'precio' => 175.0,
                'inicio' => 180.02,
                'id_usuario' => 1627,
                'fecha' => '2019-09-12 18:58:00',
                'ejecutado' => 1,
            ),
            365 => 
            array (
                'id_alarmas' => 556,
                'id_cotizacion' => 10,
                'precio' => 1501.0,
                'inicio' => 1499.47,
                'id_usuario' => 1787,
                'fecha' => '2019-09-12 19:20:00',
                'ejecutado' => 1,
            ),
            366 => 
            array (
                'id_alarmas' => 557,
                'id_cotizacion' => 10,
                'precio' => 1497.0,
                'inicio' => 1499.59,
                'id_usuario' => 1787,
                'fecha' => '2019-09-12 19:20:00',
                'ejecutado' => 1,
            ),
            367 => 
            array (
                'id_alarmas' => 558,
                'id_cotizacion' => 5,
                'precio' => 180.0,
                'inicio' => 181.22,
                'id_usuario' => 1133,
                'fecha' => '2019-09-13 01:37:00',
                'ejecutado' => 1,
            ),
            368 => 
            array (
                'id_alarmas' => 559,
                'id_cotizacion' => 10,
                'precio' => 1488.0,
                'inicio' => 1498.29,
                'id_usuario' => 1347,
                'fecha' => '2019-09-13 04:51:00',
                'ejecutado' => 1,
            ),
            369 => 
            array (
                'id_alarmas' => 560,
                'id_cotizacion' => 9,
                'precio' => 27289.0,
                'inicio' => 27252.34,
                'id_usuario' => 1749,
                'fecha' => '2019-09-13 08:29:00',
                'ejecutado' => 1,
            ),
            370 => 
            array (
                'id_alarmas' => 561,
                'id_cotizacion' => 9,
                'precio' => 27289.0,
                'inicio' => 27253.84,
                'id_usuario' => 1749,
                'fecha' => '2019-09-13 08:29:00',
                'ejecutado' => 1,
            ),
            371 => 
            array (
                'id_alarmas' => 562,
                'id_cotizacion' => 9,
                'precio' => 27290.0,
                'inicio' => 27260.34,
                'id_usuario' => 1749,
                'fecha' => '2019-09-13 08:41:00',
                'ejecutado' => 1,
            ),
            372 => 
            array (
                'id_alarmas' => 565,
                'id_cotizacion' => 2,
                'precio' => 0.258,
                'inicio' => 0.26328,
                'id_usuario' => 493,
                'fecha' => '2019-09-14 18:29:00',
                'ejecutado' => 1,
            ),
            373 => 
            array (
                'id_alarmas' => 566,
                'id_cotizacion' => 5,
                'precio' => 184.5,
                'inicio' => 188.19,
                'id_usuario' => 493,
                'fecha' => '2019-09-14 18:30:00',
                'ejecutado' => 1,
            ),
            374 => 
            array (
                'id_alarmas' => 567,
                'id_cotizacion' => 10,
                'precio' => 1498.0,
                'inicio' => 1489.19,
                'id_usuario' => 493,
                'fecha' => '2019-09-14 18:32:00',
                'ejecutado' => 1,
            ),
            375 => 
            array (
                'id_alarmas' => 568,
                'id_cotizacion' => 7,
                'precio' => 19.479,
                'inicio' => 196.106,
                'id_usuario' => 493,
                'fecha' => '2019-09-14 18:35:00',
                'ejecutado' => 1,
            ),
            376 => 
            array (
                'id_alarmas' => 570,
                'id_cotizacion' => 2,
                'precio' => 0.26269,
                'inicio' => 0.26117,
                'id_usuario' => 1133,
                'fecha' => '2019-09-15 19:50:00',
                'ejecutado' => 1,
            ),
            377 => 
            array (
                'id_alarmas' => 571,
                'id_cotizacion' => 3,
                'precio' => 10346.3,
                'inicio' => 10346.37,
                'id_usuario' => 1133,
                'fecha' => '2019-09-15 20:15:00',
                'ejecutado' => 1,
            ),
            378 => 
            array (
                'id_alarmas' => 572,
                'id_cotizacion' => 3,
                'precio' => 10346.3,
                'inicio' => 10344.37,
                'id_usuario' => 1133,
                'fecha' => '2019-09-15 20:17:00',
                'ejecutado' => 1,
            ),
            379 => 
            array (
                'id_alarmas' => 573,
                'id_cotizacion' => 3,
                'precio' => 10346.9,
                'inicio' => 10344.37,
                'id_usuario' => 1133,
                'fecha' => '2019-09-15 20:17:00',
                'ejecutado' => 1,
            ),
            380 => 
            array (
                'id_alarmas' => 574,
                'id_cotizacion' => 3,
                'precio' => 10346.9,
                'inicio' => 10344.37,
                'id_usuario' => 1133,
                'fecha' => '2019-09-15 20:17:00',
                'ejecutado' => 1,
            ),
            381 => 
            array (
                'id_alarmas' => 575,
                'id_cotizacion' => 8,
                'precio' => 180.782,
                'inicio' => 181.668,
                'id_usuario' => 1717,
                'fecha' => '2019-09-15 23:22:00',
                'ejecutado' => 1,
            ),
            382 => 
            array (
                'id_alarmas' => 576,
                'id_cotizacion' => 8,
                'precio' => 181.581,
                'inicio' => 181.635,
                'id_usuario' => 1752,
                'fecha' => '2019-09-15 23:32:00',
                'ejecutado' => 1,
            ),
            383 => 
            array (
                'id_alarmas' => 577,
                'id_cotizacion' => 8,
                'precio' => 18.205,
                'inicio' => 181.791,
                'id_usuario' => 1627,
                'fecha' => '2019-09-16 00:32:00',
                'ejecutado' => 1,
            ),
            384 => 
            array (
                'id_alarmas' => 578,
                'id_cotizacion' => 6,
                'precio' => 107.5,
                'inicio' => 107.593,
                'id_usuario' => 1627,
                'fecha' => '2019-09-16 00:32:00',
                'ejecutado' => 1,
            ),
            385 => 
            array (
                'id_alarmas' => 579,
                'id_cotizacion' => 8,
                'precio' => 1.806,
                'inicio' => 181.359,
                'id_usuario' => 1691,
                'fecha' => '2019-09-16 05:45:00',
                'ejecutado' => 1,
            ),
            386 => 
            array (
                'id_alarmas' => 580,
                'id_cotizacion' => 10,
                'precio' => 1489.0,
                'inicio' => 1504.07,
                'id_usuario' => 1749,
                'fecha' => '2019-09-16 07:58:00',
                'ejecutado' => 1,
            ),
            387 => 
            array (
                'id_alarmas' => 581,
                'id_cotizacion' => 8,
                'precio' => 1.802,
                'inicio' => 181.202,
                'id_usuario' => 1749,
                'fecha' => '2019-09-16 07:59:00',
                'ejecutado' => 0,
            ),
            388 => 
            array (
                'id_alarmas' => 582,
                'id_cotizacion' => 9,
                'precio' => 27200.0,
                'inicio' => 27107.84,
                'id_usuario' => 1749,
                'fecha' => '2019-09-16 08:00:00',
                'ejecutado' => 1,
            ),
            389 => 
            array (
                'id_alarmas' => 584,
                'id_cotizacion' => 7,
                'precio' => 195.232,
                'inicio' => 195.126,
                'id_usuario' => 245,
                'fecha' => '2019-09-16 11:11:00',
                'ejecutado' => 1,
            ),
            390 => 
            array (
                'id_alarmas' => 585,
                'id_cotizacion' => 7,
                'precio' => 195.232,
                'inicio' => 195.126,
                'id_usuario' => 245,
                'fecha' => '2019-09-16 11:11:00',
                'ejecutado' => 1,
            ),
            391 => 
            array (
                'id_alarmas' => 592,
                'id_cotizacion' => 7,
                'precio' => 1.952,
                'inicio' => 195.126,
                'id_usuario' => 245,
                'fecha' => '2019-09-16 11:11:00',
                'ejecutado' => 1,
            ),
            392 => 
            array (
                'id_alarmas' => 593,
                'id_cotizacion' => 7,
                'precio' => 1.976,
                'inicio' => 195.131,
                'id_usuario' => 1691,
                'fecha' => '2019-09-16 11:11:00',
                'ejecutado' => 1,
            ),
            393 => 
            array (
                'id_alarmas' => 594,
                'id_cotizacion' => 7,
                'precio' => 195.225,
                'inicio' => 195.207,
                'id_usuario' => 245,
                'fecha' => '2019-09-16 11:18:00',
                'ejecutado' => 1,
            ),
            394 => 
            array (
                'id_alarmas' => 595,
                'id_cotizacion' => 7,
                'precio' => 19.575,
                'inicio' => 195.589,
                'id_usuario' => 1740,
                'fecha' => '2019-09-16 12:11:00',
                'ejecutado' => 1,
            ),
            395 => 
            array (
                'id_alarmas' => 599,
                'id_cotizacion' => 3,
                'precio' => 10160.01,
                'inicio' => 10160.17,
                'id_usuario' => 1787,
                'fecha' => '2019-09-16 18:52:00',
                'ejecutado' => 1,
            ),
            396 => 
            array (
                'id_alarmas' => 600,
                'id_cotizacion' => 2,
                'precio' => 0.25976,
                'inicio' => 0.26277,
                'id_usuario' => 1631,
                'fecha' => '2019-09-16 21:10:00',
                'ejecutado' => 1,
            ),
            397 => 
            array (
                'id_alarmas' => 602,
                'id_cotizacion' => 6,
                'precio' => 107.8,
                'inicio' => 108.13,
                'id_usuario' => 1691,
                'fecha' => '2019-09-16 22:04:00',
                'ejecutado' => 1,
            ),
            398 => 
            array (
                'id_alarmas' => 603,
                'id_cotizacion' => 3,
                'precio' => 10267.29,
                'inicio' => 10267.29,
                'id_usuario' => 1133,
                'fecha' => '2019-09-17 00:52:00',
                'ejecutado' => 0,
            ),
            399 => 
            array (
                'id_alarmas' => 604,
                'id_cotizacion' => 3,
                'precio' => 10243.1,
                'inicio' => 10267.29,
                'id_usuario' => 1133,
                'fecha' => '2019-09-17 00:52:00',
                'ejecutado' => 1,
            ),
            400 => 
            array (
                'id_alarmas' => 605,
                'id_cotizacion' => 9,
                'precio' => 27159.5,
                'inicio' => 27074.74,
                'id_usuario' => 1133,
                'fecha' => '2019-09-17 01:22:00',
                'ejecutado' => 1,
            ),
            401 => 
            array (
                'id_alarmas' => 606,
                'id_cotizacion' => 6,
                'precio' => 109.047,
                'inicio' => 108.224,
                'id_usuario' => 1633,
                'fecha' => '2019-09-17 04:33:00',
                'ejecutado' => 1,
            ),
            402 => 
            array (
                'id_alarmas' => 607,
                'id_cotizacion' => 10,
                'precio' => 1496.9,
                'inicio' => 1498.35,
                'id_usuario' => 728,
                'fecha' => '2019-09-17 04:37:00',
                'ejecutado' => 1,
            ),
            403 => 
            array (
                'id_alarmas' => 608,
                'id_cotizacion' => 9,
                'precio' => 27024.0,
                'inicio' => 27057.44,
                'id_usuario' => 1784,
                'fecha' => '2019-09-17 08:33:00',
                'ejecutado' => 1,
            ),
            404 => 
            array (
                'id_alarmas' => 609,
                'id_cotizacion' => 10,
                'precio' => 1503.0,
                'inicio' => 1498.6,
                'id_usuario' => 1784,
                'fecha' => '2019-09-17 08:34:00',
                'ejecutado' => 1,
            ),
            405 => 
            array (
                'id_alarmas' => 610,
                'id_cotizacion' => 7,
                'precio' => 198.199,
                'inicio' => 196.635,
                'id_usuario' => 1786,
                'fecha' => '2019-09-17 14:26:00',
                'ejecutado' => 1,
            ),
            406 => 
            array (
                'id_alarmas' => 611,
                'id_cotizacion' => 7,
                'precio' => 194.232,
                'inicio' => 196.669,
                'id_usuario' => 1786,
                'fecha' => '2019-09-17 14:27:00',
                'ejecutado' => 1,
            ),
            407 => 
            array (
                'id_alarmas' => 612,
                'id_cotizacion' => 2,
                'precio' => 0.29,
                'inicio' => 0.2943,
                'id_usuario' => 1549,
                'fecha' => '2019-09-18 00:44:00',
                'ejecutado' => 1,
            ),
            408 => 
            array (
                'id_alarmas' => 613,
                'id_cotizacion' => 2,
                'precio' => 0.29,
                'inicio' => 0.29429,
                'id_usuario' => 1549,
                'fecha' => '2019-09-18 00:44:00',
                'ejecutado' => 1,
            ),
            409 => 
            array (
                'id_alarmas' => 614,
                'id_cotizacion' => 2,
                'precio' => 0.303,
                'inicio' => 0.29953,
                'id_usuario' => 1811,
                'fecha' => '2019-09-18 00:55:00',
                'ejecutado' => 1,
            ),
            410 => 
            array (
                'id_alarmas' => 615,
                'id_cotizacion' => 8,
                'precio' => 181.812,
                'inicio' => 182.323,
                'id_usuario' => 1313,
                'fecha' => '2019-09-18 02:20:00',
                'ejecutado' => 1,
            ),
            411 => 
            array (
                'id_alarmas' => 616,
                'id_cotizacion' => 6,
                'precio' => 107.73,
                'inicio' => 108.229,
                'id_usuario' => 1313,
                'fecha' => '2019-09-18 02:21:00',
                'ejecutado' => 1,
            ),
            412 => 
            array (
                'id_alarmas' => 618,
                'id_cotizacion' => 6,
                'precio' => 108.439,
                'inicio' => 108.208,
                'id_usuario' => 1786,
                'fecha' => '2019-09-18 04:08:00',
                'ejecutado' => 1,
            ),
            413 => 
            array (
                'id_alarmas' => 619,
                'id_cotizacion' => 6,
                'precio' => 108.265,
                'inicio' => 108.213,
                'id_usuario' => 104,
                'fecha' => '2019-09-18 04:45:00',
                'ejecutado' => 1,
            ),
            414 => 
            array (
                'id_alarmas' => 621,
                'id_cotizacion' => 3,
                'precio' => 10188.8,
                'inicio' => 10203.22,
                'id_usuario' => 1236,
                'fecha' => '2019-09-18 11:13:00',
                'ejecutado' => 1,
            ),
            415 => 
            array (
                'id_alarmas' => 622,
                'id_cotizacion' => 3,
                'precio' => 10160.0,
                'inicio' => 10203.22,
                'id_usuario' => 1236,
                'fecha' => '2019-09-18 11:13:00',
                'ejecutado' => 1,
            ),
            416 => 
            array (
                'id_alarmas' => 625,
                'id_cotizacion' => 2,
                'precio' => 0.30801,
                'inicio' => 0.30645,
                'id_usuario' => 1769,
                'fecha' => '2019-09-18 12:02:00',
                'ejecutado' => 1,
            ),
            417 => 
            array (
                'id_alarmas' => 626,
                'id_cotizacion' => 6,
                'precio' => 108.0,
                'inicio' => 108.158,
                'id_usuario' => 1730,
                'fecha' => '2019-09-18 12:08:00',
                'ejecutado' => 1,
            ),
            418 => 
            array (
                'id_alarmas' => 628,
                'id_cotizacion' => 2,
                'precio' => 0.314,
                'inicio' => 0.30887,
                'id_usuario' => 1691,
                'fecha' => '2019-09-18 12:30:00',
                'ejecutado' => 1,
            ),
            419 => 
            array (
                'id_alarmas' => 629,
                'id_cotizacion' => 2,
                'precio' => 0.31555,
                'inicio' => 0.31554,
                'id_usuario' => 1760,
                'fecha' => '2019-09-18 17:58:00',
                'ejecutado' => 1,
            ),
            420 => 
            array (
                'id_alarmas' => 630,
                'id_cotizacion' => 2,
                'precio' => 0.322,
                'inicio' => 0.31554,
                'id_usuario' => 1760,
                'fecha' => '2019-09-18 17:59:00',
                'ejecutado' => 0,
            ),
            421 => 
            array (
                'id_alarmas' => 631,
                'id_cotizacion' => 2,
                'precio' => 0.3134,
                'inicio' => 0.31558,
                'id_usuario' => 1786,
                'fecha' => '2019-09-18 21:41:00',
                'ejecutado' => 1,
            ),
            422 => 
            array (
                'id_alarmas' => 633,
                'id_cotizacion' => 2,
                'precio' => 0.32,
                'inicio' => 0.31873,
                'id_usuario' => 1427,
                'fecha' => '2019-09-18 22:50:00',
                'ejecutado' => 0,
            ),
            423 => 
            array (
                'id_alarmas' => 636,
                'id_cotizacion' => 2,
                'precio' => 0.31674,
                'inicio' => 0.31488,
                'id_usuario' => 1659,
                'fecha' => '2019-09-18 23:59:00',
                'ejecutado' => 0,
            ),
            424 => 
            array (
                'id_alarmas' => 637,
                'id_cotizacion' => 6,
                'precio' => 108.0,
                'inicio' => 108.146,
                'id_usuario' => 1730,
                'fecha' => '2019-09-19 02:17:00',
                'ejecutado' => 1,
            ),
            425 => 
            array (
                'id_alarmas' => 639,
                'id_cotizacion' => 2,
                'precio' => 0.3052,
                'inicio' => 0.30516,
                'id_usuario' => 1659,
                'fecha' => '2019-09-19 02:36:00',
                'ejecutado' => 1,
            ),
            426 => 
            array (
                'id_alarmas' => 640,
                'id_cotizacion' => 5,
                'precio' => 204.7,
                'inicio' => 206.23,
                'id_usuario' => 380,
                'fecha' => '2019-09-19 05:42:00',
                'ejecutado' => 1,
            ),
            427 => 
            array (
                'id_alarmas' => 641,
                'id_cotizacion' => 9,
                'precio' => 27067.1,
                'inicio' => 27077.84,
                'id_usuario' => 1538,
                'fecha' => '2019-09-19 06:29:00',
                'ejecutado' => 1,
            ),
            428 => 
            array (
                'id_alarmas' => 642,
                'id_cotizacion' => 5,
                'precio' => 205.0,
                'inicio' => 207.58,
                'id_usuario' => 1626,
                'fecha' => '2019-09-19 08:36:00',
                'ejecutado' => 1,
            ),
            429 => 
            array (
                'id_alarmas' => 645,
                'id_cotizacion' => 6,
                'precio' => 107.83,
                'inicio' => 108.011,
                'id_usuario' => 1835,
                'fecha' => '2019-09-19 15:35:00',
                'ejecutado' => 1,
            ),
            430 => 
            array (
                'id_alarmas' => 646,
                'id_cotizacion' => 7,
                'precio' => 1.982,
                'inicio' => 198.015,
                'id_usuario' => 1786,
                'fecha' => '2019-09-19 15:54:00',
                'ejecutado' => 1,
            ),
            431 => 
            array (
                'id_alarmas' => 647,
                'id_cotizacion' => 2,
                'precio' => 0.3033,
                'inicio' => 0.30326,
                'id_usuario' => 728,
                'fecha' => '2019-09-19 17:05:00',
                'ejecutado' => 1,
            ),
            432 => 
            array (
                'id_alarmas' => 648,
                'id_cotizacion' => 2,
                'precio' => 0.30676,
                'inicio' => 0.30682,
                'id_usuario' => 1862,
                'fecha' => '2019-09-19 19:50:00',
                'ejecutado' => 1,
            ),
            433 => 
            array (
                'id_alarmas' => 649,
                'id_cotizacion' => 3,
                'precio' => 10081.08,
                'inicio' => 10081.08,
                'id_usuario' => 1862,
                'fecha' => '2019-09-19 19:50:00',
                'ejecutado' => 0,
            ),
            434 => 
            array (
                'id_alarmas' => 652,
                'id_cotizacion' => 8,
                'precio' => 184.304,
                'inicio' => 184.303,
                'id_usuario' => 1867,
                'fecha' => '2019-09-19 22:33:00',
                'ejecutado' => 1,
            ),
            435 => 
            array (
                'id_alarmas' => 655,
                'id_cotizacion' => 10,
                'precio' => 1498.74,
                'inicio' => 1499.43,
                'id_usuario' => 1819,
                'fecha' => '2019-09-19 23:48:00',
                'ejecutado' => 1,
            ),
            436 => 
            array (
                'id_alarmas' => 658,
                'id_cotizacion' => 10,
                'precio' => 1496.02,
                'inicio' => 1501.2,
                'id_usuario' => 1747,
                'fecha' => '2019-09-20 01:19:00',
                'ejecutado' => 1,
            ),
            437 => 
            array (
                'id_alarmas' => 660,
                'id_cotizacion' => 8,
                'precio' => 184.624,
                'inicio' => 18.461,
                'id_usuario' => 1867,
                'fecha' => '2019-09-20 01:44:00',
                'ejecutado' => 1,
            ),
            438 => 
            array (
                'id_alarmas' => 661,
                'id_cotizacion' => 10,
                'precio' => 1505.0,
                'inicio' => 1501.8,
                'id_usuario' => 1786,
                'fecha' => '2019-09-20 02:04:00',
                'ejecutado' => 1,
            ),
            439 => 
            array (
                'id_alarmas' => 662,
                'id_cotizacion' => 8,
                'precio' => 184.825,
                'inicio' => 184.744,
                'id_usuario' => 1786,
                'fecha' => '2019-09-20 02:16:00',
                'ejecutado' => 1,
            ),
            440 => 
            array (
                'id_alarmas' => 663,
                'id_cotizacion' => 8,
                'precio' => 185.221,
                'inicio' => 1.848,
                'id_usuario' => 1786,
                'fecha' => '2019-09-20 02:27:00',
                'ejecutado' => 1,
            ),
            441 => 
            array (
                'id_alarmas' => 664,
                'id_cotizacion' => 6,
                'precio' => 107.9,
                'inicio' => 107.955,
                'id_usuario' => 1688,
                'fecha' => '2019-09-20 02:36:00',
                'ejecutado' => 1,
            ),
            442 => 
            array (
                'id_alarmas' => 665,
                'id_cotizacion' => 10,
                'precio' => 1505.0,
                'inicio' => 1503.45,
                'id_usuario' => 1730,
                'fecha' => '2019-09-20 02:56:00',
                'ejecutado' => 1,
            ),
            443 => 
            array (
                'id_alarmas' => 667,
                'id_cotizacion' => 3,
                'precio' => 10340.0,
                'inicio' => 10256.27,
                'id_usuario' => 1795,
                'fecha' => '2019-09-20 03:10:00',
                'ejecutado' => 1,
            ),
            444 => 
            array (
                'id_alarmas' => 668,
                'id_cotizacion' => 3,
                'precio' => 10340.0,
                'inicio' => 10256.27,
                'id_usuario' => 1795,
                'fecha' => '2019-09-20 03:10:00',
                'ejecutado' => 1,
            ),
            445 => 
            array (
                'id_alarmas' => 669,
                'id_cotizacion' => 6,
                'precio' => 107.6,
                'inicio' => 107.833,
                'id_usuario' => 1691,
                'fecha' => '2019-09-20 04:39:00',
                'ejecutado' => 1,
            ),
            446 => 
            array (
                'id_alarmas' => 670,
                'id_cotizacion' => 10,
                'precio' => 1507.14,
                'inicio' => 1505.32,
                'id_usuario' => 1786,
                'fecha' => '2019-09-20 04:40:00',
                'ejecutado' => 1,
            ),
            447 => 
            array (
                'id_alarmas' => 671,
                'id_cotizacion' => 10,
                'precio' => 1504.6,
                'inicio' => 1506.52,
                'id_usuario' => 1779,
                'fecha' => '2019-09-20 06:11:00',
                'ejecutado' => 1,
            ),
            448 => 
            array (
                'id_alarmas' => 672,
                'id_cotizacion' => 9,
                'precio' => 27194.5,
                'inicio' => 27157.84,
                'id_usuario' => 1848,
                'fecha' => '2019-09-20 10:15:00',
                'ejecutado' => 1,
            ),
            449 => 
            array (
                'id_alarmas' => 674,
                'id_cotizacion' => 9,
                'precio' => 27231.5,
                'inicio' => 27158.84,
                'id_usuario' => 1848,
                'fecha' => '2019-09-20 10:15:00',
                'ejecutado' => 1,
            ),
            450 => 
            array (
                'id_alarmas' => 676,
                'id_cotizacion' => 9,
                'precio' => 27141.76,
                'inicio' => 27147.84,
                'id_usuario' => 321,
                'fecha' => '2019-09-20 10:47:00',
                'ejecutado' => 1,
            ),
            451 => 
            array (
                'id_alarmas' => 677,
                'id_cotizacion' => 9,
                'precio' => 27200.0,
                'inicio' => 27141.34,
                'id_usuario' => 964,
                'fecha' => '2019-09-20 11:13:00',
                'ejecutado' => 1,
            ),
            452 => 
            array (
                'id_alarmas' => 678,
                'id_cotizacion' => 9,
                'precio' => 27141.84,
                'inicio' => 27138.84,
                'id_usuario' => 321,
                'fecha' => '2019-09-20 11:17:00',
                'ejecutado' => 1,
            ),
            453 => 
            array (
                'id_alarmas' => 679,
                'id_cotizacion' => 9,
                'precio' => 27123.84,
                'inicio' => 27170.34,
                'id_usuario' => 1691,
                'fecha' => '2019-09-20 14:22:00',
                'ejecutado' => 1,
            ),
            454 => 
            array (
                'id_alarmas' => 680,
                'id_cotizacion' => 5,
                'precio' => 217.03,
                'inicio' => 217.03,
                'id_usuario' => 1878,
                'fecha' => '2019-09-20 15:37:00',
                'ejecutado' => 0,
            ),
            455 => 
            array (
                'id_alarmas' => 683,
                'id_cotizacion' => 10,
                'precio' => 1506.62,
                'inicio' => 1512.2,
                'id_usuario' => 1779,
                'fecha' => '2019-09-20 18:30:00',
                'ejecutado' => 1,
            ),
            456 => 
            array (
                'id_alarmas' => 685,
                'id_cotizacion' => 10,
                'precio' => 1520.0,
                'inicio' => 1516.94,
                'id_usuario' => 1786,
                'fecha' => '2019-09-21 11:05:00',
                'ejecutado' => 1,
            ),
            457 => 
            array (
                'id_alarmas' => 686,
                'id_cotizacion' => 10,
                'precio' => 1518.0,
                'inicio' => 1516.94,
                'id_usuario' => 1786,
                'fecha' => '2019-09-21 11:06:00',
                'ejecutado' => 1,
            ),
            458 => 
            array (
                'id_alarmas' => 687,
                'id_cotizacion' => 3,
                'precio' => 9900.0,
                'inicio' => 10000.99,
                'id_usuario' => 1834,
                'fecha' => '2019-09-21 13:30:00',
                'ejecutado' => 1,
            ),
            459 => 
            array (
                'id_alarmas' => 688,
                'id_cotizacion' => 5,
                'precio' => 210.0,
                'inicio' => 215.84,
                'id_usuario' => 1834,
                'fecha' => '2019-09-21 13:34:00',
                'ejecutado' => 1,
            ),
            460 => 
            array (
                'id_alarmas' => 690,
                'id_cotizacion' => 5,
                'precio' => 212.9,
                'inicio' => 216.49,
                'id_usuario' => 445,
                'fecha' => '2019-09-21 14:12:00',
                'ejecutado' => 1,
            ),
            461 => 
            array (
                'id_alarmas' => 691,
                'id_cotizacion' => 2,
                'precio' => 0.285,
                'inicio' => 0.29072,
                'id_usuario' => 1575,
                'fecha' => '2019-09-21 21:16:00',
                'ejecutado' => 1,
            ),
            462 => 
            array (
                'id_alarmas' => 693,
                'id_cotizacion' => 2,
                'precio' => 0.28442,
                'inicio' => 0.28841,
                'id_usuario' => 445,
                'fecha' => '2019-09-22 00:36:00',
                'ejecutado' => 1,
            ),
            463 => 
            array (
                'id_alarmas' => 694,
                'id_cotizacion' => 2,
                'precio' => 0.28889,
                'inicio' => 0.28801,
                'id_usuario' => 1786,
                'fecha' => '2019-09-22 00:54:00',
                'ejecutado' => 1,
            ),
            464 => 
            array (
                'id_alarmas' => 695,
                'id_cotizacion' => 2,
                'precio' => 0.2907,
                'inicio' => 0.28797,
                'id_usuario' => 1786,
                'fecha' => '2019-09-22 00:54:00',
                'ejecutado' => 1,
            ),
            465 => 
            array (
                'id_alarmas' => 698,
                'id_cotizacion' => 7,
                'precio' => 199.252,
                'inicio' => 199.252,
                'id_usuario' => 1880,
                'fecha' => '2019-09-22 04:09:00',
                'ejecutado' => 0,
            ),
            466 => 
            array (
                'id_alarmas' => 699,
                'id_cotizacion' => 5,
                'precio' => 207.9,
                'inicio' => 212.09,
                'id_usuario' => 1691,
                'fecha' => '2019-09-22 12:05:00',
                'ejecutado' => 1,
            ),
            467 => 
            array (
                'id_alarmas' => 701,
                'id_cotizacion' => 5,
                'precio' => 206.8,
                'inicio' => 212.34,
                'id_usuario' => 445,
                'fecha' => '2019-09-22 13:35:00',
                'ejecutado' => 1,
            ),
            468 => 
            array (
                'id_alarmas' => 703,
                'id_cotizacion' => 5,
                'precio' => 209.6,
                'inicio' => 208.2,
                'id_usuario' => 445,
                'fecha' => '2019-09-22 19:02:00',
                'ejecutado' => 1,
            ),
            469 => 
            array (
                'id_alarmas' => 705,
                'id_cotizacion' => 6,
                'precio' => 107.751,
                'inicio' => 107.708,
                'id_usuario' => 1133,
                'fecha' => '2019-09-22 22:55:00',
                'ejecutado' => 1,
            ),
            470 => 
            array (
                'id_alarmas' => 706,
                'id_cotizacion' => 6,
                'precio' => 107.815,
                'inicio' => 107.705,
                'id_usuario' => 1133,
                'fecha' => '2019-09-22 22:55:00',
                'ejecutado' => 1,
            ),
            471 => 
            array (
                'id_alarmas' => 708,
                'id_cotizacion' => 5,
                'precio' => 208.0,
                'inicio' => 209.88,
                'id_usuario' => 1133,
                'fecha' => '2019-09-23 01:20:00',
                'ejecutado' => 1,
            ),
            472 => 
            array (
                'id_alarmas' => 709,
                'id_cotizacion' => 3,
                'precio' => 9979.9,
                'inicio' => 10013.01,
                'id_usuario' => 1133,
                'fecha' => '2019-09-23 01:34:00',
                'ejecutado' => 1,
            ),
            473 => 
            array (
                'id_alarmas' => 710,
                'id_cotizacion' => 8,
                'precio' => 184.257,
                'inicio' => 184.256,
                'id_usuario' => 390,
                'fecha' => '2019-09-23 03:17:00',
                'ejecutado' => 1,
            ),
            474 => 
            array (
                'id_alarmas' => 711,
                'id_cotizacion' => 6,
                'precio' => 107.5,
                'inicio' => 107.712,
                'id_usuario' => 1691,
                'fecha' => '2019-09-23 03:52:00',
                'ejecutado' => 1,
            ),
            475 => 
            array (
                'id_alarmas' => 712,
                'id_cotizacion' => 8,
                'precio' => 184.485,
                'inicio' => 184.241,
                'id_usuario' => 1581,
                'fecha' => '2019-09-23 04:14:00',
                'ejecutado' => 1,
            ),
            476 => 
            array (
                'id_alarmas' => 713,
                'id_cotizacion' => 2,
                'precio' => 0.27612,
                'inicio' => 0.27225,
                'id_usuario' => 445,
                'fecha' => '2019-09-23 06:03:00',
                'ejecutado' => 1,
            ),
            477 => 
            array (
                'id_alarmas' => 714,
                'id_cotizacion' => 2,
                'precio' => 0.26812,
                'inicio' => 0.27216,
                'id_usuario' => 445,
                'fecha' => '2019-09-23 06:04:00',
                'ejecutado' => 1,
            ),
            478 => 
            array (
                'id_alarmas' => 718,
                'id_cotizacion' => 8,
                'precio' => 1.841,
                'inicio' => 183.681,
                'id_usuario' => 1691,
                'fecha' => '2019-09-23 09:04:00',
                'ejecutado' => 1,
            ),
            479 => 
            array (
                'id_alarmas' => 719,
                'id_cotizacion' => 2,
                'precio' => 0.27702,
                'inicio' => 0.27602,
                'id_usuario' => 445,
                'fecha' => '2019-09-23 10:27:00',
                'ejecutado' => 1,
            ),
            480 => 
            array (
                'id_alarmas' => 720,
                'id_cotizacion' => 3,
                'precio' => 9891.0,
                'inicio' => 9949.64,
                'id_usuario' => 1236,
                'fecha' => '2019-09-23 10:48:00',
                'ejecutado' => 1,
            ),
            481 => 
            array (
                'id_alarmas' => 721,
                'id_cotizacion' => 3,
                'precio' => 9655.0,
                'inicio' => 9952.74,
                'id_usuario' => 1236,
                'fecha' => '2019-09-23 10:49:00',
                'ejecutado' => 1,
            ),
            482 => 
            array (
                'id_alarmas' => 722,
                'id_cotizacion' => 6,
                'precio' => 107.212,
                'inicio' => 107.459,
                'id_usuario' => 1688,
                'fecha' => '2019-09-23 11:41:00',
                'ejecutado' => 1,
            ),
            483 => 
            array (
                'id_alarmas' => 723,
                'id_cotizacion' => 2,
                'precio' => 0.26608,
                'inicio' => 0.27269,
                'id_usuario' => 1786,
                'fecha' => '2019-09-23 14:18:00',
                'ejecutado' => 1,
            ),
            484 => 
            array (
                'id_alarmas' => 724,
                'id_cotizacion' => 2,
                'precio' => 0.25311,
                'inicio' => 0.27266,
                'id_usuario' => 1786,
                'fecha' => '2019-09-23 14:19:00',
                'ejecutado' => 1,
            ),
            485 => 
            array (
                'id_alarmas' => 726,
                'id_cotizacion' => 9,
                'precio' => 27030.0,
                'inicio' => 26964.84,
                'id_usuario' => 1786,
                'fecha' => '2019-09-23 16:45:00',
                'ejecutado' => 1,
            ),
            486 => 
            array (
                'id_alarmas' => 727,
                'id_cotizacion' => 6,
                'precio' => 108.4,
                'inicio' => 107.497,
                'id_usuario' => 1786,
                'fecha' => '2019-09-23 16:56:00',
                'ejecutado' => 1,
            ),
            487 => 
            array (
                'id_alarmas' => 728,
                'id_cotizacion' => 6,
                'precio' => 107.15,
                'inicio' => 107.498,
                'id_usuario' => 1786,
                'fecha' => '2019-09-23 16:57:00',
                'ejecutado' => 1,
            ),
            488 => 
            array (
                'id_alarmas' => 736,
                'id_cotizacion' => 6,
                'precio' => 107.563,
                'inicio' => 107.516,
                'id_usuario' => 108,
                'fecha' => '2019-09-23 17:57:00',
                'ejecutado' => 1,
            ),
            489 => 
            array (
                'id_alarmas' => 737,
                'id_cotizacion' => 6,
                'precio' => 107.563,
                'inicio' => 107.519,
                'id_usuario' => 108,
                'fecha' => '2019-09-23 17:57:00',
                'ejecutado' => 1,
            ),
            490 => 
            array (
                'id_alarmas' => 738,
                'id_cotizacion' => 6,
                'precio' => 107.377,
                'inicio' => 107.537,
                'id_usuario' => 108,
                'fecha' => '2019-09-23 18:14:00',
                'ejecutado' => 1,
            ),
            491 => 
            array (
                'id_alarmas' => 739,
                'id_cotizacion' => 10,
                'precio' => 1525.64,
                'inicio' => 1524.28,
                'id_usuario' => 108,
                'fecha' => '2019-09-23 18:59:00',
                'ejecutado' => 1,
            ),
            492 => 
            array (
                'id_alarmas' => 740,
                'id_cotizacion' => 6,
                'precio' => 107.5,
                'inicio' => 107.565,
                'id_usuario' => 1362,
                'fecha' => '2019-09-23 21:46:00',
                'ejecutado' => 1,
            ),
            493 => 
            array (
                'id_alarmas' => 742,
                'id_cotizacion' => 7,
                'precio' => 19.363,
                'inicio' => 197.595,
                'id_usuario' => 1565,
                'fecha' => '2019-09-23 23:22:00',
                'ejecutado' => 1,
            ),
            494 => 
            array (
                'id_alarmas' => 743,
                'id_cotizacion' => 7,
                'precio' => 19.417,
                'inicio' => 197.596,
                'id_usuario' => 1565,
                'fecha' => '2019-09-23 23:22:00',
                'ejecutado' => 1,
            ),
            495 => 
            array (
                'id_alarmas' => 744,
                'id_cotizacion' => 2,
                'precio' => 0.26707,
                'inicio' => 0.26988,
                'id_usuario' => 1786,
                'fecha' => '2019-09-24 01:17:00',
                'ejecutado' => 1,
            ),
            496 => 
            array (
                'id_alarmas' => 745,
                'id_cotizacion' => 9,
                'precio' => 27044.0,
                'inicio' => 27065.74,
                'id_usuario' => 1787,
                'fecha' => '2019-09-24 02:35:00',
                'ejecutado' => 1,
            ),
            497 => 
            array (
                'id_alarmas' => 747,
                'id_cotizacion' => 3,
                'precio' => 9609.4,
                'inicio' => 9739.71,
                'id_usuario' => 1236,
                'fecha' => '2019-09-24 09:18:00',
                'ejecutado' => 1,
            ),
            498 => 
            array (
                'id_alarmas' => 748,
                'id_cotizacion' => 3,
                'precio' => 9505.9,
                'inicio' => 9739.71,
                'id_usuario' => 1236,
                'fecha' => '2019-09-24 09:19:00',
                'ejecutado' => 1,
            ),
            499 => 
            array (
                'id_alarmas' => 749,
                'id_cotizacion' => 6,
                'precio' => 107.65,
                'inicio' => 107.712,
                'id_usuario' => 1688,
                'fecha' => '2019-09-24 12:24:00',
                'ejecutado' => 1,
            ),
        ));
        \DB::table('alarmas')->insert(array (
            0 => 
            array (
                'id_alarmas' => 750,
                'id_cotizacion' => 6,
                'precio' => 107.65,
                'inicio' => 107.706,
                'id_usuario' => 1688,
                'fecha' => '2019-09-24 12:25:00',
                'ejecutado' => 1,
            ),
            1 => 
            array (
                'id_alarmas' => 751,
                'id_cotizacion' => 6,
                'precio' => 107.605,
                'inicio' => 107.639,
                'id_usuario' => 1688,
                'fecha' => '2019-09-24 12:48:00',
                'ejecutado' => 1,
            ),
            2 => 
            array (
                'id_alarmas' => 752,
                'id_cotizacion' => 6,
                'precio' => 107.605,
                'inicio' => 107.639,
                'id_usuario' => 1688,
                'fecha' => '2019-09-24 12:48:00',
                'ejecutado' => 1,
            ),
            3 => 
            array (
                'id_alarmas' => 753,
                'id_cotizacion' => 3,
                'precio' => 9475.0,
                'inicio' => 9650.31,
                'id_usuario' => 1236,
                'fecha' => '2019-09-24 13:56:00',
                'ejecutado' => 1,
            ),
            4 => 
            array (
                'id_alarmas' => 754,
                'id_cotizacion' => 11,
                'precio' => 165.548,
                'inicio' => 165.572,
                'id_usuario' => 1891,
                'fecha' => '2019-09-24 14:11:00',
                'ejecutado' => 1,
            ),
            5 => 
            array (
                'id_alarmas' => 755,
                'id_cotizacion' => 10,
                'precio' => 1521.58,
                'inicio' => 1521.98,
                'id_usuario' => 1891,
                'fecha' => '2019-09-24 14:12:00',
                'ejecutado' => 1,
            ),
            6 => 
            array (
                'id_alarmas' => 756,
                'id_cotizacion' => 9,
                'precio' => 27013.34,
                'inicio' => 27018.34,
                'id_usuario' => 1891,
                'fecha' => '2019-09-24 14:12:00',
                'ejecutado' => 1,
            ),
            7 => 
            array (
                'id_alarmas' => 757,
                'id_cotizacion' => 8,
                'precio' => 183.585,
                'inicio' => 183.635,
                'id_usuario' => 1891,
                'fecha' => '2019-09-24 14:12:00',
                'ejecutado' => 1,
            ),
            8 => 
            array (
                'id_alarmas' => 758,
                'id_cotizacion' => 7,
                'precio' => 197.331,
                'inicio' => 197.435,
                'id_usuario' => 1891,
                'fecha' => '2019-09-24 14:12:00',
                'ejecutado' => 1,
            ),
            9 => 
            array (
                'id_alarmas' => 759,
                'id_cotizacion' => 6,
                'precio' => 107.465,
                'inicio' => 107.449,
                'id_usuario' => 1891,
                'fecha' => '2019-09-24 14:12:00',
                'ejecutado' => 1,
            ),
            10 => 
            array (
                'id_alarmas' => 760,
                'id_cotizacion' => 5,
                'precio' => 194.11,
                'inicio' => 193.93,
                'id_usuario' => 1891,
                'fecha' => '2019-09-24 14:12:00',
                'ejecutado' => 1,
            ),
            11 => 
            array (
                'id_alarmas' => 761,
                'id_cotizacion' => 4,
                'precio' => 44.09,
                'inicio' => 44.04,
                'id_usuario' => 1891,
                'fecha' => '2019-09-24 14:12:00',
                'ejecutado' => 1,
            ),
            12 => 
            array (
                'id_alarmas' => 762,
                'id_cotizacion' => 3,
                'precio' => 9622.98,
                'inicio' => 9600.55,
                'id_usuario' => 1891,
                'fecha' => '2019-09-24 14:12:00',
                'ejecutado' => 1,
            ),
            13 => 
            array (
                'id_alarmas' => 763,
                'id_cotizacion' => 2,
                'precio' => 0.26512,
                'inicio' => 0.26463,
                'id_usuario' => 1891,
                'fecha' => '2019-09-24 14:12:00',
                'ejecutado' => 1,
            ),
            14 => 
            array (
                'id_alarmas' => 764,
                'id_cotizacion' => 11,
                'precio' => 165.384,
                'inicio' => 165.358,
                'id_usuario' => 1891,
                'fecha' => '2019-09-24 14:36:00',
                'ejecutado' => 1,
            ),
            15 => 
            array (
                'id_alarmas' => 765,
                'id_cotizacion' => 10,
                'precio' => 1523.48,
                'inicio' => 1523.59,
                'id_usuario' => 1891,
                'fecha' => '2019-09-24 14:36:00',
                'ejecutado' => 1,
            ),
            16 => 
            array (
                'id_alarmas' => 766,
                'id_cotizacion' => 9,
                'precio' => 27012.34,
                'inicio' => 27014.34,
                'id_usuario' => 1891,
                'fecha' => '2019-09-24 14:36:00',
                'ejecutado' => 1,
            ),
            17 => 
            array (
                'id_alarmas' => 767,
                'id_cotizacion' => 8,
                'precio' => 183.621,
                'inicio' => 183.558,
                'id_usuario' => 1891,
                'fecha' => '2019-09-24 14:36:00',
                'ejecutado' => 1,
            ),
            18 => 
            array (
                'id_alarmas' => 768,
                'id_cotizacion' => 7,
                'precio' => 197.333,
                'inicio' => 197.263,
                'id_usuario' => 1891,
                'fecha' => '2019-09-24 14:36:00',
                'ejecutado' => 1,
            ),
            19 => 
            array (
                'id_alarmas' => 769,
                'id_cotizacion' => 6,
                'precio' => 107.468,
                'inicio' => 107.464,
                'id_usuario' => 1891,
                'fecha' => '2019-09-24 14:36:00',
                'ejecutado' => 1,
            ),
            20 => 
            array (
                'id_alarmas' => 770,
                'id_cotizacion' => 5,
                'precio' => 192.88,
                'inicio' => 192.82,
                'id_usuario' => 1891,
                'fecha' => '2019-09-24 14:36:00',
                'ejecutado' => 1,
            ),
            21 => 
            array (
                'id_alarmas' => 771,
                'id_cotizacion' => 4,
                'precio' => 44.05,
                'inicio' => 44.04,
                'id_usuario' => 1891,
                'fecha' => '2019-09-24 14:37:00',
                'ejecutado' => 1,
            ),
            22 => 
            array (
                'id_alarmas' => 772,
                'id_cotizacion' => 3,
                'precio' => 9570.52,
                'inicio' => 9563.81,
                'id_usuario' => 1891,
                'fecha' => '2019-09-24 14:37:00',
                'ejecutado' => 1,
            ),
            23 => 
            array (
                'id_alarmas' => 773,
                'id_cotizacion' => 2,
                'precio' => 0.26129,
                'inicio' => 0.26173,
                'id_usuario' => 1891,
                'fecha' => '2019-09-24 14:37:00',
                'ejecutado' => 1,
            ),
            24 => 
            array (
                'id_alarmas' => 775,
                'id_cotizacion' => 10,
                'precio' => 1532.0,
                'inicio' => 1526.39,
                'id_usuario' => 1730,
                'fecha' => '2019-09-24 18:31:00',
                'ejecutado' => 1,
            ),
            25 => 
            array (
                'id_alarmas' => 777,
                'id_cotizacion' => 2,
                'precio' => 0.2625,
                'inicio' => 0.26351,
                'id_usuario' => 445,
                'fecha' => '2019-09-24 18:34:00',
                'ejecutado' => 1,
            ),
            26 => 
            array (
                'id_alarmas' => 778,
                'id_cotizacion' => 2,
                'precio' => 0.2555,
                'inicio' => 0.26351,
                'id_usuario' => 445,
                'fecha' => '2019-09-24 18:34:00',
                'ejecutado' => 1,
            ),
            27 => 
            array (
                'id_alarmas' => 781,
                'id_cotizacion' => 10,
                'precio' => 1525.0,
                'inicio' => 1529.38,
                'id_usuario' => 1912,
                'fecha' => '2019-09-25 03:32:00',
                'ejecutado' => 1,
            ),
            28 => 
            array (
                'id_alarmas' => 782,
                'id_cotizacion' => 10,
                'precio' => 1534.0,
                'inicio' => 1530.17,
                'id_usuario' => 1912,
                'fecha' => '2019-09-25 03:46:00',
                'ejecutado' => 0,
            ),
            29 => 
            array (
                'id_alarmas' => 784,
                'id_cotizacion' => 5,
                'precio' => 164.5,
                'inicio' => 169.74,
                'id_usuario' => 1691,
                'fecha' => '2019-09-25 04:47:00',
                'ejecutado' => 1,
            ),
            30 => 
            array (
                'id_alarmas' => 787,
                'id_cotizacion' => 4,
                'precio' => 35.28,
                'inicio' => 36.25,
                'id_usuario' => 1752,
                'fecha' => '2019-09-25 05:08:00',
                'ejecutado' => 1,
            ),
            31 => 
            array (
                'id_alarmas' => 789,
                'id_cotizacion' => 5,
                'precio' => 167.96,
                'inicio' => 168.7,
                'id_usuario' => 1752,
                'fecha' => '2019-09-25 05:09:00',
                'ejecutado' => 1,
            ),
            32 => 
            array (
                'id_alarmas' => 790,
                'id_cotizacion' => 5,
                'precio' => 167.5,
                'inicio' => 169.32,
                'id_usuario' => 1786,
                'fecha' => '2019-09-25 05:22:00',
                'ejecutado' => 1,
            ),
            33 => 
            array (
                'id_alarmas' => 791,
                'id_cotizacion' => 7,
                'precio' => 197.269,
                'inicio' => 197.018,
                'id_usuario' => 1133,
                'fecha' => '2019-09-25 13:24:00',
                'ejecutado' => 1,
            ),
            34 => 
            array (
                'id_alarmas' => 794,
                'id_cotizacion' => 9,
                'precio' => 26995.5,
                'inicio' => 26978.34,
                'id_usuario' => 1787,
                'fecha' => '2019-09-25 18:40:00',
                'ejecutado' => 1,
            ),
            35 => 
            array (
                'id_alarmas' => 796,
                'id_cotizacion' => 9,
                'precio' => 27.0,
                'inicio' => 26978.84,
                'id_usuario' => 1627,
                'fecha' => '2019-09-25 18:59:00',
                'ejecutado' => 0,
            ),
            36 => 
            array (
                'id_alarmas' => 797,
                'id_cotizacion' => 9,
                'precio' => 26963.0,
                'inicio' => 26979.84,
                'id_usuario' => 1787,
                'fecha' => '2019-09-25 19:17:00',
                'ejecutado' => 1,
            ),
            37 => 
            array (
                'id_alarmas' => 798,
                'id_cotizacion' => 9,
                'precio' => 26983.0,
                'inicio' => 26979.24,
                'id_usuario' => 1787,
                'fecha' => '2019-09-25 20:20:00',
                'ejecutado' => 1,
            ),
            38 => 
            array (
                'id_alarmas' => 799,
                'id_cotizacion' => 9,
                'precio' => 26980.0,
                'inicio' => 26979.24,
                'id_usuario' => 1787,
                'fecha' => '2019-09-25 20:20:00',
                'ejecutado' => 1,
            ),
            39 => 
            array (
                'id_alarmas' => 801,
                'id_cotizacion' => 9,
                'precio' => 26996.0,
                'inicio' => 26984.24,
                'id_usuario' => 1787,
                'fecha' => '2019-09-25 20:43:00',
                'ejecutado' => 1,
            ),
            40 => 
            array (
                'id_alarmas' => 802,
                'id_cotizacion' => 9,
                'precio' => 26997.0,
                'inicio' => 26995.24,
                'id_usuario' => 1787,
                'fecha' => '2019-09-25 22:09:00',
                'ejecutado' => 1,
            ),
            41 => 
            array (
                'id_alarmas' => 803,
                'id_cotizacion' => 8,
                'precio' => 1.842,
                'inicio' => 182.948,
                'id_usuario' => 1786,
                'fecha' => '2019-09-25 23:39:00',
                'ejecutado' => 1,
            ),
            42 => 
            array (
                'id_alarmas' => 806,
                'id_cotizacion' => 10,
                'precio' => 1503.0,
                'inicio' => 1508.11,
                'id_usuario' => 1730,
                'fecha' => '2019-09-26 00:58:00',
                'ejecutado' => 1,
            ),
            43 => 
            array (
                'id_alarmas' => 807,
                'id_cotizacion' => 6,
                'precio' => 107.884,
                'inicio' => 107.715,
                'id_usuario' => 1133,
                'fecha' => '2019-09-26 01:03:00',
                'ejecutado' => 1,
            ),
            44 => 
            array (
                'id_alarmas' => 808,
                'id_cotizacion' => 6,
                'precio' => 108.11,
                'inicio' => 107.716,
                'id_usuario' => 1133,
                'fecha' => '2019-09-26 01:03:00',
                'ejecutado' => 1,
            ),
            45 => 
            array (
                'id_alarmas' => 809,
                'id_cotizacion' => 9,
                'precio' => 26949.34,
                'inicio' => 26965.74,
                'id_usuario' => 321,
                'fecha' => '2019-09-26 01:22:00',
                'ejecutado' => 1,
            ),
            46 => 
            array (
                'id_alarmas' => 811,
                'id_cotizacion' => 3,
                'precio' => 8250.75,
                'inicio' => 8416.55,
                'id_usuario' => 100,
                'fecha' => '2019-09-26 03:47:00',
                'ejecutado' => 1,
            ),
            47 => 
            array (
                'id_alarmas' => 812,
                'id_cotizacion' => 7,
                'precio' => 195.814,
                'inicio' => 196.203,
                'id_usuario' => 1108,
                'fecha' => '2019-09-26 04:14:00',
                'ejecutado' => 1,
            ),
            48 => 
            array (
                'id_alarmas' => 813,
                'id_cotizacion' => 7,
                'precio' => 195.814,
                'inicio' => 196.203,
                'id_usuario' => 1108,
                'fecha' => '2019-09-26 04:14:00',
                'ejecutado' => 1,
            ),
            49 => 
            array (
                'id_alarmas' => 814,
                'id_cotizacion' => 7,
                'precio' => 195.814,
                'inicio' => 19.621,
                'id_usuario' => 1108,
                'fecha' => '2019-09-26 04:14:00',
                'ejecutado' => 1,
            ),
            50 => 
            array (
                'id_alarmas' => 815,
                'id_cotizacion' => 3,
                'precio' => 8550.4,
                'inicio' => 8393.53,
                'id_usuario' => 486,
                'fecha' => '2019-09-26 04:34:00',
                'ejecutado' => 1,
            ),
            51 => 
            array (
                'id_alarmas' => 817,
                'id_cotizacion' => 7,
                'precio' => 1.96,
                'inicio' => 196.187,
                'id_usuario' => 1709,
                'fecha' => '2019-09-26 04:34:00',
                'ejecutado' => 1,
            ),
            52 => 
            array (
                'id_alarmas' => 818,
                'id_cotizacion' => 9,
                'precio' => 26938.74,
                'inicio' => 26946.74,
                'id_usuario' => 1409,
                'fecha' => '2019-09-26 04:41:00',
                'ejecutado' => 1,
            ),
            53 => 
            array (
                'id_alarmas' => 819,
                'id_cotizacion' => 10,
                'precio' => 1509.55,
                'inicio' => 1509.55,
                'id_usuario' => 1707,
                'fecha' => '2019-09-26 08:10:00',
                'ejecutado' => 0,
            ),
            54 => 
            array (
                'id_alarmas' => 821,
                'id_cotizacion' => 3,
                'precio' => 8367.98,
                'inicio' => 8459.5,
                'id_usuario' => 1108,
                'fecha' => '2019-09-26 13:29:00',
                'ejecutado' => 1,
            ),
            55 => 
            array (
                'id_alarmas' => 822,
                'id_cotizacion' => 3,
                'precio' => 8367.98,
                'inicio' => 8459.5,
                'id_usuario' => 1108,
                'fecha' => '2019-09-26 13:29:00',
                'ejecutado' => 1,
            ),
            56 => 
            array (
                'id_alarmas' => 823,
                'id_cotizacion' => 3,
                'precio' => 8367.98,
                'inicio' => 8459.5,
                'id_usuario' => 1108,
                'fecha' => '2019-09-26 13:29:00',
                'ejecutado' => 1,
            ),
            57 => 
            array (
                'id_alarmas' => 824,
                'id_cotizacion' => 3,
                'precio' => 8367.98,
                'inicio' => 8459.5,
                'id_usuario' => 1108,
                'fecha' => '2019-09-26 13:29:00',
                'ejecutado' => 1,
            ),
            58 => 
            array (
                'id_alarmas' => 825,
                'id_cotizacion' => 6,
                'precio' => 107.82,
                'inicio' => 107.795,
                'id_usuario' => 1786,
                'fecha' => '2019-09-26 16:43:00',
                'ejecutado' => 1,
            ),
            59 => 
            array (
                'id_alarmas' => 826,
                'id_cotizacion' => 7,
                'precio' => 19.585,
                'inicio' => 195.651,
                'id_usuario' => 840,
                'fecha' => '2019-09-26 19:44:00',
                'ejecutado' => 1,
            ),
            60 => 
            array (
                'id_alarmas' => 827,
                'id_cotizacion' => 9,
                'precio' => 26932.0,
                'inicio' => 26927.74,
                'id_usuario' => 1236,
                'fecha' => '2019-09-26 22:42:00',
                'ejecutado' => 1,
            ),
            61 => 
            array (
                'id_alarmas' => 828,
                'id_cotizacion' => 10,
                'precio' => 1513.0,
                'inicio' => 1505.75,
                'id_usuario' => 1688,
                'fecha' => '2019-09-26 23:29:00',
                'ejecutado' => 1,
            ),
            62 => 
            array (
                'id_alarmas' => 829,
                'id_cotizacion' => 10,
                'precio' => 1513.0,
                'inicio' => 1505.75,
                'id_usuario' => 1688,
                'fecha' => '2019-09-26 23:29:00',
                'ejecutado' => 1,
            ),
            63 => 
            array (
                'id_alarmas' => 830,
                'id_cotizacion' => 10,
                'precio' => 1513.0,
                'inicio' => 1505.75,
                'id_usuario' => 1688,
                'fecha' => '2019-09-26 23:29:00',
                'ejecutado' => 1,
            ),
            64 => 
            array (
                'id_alarmas' => 831,
                'id_cotizacion' => 7,
                'precio' => 19.655,
                'inicio' => 19.586,
                'id_usuario' => 1688,
                'fecha' => '2019-09-26 23:30:00',
                'ejecutado' => 1,
            ),
            65 => 
            array (
                'id_alarmas' => 832,
                'id_cotizacion' => 7,
                'precio' => 19.655,
                'inicio' => 19.586,
                'id_usuario' => 1688,
                'fecha' => '2019-09-26 23:30:00',
                'ejecutado' => 1,
            ),
            66 => 
            array (
                'id_alarmas' => 833,
                'id_cotizacion' => 3,
                'precio' => 8000.0,
                'inicio' => 8137.95,
                'id_usuario' => 1236,
                'fecha' => '2019-09-27 00:36:00',
                'ejecutado' => 1,
            ),
            67 => 
            array (
                'id_alarmas' => 834,
                'id_cotizacion' => 9,
                'precio' => 26825.0,
                'inicio' => 26878.74,
                'id_usuario' => 1236,
                'fecha' => '2019-09-27 00:54:00',
                'ejecutado' => 1,
            ),
            68 => 
            array (
                'id_alarmas' => 835,
                'id_cotizacion' => 9,
                'precio' => 26775.6,
                'inicio' => 26879.24,
                'id_usuario' => 1236,
                'fecha' => '2019-09-27 00:56:00',
                'ejecutado' => 1,
            ),
            69 => 
            array (
                'id_alarmas' => 838,
                'id_cotizacion' => 10,
                'precio' => 1493.5,
                'inicio' => 1497.64,
                'id_usuario' => 1786,
                'fecha' => '2019-09-27 09:23:00',
                'ejecutado' => 1,
            ),
            70 => 
            array (
                'id_alarmas' => 843,
                'id_cotizacion' => 3,
                'precio' => 8800.0,
                'inicio' => 8063.87,
                'id_usuario' => 644,
                'fecha' => '2019-09-27 19:36:00',
                'ejecutado' => 1,
            ),
            71 => 
            array (
                'id_alarmas' => 844,
                'id_cotizacion' => 3,
                'precio' => 8600.0,
                'inicio' => 8064.57,
                'id_usuario' => 644,
                'fecha' => '2019-09-27 19:36:00',
                'ejecutado' => 1,
            ),
            72 => 
            array (
                'id_alarmas' => 845,
                'id_cotizacion' => 2,
                'precio' => 0.2437,
                'inicio' => 0.24358,
                'id_usuario' => 1881,
                'fecha' => '2019-09-28 00:12:00',
                'ejecutado' => 1,
            ),
            73 => 
            array (
                'id_alarmas' => 846,
                'id_cotizacion' => 3,
                'precio' => 8270.0,
                'inicio' => 8167.38,
                'id_usuario' => 1749,
                'fecha' => '2019-09-28 17:37:00',
                'ejecutado' => 1,
            ),
            74 => 
            array (
                'id_alarmas' => 847,
                'id_cotizacion' => 2,
                'precio' => 0.2439,
                'inicio' => 0.24318,
                'id_usuario' => 1786,
                'fecha' => '2019-09-28 23:56:00',
                'ejecutado' => 1,
            ),
            75 => 
            array (
                'id_alarmas' => 848,
                'id_cotizacion' => 2,
                'precio' => 0.2469,
                'inicio' => 0.24336,
                'id_usuario' => 1786,
                'fecha' => '2019-09-28 23:56:00',
                'ejecutado' => 1,
            ),
            76 => 
            array (
                'id_alarmas' => 849,
                'id_cotizacion' => 2,
                'precio' => 0.2416,
                'inicio' => 0.24314,
                'id_usuario' => 1786,
                'fecha' => '2019-09-28 23:56:00',
                'ejecutado' => 1,
            ),
            77 => 
            array (
                'id_alarmas' => 850,
                'id_cotizacion' => 2,
                'precio' => 0.2412,
                'inicio' => 0.24313,
                'id_usuario' => 1786,
                'fecha' => '2019-09-28 23:56:00',
                'ejecutado' => 1,
            ),
            78 => 
            array (
                'id_alarmas' => 852,
                'id_cotizacion' => 2,
                'precio' => 0.24015,
                'inicio' => 0.2401,
                'id_usuario' => 1937,
                'fecha' => '2019-09-29 05:11:00',
                'ejecutado' => 1,
            ),
            79 => 
            array (
                'id_alarmas' => 853,
                'id_cotizacion' => 9,
                'precio' => 26821.24,
                'inicio' => 26821.24,
                'id_usuario' => 1937,
                'fecha' => '2019-09-29 05:11:00',
                'ejecutado' => 0,
            ),
            80 => 
            array (
                'id_alarmas' => 855,
                'id_cotizacion' => 8,
                'precio' => 1.816,
                'inicio' => 181.775,
                'id_usuario' => 433,
                'fecha' => '2019-09-30 00:32:00',
                'ejecutado' => 1,
            ),
            81 => 
            array (
                'id_alarmas' => 856,
                'id_cotizacion' => 2,
                'precio' => 0.24036,
                'inicio' => 0.24032,
                'id_usuario' => 1931,
                'fecha' => '2019-09-30 00:35:00',
                'ejecutado' => 1,
            ),
            82 => 
            array (
                'id_alarmas' => 857,
                'id_cotizacion' => 10,
                'precio' => 1496.69,
                'inicio' => 1496.62,
                'id_usuario' => 1931,
                'fecha' => '2019-09-30 00:35:00',
                'ejecutado' => 1,
            ),
            83 => 
            array (
                'id_alarmas' => 858,
                'id_cotizacion' => 5,
                'precio' => 170.5,
                'inicio' => 170.01,
                'id_usuario' => 1691,
                'fecha' => '2019-09-30 00:37:00',
                'ejecutado' => 1,
            ),
            84 => 
            array (
                'id_alarmas' => 859,
                'id_cotizacion' => 5,
                'precio' => 170.5,
                'inicio' => 170.01,
                'id_usuario' => 1691,
                'fecha' => '2019-09-30 00:37:00',
                'ejecutado' => 1,
            ),
            85 => 
            array (
                'id_alarmas' => 860,
                'id_cotizacion' => 6,
                'precio' => 108.179,
                'inicio' => 107.917,
                'id_usuario' => 1133,
                'fecha' => '2019-09-30 02:39:00',
                'ejecutado' => 1,
            ),
            86 => 
            array (
                'id_alarmas' => 861,
                'id_cotizacion' => 7,
                'precio' => 1.965,
                'inicio' => 196.227,
                'id_usuario' => 1575,
                'fecha' => '2019-09-30 03:10:00',
                'ejecutado' => 1,
            ),
            87 => 
            array (
                'id_alarmas' => 863,
                'id_cotizacion' => 10,
                'precio' => 1487.0,
                'inicio' => 1492.3,
                'id_usuario' => 1786,
                'fecha' => '2019-09-30 03:22:00',
                'ejecutado' => 1,
            ),
            88 => 
            array (
                'id_alarmas' => 864,
                'id_cotizacion' => 10,
                'precio' => 1487.0,
                'inicio' => 1492.71,
                'id_usuario' => 1912,
                'fecha' => '2019-09-30 03:35:00',
                'ejecutado' => 1,
            ),
            89 => 
            array (
                'id_alarmas' => 866,
                'id_cotizacion' => 7,
                'precio' => 1.97,
                'inicio' => 196.425,
                'id_usuario' => 1691,
                'fecha' => '2019-09-30 05:35:00',
                'ejecutado' => 1,
            ),
            90 => 
            array (
                'id_alarmas' => 867,
                'id_cotizacion' => 5,
                'precio' => 170.41,
                'inicio' => 167.32,
                'id_usuario' => 1691,
                'fecha' => '2019-09-30 05:38:00',
                'ejecutado' => 1,
            ),
            91 => 
            array (
                'id_alarmas' => 868,
                'id_cotizacion' => 3,
                'precio' => 9000.0,
                'inicio' => 8280.5,
                'id_usuario' => 1003,
                'fecha' => '2019-09-30 22:05:00',
                'ejecutado' => 1,
            ),
            92 => 
            array (
                'id_alarmas' => 869,
                'id_cotizacion' => 9,
                'precio' => 27350.0,
                'inicio' => 26997.24,
                'id_usuario' => 1003,
                'fecha' => '2019-09-30 22:06:00',
                'ejecutado' => 1,
            ),
            93 => 
            array (
                'id_alarmas' => 870,
                'id_cotizacion' => 2,
                'precio' => 0.25754,
                'inicio' => 0.25734,
                'id_usuario' => 262,
                'fecha' => '2019-09-30 22:06:00',
                'ejecutado' => 1,
            ),
            94 => 
            array (
                'id_alarmas' => 871,
                'id_cotizacion' => 7,
                'precio' => 196.477,
                'inicio' => 196.435,
                'id_usuario' => 262,
                'fecha' => '2019-09-30 22:07:00',
                'ejecutado' => 1,
            ),
            95 => 
            array (
                'id_alarmas' => 873,
                'id_cotizacion' => 7,
                'precio' => 19.625,
                'inicio' => 1.964,
                'id_usuario' => 1786,
                'fecha' => '2019-09-30 22:13:00',
                'ejecutado' => 1,
            ),
            96 => 
            array (
                'id_alarmas' => 877,
                'id_cotizacion' => 3,
                'precio' => 7950.0,
                'inicio' => 8315.94,
                'id_usuario' => 1778,
                'fecha' => '2019-09-30 23:38:00',
                'ejecutado' => 1,
            ),
            97 => 
            array (
                'id_alarmas' => 878,
                'id_cotizacion' => 3,
                'precio' => 7900.0,
                'inicio' => 8315.94,
                'id_usuario' => 1778,
                'fecha' => '2019-09-30 23:38:00',
                'ejecutado' => 1,
            ),
            98 => 
            array (
                'id_alarmas' => 879,
                'id_cotizacion' => 3,
                'precio' => 7850.0,
                'inicio' => 8315.94,
                'id_usuario' => 1778,
                'fecha' => '2019-09-30 23:38:00',
                'ejecutado' => 1,
            ),
            99 => 
            array (
                'id_alarmas' => 881,
                'id_cotizacion' => 5,
                'precio' => 185.0,
                'inicio' => 179.8,
                'id_usuario' => 1778,
                'fecha' => '2019-09-30 23:39:00',
                'ejecutado' => 1,
            ),
            100 => 
            array (
                'id_alarmas' => 882,
                'id_cotizacion' => 2,
                'precio' => 0.265,
                'inicio' => 0.25707,
                'id_usuario' => 1778,
                'fecha' => '2019-09-30 23:39:00',
                'ejecutado' => 1,
            ),
            101 => 
            array (
                'id_alarmas' => 883,
                'id_cotizacion' => 2,
                'precio' => 0.25754,
                'inicio' => 0.25578,
                'id_usuario' => 1935,
                'fecha' => '2019-10-01 01:17:00',
                'ejecutado' => 1,
            ),
            102 => 
            array (
                'id_alarmas' => 886,
                'id_cotizacion' => 5,
                'precio' => 181.42,
                'inicio' => 181.18,
                'id_usuario' => 1935,
                'fecha' => '2019-10-01 11:44:00',
                'ejecutado' => 1,
            ),
            103 => 
            array (
                'id_alarmas' => 887,
                'id_cotizacion' => 9,
                'precio' => 26616.24,
                'inicio' => 26617.24,
                'id_usuario' => 1301,
                'fecha' => '2019-10-02 00:51:00',
                'ejecutado' => 1,
            ),
            104 => 
            array (
                'id_alarmas' => 888,
                'id_cotizacion' => 10,
                'precio' => 1478.55,
                'inicio' => 1478.21,
                'id_usuario' => 1935,
                'fecha' => '2019-10-02 02:04:00',
                'ejecutado' => 1,
            ),
            105 => 
            array (
                'id_alarmas' => 889,
                'id_cotizacion' => 10,
                'precio' => 1478.0,
                'inicio' => 1477.49,
                'id_usuario' => 1131,
                'fecha' => '2019-10-02 02:36:00',
                'ejecutado' => 1,
            ),
            106 => 
            array (
                'id_alarmas' => 891,
                'id_cotizacion' => 12,
                'precio' => 1.33,
                'inicio' => 133.253,
                'id_usuario' => 1934,
                'fecha' => '2019-10-03 18:54:00',
                'ejecutado' => 1,
            ),
            107 => 
            array (
                'id_alarmas' => 892,
                'id_cotizacion' => 10,
                'precio' => 1505.5,
                'inicio' => 1508.2,
                'id_usuario' => 1777,
                'fecha' => '2019-10-04 01:56:00',
                'ejecutado' => 1,
            ),
            108 => 
            array (
                'id_alarmas' => 893,
                'id_cotizacion' => 12,
                'precio' => 133.316,
                'inicio' => 13.328,
                'id_usuario' => 312,
                'fecha' => '2019-10-04 02:52:00',
                'ejecutado' => 1,
            ),
            109 => 
            array (
                'id_alarmas' => 897,
                'id_cotizacion' => 7,
                'precio' => 194.887,
                'inicio' => 194.907,
                'id_usuario' => 1777,
                'fecha' => '2019-10-04 16:05:00',
                'ejecutado' => 1,
            ),
            110 => 
            array (
                'id_alarmas' => 898,
                'id_cotizacion' => 7,
                'precio' => 194.959,
                'inicio' => 194.871,
                'id_usuario' => 1777,
                'fecha' => '2019-10-04 16:05:00',
                'ejecutado' => 1,
            ),
            111 => 
            array (
                'id_alarmas' => 899,
                'id_cotizacion' => 3,
                'precio' => 8261.5,
                'inicio' => 8225.44,
                'id_usuario' => 1236,
                'fecha' => '2019-10-04 18:38:00',
                'ejecutado' => 1,
            ),
            112 => 
            array (
                'id_alarmas' => 901,
                'id_cotizacion' => 3,
                'precio' => 8143.5,
                'inicio' => 8129.94,
                'id_usuario' => 1236,
                'fecha' => '2019-10-05 09:30:00',
                'ejecutado' => 1,
            ),
            113 => 
            array (
                'id_alarmas' => 902,
                'id_cotizacion' => 5,
                'precio' => 173.0,
                'inicio' => 175.58,
                'id_usuario' => 1786,
                'fecha' => '2019-10-05 11:27:00',
                'ejecutado' => 1,
            ),
            114 => 
            array (
                'id_alarmas' => 903,
                'id_cotizacion' => 5,
                'precio' => 172.0,
                'inicio' => 175.48,
                'id_usuario' => 1786,
                'fecha' => '2019-10-05 11:28:00',
                'ejecutado' => 1,
            ),
            115 => 
            array (
                'id_alarmas' => 904,
                'id_cotizacion' => 3,
                'precio' => 8169.2,
                'inicio' => 8129.94,
                'id_usuario' => 1236,
                'fecha' => '2019-10-05 21:01:00',
                'ejecutado' => 1,
            ),
            116 => 
            array (
                'id_alarmas' => 909,
                'id_cotizacion' => 12,
                'precio' => 13.301,
                'inicio' => 133.072,
                'id_usuario' => 969,
                'fecha' => '2019-10-06 23:13:00',
                'ejecutado' => 1,
            ),
            117 => 
            array (
                'id_alarmas' => 910,
                'id_cotizacion' => 17,
                'precio' => 10.995,
                'inicio' => 109.827,
                'id_usuario' => 1970,
                'fecha' => '2019-10-07 03:24:00',
                'ejecutado' => 1,
            ),
            118 => 
            array (
                'id_alarmas' => 911,
                'id_cotizacion' => 17,
                'precio' => 10.992,
                'inicio' => 109.831,
                'id_usuario' => 1970,
                'fecha' => '2019-10-07 03:24:00',
                'ejecutado' => 1,
            ),
            119 => 
            array (
                'id_alarmas' => 912,
                'id_cotizacion' => 10,
                'precio' => 1504.8,
                'inicio' => 1507.43,
                'id_usuario' => 462,
                'fecha' => '2019-10-07 03:26:00',
                'ejecutado' => 1,
            ),
            120 => 
            array (
                'id_alarmas' => 913,
                'id_cotizacion' => 7,
                'precio' => 194.965,
                'inicio' => 19.501,
                'id_usuario' => 1777,
                'fecha' => '2019-10-07 06:05:00',
                'ejecutado' => 1,
            ),
            121 => 
            array (
                'id_alarmas' => 914,
                'id_cotizacion' => 10,
                'precio' => 1498.5,
                'inicio' => 1502.06,
                'id_usuario' => 1691,
                'fecha' => '2019-10-07 08:20:00',
                'ejecutado' => 1,
            ),
            122 => 
            array (
                'id_alarmas' => 915,
                'id_cotizacion' => 9,
                'precio' => 26486.84,
                'inicio' => 26448.34,
                'id_usuario' => 1691,
                'fecha' => '2019-10-07 08:21:00',
                'ejecutado' => 1,
            ),
            123 => 
            array (
                'id_alarmas' => 917,
                'id_cotizacion' => 8,
                'precio' => 1.83,
                'inicio' => 182.628,
                'id_usuario' => 1691,
                'fecha' => '2019-10-07 09:14:00',
                'ejecutado' => 1,
            ),
            124 => 
            array (
                'id_alarmas' => 918,
                'id_cotizacion' => 17,
                'precio' => 110.126,
                'inicio' => 109.702,
                'id_usuario' => 1640,
                'fecha' => '2019-10-07 09:27:00',
                'ejecutado' => 1,
            ),
            125 => 
            array (
                'id_alarmas' => 920,
                'id_cotizacion' => 9,
                'precio' => 26476.34,
                'inicio' => 26499.84,
                'id_usuario' => 1691,
                'fecha' => '2019-10-07 10:54:00',
                'ejecutado' => 1,
            ),
            126 => 
            array (
                'id_alarmas' => 924,
                'id_cotizacion' => 7,
                'precio' => 195.258,
                'inicio' => 195.437,
                'id_usuario' => 1882,
                'fecha' => '2019-10-07 18:11:00',
                'ejecutado' => 1,
            ),
            127 => 
            array (
                'id_alarmas' => 925,
                'id_cotizacion' => 7,
                'precio' => 19.517,
                'inicio' => 195.479,
                'id_usuario' => 1882,
                'fecha' => '2019-10-07 18:11:00',
                'ejecutado' => 1,
            ),
            128 => 
            array (
                'id_alarmas' => 926,
                'id_cotizacion' => 7,
                'precio' => 195.229,
                'inicio' => 195.533,
                'id_usuario' => 1968,
                'fecha' => '2019-10-07 20:03:00',
                'ejecutado' => 1,
            ),
            129 => 
            array (
                'id_alarmas' => 927,
                'id_cotizacion' => 10,
                'precio' => 1490.73,
                'inicio' => 1492.1,
                'id_usuario' => 1730,
                'fecha' => '2019-10-07 20:25:00',
                'ejecutado' => 1,
            ),
            130 => 
            array (
                'id_alarmas' => 930,
                'id_cotizacion' => 7,
                'precio' => 195.408,
                'inicio' => 195.565,
                'id_usuario' => 1777,
                'fecha' => '2019-10-07 21:19:00',
                'ejecutado' => 1,
            ),
            131 => 
            array (
                'id_alarmas' => 933,
                'id_cotizacion' => 7,
                'precio' => 195.015,
                'inicio' => 195.412,
                'id_usuario' => 1882,
                'fecha' => '2019-10-07 21:58:00',
                'ejecutado' => 1,
            ),
            132 => 
            array (
                'id_alarmas' => 935,
                'id_cotizacion' => 9,
                'precio' => 26500.0,
                'inicio' => 26465.74,
                'id_usuario' => 380,
                'fecha' => '2019-10-07 22:09:00',
                'ejecutado' => 1,
            ),
            133 => 
            array (
                'id_alarmas' => 936,
                'id_cotizacion' => 9,
                'precio' => 26480.0,
                'inicio' => 26471.24,
                'id_usuario' => 380,
                'fecha' => '2019-10-07 22:12:00',
                'ejecutado' => 1,
            ),
            134 => 
            array (
                'id_alarmas' => 937,
                'id_cotizacion' => 10,
                'precio' => 1495.97,
                'inicio' => 1494.33,
                'id_usuario' => 1934,
                'fecha' => '2019-10-07 22:12:00',
                'ejecutado' => 1,
            ),
            135 => 
            array (
                'id_alarmas' => 938,
                'id_cotizacion' => 10,
                'precio' => 1495.97,
                'inicio' => 1494.33,
                'id_usuario' => 1934,
                'fecha' => '2019-10-07 22:12:00',
                'ejecutado' => 1,
            ),
            136 => 
            array (
                'id_alarmas' => 939,
                'id_cotizacion' => 10,
                'precio' => 1500.0,
                'inicio' => 1493.1,
                'id_usuario' => 1730,
                'fecha' => '2019-10-07 22:56:00',
                'ejecutado' => 1,
            ),
            137 => 
            array (
                'id_alarmas' => 940,
                'id_cotizacion' => 7,
                'precio' => 19.528,
                'inicio' => 195.339,
                'id_usuario' => 1882,
                'fecha' => '2019-10-07 23:55:00',
                'ejecutado' => 1,
            ),
            138 => 
            array (
                'id_alarmas' => 941,
                'id_cotizacion' => 9,
                'precio' => 26522.0,
                'inicio' => 26498.24,
                'id_usuario' => 380,
                'fecha' => '2019-10-08 00:19:00',
                'ejecutado' => 1,
            ),
            139 => 
            array (
                'id_alarmas' => 942,
                'id_cotizacion' => 9,
                'precio' => 26520.0,
                'inicio' => 26491.74,
                'id_usuario' => 1876,
                'fecha' => '2019-10-08 00:27:00',
                'ejecutado' => 1,
            ),
            140 => 
            array (
                'id_alarmas' => 943,
                'id_cotizacion' => 9,
                'precio' => 26550.0,
                'inicio' => 26515.24,
                'id_usuario' => 1876,
                'fecha' => '2019-10-08 00:45:00',
                'ejecutado' => 1,
            ),
            141 => 
            array (
                'id_alarmas' => 944,
                'id_cotizacion' => 6,
                'precio' => 107.208,
                'inicio' => 107.316,
                'id_usuario' => 969,
                'fecha' => '2019-10-08 01:23:00',
                'ejecutado' => 1,
            ),
            142 => 
            array (
                'id_alarmas' => 947,
                'id_cotizacion' => 9,
                'precio' => 26445.0,
                'inicio' => 26511.24,
                'id_usuario' => 380,
                'fecha' => '2019-10-08 01:29:00',
                'ejecutado' => 1,
            ),
            143 => 
            array (
                'id_alarmas' => 948,
                'id_cotizacion' => 9,
                'precio' => 26590.0,
                'inicio' => 26544.24,
                'id_usuario' => 1876,
                'fecha' => '2019-10-08 02:01:00',
                'ejecutado' => 1,
            ),
            144 => 
            array (
                'id_alarmas' => 949,
                'id_cotizacion' => 9,
                'precio' => 26549.17,
                'inicio' => 26578.74,
                'id_usuario' => 1778,
                'fecha' => '2019-10-08 03:16:00',
                'ejecutado' => 1,
            ),
            145 => 
            array (
                'id_alarmas' => 950,
                'id_cotizacion' => 9,
                'precio' => 26531.17,
                'inicio' => 26578.24,
                'id_usuario' => 1778,
                'fecha' => '2019-10-08 03:17:00',
                'ejecutado' => 1,
            ),
            146 => 
            array (
                'id_alarmas' => 952,
                'id_cotizacion' => 9,
                'precio' => 26580.34,
                'inicio' => 26541.34,
                'id_usuario' => 1691,
                'fecha' => '2019-10-08 07:13:00',
                'ejecutado' => 1,
            ),
            147 => 
            array (
                'id_alarmas' => 953,
                'id_cotizacion' => 9,
                'precio' => 26531.17,
                'inicio' => 26539.34,
                'id_usuario' => 1778,
                'fecha' => '2019-10-08 07:23:00',
                'ejecutado' => 1,
            ),
            148 => 
            array (
                'id_alarmas' => 954,
                'id_cotizacion' => 9,
                'precio' => 26424.86,
                'inicio' => 26540.84,
                'id_usuario' => 1778,
                'fecha' => '2019-10-08 07:25:00',
                'ejecutado' => 1,
            ),
            149 => 
            array (
                'id_alarmas' => 956,
                'id_cotizacion' => 9,
                'precio' => 26435.0,
                'inicio' => 26471.84,
                'id_usuario' => 1778,
                'fecha' => '2019-10-08 08:24:00',
                'ejecutado' => 1,
            ),
            150 => 
            array (
                'id_alarmas' => 958,
                'id_cotizacion' => 17,
                'precio' => 10.945,
                'inicio' => 10.987,
                'id_usuario' => 1691,
                'fecha' => '2019-10-08 08:34:00',
                'ejecutado' => 1,
            ),
            151 => 
            array (
                'id_alarmas' => 959,
                'id_cotizacion' => 3,
                'precio' => 8060.0,
                'inicio' => 8206.72,
                'id_usuario' => 1876,
                'fecha' => '2019-10-08 10:58:00',
                'ejecutado' => 1,
            ),
            152 => 
            array (
                'id_alarmas' => 960,
                'id_cotizacion' => 2,
                'precio' => 0.27755,
                'inicio' => 0.27579,
                'id_usuario' => 1671,
                'fecha' => '2019-10-08 12:35:00',
                'ejecutado' => 1,
            ),
            153 => 
            array (
                'id_alarmas' => 961,
                'id_cotizacion' => 10,
                'precio' => 1505.1,
                'inicio' => 1503.63,
                'id_usuario' => 1882,
                'fecha' => '2019-10-08 15:10:00',
                'ejecutado' => 1,
            ),
            154 => 
            array (
                'id_alarmas' => 962,
                'id_cotizacion' => 10,
                'precio' => 1500.0,
                'inicio' => 1503.31,
                'id_usuario' => 1882,
                'fecha' => '2019-10-08 15:10:00',
                'ejecutado' => 1,
            ),
            155 => 
            array (
                'id_alarmas' => 965,
                'id_cotizacion' => 10,
                'precio' => 1499.15,
                'inicio' => 1503.23,
                'id_usuario' => 1882,
                'fecha' => '2019-10-08 15:25:00',
                'ejecutado' => 1,
            ),
            156 => 
            array (
                'id_alarmas' => 967,
                'id_cotizacion' => 17,
                'precio' => 10.927,
                'inicio' => 109.522,
                'id_usuario' => 1734,
                'fecha' => '2019-10-08 16:16:00',
                'ejecutado' => 0,
            ),
            157 => 
            array (
                'id_alarmas' => 968,
                'id_cotizacion' => 10,
                'precio' => 1501.91,
                'inicio' => 1501.94,
                'id_usuario' => 1812,
                'fecha' => '2019-10-08 16:17:00',
                'ejecutado' => 1,
            ),
            158 => 
            array (
                'id_alarmas' => 969,
                'id_cotizacion' => 10,
                'precio' => 1501.0,
                'inicio' => 1499.35,
                'id_usuario' => 1640,
                'fecha' => '2019-10-08 17:48:00',
                'ejecutado' => 1,
            ),
            159 => 
            array (
                'id_alarmas' => 970,
                'id_cotizacion' => 2,
                'precio' => 0.27,
                'inicio' => 0.27742,
                'id_usuario' => 1666,
                'fecha' => '2019-10-08 19:39:00',
                'ejecutado' => 1,
            ),
            160 => 
            array (
                'id_alarmas' => 971,
                'id_cotizacion' => 2,
                'precio' => 0.27,
                'inicio' => 0.27742,
                'id_usuario' => 1666,
                'fecha' => '2019-10-08 19:39:00',
                'ejecutado' => 1,
            ),
            161 => 
            array (
                'id_alarmas' => 972,
                'id_cotizacion' => 2,
                'precio' => 0.27,
                'inicio' => 0.27742,
                'id_usuario' => 1666,
                'fecha' => '2019-10-08 19:39:00',
                'ejecutado' => 1,
            ),
            162 => 
            array (
                'id_alarmas' => 973,
                'id_cotizacion' => 2,
                'precio' => 0.27,
                'inicio' => 0.27742,
                'id_usuario' => 1666,
                'fecha' => '2019-10-08 19:39:00',
                'ejecutado' => 1,
            ),
            163 => 
            array (
                'id_alarmas' => 974,
                'id_cotizacion' => 2,
                'precio' => 0.27,
                'inicio' => 0.27742,
                'id_usuario' => 1666,
                'fecha' => '2019-10-08 19:39:00',
                'ejecutado' => 1,
            ),
            164 => 
            array (
                'id_alarmas' => 975,
                'id_cotizacion' => 2,
                'precio' => 0.27,
                'inicio' => 0.27742,
                'id_usuario' => 1666,
                'fecha' => '2019-10-08 19:39:00',
                'ejecutado' => 1,
            ),
            165 => 
            array (
                'id_alarmas' => 976,
                'id_cotizacion' => 2,
                'precio' => 0.27,
                'inicio' => 0.27742,
                'id_usuario' => 1666,
                'fecha' => '2019-10-08 19:39:00',
                'ejecutado' => 1,
            ),
            166 => 
            array (
                'id_alarmas' => 977,
                'id_cotizacion' => 2,
                'precio' => 0.27,
                'inicio' => 0.27742,
                'id_usuario' => 1666,
                'fecha' => '2019-10-08 19:39:00',
                'ejecutado' => 1,
            ),
            167 => 
            array (
                'id_alarmas' => 978,
                'id_cotizacion' => 2,
                'precio' => 0.27,
                'inicio' => 0.27742,
                'id_usuario' => 1666,
                'fecha' => '2019-10-08 19:39:00',
                'ejecutado' => 1,
            ),
            168 => 
            array (
                'id_alarmas' => 979,
                'id_cotizacion' => 2,
                'precio' => 0.27,
                'inicio' => 0.27742,
                'id_usuario' => 1666,
                'fecha' => '2019-10-08 19:39:00',
                'ejecutado' => 1,
            ),
            169 => 
            array (
                'id_alarmas' => 980,
                'id_cotizacion' => 2,
                'precio' => 0.27,
                'inicio' => 0.27742,
                'id_usuario' => 1666,
                'fecha' => '2019-10-08 19:39:00',
                'ejecutado' => 1,
            ),
            170 => 
            array (
                'id_alarmas' => 981,
                'id_cotizacion' => 2,
                'precio' => 0.27,
                'inicio' => 0.27742,
                'id_usuario' => 1666,
                'fecha' => '2019-10-08 19:39:00',
                'ejecutado' => 1,
            ),
            171 => 
            array (
                'id_alarmas' => 982,
                'id_cotizacion' => 2,
                'precio' => 0.27,
                'inicio' => 0.27742,
                'id_usuario' => 1666,
                'fecha' => '2019-10-08 19:39:00',
                'ejecutado' => 1,
            ),
            172 => 
            array (
                'id_alarmas' => 983,
                'id_cotizacion' => 2,
                'precio' => 0.27,
                'inicio' => 0.27742,
                'id_usuario' => 1666,
                'fecha' => '2019-10-08 19:39:00',
                'ejecutado' => 1,
            ),
            173 => 
            array (
                'id_alarmas' => 984,
                'id_cotizacion' => 2,
                'precio' => 0.27,
                'inicio' => 0.27742,
                'id_usuario' => 1666,
                'fecha' => '2019-10-08 19:39:00',
                'ejecutado' => 1,
            ),
            174 => 
            array (
                'id_alarmas' => 985,
                'id_cotizacion' => 2,
                'precio' => 0.27,
                'inicio' => 0.27742,
                'id_usuario' => 1666,
                'fecha' => '2019-10-08 19:39:00',
                'ejecutado' => 1,
            ),
            175 => 
            array (
                'id_alarmas' => 986,
                'id_cotizacion' => 2,
                'precio' => 0.27,
                'inicio' => 0.27742,
                'id_usuario' => 1666,
                'fecha' => '2019-10-08 19:39:00',
                'ejecutado' => 1,
            ),
            176 => 
            array (
                'id_alarmas' => 987,
                'id_cotizacion' => 2,
                'precio' => 0.27,
                'inicio' => 0.27742,
                'id_usuario' => 1666,
                'fecha' => '2019-10-08 19:39:00',
                'ejecutado' => 1,
            ),
            177 => 
            array (
                'id_alarmas' => 988,
                'id_cotizacion' => 2,
                'precio' => 0.27,
                'inicio' => 0.27742,
                'id_usuario' => 1666,
                'fecha' => '2019-10-08 19:39:00',
                'ejecutado' => 1,
            ),
            178 => 
            array (
                'id_alarmas' => 990,
                'id_cotizacion' => 10,
                'precio' => 1507.37,
                'inicio' => 1508.36,
                'id_usuario' => 462,
                'fecha' => '2019-10-08 23:31:00',
                'ejecutado' => 1,
            ),
            179 => 
            array (
                'id_alarmas' => 991,
                'id_cotizacion' => 8,
                'precio' => 181.471,
                'inicio' => 181.553,
                'id_usuario' => 1671,
                'fecha' => '2019-10-09 00:07:00',
                'ejecutado' => 1,
            ),
            180 => 
            array (
                'id_alarmas' => 992,
                'id_cotizacion' => 8,
                'precio' => 181.471,
                'inicio' => 181.555,
                'id_usuario' => 1671,
                'fecha' => '2019-10-09 00:07:00',
                'ejecutado' => 1,
            ),
            181 => 
            array (
                'id_alarmas' => 993,
                'id_cotizacion' => 7,
                'precio' => 19.383,
                'inicio' => 193.979,
                'id_usuario' => 1671,
                'fecha' => '2019-10-09 00:09:00',
                'ejecutado' => 1,
            ),
            182 => 
            array (
                'id_alarmas' => 994,
                'id_cotizacion' => 3,
                'precio' => 8180.0,
                'inicio' => 8177.09,
                'id_usuario' => 787,
                'fecha' => '2019-10-09 03:35:00',
                'ejecutado' => 1,
            ),
            183 => 
            array (
                'id_alarmas' => 995,
                'id_cotizacion' => 3,
                'precio' => 8180.0,
                'inicio' => 8177.09,
                'id_usuario' => 787,
                'fecha' => '2019-10-09 03:35:00',
                'ejecutado' => 1,
            ),
            184 => 
            array (
                'id_alarmas' => 996,
                'id_cotizacion' => 6,
                'precio' => 107.295,
                'inicio' => 107.233,
                'id_usuario' => 1538,
                'fecha' => '2019-10-09 08:11:00',
                'ejecutado' => 1,
            ),
            185 => 
            array (
                'id_alarmas' => 997,
                'id_cotizacion' => 6,
                'precio' => 107.3,
                'inicio' => 107.289,
                'id_usuario' => 1538,
                'fecha' => '2019-10-09 08:26:00',
                'ejecutado' => 1,
            ),
            186 => 
            array (
                'id_alarmas' => 1000,
                'id_cotizacion' => 10,
                'precio' => 26232.6,
                'inicio' => 1508.26,
                'id_usuario' => 1671,
                'fecha' => '2019-10-09 14:24:00',
                'ejecutado' => 0,
            ),
            187 => 
            array (
                'id_alarmas' => 1001,
                'id_cotizacion' => 9,
                'precio' => 26232.6,
                'inicio' => 26330.84,
                'id_usuario' => 1671,
                'fecha' => '2019-10-09 14:25:00',
                'ejecutado' => 1,
            ),
            188 => 
            array (
                'id_alarmas' => 1002,
                'id_cotizacion' => 9,
                'precio' => 26232.6,
                'inicio' => 26329.84,
                'id_usuario' => 1671,
                'fecha' => '2019-10-09 14:25:00',
                'ejecutado' => 1,
            ),
            189 => 
            array (
                'id_alarmas' => 1006,
                'id_cotizacion' => 10,
                'precio' => 1506.5,
                'inicio' => 1505.3,
                'id_usuario' => 1882,
                'fecha' => '2019-10-09 17:05:00',
                'ejecutado' => 1,
            ),
            190 => 
            array (
                'id_alarmas' => 1007,
                'id_cotizacion' => 10,
                'precio' => 1504.95,
                'inicio' => 1506.39,
                'id_usuario' => 1935,
                'fecha' => '2019-10-09 17:12:00',
                'ejecutado' => 1,
            ),
            191 => 
            array (
                'id_alarmas' => 1008,
                'id_cotizacion' => 10,
                'precio' => 1503.32,
                'inicio' => 1506.98,
                'id_usuario' => 1362,
                'fecha' => '2019-10-09 17:52:00',
                'ejecutado' => 1,
            ),
            192 => 
            array (
                'id_alarmas' => 1009,
                'id_cotizacion' => 9,
                'precio' => 26356.24,
                'inicio' => 26356.24,
                'id_usuario' => 1989,
                'fecha' => '2019-10-09 20:17:00',
                'ejecutado' => 0,
            ),
            193 => 
            array (
                'id_alarmas' => 1010,
                'id_cotizacion' => 10,
                'precio' => 1506.49,
                'inicio' => 1506.45,
                'id_usuario' => 1989,
                'fecha' => '2019-10-09 20:17:00',
                'ejecutado' => 1,
            ),
            194 => 
            array (
                'id_alarmas' => 1011,
                'id_cotizacion' => 10,
                'precio' => 1506.49,
                'inicio' => 1505.81,
                'id_usuario' => 1989,
                'fecha' => '2019-10-09 20:27:00',
                'ejecutado' => 1,
            ),
            195 => 
            array (
                'id_alarmas' => 1012,
                'id_cotizacion' => 10,
                'precio' => 1506.49,
                'inicio' => 1506.0,
                'id_usuario' => 1989,
                'fecha' => '2019-10-09 20:28:00',
                'ejecutado' => 1,
            ),
            196 => 
            array (
                'id_alarmas' => 1013,
                'id_cotizacion' => 17,
                'precio' => 10.987,
                'inicio' => 109.905,
                'id_usuario' => 1882,
                'fecha' => '2019-10-10 02:39:00',
                'ejecutado' => 1,
            ),
            197 => 
            array (
                'id_alarmas' => 1014,
                'id_cotizacion' => 7,
                'precio' => 193.482,
                'inicio' => 193.652,
                'id_usuario' => 1777,
                'fecha' => '2019-10-10 03:29:00',
                'ejecutado' => 1,
            ),
            198 => 
            array (
                'id_alarmas' => 1015,
                'id_cotizacion' => 17,
                'precio' => 1.099,
                'inicio' => 109.862,
                'id_usuario' => 1643,
                'fecha' => '2019-10-10 06:02:00',
                'ejecutado' => 1,
            ),
            199 => 
            array (
                'id_alarmas' => 1016,
                'id_cotizacion' => 9,
                'precio' => 26505.84,
                'inicio' => 26305.34,
                'id_usuario' => 1437,
                'fecha' => '2019-10-10 12:35:00',
                'ejecutado' => 1,
            ),
            200 => 
            array (
                'id_alarmas' => 1017,
                'id_cotizacion' => 6,
                'precio' => 107.973,
                'inicio' => 107.761,
                'id_usuario' => 1133,
                'fecha' => '2019-10-10 13:47:00',
                'ejecutado' => 1,
            ),
            201 => 
            array (
                'id_alarmas' => 1018,
                'id_cotizacion' => 15,
                'precio' => 132.727,
                'inicio' => 133.938,
                'id_usuario' => 1292,
                'fecha' => '2019-10-10 17:24:00',
                'ejecutado' => 0,
            ),
            202 => 
            array (
                'id_alarmas' => 1019,
                'id_cotizacion' => 6,
                'precio' => 107.615,
                'inicio' => 107.862,
                'id_usuario' => 1292,
                'fecha' => '2019-10-10 17:24:00',
                'ejecutado' => 0,
            ),
            203 => 
            array (
                'id_alarmas' => 1020,
                'id_cotizacion' => 6,
                'precio' => 108.15,
                'inicio' => 107.898,
                'id_usuario' => 1691,
                'fecha' => '2019-10-10 18:40:00',
                'ejecutado' => 1,
            ),
            204 => 
            array (
                'id_alarmas' => 1021,
                'id_cotizacion' => 6,
                'precio' => 107.965,
                'inicio' => 107.963,
                'id_usuario' => 1979,
                'fecha' => '2019-10-10 19:13:00',
                'ejecutado' => 1,
            ),
            205 => 
            array (
                'id_alarmas' => 1022,
                'id_cotizacion' => 6,
                'precio' => 107.965,
                'inicio' => 107.961,
                'id_usuario' => 1979,
                'fecha' => '2019-10-10 19:13:00',
                'ejecutado' => 1,
            ),
            206 => 
            array (
                'id_alarmas' => 1023,
                'id_cotizacion' => 2,
                'precio' => 0.27263,
                'inicio' => 0.27258,
                'id_usuario' => 1952,
                'fecha' => '2019-10-11 03:37:00',
                'ejecutado' => 1,
            ),
            207 => 
            array (
                'id_alarmas' => 1024,
                'id_cotizacion' => 8,
                'precio' => 18.359,
                'inicio' => 183.682,
                'id_usuario' => 1691,
                'fecha' => '2019-10-11 05:29:00',
                'ejecutado' => 1,
            ),
            208 => 
            array (
                'id_alarmas' => 1027,
                'id_cotizacion' => 7,
                'precio' => 1.96,
                'inicio' => 196.938,
                'id_usuario' => 1691,
                'fecha' => '2019-10-11 05:31:00',
                'ejecutado' => 1,
            ),
            209 => 
            array (
                'id_alarmas' => 1029,
                'id_cotizacion' => 7,
                'precio' => 19.796,
                'inicio' => 197.025,
                'id_usuario' => 1691,
                'fecha' => '2019-10-11 06:44:00',
                'ejecutado' => 1,
            ),
            210 => 
            array (
                'id_alarmas' => 1030,
                'id_cotizacion' => 8,
                'precio' => 1.833,
                'inicio' => 183.169,
                'id_usuario' => 1691,
                'fecha' => '2019-10-11 08:49:00',
                'ejecutado' => 1,
            ),
            211 => 
            array (
                'id_alarmas' => 1035,
                'id_cotizacion' => 10,
                'precio' => 1491.95,
                'inicio' => 1494.91,
                'id_usuario' => 1882,
                'fecha' => '2019-10-11 10:59:00',
                'ejecutado' => 1,
            ),
            212 => 
            array (
                'id_alarmas' => 1036,
                'id_cotizacion' => 10,
                'precio' => 1493.5,
                'inicio' => 1494.82,
                'id_usuario' => 1882,
                'fecha' => '2019-10-11 10:59:00',
                'ejecutado' => 1,
            ),
            213 => 
            array (
                'id_alarmas' => 1037,
                'id_cotizacion' => 10,
                'precio' => 1489.6,
                'inicio' => 1491.98,
                'id_usuario' => 1882,
                'fecha' => '2019-10-11 11:32:00',
                'ejecutado' => 1,
            ),
            214 => 
            array (
                'id_alarmas' => 1038,
                'id_cotizacion' => 3,
                'precio' => 8170.0,
                'inicio' => 8350.68,
                'id_usuario' => 1749,
                'fecha' => '2019-10-11 22:57:00',
                'ejecutado' => 1,
            ),
            215 => 
            array (
                'id_alarmas' => 1039,
                'id_cotizacion' => 5,
                'precio' => 183.44,
                'inicio' => 184.36,
                'id_usuario' => 108,
                'fecha' => '2019-10-12 07:52:00',
                'ejecutado' => 1,
            ),
            216 => 
            array (
                'id_alarmas' => 1040,
                'id_cotizacion' => 5,
                'precio' => 182.3,
                'inicio' => 184.04,
                'id_usuario' => 1671,
                'fecha' => '2019-10-12 15:58:00',
                'ejecutado' => 1,
            ),
            217 => 
            array (
                'id_alarmas' => 1041,
                'id_cotizacion' => 8,
                'precio' => 185.965,
                'inicio' => 186.251,
                'id_usuario' => 1777,
                'fecha' => '2019-10-13 21:08:00',
                'ejecutado' => 1,
            ),
            218 => 
            array (
                'id_alarmas' => 1042,
                'id_cotizacion' => 15,
                'precio' => 136.706,
                'inicio' => 136.528,
                'id_usuario' => 1909,
                'fecha' => '2019-10-14 03:38:00',
                'ejecutado' => 1,
            ),
            219 => 
            array (
                'id_alarmas' => 1044,
                'id_cotizacion' => 8,
                'precio' => 185.955,
                'inicio' => 185.949,
                'id_usuario' => 1640,
                'fecha' => '2019-10-14 08:19:00',
                'ejecutado' => 1,
            ),
            220 => 
            array (
                'id_alarmas' => 1045,
                'id_cotizacion' => 17,
                'precio' => 1.1,
                'inicio' => 110.243,
                'id_usuario' => 1691,
                'fecha' => '2019-10-14 08:30:00',
                'ejecutado' => 1,
            ),
            221 => 
            array (
                'id_alarmas' => 1047,
                'id_cotizacion' => 8,
                'precio' => 185.832,
                'inicio' => 185.988,
                'id_usuario' => 1920,
                'fecha' => '2019-10-14 10:37:00',
                'ejecutado' => 1,
            ),
            222 => 
            array (
                'id_alarmas' => 1048,
                'id_cotizacion' => 6,
                'precio' => 108.223,
                'inicio' => 108.225,
                'id_usuario' => 1996,
                'fecha' => '2019-10-14 11:56:00',
                'ejecutado' => 1,
            ),
            223 => 
            array (
                'id_alarmas' => 1049,
                'id_cotizacion' => 6,
                'precio' => 108.223,
                'inicio' => 108.226,
                'id_usuario' => 1996,
                'fecha' => '2019-10-14 11:56:00',
                'ejecutado' => 1,
            ),
            224 => 
            array (
                'id_alarmas' => 1051,
                'id_cotizacion' => 2,
                'precio' => 0.2909,
                'inicio' => 0.29039,
                'id_usuario' => 1777,
                'fecha' => '2019-10-14 14:09:00',
                'ejecutado' => 1,
            ),
            225 => 
            array (
                'id_alarmas' => 1052,
                'id_cotizacion' => 10,
                'precio' => 1494.8,
                'inicio' => 1492.62,
                'id_usuario' => 1963,
                'fecha' => '2019-10-14 17:07:00',
                'ejecutado' => 1,
            ),
            226 => 
            array (
                'id_alarmas' => 1053,
                'id_cotizacion' => 6,
                'precio' => 108.24,
                'inicio' => 108.363,
                'id_usuario' => 1362,
                'fecha' => '2019-10-14 19:42:00',
                'ejecutado' => 1,
            ),
            227 => 
            array (
                'id_alarmas' => 1054,
                'id_cotizacion' => 7,
                'precio' => 19.972,
                'inicio' => 19.941,
                'id_usuario' => 1691,
                'fecha' => '2019-10-14 20:07:00',
                'ejecutado' => 1,
            ),
            228 => 
            array (
                'id_alarmas' => 1056,
                'id_cotizacion' => 12,
                'precio' => 132.222,
                'inicio' => 132.279,
                'id_usuario' => 735,
                'fecha' => '2019-10-15 01:06:00',
                'ejecutado' => 1,
            ),
            229 => 
            array (
                'id_alarmas' => 1057,
                'id_cotizacion' => 8,
                'precio' => 186.238,
                'inicio' => 186.157,
                'id_usuario' => 1777,
                'fecha' => '2019-10-15 01:35:00',
                'ejecutado' => 1,
            ),
            230 => 
            array (
                'id_alarmas' => 1060,
                'id_cotizacion' => 8,
                'precio' => 186.238,
                'inicio' => 186.183,
                'id_usuario' => 1777,
                'fecha' => '2019-10-15 03:48:00',
                'ejecutado' => 1,
            ),
            231 => 
            array (
                'id_alarmas' => 1062,
                'id_cotizacion' => 6,
                'precio' => 108.6,
                'inicio' => 108.336,
                'id_usuario' => 1691,
                'fecha' => '2019-10-15 04:55:00',
                'ejecutado' => 1,
            ),
            232 => 
            array (
                'id_alarmas' => 1063,
                'id_cotizacion' => 8,
                'precio' => 18.682,
                'inicio' => 186.902,
                'id_usuario' => 1362,
                'fecha' => '2019-10-15 09:51:00',
                'ejecutado' => 1,
            ),
            233 => 
            array (
                'id_alarmas' => 1066,
                'id_cotizacion' => 8,
                'precio' => 18.692,
                'inicio' => 18.694,
                'id_usuario' => 1362,
                'fecha' => '2019-10-15 09:56:00',
                'ejecutado' => 1,
            ),
            234 => 
            array (
                'id_alarmas' => 1067,
                'id_cotizacion' => 8,
                'precio' => 18.736,
                'inicio' => 187.368,
                'id_usuario' => 1362,
                'fecha' => '2019-10-15 10:46:00',
                'ejecutado' => 1,
            ),
            235 => 
            array (
                'id_alarmas' => 1070,
                'id_cotizacion' => 2,
                'precio' => 0.2946,
                'inicio' => 0.29411,
                'id_usuario' => 1777,
                'fecha' => '2019-10-15 12:39:00',
                'ejecutado' => 1,
            ),
            236 => 
            array (
                'id_alarmas' => 1071,
                'id_cotizacion' => 2,
                'precio' => 0.2963,
                'inicio' => 0.29551,
                'id_usuario' => 1882,
                'fecha' => '2019-10-15 12:49:00',
                'ejecutado' => 1,
            ),
            237 => 
            array (
                'id_alarmas' => 1072,
                'id_cotizacion' => 2,
                'precio' => 0.298,
                'inicio' => 0.2954,
                'id_usuario' => 1882,
                'fecha' => '2019-10-15 12:49:00',
                'ejecutado' => 1,
            ),
            238 => 
            array (
                'id_alarmas' => 1077,
                'id_cotizacion' => 6,
                'precio' => 108.55,
                'inicio' => 108.629,
                'id_usuario' => 1691,
                'fecha' => '2019-10-15 15:15:00',
                'ejecutado' => 1,
            ),
            239 => 
            array (
                'id_alarmas' => 1080,
                'id_cotizacion' => 5,
                'precio' => 178.0,
                'inicio' => 180.72,
                'id_usuario' => 493,
                'fecha' => '2019-10-15 18:32:00',
                'ejecutado' => 1,
            ),
            240 => 
            array (
                'id_alarmas' => 1084,
                'id_cotizacion' => 6,
                'precio' => 108.841,
                'inicio' => 108.879,
                'id_usuario' => 1292,
                'fecha' => '2019-10-15 19:50:00',
                'ejecutado' => 1,
            ),
            241 => 
            array (
                'id_alarmas' => 1085,
                'id_cotizacion' => 5,
                'precio' => 181.0,
                'inicio' => 179.05,
                'id_usuario' => 493,
                'fecha' => '2019-10-15 21:10:00',
                'ejecutado' => 1,
            ),
            242 => 
            array (
                'id_alarmas' => 1087,
                'id_cotizacion' => 10,
                'precio' => 1488.0,
                'inicio' => 1483.37,
                'id_usuario' => 1963,
                'fecha' => '2019-10-16 03:44:00',
                'ejecutado' => 1,
            ),
            243 => 
            array (
                'id_alarmas' => 1088,
                'id_cotizacion' => 10,
                'precio' => 1487.0,
                'inicio' => 1483.46,
                'id_usuario' => 1963,
                'fecha' => '2019-10-16 07:22:00',
                'ejecutado' => 1,
            ),
            244 => 
            array (
                'id_alarmas' => 1089,
                'id_cotizacion' => 9,
                'precio' => 27037.0,
                'inicio' => 27009.34,
                'id_usuario' => 1671,
                'fecha' => '2019-10-16 13:00:00',
                'ejecutado' => 1,
            ),
            245 => 
            array (
                'id_alarmas' => 1095,
                'id_cotizacion' => 3,
                'precio' => 7860.0,
                'inicio' => 7994.19,
                'id_usuario' => 1778,
                'fecha' => '2019-10-16 14:55:00',
                'ejecutado' => 1,
            ),
            246 => 
            array (
                'id_alarmas' => 1096,
                'id_cotizacion' => 3,
                'precio' => 8250.0,
                'inicio' => 7994.29,
                'id_usuario' => 1778,
                'fecha' => '2019-10-16 14:55:00',
                'ejecutado' => 1,
            ),
            247 => 
            array (
                'id_alarmas' => 1099,
                'id_cotizacion' => 18,
                'precio' => 0.99506,
                'inicio' => 0.99631,
                'id_usuario' => 1691,
                'fecha' => '2019-10-16 15:46:00',
                'ejecutado' => 1,
            ),
            248 => 
            array (
                'id_alarmas' => 1100,
                'id_cotizacion' => 3,
                'precio' => 7960.0,
                'inicio' => 8003.9,
                'id_usuario' => 1575,
                'fecha' => '2019-10-16 15:55:00',
                'ejecutado' => 1,
            ),
            249 => 
            array (
                'id_alarmas' => 1102,
                'id_cotizacion' => 2,
                'precio' => 0.28378,
                'inicio' => 0.28258,
                'id_usuario' => 1777,
                'fecha' => '2019-10-16 17:23:00',
                'ejecutado' => 1,
            ),
            250 => 
            array (
                'id_alarmas' => 1103,
                'id_cotizacion' => 5,
                'precio' => 174.7,
                'inicio' => 174.28,
                'id_usuario' => 493,
                'fecha' => '2019-10-16 19:48:00',
                'ejecutado' => 1,
            ),
            251 => 
            array (
                'id_alarmas' => 1104,
                'id_cotizacion' => 10,
                'precio' => 1492.8,
                'inicio' => 1490.47,
                'id_usuario' => 1963,
                'fecha' => '2019-10-16 22:42:00',
                'ejecutado' => 1,
            ),
            252 => 
            array (
                'id_alarmas' => 1105,
                'id_cotizacion' => 10,
                'precio' => 1491.5,
                'inicio' => 1490.51,
                'id_usuario' => 1963,
                'fecha' => '2019-10-16 22:42:00',
                'ejecutado' => 1,
            ),
            253 => 
            array (
                'id_alarmas' => 1107,
                'id_cotizacion' => 2,
                'precio' => 0.28382,
                'inicio' => 0.28382,
                'id_usuario' => 1996,
                'fecha' => '2019-10-17 01:16:00',
                'ejecutado' => 0,
            ),
            254 => 
            array (
                'id_alarmas' => 1109,
                'id_cotizacion' => 3,
                'precio' => 7991.99,
                'inicio' => 7991.99,
                'id_usuario' => 1996,
                'fecha' => '2019-10-17 01:16:00',
                'ejecutado' => 0,
            ),
            255 => 
            array (
                'id_alarmas' => 1111,
                'id_cotizacion' => 4,
                'precio' => 35.85,
                'inicio' => 35.86,
                'id_usuario' => 1996,
                'fecha' => '2019-10-17 01:16:00',
                'ejecutado' => 1,
            ),
            256 => 
            array (
                'id_alarmas' => 1112,
                'id_cotizacion' => 5,
                'precio' => 174.36,
                'inicio' => 174.36,
                'id_usuario' => 1996,
                'fecha' => '2019-10-17 01:16:00',
                'ejecutado' => 0,
            ),
            257 => 
            array (
                'id_alarmas' => 1113,
                'id_cotizacion' => 6,
                'precio' => 108.753,
                'inicio' => 108.756,
                'id_usuario' => 1996,
                'fecha' => '2019-10-17 01:16:00',
                'ejecutado' => 1,
            ),
            258 => 
            array (
                'id_alarmas' => 1114,
                'id_cotizacion' => 7,
                'precio' => 203.788,
                'inicio' => 203.876,
                'id_usuario' => 1996,
                'fecha' => '2019-10-17 01:17:00',
                'ejecutado' => 1,
            ),
            259 => 
            array (
                'id_alarmas' => 1115,
                'id_cotizacion' => 15,
                'precio' => 139.514,
                'inicio' => 139.529,
                'id_usuario' => 1996,
                'fecha' => '2019-10-17 01:17:00',
                'ejecutado' => 1,
            ),
            260 => 
            array (
                'id_alarmas' => 1116,
                'id_cotizacion' => 8,
                'precio' => 189.019,
                'inicio' => 189.118,
                'id_usuario' => 1996,
                'fecha' => '2019-10-17 01:17:00',
                'ejecutado' => 1,
            ),
            261 => 
            array (
                'id_alarmas' => 1117,
                'id_cotizacion' => 9,
                'precio' => 26961.24,
                'inicio' => 26959.24,
                'id_usuario' => 1996,
                'fecha' => '2019-10-17 01:17:00',
                'ejecutado' => 1,
            ),
            262 => 
            array (
                'id_alarmas' => 1118,
                'id_cotizacion' => 17,
                'precio' => 11.082,
                'inicio' => 110.831,
                'id_usuario' => 1996,
                'fecha' => '2019-10-17 01:17:00',
                'ejecutado' => 1,
            ),
            263 => 
            array (
                'id_alarmas' => 1120,
                'id_cotizacion' => 3,
                'precio' => 7933.8,
                'inicio' => 7992.89,
                'id_usuario' => 1133,
                'fecha' => '2019-10-17 02:26:00',
                'ejecutado' => 1,
            ),
            264 => 
            array (
                'id_alarmas' => 1121,
                'id_cotizacion' => 5,
                'precio' => 172.25,
                'inicio' => 174.53,
                'id_usuario' => 1133,
                'fecha' => '2019-10-17 02:31:00',
                'ejecutado' => 1,
            ),
            265 => 
            array (
                'id_alarmas' => 1122,
                'id_cotizacion' => 2,
                'precio' => 0.28097,
                'inicio' => 0.28381,
                'id_usuario' => 1133,
                'fecha' => '2019-10-17 02:44:00',
                'ejecutado' => 1,
            ),
            266 => 
            array (
                'id_alarmas' => 1123,
                'id_cotizacion' => 10,
                'precio' => 1484.0,
                'inicio' => 1488.8,
                'id_usuario' => 1963,
                'fecha' => '2019-10-17 03:47:00',
                'ejecutado' => 1,
            ),
            267 => 
            array (
                'id_alarmas' => 1124,
                'id_cotizacion' => 10,
                'precio' => 1484.0,
                'inicio' => 1488.82,
                'id_usuario' => 1963,
                'fecha' => '2019-10-17 03:47:00',
                'ejecutado' => 1,
            ),
            268 => 
            array (
                'id_alarmas' => 1125,
                'id_cotizacion' => 6,
                'precio' => 108.47,
                'inicio' => 108.602,
                'id_usuario' => 1362,
                'fecha' => '2019-10-17 13:57:00',
                'ejecutado' => 1,
            ),
            269 => 
            array (
                'id_alarmas' => 1126,
                'id_cotizacion' => 2,
                'precio' => 0.3015,
                'inicio' => 0.2996,
                'id_usuario' => 1749,
                'fecha' => '2019-10-17 19:10:00',
                'ejecutado' => 1,
            ),
            270 => 
            array (
                'id_alarmas' => 1127,
                'id_cotizacion' => 19,
                'precio' => 174.095,
                'inicio' => 174.595,
                'id_usuario' => 1909,
                'fecha' => '2019-10-18 01:25:00',
                'ejecutado' => 1,
            ),
            271 => 
            array (
                'id_alarmas' => 1128,
                'id_cotizacion' => 10,
                'precio' => 1488.5,
                'inicio' => 1488.56,
                'id_usuario' => 1963,
                'fecha' => '2019-10-18 07:03:00',
                'ejecutado' => 1,
            ),
            272 => 
            array (
                'id_alarmas' => 1129,
                'id_cotizacion' => 7,
                'precio' => 2.022,
                'inicio' => 202.345,
                'id_usuario' => 1859,
                'fecha' => '2019-10-18 07:39:00',
                'ejecutado' => 1,
            ),
            273 => 
            array (
                'id_alarmas' => 1130,
                'id_cotizacion' => 5,
                'precio' => 169.0,
                'inicio' => 171.12,
                'id_usuario' => 493,
                'fecha' => '2019-10-20 01:52:00',
                'ejecutado' => 1,
            ),
            274 => 
            array (
                'id_alarmas' => 1132,
                'id_cotizacion' => 5,
                'precio' => 174.0,
                'inicio' => 171.08,
                'id_usuario' => 493,
                'fecha' => '2019-10-20 01:53:00',
                'ejecutado' => 1,
            ),
            275 => 
            array (
                'id_alarmas' => 1133,
                'id_cotizacion' => 3,
                'precio' => 7800.0,
                'inicio' => 7963.56,
                'id_usuario' => 493,
                'fecha' => '2019-10-20 01:53:00',
                'ejecutado' => 1,
            ),
            276 => 
            array (
                'id_alarmas' => 1134,
                'id_cotizacion' => 3,
                'precio' => 8200.0,
                'inicio' => 7962.75,
                'id_usuario' => 493,
                'fecha' => '2019-10-20 01:53:00',
                'ejecutado' => 1,
            ),
            277 => 
            array (
                'id_alarmas' => 1135,
                'id_cotizacion' => 2,
                'precio' => 0.29515,
                'inicio' => 0.29514,
                'id_usuario' => 1669,
                'fecha' => '2019-10-21 00:18:00',
                'ejecutado' => 1,
            ),
            278 => 
            array (
                'id_alarmas' => 1137,
                'id_cotizacion' => 9,
                'precio' => 26870.0,
                'inicio' => 26858.74,
                'id_usuario' => 380,
                'fecha' => '2019-10-21 02:48:00',
                'ejecutado' => 1,
            ),
            279 => 
            array (
                'id_alarmas' => 1138,
                'id_cotizacion' => 18,
                'precio' => 0.992,
                'inicio' => 0.98606,
                'id_usuario' => 1691,
                'fecha' => '2019-10-21 04:52:00',
                'ejecutado' => 1,
            ),
            280 => 
            array (
                'id_alarmas' => 1139,
                'id_cotizacion' => 7,
                'precio' => 202.984,
                'inicio' => 202.539,
                'id_usuario' => 1671,
                'fecha' => '2019-10-21 17:54:00',
                'ejecutado' => 1,
            ),
            281 => 
            array (
                'id_alarmas' => 1140,
                'id_cotizacion' => 8,
                'precio' => 18.905,
                'inicio' => 188.807,
                'id_usuario' => 1671,
                'fecha' => '2019-10-21 17:55:00',
                'ejecutado' => 1,
            ),
            282 => 
            array (
                'id_alarmas' => 1141,
                'id_cotizacion' => 2,
                'precio' => 0.29621,
                'inicio' => 0.29283,
                'id_usuario' => 559,
                'fecha' => '2019-10-21 19:54:00',
                'ejecutado' => 1,
            ),
            283 => 
            array (
                'id_alarmas' => 1142,
                'id_cotizacion' => 2,
                'precio' => 0.29621,
                'inicio' => 0.29283,
                'id_usuario' => 559,
                'fecha' => '2019-10-21 19:54:00',
                'ejecutado' => 1,
            ),
            284 => 
            array (
                'id_alarmas' => 1143,
                'id_cotizacion' => 9,
                'precio' => 26896.74,
                'inicio' => 26879.74,
                'id_usuario' => 2001,
                'fecha' => '2019-10-22 01:32:00',
                'ejecutado' => 1,
            ),
            285 => 
            array (
                'id_alarmas' => 1144,
                'id_cotizacion' => 9,
                'precio' => 26856.0,
                'inicio' => 26878.24,
                'id_usuario' => 2001,
                'fecha' => '2019-10-22 01:33:00',
                'ejecutado' => 1,
            ),
            286 => 
            array (
                'id_alarmas' => 1145,
                'id_cotizacion' => 9,
                'precio' => 26965.0,
                'inicio' => 26914.34,
                'id_usuario' => 2001,
                'fecha' => '2019-10-22 16:37:00',
                'ejecutado' => 1,
            ),
            287 => 
            array (
                'id_alarmas' => 1146,
                'id_cotizacion' => 7,
                'precio' => 202.035,
                'inicio' => 201.966,
                'id_usuario' => 1777,
                'fecha' => '2019-10-22 16:57:00',
                'ejecutado' => 1,
            ),
            288 => 
            array (
                'id_alarmas' => 1147,
                'id_cotizacion' => 9,
                'precio' => 26730.0,
                'inicio' => 26777.74,
                'id_usuario' => 2001,
                'fecha' => '2019-10-22 23:37:00',
                'ejecutado' => 1,
            ),
            289 => 
            array (
                'id_alarmas' => 1148,
                'id_cotizacion' => 9,
                'precio' => 26730.0,
                'inicio' => 26777.74,
                'id_usuario' => 2001,
                'fecha' => '2019-10-22 23:37:00',
                'ejecutado' => 1,
            ),
            290 => 
            array (
                'id_alarmas' => 1149,
                'id_cotizacion' => 7,
                'precio' => 20.118,
                'inicio' => 200.997,
                'id_usuario' => 1777,
                'fecha' => '2019-10-22 23:40:00',
                'ejecutado' => 1,
            ),
            291 => 
            array (
                'id_alarmas' => 1150,
                'id_cotizacion' => 3,
                'precio' => 8000.0,
                'inicio' => 8072.07,
                'id_usuario' => 1778,
                'fecha' => '2019-10-23 01:47:00',
                'ejecutado' => 1,
            ),
            292 => 
            array (
                'id_alarmas' => 1151,
                'id_cotizacion' => 9,
                'precio' => 26700.0,
                'inicio' => 26735.74,
                'id_usuario' => 2001,
                'fecha' => '2019-10-23 02:04:00',
                'ejecutado' => 1,
            ),
            293 => 
            array (
                'id_alarmas' => 1152,
                'id_cotizacion' => 18,
                'precio' => 0.98798,
                'inicio' => 0.98901,
                'id_usuario' => 1538,
                'fecha' => '2019-10-23 06:45:00',
                'ejecutado' => 1,
            ),
            294 => 
            array (
                'id_alarmas' => 1153,
                'id_cotizacion' => 9,
                'precio' => 26690.0,
                'inicio' => 26718.34,
                'id_usuario' => 2001,
                'fecha' => '2019-10-23 10:52:00',
                'ejecutado' => 1,
            ),
            295 => 
            array (
                'id_alarmas' => 1155,
                'id_cotizacion' => 9,
                'precio' => 26700.0,
                'inicio' => 26714.84,
                'id_usuario' => 1877,
                'fecha' => '2019-10-23 10:59:00',
                'ejecutado' => 1,
            ),
            296 => 
            array (
                'id_alarmas' => 1156,
                'id_cotizacion' => 9,
                'precio' => 26700.0,
                'inicio' => 26714.84,
                'id_usuario' => 1877,
                'fecha' => '2019-10-23 10:59:00',
                'ejecutado' => 1,
            ),
            297 => 
            array (
                'id_alarmas' => 1161,
                'id_cotizacion' => 3,
                'precio' => 6300.0,
                'inicio' => 7467.91,
                'id_usuario' => 950,
                'fecha' => '2019-10-23 21:50:00',
                'ejecutado' => 0,
            ),
            298 => 
            array (
                'id_alarmas' => 1162,
                'id_cotizacion' => 10,
                'precio' => 1490.45,
                'inicio' => 1493.22,
                'id_usuario' => 1855,
                'fecha' => '2019-10-24 02:08:00',
                'ejecutado' => 1,
            ),
            299 => 
            array (
                'id_alarmas' => 1163,
                'id_cotizacion' => 6,
                'precio' => 108.683,
                'inicio' => 108.685,
                'id_usuario' => 1621,
                'fecha' => '2019-10-25 01:20:00',
                'ejecutado' => 1,
            ),
            300 => 
            array (
                'id_alarmas' => 1164,
                'id_cotizacion' => 9,
                'precio' => 26817.74,
                'inicio' => 26820.24,
                'id_usuario' => 1621,
                'fecha' => '2019-10-25 01:20:00',
                'ejecutado' => 1,
            ),
            301 => 
            array (
                'id_alarmas' => 1165,
                'id_cotizacion' => 10,
                'precio' => 1508.3,
                'inicio' => 1505.59,
                'id_usuario' => 1963,
                'fecha' => '2019-10-25 07:03:00',
                'ejecutado' => 1,
            ),
            302 => 
            array (
                'id_alarmas' => 1166,
                'id_cotizacion' => 10,
                'precio' => 1508.5,
                'inicio' => 1505.44,
                'id_usuario' => 1963,
                'fecha' => '2019-10-25 07:03:00',
                'ejecutado' => 1,
            ),
            303 => 
            array (
                'id_alarmas' => 1167,
                'id_cotizacion' => 10,
                'precio' => 1504.33,
                'inicio' => 1506.21,
                'id_usuario' => 1918,
                'fecha' => '2019-10-25 09:14:00',
                'ejecutado' => 1,
            ),
            304 => 
            array (
                'id_alarmas' => 1168,
                'id_cotizacion' => 10,
                'precio' => 1506.41,
                'inicio' => 1505.81,
                'id_usuario' => 1918,
                'fecha' => '2019-10-25 09:17:00',
                'ejecutado' => 1,
            ),
            305 => 
            array (
                'id_alarmas' => 1169,
                'id_cotizacion' => 8,
                'precio' => 188.069,
                'inicio' => 188.013,
                'id_usuario' => 1671,
                'fecha' => '2019-10-25 18:05:00',
                'ejecutado' => 1,
            ),
            306 => 
            array (
                'id_alarmas' => 1170,
                'id_cotizacion' => 2,
                'precio' => 0.29865,
                'inicio' => 0.29754,
                'id_usuario' => 1575,
                'fecha' => '2019-10-25 23:23:00',
                'ejecutado' => 1,
            ),
            307 => 
            array (
                'id_alarmas' => 1171,
                'id_cotizacion' => 3,
                'precio' => 10000.0,
                'inicio' => 9689.75,
                'id_usuario' => 1768,
                'fecha' => '2019-10-26 05:04:00',
                'ejecutado' => 0,
            ),
            308 => 
            array (
                'id_alarmas' => 1172,
                'id_cotizacion' => 3,
                'precio' => 9149.15,
                'inicio' => 9204.72,
                'id_usuario' => 1470,
                'fecha' => '2019-10-26 15:55:00',
                'ejecutado' => 1,
            ),
            309 => 
            array (
                'id_alarmas' => 1173,
                'id_cotizacion' => 4,
                'precio' => 36.16,
                'inicio' => 36.14,
                'id_usuario' => 1552,
                'fecha' => '2019-10-26 19:03:00',
                'ejecutado' => 1,
            ),
            310 => 
            array (
                'id_alarmas' => 1174,
                'id_cotizacion' => 3,
                'precio' => 9144.82,
                'inicio' => 8991.79,
                'id_usuario' => 1470,
                'fecha' => '2019-10-26 19:38:00',
                'ejecutado' => 1,
            ),
            311 => 
            array (
                'id_alarmas' => 1176,
                'id_cotizacion' => 3,
                'precio' => 9200.0,
                'inicio' => 9570.42,
                'id_usuario' => 1749,
                'fecha' => '2019-10-27 21:12:00',
                'ejecutado' => 1,
            ),
            312 => 
            array (
                'id_alarmas' => 1177,
                'id_cotizacion' => 9,
                'precio' => 27116.0,
                'inicio' => 27007.24,
                'id_usuario' => 2001,
                'fecha' => '2019-10-28 02:01:00',
                'ejecutado' => 1,
            ),
            313 => 
            array (
                'id_alarmas' => 1178,
                'id_cotizacion' => 9,
                'precio' => 27050.0,
                'inicio' => 27008.74,
                'id_usuario' => 1877,
                'fecha' => '2019-10-28 05:48:00',
                'ejecutado' => 1,
            ),
            314 => 
            array (
                'id_alarmas' => 1179,
                'id_cotizacion' => 9,
                'precio' => 27040.0,
                'inicio' => 27009.24,
                'id_usuario' => 1877,
                'fecha' => '2019-10-28 05:48:00',
                'ejecutado' => 1,
            ),
            315 => 
            array (
                'id_alarmas' => 1181,
                'id_cotizacion' => 17,
                'precio' => 11.085,
                'inicio' => 110.889,
                'id_usuario' => 1691,
                'fecha' => '2019-10-28 07:43:00',
                'ejecutado' => 1,
            ),
            316 => 
            array (
                'id_alarmas' => 1182,
                'id_cotizacion' => 3,
                'precio' => 9420.61,
                'inicio' => 9384.82,
                'id_usuario' => 1958,
                'fecha' => '2019-10-28 17:20:00',
                'ejecutado' => 1,
            ),
            317 => 
            array (
                'id_alarmas' => 1183,
                'id_cotizacion' => 3,
                'precio' => 9454.36,
                'inicio' => 9424.36,
                'id_usuario' => 1958,
                'fecha' => '2019-10-28 18:09:00',
                'ejecutado' => 1,
            ),
            318 => 
            array (
                'id_alarmas' => 1184,
                'id_cotizacion' => 13,
                'precio' => 0.63547,
                'inicio' => 0.63631,
                'id_usuario' => 1691,
                'fecha' => '2019-10-29 03:22:00',
                'ejecutado' => 1,
            ),
            319 => 
            array (
                'id_alarmas' => 1185,
                'id_cotizacion' => 5,
                'precio' => 188.07,
                'inicio' => 186.26,
                'id_usuario' => 2017,
                'fecha' => '2019-10-29 15:42:00',
                'ejecutado' => 1,
            ),
            320 => 
            array (
                'id_alarmas' => 1186,
                'id_cotizacion' => 5,
                'precio' => 187.48,
                'inicio' => 187.49,
                'id_usuario' => 1958,
                'fecha' => '2019-10-29 17:34:00',
                'ejecutado' => 1,
            ),
            321 => 
            array (
                'id_alarmas' => 1187,
                'id_cotizacion' => 5,
                'precio' => 190.48,
                'inicio' => 187.49,
                'id_usuario' => 1958,
                'fecha' => '2019-10-29 17:34:00',
                'ejecutado' => 1,
            ),
            322 => 
            array (
                'id_alarmas' => 1188,
                'id_cotizacion' => 18,
                'precio' => 0.99,
                'inicio' => 0.9937,
                'id_usuario' => 1691,
                'fecha' => '2019-10-30 05:58:00',
                'ejecutado' => 1,
            ),
            323 => 
            array (
                'id_alarmas' => 1189,
                'id_cotizacion' => 12,
                'precio' => 1.311,
                'inicio' => 130.887,
                'id_usuario' => 1691,
                'fecha' => '2019-10-30 05:59:00',
                'ejecutado' => 1,
            ),
            324 => 
            array (
                'id_alarmas' => 1190,
                'id_cotizacion' => 6,
                'precio' => 109.0,
                'inicio' => 108.85,
                'id_usuario' => 1691,
                'fecha' => '2019-10-30 06:01:00',
                'ejecutado' => 1,
            ),
            325 => 
            array (
                'id_alarmas' => 1191,
                'id_cotizacion' => 17,
                'precio' => 1.115,
                'inicio' => 111.114,
                'id_usuario' => 1691,
                'fecha' => '2019-10-30 06:02:00',
                'ejecutado' => 1,
            ),
            326 => 
            array (
                'id_alarmas' => 1192,
                'id_cotizacion' => 17,
                'precio' => 111.366,
                'inicio' => 111.317,
                'id_usuario' => 1133,
                'fecha' => '2019-10-30 19:02:00',
                'ejecutado' => 1,
            ),
            327 => 
            array (
                'id_alarmas' => 1193,
                'id_cotizacion' => 18,
                'precio' => 0.98994,
                'inicio' => 0.99187,
                'id_usuario' => 1133,
                'fecha' => '2019-10-30 19:04:00',
                'ejecutado' => 1,
            ),
            328 => 
            array (
                'id_alarmas' => 1194,
                'id_cotizacion' => 15,
                'precio' => 140.8,
                'inicio' => 140.462,
                'id_usuario' => 1749,
                'fecha' => '2019-10-30 22:17:00',
                'ejecutado' => 1,
            ),
            329 => 
            array (
                'id_alarmas' => 1195,
                'id_cotizacion' => 3,
                'precio' => 8900.0,
                'inicio' => 9200.11,
                'id_usuario' => 1749,
                'fecha' => '2019-10-30 22:18:00',
                'ejecutado' => 1,
            ),
            330 => 
            array (
                'id_alarmas' => 1196,
                'id_cotizacion' => 3,
                'precio' => 9177.0,
                'inicio' => 9234.25,
                'id_usuario' => 1236,
                'fecha' => '2019-10-30 22:48:00',
                'ejecutado' => 1,
            ),
            331 => 
            array (
                'id_alarmas' => 1197,
                'id_cotizacion' => 3,
                'precio' => 9380.4,
                'inicio' => 9240.66,
                'id_usuario' => 1236,
                'fecha' => '2019-10-30 22:49:00',
                'ejecutado' => 1,
            ),
            332 => 
            array (
                'id_alarmas' => 1198,
                'id_cotizacion' => 3,
                'precio' => 9347.6,
                'inicio' => 9241.16,
                'id_usuario' => 1236,
                'fecha' => '2019-10-30 22:49:00',
                'ejecutado' => 1,
            ),
            333 => 
            array (
                'id_alarmas' => 1201,
                'id_cotizacion' => 3,
                'precio' => 9083.5,
                'inicio' => 9194.01,
                'id_usuario' => 1236,
                'fecha' => '2019-10-31 01:24:00',
                'ejecutado' => 1,
            ),
            334 => 
            array (
                'id_alarmas' => 1202,
                'id_cotizacion' => 9,
                'precio' => 27195.77,
                'inicio' => 27205.74,
                'id_usuario' => 1444,
                'fecha' => '2019-10-31 05:08:00',
                'ejecutado' => 1,
            ),
            335 => 
            array (
                'id_alarmas' => 1203,
                'id_cotizacion' => 9,
                'precio' => 27195.77,
                'inicio' => 27205.74,
                'id_usuario' => 1444,
                'fecha' => '2019-10-31 05:08:00',
                'ejecutado' => 1,
            ),
            336 => 
            array (
                'id_alarmas' => 1204,
                'id_cotizacion' => 9,
                'precio' => 27195.77,
                'inicio' => 27205.74,
                'id_usuario' => 1444,
                'fecha' => '2019-10-31 05:08:00',
                'ejecutado' => 1,
            ),
            337 => 
            array (
                'id_alarmas' => 1205,
                'id_cotizacion' => 7,
                'precio' => 202.131,
                'inicio' => 201.551,
                'id_usuario' => 1671,
                'fecha' => '2019-10-31 15:03:00',
                'ejecutado' => 1,
            ),
            338 => 
            array (
                'id_alarmas' => 1206,
                'id_cotizacion' => 3,
                'precio' => 9500.0,
                'inicio' => 9405.34,
                'id_usuario' => 1749,
                'fecha' => '2019-11-02 15:39:00',
                'ejecutado' => 1,
            ),
            339 => 
            array (
                'id_alarmas' => 1208,
                'id_cotizacion' => 6,
                'precio' => 108.35,
                'inicio' => 108.158,
                'id_usuario' => 1691,
                'fecha' => '2019-11-03 21:45:00',
                'ejecutado' => 1,
            ),
            340 => 
            array (
                'id_alarmas' => 1209,
                'id_cotizacion' => 18,
                'precio' => 0.9859,
                'inicio' => 0.98568,
                'id_usuario' => 1691,
                'fecha' => '2019-11-03 21:48:00',
                'ejecutado' => 1,
            ),
            341 => 
            array (
                'id_alarmas' => 1211,
                'id_cotizacion' => 12,
                'precio' => 13.144,
                'inicio' => 131.425,
                'id_usuario' => 1691,
                'fecha' => '2019-11-03 21:54:00',
                'ejecutado' => 1,
            ),
            342 => 
            array (
                'id_alarmas' => 1212,
                'id_cotizacion' => 13,
                'precio' => 0.6405,
                'inicio' => 0.64275,
                'id_usuario' => 1691,
                'fecha' => '2019-11-03 21:57:00',
                'ejecutado' => 1,
            ),
            343 => 
            array (
                'id_alarmas' => 1213,
                'id_cotizacion' => 10,
                'precio' => 1511.17,
                'inicio' => 1512.16,
                'id_usuario' => 2018,
                'fecha' => '2019-11-04 04:39:00',
                'ejecutado' => 1,
            ),
            344 => 
            array (
                'id_alarmas' => 1214,
                'id_cotizacion' => 10,
                'precio' => 1515.0,
                'inicio' => 1511.79,
                'id_usuario' => 1552,
                'fecha' => '2019-11-04 06:18:00',
                'ejecutado' => 0,
            ),
            345 => 
            array (
                'id_alarmas' => 1215,
                'id_cotizacion' => 12,
                'precio' => 131.529,
                'inicio' => 131.384,
                'id_usuario' => 954,
                'fecha' => '2019-11-04 06:35:00',
                'ejecutado' => 1,
            ),
            346 => 
            array (
                'id_alarmas' => 1216,
                'id_cotizacion' => 6,
                'precio' => 108.3,
                'inicio' => 108.388,
                'id_usuario' => 1691,
                'fecha' => '2019-11-04 08:40:00',
                'ejecutado' => 1,
            ),
            347 => 
            array (
                'id_alarmas' => 1217,
                'id_cotizacion' => 8,
                'precio' => 18.745,
                'inicio' => 187.033,
                'id_usuario' => 2036,
                'fecha' => '2019-11-04 14:16:00',
                'ejecutado' => 1,
            ),
            348 => 
            array (
                'id_alarmas' => 1218,
                'id_cotizacion' => 6,
                'precio' => 108.85,
                'inicio' => 108.784,
                'id_usuario' => 1691,
                'fecha' => '2019-11-05 03:30:00',
                'ejecutado' => 1,
            ),
            349 => 
            array (
                'id_alarmas' => 1219,
                'id_cotizacion' => 16,
                'precio' => 121.31,
                'inicio' => 121.201,
                'id_usuario' => 1749,
                'fecha' => '2019-11-05 08:58:00',
                'ejecutado' => 0,
            ),
            350 => 
            array (
                'id_alarmas' => 1220,
                'id_cotizacion' => 6,
                'precio' => 108.732,
                'inicio' => 108.885,
                'id_usuario' => 1236,
                'fecha' => '2019-11-07 01:04:00',
                'ejecutado' => 1,
            ),
            351 => 
            array (
                'id_alarmas' => 1221,
                'id_cotizacion' => 2,
                'precio' => 0.29963,
                'inicio' => 0.30154,
                'id_usuario' => 1236,
                'fecha' => '2019-11-07 02:12:00',
                'ejecutado' => 1,
            ),
            352 => 
            array (
                'id_alarmas' => 1222,
                'id_cotizacion' => 3,
                'precio' => 9260.0,
                'inicio' => 9321.65,
                'id_usuario' => 1236,
                'fecha' => '2019-11-07 04:48:00',
                'ejecutado' => 1,
            ),
            353 => 
            array (
                'id_alarmas' => 1223,
                'id_cotizacion' => 3,
                'precio' => 9232.4,
                'inicio' => 9296.72,
                'id_usuario' => 1236,
                'fecha' => '2019-11-07 05:50:00',
                'ejecutado' => 1,
            ),
            354 => 
            array (
                'id_alarmas' => 1224,
                'id_cotizacion' => 3,
                'precio' => 9240.0,
                'inicio' => 9302.33,
                'id_usuario' => 1236,
                'fecha' => '2019-11-07 06:13:00',
                'ejecutado' => 1,
            ),
            355 => 
            array (
                'id_alarmas' => 1225,
                'id_cotizacion' => 10,
                'precio' => 1485.83,
                'inicio' => 1483.9,
                'id_usuario' => 1292,
                'fecha' => '2019-11-07 11:57:00',
                'ejecutado' => 1,
            ),
            356 => 
            array (
                'id_alarmas' => 1226,
                'id_cotizacion' => 9,
                'precio' => 27560.0,
                'inicio' => 27597.84,
                'id_usuario' => 1749,
                'fecha' => '2019-11-07 12:03:00',
                'ejecutado' => 1,
            ),
            357 => 
            array (
                'id_alarmas' => 1227,
                'id_cotizacion' => 9,
                'precio' => 27630.0,
                'inicio' => 27604.34,
                'id_usuario' => 1749,
                'fecha' => '2019-11-07 12:14:00',
                'ejecutado' => 1,
            ),
            358 => 
            array (
                'id_alarmas' => 1228,
                'id_cotizacion' => 3,
                'precio' => 9150.3,
                'inicio' => 9218.83,
                'id_usuario' => 1236,
                'fecha' => '2019-11-07 19:25:00',
                'ejecutado' => 1,
            ),
            359 => 
            array (
                'id_alarmas' => 1230,
                'id_cotizacion' => 2,
                'precio' => 0.28801,
                'inicio' => 0.29143,
                'id_usuario' => 1236,
                'fecha' => '2019-11-07 19:38:00',
                'ejecutado' => 1,
            ),
            360 => 
            array (
                'id_alarmas' => 1232,
                'id_cotizacion' => 5,
                'precio' => 182.8,
                'inicio' => 186.33,
                'id_usuario' => 1236,
                'fecha' => '2019-11-07 20:06:00',
                'ejecutado' => 1,
            ),
            361 => 
            array (
                'id_alarmas' => 1233,
                'id_cotizacion' => 9,
                'precio' => 27621.1,
                'inicio' => 27709.24,
                'id_usuario' => 1236,
                'fecha' => '2019-11-08 00:05:00',
                'ejecutado' => 1,
            ),
            362 => 
            array (
                'id_alarmas' => 1234,
                'id_cotizacion' => 3,
                'precio' => 9044.0,
                'inicio' => 9198.21,
                'id_usuario' => 1236,
                'fecha' => '2019-11-08 00:07:00',
                'ejecutado' => 1,
            ),
            363 => 
            array (
                'id_alarmas' => 1235,
                'id_cotizacion' => 5,
                'precio' => 182.34,
                'inicio' => 183.95,
                'id_usuario' => 1236,
                'fecha' => '2019-11-09 00:14:00',
                'ejecutado' => 1,
            ),
            364 => 
            array (
                'id_alarmas' => 1236,
                'id_cotizacion' => 3,
                'precio' => 8527.0,
                'inicio' => 8810.99,
                'id_usuario' => 1236,
                'fecha' => '2019-11-09 02:13:00',
                'ejecutado' => 1,
            ),
            365 => 
            array (
                'id_alarmas' => 1237,
                'id_cotizacion' => 3,
                'precio' => 9000.15,
                'inicio' => 8869.15,
                'id_usuario' => 1562,
                'fecha' => '2019-11-09 14:15:00',
                'ejecutado' => 1,
            ),
            366 => 
            array (
                'id_alarmas' => 1238,
                'id_cotizacion' => 5,
                'precio' => 182.5,
                'inicio' => 183.46,
                'id_usuario' => 1236,
                'fecha' => '2019-11-09 16:26:00',
                'ejecutado' => 1,
            ),
            367 => 
            array (
                'id_alarmas' => 1239,
                'id_cotizacion' => 5,
                'precio' => 182.94,
                'inicio' => 184.32,
                'id_usuario' => 1236,
                'fecha' => '2019-11-09 19:30:00',
                'ejecutado' => 1,
            ),
            368 => 
            array (
                'id_alarmas' => 1240,
                'id_cotizacion' => 7,
                'precio' => 201.512,
                'inicio' => 201.981,
                'id_usuario' => 1584,
                'fecha' => '2019-11-10 18:15:00',
                'ejecutado' => 1,
            ),
            369 => 
            array (
                'id_alarmas' => 1241,
                'id_cotizacion' => 5,
                'precio' => 190.54,
                'inicio' => 190.54,
                'id_usuario' => 681,
                'fecha' => '2019-11-10 19:30:00',
                'ejecutado' => 0,
            ),
            370 => 
            array (
                'id_alarmas' => 1242,
                'id_cotizacion' => 2,
                'precio' => 0.28262,
                'inicio' => 0.28262,
                'id_usuario' => 681,
                'fecha' => '2019-11-10 19:31:00',
                'ejecutado' => 0,
            ),
            371 => 
            array (
                'id_alarmas' => 1243,
                'id_cotizacion' => 7,
                'precio' => 201.746,
                'inicio' => 201.981,
                'id_usuario' => 1590,
                'fecha' => '2019-11-10 20:57:00',
                'ejecutado' => 1,
            ),
            372 => 
            array (
                'id_alarmas' => 1244,
                'id_cotizacion' => 7,
                'precio' => 201.741,
                'inicio' => 201.981,
                'id_usuario' => 1590,
                'fecha' => '2019-11-10 20:58:00',
                'ejecutado' => 1,
            ),
            373 => 
            array (
                'id_alarmas' => 1245,
                'id_cotizacion' => 6,
                'precio' => 109.242,
                'inicio' => 109.242,
                'id_usuario' => 678,
                'fecha' => '2019-11-10 21:28:00',
                'ejecutado' => 0,
            ),
            374 => 
            array (
                'id_alarmas' => 1247,
                'id_cotizacion' => 17,
                'precio' => 1.103,
                'inicio' => 11.021,
                'id_usuario' => 1688,
                'fecha' => '2019-11-10 23:03:00',
                'ejecutado' => 1,
            ),
            375 => 
            array (
                'id_alarmas' => 1248,
                'id_cotizacion' => 17,
                'precio' => 1.104,
                'inicio' => 110.211,
                'id_usuario' => 1688,
                'fecha' => '2019-11-10 23:04:00',
                'ejecutado' => 1,
            ),
            376 => 
            array (
                'id_alarmas' => 1249,
                'id_cotizacion' => 17,
                'precio' => 1.104,
                'inicio' => 110.211,
                'id_usuario' => 1688,
                'fecha' => '2019-11-10 23:04:00',
                'ejecutado' => 1,
            ),
            377 => 
            array (
                'id_alarmas' => 1250,
                'id_cotizacion' => 10,
                'precio' => 1467.01,
                'inicio' => 1461.03,
                'id_usuario' => 1688,
                'fecha' => '2019-11-10 23:48:00',
                'ejecutado' => 1,
            ),
            378 => 
            array (
                'id_alarmas' => 1251,
                'id_cotizacion' => 2,
                'precio' => 0.2815,
                'inicio' => 0.28144,
                'id_usuario' => 853,
                'fecha' => '2019-11-11 00:39:00',
                'ejecutado' => 1,
            ),
            379 => 
            array (
                'id_alarmas' => 1255,
                'id_cotizacion' => 9,
                'precio' => 27603.2,
                'inicio' => 27603.74,
                'id_usuario' => 853,
                'fecha' => '2019-11-11 03:21:00',
                'ejecutado' => 1,
            ),
            380 => 
            array (
                'id_alarmas' => 1256,
                'id_cotizacion' => 8,
                'precio' => 186.573,
                'inicio' => 186.624,
                'id_usuario' => 853,
                'fecha' => '2019-11-11 03:35:00',
                'ejecutado' => 1,
            ),
            381 => 
            array (
                'id_alarmas' => 1262,
                'id_cotizacion' => 8,
                'precio' => 186.573,
                'inicio' => 186.624,
                'id_usuario' => 616,
                'fecha' => '2019-11-11 03:35:00',
                'ejecutado' => 1,
            ),
            382 => 
            array (
                'id_alarmas' => 1270,
                'id_cotizacion' => 9,
                'precio' => 27604.0,
                'inicio' => 27601.24,
                'id_usuario' => 616,
                'fecha' => '2019-11-11 03:37:00',
                'ejecutado' => 1,
            ),
            383 => 
            array (
                'id_alarmas' => 1271,
                'id_cotizacion' => 9,
                'precio' => 27604.0,
                'inicio' => 27601.24,
                'id_usuario' => 853,
                'fecha' => '2019-11-11 03:37:00',
                'ejecutado' => 1,
            ),
            384 => 
            array (
                'id_alarmas' => 1274,
                'id_cotizacion' => 2,
                'precio' => 0.2815,
                'inicio' => 0.28106,
                'id_usuario' => 616,
                'fecha' => '2019-11-11 03:42:00',
                'ejecutado' => 0,
            ),
            385 => 
            array (
                'id_alarmas' => 1275,
                'id_cotizacion' => 18,
                'precio' => 0.9939,
                'inicio' => 0.99493,
                'id_usuario' => 1691,
                'fecha' => '2019-11-11 10:54:00',
                'ejecutado' => 1,
            ),
            386 => 
            array (
                'id_alarmas' => 1277,
                'id_cotizacion' => 13,
                'precio' => 0.63809,
                'inicio' => 0.63651,
                'id_usuario' => 1691,
                'fecha' => '2019-11-11 10:57:00',
                'ejecutado' => 1,
            ),
            387 => 
            array (
                'id_alarmas' => 1278,
                'id_cotizacion' => 9,
                'precio' => 27696.53,
                'inicio' => 27676.84,
                'id_usuario' => 739,
                'fecha' => '2019-11-11 17:31:00',
                'ejecutado' => 1,
            ),
            388 => 
            array (
                'id_alarmas' => 1279,
                'id_cotizacion' => 8,
                'precio' => 18.735,
                'inicio' => 187.519,
                'id_usuario' => 1292,
                'fecha' => '2019-11-11 18:46:00',
                'ejecutado' => 1,
            ),
            389 => 
            array (
                'id_alarmas' => 1280,
                'id_cotizacion' => 3,
                'precio' => 7895.0,
                'inicio' => 8747.42,
                'id_usuario' => 1810,
                'fecha' => '2019-11-11 23:59:00',
                'ejecutado' => 1,
            ),
            390 => 
            array (
                'id_alarmas' => 1284,
                'id_cotizacion' => 7,
                'precio' => 20.213,
                'inicio' => 202.948,
                'id_usuario' => 1691,
                'fecha' => '2019-11-12 06:26:00',
                'ejecutado' => 1,
            ),
            391 => 
            array (
                'id_alarmas' => 1289,
                'id_cotizacion' => 6,
                'precio' => 108.883,
                'inicio' => 108.983,
                'id_usuario' => 1236,
                'fecha' => '2019-11-12 23:39:00',
                'ejecutado' => 1,
            ),
            392 => 
            array (
                'id_alarmas' => 1290,
                'id_cotizacion' => 17,
                'precio' => 109.897,
                'inicio' => 11.013,
                'id_usuario' => 1236,
                'fecha' => '2019-11-12 23:40:00',
                'ejecutado' => 1,
            ),
            393 => 
            array (
                'id_alarmas' => 1293,
                'id_cotizacion' => 8,
                'precio' => 187.755,
                'inicio' => 187.696,
                'id_usuario' => 100,
                'fecha' => '2019-11-13 06:24:00',
                'ejecutado' => 1,
            ),
            394 => 
            array (
                'id_alarmas' => 1296,
                'id_cotizacion' => 2,
                'precio' => 0.269,
                'inicio' => 0.274,
                'id_usuario' => 100,
                'fecha' => '2019-11-13 10:25:00',
                'ejecutado' => 1,
            ),
            395 => 
            array (
                'id_alarmas' => 1297,
                'id_cotizacion' => 9,
                'precio' => 27670.63,
                'inicio' => 27755.84,
                'id_usuario' => 2019,
                'fecha' => '2019-11-13 17:57:00',
                'ejecutado' => 1,
            ),
            396 => 
            array (
                'id_alarmas' => 1298,
                'id_cotizacion' => 9,
                'precio' => 27670.63,
                'inicio' => 27752.84,
                'id_usuario' => 2019,
                'fecha' => '2019-11-13 18:30:00',
                'ejecutado' => 1,
            ),
            397 => 
            array (
                'id_alarmas' => 1299,
                'id_cotizacion' => 12,
                'precio' => 132.638,
                'inicio' => 132.564,
                'id_usuario' => 2050,
                'fecha' => '2019-11-14 02:33:00',
                'ejecutado' => 1,
            ),
            398 => 
            array (
                'id_alarmas' => 1300,
                'id_cotizacion' => 17,
                'precio' => 109.875,
                'inicio' => 110.021,
                'id_usuario' => 2050,
                'fecha' => '2019-11-14 02:34:00',
                'ejecutado' => 1,
            ),
            399 => 
            array (
                'id_alarmas' => 1301,
                'id_cotizacion' => 10,
                'precio' => 1469.0,
                'inicio' => 1464.83,
                'id_usuario' => 100,
                'fecha' => '2019-11-14 05:26:00',
                'ejecutado' => 1,
            ),
            400 => 
            array (
                'id_alarmas' => 1307,
                'id_cotizacion' => 4,
                'precio' => 36.37,
                'inicio' => 36.62,
                'id_usuario' => 853,
                'fecha' => '2019-11-14 14:57:00',
                'ejecutado' => 1,
            ),
            401 => 
            array (
                'id_alarmas' => 1308,
                'id_cotizacion' => 6,
                'precio' => 108.6,
                'inicio' => 108.448,
                'id_usuario' => 1985,
                'fecha' => '2019-11-14 21:27:00',
                'ejecutado' => 1,
            ),
            402 => 
            array (
                'id_alarmas' => 1309,
                'id_cotizacion' => 6,
                'precio' => 108.65,
                'inicio' => 108.448,
                'id_usuario' => 1985,
                'fecha' => '2019-11-14 21:27:00',
                'ejecutado' => 1,
            ),
            403 => 
            array (
                'id_alarmas' => 1310,
                'id_cotizacion' => 17,
                'precio' => 11.013,
                'inicio' => 110.238,
                'id_usuario' => 551,
                'fecha' => '2019-11-15 01:20:00',
                'ejecutado' => 1,
            ),
            404 => 
            array (
                'id_alarmas' => 1311,
                'id_cotizacion' => 12,
                'precio' => 132.513,
                'inicio' => 132.392,
                'id_usuario' => 551,
                'fecha' => '2019-11-15 01:22:00',
                'ejecutado' => 1,
            ),
            405 => 
            array (
                'id_alarmas' => 1312,
                'id_cotizacion' => 12,
                'precio' => 1.323,
                'inicio' => 132.232,
                'id_usuario' => 1691,
                'fecha' => '2019-11-15 04:22:00',
                'ejecutado' => 1,
            ),
            406 => 
            array (
                'id_alarmas' => 1313,
                'id_cotizacion' => 7,
                'precio' => 201.354,
                'inicio' => 201.594,
                'id_usuario' => 321,
                'fecha' => '2019-11-15 11:45:00',
                'ejecutado' => 1,
            ),
            407 => 
            array (
                'id_alarmas' => 1314,
                'id_cotizacion' => 7,
                'precio' => 201.886,
                'inicio' => 201.594,
                'id_usuario' => 321,
                'fecha' => '2019-11-15 11:45:00',
                'ejecutado' => 1,
            ),
            408 => 
            array (
                'id_alarmas' => 1315,
                'id_cotizacion' => 5,
                'precio' => 182.0,
                'inicio' => 184.1,
                'id_usuario' => 321,
                'fecha' => '2019-11-15 11:47:00',
                'ejecutado' => 1,
            ),
            409 => 
            array (
                'id_alarmas' => 1316,
                'id_cotizacion' => 5,
                'precio' => 182.31,
                'inicio' => 183.44,
                'id_usuario' => 853,
                'fecha' => '2019-11-17 00:34:00',
                'ejecutado' => 1,
            ),
            410 => 
            array (
                'id_alarmas' => 1317,
                'id_cotizacion' => 10,
                'precio' => 1472.95,
                'inicio' => 1469.06,
                'id_usuario' => 576,
                'fecha' => '2019-11-17 23:42:00',
                'ejecutado' => 1,
            ),
            411 => 
            array (
                'id_alarmas' => 1319,
                'id_cotizacion' => 7,
                'precio' => 2.003,
                'inicio' => 201.857,
                'id_usuario' => 1786,
                'fecha' => '2019-11-17 23:51:00',
                'ejecutado' => 1,
            ),
            412 => 
            array (
                'id_alarmas' => 1320,
                'id_cotizacion' => 12,
                'precio' => 132.206,
                'inicio' => 132.162,
                'id_usuario' => 1133,
                'fecha' => '2019-11-18 07:48:00',
                'ejecutado' => 1,
            ),
            413 => 
            array (
                'id_alarmas' => 1321,
                'id_cotizacion' => 12,
                'precio' => 132.168,
                'inicio' => 13.216,
                'id_usuario' => 1133,
                'fecha' => '2019-11-18 07:48:00',
                'ejecutado' => 1,
            ),
            414 => 
            array (
                'id_alarmas' => 1322,
                'id_cotizacion' => 6,
                'precio' => 109.242,
                'inicio' => 108.915,
                'id_usuario' => 1133,
                'fecha' => '2019-11-18 07:59:00',
                'ejecutado' => 1,
            ),
            415 => 
            array (
                'id_alarmas' => 1323,
                'id_cotizacion' => 12,
                'precio' => 132.206,
                'inicio' => 132.193,
                'id_usuario' => 1133,
                'fecha' => '2019-11-18 08:01:00',
                'ejecutado' => 1,
            ),
            416 => 
            array (
                'id_alarmas' => 1324,
                'id_cotizacion' => 12,
                'precio' => 132.125,
                'inicio' => 132.156,
                'id_usuario' => 1133,
                'fecha' => '2019-11-18 08:07:00',
                'ejecutado' => 1,
            ),
            417 => 
            array (
                'id_alarmas' => 1325,
                'id_cotizacion' => 10,
                'precio' => 1469.26,
                'inicio' => 1471.49,
                'id_usuario' => 853,
                'fecha' => '2019-11-18 19:54:00',
                'ejecutado' => 1,
            ),
            418 => 
            array (
                'id_alarmas' => 1326,
                'id_cotizacion' => 17,
                'precio' => 110.566,
                'inicio' => 110.799,
                'id_usuario' => 1133,
                'fecha' => '2019-11-19 07:16:00',
                'ejecutado' => 1,
            ),
            419 => 
            array (
                'id_alarmas' => 1327,
                'id_cotizacion' => 6,
                'precio' => 108.71,
                'inicio' => 108.639,
                'id_usuario' => 1133,
                'fecha' => '2019-11-19 07:17:00',
                'ejecutado' => 1,
            ),
            420 => 
            array (
                'id_alarmas' => 1328,
                'id_cotizacion' => 12,
                'precio' => 132.085,
                'inicio' => 132.064,
                'id_usuario' => 1133,
                'fecha' => '2019-11-19 07:20:00',
                'ejecutado' => 1,
            ),
            421 => 
            array (
                'id_alarmas' => 1329,
                'id_cotizacion' => 17,
                'precio' => 110.633,
                'inicio' => 110.697,
                'id_usuario' => 1691,
                'fecha' => '2019-11-19 10:19:00',
                'ejecutado' => 1,
            ),
            422 => 
            array (
                'id_alarmas' => 1330,
                'id_cotizacion' => 3,
                'precio' => 8000.0,
                'inicio' => 8180.79,
                'id_usuario' => 286,
                'fecha' => '2019-11-19 10:37:00',
                'ejecutado' => 1,
            ),
            423 => 
            array (
                'id_alarmas' => 1331,
                'id_cotizacion' => 10,
                'precio' => 1459.0,
                'inicio' => 1465.69,
                'id_usuario' => 286,
                'fecha' => '2019-11-19 10:38:00',
                'ejecutado' => 1,
            ),
            424 => 
            array (
                'id_alarmas' => 1335,
                'id_cotizacion' => 10,
                'precio' => 1460.47,
                'inicio' => 1467.48,
                'id_usuario' => 1236,
                'fecha' => '2019-11-19 11:57:00',
                'ejecutado' => 1,
            ),
            425 => 
            array (
                'id_alarmas' => 1336,
                'id_cotizacion' => 7,
                'precio' => 201.848,
                'inicio' => 201.771,
                'id_usuario' => 853,
                'fecha' => '2019-11-19 15:23:00',
                'ejecutado' => 1,
            ),
            426 => 
            array (
                'id_alarmas' => 1338,
                'id_cotizacion' => 8,
                'precio' => 189.624,
                'inicio' => 189.628,
                'id_usuario' => 853,
                'fecha' => '2019-11-19 15:45:00',
                'ejecutado' => 1,
            ),
            427 => 
            array (
                'id_alarmas' => 1339,
                'id_cotizacion' => 2,
                'precio' => 0.25163,
                'inicio' => 0.25138,
                'id_usuario' => 2064,
                'fecha' => '2019-11-19 16:55:00',
                'ejecutado' => 1,
            ),
            428 => 
            array (
                'id_alarmas' => 1340,
                'id_cotizacion' => 2,
                'precio' => 0.25163,
                'inicio' => 0.25138,
                'id_usuario' => 2064,
                'fecha' => '2019-11-19 16:55:00',
                'ejecutado' => 1,
            ),
            429 => 
            array (
                'id_alarmas' => 1341,
                'id_cotizacion' => 2,
                'precio' => 0.25163,
                'inicio' => 0.25143,
                'id_usuario' => 2064,
                'fecha' => '2019-11-19 16:56:00',
                'ejecutado' => 1,
            ),
            430 => 
            array (
                'id_alarmas' => 1342,
                'id_cotizacion' => 3,
                'precio' => 8549.06,
                'inicio' => 8149.07,
                'id_usuario' => 2064,
                'fecha' => '2019-11-19 16:56:00',
                'ejecutado' => 0,
            ),
            431 => 
            array (
                'id_alarmas' => 1343,
                'id_cotizacion' => 9,
                'precio' => 27960.9,
                'inicio' => 27926.84,
                'id_usuario' => 853,
                'fecha' => '2019-11-19 17:04:00',
                'ejecutado' => 1,
            ),
            432 => 
            array (
                'id_alarmas' => 1344,
                'id_cotizacion' => 8,
                'precio' => 189.616,
                'inicio' => 189.265,
                'id_usuario' => 853,
                'fecha' => '2019-11-19 17:10:00',
                'ejecutado' => 1,
            ),
            433 => 
            array (
                'id_alarmas' => 1345,
                'id_cotizacion' => 10,
                'precio' => 1471.35,
                'inicio' => 1473.53,
                'id_usuario' => 853,
                'fecha' => '2019-11-19 19:14:00',
                'ejecutado' => 1,
            ),
            434 => 
            array (
                'id_alarmas' => 1346,
                'id_cotizacion' => 3,
                'precio' => 8200.0,
                'inicio' => 8125.03,
                'id_usuario' => 576,
                'fecha' => '2019-11-19 20:25:00',
                'ejecutado' => 1,
            ),
            435 => 
            array (
                'id_alarmas' => 1347,
                'id_cotizacion' => 10,
                'precio' => 1472.6,
                'inicio' => 1472.47,
                'id_usuario' => 576,
                'fecha' => '2019-11-19 20:25:00',
                'ejecutado' => 1,
            ),
            436 => 
            array (
                'id_alarmas' => 1348,
                'id_cotizacion' => 10,
                'precio' => 1473.4,
                'inicio' => 1472.41,
                'id_usuario' => 576,
                'fecha' => '2019-11-19 20:57:00',
                'ejecutado' => 1,
            ),
            437 => 
            array (
                'id_alarmas' => 1349,
                'id_cotizacion' => 2,
                'precio' => 0.251,
                'inicio' => 0.25507,
                'id_usuario' => 100,
                'fecha' => '2019-11-20 04:34:00',
                'ejecutado' => 1,
            ),
            438 => 
            array (
                'id_alarmas' => 1351,
                'id_cotizacion' => 6,
                'precio' => 108.644,
                'inicio' => 108.645,
                'id_usuario' => 2024,
                'fecha' => '2019-11-20 15:09:00',
                'ejecutado' => 1,
            ),
            439 => 
            array (
                'id_alarmas' => 1352,
                'id_cotizacion' => 7,
                'precio' => 201.461,
                'inicio' => 201.637,
                'id_usuario' => 853,
                'fecha' => '2019-11-20 18:36:00',
                'ejecutado' => 1,
            ),
            440 => 
            array (
                'id_alarmas' => 1353,
                'id_cotizacion' => 9,
                'precio' => 27835.0,
                'inicio' => 27789.34,
                'id_usuario' => 1552,
                'fecha' => '2019-11-20 20:00:00',
                'ejecutado' => 1,
            ),
            441 => 
            array (
                'id_alarmas' => 1354,
                'id_cotizacion' => 10,
                'precio' => 1470.76,
                'inicio' => 1471.65,
                'id_usuario' => 1918,
                'fecha' => '2019-11-20 21:35:00',
                'ejecutado' => 1,
            ),
            442 => 
            array (
                'id_alarmas' => 1355,
                'id_cotizacion' => 10,
                'precio' => 1473.74,
                'inicio' => 1471.83,
                'id_usuario' => 1918,
                'fecha' => '2019-11-20 21:35:00',
                'ejecutado' => 1,
            ),
            443 => 
            array (
                'id_alarmas' => 1356,
                'id_cotizacion' => 10,
                'precio' => 1470.85,
                'inicio' => 1471.33,
                'id_usuario' => 576,
                'fecha' => '2019-11-20 23:04:00',
                'ejecutado' => 1,
            ),
            444 => 
            array (
                'id_alarmas' => 1357,
                'id_cotizacion' => 6,
                'precio' => 108.6,
                'inicio' => 108.476,
                'id_usuario' => 380,
                'fecha' => '2019-11-20 23:20:00',
                'ejecutado' => 1,
            ),
            445 => 
            array (
                'id_alarmas' => 1361,
                'id_cotizacion' => 3,
                'precio' => 8000.0,
                'inicio' => 7963.05,
                'id_usuario' => 576,
                'fecha' => '2019-11-21 10:55:00',
                'ejecutado' => 0,
            ),
            446 => 
            array (
                'id_alarmas' => 1362,
                'id_cotizacion' => 10,
                'precio' => 1464.14,
                'inicio' => 1464.68,
                'id_usuario' => 576,
                'fecha' => '2019-11-21 21:34:00',
                'ejecutado' => 1,
            ),
            447 => 
            array (
                'id_alarmas' => 1363,
                'id_cotizacion' => 10,
                'precio' => 1463.15,
                'inicio' => 1463.94,
                'id_usuario' => 576,
                'fecha' => '2019-11-22 00:39:00',
                'ejecutado' => 1,
            ),
            448 => 
            array (
                'id_alarmas' => 1365,
                'id_cotizacion' => 12,
                'precio' => 13.264,
                'inicio' => 13.274,
                'id_usuario' => 1810,
                'fecha' => '2019-11-22 01:03:00',
                'ejecutado' => 1,
            ),
            449 => 
            array (
                'id_alarmas' => 1366,
                'id_cotizacion' => 17,
                'precio' => 11.055,
                'inicio' => 110.631,
                'id_usuario' => 1810,
                'fecha' => '2019-11-22 01:05:00',
                'ejecutado' => 1,
            ),
            450 => 
            array (
                'id_alarmas' => 1367,
                'id_cotizacion' => 10,
                'precio' => 1464.1,
                'inicio' => 1464.96,
                'id_usuario' => 576,
                'fecha' => '2019-11-22 01:07:00',
                'ejecutado' => 1,
            ),
            451 => 
            array (
                'id_alarmas' => 1369,
                'id_cotizacion' => 9,
                'precio' => 28130.7,
                'inicio' => 27819.24,
                'id_usuario' => 1236,
                'fecha' => '2019-11-22 02:13:00',
                'ejecutado' => 1,
            ),
            452 => 
            array (
                'id_alarmas' => 1370,
                'id_cotizacion' => 9,
                'precio' => 27847.8,
                'inicio' => 27818.24,
                'id_usuario' => 1236,
                'fecha' => '2019-11-22 02:14:00',
                'ejecutado' => 1,
            ),
            453 => 
            array (
                'id_alarmas' => 1371,
                'id_cotizacion' => 3,
                'precio' => 7530.35,
                'inicio' => 7678.44,
                'id_usuario' => 576,
                'fecha' => '2019-11-22 02:51:00',
                'ejecutado' => 1,
            ),
            454 => 
            array (
                'id_alarmas' => 1372,
                'id_cotizacion' => 5,
                'precio' => 158.0,
                'inicio' => 161.54,
                'id_usuario' => 100,
                'fecha' => '2019-11-22 05:07:00',
                'ejecutado' => 1,
            ),
            455 => 
            array (
                'id_alarmas' => 1373,
                'id_cotizacion' => 10,
                'precio' => 1463.1,
                'inicio' => 1465.87,
                'id_usuario' => 576,
                'fecha' => '2019-11-22 16:17:00',
                'ejecutado' => 1,
            ),
            456 => 
            array (
                'id_alarmas' => 1374,
                'id_cotizacion' => 8,
                'precio' => 189.292,
                'inicio' => 189.059,
                'id_usuario' => 1236,
                'fecha' => '2019-11-22 16:34:00',
                'ejecutado' => 1,
            ),
            457 => 
            array (
                'id_alarmas' => 1375,
                'id_cotizacion' => 17,
                'precio' => 11.002,
                'inicio' => 110.313,
                'id_usuario' => 1236,
                'fecha' => '2019-11-22 16:41:00',
                'ejecutado' => 1,
            ),
            458 => 
            array (
                'id_alarmas' => 1377,
                'id_cotizacion' => 15,
                'precio' => 139.5,
                'inicio' => 139.475,
                'id_usuario' => 1236,
                'fecha' => '2019-11-22 17:10:00',
                'ejecutado' => 1,
            ),
            459 => 
            array (
                'id_alarmas' => 1378,
                'id_cotizacion' => 10,
                'precio' => 1460.7,
                'inicio' => 1463.37,
                'id_usuario' => 576,
                'fecha' => '2019-11-22 17:45:00',
                'ejecutado' => 1,
            ),
            460 => 
            array (
                'id_alarmas' => 1379,
                'id_cotizacion' => 3,
                'precio' => 7259.9,
                'inicio' => 7190.11,
                'id_usuario' => 576,
                'fecha' => '2019-11-22 17:45:00',
                'ejecutado' => 1,
            ),
            461 => 
            array (
                'id_alarmas' => 1380,
                'id_cotizacion' => 10,
                'precio' => 1463.0,
                'inicio' => 1463.01,
                'id_usuario' => 2117,
                'fecha' => '2019-11-22 20:33:00',
                'ejecutado' => 1,
            ),
            462 => 
            array (
                'id_alarmas' => 1381,
                'id_cotizacion' => 10,
                'precio' => 1463.0,
                'inicio' => 1463.24,
                'id_usuario' => 2117,
                'fecha' => '2019-11-22 20:35:00',
                'ejecutado' => 1,
            ),
            463 => 
            array (
                'id_alarmas' => 1384,
                'id_cotizacion' => 2,
                'precio' => 0.23187,
                'inicio' => 0.23493,
                'id_usuario' => 453,
                'fecha' => '2019-11-22 21:22:00',
                'ejecutado' => 1,
            ),
            464 => 
            array (
                'id_alarmas' => 1385,
                'id_cotizacion' => 3,
                'precio' => 7282.7,
                'inicio' => 7388.13,
                'id_usuario' => 453,
                'fecha' => '2019-11-22 21:23:00',
                'ejecutado' => 1,
            ),
            465 => 
            array (
                'id_alarmas' => 1386,
                'id_cotizacion' => 5,
                'precio' => 154.3,
                'inicio' => 154.28,
                'id_usuario' => 453,
                'fecha' => '2019-11-22 21:24:00',
                'ejecutado' => 1,
            ),
            466 => 
            array (
                'id_alarmas' => 1387,
                'id_cotizacion' => 5,
                'precio' => 154.0,
                'inicio' => 154.09,
                'id_usuario' => 453,
                'fecha' => '2019-11-22 21:24:00',
                'ejecutado' => 1,
            ),
            467 => 
            array (
                'id_alarmas' => 1388,
                'id_cotizacion' => 5,
                'precio' => 150.9,
                'inicio' => 154.25,
                'id_usuario' => 453,
                'fecha' => '2019-11-22 21:44:00',
                'ejecutado' => 1,
            ),
            468 => 
            array (
                'id_alarmas' => 1389,
                'id_cotizacion' => 3,
                'precio' => 7463.85,
                'inicio' => 7308.14,
                'id_usuario' => 576,
                'fecha' => '2019-11-22 22:57:00',
                'ejecutado' => 1,
            ),
            469 => 
            array (
                'id_alarmas' => 1390,
                'id_cotizacion' => 3,
                'precio' => 7258.2,
                'inicio' => 7289.81,
                'id_usuario' => 1236,
                'fecha' => '2019-11-23 02:44:00',
                'ejecutado' => 1,
            ),
            470 => 
            array (
                'id_alarmas' => 1391,
                'id_cotizacion' => 2,
                'precio' => 0.226,
                'inicio' => 0.23168,
                'id_usuario' => 1236,
                'fecha' => '2019-11-23 04:57:00',
                'ejecutado' => 1,
            ),
            471 => 
            array (
                'id_alarmas' => 1392,
                'id_cotizacion' => 3,
                'precio' => 7100.0,
                'inicio' => 7268.99,
                'id_usuario' => 1236,
                'fecha' => '2019-11-23 04:58:00',
                'ejecutado' => 1,
            ),
            472 => 
            array (
                'id_alarmas' => 1393,
                'id_cotizacion' => 3,
                'precio' => 7300.0,
                'inicio' => 7089.59,
                'id_usuario' => 2079,
                'fecha' => '2019-11-24 14:52:00',
                'ejecutado' => 1,
            ),
            473 => 
            array (
                'id_alarmas' => 1394,
                'id_cotizacion' => 5,
                'precio' => 146.64,
                'inicio' => 146.66,
                'id_usuario' => 2079,
                'fecha' => '2019-11-24 14:53:00',
                'ejecutado' => 1,
            ),
            474 => 
            array (
                'id_alarmas' => 1398,
                'id_cotizacion' => 17,
                'precio' => 110.153,
                'inicio' => 110.227,
                'id_usuario' => 1236,
                'fecha' => '2019-11-25 01:24:00',
                'ejecutado' => 1,
            ),
            475 => 
            array (
                'id_alarmas' => 1401,
                'id_cotizacion' => 15,
                'precio' => 139.854,
                'inicio' => 139.83,
                'id_usuario' => 1236,
                'fecha' => '2019-11-25 02:36:00',
                'ejecutado' => 1,
            ),
            476 => 
            array (
                'id_alarmas' => 1402,
                'id_cotizacion' => 9,
                'precio' => 28016.0,
                'inicio' => 27967.24,
                'id_usuario' => 1236,
                'fecha' => '2019-11-25 03:05:00',
                'ejecutado' => 1,
            ),
            477 => 
            array (
                'id_alarmas' => 1404,
                'id_cotizacion' => 15,
                'precio' => 140.106,
                'inicio' => 139.872,
                'id_usuario' => 1236,
                'fecha' => '2019-11-25 06:09:00',
                'ejecutado' => 1,
            ),
            478 => 
            array (
                'id_alarmas' => 1408,
                'id_cotizacion' => 15,
                'precio' => 140.203,
                'inicio' => 140.207,
                'id_usuario' => 1236,
                'fecha' => '2019-11-25 09:19:00',
                'ejecutado' => 1,
            ),
            479 => 
            array (
                'id_alarmas' => 1409,
                'id_cotizacion' => 18,
                'precio' => 0.9963,
                'inicio' => 0.9973,
                'id_usuario' => 1236,
                'fecha' => '2019-11-25 09:29:00',
                'ejecutado' => 1,
            ),
            480 => 
            array (
                'id_alarmas' => 1410,
                'id_cotizacion' => 17,
                'precio' => 110.216,
                'inicio' => 11.015,
                'id_usuario' => 1236,
                'fecha' => '2019-11-25 10:08:00',
                'ejecutado' => 1,
            ),
            481 => 
            array (
                'id_alarmas' => 1415,
                'id_cotizacion' => 12,
                'precio' => 1.332,
                'inicio' => 133.024,
                'id_usuario' => 2142,
                'fecha' => '2019-11-25 10:33:00',
                'ejecutado' => 1,
            ),
            482 => 
            array (
                'id_alarmas' => 1418,
                'id_cotizacion' => 12,
                'precio' => 13.282,
                'inicio' => 132.996,
                'id_usuario' => 1952,
                'fecha' => '2019-11-25 10:46:00',
                'ejecutado' => 1,
            ),
            483 => 
            array (
                'id_alarmas' => 1419,
                'id_cotizacion' => 6,
                'precio' => 108.95,
                'inicio' => 108.882,
                'id_usuario' => 1584,
                'fecha' => '2019-11-25 12:32:00',
                'ejecutado' => 1,
            ),
            484 => 
            array (
                'id_alarmas' => 1420,
                'id_cotizacion' => 5,
                'precio' => 147.31,
                'inicio' => 150.06,
                'id_usuario' => 853,
                'fecha' => '2019-11-25 14:18:00',
                'ejecutado' => 1,
            ),
            485 => 
            array (
                'id_alarmas' => 1421,
                'id_cotizacion' => 5,
                'precio' => 147.31,
                'inicio' => 150.06,
                'id_usuario' => 853,
                'fecha' => '2019-11-25 14:18:00',
                'ejecutado' => 1,
            ),
            486 => 
            array (
                'id_alarmas' => 1422,
                'id_cotizacion' => 5,
                'precio' => 148.83,
                'inicio' => 150.81,
                'id_usuario' => 853,
                'fecha' => '2019-11-25 15:49:00',
                'ejecutado' => 1,
            ),
            487 => 
            array (
                'id_alarmas' => 1423,
                'id_cotizacion' => 3,
                'precio' => 7216.2,
                'inicio' => 7331.06,
                'id_usuario' => 853,
                'fecha' => '2019-11-25 15:51:00',
                'ejecutado' => 1,
            ),
            488 => 
            array (
                'id_alarmas' => 1424,
                'id_cotizacion' => 2,
                'precio' => 0.21879,
                'inicio' => 0.22435,
                'id_usuario' => 853,
                'fecha' => '2019-11-25 15:52:00',
                'ejecutado' => 1,
            ),
            489 => 
            array (
                'id_alarmas' => 1425,
                'id_cotizacion' => 10,
                'precio' => 1457.27,
                'inicio' => 1457.97,
                'id_usuario' => 1918,
                'fecha' => '2019-11-25 17:33:00',
                'ejecutado' => 1,
            ),
            490 => 
            array (
                'id_alarmas' => 1426,
                'id_cotizacion' => 10,
                'precio' => 1457.27,
                'inicio' => 1458.0,
                'id_usuario' => 1918,
                'fecha' => '2019-11-25 17:33:00',
                'ejecutado' => 1,
            ),
            491 => 
            array (
                'id_alarmas' => 1428,
                'id_cotizacion' => 10,
                'precio' => 1460.0,
                'inicio' => 1457.89,
                'id_usuario' => 1918,
                'fecha' => '2019-11-25 17:34:00',
                'ejecutado' => 1,
            ),
            492 => 
            array (
                'id_alarmas' => 1429,
                'id_cotizacion' => 10,
                'precio' => 1460.5,
                'inicio' => 1457.69,
                'id_usuario' => 1918,
                'fecha' => '2019-11-25 17:34:00',
                'ejecutado' => 1,
            ),
            493 => 
            array (
                'id_alarmas' => 1430,
                'id_cotizacion' => 10,
                'precio' => 1456.08,
                'inicio' => 1456.73,
                'id_usuario' => 1918,
                'fecha' => '2019-11-25 17:54:00',
                'ejecutado' => 1,
            ),
            494 => 
            array (
                'id_alarmas' => 1431,
                'id_cotizacion' => 7,
                'precio' => 20.182,
                'inicio' => 201.493,
                'id_usuario' => 1491,
                'fecha' => '2019-11-25 17:55:00',
                'ejecutado' => 1,
            ),
            495 => 
            array (
                'id_alarmas' => 1432,
                'id_cotizacion' => 8,
                'precio' => 190.663,
                'inicio' => 190.343,
                'id_usuario' => 1491,
                'fecha' => '2019-11-25 17:56:00',
                'ejecutado' => 1,
            ),
            496 => 
            array (
                'id_alarmas' => 1433,
                'id_cotizacion' => 10,
                'precio' => 1460.0,
                'inicio' => 1456.57,
                'id_usuario' => 1491,
                'fecha' => '2019-11-25 18:11:00',
                'ejecutado' => 1,
            ),
            497 => 
            array (
                'id_alarmas' => 1434,
                'id_cotizacion' => 17,
                'precio' => 10.996,
                'inicio' => 110.091,
                'id_usuario' => 1491,
                'fecha' => '2019-11-25 19:38:00',
                'ejecutado' => 1,
            ),
            498 => 
            array (
                'id_alarmas' => 1435,
                'id_cotizacion' => 10,
                'precio' => 1454.72,
                'inicio' => 1455.22,
                'id_usuario' => 576,
                'fecha' => '2019-11-25 20:03:00',
                'ejecutado' => 1,
            ),
            499 => 
            array (
                'id_alarmas' => 1436,
                'id_cotizacion' => 5,
                'precio' => 149.1,
                'inicio' => 149.11,
                'id_usuario' => 2159,
                'fecha' => '2019-11-25 22:09:00',
                'ejecutado' => 1,
            ),
        ));
        \DB::table('alarmas')->insert(array (
            0 => 
            array (
                'id_alarmas' => 1438,
                'id_cotizacion' => 5,
                'precio' => 149.1,
                'inicio' => 149.09,
                'id_usuario' => 2159,
                'fecha' => '2019-11-25 22:09:00',
                'ejecutado' => 1,
            ),
            1 => 
            array (
                'id_alarmas' => 1439,
                'id_cotizacion' => 5,
                'precio' => 149.1,
                'inicio' => 149.09,
                'id_usuario' => 2159,
                'fecha' => '2019-11-25 22:09:00',
                'ejecutado' => 1,
            ),
            2 => 
            array (
                'id_alarmas' => 1440,
                'id_cotizacion' => 5,
                'precio' => 149.1,
                'inicio' => 149.09,
                'id_usuario' => 2159,
                'fecha' => '2019-11-25 22:09:00',
                'ejecutado' => 1,
            ),
            3 => 
            array (
                'id_alarmas' => 1441,
                'id_cotizacion' => 5,
                'precio' => 149.1,
                'inicio' => 149.04,
                'id_usuario' => 2159,
                'fecha' => '2019-11-25 22:09:00',
                'ejecutado' => 1,
            ),
            4 => 
            array (
                'id_alarmas' => 1444,
                'id_cotizacion' => 6,
                'precio' => 108.761,
                'inicio' => 108.941,
                'id_usuario' => 1545,
                'fecha' => '2019-11-25 22:31:00',
                'ejecutado' => 1,
            ),
            5 => 
            array (
                'id_alarmas' => 1445,
                'id_cotizacion' => 18,
                'precio' => 0.99661,
                'inicio' => 0.99668,
                'id_usuario' => 1491,
                'fecha' => '2019-11-25 23:06:00',
                'ejecutado' => 1,
            ),
            6 => 
            array (
                'id_alarmas' => 1447,
                'id_cotizacion' => 3,
                'precio' => 7281.0,
                'inicio' => 7246.17,
                'id_usuario' => 1236,
                'fecha' => '2019-11-26 07:48:00',
                'ejecutado' => 1,
            ),
            7 => 
            array (
                'id_alarmas' => 1450,
                'id_cotizacion' => 9,
                'precio' => 28100.1,
                'inicio' => 28079.24,
                'id_usuario' => 1236,
                'fecha' => '2019-11-26 07:57:00',
                'ejecutado' => 1,
            ),
            8 => 
            array (
                'id_alarmas' => 1453,
                'id_cotizacion' => 3,
                'precio' => 7281.8,
                'inicio' => 7230.26,
                'id_usuario' => 1236,
                'fecha' => '2019-11-26 08:00:00',
                'ejecutado' => 1,
            ),
            9 => 
            array (
                'id_alarmas' => 1454,
                'id_cotizacion' => 3,
                'precio' => 7175.0,
                'inicio' => 7222.04,
                'id_usuario' => 1236,
                'fecha' => '2019-11-26 08:02:00',
                'ejecutado' => 1,
            ),
            10 => 
            array (
                'id_alarmas' => 1455,
                'id_cotizacion' => 10,
                'precio' => 1459.05,
                'inicio' => 1457.77,
                'id_usuario' => 1584,
                'fecha' => '2019-11-26 10:31:00',
                'ejecutado' => 1,
            ),
            11 => 
            array (
                'id_alarmas' => 1457,
                'id_cotizacion' => 9,
                'precio' => 28100.0,
                'inicio' => 28097.24,
                'id_usuario' => 1236,
                'fecha' => '2019-11-26 13:43:00',
                'ejecutado' => 1,
            ),
            12 => 
            array (
                'id_alarmas' => 1458,
                'id_cotizacion' => 3,
                'precio' => 6995.4,
                'inicio' => 7108.42,
                'id_usuario' => 1236,
                'fecha' => '2019-11-26 13:46:00',
                'ejecutado' => 1,
            ),
            13 => 
            array (
                'id_alarmas' => 1459,
                'id_cotizacion' => 7,
                'precio' => 200.379,
                'inicio' => 200.105,
                'id_usuario' => 1236,
                'fecha' => '2019-11-26 14:20:00',
                'ejecutado' => 1,
            ),
            14 => 
            array (
                'id_alarmas' => 1460,
                'id_cotizacion' => 5,
                'precio' => 146.66,
                'inicio' => 145.99,
                'id_usuario' => 1655,
                'fecha' => '2019-11-26 14:21:00',
                'ejecutado' => 1,
            ),
            15 => 
            array (
                'id_alarmas' => 1461,
                'id_cotizacion' => 18,
                'precio' => 0.998,
                'inicio' => 0.99708,
                'id_usuario' => 1749,
                'fecha' => '2019-11-26 20:19:00',
                'ejecutado' => 1,
            ),
            16 => 
            array (
                'id_alarmas' => 1462,
                'id_cotizacion' => 9,
                'precio' => 28109.9,
                'inicio' => 28107.84,
                'id_usuario' => 1236,
                'fecha' => '2019-11-26 20:41:00',
                'ejecutado' => 1,
            ),
            17 => 
            array (
                'id_alarmas' => 1463,
                'id_cotizacion' => 5,
                'precio' => 145.82,
                'inicio' => 145.49,
                'id_usuario' => 1236,
                'fecha' => '2019-11-27 04:11:00',
                'ejecutado' => 1,
            ),
            18 => 
            array (
                'id_alarmas' => 1465,
                'id_cotizacion' => 9,
                'precio' => 28120.0,
                'inicio' => 28155.84,
                'id_usuario' => 1236,
                'fecha' => '2019-11-27 08:13:00',
                'ejecutado' => 1,
            ),
            19 => 
            array (
                'id_alarmas' => 1466,
                'id_cotizacion' => 9,
                'precio' => 28097.7,
                'inicio' => 28155.84,
                'id_usuario' => 1236,
                'fecha' => '2019-11-27 08:13:00',
                'ejecutado' => 1,
            ),
            20 => 
            array (
                'id_alarmas' => 1467,
                'id_cotizacion' => 9,
                'precio' => 28200.0,
                'inicio' => 28155.34,
                'id_usuario' => 1236,
                'fecha' => '2019-11-27 08:14:00',
                'ejecutado' => 1,
            ),
            21 => 
            array (
                'id_alarmas' => 1468,
                'id_cotizacion' => 10,
                'precio' => 1459.26,
                'inicio' => 1458.75,
                'id_usuario' => 1606,
                'fecha' => '2019-11-27 12:29:00',
                'ejecutado' => 1,
            ),
            22 => 
            array (
                'id_alarmas' => 1470,
                'id_cotizacion' => 12,
                'precio' => 132.797,
                'inicio' => 132.831,
                'id_usuario' => 1810,
                'fecha' => '2019-11-27 19:19:00',
                'ejecutado' => 1,
            ),
            23 => 
            array (
                'id_alarmas' => 1472,
                'id_cotizacion' => 6,
                'precio' => 109.9,
                'inicio' => 109.53,
                'id_usuario' => 2159,
                'fecha' => '2019-11-27 19:31:00',
                'ejecutado' => 0,
            ),
            24 => 
            array (
                'id_alarmas' => 1473,
                'id_cotizacion' => 3,
                'precio' => 7652.51,
                'inicio' => 7560.01,
                'id_usuario' => 1626,
                'fecha' => '2019-11-27 23:20:00',
                'ejecutado' => 1,
            ),
            25 => 
            array (
                'id_alarmas' => 1474,
                'id_cotizacion' => 3,
                'precio' => 7451.0,
                'inicio' => 7562.11,
                'id_usuario' => 1626,
                'fecha' => '2019-11-27 23:20:00',
                'ejecutado' => 1,
            ),
            26 => 
            array (
                'id_alarmas' => 1475,
                'id_cotizacion' => 6,
                'precio' => 109.458,
                'inicio' => 109.459,
                'id_usuario' => 2170,
                'fecha' => '2019-11-28 03:26:00',
                'ejecutado' => 1,
            ),
            27 => 
            array (
                'id_alarmas' => 1476,
                'id_cotizacion' => 9,
                'precio' => 28069.4,
                'inicio' => 28105.24,
                'id_usuario' => 1236,
                'fecha' => '2019-11-28 03:47:00',
                'ejecutado' => 1,
            ),
            28 => 
            array (
                'id_alarmas' => 1477,
                'id_cotizacion' => 3,
                'precio' => 7731.0,
                'inicio' => 7671.13,
                'id_usuario' => 576,
                'fecha' => '2019-11-28 19:58:00',
                'ejecutado' => 1,
            ),
            29 => 
            array (
                'id_alarmas' => 1478,
                'id_cotizacion' => 6,
                'precio' => 109.5,
                'inicio' => 109.519,
                'id_usuario' => 2124,
                'fecha' => '2019-11-28 22:40:00',
                'ejecutado' => 1,
            ),
            30 => 
            array (
                'id_alarmas' => 1480,
                'id_cotizacion' => 7,
                'precio' => 201.177,
                'inicio' => 201.137,
                'id_usuario' => 1982,
                'fecha' => '2019-11-29 06:18:00',
                'ejecutado' => 1,
            ),
            31 => 
            array (
                'id_alarmas' => 1481,
                'id_cotizacion' => 7,
                'precio' => 201.221,
                'inicio' => 201.139,
                'id_usuario' => 1982,
                'fecha' => '2019-11-29 06:22:00',
                'ejecutado' => 1,
            ),
            32 => 
            array (
                'id_alarmas' => 1483,
                'id_cotizacion' => 7,
                'precio' => 200.913,
                'inicio' => 201.119,
                'id_usuario' => 1982,
                'fecha' => '2019-11-29 06:24:00',
                'ejecutado' => 1,
            ),
            33 => 
            array (
                'id_alarmas' => 1485,
                'id_cotizacion' => 10,
                'precio' => 1460.0,
                'inicio' => 1464.2,
                'id_usuario' => 1626,
                'fecha' => '2019-11-29 18:23:00',
                'ejecutado' => 1,
            ),
            34 => 
            array (
                'id_alarmas' => 1486,
                'id_cotizacion' => 10,
                'precio' => 1464.0,
                'inicio' => 1464.2,
                'id_usuario' => 1982,
                'fecha' => '2019-11-29 19:43:00',
                'ejecutado' => 1,
            ),
            35 => 
            array (
                'id_alarmas' => 1487,
                'id_cotizacion' => 10,
                'precio' => 1464.2,
                'inicio' => 1464.2,
                'id_usuario' => 2189,
                'fecha' => '2019-11-30 03:33:00',
                'ejecutado' => 0,
            ),
            36 => 
            array (
                'id_alarmas' => 1489,
                'id_cotizacion' => 2,
                'precio' => 0.22174,
                'inicio' => 0.22337,
                'id_usuario' => 1982,
                'fecha' => '2019-12-01 17:54:00',
                'ejecutado' => 1,
            ),
            37 => 
            array (
                'id_alarmas' => 1493,
                'id_cotizacion' => 2,
                'precio' => 0.22503,
                'inicio' => 0.22345,
                'id_usuario' => 1982,
                'fecha' => '2019-12-01 18:08:00',
                'ejecutado' => 1,
            ),
            38 => 
            array (
                'id_alarmas' => 1494,
                'id_cotizacion' => 10,
                'precio' => 1464.0,
                'inicio' => 1461.81,
                'id_usuario' => 380,
                'fecha' => '2019-12-01 23:17:00',
                'ejecutado' => 1,
            ),
            39 => 
            array (
                'id_alarmas' => 1495,
                'id_cotizacion' => 10,
                'precio' => 1455.6,
                'inicio' => 1456.61,
                'id_usuario' => 576,
                'fecha' => '2019-12-02 08:10:00',
                'ejecutado' => 1,
            ),
            40 => 
            array (
                'id_alarmas' => 1496,
                'id_cotizacion' => 10,
                'precio' => 1454.1,
                'inicio' => 1455.64,
                'id_usuario' => 576,
                'fecha' => '2019-12-02 08:29:00',
                'ejecutado' => 1,
            ),
            41 => 
            array (
                'id_alarmas' => 1497,
                'id_cotizacion' => 3,
                'precio' => 7122.2,
                'inicio' => 7277.9,
                'id_usuario' => 576,
                'fecha' => '2019-12-02 08:30:00',
                'ejecutado' => 0,
            ),
            42 => 
            array (
                'id_alarmas' => 1498,
                'id_cotizacion' => 6,
                'precio' => 109.128,
                'inicio' => 109.055,
                'id_usuario' => 1982,
                'fecha' => '2019-12-02 17:44:00',
                'ejecutado' => 1,
            ),
            43 => 
            array (
                'id_alarmas' => 1500,
                'id_cotizacion' => 2,
                'precio' => 0.22141,
                'inicio' => 0.22113,
                'id_usuario' => 1982,
                'fecha' => '2019-12-02 19:49:00',
                'ejecutado' => 1,
            ),
            44 => 
            array (
                'id_alarmas' => 1501,
                'id_cotizacion' => 2,
                'precio' => 0.21875,
                'inicio' => 0.22111,
                'id_usuario' => 1982,
                'fecha' => '2019-12-02 19:49:00',
                'ejecutado' => 1,
            ),
            45 => 
            array (
                'id_alarmas' => 1502,
                'id_cotizacion' => 10,
                'precio' => 1460.47,
                'inicio' => 1463.72,
                'id_usuario' => 1491,
                'fecha' => '2019-12-02 20:20:00',
                'ejecutado' => 1,
            ),
            46 => 
            array (
                'id_alarmas' => 1503,
                'id_cotizacion' => 3,
                'precio' => 7380.69,
                'inicio' => 7360.89,
                'id_usuario' => 2182,
                'fecha' => '2019-12-02 21:46:00',
                'ejecutado' => 1,
            ),
            47 => 
            array (
                'id_alarmas' => 1505,
                'id_cotizacion' => 2,
                'precio' => 0.22189,
                'inicio' => 0.22318,
                'id_usuario' => 1982,
                'fecha' => '2019-12-03 01:09:00',
                'ejecutado' => 1,
            ),
            48 => 
            array (
                'id_alarmas' => 1506,
                'id_cotizacion' => 15,
                'precio' => 141.204,
                'inicio' => 141.271,
                'id_usuario' => 1606,
                'fecha' => '2019-12-03 03:58:00',
                'ejecutado' => 1,
            ),
            49 => 
            array (
                'id_alarmas' => 1508,
                'id_cotizacion' => 17,
                'precio' => 110.701,
                'inicio' => 11.082,
                'id_usuario' => 1584,
                'fecha' => '2019-12-03 07:24:00',
                'ejecutado' => 1,
            ),
            50 => 
            array (
                'id_alarmas' => 1509,
                'id_cotizacion' => 6,
                'precio' => 109.204,
                'inicio' => 109.127,
                'id_usuario' => 1584,
                'fecha' => '2019-12-03 07:28:00',
                'ejecutado' => 0,
            ),
            51 => 
            array (
                'id_alarmas' => 1510,
                'id_cotizacion' => 7,
                'precio' => 198.572,
                'inicio' => 19.888,
                'id_usuario' => 1584,
                'fecha' => '2019-12-03 07:31:00',
                'ejecutado' => 0,
            ),
            52 => 
            array (
                'id_alarmas' => 1511,
                'id_cotizacion' => 16,
                'precio' => 121.0,
                'inicio' => 120.87,
                'id_usuario' => 1749,
                'fecha' => '2019-12-03 08:10:00',
                'ejecutado' => 0,
            ),
            53 => 
            array (
                'id_alarmas' => 1512,
                'id_cotizacion' => 16,
                'precio' => 120.69,
                'inicio' => 120.883,
                'id_usuario' => 1749,
                'fecha' => '2019-12-03 08:11:00',
                'ejecutado' => 1,
            ),
            54 => 
            array (
                'id_alarmas' => 1513,
                'id_cotizacion' => 6,
                'precio' => 108.72,
                'inicio' => 108.832,
                'id_usuario' => 2124,
                'fecha' => '2019-12-03 12:42:00',
                'ejecutado' => 1,
            ),
            55 => 
            array (
                'id_alarmas' => 1514,
                'id_cotizacion' => 8,
                'precio' => 1.902,
                'inicio' => 189.936,
                'id_usuario' => 2124,
                'fecha' => '2019-12-03 12:48:00',
                'ejecutado' => 1,
            ),
            56 => 
            array (
                'id_alarmas' => 1515,
                'id_cotizacion' => 10,
                'precio' => 1472.31,
                'inicio' => 1470.57,
                'id_usuario' => 2124,
                'fecha' => '2019-12-03 12:50:00',
                'ejecutado' => 1,
            ),
            57 => 
            array (
                'id_alarmas' => 1517,
                'id_cotizacion' => 7,
                'precio' => 199.141,
                'inicio' => 199.316,
                'id_usuario' => 853,
                'fecha' => '2019-12-03 14:35:00',
                'ejecutado' => 1,
            ),
            58 => 
            array (
                'id_alarmas' => 1518,
                'id_cotizacion' => 6,
                'precio' => 108.66,
                'inicio' => 108.577,
                'id_usuario' => 1810,
                'fecha' => '2019-12-03 19:59:00',
                'ejecutado' => 1,
            ),
            59 => 
            array (
                'id_alarmas' => 1519,
                'id_cotizacion' => 15,
                'precio' => 141.021,
                'inicio' => 141.072,
                'id_usuario' => 1810,
                'fecha' => '2019-12-03 20:00:00',
                'ejecutado' => 1,
            ),
            60 => 
            array (
                'id_alarmas' => 1520,
                'id_cotizacion' => 11,
                'precio' => 172.737,
                'inicio' => 172.837,
                'id_usuario' => 1810,
                'fecha' => '2019-12-03 20:01:00',
                'ejecutado' => 1,
            ),
            61 => 
            array (
                'id_alarmas' => 1521,
                'id_cotizacion' => 3,
                'precio' => 7530.0,
                'inicio' => 7358.19,
                'id_usuario' => 1810,
                'fecha' => '2019-12-03 20:03:00',
                'ejecutado' => 1,
            ),
            62 => 
            array (
                'id_alarmas' => 1522,
                'id_cotizacion' => 17,
                'precio' => 110.818,
                'inicio' => 110.825,
                'id_usuario' => 1491,
                'fecha' => '2019-12-03 22:30:00',
                'ejecutado' => 1,
            ),
            63 => 
            array (
                'id_alarmas' => 1528,
                'id_cotizacion' => 7,
                'precio' => 199.639,
                'inicio' => 199.483,
                'id_usuario' => 1982,
                'fecha' => '2019-12-04 04:54:00',
                'ejecutado' => 1,
            ),
            64 => 
            array (
                'id_alarmas' => 1529,
                'id_cotizacion' => 10,
                'precio' => 1472.57,
                'inicio' => 1476.91,
                'id_usuario' => 1491,
                'fecha' => '2019-12-04 11:27:00',
                'ejecutado' => 1,
            ),
            65 => 
            array (
                'id_alarmas' => 1530,
                'id_cotizacion' => 10,
                'precio' => 1484.0,
                'inicio' => 1476.9,
                'id_usuario' => 1491,
                'fecha' => '2019-12-04 11:27:00',
                'ejecutado' => 0,
            ),
            66 => 
            array (
                'id_alarmas' => 1531,
                'id_cotizacion' => 16,
                'precio' => 120.137,
                'inicio' => 120.303,
                'id_usuario' => 1491,
                'fecha' => '2019-12-04 11:29:00',
                'ejecutado' => 1,
            ),
            67 => 
            array (
                'id_alarmas' => 1532,
                'id_cotizacion' => 10,
                'precio' => 1474.92,
                'inicio' => 1476.84,
                'id_usuario' => 2124,
                'fecha' => '2019-12-04 11:31:00',
                'ejecutado' => 1,
            ),
            68 => 
            array (
                'id_alarmas' => 1533,
                'id_cotizacion' => 7,
                'precio' => 200.306,
                'inicio' => 200.726,
                'id_usuario' => 853,
                'fecha' => '2019-12-04 15:18:00',
                'ejecutado' => 1,
            ),
            69 => 
            array (
                'id_alarmas' => 1534,
                'id_cotizacion' => 7,
                'precio' => 200.306,
                'inicio' => 200.728,
                'id_usuario' => 853,
                'fecha' => '2019-12-04 15:18:00',
                'ejecutado' => 1,
            ),
            70 => 
            array (
                'id_alarmas' => 1535,
                'id_cotizacion' => 4,
                'precio' => 27.44,
                'inicio' => 27.74,
                'id_usuario' => 853,
                'fecha' => '2019-12-04 15:27:00',
                'ejecutado' => 1,
            ),
            71 => 
            array (
                'id_alarmas' => 1537,
                'id_cotizacion' => 7,
                'precio' => 200.412,
                'inicio' => 200.585,
                'id_usuario' => 853,
                'fecha' => '2019-12-04 19:02:00',
                'ejecutado' => 1,
            ),
            72 => 
            array (
                'id_alarmas' => 1538,
                'id_cotizacion' => 7,
                'precio' => 200.412,
                'inicio' => 200.599,
                'id_usuario' => 853,
                'fecha' => '2019-12-04 19:02:00',
                'ejecutado' => 1,
            ),
            73 => 
            array (
                'id_alarmas' => 1539,
                'id_cotizacion' => 7,
                'precio' => 200.318,
                'inicio' => 200.745,
                'id_usuario' => 853,
                'fecha' => '2019-12-04 21:31:00',
                'ejecutado' => 1,
            ),
            74 => 
            array (
                'id_alarmas' => 1540,
                'id_cotizacion' => 8,
                'precio' => 1.914,
                'inicio' => 191.314,
                'id_usuario' => 1491,
                'fecha' => '2019-12-04 21:48:00',
                'ejecutado' => 1,
            ),
            75 => 
            array (
                'id_alarmas' => 1541,
                'id_cotizacion' => 8,
                'precio' => 19.142,
                'inicio' => 191.403,
                'id_usuario' => 1491,
                'fecha' => '2019-12-04 22:07:00',
                'ejecutado' => 1,
            ),
            76 => 
            array (
                'id_alarmas' => 1542,
                'id_cotizacion' => 6,
                'precio' => 108.959,
                'inicio' => 108.878,
                'id_usuario' => 459,
                'fecha' => '2019-12-04 22:19:00',
                'ejecutado' => 1,
            ),
            77 => 
            array (
                'id_alarmas' => 1543,
                'id_cotizacion' => 6,
                'precio' => 109.105,
                'inicio' => 108.869,
                'id_usuario' => 459,
                'fecha' => '2019-12-04 22:19:00',
                'ejecutado' => 0,
            ),
            78 => 
            array (
                'id_alarmas' => 1544,
                'id_cotizacion' => 7,
                'precio' => 200.516,
                'inicio' => 200.846,
                'id_usuario' => 1810,
                'fecha' => '2019-12-04 22:56:00',
                'ejecutado' => 1,
            ),
            79 => 
            array (
                'id_alarmas' => 1545,
                'id_cotizacion' => 10,
                'precio' => 1475.02,
                'inicio' => 1475.08,
                'id_usuario' => 2151,
                'fecha' => '2019-12-04 23:21:00',
                'ejecutado' => 1,
            ),
            80 => 
            array (
                'id_alarmas' => 1547,
                'id_cotizacion' => 10,
                'precio' => 1475.02,
                'inicio' => 1475.08,
                'id_usuario' => 2151,
                'fecha' => '2019-12-04 23:21:00',
                'ejecutado' => 1,
            ),
            81 => 
            array (
                'id_alarmas' => 1548,
                'id_cotizacion' => 10,
                'precio' => 1475.02,
                'inicio' => 1475.15,
                'id_usuario' => 2151,
                'fecha' => '2019-12-04 23:21:00',
                'ejecutado' => 1,
            ),
            82 => 
            array (
                'id_alarmas' => 1549,
                'id_cotizacion' => 7,
                'precio' => 20.021,
                'inicio' => 200.022,
                'id_usuario' => 1470,
                'fecha' => '2019-12-05 00:36:00',
                'ejecutado' => 1,
            ),
            83 => 
            array (
                'id_alarmas' => 1550,
                'id_cotizacion' => 7,
                'precio' => 200.636,
                'inicio' => 200.548,
                'id_usuario' => 1982,
                'fecha' => '2019-12-05 03:23:00',
                'ejecutado' => 1,
            ),
            84 => 
            array (
                'id_alarmas' => 1551,
                'id_cotizacion' => 7,
                'precio' => 199.822,
                'inicio' => 200.542,
                'id_usuario' => 1982,
                'fecha' => '2019-12-05 03:24:00',
                'ejecutado' => 1,
            ),
            85 => 
            array (
                'id_alarmas' => 1552,
                'id_cotizacion' => 10,
                'precio' => 1479.12,
                'inicio' => 1477.11,
                'id_usuario' => 1640,
                'fecha' => '2019-12-05 07:09:00',
                'ejecutado' => 1,
            ),
            86 => 
            array (
                'id_alarmas' => 1553,
                'id_cotizacion' => 10,
                'precio' => 1479.12,
                'inicio' => 1477.11,
                'id_usuario' => 1640,
                'fecha' => '2019-12-05 07:09:00',
                'ejecutado' => 1,
            ),
            87 => 
            array (
                'id_alarmas' => 1554,
                'id_cotizacion' => 10,
                'precio' => 1481.74,
                'inicio' => 1479.78,
                'id_usuario' => 1491,
                'fecha' => '2019-12-05 17:26:00',
                'ejecutado' => 0,
            ),
            88 => 
            array (
                'id_alarmas' => 1555,
                'id_cotizacion' => 8,
                'precio' => 192.354,
                'inicio' => 192.542,
                'id_usuario' => 1491,
                'fecha' => '2019-12-05 17:56:00',
                'ejecutado' => 1,
            ),
            89 => 
            array (
                'id_alarmas' => 1558,
                'id_cotizacion' => 3,
                'precio' => 7407.4,
                'inicio' => 7403.44,
                'id_usuario' => 1236,
                'fecha' => '2019-12-05 21:36:00',
                'ejecutado' => 1,
            ),
            90 => 
            array (
                'id_alarmas' => 1559,
                'id_cotizacion' => 3,
                'precio' => 7440.4,
                'inicio' => 7413.05,
                'id_usuario' => 1236,
                'fecha' => '2019-12-05 21:41:00',
                'ejecutado' => 1,
            ),
            91 => 
            array (
                'id_alarmas' => 1560,
                'id_cotizacion' => 3,
                'precio' => 7374.3,
                'inicio' => 7412.75,
                'id_usuario' => 1236,
                'fecha' => '2019-12-05 21:47:00',
                'ejecutado' => 1,
            ),
            92 => 
            array (
                'id_alarmas' => 1561,
                'id_cotizacion' => 10,
                'precio' => 1471.0,
                'inicio' => 1475.96,
                'id_usuario' => 1491,
                'fecha' => '2019-12-05 22:01:00',
                'ejecutado' => 1,
            ),
            93 => 
            array (
                'id_alarmas' => 1565,
                'id_cotizacion' => 9,
                'precio' => 27726.0,
                'inicio' => 27703.24,
                'id_usuario' => 1606,
                'fecha' => '2019-12-06 03:12:00',
                'ejecutado' => 1,
            ),
            94 => 
            array (
                'id_alarmas' => 1566,
                'id_cotizacion' => 9,
                'precio' => 27681.0,
                'inicio' => 27703.24,
                'id_usuario' => 1606,
                'fecha' => '2019-12-06 03:13:00',
                'ejecutado' => 0,
            ),
            95 => 
            array (
                'id_alarmas' => 1567,
                'id_cotizacion' => 3,
                'precio' => 6100.0,
                'inicio' => 7454.7,
                'id_usuario' => 1372,
                'fecha' => '2019-12-06 15:32:00',
                'ejecutado' => 0,
            ),
            96 => 
            array (
                'id_alarmas' => 1568,
                'id_cotizacion' => 3,
                'precio' => 6480.0,
                'inicio' => 7454.1,
                'id_usuario' => 1372,
                'fecha' => '2019-12-06 15:33:00',
                'ejecutado' => 0,
            ),
            97 => 
            array (
                'id_alarmas' => 1569,
                'id_cotizacion' => 3,
                'precio' => 6100.0,
                'inicio' => 7448.59,
                'id_usuario' => 1372,
                'fecha' => '2019-12-06 15:33:00',
                'ejecutado' => 0,
            ),
            98 => 
            array (
                'id_alarmas' => 1570,
                'id_cotizacion' => 3,
                'precio' => 6080.0,
                'inicio' => 7448.59,
                'id_usuario' => 1372,
                'fecha' => '2019-12-06 15:34:00',
                'ejecutado' => 0,
            ),
            99 => 
            array (
                'id_alarmas' => 1571,
                'id_cotizacion' => 3,
                'precio' => 6200.0,
                'inicio' => 7450.09,
                'id_usuario' => 1372,
                'fecha' => '2019-12-06 15:34:00',
                'ejecutado' => 0,
            ),
            100 => 
            array (
                'id_alarmas' => 1572,
                'id_cotizacion' => 10,
                'precio' => 1455.0,
                'inicio' => 1461.23,
                'id_usuario' => 1491,
                'fecha' => '2019-12-06 16:01:00',
                'ejecutado' => 0,
            ),
            101 => 
            array (
                'id_alarmas' => 1574,
                'id_cotizacion' => 3,
                'precio' => 8200.0,
                'inicio' => 7591.45,
                'id_usuario' => 1810,
                'fecha' => '2019-12-07 00:41:00',
                'ejecutado' => 0,
            ),
            102 => 
            array (
                'id_alarmas' => 1575,
                'id_cotizacion' => 3,
                'precio' => 7950.0,
                'inicio' => 7591.45,
                'id_usuario' => 1810,
                'fecha' => '2019-12-07 00:41:00',
                'ejecutado' => 0,
            ),
            103 => 
            array (
                'id_alarmas' => 1576,
                'id_cotizacion' => 3,
                'precio' => 8500.0,
                'inicio' => 7591.45,
                'id_usuario' => 1810,
                'fecha' => '2019-12-07 00:41:00',
                'ejecutado' => 0,
            ),
            104 => 
            array (
                'id_alarmas' => 1577,
                'id_cotizacion' => 3,
                'precio' => 7460.0,
                'inicio' => 7455.3,
                'id_usuario' => 1236,
                'fecha' => '2019-12-08 04:44:00',
                'ejecutado' => 1,
            ),
            105 => 
            array (
                'id_alarmas' => 1578,
                'id_cotizacion' => 3,
                'precio' => 7486.0,
                'inicio' => 7484.53,
                'id_usuario' => 1236,
                'fecha' => '2019-12-08 04:59:00',
                'ejecutado' => 1,
            ),
            106 => 
            array (
                'id_alarmas' => 1581,
                'id_cotizacion' => 3,
                'precio' => 7580.0,
                'inicio' => 7535.79,
                'id_usuario' => 1236,
                'fecha' => '2019-12-08 09:34:00',
                'ejecutado' => 1,
            ),
            107 => 
            array (
                'id_alarmas' => 1582,
                'id_cotizacion' => 3,
                'precio' => 7600.0,
                'inicio' => 7555.31,
                'id_usuario' => 1236,
                'fecha' => '2019-12-08 10:36:00',
                'ejecutado' => 1,
            ),
            108 => 
            array (
                'id_alarmas' => 1583,
                'id_cotizacion' => 3,
                'precio' => 7620.0,
                'inicio' => 7592.05,
                'id_usuario' => 1236,
                'fecha' => '2019-12-08 11:21:00',
                'ejecutado' => 1,
            ),
            109 => 
            array (
                'id_alarmas' => 1584,
                'id_cotizacion' => 10,
                'precio' => 1457.3,
                'inicio' => 1460.07,
                'id_usuario' => 1491,
                'fecha' => '2019-12-08 23:58:00',
                'ejecutado' => 0,
            ),
            110 => 
            array (
                'id_alarmas' => 1585,
                'id_cotizacion' => 9,
                'precio' => 27869.2,
                'inicio' => 27983.74,
                'id_usuario' => 1236,
                'fecha' => '2019-12-09 00:45:00',
                'ejecutado' => 1,
            ),
            111 => 
            array (
                'id_alarmas' => 1586,
                'id_cotizacion' => 9,
                'precio' => 27984.0,
                'inicio' => 27975.74,
                'id_usuario' => 1236,
                'fecha' => '2019-12-09 02:15:00',
                'ejecutado' => 1,
            ),
            112 => 
            array (
                'id_alarmas' => 1587,
                'id_cotizacion' => 9,
                'precio' => 27957.0,
                'inicio' => 27976.74,
                'id_usuario' => 1236,
                'fecha' => '2019-12-09 02:15:00',
                'ejecutado' => 1,
            ),
            113 => 
            array (
                'id_alarmas' => 1588,
                'id_cotizacion' => 8,
                'precio' => 19.262,
                'inicio' => 192.345,
                'id_usuario' => 1491,
                'fecha' => '2019-12-09 02:16:00',
                'ejecutado' => 1,
            ),
            114 => 
            array (
                'id_alarmas' => 1589,
                'id_cotizacion' => 9,
                'precio' => 2794.0,
                'inicio' => 27986.24,
                'id_usuario' => 1236,
                'fecha' => '2019-12-09 03:00:00',
                'ejecutado' => 0,
            ),
            115 => 
            array (
                'id_alarmas' => 1590,
                'id_cotizacion' => 3,
                'precio' => 7587.1,
                'inicio' => 7550.4,
                'id_usuario' => 1236,
                'fecha' => '2019-12-09 03:02:00',
                'ejecutado' => 1,
            ),
            116 => 
            array (
                'id_alarmas' => 1591,
                'id_cotizacion' => 6,
                'precio' => 108.524,
                'inicio' => 108.58,
                'id_usuario' => 1810,
                'fecha' => '2019-12-09 03:55:00',
                'ejecutado' => 1,
            ),
            117 => 
            array (
                'id_alarmas' => 1592,
                'id_cotizacion' => 3,
                'precio' => 7562.3,
                'inicio' => 7558.51,
                'id_usuario' => 1236,
                'fecha' => '2019-12-09 05:13:00',
                'ejecutado' => 1,
            ),
            118 => 
            array (
                'id_alarmas' => 1593,
                'id_cotizacion' => 3,
                'precio' => 7543.7,
                'inicio' => 7539.69,
                'id_usuario' => 1236,
                'fecha' => '2019-12-09 05:58:00',
                'ejecutado' => 1,
            ),
            119 => 
            array (
                'id_alarmas' => 1594,
                'id_cotizacion' => 6,
                'precio' => 108.644,
                'inicio' => 108.594,
                'id_usuario' => 1982,
                'fecha' => '2019-12-09 06:19:00',
                'ejecutado' => 1,
            ),
            120 => 
            array (
                'id_alarmas' => 1595,
                'id_cotizacion' => 8,
                'precio' => 1.928,
                'inicio' => 192.821,
                'id_usuario' => 2109,
                'fecha' => '2019-12-09 06:48:00',
                'ejecutado' => 1,
            ),
            121 => 
            array (
                'id_alarmas' => 1596,
                'id_cotizacion' => 8,
                'precio' => 1.93,
                'inicio' => 192.855,
                'id_usuario' => 1491,
                'fecha' => '2019-12-09 07:42:00',
                'ejecutado' => 1,
            ),
            122 => 
            array (
                'id_alarmas' => 1597,
                'id_cotizacion' => 15,
                'precio' => 143.315,
                'inicio' => 143.025,
                'id_usuario' => 1491,
                'fecha' => '2019-12-09 07:43:00',
                'ejecutado' => 1,
            ),
            123 => 
            array (
                'id_alarmas' => 1598,
                'id_cotizacion' => 9,
                'precio' => 27920.4,
                'inicio' => 27965.34,
                'id_usuario' => 1236,
                'fecha' => '2019-12-09 09:25:00',
                'ejecutado' => 1,
            ),
            124 => 
            array (
                'id_alarmas' => 1599,
                'id_cotizacion' => 3,
                'precio' => 7550.2,
                'inicio' => 7521.87,
                'id_usuario' => 1236,
                'fecha' => '2019-12-09 09:26:00',
                'ejecutado' => 1,
            ),
            125 => 
            array (
                'id_alarmas' => 1600,
                'id_cotizacion' => 3,
                'precio' => 7577.0,
                'inicio' => 7523.18,
                'id_usuario' => 1236,
                'fecha' => '2019-12-09 09:27:00',
                'ejecutado' => 1,
            ),
            126 => 
            array (
                'id_alarmas' => 1601,
                'id_cotizacion' => 3,
                'precio' => 7594.2,
                'inicio' => 7513.56,
                'id_usuario' => 1236,
                'fecha' => '2019-12-09 09:45:00',
                'ejecutado' => 1,
            ),
            127 => 
            array (
                'id_alarmas' => 1602,
                'id_cotizacion' => 9,
                'precio' => 27932.5,
                'inicio' => 27982.34,
                'id_usuario' => 1236,
                'fecha' => '2019-12-09 09:49:00',
                'ejecutado' => 1,
            ),
            128 => 
            array (
                'id_alarmas' => 1603,
                'id_cotizacion' => 8,
                'precio' => 1.928,
                'inicio' => 192.991,
                'id_usuario' => 1491,
                'fecha' => '2019-12-09 11:28:00',
                'ejecutado' => 1,
            ),
            129 => 
            array (
                'id_alarmas' => 1604,
                'id_cotizacion' => 7,
                'precio' => 20.046,
                'inicio' => 200.612,
                'id_usuario' => 853,
                'fecha' => '2019-12-09 14:40:00',
                'ejecutado' => 1,
            ),
            130 => 
            array (
                'id_alarmas' => 1605,
                'id_cotizacion' => 12,
                'precio' => 1.324,
                'inicio' => 13.234,
                'id_usuario' => 100,
                'fecha' => '2019-12-09 15:11:00',
                'ejecutado' => 1,
            ),
            131 => 
            array (
                'id_alarmas' => 1606,
                'id_cotizacion' => 6,
                'precio' => 108.602,
                'inicio' => 108.516,
                'id_usuario' => 1810,
                'fecha' => '2019-12-09 15:25:00',
                'ejecutado' => 1,
            ),
            132 => 
            array (
                'id_alarmas' => 1607,
                'id_cotizacion' => 17,
                'precio' => 110.675,
                'inicio' => 110.722,
                'id_usuario' => 1810,
                'fecha' => '2019-12-09 15:26:00',
                'ejecutado' => 1,
            ),
            133 => 
            array (
                'id_alarmas' => 1608,
                'id_cotizacion' => 11,
                'precio' => 174.426,
                'inicio' => 174.118,
                'id_usuario' => 1671,
                'fecha' => '2019-12-09 15:27:00',
                'ejecutado' => 1,
            ),
            134 => 
            array (
                'id_alarmas' => 1609,
                'id_cotizacion' => 11,
                'precio' => 17.442,
                'inicio' => 174.137,
                'id_usuario' => 1671,
                'fecha' => '2019-12-09 15:28:00',
                'ejecutado' => 1,
            ),
            135 => 
            array (
                'id_alarmas' => 1610,
                'id_cotizacion' => 11,
                'precio' => 17.442,
                'inicio' => 174.119,
                'id_usuario' => 100,
                'fecha' => '2019-12-09 15:30:00',
                'ejecutado' => 1,
            ),
            136 => 
            array (
                'id_alarmas' => 1612,
                'id_cotizacion' => 11,
                'precio' => 17.442,
                'inicio' => 174.041,
                'id_usuario' => 1952,
                'fecha' => '2019-12-09 15:39:00',
                'ejecutado' => 1,
            ),
            137 => 
            array (
                'id_alarmas' => 1613,
                'id_cotizacion' => 11,
                'precio' => 17.442,
                'inicio' => 174.046,
                'id_usuario' => 1952,
                'fecha' => '2019-12-09 15:39:00',
                'ejecutado' => 1,
            ),
            138 => 
            array (
                'id_alarmas' => 1614,
                'id_cotizacion' => 11,
                'precio' => 17.442,
                'inicio' => 174.046,
                'id_usuario' => 1952,
                'fecha' => '2019-12-09 15:39:00',
                'ejecutado' => 1,
            ),
            139 => 
            array (
                'id_alarmas' => 1615,
                'id_cotizacion' => 11,
                'precio' => 17.442,
                'inicio' => 174.046,
                'id_usuario' => 1952,
                'fecha' => '2019-12-09 15:39:00',
                'ejecutado' => 1,
            ),
            140 => 
            array (
                'id_alarmas' => 1616,
                'id_cotizacion' => 11,
                'precio' => 17.442,
                'inicio' => 174.058,
                'id_usuario' => 1952,
                'fecha' => '2019-12-09 15:39:00',
                'ejecutado' => 1,
            ),
            141 => 
            array (
                'id_alarmas' => 1617,
                'id_cotizacion' => 12,
                'precio' => 1.324,
                'inicio' => 132.359,
                'id_usuario' => 1952,
                'fecha' => '2019-12-09 15:42:00',
                'ejecutado' => 1,
            ),
            142 => 
            array (
                'id_alarmas' => 1618,
                'id_cotizacion' => 12,
                'precio' => 1.324,
                'inicio' => 13.235,
                'id_usuario' => 1952,
                'fecha' => '2019-12-09 15:42:00',
                'ejecutado' => 1,
            ),
            143 => 
            array (
                'id_alarmas' => 1619,
                'id_cotizacion' => 12,
                'precio' => 1.324,
                'inicio' => 132.347,
                'id_usuario' => 1952,
                'fecha' => '2019-12-09 15:42:00',
                'ejecutado' => 1,
            ),
            144 => 
            array (
                'id_alarmas' => 1620,
                'id_cotizacion' => 12,
                'precio' => 1.324,
                'inicio' => 132.347,
                'id_usuario' => 1952,
                'fecha' => '2019-12-09 15:42:00',
                'ejecutado' => 1,
            ),
            145 => 
            array (
                'id_alarmas' => 1621,
                'id_cotizacion' => 12,
                'precio' => 1.324,
                'inicio' => 132.349,
                'id_usuario' => 1952,
                'fecha' => '2019-12-09 15:42:00',
                'ejecutado' => 1,
            ),
            146 => 
            array (
                'id_alarmas' => 1622,
                'id_cotizacion' => 12,
                'precio' => 1.324,
                'inicio' => 132.348,
                'id_usuario' => 1952,
                'fecha' => '2019-12-09 15:42:00',
                'ejecutado' => 1,
            ),
            147 => 
            array (
                'id_alarmas' => 1623,
                'id_cotizacion' => 8,
                'precio' => 192.682,
                'inicio' => 192.554,
                'id_usuario' => 1491,
                'fecha' => '2019-12-09 16:00:00',
                'ejecutado' => 1,
            ),
            148 => 
            array (
                'id_alarmas' => 1624,
                'id_cotizacion' => 11,
                'precio' => 174.415,
                'inicio' => 173.996,
                'id_usuario' => 2028,
                'fecha' => '2019-12-09 16:18:00',
                'ejecutado' => 1,
            ),
            149 => 
            array (
                'id_alarmas' => 1625,
                'id_cotizacion' => 15,
                'precio' => 142.98,
                'inicio' => 142.924,
                'id_usuario' => 1491,
                'fecha' => '2019-12-09 18:04:00',
                'ejecutado' => 1,
            ),
            150 => 
            array (
                'id_alarmas' => 1626,
                'id_cotizacion' => 6,
                'precio' => 108.598,
                'inicio' => 108.655,
                'id_usuario' => 1810,
                'fecha' => '2019-12-09 18:08:00',
                'ejecutado' => 1,
            ),
            151 => 
            array (
                'id_alarmas' => 1627,
                'id_cotizacion' => 6,
                'precio' => 108.6,
                'inicio' => 108.633,
                'id_usuario' => 2124,
                'fecha' => '2019-12-09 18:30:00',
                'ejecutado' => 1,
            ),
            152 => 
            array (
                'id_alarmas' => 1629,
                'id_cotizacion' => 8,
                'precio' => 192.693,
                'inicio' => 192.522,
                'id_usuario' => 2124,
                'fecha' => '2019-12-09 19:04:00',
                'ejecutado' => 1,
            ),
            153 => 
            array (
                'id_alarmas' => 1630,
                'id_cotizacion' => 9,
                'precio' => 27851.6,
                'inicio' => 27935.34,
                'id_usuario' => 853,
                'fecha' => '2019-12-09 19:33:00',
                'ejecutado' => 1,
            ),
            154 => 
            array (
                'id_alarmas' => 1631,
                'id_cotizacion' => 11,
                'precio' => 174.287,
                'inicio' => 173.982,
                'id_usuario' => 1236,
                'fecha' => '2019-12-09 19:39:00',
                'ejecutado' => 1,
            ),
            155 => 
            array (
                'id_alarmas' => 1632,
                'id_cotizacion' => 10,
                'precio' => 1459.0,
                'inicio' => 1460.03,
                'id_usuario' => 1491,
                'fecha' => '2019-12-09 19:56:00',
                'ejecutado' => 0,
            ),
            156 => 
            array (
                'id_alarmas' => 1633,
                'id_cotizacion' => 18,
                'precio' => 0.99153,
                'inicio' => 0.98815,
                'id_usuario' => 1236,
                'fecha' => '2019-12-09 20:18:00',
                'ejecutado' => 0,
            ),
            157 => 
            array (
                'id_alarmas' => 1634,
                'id_cotizacion' => 18,
                'precio' => 0.98665,
                'inicio' => 0.98813,
                'id_usuario' => 1236,
                'fecha' => '2019-12-09 20:19:00',
                'ejecutado' => 1,
            ),
            158 => 
            array (
                'id_alarmas' => 1636,
                'id_cotizacion' => 17,
                'precio' => 110.614,
                'inicio' => 11.065,
                'id_usuario' => 680,
                'fecha' => '2019-12-09 21:10:00',
                'ejecutado' => 0,
            ),
            159 => 
            array (
                'id_alarmas' => 1637,
                'id_cotizacion' => 17,
                'precio' => 11.061,
                'inicio' => 110.649,
                'id_usuario' => 680,
                'fecha' => '2019-12-09 21:10:00',
                'ejecutado' => 0,
            ),
            160 => 
            array (
                'id_alarmas' => 1638,
                'id_cotizacion' => 17,
                'precio' => 11.061,
                'inicio' => 110.649,
                'id_usuario' => 680,
                'fecha' => '2019-12-09 21:10:00',
                'ejecutado' => 0,
            ),
            161 => 
            array (
                'id_alarmas' => 1640,
                'id_cotizacion' => 6,
                'precio' => 108.592,
                'inicio' => 108.613,
                'id_usuario' => 1810,
                'fecha' => '2019-12-09 21:10:00',
                'ejecutado' => 1,
            ),
            162 => 
            array (
                'id_alarmas' => 1641,
                'id_cotizacion' => 3,
                'precio' => 7239.4,
                'inicio' => 7378.41,
                'id_usuario' => 1236,
                'fecha' => '2019-12-09 23:08:00',
                'ejecutado' => 1,
            ),
            163 => 
            array (
                'id_alarmas' => 1642,
                'id_cotizacion' => 20,
                'precio' => 13.155,
                'inicio' => 131.468,
                'id_usuario' => 1491,
                'fecha' => '2019-12-10 00:57:00',
                'ejecutado' => 1,
            ),
            164 => 
            array (
                'id_alarmas' => 1643,
                'id_cotizacion' => 20,
                'precio' => 1.317,
                'inicio' => 13.145,
                'id_usuario' => 1491,
                'fecha' => '2019-12-10 00:58:00',
                'ejecutado' => 1,
            ),
            165 => 
            array (
                'id_alarmas' => 1644,
                'id_cotizacion' => 13,
                'precio' => 0.65421,
                'inicio' => 0.65661,
                'id_usuario' => 351,
                'fecha' => '2019-12-10 02:30:00',
                'ejecutado' => 1,
            ),
            166 => 
            array (
                'id_alarmas' => 1645,
                'id_cotizacion' => 3,
                'precio' => 7450.0,
                'inicio' => 7407.85,
                'id_usuario' => 853,
                'fecha' => '2019-12-10 03:02:00',
                'ejecutado' => 0,
            ),
            167 => 
            array (
                'id_alarmas' => 1646,
                'id_cotizacion' => 14,
                'precio' => 71.5,
                'inicio' => 71.307,
                'id_usuario' => 2109,
                'fecha' => '2019-12-10 06:17:00',
                'ejecutado' => 0,
            ),
            168 => 
            array (
                'id_alarmas' => 1647,
                'id_cotizacion' => 13,
                'precio' => 0.659,
                'inicio' => 0.6565,
                'id_usuario' => 2109,
                'fecha' => '2019-12-10 06:19:00',
                'ejecutado' => 0,
            ),
            169 => 
            array (
                'id_alarmas' => 1648,
                'id_cotizacion' => 15,
                'precio' => 143.8,
                'inicio' => 142.878,
                'id_usuario' => 2109,
                'fecha' => '2019-12-10 06:20:00',
                'ejecutado' => 0,
            ),
            170 => 
            array (
                'id_alarmas' => 1649,
                'id_cotizacion' => 20,
                'precio' => 1.323,
                'inicio' => 131.547,
                'id_usuario' => 2109,
                'fecha' => '2019-12-10 06:24:00',
                'ejecutado' => 0,
            ),
            171 => 
            array (
                'id_alarmas' => 1650,
                'id_cotizacion' => 8,
                'precio' => 19.308,
                'inicio' => 192.836,
                'id_usuario' => 1491,
                'fecha' => '2019-12-10 07:07:00',
                'ejecutado' => 1,
            ),
            172 => 
            array (
                'id_alarmas' => 1651,
                'id_cotizacion' => 12,
                'precio' => 132.223,
                'inicio' => 132.277,
                'id_usuario' => 1684,
                'fecha' => '2019-12-10 07:57:00',
                'ejecutado' => 0,
            ),
            173 => 
            array (
                'id_alarmas' => 1654,
                'id_cotizacion' => 12,
                'precio' => 1.324,
                'inicio' => 132.363,
                'id_usuario' => 2028,
                'fecha' => '2019-12-10 08:58:00',
                'ejecutado' => 1,
            ),
            174 => 
            array (
                'id_alarmas' => 1655,
                'id_cotizacion' => 7,
                'precio' => 201.007,
                'inicio' => 201.353,
                'id_usuario' => 1959,
                'fecha' => '2019-12-10 12:38:00',
                'ejecutado' => 0,
            ),
            175 => 
            array (
                'id_alarmas' => 1656,
                'id_cotizacion' => 10,
                'precio' => 1480.0,
                'inicio' => 1467.3,
                'id_usuario' => 680,
                'fecha' => '2019-12-10 12:44:00',
                'ejecutado' => 0,
            ),
            176 => 
            array (
                'id_alarmas' => 1657,
                'id_cotizacion' => 12,
                'precio' => 13.237,
                'inicio' => 132.447,
                'id_usuario' => 680,
                'fecha' => '2019-12-10 12:47:00',
                'ejecutado' => 1,
            ),
            177 => 
            array (
                'id_alarmas' => 1658,
                'id_cotizacion' => 12,
                'precio' => 13.238,
                'inicio' => 132.437,
                'id_usuario' => 680,
                'fecha' => '2019-12-10 12:52:00',
                'ejecutado' => 1,
            ),
            178 => 
            array (
                'id_alarmas' => 1659,
                'id_cotizacion' => 3,
                'precio' => 7200.0,
                'inicio' => 7288.51,
                'id_usuario' => 1149,
                'fecha' => '2019-12-10 16:13:00',
                'ejecutado' => 0,
            ),
            179 => 
            array (
                'id_alarmas' => 1661,
                'id_cotizacion' => 6,
                'precio' => 108.69,
                'inicio' => 108.756,
                'id_usuario' => 1982,
                'fecha' => '2019-12-10 18:05:00',
                'ejecutado' => 0,
            ),
            180 => 
            array (
                'id_alarmas' => 1662,
                'id_cotizacion' => 11,
                'precio' => 173.906,
                'inicio' => 174.538,
                'id_usuario' => 1982,
                'fecha' => '2019-12-10 18:15:00',
                'ejecutado' => 0,
            ),
            181 => 
            array (
                'id_alarmas' => 1663,
                'id_cotizacion' => 6,
                'precio' => 108.43,
                'inicio' => 108.725,
                'id_usuario' => 2210,
                'fecha' => '2019-12-10 19:40:00',
                'ejecutado' => 0,
            ),
            182 => 
            array (
                'id_alarmas' => 1664,
                'id_cotizacion' => 6,
                'precio' => 108.705,
                'inicio' => 108.721,
                'id_usuario' => 1503,
                'fecha' => '2019-12-10 19:57:00',
                'ejecutado' => 0,
            ),
            183 => 
            array (
                'id_alarmas' => 1665,
                'id_cotizacion' => 9,
                'precio' => 27859.4,
                'inicio' => 27915.84,
                'id_usuario' => 853,
                'fecha' => '2019-12-10 20:09:00',
                'ejecutado' => 0,
            ),
            184 => 
            array (
                'id_alarmas' => 1666,
                'id_cotizacion' => 6,
                'precio' => 108.7,
                'inicio' => 108.737,
                'id_usuario' => 1810,
                'fecha' => '2019-12-10 20:18:00',
                'ejecutado' => 0,
            ),
        ));
                    
    }
}