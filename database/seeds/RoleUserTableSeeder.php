<?php

use Illuminate\Database\Seeder;

class RoleUserTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('role_user')->delete();
        
        \DB::table('role_user')->insert(array (
            0 => 
            array (
                'user_id' => 1,
                'role_id' => 2,
            ),
            1 => 
            array (
                'user_id' => 2,
                'role_id' => 2,
            ),
            2 => 
            array (
                'user_id' => 3,
                'role_id' => 2,
            ),
            3 => 
            array (
                'user_id' => 4,
                'role_id' => 2,
            ),
            4 => 
            array (
                'user_id' => 5,
                'role_id' => 2,
            ),
            5 => 
            array (
                'user_id' => 6,
                'role_id' => 2,
            ),
            6 => 
            array (
                'user_id' => 7,
                'role_id' => 2,
            ),
            7 => 
            array (
                'user_id' => 8,
                'role_id' => 2,
            ),
            8 => 
            array (
                'user_id' => 9,
                'role_id' => 2,
            ),
            9 => 
            array (
                'user_id' => 10,
                'role_id' => 2,
            ),
            10 => 
            array (
                'user_id' => 11,
                'role_id' => 2,
            ),
            11 => 
            array (
                'user_id' => 12,
                'role_id' => 2,
            ),
            12 => 
            array (
                'user_id' => 13,
                'role_id' => 2,
            ),
            13 => 
            array (
                'user_id' => 14,
                'role_id' => 2,
            ),
            14 => 
            array (
                'user_id' => 15,
                'role_id' => 2,
            ),
            15 => 
            array (
                'user_id' => 16,
                'role_id' => 2,
            ),
            16 => 
            array (
                'user_id' => 17,
                'role_id' => 2,
            ),
            17 => 
            array (
                'user_id' => 18,
                'role_id' => 2,
            ),
            18 => 
            array (
                'user_id' => 19,
                'role_id' => 2,
            ),
            19 => 
            array (
                'user_id' => 20,
                'role_id' => 2,
            ),
            20 => 
            array (
                'user_id' => 21,
                'role_id' => 2,
            ),
            21 => 
            array (
                'user_id' => 22,
                'role_id' => 2,
            ),
            22 => 
            array (
                'user_id' => 23,
                'role_id' => 2,
            ),
            23 => 
            array (
                'user_id' => 24,
                'role_id' => 2,
            ),
            24 => 
            array (
                'user_id' => 25,
                'role_id' => 2,
            ),
            25 => 
            array (
                'user_id' => 26,
                'role_id' => 2,
            ),
            26 => 
            array (
                'user_id' => 27,
                'role_id' => 2,
            ),
            27 => 
            array (
                'user_id' => 28,
                'role_id' => 2,
            ),
            28 => 
            array (
                'user_id' => 29,
                'role_id' => 2,
            ),
            29 => 
            array (
                'user_id' => 30,
                'role_id' => 2,
            ),
            30 => 
            array (
                'user_id' => 31,
                'role_id' => 2,
            ),
            31 => 
            array (
                'user_id' => 32,
                'role_id' => 2,
            ),
            32 => 
            array (
                'user_id' => 33,
                'role_id' => 2,
            ),
            33 => 
            array (
                'user_id' => 34,
                'role_id' => 2,
            ),
            34 => 
            array (
                'user_id' => 35,
                'role_id' => 2,
            ),
            35 => 
            array (
                'user_id' => 36,
                'role_id' => 2,
            ),
            36 => 
            array (
                'user_id' => 37,
                'role_id' => 2,
            ),
            37 => 
            array (
                'user_id' => 38,
                'role_id' => 2,
            ),
            38 => 
            array (
                'user_id' => 39,
                'role_id' => 2,
            ),
            39 => 
            array (
                'user_id' => 40,
                'role_id' => 2,
            ),
            40 => 
            array (
                'user_id' => 41,
                'role_id' => 2,
            ),
            41 => 
            array (
                'user_id' => 42,
                'role_id' => 2,
            ),
            42 => 
            array (
                'user_id' => 43,
                'role_id' => 2,
            ),
            43 => 
            array (
                'user_id' => 44,
                'role_id' => 2,
            ),
            44 => 
            array (
                'user_id' => 45,
                'role_id' => 2,
            ),
            45 => 
            array (
                'user_id' => 46,
                'role_id' => 2,
            ),
            46 => 
            array (
                'user_id' => 47,
                'role_id' => 2,
            ),
            47 => 
            array (
                'user_id' => 48,
                'role_id' => 2,
            ),
            48 => 
            array (
                'user_id' => 49,
                'role_id' => 2,
            ),
            49 => 
            array (
                'user_id' => 50,
                'role_id' => 2,
            ),
            50 => 
            array (
                'user_id' => 51,
                'role_id' => 2,
            ),
            51 => 
            array (
                'user_id' => 52,
                'role_id' => 2,
            ),
            52 => 
            array (
                'user_id' => 53,
                'role_id' => 2,
            ),
            53 => 
            array (
                'user_id' => 54,
                'role_id' => 2,
            ),
            54 => 
            array (
                'user_id' => 55,
                'role_id' => 2,
            ),
            55 => 
            array (
                'user_id' => 56,
                'role_id' => 2,
            ),
            56 => 
            array (
                'user_id' => 57,
                'role_id' => 2,
            ),
            57 => 
            array (
                'user_id' => 58,
                'role_id' => 2,
            ),
            58 => 
            array (
                'user_id' => 59,
                'role_id' => 2,
            ),
            59 => 
            array (
                'user_id' => 60,
                'role_id' => 2,
            ),
            60 => 
            array (
                'user_id' => 61,
                'role_id' => 2,
            ),
            61 => 
            array (
                'user_id' => 62,
                'role_id' => 2,
            ),
            62 => 
            array (
                'user_id' => 63,
                'role_id' => 2,
            ),
            63 => 
            array (
                'user_id' => 64,
                'role_id' => 2,
            ),
            64 => 
            array (
                'user_id' => 65,
                'role_id' => 2,
            ),
            65 => 
            array (
                'user_id' => 66,
                'role_id' => 2,
            ),
            66 => 
            array (
                'user_id' => 67,
                'role_id' => 2,
            ),
            67 => 
            array (
                'user_id' => 68,
                'role_id' => 2,
            ),
            68 => 
            array (
                'user_id' => 69,
                'role_id' => 2,
            ),
            69 => 
            array (
                'user_id' => 70,
                'role_id' => 2,
            ),
            70 => 
            array (
                'user_id' => 71,
                'role_id' => 2,
            ),
            71 => 
            array (
                'user_id' => 72,
                'role_id' => 2,
            ),
            72 => 
            array (
                'user_id' => 73,
                'role_id' => 2,
            ),
            73 => 
            array (
                'user_id' => 74,
                'role_id' => 2,
            ),
            74 => 
            array (
                'user_id' => 75,
                'role_id' => 2,
            ),
            75 => 
            array (
                'user_id' => 76,
                'role_id' => 2,
            ),
            76 => 
            array (
                'user_id' => 77,
                'role_id' => 2,
            ),
            77 => 
            array (
                'user_id' => 78,
                'role_id' => 2,
            ),
            78 => 
            array (
                'user_id' => 79,
                'role_id' => 2,
            ),
            79 => 
            array (
                'user_id' => 80,
                'role_id' => 2,
            ),
            80 => 
            array (
                'user_id' => 81,
                'role_id' => 2,
            ),
            81 => 
            array (
                'user_id' => 82,
                'role_id' => 2,
            ),
            82 => 
            array (
                'user_id' => 83,
                'role_id' => 2,
            ),
            83 => 
            array (
                'user_id' => 84,
                'role_id' => 2,
            ),
            84 => 
            array (
                'user_id' => 85,
                'role_id' => 2,
            ),
            85 => 
            array (
                'user_id' => 86,
                'role_id' => 2,
            ),
            86 => 
            array (
                'user_id' => 87,
                'role_id' => 2,
            ),
            87 => 
            array (
                'user_id' => 88,
                'role_id' => 2,
            ),
            88 => 
            array (
                'user_id' => 89,
                'role_id' => 2,
            ),
            89 => 
            array (
                'user_id' => 90,
                'role_id' => 2,
            ),
            90 => 
            array (
                'user_id' => 91,
                'role_id' => 2,
            ),
            91 => 
            array (
                'user_id' => 92,
                'role_id' => 2,
            ),
            92 => 
            array (
                'user_id' => 93,
                'role_id' => 2,
            ),
            93 => 
            array (
                'user_id' => 94,
                'role_id' => 2,
            ),
            94 => 
            array (
                'user_id' => 95,
                'role_id' => 2,
            ),
            95 => 
            array (
                'user_id' => 96,
                'role_id' => 2,
            ),
            96 => 
            array (
                'user_id' => 97,
                'role_id' => 2,
            ),
            97 => 
            array (
                'user_id' => 98,
                'role_id' => 2,
            ),
            98 => 
            array (
                'user_id' => 99,
                'role_id' => 2,
            ),
            99 => 
            array (
                'user_id' => 100,
                'role_id' => 2,
            ),
            100 => 
            array (
                'user_id' => 101,
                'role_id' => 2,
            ),
            101 => 
            array (
                'user_id' => 102,
                'role_id' => 2,
            ),
            102 => 
            array (
                'user_id' => 103,
                'role_id' => 2,
            ),
            103 => 
            array (
                'user_id' => 104,
                'role_id' => 2,
            ),
            104 => 
            array (
                'user_id' => 105,
                'role_id' => 2,
            ),
            105 => 
            array (
                'user_id' => 106,
                'role_id' => 2,
            ),
            106 => 
            array (
                'user_id' => 107,
                'role_id' => 2,
            ),
            107 => 
            array (
                'user_id' => 108,
                'role_id' => 2,
            ),
            108 => 
            array (
                'user_id' => 109,
                'role_id' => 2,
            ),
            109 => 
            array (
                'user_id' => 110,
                'role_id' => 2,
            ),
            110 => 
            array (
                'user_id' => 111,
                'role_id' => 2,
            ),
            111 => 
            array (
                'user_id' => 112,
                'role_id' => 2,
            ),
            112 => 
            array (
                'user_id' => 113,
                'role_id' => 2,
            ),
            113 => 
            array (
                'user_id' => 114,
                'role_id' => 2,
            ),
            114 => 
            array (
                'user_id' => 115,
                'role_id' => 2,
            ),
            115 => 
            array (
                'user_id' => 116,
                'role_id' => 2,
            ),
            116 => 
            array (
                'user_id' => 117,
                'role_id' => 2,
            ),
            117 => 
            array (
                'user_id' => 118,
                'role_id' => 2,
            ),
            118 => 
            array (
                'user_id' => 119,
                'role_id' => 2,
            ),
            119 => 
            array (
                'user_id' => 120,
                'role_id' => 2,
            ),
            120 => 
            array (
                'user_id' => 121,
                'role_id' => 2,
            ),
            121 => 
            array (
                'user_id' => 122,
                'role_id' => 2,
            ),
            122 => 
            array (
                'user_id' => 123,
                'role_id' => 2,
            ),
            123 => 
            array (
                'user_id' => 124,
                'role_id' => 2,
            ),
            124 => 
            array (
                'user_id' => 125,
                'role_id' => 2,
            ),
            125 => 
            array (
                'user_id' => 126,
                'role_id' => 2,
            ),
            126 => 
            array (
                'user_id' => 127,
                'role_id' => 2,
            ),
            127 => 
            array (
                'user_id' => 128,
                'role_id' => 2,
            ),
            128 => 
            array (
                'user_id' => 129,
                'role_id' => 2,
            ),
            129 => 
            array (
                'user_id' => 130,
                'role_id' => 2,
            ),
            130 => 
            array (
                'user_id' => 131,
                'role_id' => 2,
            ),
            131 => 
            array (
                'user_id' => 132,
                'role_id' => 2,
            ),
            132 => 
            array (
                'user_id' => 133,
                'role_id' => 2,
            ),
            133 => 
            array (
                'user_id' => 134,
                'role_id' => 2,
            ),
            134 => 
            array (
                'user_id' => 135,
                'role_id' => 2,
            ),
            135 => 
            array (
                'user_id' => 136,
                'role_id' => 2,
            ),
            136 => 
            array (
                'user_id' => 137,
                'role_id' => 2,
            ),
            137 => 
            array (
                'user_id' => 138,
                'role_id' => 2,
            ),
            138 => 
            array (
                'user_id' => 139,
                'role_id' => 2,
            ),
            139 => 
            array (
                'user_id' => 140,
                'role_id' => 2,
            ),
            140 => 
            array (
                'user_id' => 141,
                'role_id' => 2,
            ),
            141 => 
            array (
                'user_id' => 142,
                'role_id' => 2,
            ),
            142 => 
            array (
                'user_id' => 143,
                'role_id' => 2,
            ),
            143 => 
            array (
                'user_id' => 144,
                'role_id' => 2,
            ),
            144 => 
            array (
                'user_id' => 145,
                'role_id' => 2,
            ),
            145 => 
            array (
                'user_id' => 146,
                'role_id' => 2,
            ),
            146 => 
            array (
                'user_id' => 147,
                'role_id' => 2,
            ),
            147 => 
            array (
                'user_id' => 148,
                'role_id' => 2,
            ),
            148 => 
            array (
                'user_id' => 149,
                'role_id' => 2,
            ),
            149 => 
            array (
                'user_id' => 150,
                'role_id' => 2,
            ),
            150 => 
            array (
                'user_id' => 151,
                'role_id' => 2,
            ),
            151 => 
            array (
                'user_id' => 152,
                'role_id' => 2,
            ),
            152 => 
            array (
                'user_id' => 153,
                'role_id' => 2,
            ),
            153 => 
            array (
                'user_id' => 154,
                'role_id' => 2,
            ),
            154 => 
            array (
                'user_id' => 155,
                'role_id' => 2,
            ),
            155 => 
            array (
                'user_id' => 156,
                'role_id' => 2,
            ),
            156 => 
            array (
                'user_id' => 157,
                'role_id' => 2,
            ),
            157 => 
            array (
                'user_id' => 158,
                'role_id' => 2,
            ),
            158 => 
            array (
                'user_id' => 159,
                'role_id' => 2,
            ),
            159 => 
            array (
                'user_id' => 160,
                'role_id' => 2,
            ),
            160 => 
            array (
                'user_id' => 161,
                'role_id' => 2,
            ),
            161 => 
            array (
                'user_id' => 162,
                'role_id' => 2,
            ),
            162 => 
            array (
                'user_id' => 163,
                'role_id' => 2,
            ),
            163 => 
            array (
                'user_id' => 164,
                'role_id' => 2,
            ),
            164 => 
            array (
                'user_id' => 165,
                'role_id' => 2,
            ),
            165 => 
            array (
                'user_id' => 166,
                'role_id' => 2,
            ),
            166 => 
            array (
                'user_id' => 167,
                'role_id' => 2,
            ),
            167 => 
            array (
                'user_id' => 168,
                'role_id' => 2,
            ),
            168 => 
            array (
                'user_id' => 169,
                'role_id' => 2,
            ),
            169 => 
            array (
                'user_id' => 170,
                'role_id' => 2,
            ),
            170 => 
            array (
                'user_id' => 171,
                'role_id' => 2,
            ),
            171 => 
            array (
                'user_id' => 172,
                'role_id' => 2,
            ),
            172 => 
            array (
                'user_id' => 173,
                'role_id' => 2,
            ),
            173 => 
            array (
                'user_id' => 174,
                'role_id' => 2,
            ),
            174 => 
            array (
                'user_id' => 175,
                'role_id' => 2,
            ),
            175 => 
            array (
                'user_id' => 176,
                'role_id' => 2,
            ),
            176 => 
            array (
                'user_id' => 177,
                'role_id' => 2,
            ),
            177 => 
            array (
                'user_id' => 178,
                'role_id' => 2,
            ),
            178 => 
            array (
                'user_id' => 179,
                'role_id' => 2,
            ),
            179 => 
            array (
                'user_id' => 180,
                'role_id' => 2,
            ),
            180 => 
            array (
                'user_id' => 181,
                'role_id' => 2,
            ),
            181 => 
            array (
                'user_id' => 182,
                'role_id' => 2,
            ),
            182 => 
            array (
                'user_id' => 183,
                'role_id' => 2,
            ),
            183 => 
            array (
                'user_id' => 184,
                'role_id' => 2,
            ),
            184 => 
            array (
                'user_id' => 185,
                'role_id' => 2,
            ),
            185 => 
            array (
                'user_id' => 186,
                'role_id' => 2,
            ),
            186 => 
            array (
                'user_id' => 187,
                'role_id' => 2,
            ),
            187 => 
            array (
                'user_id' => 188,
                'role_id' => 2,
            ),
            188 => 
            array (
                'user_id' => 189,
                'role_id' => 2,
            ),
            189 => 
            array (
                'user_id' => 190,
                'role_id' => 2,
            ),
            190 => 
            array (
                'user_id' => 191,
                'role_id' => 2,
            ),
            191 => 
            array (
                'user_id' => 192,
                'role_id' => 2,
            ),
            192 => 
            array (
                'user_id' => 193,
                'role_id' => 2,
            ),
            193 => 
            array (
                'user_id' => 194,
                'role_id' => 2,
            ),
            194 => 
            array (
                'user_id' => 195,
                'role_id' => 2,
            ),
            195 => 
            array (
                'user_id' => 196,
                'role_id' => 2,
            ),
            196 => 
            array (
                'user_id' => 197,
                'role_id' => 2,
            ),
            197 => 
            array (
                'user_id' => 198,
                'role_id' => 2,
            ),
            198 => 
            array (
                'user_id' => 199,
                'role_id' => 2,
            ),
            199 => 
            array (
                'user_id' => 200,
                'role_id' => 2,
            ),
            200 => 
            array (
                'user_id' => 201,
                'role_id' => 2,
            ),
            201 => 
            array (
                'user_id' => 202,
                'role_id' => 2,
            ),
            202 => 
            array (
                'user_id' => 203,
                'role_id' => 2,
            ),
            203 => 
            array (
                'user_id' => 204,
                'role_id' => 2,
            ),
            204 => 
            array (
                'user_id' => 205,
                'role_id' => 2,
            ),
            205 => 
            array (
                'user_id' => 206,
                'role_id' => 2,
            ),
            206 => 
            array (
                'user_id' => 207,
                'role_id' => 2,
            ),
            207 => 
            array (
                'user_id' => 208,
                'role_id' => 2,
            ),
            208 => 
            array (
                'user_id' => 209,
                'role_id' => 2,
            ),
            209 => 
            array (
                'user_id' => 210,
                'role_id' => 2,
            ),
            210 => 
            array (
                'user_id' => 211,
                'role_id' => 2,
            ),
            211 => 
            array (
                'user_id' => 212,
                'role_id' => 2,
            ),
            212 => 
            array (
                'user_id' => 213,
                'role_id' => 2,
            ),
            213 => 
            array (
                'user_id' => 214,
                'role_id' => 2,
            ),
            214 => 
            array (
                'user_id' => 215,
                'role_id' => 2,
            ),
            215 => 
            array (
                'user_id' => 216,
                'role_id' => 2,
            ),
            216 => 
            array (
                'user_id' => 217,
                'role_id' => 2,
            ),
            217 => 
            array (
                'user_id' => 218,
                'role_id' => 2,
            ),
            218 => 
            array (
                'user_id' => 219,
                'role_id' => 2,
            ),
            219 => 
            array (
                'user_id' => 220,
                'role_id' => 2,
            ),
            220 => 
            array (
                'user_id' => 221,
                'role_id' => 2,
            ),
            221 => 
            array (
                'user_id' => 222,
                'role_id' => 2,
            ),
            222 => 
            array (
                'user_id' => 223,
                'role_id' => 2,
            ),
            223 => 
            array (
                'user_id' => 224,
                'role_id' => 2,
            ),
            224 => 
            array (
                'user_id' => 225,
                'role_id' => 2,
            ),
            225 => 
            array (
                'user_id' => 226,
                'role_id' => 2,
            ),
            226 => 
            array (
                'user_id' => 227,
                'role_id' => 2,
            ),
            227 => 
            array (
                'user_id' => 228,
                'role_id' => 2,
            ),
            228 => 
            array (
                'user_id' => 229,
                'role_id' => 2,
            ),
            229 => 
            array (
                'user_id' => 230,
                'role_id' => 2,
            ),
            230 => 
            array (
                'user_id' => 231,
                'role_id' => 2,
            ),
            231 => 
            array (
                'user_id' => 232,
                'role_id' => 2,
            ),
            232 => 
            array (
                'user_id' => 233,
                'role_id' => 2,
            ),
            233 => 
            array (
                'user_id' => 234,
                'role_id' => 2,
            ),
            234 => 
            array (
                'user_id' => 235,
                'role_id' => 2,
            ),
            235 => 
            array (
                'user_id' => 236,
                'role_id' => 2,
            ),
            236 => 
            array (
                'user_id' => 237,
                'role_id' => 2,
            ),
            237 => 
            array (
                'user_id' => 238,
                'role_id' => 2,
            ),
            238 => 
            array (
                'user_id' => 239,
                'role_id' => 2,
            ),
            239 => 
            array (
                'user_id' => 240,
                'role_id' => 2,
            ),
            240 => 
            array (
                'user_id' => 241,
                'role_id' => 2,
            ),
            241 => 
            array (
                'user_id' => 242,
                'role_id' => 2,
            ),
            242 => 
            array (
                'user_id' => 243,
                'role_id' => 2,
            ),
            243 => 
            array (
                'user_id' => 244,
                'role_id' => 2,
            ),
            244 => 
            array (
                'user_id' => 245,
                'role_id' => 2,
            ),
            245 => 
            array (
                'user_id' => 246,
                'role_id' => 2,
            ),
            246 => 
            array (
                'user_id' => 247,
                'role_id' => 2,
            ),
            247 => 
            array (
                'user_id' => 248,
                'role_id' => 2,
            ),
            248 => 
            array (
                'user_id' => 249,
                'role_id' => 2,
            ),
            249 => 
            array (
                'user_id' => 250,
                'role_id' => 2,
            ),
            250 => 
            array (
                'user_id' => 251,
                'role_id' => 2,
            ),
            251 => 
            array (
                'user_id' => 252,
                'role_id' => 2,
            ),
            252 => 
            array (
                'user_id' => 253,
                'role_id' => 2,
            ),
            253 => 
            array (
                'user_id' => 254,
                'role_id' => 2,
            ),
            254 => 
            array (
                'user_id' => 255,
                'role_id' => 2,
            ),
            255 => 
            array (
                'user_id' => 256,
                'role_id' => 2,
            ),
            256 => 
            array (
                'user_id' => 257,
                'role_id' => 2,
            ),
            257 => 
            array (
                'user_id' => 258,
                'role_id' => 2,
            ),
            258 => 
            array (
                'user_id' => 259,
                'role_id' => 2,
            ),
            259 => 
            array (
                'user_id' => 260,
                'role_id' => 2,
            ),
            260 => 
            array (
                'user_id' => 261,
                'role_id' => 2,
            ),
            261 => 
            array (
                'user_id' => 262,
                'role_id' => 2,
            ),
            262 => 
            array (
                'user_id' => 263,
                'role_id' => 2,
            ),
            263 => 
            array (
                'user_id' => 264,
                'role_id' => 2,
            ),
            264 => 
            array (
                'user_id' => 265,
                'role_id' => 2,
            ),
            265 => 
            array (
                'user_id' => 266,
                'role_id' => 2,
            ),
            266 => 
            array (
                'user_id' => 267,
                'role_id' => 2,
            ),
            267 => 
            array (
                'user_id' => 268,
                'role_id' => 2,
            ),
            268 => 
            array (
                'user_id' => 269,
                'role_id' => 2,
            ),
            269 => 
            array (
                'user_id' => 270,
                'role_id' => 2,
            ),
            270 => 
            array (
                'user_id' => 271,
                'role_id' => 2,
            ),
            271 => 
            array (
                'user_id' => 272,
                'role_id' => 2,
            ),
            272 => 
            array (
                'user_id' => 273,
                'role_id' => 2,
            ),
            273 => 
            array (
                'user_id' => 274,
                'role_id' => 2,
            ),
            274 => 
            array (
                'user_id' => 275,
                'role_id' => 2,
            ),
            275 => 
            array (
                'user_id' => 276,
                'role_id' => 2,
            ),
            276 => 
            array (
                'user_id' => 277,
                'role_id' => 2,
            ),
            277 => 
            array (
                'user_id' => 278,
                'role_id' => 2,
            ),
            278 => 
            array (
                'user_id' => 279,
                'role_id' => 2,
            ),
            279 => 
            array (
                'user_id' => 280,
                'role_id' => 2,
            ),
            280 => 
            array (
                'user_id' => 281,
                'role_id' => 2,
            ),
            281 => 
            array (
                'user_id' => 282,
                'role_id' => 2,
            ),
            282 => 
            array (
                'user_id' => 283,
                'role_id' => 2,
            ),
            283 => 
            array (
                'user_id' => 284,
                'role_id' => 2,
            ),
            284 => 
            array (
                'user_id' => 285,
                'role_id' => 2,
            ),
            285 => 
            array (
                'user_id' => 286,
                'role_id' => 2,
            ),
            286 => 
            array (
                'user_id' => 287,
                'role_id' => 2,
            ),
            287 => 
            array (
                'user_id' => 288,
                'role_id' => 2,
            ),
            288 => 
            array (
                'user_id' => 289,
                'role_id' => 2,
            ),
            289 => 
            array (
                'user_id' => 290,
                'role_id' => 2,
            ),
            290 => 
            array (
                'user_id' => 291,
                'role_id' => 2,
            ),
            291 => 
            array (
                'user_id' => 292,
                'role_id' => 2,
            ),
            292 => 
            array (
                'user_id' => 293,
                'role_id' => 2,
            ),
            293 => 
            array (
                'user_id' => 294,
                'role_id' => 2,
            ),
            294 => 
            array (
                'user_id' => 295,
                'role_id' => 2,
            ),
            295 => 
            array (
                'user_id' => 296,
                'role_id' => 2,
            ),
            296 => 
            array (
                'user_id' => 297,
                'role_id' => 2,
            ),
            297 => 
            array (
                'user_id' => 298,
                'role_id' => 2,
            ),
            298 => 
            array (
                'user_id' => 299,
                'role_id' => 2,
            ),
            299 => 
            array (
                'user_id' => 300,
                'role_id' => 2,
            ),
            300 => 
            array (
                'user_id' => 301,
                'role_id' => 2,
            ),
            301 => 
            array (
                'user_id' => 302,
                'role_id' => 2,
            ),
            302 => 
            array (
                'user_id' => 303,
                'role_id' => 2,
            ),
            303 => 
            array (
                'user_id' => 304,
                'role_id' => 2,
            ),
            304 => 
            array (
                'user_id' => 305,
                'role_id' => 2,
            ),
            305 => 
            array (
                'user_id' => 306,
                'role_id' => 2,
            ),
            306 => 
            array (
                'user_id' => 307,
                'role_id' => 2,
            ),
            307 => 
            array (
                'user_id' => 308,
                'role_id' => 2,
            ),
            308 => 
            array (
                'user_id' => 309,
                'role_id' => 2,
            ),
            309 => 
            array (
                'user_id' => 310,
                'role_id' => 2,
            ),
            310 => 
            array (
                'user_id' => 311,
                'role_id' => 2,
            ),
            311 => 
            array (
                'user_id' => 312,
                'role_id' => 2,
            ),
            312 => 
            array (
                'user_id' => 313,
                'role_id' => 2,
            ),
            313 => 
            array (
                'user_id' => 314,
                'role_id' => 2,
            ),
            314 => 
            array (
                'user_id' => 315,
                'role_id' => 2,
            ),
            315 => 
            array (
                'user_id' => 316,
                'role_id' => 2,
            ),
            316 => 
            array (
                'user_id' => 317,
                'role_id' => 2,
            ),
            317 => 
            array (
                'user_id' => 318,
                'role_id' => 2,
            ),
            318 => 
            array (
                'user_id' => 319,
                'role_id' => 2,
            ),
            319 => 
            array (
                'user_id' => 320,
                'role_id' => 2,
            ),
            320 => 
            array (
                'user_id' => 321,
                'role_id' => 2,
            ),
            321 => 
            array (
                'user_id' => 322,
                'role_id' => 2,
            ),
            322 => 
            array (
                'user_id' => 323,
                'role_id' => 2,
            ),
            323 => 
            array (
                'user_id' => 324,
                'role_id' => 2,
            ),
            324 => 
            array (
                'user_id' => 325,
                'role_id' => 2,
            ),
            325 => 
            array (
                'user_id' => 326,
                'role_id' => 2,
            ),
            326 => 
            array (
                'user_id' => 327,
                'role_id' => 2,
            ),
            327 => 
            array (
                'user_id' => 328,
                'role_id' => 2,
            ),
            328 => 
            array (
                'user_id' => 329,
                'role_id' => 2,
            ),
            329 => 
            array (
                'user_id' => 330,
                'role_id' => 2,
            ),
            330 => 
            array (
                'user_id' => 331,
                'role_id' => 2,
            ),
            331 => 
            array (
                'user_id' => 332,
                'role_id' => 2,
            ),
            332 => 
            array (
                'user_id' => 333,
                'role_id' => 2,
            ),
            333 => 
            array (
                'user_id' => 334,
                'role_id' => 2,
            ),
            334 => 
            array (
                'user_id' => 335,
                'role_id' => 2,
            ),
            335 => 
            array (
                'user_id' => 336,
                'role_id' => 2,
            ),
            336 => 
            array (
                'user_id' => 337,
                'role_id' => 2,
            ),
            337 => 
            array (
                'user_id' => 338,
                'role_id' => 2,
            ),
            338 => 
            array (
                'user_id' => 339,
                'role_id' => 2,
            ),
            339 => 
            array (
                'user_id' => 340,
                'role_id' => 2,
            ),
            340 => 
            array (
                'user_id' => 341,
                'role_id' => 2,
            ),
            341 => 
            array (
                'user_id' => 342,
                'role_id' => 2,
            ),
            342 => 
            array (
                'user_id' => 343,
                'role_id' => 2,
            ),
            343 => 
            array (
                'user_id' => 344,
                'role_id' => 2,
            ),
            344 => 
            array (
                'user_id' => 345,
                'role_id' => 2,
            ),
            345 => 
            array (
                'user_id' => 346,
                'role_id' => 2,
            ),
            346 => 
            array (
                'user_id' => 347,
                'role_id' => 2,
            ),
            347 => 
            array (
                'user_id' => 348,
                'role_id' => 2,
            ),
            348 => 
            array (
                'user_id' => 349,
                'role_id' => 2,
            ),
            349 => 
            array (
                'user_id' => 350,
                'role_id' => 2,
            ),
            350 => 
            array (
                'user_id' => 351,
                'role_id' => 1,
            ),
            351 => 
            array (
                'user_id' => 352,
                'role_id' => 2,
            ),
            352 => 
            array (
                'user_id' => 353,
                'role_id' => 2,
            ),
            353 => 
            array (
                'user_id' => 354,
                'role_id' => 2,
            ),
            354 => 
            array (
                'user_id' => 355,
                'role_id' => 2,
            ),
            355 => 
            array (
                'user_id' => 356,
                'role_id' => 2,
            ),
            356 => 
            array (
                'user_id' => 357,
                'role_id' => 2,
            ),
            357 => 
            array (
                'user_id' => 358,
                'role_id' => 2,
            ),
            358 => 
            array (
                'user_id' => 359,
                'role_id' => 2,
            ),
            359 => 
            array (
                'user_id' => 360,
                'role_id' => 2,
            ),
            360 => 
            array (
                'user_id' => 361,
                'role_id' => 2,
            ),
            361 => 
            array (
                'user_id' => 362,
                'role_id' => 2,
            ),
            362 => 
            array (
                'user_id' => 363,
                'role_id' => 2,
            ),
            363 => 
            array (
                'user_id' => 364,
                'role_id' => 2,
            ),
            364 => 
            array (
                'user_id' => 365,
                'role_id' => 2,
            ),
            365 => 
            array (
                'user_id' => 366,
                'role_id' => 2,
            ),
            366 => 
            array (
                'user_id' => 367,
                'role_id' => 2,
            ),
            367 => 
            array (
                'user_id' => 368,
                'role_id' => 2,
            ),
            368 => 
            array (
                'user_id' => 369,
                'role_id' => 2,
            ),
            369 => 
            array (
                'user_id' => 370,
                'role_id' => 2,
            ),
            370 => 
            array (
                'user_id' => 371,
                'role_id' => 2,
            ),
            371 => 
            array (
                'user_id' => 372,
                'role_id' => 2,
            ),
            372 => 
            array (
                'user_id' => 373,
                'role_id' => 2,
            ),
            373 => 
            array (
                'user_id' => 374,
                'role_id' => 2,
            ),
            374 => 
            array (
                'user_id' => 375,
                'role_id' => 2,
            ),
            375 => 
            array (
                'user_id' => 376,
                'role_id' => 2,
            ),
            376 => 
            array (
                'user_id' => 377,
                'role_id' => 2,
            ),
            377 => 
            array (
                'user_id' => 378,
                'role_id' => 2,
            ),
            378 => 
            array (
                'user_id' => 379,
                'role_id' => 2,
            ),
            379 => 
            array (
                'user_id' => 380,
                'role_id' => 2,
            ),
            380 => 
            array (
                'user_id' => 381,
                'role_id' => 2,
            ),
            381 => 
            array (
                'user_id' => 382,
                'role_id' => 2,
            ),
            382 => 
            array (
                'user_id' => 383,
                'role_id' => 2,
            ),
            383 => 
            array (
                'user_id' => 384,
                'role_id' => 2,
            ),
            384 => 
            array (
                'user_id' => 385,
                'role_id' => 2,
            ),
            385 => 
            array (
                'user_id' => 386,
                'role_id' => 2,
            ),
            386 => 
            array (
                'user_id' => 387,
                'role_id' => 2,
            ),
            387 => 
            array (
                'user_id' => 388,
                'role_id' => 2,
            ),
            388 => 
            array (
                'user_id' => 389,
                'role_id' => 2,
            ),
            389 => 
            array (
                'user_id' => 390,
                'role_id' => 2,
            ),
            390 => 
            array (
                'user_id' => 391,
                'role_id' => 2,
            ),
            391 => 
            array (
                'user_id' => 392,
                'role_id' => 2,
            ),
            392 => 
            array (
                'user_id' => 393,
                'role_id' => 2,
            ),
            393 => 
            array (
                'user_id' => 394,
                'role_id' => 2,
            ),
            394 => 
            array (
                'user_id' => 395,
                'role_id' => 2,
            ),
            395 => 
            array (
                'user_id' => 396,
                'role_id' => 2,
            ),
            396 => 
            array (
                'user_id' => 397,
                'role_id' => 2,
            ),
            397 => 
            array (
                'user_id' => 398,
                'role_id' => 2,
            ),
            398 => 
            array (
                'user_id' => 399,
                'role_id' => 2,
            ),
            399 => 
            array (
                'user_id' => 400,
                'role_id' => 2,
            ),
            400 => 
            array (
                'user_id' => 401,
                'role_id' => 2,
            ),
            401 => 
            array (
                'user_id' => 402,
                'role_id' => 2,
            ),
            402 => 
            array (
                'user_id' => 403,
                'role_id' => 2,
            ),
            403 => 
            array (
                'user_id' => 404,
                'role_id' => 2,
            ),
            404 => 
            array (
                'user_id' => 405,
                'role_id' => 2,
            ),
            405 => 
            array (
                'user_id' => 406,
                'role_id' => 2,
            ),
            406 => 
            array (
                'user_id' => 407,
                'role_id' => 2,
            ),
            407 => 
            array (
                'user_id' => 408,
                'role_id' => 2,
            ),
            408 => 
            array (
                'user_id' => 409,
                'role_id' => 2,
            ),
            409 => 
            array (
                'user_id' => 410,
                'role_id' => 2,
            ),
            410 => 
            array (
                'user_id' => 411,
                'role_id' => 2,
            ),
            411 => 
            array (
                'user_id' => 412,
                'role_id' => 2,
            ),
            412 => 
            array (
                'user_id' => 413,
                'role_id' => 2,
            ),
            413 => 
            array (
                'user_id' => 414,
                'role_id' => 2,
            ),
            414 => 
            array (
                'user_id' => 415,
                'role_id' => 2,
            ),
            415 => 
            array (
                'user_id' => 416,
                'role_id' => 2,
            ),
            416 => 
            array (
                'user_id' => 417,
                'role_id' => 2,
            ),
            417 => 
            array (
                'user_id' => 418,
                'role_id' => 2,
            ),
            418 => 
            array (
                'user_id' => 419,
                'role_id' => 2,
            ),
            419 => 
            array (
                'user_id' => 420,
                'role_id' => 2,
            ),
            420 => 
            array (
                'user_id' => 421,
                'role_id' => 2,
            ),
            421 => 
            array (
                'user_id' => 422,
                'role_id' => 2,
            ),
            422 => 
            array (
                'user_id' => 423,
                'role_id' => 2,
            ),
            423 => 
            array (
                'user_id' => 424,
                'role_id' => 2,
            ),
            424 => 
            array (
                'user_id' => 425,
                'role_id' => 2,
            ),
            425 => 
            array (
                'user_id' => 426,
                'role_id' => 2,
            ),
            426 => 
            array (
                'user_id' => 427,
                'role_id' => 2,
            ),
            427 => 
            array (
                'user_id' => 428,
                'role_id' => 2,
            ),
            428 => 
            array (
                'user_id' => 429,
                'role_id' => 2,
            ),
            429 => 
            array (
                'user_id' => 430,
                'role_id' => 2,
            ),
            430 => 
            array (
                'user_id' => 431,
                'role_id' => 2,
            ),
            431 => 
            array (
                'user_id' => 432,
                'role_id' => 2,
            ),
            432 => 
            array (
                'user_id' => 433,
                'role_id' => 2,
            ),
            433 => 
            array (
                'user_id' => 434,
                'role_id' => 2,
            ),
            434 => 
            array (
                'user_id' => 435,
                'role_id' => 2,
            ),
            435 => 
            array (
                'user_id' => 436,
                'role_id' => 2,
            ),
            436 => 
            array (
                'user_id' => 437,
                'role_id' => 2,
            ),
            437 => 
            array (
                'user_id' => 438,
                'role_id' => 2,
            ),
            438 => 
            array (
                'user_id' => 439,
                'role_id' => 2,
            ),
            439 => 
            array (
                'user_id' => 440,
                'role_id' => 2,
            ),
            440 => 
            array (
                'user_id' => 441,
                'role_id' => 2,
            ),
            441 => 
            array (
                'user_id' => 442,
                'role_id' => 2,
            ),
            442 => 
            array (
                'user_id' => 443,
                'role_id' => 2,
            ),
            443 => 
            array (
                'user_id' => 444,
                'role_id' => 2,
            ),
            444 => 
            array (
                'user_id' => 445,
                'role_id' => 2,
            ),
            445 => 
            array (
                'user_id' => 446,
                'role_id' => 2,
            ),
            446 => 
            array (
                'user_id' => 447,
                'role_id' => 2,
            ),
            447 => 
            array (
                'user_id' => 448,
                'role_id' => 2,
            ),
            448 => 
            array (
                'user_id' => 449,
                'role_id' => 2,
            ),
            449 => 
            array (
                'user_id' => 450,
                'role_id' => 2,
            ),
            450 => 
            array (
                'user_id' => 451,
                'role_id' => 2,
            ),
            451 => 
            array (
                'user_id' => 452,
                'role_id' => 2,
            ),
            452 => 
            array (
                'user_id' => 453,
                'role_id' => 2,
            ),
            453 => 
            array (
                'user_id' => 454,
                'role_id' => 2,
            ),
            454 => 
            array (
                'user_id' => 455,
                'role_id' => 2,
            ),
            455 => 
            array (
                'user_id' => 456,
                'role_id' => 2,
            ),
            456 => 
            array (
                'user_id' => 457,
                'role_id' => 2,
            ),
            457 => 
            array (
                'user_id' => 458,
                'role_id' => 2,
            ),
            458 => 
            array (
                'user_id' => 459,
                'role_id' => 2,
            ),
            459 => 
            array (
                'user_id' => 460,
                'role_id' => 2,
            ),
            460 => 
            array (
                'user_id' => 461,
                'role_id' => 2,
            ),
            461 => 
            array (
                'user_id' => 462,
                'role_id' => 2,
            ),
            462 => 
            array (
                'user_id' => 463,
                'role_id' => 2,
            ),
            463 => 
            array (
                'user_id' => 464,
                'role_id' => 2,
            ),
            464 => 
            array (
                'user_id' => 465,
                'role_id' => 2,
            ),
            465 => 
            array (
                'user_id' => 466,
                'role_id' => 2,
            ),
            466 => 
            array (
                'user_id' => 467,
                'role_id' => 2,
            ),
            467 => 
            array (
                'user_id' => 468,
                'role_id' => 2,
            ),
            468 => 
            array (
                'user_id' => 469,
                'role_id' => 2,
            ),
            469 => 
            array (
                'user_id' => 470,
                'role_id' => 2,
            ),
            470 => 
            array (
                'user_id' => 471,
                'role_id' => 2,
            ),
            471 => 
            array (
                'user_id' => 472,
                'role_id' => 2,
            ),
            472 => 
            array (
                'user_id' => 473,
                'role_id' => 2,
            ),
            473 => 
            array (
                'user_id' => 474,
                'role_id' => 2,
            ),
            474 => 
            array (
                'user_id' => 475,
                'role_id' => 2,
            ),
            475 => 
            array (
                'user_id' => 476,
                'role_id' => 2,
            ),
            476 => 
            array (
                'user_id' => 477,
                'role_id' => 2,
            ),
            477 => 
            array (
                'user_id' => 478,
                'role_id' => 2,
            ),
            478 => 
            array (
                'user_id' => 479,
                'role_id' => 2,
            ),
            479 => 
            array (
                'user_id' => 480,
                'role_id' => 2,
            ),
            480 => 
            array (
                'user_id' => 481,
                'role_id' => 2,
            ),
            481 => 
            array (
                'user_id' => 482,
                'role_id' => 2,
            ),
            482 => 
            array (
                'user_id' => 483,
                'role_id' => 2,
            ),
            483 => 
            array (
                'user_id' => 484,
                'role_id' => 2,
            ),
            484 => 
            array (
                'user_id' => 485,
                'role_id' => 2,
            ),
            485 => 
            array (
                'user_id' => 486,
                'role_id' => 2,
            ),
            486 => 
            array (
                'user_id' => 487,
                'role_id' => 2,
            ),
            487 => 
            array (
                'user_id' => 488,
                'role_id' => 2,
            ),
            488 => 
            array (
                'user_id' => 489,
                'role_id' => 2,
            ),
            489 => 
            array (
                'user_id' => 490,
                'role_id' => 2,
            ),
            490 => 
            array (
                'user_id' => 491,
                'role_id' => 2,
            ),
            491 => 
            array (
                'user_id' => 492,
                'role_id' => 2,
            ),
            492 => 
            array (
                'user_id' => 493,
                'role_id' => 2,
            ),
            493 => 
            array (
                'user_id' => 494,
                'role_id' => 2,
            ),
            494 => 
            array (
                'user_id' => 495,
                'role_id' => 2,
            ),
            495 => 
            array (
                'user_id' => 496,
                'role_id' => 2,
            ),
            496 => 
            array (
                'user_id' => 497,
                'role_id' => 2,
            ),
            497 => 
            array (
                'user_id' => 498,
                'role_id' => 2,
            ),
            498 => 
            array (
                'user_id' => 499,
                'role_id' => 2,
            ),
            499 => 
            array (
                'user_id' => 500,
                'role_id' => 2,
            ),
        ));
        \DB::table('role_user')->insert(array (
            0 => 
            array (
                'user_id' => 501,
                'role_id' => 2,
            ),
            1 => 
            array (
                'user_id' => 502,
                'role_id' => 2,
            ),
            2 => 
            array (
                'user_id' => 503,
                'role_id' => 2,
            ),
            3 => 
            array (
                'user_id' => 504,
                'role_id' => 2,
            ),
            4 => 
            array (
                'user_id' => 505,
                'role_id' => 2,
            ),
            5 => 
            array (
                'user_id' => 506,
                'role_id' => 2,
            ),
            6 => 
            array (
                'user_id' => 507,
                'role_id' => 2,
            ),
            7 => 
            array (
                'user_id' => 508,
                'role_id' => 2,
            ),
            8 => 
            array (
                'user_id' => 509,
                'role_id' => 2,
            ),
            9 => 
            array (
                'user_id' => 510,
                'role_id' => 2,
            ),
            10 => 
            array (
                'user_id' => 511,
                'role_id' => 2,
            ),
            11 => 
            array (
                'user_id' => 512,
                'role_id' => 2,
            ),
            12 => 
            array (
                'user_id' => 513,
                'role_id' => 2,
            ),
            13 => 
            array (
                'user_id' => 514,
                'role_id' => 2,
            ),
            14 => 
            array (
                'user_id' => 515,
                'role_id' => 2,
            ),
            15 => 
            array (
                'user_id' => 516,
                'role_id' => 2,
            ),
            16 => 
            array (
                'user_id' => 517,
                'role_id' => 2,
            ),
            17 => 
            array (
                'user_id' => 518,
                'role_id' => 2,
            ),
            18 => 
            array (
                'user_id' => 519,
                'role_id' => 2,
            ),
            19 => 
            array (
                'user_id' => 520,
                'role_id' => 2,
            ),
            20 => 
            array (
                'user_id' => 521,
                'role_id' => 2,
            ),
            21 => 
            array (
                'user_id' => 522,
                'role_id' => 2,
            ),
            22 => 
            array (
                'user_id' => 523,
                'role_id' => 2,
            ),
            23 => 
            array (
                'user_id' => 524,
                'role_id' => 2,
            ),
            24 => 
            array (
                'user_id' => 525,
                'role_id' => 2,
            ),
            25 => 
            array (
                'user_id' => 526,
                'role_id' => 2,
            ),
            26 => 
            array (
                'user_id' => 527,
                'role_id' => 2,
            ),
            27 => 
            array (
                'user_id' => 528,
                'role_id' => 2,
            ),
            28 => 
            array (
                'user_id' => 529,
                'role_id' => 2,
            ),
            29 => 
            array (
                'user_id' => 530,
                'role_id' => 2,
            ),
            30 => 
            array (
                'user_id' => 531,
                'role_id' => 2,
            ),
            31 => 
            array (
                'user_id' => 532,
                'role_id' => 2,
            ),
            32 => 
            array (
                'user_id' => 533,
                'role_id' => 2,
            ),
            33 => 
            array (
                'user_id' => 534,
                'role_id' => 2,
            ),
            34 => 
            array (
                'user_id' => 535,
                'role_id' => 2,
            ),
            35 => 
            array (
                'user_id' => 536,
                'role_id' => 2,
            ),
            36 => 
            array (
                'user_id' => 537,
                'role_id' => 2,
            ),
            37 => 
            array (
                'user_id' => 538,
                'role_id' => 2,
            ),
            38 => 
            array (
                'user_id' => 539,
                'role_id' => 2,
            ),
            39 => 
            array (
                'user_id' => 540,
                'role_id' => 2,
            ),
            40 => 
            array (
                'user_id' => 541,
                'role_id' => 2,
            ),
            41 => 
            array (
                'user_id' => 542,
                'role_id' => 2,
            ),
            42 => 
            array (
                'user_id' => 543,
                'role_id' => 2,
            ),
            43 => 
            array (
                'user_id' => 544,
                'role_id' => 2,
            ),
            44 => 
            array (
                'user_id' => 545,
                'role_id' => 2,
            ),
            45 => 
            array (
                'user_id' => 546,
                'role_id' => 2,
            ),
            46 => 
            array (
                'user_id' => 547,
                'role_id' => 2,
            ),
            47 => 
            array (
                'user_id' => 548,
                'role_id' => 2,
            ),
            48 => 
            array (
                'user_id' => 549,
                'role_id' => 2,
            ),
            49 => 
            array (
                'user_id' => 550,
                'role_id' => 2,
            ),
            50 => 
            array (
                'user_id' => 551,
                'role_id' => 2,
            ),
            51 => 
            array (
                'user_id' => 552,
                'role_id' => 2,
            ),
            52 => 
            array (
                'user_id' => 553,
                'role_id' => 2,
            ),
            53 => 
            array (
                'user_id' => 554,
                'role_id' => 2,
            ),
            54 => 
            array (
                'user_id' => 555,
                'role_id' => 2,
            ),
            55 => 
            array (
                'user_id' => 556,
                'role_id' => 2,
            ),
            56 => 
            array (
                'user_id' => 557,
                'role_id' => 2,
            ),
            57 => 
            array (
                'user_id' => 558,
                'role_id' => 2,
            ),
            58 => 
            array (
                'user_id' => 559,
                'role_id' => 2,
            ),
            59 => 
            array (
                'user_id' => 560,
                'role_id' => 2,
            ),
            60 => 
            array (
                'user_id' => 561,
                'role_id' => 2,
            ),
            61 => 
            array (
                'user_id' => 562,
                'role_id' => 2,
            ),
            62 => 
            array (
                'user_id' => 563,
                'role_id' => 2,
            ),
            63 => 
            array (
                'user_id' => 564,
                'role_id' => 2,
            ),
            64 => 
            array (
                'user_id' => 565,
                'role_id' => 2,
            ),
            65 => 
            array (
                'user_id' => 566,
                'role_id' => 2,
            ),
            66 => 
            array (
                'user_id' => 567,
                'role_id' => 2,
            ),
            67 => 
            array (
                'user_id' => 568,
                'role_id' => 2,
            ),
            68 => 
            array (
                'user_id' => 569,
                'role_id' => 2,
            ),
            69 => 
            array (
                'user_id' => 570,
                'role_id' => 2,
            ),
            70 => 
            array (
                'user_id' => 571,
                'role_id' => 2,
            ),
            71 => 
            array (
                'user_id' => 572,
                'role_id' => 2,
            ),
            72 => 
            array (
                'user_id' => 573,
                'role_id' => 2,
            ),
            73 => 
            array (
                'user_id' => 574,
                'role_id' => 2,
            ),
            74 => 
            array (
                'user_id' => 575,
                'role_id' => 2,
            ),
            75 => 
            array (
                'user_id' => 576,
                'role_id' => 2,
            ),
            76 => 
            array (
                'user_id' => 577,
                'role_id' => 2,
            ),
            77 => 
            array (
                'user_id' => 578,
                'role_id' => 2,
            ),
            78 => 
            array (
                'user_id' => 579,
                'role_id' => 2,
            ),
            79 => 
            array (
                'user_id' => 580,
                'role_id' => 2,
            ),
            80 => 
            array (
                'user_id' => 581,
                'role_id' => 2,
            ),
            81 => 
            array (
                'user_id' => 582,
                'role_id' => 2,
            ),
            82 => 
            array (
                'user_id' => 583,
                'role_id' => 2,
            ),
            83 => 
            array (
                'user_id' => 584,
                'role_id' => 2,
            ),
            84 => 
            array (
                'user_id' => 585,
                'role_id' => 2,
            ),
            85 => 
            array (
                'user_id' => 586,
                'role_id' => 2,
            ),
            86 => 
            array (
                'user_id' => 587,
                'role_id' => 2,
            ),
            87 => 
            array (
                'user_id' => 588,
                'role_id' => 2,
            ),
            88 => 
            array (
                'user_id' => 589,
                'role_id' => 2,
            ),
            89 => 
            array (
                'user_id' => 590,
                'role_id' => 2,
            ),
            90 => 
            array (
                'user_id' => 591,
                'role_id' => 2,
            ),
            91 => 
            array (
                'user_id' => 592,
                'role_id' => 2,
            ),
            92 => 
            array (
                'user_id' => 593,
                'role_id' => 2,
            ),
            93 => 
            array (
                'user_id' => 594,
                'role_id' => 2,
            ),
            94 => 
            array (
                'user_id' => 595,
                'role_id' => 2,
            ),
            95 => 
            array (
                'user_id' => 596,
                'role_id' => 2,
            ),
            96 => 
            array (
                'user_id' => 597,
                'role_id' => 2,
            ),
            97 => 
            array (
                'user_id' => 598,
                'role_id' => 2,
            ),
            98 => 
            array (
                'user_id' => 599,
                'role_id' => 2,
            ),
            99 => 
            array (
                'user_id' => 600,
                'role_id' => 2,
            ),
            100 => 
            array (
                'user_id' => 601,
                'role_id' => 2,
            ),
            101 => 
            array (
                'user_id' => 602,
                'role_id' => 2,
            ),
            102 => 
            array (
                'user_id' => 603,
                'role_id' => 2,
            ),
            103 => 
            array (
                'user_id' => 604,
                'role_id' => 2,
            ),
            104 => 
            array (
                'user_id' => 605,
                'role_id' => 2,
            ),
            105 => 
            array (
                'user_id' => 606,
                'role_id' => 2,
            ),
            106 => 
            array (
                'user_id' => 607,
                'role_id' => 2,
            ),
            107 => 
            array (
                'user_id' => 608,
                'role_id' => 2,
            ),
            108 => 
            array (
                'user_id' => 609,
                'role_id' => 2,
            ),
            109 => 
            array (
                'user_id' => 610,
                'role_id' => 2,
            ),
            110 => 
            array (
                'user_id' => 611,
                'role_id' => 2,
            ),
            111 => 
            array (
                'user_id' => 612,
                'role_id' => 2,
            ),
            112 => 
            array (
                'user_id' => 613,
                'role_id' => 2,
            ),
            113 => 
            array (
                'user_id' => 614,
                'role_id' => 2,
            ),
            114 => 
            array (
                'user_id' => 615,
                'role_id' => 2,
            ),
            115 => 
            array (
                'user_id' => 616,
                'role_id' => 2,
            ),
            116 => 
            array (
                'user_id' => 617,
                'role_id' => 2,
            ),
            117 => 
            array (
                'user_id' => 618,
                'role_id' => 2,
            ),
            118 => 
            array (
                'user_id' => 619,
                'role_id' => 2,
            ),
            119 => 
            array (
                'user_id' => 620,
                'role_id' => 2,
            ),
            120 => 
            array (
                'user_id' => 621,
                'role_id' => 2,
            ),
            121 => 
            array (
                'user_id' => 622,
                'role_id' => 2,
            ),
            122 => 
            array (
                'user_id' => 623,
                'role_id' => 2,
            ),
            123 => 
            array (
                'user_id' => 624,
                'role_id' => 2,
            ),
            124 => 
            array (
                'user_id' => 625,
                'role_id' => 2,
            ),
            125 => 
            array (
                'user_id' => 626,
                'role_id' => 2,
            ),
            126 => 
            array (
                'user_id' => 627,
                'role_id' => 2,
            ),
            127 => 
            array (
                'user_id' => 628,
                'role_id' => 2,
            ),
            128 => 
            array (
                'user_id' => 629,
                'role_id' => 2,
            ),
            129 => 
            array (
                'user_id' => 630,
                'role_id' => 2,
            ),
            130 => 
            array (
                'user_id' => 631,
                'role_id' => 2,
            ),
            131 => 
            array (
                'user_id' => 632,
                'role_id' => 2,
            ),
            132 => 
            array (
                'user_id' => 633,
                'role_id' => 2,
            ),
            133 => 
            array (
                'user_id' => 634,
                'role_id' => 2,
            ),
            134 => 
            array (
                'user_id' => 635,
                'role_id' => 2,
            ),
            135 => 
            array (
                'user_id' => 636,
                'role_id' => 2,
            ),
            136 => 
            array (
                'user_id' => 637,
                'role_id' => 2,
            ),
            137 => 
            array (
                'user_id' => 638,
                'role_id' => 2,
            ),
            138 => 
            array (
                'user_id' => 639,
                'role_id' => 2,
            ),
            139 => 
            array (
                'user_id' => 640,
                'role_id' => 2,
            ),
            140 => 
            array (
                'user_id' => 641,
                'role_id' => 2,
            ),
            141 => 
            array (
                'user_id' => 642,
                'role_id' => 2,
            ),
            142 => 
            array (
                'user_id' => 643,
                'role_id' => 2,
            ),
            143 => 
            array (
                'user_id' => 644,
                'role_id' => 2,
            ),
            144 => 
            array (
                'user_id' => 645,
                'role_id' => 2,
            ),
            145 => 
            array (
                'user_id' => 646,
                'role_id' => 2,
            ),
            146 => 
            array (
                'user_id' => 647,
                'role_id' => 2,
            ),
            147 => 
            array (
                'user_id' => 648,
                'role_id' => 2,
            ),
            148 => 
            array (
                'user_id' => 649,
                'role_id' => 2,
            ),
            149 => 
            array (
                'user_id' => 650,
                'role_id' => 2,
            ),
            150 => 
            array (
                'user_id' => 651,
                'role_id' => 2,
            ),
            151 => 
            array (
                'user_id' => 652,
                'role_id' => 2,
            ),
            152 => 
            array (
                'user_id' => 653,
                'role_id' => 2,
            ),
            153 => 
            array (
                'user_id' => 654,
                'role_id' => 2,
            ),
            154 => 
            array (
                'user_id' => 655,
                'role_id' => 2,
            ),
            155 => 
            array (
                'user_id' => 656,
                'role_id' => 2,
            ),
            156 => 
            array (
                'user_id' => 657,
                'role_id' => 2,
            ),
            157 => 
            array (
                'user_id' => 658,
                'role_id' => 2,
            ),
            158 => 
            array (
                'user_id' => 659,
                'role_id' => 2,
            ),
            159 => 
            array (
                'user_id' => 660,
                'role_id' => 2,
            ),
            160 => 
            array (
                'user_id' => 661,
                'role_id' => 2,
            ),
            161 => 
            array (
                'user_id' => 662,
                'role_id' => 2,
            ),
            162 => 
            array (
                'user_id' => 663,
                'role_id' => 2,
            ),
            163 => 
            array (
                'user_id' => 664,
                'role_id' => 2,
            ),
            164 => 
            array (
                'user_id' => 665,
                'role_id' => 2,
            ),
            165 => 
            array (
                'user_id' => 666,
                'role_id' => 2,
            ),
            166 => 
            array (
                'user_id' => 667,
                'role_id' => 2,
            ),
            167 => 
            array (
                'user_id' => 668,
                'role_id' => 2,
            ),
            168 => 
            array (
                'user_id' => 669,
                'role_id' => 2,
            ),
            169 => 
            array (
                'user_id' => 670,
                'role_id' => 2,
            ),
            170 => 
            array (
                'user_id' => 671,
                'role_id' => 2,
            ),
            171 => 
            array (
                'user_id' => 672,
                'role_id' => 2,
            ),
            172 => 
            array (
                'user_id' => 673,
                'role_id' => 2,
            ),
            173 => 
            array (
                'user_id' => 674,
                'role_id' => 2,
            ),
            174 => 
            array (
                'user_id' => 675,
                'role_id' => 2,
            ),
            175 => 
            array (
                'user_id' => 676,
                'role_id' => 2,
            ),
            176 => 
            array (
                'user_id' => 677,
                'role_id' => 2,
            ),
            177 => 
            array (
                'user_id' => 678,
                'role_id' => 2,
            ),
            178 => 
            array (
                'user_id' => 679,
                'role_id' => 2,
            ),
            179 => 
            array (
                'user_id' => 680,
                'role_id' => 2,
            ),
            180 => 
            array (
                'user_id' => 681,
                'role_id' => 2,
            ),
            181 => 
            array (
                'user_id' => 682,
                'role_id' => 2,
            ),
            182 => 
            array (
                'user_id' => 683,
                'role_id' => 2,
            ),
            183 => 
            array (
                'user_id' => 684,
                'role_id' => 1,
            ),
            184 => 
            array (
                'user_id' => 685,
                'role_id' => 2,
            ),
            185 => 
            array (
                'user_id' => 686,
                'role_id' => 2,
            ),
            186 => 
            array (
                'user_id' => 687,
                'role_id' => 2,
            ),
            187 => 
            array (
                'user_id' => 688,
                'role_id' => 2,
            ),
            188 => 
            array (
                'user_id' => 689,
                'role_id' => 2,
            ),
            189 => 
            array (
                'user_id' => 690,
                'role_id' => 2,
            ),
            190 => 
            array (
                'user_id' => 691,
                'role_id' => 2,
            ),
            191 => 
            array (
                'user_id' => 692,
                'role_id' => 2,
            ),
            192 => 
            array (
                'user_id' => 693,
                'role_id' => 2,
            ),
            193 => 
            array (
                'user_id' => 694,
                'role_id' => 2,
            ),
            194 => 
            array (
                'user_id' => 695,
                'role_id' => 2,
            ),
            195 => 
            array (
                'user_id' => 696,
                'role_id' => 2,
            ),
            196 => 
            array (
                'user_id' => 697,
                'role_id' => 2,
            ),
            197 => 
            array (
                'user_id' => 698,
                'role_id' => 2,
            ),
            198 => 
            array (
                'user_id' => 699,
                'role_id' => 2,
            ),
            199 => 
            array (
                'user_id' => 700,
                'role_id' => 2,
            ),
            200 => 
            array (
                'user_id' => 701,
                'role_id' => 2,
            ),
            201 => 
            array (
                'user_id' => 702,
                'role_id' => 2,
            ),
            202 => 
            array (
                'user_id' => 703,
                'role_id' => 2,
            ),
            203 => 
            array (
                'user_id' => 704,
                'role_id' => 2,
            ),
            204 => 
            array (
                'user_id' => 705,
                'role_id' => 2,
            ),
            205 => 
            array (
                'user_id' => 706,
                'role_id' => 2,
            ),
            206 => 
            array (
                'user_id' => 707,
                'role_id' => 2,
            ),
            207 => 
            array (
                'user_id' => 708,
                'role_id' => 2,
            ),
            208 => 
            array (
                'user_id' => 709,
                'role_id' => 2,
            ),
            209 => 
            array (
                'user_id' => 710,
                'role_id' => 2,
            ),
            210 => 
            array (
                'user_id' => 711,
                'role_id' => 2,
            ),
            211 => 
            array (
                'user_id' => 712,
                'role_id' => 2,
            ),
            212 => 
            array (
                'user_id' => 713,
                'role_id' => 2,
            ),
            213 => 
            array (
                'user_id' => 714,
                'role_id' => 2,
            ),
            214 => 
            array (
                'user_id' => 715,
                'role_id' => 2,
            ),
            215 => 
            array (
                'user_id' => 716,
                'role_id' => 2,
            ),
            216 => 
            array (
                'user_id' => 717,
                'role_id' => 2,
            ),
            217 => 
            array (
                'user_id' => 718,
                'role_id' => 2,
            ),
            218 => 
            array (
                'user_id' => 719,
                'role_id' => 2,
            ),
            219 => 
            array (
                'user_id' => 720,
                'role_id' => 2,
            ),
            220 => 
            array (
                'user_id' => 721,
                'role_id' => 2,
            ),
            221 => 
            array (
                'user_id' => 722,
                'role_id' => 2,
            ),
            222 => 
            array (
                'user_id' => 723,
                'role_id' => 2,
            ),
            223 => 
            array (
                'user_id' => 724,
                'role_id' => 2,
            ),
            224 => 
            array (
                'user_id' => 725,
                'role_id' => 2,
            ),
            225 => 
            array (
                'user_id' => 726,
                'role_id' => 2,
            ),
            226 => 
            array (
                'user_id' => 727,
                'role_id' => 2,
            ),
            227 => 
            array (
                'user_id' => 728,
                'role_id' => 2,
            ),
            228 => 
            array (
                'user_id' => 729,
                'role_id' => 2,
            ),
            229 => 
            array (
                'user_id' => 730,
                'role_id' => 2,
            ),
            230 => 
            array (
                'user_id' => 731,
                'role_id' => 2,
            ),
            231 => 
            array (
                'user_id' => 732,
                'role_id' => 2,
            ),
            232 => 
            array (
                'user_id' => 733,
                'role_id' => 2,
            ),
            233 => 
            array (
                'user_id' => 734,
                'role_id' => 2,
            ),
            234 => 
            array (
                'user_id' => 735,
                'role_id' => 2,
            ),
            235 => 
            array (
                'user_id' => 736,
                'role_id' => 2,
            ),
            236 => 
            array (
                'user_id' => 737,
                'role_id' => 2,
            ),
            237 => 
            array (
                'user_id' => 738,
                'role_id' => 2,
            ),
            238 => 
            array (
                'user_id' => 739,
                'role_id' => 2,
            ),
            239 => 
            array (
                'user_id' => 740,
                'role_id' => 2,
            ),
            240 => 
            array (
                'user_id' => 741,
                'role_id' => 2,
            ),
            241 => 
            array (
                'user_id' => 742,
                'role_id' => 2,
            ),
            242 => 
            array (
                'user_id' => 743,
                'role_id' => 2,
            ),
            243 => 
            array (
                'user_id' => 744,
                'role_id' => 2,
            ),
            244 => 
            array (
                'user_id' => 745,
                'role_id' => 2,
            ),
            245 => 
            array (
                'user_id' => 746,
                'role_id' => 2,
            ),
            246 => 
            array (
                'user_id' => 747,
                'role_id' => 2,
            ),
            247 => 
            array (
                'user_id' => 748,
                'role_id' => 2,
            ),
            248 => 
            array (
                'user_id' => 749,
                'role_id' => 2,
            ),
            249 => 
            array (
                'user_id' => 750,
                'role_id' => 2,
            ),
            250 => 
            array (
                'user_id' => 751,
                'role_id' => 2,
            ),
            251 => 
            array (
                'user_id' => 752,
                'role_id' => 2,
            ),
            252 => 
            array (
                'user_id' => 753,
                'role_id' => 2,
            ),
            253 => 
            array (
                'user_id' => 754,
                'role_id' => 2,
            ),
            254 => 
            array (
                'user_id' => 755,
                'role_id' => 2,
            ),
            255 => 
            array (
                'user_id' => 756,
                'role_id' => 2,
            ),
            256 => 
            array (
                'user_id' => 757,
                'role_id' => 2,
            ),
            257 => 
            array (
                'user_id' => 758,
                'role_id' => 2,
            ),
            258 => 
            array (
                'user_id' => 759,
                'role_id' => 2,
            ),
            259 => 
            array (
                'user_id' => 760,
                'role_id' => 2,
            ),
            260 => 
            array (
                'user_id' => 761,
                'role_id' => 2,
            ),
            261 => 
            array (
                'user_id' => 762,
                'role_id' => 2,
            ),
            262 => 
            array (
                'user_id' => 763,
                'role_id' => 2,
            ),
            263 => 
            array (
                'user_id' => 764,
                'role_id' => 2,
            ),
            264 => 
            array (
                'user_id' => 765,
                'role_id' => 2,
            ),
            265 => 
            array (
                'user_id' => 766,
                'role_id' => 2,
            ),
            266 => 
            array (
                'user_id' => 767,
                'role_id' => 2,
            ),
            267 => 
            array (
                'user_id' => 768,
                'role_id' => 2,
            ),
            268 => 
            array (
                'user_id' => 769,
                'role_id' => 2,
            ),
            269 => 
            array (
                'user_id' => 770,
                'role_id' => 2,
            ),
            270 => 
            array (
                'user_id' => 771,
                'role_id' => 2,
            ),
            271 => 
            array (
                'user_id' => 772,
                'role_id' => 2,
            ),
            272 => 
            array (
                'user_id' => 773,
                'role_id' => 2,
            ),
            273 => 
            array (
                'user_id' => 774,
                'role_id' => 2,
            ),
            274 => 
            array (
                'user_id' => 775,
                'role_id' => 2,
            ),
            275 => 
            array (
                'user_id' => 776,
                'role_id' => 2,
            ),
            276 => 
            array (
                'user_id' => 777,
                'role_id' => 2,
            ),
            277 => 
            array (
                'user_id' => 778,
                'role_id' => 2,
            ),
            278 => 
            array (
                'user_id' => 779,
                'role_id' => 2,
            ),
            279 => 
            array (
                'user_id' => 780,
                'role_id' => 2,
            ),
            280 => 
            array (
                'user_id' => 781,
                'role_id' => 2,
            ),
            281 => 
            array (
                'user_id' => 782,
                'role_id' => 2,
            ),
            282 => 
            array (
                'user_id' => 783,
                'role_id' => 2,
            ),
            283 => 
            array (
                'user_id' => 784,
                'role_id' => 2,
            ),
            284 => 
            array (
                'user_id' => 785,
                'role_id' => 2,
            ),
            285 => 
            array (
                'user_id' => 786,
                'role_id' => 2,
            ),
            286 => 
            array (
                'user_id' => 787,
                'role_id' => 2,
            ),
            287 => 
            array (
                'user_id' => 788,
                'role_id' => 2,
            ),
            288 => 
            array (
                'user_id' => 789,
                'role_id' => 2,
            ),
            289 => 
            array (
                'user_id' => 790,
                'role_id' => 2,
            ),
            290 => 
            array (
                'user_id' => 791,
                'role_id' => 2,
            ),
            291 => 
            array (
                'user_id' => 792,
                'role_id' => 2,
            ),
            292 => 
            array (
                'user_id' => 793,
                'role_id' => 2,
            ),
            293 => 
            array (
                'user_id' => 794,
                'role_id' => 2,
            ),
            294 => 
            array (
                'user_id' => 795,
                'role_id' => 2,
            ),
            295 => 
            array (
                'user_id' => 796,
                'role_id' => 2,
            ),
            296 => 
            array (
                'user_id' => 797,
                'role_id' => 2,
            ),
            297 => 
            array (
                'user_id' => 798,
                'role_id' => 2,
            ),
            298 => 
            array (
                'user_id' => 799,
                'role_id' => 2,
            ),
            299 => 
            array (
                'user_id' => 800,
                'role_id' => 2,
            ),
            300 => 
            array (
                'user_id' => 801,
                'role_id' => 2,
            ),
            301 => 
            array (
                'user_id' => 802,
                'role_id' => 2,
            ),
            302 => 
            array (
                'user_id' => 803,
                'role_id' => 2,
            ),
            303 => 
            array (
                'user_id' => 804,
                'role_id' => 2,
            ),
            304 => 
            array (
                'user_id' => 805,
                'role_id' => 2,
            ),
            305 => 
            array (
                'user_id' => 806,
                'role_id' => 2,
            ),
            306 => 
            array (
                'user_id' => 807,
                'role_id' => 2,
            ),
            307 => 
            array (
                'user_id' => 808,
                'role_id' => 2,
            ),
            308 => 
            array (
                'user_id' => 809,
                'role_id' => 2,
            ),
            309 => 
            array (
                'user_id' => 810,
                'role_id' => 2,
            ),
            310 => 
            array (
                'user_id' => 811,
                'role_id' => 2,
            ),
            311 => 
            array (
                'user_id' => 812,
                'role_id' => 2,
            ),
            312 => 
            array (
                'user_id' => 813,
                'role_id' => 2,
            ),
            313 => 
            array (
                'user_id' => 814,
                'role_id' => 2,
            ),
            314 => 
            array (
                'user_id' => 815,
                'role_id' => 2,
            ),
            315 => 
            array (
                'user_id' => 816,
                'role_id' => 2,
            ),
            316 => 
            array (
                'user_id' => 817,
                'role_id' => 2,
            ),
            317 => 
            array (
                'user_id' => 818,
                'role_id' => 2,
            ),
            318 => 
            array (
                'user_id' => 819,
                'role_id' => 2,
            ),
            319 => 
            array (
                'user_id' => 820,
                'role_id' => 2,
            ),
            320 => 
            array (
                'user_id' => 821,
                'role_id' => 2,
            ),
            321 => 
            array (
                'user_id' => 822,
                'role_id' => 2,
            ),
            322 => 
            array (
                'user_id' => 823,
                'role_id' => 2,
            ),
            323 => 
            array (
                'user_id' => 824,
                'role_id' => 2,
            ),
            324 => 
            array (
                'user_id' => 825,
                'role_id' => 2,
            ),
            325 => 
            array (
                'user_id' => 826,
                'role_id' => 2,
            ),
            326 => 
            array (
                'user_id' => 827,
                'role_id' => 2,
            ),
            327 => 
            array (
                'user_id' => 828,
                'role_id' => 2,
            ),
            328 => 
            array (
                'user_id' => 829,
                'role_id' => 2,
            ),
            329 => 
            array (
                'user_id' => 830,
                'role_id' => 2,
            ),
            330 => 
            array (
                'user_id' => 831,
                'role_id' => 2,
            ),
            331 => 
            array (
                'user_id' => 832,
                'role_id' => 2,
            ),
            332 => 
            array (
                'user_id' => 833,
                'role_id' => 2,
            ),
            333 => 
            array (
                'user_id' => 834,
                'role_id' => 2,
            ),
            334 => 
            array (
                'user_id' => 835,
                'role_id' => 2,
            ),
            335 => 
            array (
                'user_id' => 836,
                'role_id' => 2,
            ),
            336 => 
            array (
                'user_id' => 837,
                'role_id' => 2,
            ),
            337 => 
            array (
                'user_id' => 838,
                'role_id' => 2,
            ),
            338 => 
            array (
                'user_id' => 839,
                'role_id' => 2,
            ),
            339 => 
            array (
                'user_id' => 840,
                'role_id' => 2,
            ),
            340 => 
            array (
                'user_id' => 841,
                'role_id' => 2,
            ),
            341 => 
            array (
                'user_id' => 842,
                'role_id' => 2,
            ),
            342 => 
            array (
                'user_id' => 843,
                'role_id' => 2,
            ),
            343 => 
            array (
                'user_id' => 844,
                'role_id' => 2,
            ),
            344 => 
            array (
                'user_id' => 845,
                'role_id' => 2,
            ),
            345 => 
            array (
                'user_id' => 846,
                'role_id' => 2,
            ),
            346 => 
            array (
                'user_id' => 847,
                'role_id' => 2,
            ),
            347 => 
            array (
                'user_id' => 848,
                'role_id' => 2,
            ),
            348 => 
            array (
                'user_id' => 849,
                'role_id' => 2,
            ),
            349 => 
            array (
                'user_id' => 850,
                'role_id' => 2,
            ),
            350 => 
            array (
                'user_id' => 851,
                'role_id' => 2,
            ),
            351 => 
            array (
                'user_id' => 852,
                'role_id' => 2,
            ),
            352 => 
            array (
                'user_id' => 853,
                'role_id' => 2,
            ),
            353 => 
            array (
                'user_id' => 854,
                'role_id' => 2,
            ),
            354 => 
            array (
                'user_id' => 855,
                'role_id' => 2,
            ),
            355 => 
            array (
                'user_id' => 856,
                'role_id' => 2,
            ),
            356 => 
            array (
                'user_id' => 857,
                'role_id' => 2,
            ),
            357 => 
            array (
                'user_id' => 858,
                'role_id' => 2,
            ),
            358 => 
            array (
                'user_id' => 859,
                'role_id' => 2,
            ),
            359 => 
            array (
                'user_id' => 860,
                'role_id' => 2,
            ),
            360 => 
            array (
                'user_id' => 861,
                'role_id' => 2,
            ),
            361 => 
            array (
                'user_id' => 862,
                'role_id' => 2,
            ),
            362 => 
            array (
                'user_id' => 863,
                'role_id' => 2,
            ),
            363 => 
            array (
                'user_id' => 864,
                'role_id' => 2,
            ),
            364 => 
            array (
                'user_id' => 865,
                'role_id' => 2,
            ),
            365 => 
            array (
                'user_id' => 866,
                'role_id' => 2,
            ),
            366 => 
            array (
                'user_id' => 867,
                'role_id' => 2,
            ),
            367 => 
            array (
                'user_id' => 868,
                'role_id' => 2,
            ),
            368 => 
            array (
                'user_id' => 869,
                'role_id' => 2,
            ),
            369 => 
            array (
                'user_id' => 870,
                'role_id' => 2,
            ),
            370 => 
            array (
                'user_id' => 871,
                'role_id' => 2,
            ),
            371 => 
            array (
                'user_id' => 872,
                'role_id' => 2,
            ),
            372 => 
            array (
                'user_id' => 873,
                'role_id' => 2,
            ),
            373 => 
            array (
                'user_id' => 874,
                'role_id' => 2,
            ),
            374 => 
            array (
                'user_id' => 875,
                'role_id' => 2,
            ),
            375 => 
            array (
                'user_id' => 876,
                'role_id' => 2,
            ),
            376 => 
            array (
                'user_id' => 877,
                'role_id' => 2,
            ),
            377 => 
            array (
                'user_id' => 878,
                'role_id' => 2,
            ),
            378 => 
            array (
                'user_id' => 879,
                'role_id' => 2,
            ),
            379 => 
            array (
                'user_id' => 880,
                'role_id' => 2,
            ),
            380 => 
            array (
                'user_id' => 881,
                'role_id' => 2,
            ),
            381 => 
            array (
                'user_id' => 882,
                'role_id' => 2,
            ),
            382 => 
            array (
                'user_id' => 883,
                'role_id' => 2,
            ),
            383 => 
            array (
                'user_id' => 884,
                'role_id' => 2,
            ),
            384 => 
            array (
                'user_id' => 885,
                'role_id' => 2,
            ),
            385 => 
            array (
                'user_id' => 886,
                'role_id' => 2,
            ),
            386 => 
            array (
                'user_id' => 887,
                'role_id' => 2,
            ),
            387 => 
            array (
                'user_id' => 888,
                'role_id' => 2,
            ),
            388 => 
            array (
                'user_id' => 889,
                'role_id' => 2,
            ),
            389 => 
            array (
                'user_id' => 890,
                'role_id' => 2,
            ),
            390 => 
            array (
                'user_id' => 891,
                'role_id' => 2,
            ),
            391 => 
            array (
                'user_id' => 892,
                'role_id' => 2,
            ),
            392 => 
            array (
                'user_id' => 893,
                'role_id' => 2,
            ),
            393 => 
            array (
                'user_id' => 894,
                'role_id' => 2,
            ),
            394 => 
            array (
                'user_id' => 895,
                'role_id' => 2,
            ),
            395 => 
            array (
                'user_id' => 896,
                'role_id' => 2,
            ),
            396 => 
            array (
                'user_id' => 897,
                'role_id' => 2,
            ),
            397 => 
            array (
                'user_id' => 898,
                'role_id' => 2,
            ),
            398 => 
            array (
                'user_id' => 899,
                'role_id' => 2,
            ),
            399 => 
            array (
                'user_id' => 900,
                'role_id' => 2,
            ),
            400 => 
            array (
                'user_id' => 901,
                'role_id' => 2,
            ),
            401 => 
            array (
                'user_id' => 902,
                'role_id' => 2,
            ),
            402 => 
            array (
                'user_id' => 903,
                'role_id' => 2,
            ),
            403 => 
            array (
                'user_id' => 904,
                'role_id' => 2,
            ),
            404 => 
            array (
                'user_id' => 905,
                'role_id' => 2,
            ),
            405 => 
            array (
                'user_id' => 906,
                'role_id' => 2,
            ),
            406 => 
            array (
                'user_id' => 907,
                'role_id' => 2,
            ),
            407 => 
            array (
                'user_id' => 908,
                'role_id' => 2,
            ),
            408 => 
            array (
                'user_id' => 909,
                'role_id' => 2,
            ),
            409 => 
            array (
                'user_id' => 910,
                'role_id' => 2,
            ),
            410 => 
            array (
                'user_id' => 911,
                'role_id' => 2,
            ),
            411 => 
            array (
                'user_id' => 912,
                'role_id' => 2,
            ),
            412 => 
            array (
                'user_id' => 913,
                'role_id' => 2,
            ),
            413 => 
            array (
                'user_id' => 914,
                'role_id' => 2,
            ),
            414 => 
            array (
                'user_id' => 915,
                'role_id' => 2,
            ),
            415 => 
            array (
                'user_id' => 916,
                'role_id' => 2,
            ),
            416 => 
            array (
                'user_id' => 917,
                'role_id' => 2,
            ),
            417 => 
            array (
                'user_id' => 918,
                'role_id' => 2,
            ),
            418 => 
            array (
                'user_id' => 919,
                'role_id' => 2,
            ),
            419 => 
            array (
                'user_id' => 920,
                'role_id' => 2,
            ),
            420 => 
            array (
                'user_id' => 921,
                'role_id' => 2,
            ),
            421 => 
            array (
                'user_id' => 922,
                'role_id' => 2,
            ),
            422 => 
            array (
                'user_id' => 923,
                'role_id' => 2,
            ),
            423 => 
            array (
                'user_id' => 924,
                'role_id' => 2,
            ),
            424 => 
            array (
                'user_id' => 925,
                'role_id' => 2,
            ),
            425 => 
            array (
                'user_id' => 926,
                'role_id' => 2,
            ),
            426 => 
            array (
                'user_id' => 927,
                'role_id' => 2,
            ),
            427 => 
            array (
                'user_id' => 928,
                'role_id' => 2,
            ),
            428 => 
            array (
                'user_id' => 929,
                'role_id' => 2,
            ),
            429 => 
            array (
                'user_id' => 930,
                'role_id' => 2,
            ),
            430 => 
            array (
                'user_id' => 931,
                'role_id' => 2,
            ),
            431 => 
            array (
                'user_id' => 932,
                'role_id' => 2,
            ),
            432 => 
            array (
                'user_id' => 933,
                'role_id' => 2,
            ),
            433 => 
            array (
                'user_id' => 934,
                'role_id' => 2,
            ),
            434 => 
            array (
                'user_id' => 935,
                'role_id' => 2,
            ),
            435 => 
            array (
                'user_id' => 936,
                'role_id' => 2,
            ),
            436 => 
            array (
                'user_id' => 937,
                'role_id' => 2,
            ),
            437 => 
            array (
                'user_id' => 938,
                'role_id' => 2,
            ),
            438 => 
            array (
                'user_id' => 939,
                'role_id' => 2,
            ),
            439 => 
            array (
                'user_id' => 940,
                'role_id' => 2,
            ),
            440 => 
            array (
                'user_id' => 941,
                'role_id' => 2,
            ),
            441 => 
            array (
                'user_id' => 942,
                'role_id' => 2,
            ),
            442 => 
            array (
                'user_id' => 943,
                'role_id' => 2,
            ),
            443 => 
            array (
                'user_id' => 944,
                'role_id' => 2,
            ),
            444 => 
            array (
                'user_id' => 945,
                'role_id' => 2,
            ),
            445 => 
            array (
                'user_id' => 946,
                'role_id' => 2,
            ),
            446 => 
            array (
                'user_id' => 947,
                'role_id' => 2,
            ),
            447 => 
            array (
                'user_id' => 948,
                'role_id' => 2,
            ),
            448 => 
            array (
                'user_id' => 949,
                'role_id' => 2,
            ),
            449 => 
            array (
                'user_id' => 950,
                'role_id' => 2,
            ),
            450 => 
            array (
                'user_id' => 951,
                'role_id' => 2,
            ),
            451 => 
            array (
                'user_id' => 952,
                'role_id' => 2,
            ),
            452 => 
            array (
                'user_id' => 953,
                'role_id' => 2,
            ),
            453 => 
            array (
                'user_id' => 954,
                'role_id' => 2,
            ),
            454 => 
            array (
                'user_id' => 955,
                'role_id' => 2,
            ),
            455 => 
            array (
                'user_id' => 956,
                'role_id' => 2,
            ),
            456 => 
            array (
                'user_id' => 957,
                'role_id' => 2,
            ),
            457 => 
            array (
                'user_id' => 958,
                'role_id' => 2,
            ),
            458 => 
            array (
                'user_id' => 959,
                'role_id' => 2,
            ),
            459 => 
            array (
                'user_id' => 960,
                'role_id' => 2,
            ),
            460 => 
            array (
                'user_id' => 961,
                'role_id' => 2,
            ),
            461 => 
            array (
                'user_id' => 962,
                'role_id' => 2,
            ),
            462 => 
            array (
                'user_id' => 963,
                'role_id' => 2,
            ),
            463 => 
            array (
                'user_id' => 964,
                'role_id' => 2,
            ),
            464 => 
            array (
                'user_id' => 965,
                'role_id' => 2,
            ),
            465 => 
            array (
                'user_id' => 966,
                'role_id' => 2,
            ),
            466 => 
            array (
                'user_id' => 967,
                'role_id' => 2,
            ),
            467 => 
            array (
                'user_id' => 968,
                'role_id' => 2,
            ),
            468 => 
            array (
                'user_id' => 969,
                'role_id' => 2,
            ),
            469 => 
            array (
                'user_id' => 970,
                'role_id' => 2,
            ),
            470 => 
            array (
                'user_id' => 971,
                'role_id' => 2,
            ),
            471 => 
            array (
                'user_id' => 972,
                'role_id' => 2,
            ),
            472 => 
            array (
                'user_id' => 973,
                'role_id' => 2,
            ),
            473 => 
            array (
                'user_id' => 974,
                'role_id' => 2,
            ),
            474 => 
            array (
                'user_id' => 975,
                'role_id' => 2,
            ),
            475 => 
            array (
                'user_id' => 976,
                'role_id' => 2,
            ),
            476 => 
            array (
                'user_id' => 977,
                'role_id' => 2,
            ),
            477 => 
            array (
                'user_id' => 978,
                'role_id' => 2,
            ),
            478 => 
            array (
                'user_id' => 979,
                'role_id' => 2,
            ),
            479 => 
            array (
                'user_id' => 980,
                'role_id' => 2,
            ),
            480 => 
            array (
                'user_id' => 981,
                'role_id' => 2,
            ),
            481 => 
            array (
                'user_id' => 982,
                'role_id' => 2,
            ),
            482 => 
            array (
                'user_id' => 983,
                'role_id' => 2,
            ),
            483 => 
            array (
                'user_id' => 984,
                'role_id' => 2,
            ),
            484 => 
            array (
                'user_id' => 985,
                'role_id' => 2,
            ),
            485 => 
            array (
                'user_id' => 986,
                'role_id' => 2,
            ),
            486 => 
            array (
                'user_id' => 987,
                'role_id' => 2,
            ),
            487 => 
            array (
                'user_id' => 988,
                'role_id' => 2,
            ),
            488 => 
            array (
                'user_id' => 989,
                'role_id' => 2,
            ),
            489 => 
            array (
                'user_id' => 990,
                'role_id' => 2,
            ),
            490 => 
            array (
                'user_id' => 991,
                'role_id' => 2,
            ),
            491 => 
            array (
                'user_id' => 992,
                'role_id' => 2,
            ),
            492 => 
            array (
                'user_id' => 993,
                'role_id' => 2,
            ),
            493 => 
            array (
                'user_id' => 994,
                'role_id' => 2,
            ),
            494 => 
            array (
                'user_id' => 995,
                'role_id' => 2,
            ),
            495 => 
            array (
                'user_id' => 996,
                'role_id' => 2,
            ),
            496 => 
            array (
                'user_id' => 997,
                'role_id' => 2,
            ),
            497 => 
            array (
                'user_id' => 998,
                'role_id' => 2,
            ),
            498 => 
            array (
                'user_id' => 999,
                'role_id' => 2,
            ),
            499 => 
            array (
                'user_id' => 1000,
                'role_id' => 2,
            ),
        ));
        \DB::table('role_user')->insert(array (
            0 => 
            array (
                'user_id' => 1001,
                'role_id' => 2,
            ),
            1 => 
            array (
                'user_id' => 1002,
                'role_id' => 2,
            ),
            2 => 
            array (
                'user_id' => 1003,
                'role_id' => 2,
            ),
            3 => 
            array (
                'user_id' => 1004,
                'role_id' => 2,
            ),
            4 => 
            array (
                'user_id' => 1005,
                'role_id' => 2,
            ),
            5 => 
            array (
                'user_id' => 1006,
                'role_id' => 2,
            ),
            6 => 
            array (
                'user_id' => 1007,
                'role_id' => 2,
            ),
            7 => 
            array (
                'user_id' => 1008,
                'role_id' => 2,
            ),
            8 => 
            array (
                'user_id' => 1009,
                'role_id' => 2,
            ),
            9 => 
            array (
                'user_id' => 1010,
                'role_id' => 2,
            ),
            10 => 
            array (
                'user_id' => 1011,
                'role_id' => 2,
            ),
            11 => 
            array (
                'user_id' => 1012,
                'role_id' => 2,
            ),
            12 => 
            array (
                'user_id' => 1013,
                'role_id' => 2,
            ),
            13 => 
            array (
                'user_id' => 1014,
                'role_id' => 2,
            ),
            14 => 
            array (
                'user_id' => 1015,
                'role_id' => 2,
            ),
            15 => 
            array (
                'user_id' => 1016,
                'role_id' => 2,
            ),
            16 => 
            array (
                'user_id' => 1017,
                'role_id' => 2,
            ),
            17 => 
            array (
                'user_id' => 1018,
                'role_id' => 2,
            ),
            18 => 
            array (
                'user_id' => 1019,
                'role_id' => 2,
            ),
            19 => 
            array (
                'user_id' => 1020,
                'role_id' => 2,
            ),
            20 => 
            array (
                'user_id' => 1021,
                'role_id' => 2,
            ),
            21 => 
            array (
                'user_id' => 1022,
                'role_id' => 2,
            ),
            22 => 
            array (
                'user_id' => 1023,
                'role_id' => 2,
            ),
            23 => 
            array (
                'user_id' => 1024,
                'role_id' => 2,
            ),
            24 => 
            array (
                'user_id' => 1025,
                'role_id' => 2,
            ),
            25 => 
            array (
                'user_id' => 1026,
                'role_id' => 2,
            ),
            26 => 
            array (
                'user_id' => 1027,
                'role_id' => 2,
            ),
            27 => 
            array (
                'user_id' => 1028,
                'role_id' => 2,
            ),
            28 => 
            array (
                'user_id' => 1029,
                'role_id' => 2,
            ),
            29 => 
            array (
                'user_id' => 1030,
                'role_id' => 2,
            ),
            30 => 
            array (
                'user_id' => 1031,
                'role_id' => 2,
            ),
            31 => 
            array (
                'user_id' => 1032,
                'role_id' => 2,
            ),
            32 => 
            array (
                'user_id' => 1033,
                'role_id' => 2,
            ),
            33 => 
            array (
                'user_id' => 1034,
                'role_id' => 2,
            ),
            34 => 
            array (
                'user_id' => 1035,
                'role_id' => 2,
            ),
            35 => 
            array (
                'user_id' => 1036,
                'role_id' => 2,
            ),
            36 => 
            array (
                'user_id' => 1037,
                'role_id' => 2,
            ),
            37 => 
            array (
                'user_id' => 1038,
                'role_id' => 2,
            ),
            38 => 
            array (
                'user_id' => 1039,
                'role_id' => 2,
            ),
            39 => 
            array (
                'user_id' => 1040,
                'role_id' => 2,
            ),
            40 => 
            array (
                'user_id' => 1041,
                'role_id' => 2,
            ),
            41 => 
            array (
                'user_id' => 1042,
                'role_id' => 2,
            ),
            42 => 
            array (
                'user_id' => 1043,
                'role_id' => 2,
            ),
            43 => 
            array (
                'user_id' => 1044,
                'role_id' => 2,
            ),
            44 => 
            array (
                'user_id' => 1045,
                'role_id' => 2,
            ),
            45 => 
            array (
                'user_id' => 1046,
                'role_id' => 2,
            ),
            46 => 
            array (
                'user_id' => 1047,
                'role_id' => 2,
            ),
            47 => 
            array (
                'user_id' => 1048,
                'role_id' => 2,
            ),
            48 => 
            array (
                'user_id' => 1049,
                'role_id' => 2,
            ),
            49 => 
            array (
                'user_id' => 1050,
                'role_id' => 2,
            ),
            50 => 
            array (
                'user_id' => 1051,
                'role_id' => 2,
            ),
            51 => 
            array (
                'user_id' => 1052,
                'role_id' => 2,
            ),
            52 => 
            array (
                'user_id' => 1053,
                'role_id' => 2,
            ),
            53 => 
            array (
                'user_id' => 1054,
                'role_id' => 2,
            ),
            54 => 
            array (
                'user_id' => 1055,
                'role_id' => 2,
            ),
            55 => 
            array (
                'user_id' => 1056,
                'role_id' => 2,
            ),
            56 => 
            array (
                'user_id' => 1057,
                'role_id' => 2,
            ),
            57 => 
            array (
                'user_id' => 1058,
                'role_id' => 2,
            ),
            58 => 
            array (
                'user_id' => 1059,
                'role_id' => 2,
            ),
            59 => 
            array (
                'user_id' => 1060,
                'role_id' => 2,
            ),
            60 => 
            array (
                'user_id' => 1061,
                'role_id' => 2,
            ),
            61 => 
            array (
                'user_id' => 1062,
                'role_id' => 2,
            ),
            62 => 
            array (
                'user_id' => 1063,
                'role_id' => 2,
            ),
            63 => 
            array (
                'user_id' => 1064,
                'role_id' => 2,
            ),
            64 => 
            array (
                'user_id' => 1065,
                'role_id' => 2,
            ),
            65 => 
            array (
                'user_id' => 1066,
                'role_id' => 2,
            ),
            66 => 
            array (
                'user_id' => 1067,
                'role_id' => 2,
            ),
            67 => 
            array (
                'user_id' => 1068,
                'role_id' => 2,
            ),
            68 => 
            array (
                'user_id' => 1069,
                'role_id' => 2,
            ),
            69 => 
            array (
                'user_id' => 1070,
                'role_id' => 2,
            ),
            70 => 
            array (
                'user_id' => 1071,
                'role_id' => 2,
            ),
            71 => 
            array (
                'user_id' => 1072,
                'role_id' => 2,
            ),
            72 => 
            array (
                'user_id' => 1073,
                'role_id' => 2,
            ),
            73 => 
            array (
                'user_id' => 1074,
                'role_id' => 2,
            ),
            74 => 
            array (
                'user_id' => 1075,
                'role_id' => 2,
            ),
            75 => 
            array (
                'user_id' => 1076,
                'role_id' => 2,
            ),
            76 => 
            array (
                'user_id' => 1077,
                'role_id' => 2,
            ),
            77 => 
            array (
                'user_id' => 1078,
                'role_id' => 2,
            ),
            78 => 
            array (
                'user_id' => 1079,
                'role_id' => 2,
            ),
            79 => 
            array (
                'user_id' => 1080,
                'role_id' => 2,
            ),
            80 => 
            array (
                'user_id' => 1081,
                'role_id' => 2,
            ),
            81 => 
            array (
                'user_id' => 1082,
                'role_id' => 2,
            ),
            82 => 
            array (
                'user_id' => 1083,
                'role_id' => 2,
            ),
            83 => 
            array (
                'user_id' => 1084,
                'role_id' => 2,
            ),
            84 => 
            array (
                'user_id' => 1085,
                'role_id' => 2,
            ),
            85 => 
            array (
                'user_id' => 1086,
                'role_id' => 2,
            ),
            86 => 
            array (
                'user_id' => 1087,
                'role_id' => 2,
            ),
            87 => 
            array (
                'user_id' => 1088,
                'role_id' => 2,
            ),
            88 => 
            array (
                'user_id' => 1089,
                'role_id' => 2,
            ),
            89 => 
            array (
                'user_id' => 1090,
                'role_id' => 2,
            ),
            90 => 
            array (
                'user_id' => 1091,
                'role_id' => 2,
            ),
            91 => 
            array (
                'user_id' => 1092,
                'role_id' => 2,
            ),
            92 => 
            array (
                'user_id' => 1093,
                'role_id' => 2,
            ),
            93 => 
            array (
                'user_id' => 1094,
                'role_id' => 2,
            ),
            94 => 
            array (
                'user_id' => 1095,
                'role_id' => 2,
            ),
            95 => 
            array (
                'user_id' => 1096,
                'role_id' => 2,
            ),
            96 => 
            array (
                'user_id' => 1097,
                'role_id' => 2,
            ),
            97 => 
            array (
                'user_id' => 1098,
                'role_id' => 2,
            ),
            98 => 
            array (
                'user_id' => 1099,
                'role_id' => 2,
            ),
            99 => 
            array (
                'user_id' => 1100,
                'role_id' => 2,
            ),
            100 => 
            array (
                'user_id' => 1101,
                'role_id' => 2,
            ),
            101 => 
            array (
                'user_id' => 1102,
                'role_id' => 2,
            ),
            102 => 
            array (
                'user_id' => 1103,
                'role_id' => 2,
            ),
            103 => 
            array (
                'user_id' => 1104,
                'role_id' => 2,
            ),
            104 => 
            array (
                'user_id' => 1105,
                'role_id' => 2,
            ),
            105 => 
            array (
                'user_id' => 1106,
                'role_id' => 2,
            ),
            106 => 
            array (
                'user_id' => 1107,
                'role_id' => 2,
            ),
            107 => 
            array (
                'user_id' => 1108,
                'role_id' => 2,
            ),
            108 => 
            array (
                'user_id' => 1109,
                'role_id' => 2,
            ),
            109 => 
            array (
                'user_id' => 1110,
                'role_id' => 2,
            ),
            110 => 
            array (
                'user_id' => 1111,
                'role_id' => 2,
            ),
            111 => 
            array (
                'user_id' => 1112,
                'role_id' => 2,
            ),
            112 => 
            array (
                'user_id' => 1113,
                'role_id' => 2,
            ),
            113 => 
            array (
                'user_id' => 1114,
                'role_id' => 2,
            ),
            114 => 
            array (
                'user_id' => 1115,
                'role_id' => 2,
            ),
            115 => 
            array (
                'user_id' => 1116,
                'role_id' => 2,
            ),
            116 => 
            array (
                'user_id' => 1117,
                'role_id' => 2,
            ),
            117 => 
            array (
                'user_id' => 1118,
                'role_id' => 2,
            ),
            118 => 
            array (
                'user_id' => 1119,
                'role_id' => 2,
            ),
            119 => 
            array (
                'user_id' => 1120,
                'role_id' => 2,
            ),
            120 => 
            array (
                'user_id' => 1121,
                'role_id' => 2,
            ),
            121 => 
            array (
                'user_id' => 1122,
                'role_id' => 2,
            ),
            122 => 
            array (
                'user_id' => 1123,
                'role_id' => 2,
            ),
            123 => 
            array (
                'user_id' => 1124,
                'role_id' => 2,
            ),
            124 => 
            array (
                'user_id' => 1125,
                'role_id' => 2,
            ),
            125 => 
            array (
                'user_id' => 1126,
                'role_id' => 2,
            ),
            126 => 
            array (
                'user_id' => 1127,
                'role_id' => 2,
            ),
            127 => 
            array (
                'user_id' => 1128,
                'role_id' => 2,
            ),
            128 => 
            array (
                'user_id' => 1129,
                'role_id' => 2,
            ),
            129 => 
            array (
                'user_id' => 1130,
                'role_id' => 2,
            ),
            130 => 
            array (
                'user_id' => 1131,
                'role_id' => 2,
            ),
            131 => 
            array (
                'user_id' => 1132,
                'role_id' => 2,
            ),
            132 => 
            array (
                'user_id' => 1133,
                'role_id' => 2,
            ),
            133 => 
            array (
                'user_id' => 1134,
                'role_id' => 2,
            ),
            134 => 
            array (
                'user_id' => 1135,
                'role_id' => 2,
            ),
            135 => 
            array (
                'user_id' => 1136,
                'role_id' => 2,
            ),
            136 => 
            array (
                'user_id' => 1137,
                'role_id' => 2,
            ),
            137 => 
            array (
                'user_id' => 1138,
                'role_id' => 2,
            ),
            138 => 
            array (
                'user_id' => 1139,
                'role_id' => 2,
            ),
            139 => 
            array (
                'user_id' => 1140,
                'role_id' => 2,
            ),
            140 => 
            array (
                'user_id' => 1141,
                'role_id' => 2,
            ),
            141 => 
            array (
                'user_id' => 1142,
                'role_id' => 2,
            ),
            142 => 
            array (
                'user_id' => 1143,
                'role_id' => 2,
            ),
            143 => 
            array (
                'user_id' => 1144,
                'role_id' => 2,
            ),
            144 => 
            array (
                'user_id' => 1145,
                'role_id' => 2,
            ),
            145 => 
            array (
                'user_id' => 1146,
                'role_id' => 2,
            ),
            146 => 
            array (
                'user_id' => 1147,
                'role_id' => 2,
            ),
            147 => 
            array (
                'user_id' => 1148,
                'role_id' => 2,
            ),
            148 => 
            array (
                'user_id' => 1149,
                'role_id' => 2,
            ),
            149 => 
            array (
                'user_id' => 1150,
                'role_id' => 2,
            ),
            150 => 
            array (
                'user_id' => 1151,
                'role_id' => 2,
            ),
            151 => 
            array (
                'user_id' => 1152,
                'role_id' => 2,
            ),
            152 => 
            array (
                'user_id' => 1153,
                'role_id' => 2,
            ),
            153 => 
            array (
                'user_id' => 1154,
                'role_id' => 2,
            ),
            154 => 
            array (
                'user_id' => 1155,
                'role_id' => 2,
            ),
            155 => 
            array (
                'user_id' => 1156,
                'role_id' => 2,
            ),
            156 => 
            array (
                'user_id' => 1157,
                'role_id' => 2,
            ),
            157 => 
            array (
                'user_id' => 1158,
                'role_id' => 2,
            ),
            158 => 
            array (
                'user_id' => 1159,
                'role_id' => 2,
            ),
            159 => 
            array (
                'user_id' => 1160,
                'role_id' => 2,
            ),
            160 => 
            array (
                'user_id' => 1161,
                'role_id' => 2,
            ),
            161 => 
            array (
                'user_id' => 1162,
                'role_id' => 2,
            ),
            162 => 
            array (
                'user_id' => 1163,
                'role_id' => 2,
            ),
            163 => 
            array (
                'user_id' => 1164,
                'role_id' => 2,
            ),
            164 => 
            array (
                'user_id' => 1165,
                'role_id' => 2,
            ),
            165 => 
            array (
                'user_id' => 1166,
                'role_id' => 2,
            ),
            166 => 
            array (
                'user_id' => 1167,
                'role_id' => 2,
            ),
            167 => 
            array (
                'user_id' => 1168,
                'role_id' => 2,
            ),
            168 => 
            array (
                'user_id' => 1169,
                'role_id' => 2,
            ),
            169 => 
            array (
                'user_id' => 1170,
                'role_id' => 2,
            ),
            170 => 
            array (
                'user_id' => 1171,
                'role_id' => 2,
            ),
            171 => 
            array (
                'user_id' => 1172,
                'role_id' => 2,
            ),
            172 => 
            array (
                'user_id' => 1173,
                'role_id' => 2,
            ),
            173 => 
            array (
                'user_id' => 1174,
                'role_id' => 2,
            ),
            174 => 
            array (
                'user_id' => 1175,
                'role_id' => 2,
            ),
            175 => 
            array (
                'user_id' => 1176,
                'role_id' => 2,
            ),
            176 => 
            array (
                'user_id' => 1177,
                'role_id' => 2,
            ),
            177 => 
            array (
                'user_id' => 1178,
                'role_id' => 2,
            ),
            178 => 
            array (
                'user_id' => 1179,
                'role_id' => 2,
            ),
            179 => 
            array (
                'user_id' => 1180,
                'role_id' => 2,
            ),
            180 => 
            array (
                'user_id' => 1181,
                'role_id' => 1,
            ),
            181 => 
            array (
                'user_id' => 1182,
                'role_id' => 2,
            ),
            182 => 
            array (
                'user_id' => 1183,
                'role_id' => 2,
            ),
            183 => 
            array (
                'user_id' => 1184,
                'role_id' => 2,
            ),
            184 => 
            array (
                'user_id' => 1185,
                'role_id' => 2,
            ),
            185 => 
            array (
                'user_id' => 1186,
                'role_id' => 2,
            ),
            186 => 
            array (
                'user_id' => 1187,
                'role_id' => 2,
            ),
            187 => 
            array (
                'user_id' => 1188,
                'role_id' => 2,
            ),
            188 => 
            array (
                'user_id' => 1189,
                'role_id' => 2,
            ),
            189 => 
            array (
                'user_id' => 1190,
                'role_id' => 2,
            ),
            190 => 
            array (
                'user_id' => 1191,
                'role_id' => 2,
            ),
            191 => 
            array (
                'user_id' => 1192,
                'role_id' => 2,
            ),
            192 => 
            array (
                'user_id' => 1193,
                'role_id' => 2,
            ),
            193 => 
            array (
                'user_id' => 1194,
                'role_id' => 2,
            ),
            194 => 
            array (
                'user_id' => 1195,
                'role_id' => 2,
            ),
            195 => 
            array (
                'user_id' => 1196,
                'role_id' => 2,
            ),
            196 => 
            array (
                'user_id' => 1197,
                'role_id' => 2,
            ),
            197 => 
            array (
                'user_id' => 1198,
                'role_id' => 2,
            ),
            198 => 
            array (
                'user_id' => 1199,
                'role_id' => 2,
            ),
            199 => 
            array (
                'user_id' => 1200,
                'role_id' => 2,
            ),
            200 => 
            array (
                'user_id' => 1201,
                'role_id' => 2,
            ),
            201 => 
            array (
                'user_id' => 1202,
                'role_id' => 2,
            ),
            202 => 
            array (
                'user_id' => 1203,
                'role_id' => 2,
            ),
            203 => 
            array (
                'user_id' => 1204,
                'role_id' => 2,
            ),
            204 => 
            array (
                'user_id' => 1205,
                'role_id' => 2,
            ),
            205 => 
            array (
                'user_id' => 1206,
                'role_id' => 2,
            ),
            206 => 
            array (
                'user_id' => 1207,
                'role_id' => 2,
            ),
            207 => 
            array (
                'user_id' => 1208,
                'role_id' => 2,
            ),
            208 => 
            array (
                'user_id' => 1209,
                'role_id' => 2,
            ),
            209 => 
            array (
                'user_id' => 1210,
                'role_id' => 2,
            ),
            210 => 
            array (
                'user_id' => 1211,
                'role_id' => 2,
            ),
            211 => 
            array (
                'user_id' => 1212,
                'role_id' => 2,
            ),
            212 => 
            array (
                'user_id' => 1213,
                'role_id' => 2,
            ),
            213 => 
            array (
                'user_id' => 1214,
                'role_id' => 2,
            ),
            214 => 
            array (
                'user_id' => 1215,
                'role_id' => 2,
            ),
            215 => 
            array (
                'user_id' => 1216,
                'role_id' => 2,
            ),
            216 => 
            array (
                'user_id' => 1217,
                'role_id' => 2,
            ),
            217 => 
            array (
                'user_id' => 1218,
                'role_id' => 2,
            ),
            218 => 
            array (
                'user_id' => 1219,
                'role_id' => 2,
            ),
            219 => 
            array (
                'user_id' => 1220,
                'role_id' => 2,
            ),
            220 => 
            array (
                'user_id' => 1221,
                'role_id' => 2,
            ),
            221 => 
            array (
                'user_id' => 1222,
                'role_id' => 2,
            ),
            222 => 
            array (
                'user_id' => 1223,
                'role_id' => 2,
            ),
            223 => 
            array (
                'user_id' => 1224,
                'role_id' => 2,
            ),
            224 => 
            array (
                'user_id' => 1225,
                'role_id' => 2,
            ),
            225 => 
            array (
                'user_id' => 1226,
                'role_id' => 2,
            ),
            226 => 
            array (
                'user_id' => 1227,
                'role_id' => 2,
            ),
            227 => 
            array (
                'user_id' => 1228,
                'role_id' => 2,
            ),
            228 => 
            array (
                'user_id' => 1229,
                'role_id' => 2,
            ),
            229 => 
            array (
                'user_id' => 1230,
                'role_id' => 2,
            ),
            230 => 
            array (
                'user_id' => 1231,
                'role_id' => 2,
            ),
            231 => 
            array (
                'user_id' => 1232,
                'role_id' => 2,
            ),
            232 => 
            array (
                'user_id' => 1233,
                'role_id' => 2,
            ),
            233 => 
            array (
                'user_id' => 1234,
                'role_id' => 2,
            ),
            234 => 
            array (
                'user_id' => 1235,
                'role_id' => 2,
            ),
            235 => 
            array (
                'user_id' => 1236,
                'role_id' => 2,
            ),
            236 => 
            array (
                'user_id' => 1237,
                'role_id' => 2,
            ),
            237 => 
            array (
                'user_id' => 1238,
                'role_id' => 2,
            ),
            238 => 
            array (
                'user_id' => 1239,
                'role_id' => 2,
            ),
            239 => 
            array (
                'user_id' => 1240,
                'role_id' => 2,
            ),
            240 => 
            array (
                'user_id' => 1241,
                'role_id' => 2,
            ),
            241 => 
            array (
                'user_id' => 1242,
                'role_id' => 2,
            ),
            242 => 
            array (
                'user_id' => 1243,
                'role_id' => 2,
            ),
            243 => 
            array (
                'user_id' => 1244,
                'role_id' => 2,
            ),
            244 => 
            array (
                'user_id' => 1245,
                'role_id' => 2,
            ),
            245 => 
            array (
                'user_id' => 1246,
                'role_id' => 2,
            ),
            246 => 
            array (
                'user_id' => 1247,
                'role_id' => 2,
            ),
            247 => 
            array (
                'user_id' => 1248,
                'role_id' => 2,
            ),
            248 => 
            array (
                'user_id' => 1249,
                'role_id' => 2,
            ),
            249 => 
            array (
                'user_id' => 1250,
                'role_id' => 2,
            ),
            250 => 
            array (
                'user_id' => 1251,
                'role_id' => 2,
            ),
            251 => 
            array (
                'user_id' => 1252,
                'role_id' => 2,
            ),
            252 => 
            array (
                'user_id' => 1253,
                'role_id' => 2,
            ),
            253 => 
            array (
                'user_id' => 1254,
                'role_id' => 2,
            ),
            254 => 
            array (
                'user_id' => 1255,
                'role_id' => 2,
            ),
            255 => 
            array (
                'user_id' => 1256,
                'role_id' => 2,
            ),
            256 => 
            array (
                'user_id' => 1257,
                'role_id' => 2,
            ),
            257 => 
            array (
                'user_id' => 1258,
                'role_id' => 2,
            ),
            258 => 
            array (
                'user_id' => 1259,
                'role_id' => 2,
            ),
            259 => 
            array (
                'user_id' => 1260,
                'role_id' => 2,
            ),
            260 => 
            array (
                'user_id' => 1261,
                'role_id' => 2,
            ),
            261 => 
            array (
                'user_id' => 1262,
                'role_id' => 2,
            ),
            262 => 
            array (
                'user_id' => 1263,
                'role_id' => 2,
            ),
            263 => 
            array (
                'user_id' => 1264,
                'role_id' => 2,
            ),
            264 => 
            array (
                'user_id' => 1265,
                'role_id' => 2,
            ),
            265 => 
            array (
                'user_id' => 1266,
                'role_id' => 2,
            ),
            266 => 
            array (
                'user_id' => 1267,
                'role_id' => 2,
            ),
            267 => 
            array (
                'user_id' => 1268,
                'role_id' => 2,
            ),
            268 => 
            array (
                'user_id' => 1269,
                'role_id' => 2,
            ),
            269 => 
            array (
                'user_id' => 1270,
                'role_id' => 2,
            ),
            270 => 
            array (
                'user_id' => 1271,
                'role_id' => 2,
            ),
            271 => 
            array (
                'user_id' => 1272,
                'role_id' => 2,
            ),
            272 => 
            array (
                'user_id' => 1273,
                'role_id' => 2,
            ),
            273 => 
            array (
                'user_id' => 1274,
                'role_id' => 2,
            ),
            274 => 
            array (
                'user_id' => 1275,
                'role_id' => 2,
            ),
            275 => 
            array (
                'user_id' => 1276,
                'role_id' => 2,
            ),
            276 => 
            array (
                'user_id' => 1277,
                'role_id' => 2,
            ),
            277 => 
            array (
                'user_id' => 1278,
                'role_id' => 2,
            ),
            278 => 
            array (
                'user_id' => 1279,
                'role_id' => 2,
            ),
            279 => 
            array (
                'user_id' => 1280,
                'role_id' => 2,
            ),
            280 => 
            array (
                'user_id' => 1281,
                'role_id' => 2,
            ),
            281 => 
            array (
                'user_id' => 1282,
                'role_id' => 2,
            ),
            282 => 
            array (
                'user_id' => 1283,
                'role_id' => 2,
            ),
            283 => 
            array (
                'user_id' => 1284,
                'role_id' => 2,
            ),
            284 => 
            array (
                'user_id' => 1285,
                'role_id' => 2,
            ),
            285 => 
            array (
                'user_id' => 1286,
                'role_id' => 2,
            ),
            286 => 
            array (
                'user_id' => 1287,
                'role_id' => 2,
            ),
            287 => 
            array (
                'user_id' => 1288,
                'role_id' => 2,
            ),
            288 => 
            array (
                'user_id' => 1289,
                'role_id' => 2,
            ),
            289 => 
            array (
                'user_id' => 1290,
                'role_id' => 2,
            ),
            290 => 
            array (
                'user_id' => 1291,
                'role_id' => 2,
            ),
            291 => 
            array (
                'user_id' => 1292,
                'role_id' => 2,
            ),
            292 => 
            array (
                'user_id' => 1293,
                'role_id' => 2,
            ),
            293 => 
            array (
                'user_id' => 1294,
                'role_id' => 2,
            ),
            294 => 
            array (
                'user_id' => 1295,
                'role_id' => 2,
            ),
            295 => 
            array (
                'user_id' => 1296,
                'role_id' => 2,
            ),
            296 => 
            array (
                'user_id' => 1297,
                'role_id' => 2,
            ),
            297 => 
            array (
                'user_id' => 1298,
                'role_id' => 2,
            ),
            298 => 
            array (
                'user_id' => 1299,
                'role_id' => 2,
            ),
            299 => 
            array (
                'user_id' => 1300,
                'role_id' => 2,
            ),
            300 => 
            array (
                'user_id' => 1301,
                'role_id' => 2,
            ),
            301 => 
            array (
                'user_id' => 1302,
                'role_id' => 2,
            ),
            302 => 
            array (
                'user_id' => 1303,
                'role_id' => 2,
            ),
            303 => 
            array (
                'user_id' => 1304,
                'role_id' => 2,
            ),
            304 => 
            array (
                'user_id' => 1305,
                'role_id' => 2,
            ),
            305 => 
            array (
                'user_id' => 1306,
                'role_id' => 2,
            ),
            306 => 
            array (
                'user_id' => 1307,
                'role_id' => 2,
            ),
            307 => 
            array (
                'user_id' => 1308,
                'role_id' => 2,
            ),
            308 => 
            array (
                'user_id' => 1309,
                'role_id' => 2,
            ),
            309 => 
            array (
                'user_id' => 1310,
                'role_id' => 2,
            ),
            310 => 
            array (
                'user_id' => 1311,
                'role_id' => 2,
            ),
            311 => 
            array (
                'user_id' => 1312,
                'role_id' => 2,
            ),
            312 => 
            array (
                'user_id' => 1313,
                'role_id' => 2,
            ),
            313 => 
            array (
                'user_id' => 1314,
                'role_id' => 2,
            ),
            314 => 
            array (
                'user_id' => 1315,
                'role_id' => 2,
            ),
            315 => 
            array (
                'user_id' => 1316,
                'role_id' => 2,
            ),
            316 => 
            array (
                'user_id' => 1317,
                'role_id' => 2,
            ),
            317 => 
            array (
                'user_id' => 1318,
                'role_id' => 2,
            ),
            318 => 
            array (
                'user_id' => 1319,
                'role_id' => 2,
            ),
            319 => 
            array (
                'user_id' => 1320,
                'role_id' => 2,
            ),
            320 => 
            array (
                'user_id' => 1321,
                'role_id' => 2,
            ),
            321 => 
            array (
                'user_id' => 1322,
                'role_id' => 2,
            ),
            322 => 
            array (
                'user_id' => 1323,
                'role_id' => 2,
            ),
            323 => 
            array (
                'user_id' => 1324,
                'role_id' => 2,
            ),
            324 => 
            array (
                'user_id' => 1325,
                'role_id' => 2,
            ),
            325 => 
            array (
                'user_id' => 1326,
                'role_id' => 2,
            ),
            326 => 
            array (
                'user_id' => 1327,
                'role_id' => 2,
            ),
            327 => 
            array (
                'user_id' => 1328,
                'role_id' => 2,
            ),
            328 => 
            array (
                'user_id' => 1329,
                'role_id' => 2,
            ),
            329 => 
            array (
                'user_id' => 1330,
                'role_id' => 2,
            ),
            330 => 
            array (
                'user_id' => 1331,
                'role_id' => 2,
            ),
            331 => 
            array (
                'user_id' => 1332,
                'role_id' => 2,
            ),
            332 => 
            array (
                'user_id' => 1333,
                'role_id' => 2,
            ),
            333 => 
            array (
                'user_id' => 1334,
                'role_id' => 2,
            ),
            334 => 
            array (
                'user_id' => 1335,
                'role_id' => 2,
            ),
            335 => 
            array (
                'user_id' => 1336,
                'role_id' => 2,
            ),
            336 => 
            array (
                'user_id' => 1337,
                'role_id' => 2,
            ),
            337 => 
            array (
                'user_id' => 1338,
                'role_id' => 2,
            ),
            338 => 
            array (
                'user_id' => 1339,
                'role_id' => 2,
            ),
            339 => 
            array (
                'user_id' => 1340,
                'role_id' => 2,
            ),
            340 => 
            array (
                'user_id' => 1341,
                'role_id' => 2,
            ),
            341 => 
            array (
                'user_id' => 1342,
                'role_id' => 2,
            ),
            342 => 
            array (
                'user_id' => 1343,
                'role_id' => 2,
            ),
            343 => 
            array (
                'user_id' => 1344,
                'role_id' => 2,
            ),
            344 => 
            array (
                'user_id' => 1345,
                'role_id' => 2,
            ),
            345 => 
            array (
                'user_id' => 1346,
                'role_id' => 2,
            ),
            346 => 
            array (
                'user_id' => 1347,
                'role_id' => 2,
            ),
            347 => 
            array (
                'user_id' => 1348,
                'role_id' => 2,
            ),
            348 => 
            array (
                'user_id' => 1349,
                'role_id' => 2,
            ),
            349 => 
            array (
                'user_id' => 1350,
                'role_id' => 2,
            ),
            350 => 
            array (
                'user_id' => 1351,
                'role_id' => 2,
            ),
            351 => 
            array (
                'user_id' => 1352,
                'role_id' => 2,
            ),
            352 => 
            array (
                'user_id' => 1353,
                'role_id' => 2,
            ),
            353 => 
            array (
                'user_id' => 1354,
                'role_id' => 2,
            ),
            354 => 
            array (
                'user_id' => 1355,
                'role_id' => 2,
            ),
            355 => 
            array (
                'user_id' => 1356,
                'role_id' => 2,
            ),
            356 => 
            array (
                'user_id' => 1357,
                'role_id' => 2,
            ),
            357 => 
            array (
                'user_id' => 1358,
                'role_id' => 2,
            ),
            358 => 
            array (
                'user_id' => 1359,
                'role_id' => 2,
            ),
            359 => 
            array (
                'user_id' => 1360,
                'role_id' => 2,
            ),
            360 => 
            array (
                'user_id' => 1361,
                'role_id' => 2,
            ),
            361 => 
            array (
                'user_id' => 1362,
                'role_id' => 2,
            ),
            362 => 
            array (
                'user_id' => 1363,
                'role_id' => 2,
            ),
            363 => 
            array (
                'user_id' => 1364,
                'role_id' => 2,
            ),
            364 => 
            array (
                'user_id' => 1365,
                'role_id' => 2,
            ),
            365 => 
            array (
                'user_id' => 1366,
                'role_id' => 2,
            ),
            366 => 
            array (
                'user_id' => 1367,
                'role_id' => 2,
            ),
            367 => 
            array (
                'user_id' => 1368,
                'role_id' => 2,
            ),
            368 => 
            array (
                'user_id' => 1369,
                'role_id' => 2,
            ),
            369 => 
            array (
                'user_id' => 1370,
                'role_id' => 2,
            ),
            370 => 
            array (
                'user_id' => 1371,
                'role_id' => 2,
            ),
            371 => 
            array (
                'user_id' => 1372,
                'role_id' => 2,
            ),
            372 => 
            array (
                'user_id' => 1373,
                'role_id' => 2,
            ),
            373 => 
            array (
                'user_id' => 1374,
                'role_id' => 2,
            ),
            374 => 
            array (
                'user_id' => 1375,
                'role_id' => 2,
            ),
            375 => 
            array (
                'user_id' => 1376,
                'role_id' => 2,
            ),
            376 => 
            array (
                'user_id' => 1377,
                'role_id' => 2,
            ),
            377 => 
            array (
                'user_id' => 1378,
                'role_id' => 2,
            ),
            378 => 
            array (
                'user_id' => 1379,
                'role_id' => 2,
            ),
            379 => 
            array (
                'user_id' => 1380,
                'role_id' => 2,
            ),
            380 => 
            array (
                'user_id' => 1381,
                'role_id' => 2,
            ),
            381 => 
            array (
                'user_id' => 1382,
                'role_id' => 2,
            ),
            382 => 
            array (
                'user_id' => 1383,
                'role_id' => 2,
            ),
            383 => 
            array (
                'user_id' => 1384,
                'role_id' => 2,
            ),
            384 => 
            array (
                'user_id' => 1385,
                'role_id' => 2,
            ),
            385 => 
            array (
                'user_id' => 1386,
                'role_id' => 2,
            ),
            386 => 
            array (
                'user_id' => 1387,
                'role_id' => 2,
            ),
            387 => 
            array (
                'user_id' => 1388,
                'role_id' => 2,
            ),
            388 => 
            array (
                'user_id' => 1389,
                'role_id' => 2,
            ),
            389 => 
            array (
                'user_id' => 1390,
                'role_id' => 2,
            ),
            390 => 
            array (
                'user_id' => 1391,
                'role_id' => 2,
            ),
            391 => 
            array (
                'user_id' => 1392,
                'role_id' => 2,
            ),
            392 => 
            array (
                'user_id' => 1393,
                'role_id' => 2,
            ),
            393 => 
            array (
                'user_id' => 1394,
                'role_id' => 2,
            ),
            394 => 
            array (
                'user_id' => 1395,
                'role_id' => 2,
            ),
            395 => 
            array (
                'user_id' => 1396,
                'role_id' => 2,
            ),
            396 => 
            array (
                'user_id' => 1397,
                'role_id' => 2,
            ),
            397 => 
            array (
                'user_id' => 1398,
                'role_id' => 2,
            ),
            398 => 
            array (
                'user_id' => 1399,
                'role_id' => 2,
            ),
            399 => 
            array (
                'user_id' => 1400,
                'role_id' => 2,
            ),
            400 => 
            array (
                'user_id' => 1401,
                'role_id' => 2,
            ),
            401 => 
            array (
                'user_id' => 1402,
                'role_id' => 2,
            ),
            402 => 
            array (
                'user_id' => 1403,
                'role_id' => 2,
            ),
            403 => 
            array (
                'user_id' => 1404,
                'role_id' => 2,
            ),
            404 => 
            array (
                'user_id' => 1405,
                'role_id' => 2,
            ),
            405 => 
            array (
                'user_id' => 1406,
                'role_id' => 2,
            ),
            406 => 
            array (
                'user_id' => 1407,
                'role_id' => 2,
            ),
            407 => 
            array (
                'user_id' => 1408,
                'role_id' => 2,
            ),
            408 => 
            array (
                'user_id' => 1409,
                'role_id' => 2,
            ),
            409 => 
            array (
                'user_id' => 1410,
                'role_id' => 2,
            ),
            410 => 
            array (
                'user_id' => 1411,
                'role_id' => 2,
            ),
            411 => 
            array (
                'user_id' => 1412,
                'role_id' => 2,
            ),
            412 => 
            array (
                'user_id' => 1413,
                'role_id' => 2,
            ),
            413 => 
            array (
                'user_id' => 1414,
                'role_id' => 2,
            ),
            414 => 
            array (
                'user_id' => 1415,
                'role_id' => 2,
            ),
            415 => 
            array (
                'user_id' => 1416,
                'role_id' => 2,
            ),
            416 => 
            array (
                'user_id' => 1417,
                'role_id' => 2,
            ),
            417 => 
            array (
                'user_id' => 1418,
                'role_id' => 2,
            ),
            418 => 
            array (
                'user_id' => 1419,
                'role_id' => 2,
            ),
            419 => 
            array (
                'user_id' => 1420,
                'role_id' => 2,
            ),
            420 => 
            array (
                'user_id' => 1421,
                'role_id' => 2,
            ),
            421 => 
            array (
                'user_id' => 1422,
                'role_id' => 2,
            ),
            422 => 
            array (
                'user_id' => 1423,
                'role_id' => 2,
            ),
            423 => 
            array (
                'user_id' => 1424,
                'role_id' => 2,
            ),
            424 => 
            array (
                'user_id' => 1425,
                'role_id' => 2,
            ),
            425 => 
            array (
                'user_id' => 1426,
                'role_id' => 2,
            ),
            426 => 
            array (
                'user_id' => 1427,
                'role_id' => 2,
            ),
            427 => 
            array (
                'user_id' => 1428,
                'role_id' => 2,
            ),
            428 => 
            array (
                'user_id' => 1429,
                'role_id' => 2,
            ),
            429 => 
            array (
                'user_id' => 1430,
                'role_id' => 2,
            ),
            430 => 
            array (
                'user_id' => 1431,
                'role_id' => 2,
            ),
            431 => 
            array (
                'user_id' => 1432,
                'role_id' => 2,
            ),
            432 => 
            array (
                'user_id' => 1433,
                'role_id' => 2,
            ),
            433 => 
            array (
                'user_id' => 1434,
                'role_id' => 2,
            ),
            434 => 
            array (
                'user_id' => 1435,
                'role_id' => 2,
            ),
            435 => 
            array (
                'user_id' => 1436,
                'role_id' => 2,
            ),
            436 => 
            array (
                'user_id' => 1437,
                'role_id' => 2,
            ),
            437 => 
            array (
                'user_id' => 1438,
                'role_id' => 2,
            ),
            438 => 
            array (
                'user_id' => 1439,
                'role_id' => 2,
            ),
            439 => 
            array (
                'user_id' => 1440,
                'role_id' => 2,
            ),
            440 => 
            array (
                'user_id' => 1441,
                'role_id' => 2,
            ),
            441 => 
            array (
                'user_id' => 1442,
                'role_id' => 2,
            ),
            442 => 
            array (
                'user_id' => 1443,
                'role_id' => 2,
            ),
            443 => 
            array (
                'user_id' => 1444,
                'role_id' => 2,
            ),
            444 => 
            array (
                'user_id' => 1445,
                'role_id' => 2,
            ),
            445 => 
            array (
                'user_id' => 1446,
                'role_id' => 2,
            ),
            446 => 
            array (
                'user_id' => 1447,
                'role_id' => 2,
            ),
            447 => 
            array (
                'user_id' => 1448,
                'role_id' => 2,
            ),
            448 => 
            array (
                'user_id' => 1449,
                'role_id' => 2,
            ),
            449 => 
            array (
                'user_id' => 1450,
                'role_id' => 2,
            ),
            450 => 
            array (
                'user_id' => 1451,
                'role_id' => 2,
            ),
            451 => 
            array (
                'user_id' => 1452,
                'role_id' => 2,
            ),
            452 => 
            array (
                'user_id' => 1453,
                'role_id' => 2,
            ),
            453 => 
            array (
                'user_id' => 1454,
                'role_id' => 2,
            ),
            454 => 
            array (
                'user_id' => 1455,
                'role_id' => 2,
            ),
            455 => 
            array (
                'user_id' => 1456,
                'role_id' => 2,
            ),
            456 => 
            array (
                'user_id' => 1457,
                'role_id' => 2,
            ),
            457 => 
            array (
                'user_id' => 1458,
                'role_id' => 2,
            ),
            458 => 
            array (
                'user_id' => 1459,
                'role_id' => 2,
            ),
            459 => 
            array (
                'user_id' => 1460,
                'role_id' => 2,
            ),
            460 => 
            array (
                'user_id' => 1461,
                'role_id' => 2,
            ),
            461 => 
            array (
                'user_id' => 1462,
                'role_id' => 2,
            ),
            462 => 
            array (
                'user_id' => 1463,
                'role_id' => 2,
            ),
            463 => 
            array (
                'user_id' => 1464,
                'role_id' => 2,
            ),
            464 => 
            array (
                'user_id' => 1465,
                'role_id' => 2,
            ),
            465 => 
            array (
                'user_id' => 1466,
                'role_id' => 2,
            ),
            466 => 
            array (
                'user_id' => 1467,
                'role_id' => 2,
            ),
            467 => 
            array (
                'user_id' => 1468,
                'role_id' => 2,
            ),
            468 => 
            array (
                'user_id' => 1469,
                'role_id' => 2,
            ),
            469 => 
            array (
                'user_id' => 1470,
                'role_id' => 2,
            ),
            470 => 
            array (
                'user_id' => 1471,
                'role_id' => 2,
            ),
            471 => 
            array (
                'user_id' => 1472,
                'role_id' => 2,
            ),
            472 => 
            array (
                'user_id' => 1473,
                'role_id' => 2,
            ),
            473 => 
            array (
                'user_id' => 1474,
                'role_id' => 2,
            ),
            474 => 
            array (
                'user_id' => 1475,
                'role_id' => 2,
            ),
            475 => 
            array (
                'user_id' => 1476,
                'role_id' => 2,
            ),
            476 => 
            array (
                'user_id' => 1477,
                'role_id' => 2,
            ),
            477 => 
            array (
                'user_id' => 1478,
                'role_id' => 2,
            ),
            478 => 
            array (
                'user_id' => 1479,
                'role_id' => 2,
            ),
            479 => 
            array (
                'user_id' => 1480,
                'role_id' => 2,
            ),
            480 => 
            array (
                'user_id' => 1481,
                'role_id' => 2,
            ),
            481 => 
            array (
                'user_id' => 1482,
                'role_id' => 2,
            ),
            482 => 
            array (
                'user_id' => 1483,
                'role_id' => 2,
            ),
            483 => 
            array (
                'user_id' => 1484,
                'role_id' => 2,
            ),
            484 => 
            array (
                'user_id' => 1485,
                'role_id' => 2,
            ),
            485 => 
            array (
                'user_id' => 1486,
                'role_id' => 2,
            ),
            486 => 
            array (
                'user_id' => 1487,
                'role_id' => 2,
            ),
            487 => 
            array (
                'user_id' => 1488,
                'role_id' => 2,
            ),
            488 => 
            array (
                'user_id' => 1489,
                'role_id' => 2,
            ),
            489 => 
            array (
                'user_id' => 1490,
                'role_id' => 2,
            ),
            490 => 
            array (
                'user_id' => 1491,
                'role_id' => 2,
            ),
            491 => 
            array (
                'user_id' => 1492,
                'role_id' => 2,
            ),
            492 => 
            array (
                'user_id' => 1493,
                'role_id' => 2,
            ),
            493 => 
            array (
                'user_id' => 1494,
                'role_id' => 2,
            ),
            494 => 
            array (
                'user_id' => 1495,
                'role_id' => 2,
            ),
            495 => 
            array (
                'user_id' => 1496,
                'role_id' => 2,
            ),
            496 => 
            array (
                'user_id' => 1497,
                'role_id' => 2,
            ),
            497 => 
            array (
                'user_id' => 1498,
                'role_id' => 2,
            ),
            498 => 
            array (
                'user_id' => 1499,
                'role_id' => 2,
            ),
            499 => 
            array (
                'user_id' => 1500,
                'role_id' => 2,
            ),
        ));
        \DB::table('role_user')->insert(array (
            0 => 
            array (
                'user_id' => 1501,
                'role_id' => 2,
            ),
            1 => 
            array (
                'user_id' => 1502,
                'role_id' => 2,
            ),
            2 => 
            array (
                'user_id' => 1503,
                'role_id' => 2,
            ),
            3 => 
            array (
                'user_id' => 1504,
                'role_id' => 2,
            ),
            4 => 
            array (
                'user_id' => 1505,
                'role_id' => 2,
            ),
            5 => 
            array (
                'user_id' => 1506,
                'role_id' => 2,
            ),
            6 => 
            array (
                'user_id' => 1507,
                'role_id' => 2,
            ),
            7 => 
            array (
                'user_id' => 1508,
                'role_id' => 2,
            ),
            8 => 
            array (
                'user_id' => 1509,
                'role_id' => 2,
            ),
            9 => 
            array (
                'user_id' => 1510,
                'role_id' => 2,
            ),
            10 => 
            array (
                'user_id' => 1511,
                'role_id' => 2,
            ),
            11 => 
            array (
                'user_id' => 1512,
                'role_id' => 2,
            ),
            12 => 
            array (
                'user_id' => 1513,
                'role_id' => 2,
            ),
            13 => 
            array (
                'user_id' => 1514,
                'role_id' => 2,
            ),
            14 => 
            array (
                'user_id' => 1515,
                'role_id' => 2,
            ),
            15 => 
            array (
                'user_id' => 1516,
                'role_id' => 2,
            ),
            16 => 
            array (
                'user_id' => 1517,
                'role_id' => 2,
            ),
            17 => 
            array (
                'user_id' => 1518,
                'role_id' => 2,
            ),
            18 => 
            array (
                'user_id' => 1519,
                'role_id' => 2,
            ),
            19 => 
            array (
                'user_id' => 1520,
                'role_id' => 2,
            ),
            20 => 
            array (
                'user_id' => 1521,
                'role_id' => 2,
            ),
            21 => 
            array (
                'user_id' => 1522,
                'role_id' => 2,
            ),
            22 => 
            array (
                'user_id' => 1523,
                'role_id' => 2,
            ),
            23 => 
            array (
                'user_id' => 1524,
                'role_id' => 2,
            ),
            24 => 
            array (
                'user_id' => 1525,
                'role_id' => 2,
            ),
            25 => 
            array (
                'user_id' => 1526,
                'role_id' => 2,
            ),
            26 => 
            array (
                'user_id' => 1527,
                'role_id' => 2,
            ),
            27 => 
            array (
                'user_id' => 1528,
                'role_id' => 2,
            ),
            28 => 
            array (
                'user_id' => 1529,
                'role_id' => 2,
            ),
            29 => 
            array (
                'user_id' => 1530,
                'role_id' => 2,
            ),
            30 => 
            array (
                'user_id' => 1531,
                'role_id' => 2,
            ),
            31 => 
            array (
                'user_id' => 1532,
                'role_id' => 2,
            ),
            32 => 
            array (
                'user_id' => 1533,
                'role_id' => 2,
            ),
            33 => 
            array (
                'user_id' => 1534,
                'role_id' => 2,
            ),
            34 => 
            array (
                'user_id' => 1535,
                'role_id' => 2,
            ),
            35 => 
            array (
                'user_id' => 1536,
                'role_id' => 2,
            ),
            36 => 
            array (
                'user_id' => 1537,
                'role_id' => 2,
            ),
            37 => 
            array (
                'user_id' => 1538,
                'role_id' => 2,
            ),
            38 => 
            array (
                'user_id' => 1539,
                'role_id' => 2,
            ),
            39 => 
            array (
                'user_id' => 1540,
                'role_id' => 2,
            ),
            40 => 
            array (
                'user_id' => 1541,
                'role_id' => 2,
            ),
            41 => 
            array (
                'user_id' => 1542,
                'role_id' => 2,
            ),
            42 => 
            array (
                'user_id' => 1543,
                'role_id' => 2,
            ),
            43 => 
            array (
                'user_id' => 1544,
                'role_id' => 2,
            ),
            44 => 
            array (
                'user_id' => 1545,
                'role_id' => 2,
            ),
            45 => 
            array (
                'user_id' => 1546,
                'role_id' => 2,
            ),
            46 => 
            array (
                'user_id' => 1547,
                'role_id' => 2,
            ),
            47 => 
            array (
                'user_id' => 1548,
                'role_id' => 2,
            ),
            48 => 
            array (
                'user_id' => 1549,
                'role_id' => 2,
            ),
            49 => 
            array (
                'user_id' => 1550,
                'role_id' => 2,
            ),
            50 => 
            array (
                'user_id' => 1551,
                'role_id' => 2,
            ),
            51 => 
            array (
                'user_id' => 1552,
                'role_id' => 2,
            ),
            52 => 
            array (
                'user_id' => 1553,
                'role_id' => 2,
            ),
            53 => 
            array (
                'user_id' => 1554,
                'role_id' => 2,
            ),
            54 => 
            array (
                'user_id' => 1555,
                'role_id' => 2,
            ),
            55 => 
            array (
                'user_id' => 1556,
                'role_id' => 2,
            ),
            56 => 
            array (
                'user_id' => 1557,
                'role_id' => 2,
            ),
            57 => 
            array (
                'user_id' => 1558,
                'role_id' => 2,
            ),
            58 => 
            array (
                'user_id' => 1559,
                'role_id' => 2,
            ),
            59 => 
            array (
                'user_id' => 1560,
                'role_id' => 2,
            ),
            60 => 
            array (
                'user_id' => 1561,
                'role_id' => 2,
            ),
            61 => 
            array (
                'user_id' => 1562,
                'role_id' => 2,
            ),
            62 => 
            array (
                'user_id' => 1563,
                'role_id' => 2,
            ),
            63 => 
            array (
                'user_id' => 1564,
                'role_id' => 2,
            ),
            64 => 
            array (
                'user_id' => 1565,
                'role_id' => 2,
            ),
            65 => 
            array (
                'user_id' => 1566,
                'role_id' => 2,
            ),
            66 => 
            array (
                'user_id' => 1567,
                'role_id' => 2,
            ),
            67 => 
            array (
                'user_id' => 1568,
                'role_id' => 2,
            ),
            68 => 
            array (
                'user_id' => 1569,
                'role_id' => 2,
            ),
            69 => 
            array (
                'user_id' => 1570,
                'role_id' => 2,
            ),
            70 => 
            array (
                'user_id' => 1571,
                'role_id' => 2,
            ),
            71 => 
            array (
                'user_id' => 1572,
                'role_id' => 2,
            ),
            72 => 
            array (
                'user_id' => 1573,
                'role_id' => 2,
            ),
            73 => 
            array (
                'user_id' => 1574,
                'role_id' => 2,
            ),
            74 => 
            array (
                'user_id' => 1575,
                'role_id' => 2,
            ),
            75 => 
            array (
                'user_id' => 1576,
                'role_id' => 2,
            ),
            76 => 
            array (
                'user_id' => 1577,
                'role_id' => 2,
            ),
            77 => 
            array (
                'user_id' => 1578,
                'role_id' => 2,
            ),
            78 => 
            array (
                'user_id' => 1579,
                'role_id' => 2,
            ),
            79 => 
            array (
                'user_id' => 1580,
                'role_id' => 2,
            ),
            80 => 
            array (
                'user_id' => 1581,
                'role_id' => 2,
            ),
            81 => 
            array (
                'user_id' => 1582,
                'role_id' => 2,
            ),
            82 => 
            array (
                'user_id' => 1583,
                'role_id' => 2,
            ),
            83 => 
            array (
                'user_id' => 1584,
                'role_id' => 2,
            ),
            84 => 
            array (
                'user_id' => 1585,
                'role_id' => 2,
            ),
            85 => 
            array (
                'user_id' => 1586,
                'role_id' => 2,
            ),
            86 => 
            array (
                'user_id' => 1587,
                'role_id' => 2,
            ),
            87 => 
            array (
                'user_id' => 1588,
                'role_id' => 2,
            ),
            88 => 
            array (
                'user_id' => 1589,
                'role_id' => 2,
            ),
            89 => 
            array (
                'user_id' => 1590,
                'role_id' => 2,
            ),
            90 => 
            array (
                'user_id' => 1591,
                'role_id' => 2,
            ),
            91 => 
            array (
                'user_id' => 1592,
                'role_id' => 2,
            ),
            92 => 
            array (
                'user_id' => 1593,
                'role_id' => 2,
            ),
            93 => 
            array (
                'user_id' => 1594,
                'role_id' => 2,
            ),
            94 => 
            array (
                'user_id' => 1595,
                'role_id' => 2,
            ),
            95 => 
            array (
                'user_id' => 1596,
                'role_id' => 2,
            ),
            96 => 
            array (
                'user_id' => 1597,
                'role_id' => 2,
            ),
            97 => 
            array (
                'user_id' => 1598,
                'role_id' => 2,
            ),
            98 => 
            array (
                'user_id' => 1599,
                'role_id' => 2,
            ),
            99 => 
            array (
                'user_id' => 1600,
                'role_id' => 2,
            ),
            100 => 
            array (
                'user_id' => 1601,
                'role_id' => 2,
            ),
            101 => 
            array (
                'user_id' => 1602,
                'role_id' => 2,
            ),
            102 => 
            array (
                'user_id' => 1603,
                'role_id' => 2,
            ),
            103 => 
            array (
                'user_id' => 1604,
                'role_id' => 2,
            ),
            104 => 
            array (
                'user_id' => 1605,
                'role_id' => 2,
            ),
            105 => 
            array (
                'user_id' => 1606,
                'role_id' => 2,
            ),
            106 => 
            array (
                'user_id' => 1607,
                'role_id' => 2,
            ),
            107 => 
            array (
                'user_id' => 1608,
                'role_id' => 2,
            ),
            108 => 
            array (
                'user_id' => 1609,
                'role_id' => 2,
            ),
            109 => 
            array (
                'user_id' => 1610,
                'role_id' => 2,
            ),
            110 => 
            array (
                'user_id' => 1611,
                'role_id' => 2,
            ),
            111 => 
            array (
                'user_id' => 1612,
                'role_id' => 2,
            ),
            112 => 
            array (
                'user_id' => 1613,
                'role_id' => 2,
            ),
            113 => 
            array (
                'user_id' => 1614,
                'role_id' => 2,
            ),
            114 => 
            array (
                'user_id' => 1615,
                'role_id' => 2,
            ),
            115 => 
            array (
                'user_id' => 1616,
                'role_id' => 2,
            ),
            116 => 
            array (
                'user_id' => 1617,
                'role_id' => 2,
            ),
            117 => 
            array (
                'user_id' => 1618,
                'role_id' => 2,
            ),
            118 => 
            array (
                'user_id' => 1619,
                'role_id' => 2,
            ),
            119 => 
            array (
                'user_id' => 1620,
                'role_id' => 2,
            ),
            120 => 
            array (
                'user_id' => 1621,
                'role_id' => 2,
            ),
            121 => 
            array (
                'user_id' => 1622,
                'role_id' => 2,
            ),
            122 => 
            array (
                'user_id' => 1623,
                'role_id' => 2,
            ),
            123 => 
            array (
                'user_id' => 1624,
                'role_id' => 2,
            ),
            124 => 
            array (
                'user_id' => 1625,
                'role_id' => 2,
            ),
            125 => 
            array (
                'user_id' => 1626,
                'role_id' => 2,
            ),
            126 => 
            array (
                'user_id' => 1627,
                'role_id' => 2,
            ),
            127 => 
            array (
                'user_id' => 1628,
                'role_id' => 2,
            ),
            128 => 
            array (
                'user_id' => 1629,
                'role_id' => 2,
            ),
            129 => 
            array (
                'user_id' => 1630,
                'role_id' => 2,
            ),
            130 => 
            array (
                'user_id' => 1631,
                'role_id' => 2,
            ),
            131 => 
            array (
                'user_id' => 1632,
                'role_id' => 2,
            ),
            132 => 
            array (
                'user_id' => 1633,
                'role_id' => 2,
            ),
            133 => 
            array (
                'user_id' => 1634,
                'role_id' => 2,
            ),
            134 => 
            array (
                'user_id' => 1635,
                'role_id' => 2,
            ),
            135 => 
            array (
                'user_id' => 1636,
                'role_id' => 2,
            ),
            136 => 
            array (
                'user_id' => 1637,
                'role_id' => 2,
            ),
            137 => 
            array (
                'user_id' => 1638,
                'role_id' => 2,
            ),
            138 => 
            array (
                'user_id' => 1639,
                'role_id' => 2,
            ),
            139 => 
            array (
                'user_id' => 1640,
                'role_id' => 2,
            ),
            140 => 
            array (
                'user_id' => 1641,
                'role_id' => 2,
            ),
            141 => 
            array (
                'user_id' => 1642,
                'role_id' => 2,
            ),
            142 => 
            array (
                'user_id' => 1643,
                'role_id' => 2,
            ),
            143 => 
            array (
                'user_id' => 1644,
                'role_id' => 2,
            ),
            144 => 
            array (
                'user_id' => 1645,
                'role_id' => 2,
            ),
            145 => 
            array (
                'user_id' => 1646,
                'role_id' => 2,
            ),
            146 => 
            array (
                'user_id' => 1647,
                'role_id' => 2,
            ),
            147 => 
            array (
                'user_id' => 1648,
                'role_id' => 2,
            ),
            148 => 
            array (
                'user_id' => 1649,
                'role_id' => 2,
            ),
            149 => 
            array (
                'user_id' => 1650,
                'role_id' => 2,
            ),
            150 => 
            array (
                'user_id' => 1651,
                'role_id' => 2,
            ),
            151 => 
            array (
                'user_id' => 1652,
                'role_id' => 2,
            ),
            152 => 
            array (
                'user_id' => 1653,
                'role_id' => 2,
            ),
            153 => 
            array (
                'user_id' => 1654,
                'role_id' => 2,
            ),
            154 => 
            array (
                'user_id' => 1655,
                'role_id' => 2,
            ),
            155 => 
            array (
                'user_id' => 1656,
                'role_id' => 2,
            ),
            156 => 
            array (
                'user_id' => 1657,
                'role_id' => 2,
            ),
            157 => 
            array (
                'user_id' => 1658,
                'role_id' => 2,
            ),
            158 => 
            array (
                'user_id' => 1659,
                'role_id' => 2,
            ),
            159 => 
            array (
                'user_id' => 1660,
                'role_id' => 2,
            ),
            160 => 
            array (
                'user_id' => 1661,
                'role_id' => 2,
            ),
            161 => 
            array (
                'user_id' => 1662,
                'role_id' => 2,
            ),
            162 => 
            array (
                'user_id' => 1663,
                'role_id' => 2,
            ),
            163 => 
            array (
                'user_id' => 1664,
                'role_id' => 2,
            ),
            164 => 
            array (
                'user_id' => 1665,
                'role_id' => 2,
            ),
            165 => 
            array (
                'user_id' => 1666,
                'role_id' => 2,
            ),
            166 => 
            array (
                'user_id' => 1667,
                'role_id' => 2,
            ),
            167 => 
            array (
                'user_id' => 1668,
                'role_id' => 2,
            ),
            168 => 
            array (
                'user_id' => 1669,
                'role_id' => 2,
            ),
            169 => 
            array (
                'user_id' => 1670,
                'role_id' => 2,
            ),
            170 => 
            array (
                'user_id' => 1671,
                'role_id' => 2,
            ),
            171 => 
            array (
                'user_id' => 1672,
                'role_id' => 2,
            ),
            172 => 
            array (
                'user_id' => 1673,
                'role_id' => 2,
            ),
            173 => 
            array (
                'user_id' => 1674,
                'role_id' => 2,
            ),
            174 => 
            array (
                'user_id' => 1675,
                'role_id' => 2,
            ),
            175 => 
            array (
                'user_id' => 1676,
                'role_id' => 2,
            ),
            176 => 
            array (
                'user_id' => 1677,
                'role_id' => 2,
            ),
            177 => 
            array (
                'user_id' => 1678,
                'role_id' => 2,
            ),
            178 => 
            array (
                'user_id' => 1679,
                'role_id' => 2,
            ),
            179 => 
            array (
                'user_id' => 1680,
                'role_id' => 2,
            ),
            180 => 
            array (
                'user_id' => 1681,
                'role_id' => 2,
            ),
            181 => 
            array (
                'user_id' => 1682,
                'role_id' => 2,
            ),
            182 => 
            array (
                'user_id' => 1683,
                'role_id' => 2,
            ),
            183 => 
            array (
                'user_id' => 1684,
                'role_id' => 2,
            ),
            184 => 
            array (
                'user_id' => 1685,
                'role_id' => 2,
            ),
            185 => 
            array (
                'user_id' => 1686,
                'role_id' => 2,
            ),
            186 => 
            array (
                'user_id' => 1687,
                'role_id' => 2,
            ),
            187 => 
            array (
                'user_id' => 1688,
                'role_id' => 2,
            ),
            188 => 
            array (
                'user_id' => 1689,
                'role_id' => 2,
            ),
            189 => 
            array (
                'user_id' => 1690,
                'role_id' => 2,
            ),
            190 => 
            array (
                'user_id' => 1691,
                'role_id' => 2,
            ),
            191 => 
            array (
                'user_id' => 1692,
                'role_id' => 2,
            ),
            192 => 
            array (
                'user_id' => 1693,
                'role_id' => 2,
            ),
            193 => 
            array (
                'user_id' => 1694,
                'role_id' => 2,
            ),
            194 => 
            array (
                'user_id' => 1695,
                'role_id' => 2,
            ),
            195 => 
            array (
                'user_id' => 1696,
                'role_id' => 2,
            ),
            196 => 
            array (
                'user_id' => 1697,
                'role_id' => 2,
            ),
            197 => 
            array (
                'user_id' => 1698,
                'role_id' => 2,
            ),
            198 => 
            array (
                'user_id' => 1699,
                'role_id' => 2,
            ),
            199 => 
            array (
                'user_id' => 1700,
                'role_id' => 2,
            ),
            200 => 
            array (
                'user_id' => 1701,
                'role_id' => 2,
            ),
            201 => 
            array (
                'user_id' => 1702,
                'role_id' => 2,
            ),
            202 => 
            array (
                'user_id' => 1703,
                'role_id' => 2,
            ),
            203 => 
            array (
                'user_id' => 1704,
                'role_id' => 2,
            ),
            204 => 
            array (
                'user_id' => 1705,
                'role_id' => 2,
            ),
            205 => 
            array (
                'user_id' => 1706,
                'role_id' => 2,
            ),
            206 => 
            array (
                'user_id' => 1707,
                'role_id' => 2,
            ),
            207 => 
            array (
                'user_id' => 1708,
                'role_id' => 2,
            ),
            208 => 
            array (
                'user_id' => 1709,
                'role_id' => 2,
            ),
            209 => 
            array (
                'user_id' => 1710,
                'role_id' => 2,
            ),
            210 => 
            array (
                'user_id' => 1711,
                'role_id' => 2,
            ),
            211 => 
            array (
                'user_id' => 1712,
                'role_id' => 2,
            ),            
        ));
        
        
    }
}