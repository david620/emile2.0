<?php

use Illuminate\Database\Seeder;

class NoticiasTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('noticias')->delete();
        
        \DB::table('noticias')->insert(array (
            0 => 
            array (
                'id' => 34,
                'titulo' => 'BNB',
                'contenido' => 'Binance coin una excelente inversion a largo plazo, tanto el análisis tecnico como fundamental demostrando en el 2019 una gran alza mientras el mercado esta en tendencia bajista.',
                'link_imagen' => 'https://cdn.shortpixel.ai/client/q_glossy,ret_img,w_500,h_300/https://www.newsbtc.com/wp-content/uploads/2018/07/Binance-Burn-500x300.jpg',
                'link' => NULL,
            ),
            1 => 
            array (
                'id' => 59,
                'titulo' => 'XRP',
                'contenido' => 'Estamos en Los Últimos meses del 2019 y así como en el 2017 La criptomoneda XRP puede tener un gran movimiento a la Alza y más aún con los eventos y las noticias que vienen para XRP los últimos meses. excelente inversión a Mediano y Largo Plazo. precio actual 0.2562',
                'link_imagen' => 'imagenes/new_1568299688.png',
                'link' => NULL,
            ),
            2 => 
            array (
                'id' => 82,
                'titulo' => 'Profit4life',
                'contenido' => 'Estamos trabajando en un nuevo servicio que traerá a ustedes Profitcalls basadas en Órdenes pendientes con una imagen del análisis... Seguiremos mejorando para ustedes.',
                'link_imagen' => 'imagenes/new_1568919013.png',
                'link' => NULL,
            ),
            3 => 
            array (
                'id' => 83,
                'titulo' => 'Profit4life',
                'contenido' => 'El manejo del riesgo lo es todo acá pueden ver una cuenta que  ha crecido más de un 20% con Los Profitcalls, la rentabilidad es lo que buscamos. de igual manera esperamos tener resultados mejores...',
                'link_imagen' => 'imagenes/new_1568954132.png',
                'link' => NULL,
            ),
            4 => 
            array (
                'id' => 84,
                'titulo' => 'Importante',
                'contenido' => 'Estamos en un proceso de actualización y mejora, la app estaré inactiva un par de Horas, Gracias seguimos mejorando para ustedes.',
                'link_imagen' => 'imagenes/new_1569218066.png',
                'link' => NULL,
            ),
            5 => 
            array (
                'id' => 106,
                'titulo' => 'Update',
                'contenido' => 'Estamos a un paso de la nueva actualización de la App esperando que Apple termine de aprobarla, no se han enviado nuevos Profitcalls por ahora pero seguimos activos, ademas a todos se les regalará una semana más de su suscripción para que puedan disfrutar la nueva actualización que trae muchos beneficios, Análisis con imágenes y links en su ventana de Home junto con Criptocalls para invertir a largo plazo, Órdenes pendientes, nuevos pares y siempre buscando mejorar nuestras estadísticas. seguimos Evolucionando y mejorando.',
                'link_imagen' => 'imagenes/new_1572890505.png',
                'link' => NULL,
            ),
            6 => 
            array (
                'id' => 107,
                'titulo' => 'EURUSD',
            'contenido' => 'EURUSD panorama Diario en tendencia alcista llegando a un arrea de confluencia de 50% de Fibonacci, estaremos buscando movimientos alcistas teniendo como proyección 1) 1.1170, 2)1.1251, es decir buscaremos oportunidades de compra con las confirmaciones necesarias.   análisis Guía para complementar sus análisis y proyecciones',
            'link_imagen' => 'imagenes/new_1573487454.png',
            'link' => 'https://www.tradingview.com/x/2OkWdIuu/',
        ),
        7 => 
        array (
            'id' => 108,
            'titulo' => 'USDCAD',
            'contenido' => 'En este momento nos encontramos en un area de confluencia donde estaríamos buscando entradas en venta con un TP en 1.300, El precio actual se encuentra en 1.3233, la orden pendiente o sell limit en 1.3300 aún sigue siendo válida ya que el precio puede subir para si ahí comenzar el push bajista, en este momento faltaría una confirmación extra de la acción de precio para buscar entradas en corto en temporalidades de 1 Hora. ',
            'link_imagen' => '',
            'link' => '',
        ),
        8 => 
        array (
            'id' => 109,
            'titulo' => 'Update Iphone',
            'contenido' => 'para todos los usuarios IPhone o iOS usen este link para descargar la última versión de la app mientras Apple termina de subirla en la App Store y puedan desde ya disfrutar de las actualizaciones en su pestaña de home y Profitcalls https://testflight.apple.com/join/SL9Q42Ow',
            'link_imagen' => 'imagenes/new_1573781755.png',
            'link' => NULL,
        ),
        9 => 
        array (
            'id' => 110,
            'titulo' => 'BTCUSD',
            'contenido' => 'viendo la reacción en el cierre de la vela de H4 y viendo cómo hace tested en el EMA de 200 en tf menores veo buena oportunidad para un short hasta $8000, El SL lo dejo a criterio propio no olviden usar su calculadora de Pips para medir el riesgo, recomendación por cada $1000 en su cuenta usar 0.01',
            'link_imagen' => 'imagenes/new_1574032926.png',
            'link' => NULL,
        ),
        10 => 
        array (
            'id' => 111,
            'titulo' => 'BTCUSD',
            'contenido' => 'Pueden asegurar Ganancias con +365 pips.',
            'link_imagen' => 'imagenes/new_1574102294.png',
            'link' => NULL,
        ),
        11 => 
        array (
            'id' => 112,
            'titulo' => 'Gold',
            'contenido' => 'Oportunidad de entrada en Venta con un Sell Stop @ 1469.40 buscando un TP en 1459.00 y un SL de 1475.57',
            'link_imagen' => 'imagenes/new_1574140352.png',
            'link' => NULL,
        ),
        12 => 
        array (
            'id' => 113,
            'titulo' => 'USDJPY',
            'contenido' => 'oportunidad de entrada en venta en USDJPY al precio de 108.60 teniendo como SL @108.95 y TP @107.25. siguiendo la estructura de H4 buscamos anticipar un cambio de tendencia a bajista en el timeframe Diario. recuerden el manejo del Riesgo es vital no olviden usar su calculadora para tener el riesgo adecuado.',
            'link_imagen' => 'imagenes/new_1574289869.png',
            'link' => NULL,
        ),
        13 => 
        array (
            'id' => 114,
            'titulo' => 'USDCAD',
            'contenido' => 'Inicio de la última semana de Noviembre, teniendo una buena área para ventas, mostrando Prueba de una resistencia importante en el timeframe Diario, donde con una variación bajista podríamos buscar oportunidades de ventas en timeframes de 1 hora, buscando un movimiento de más de 200 pips de Take Profit en el precio de @1.3000 y SL en @1.3330  teniendo unos 40 pips de SL, recuerden es muy importante esperar la acción del precio con una envolvente en el Timeframe diario que indique control de vendedores antes de entrar en ventas. ',
            'link_imagen' => '',
            'link' => '',
        ),
        14 => 
        array (
            'id' => 115,
            'titulo' => 'Criptomonedas',
            'contenido' => 'En momentos De fuertes caídas del mercado de criptomonedas es cuando aparecen oportunidades únicas de duplicar y muchos más nuestras inversiones en corto plazo, más allá de las criptomonedas que muchos conocemos como XRP, TRX , BTC,ETH... se presentan oportunidades con criptomonedas como GAS donde puede obtener un retorno considerable en poco tiempo, GAS va directamente relacionada con NEO. En lo personal acabo de Comprar GAS buscando como primer Target $5 lo cual sería un retorno de x5. Pueden comprarla Directamente en Exchange como Binance.',
            'link_imagen' => 'imagenes/new_1574692186.png',
            'link' => 'https://coinmarketcap.com/currencies/gas/',
        ),
        15 => 
        array (
            'id' => 116,
            'titulo' => 'EURUSD BUY',
            'contenido' => 'oportunidad de entrada en compra al mercado en @1.1079 buscando una continuación de la tendencia alcista hasta un precio de TP de @1.1150 manteniendo un SL corto de 25 pips en @1.1064. riesgo recomendado 3% del balance de su cuenta, recuerden pueden calcular de forma rápida el lotaje usando su calculadora de pips.',
            'link_imagen' => 'imagenes/new_1575501411.png',
            'link' => NULL,
        ),
        16 => 
        array (
            'id' => 117,
            'titulo' => 'Nuevo espacio de Psicología',
            'contenido' => 'Cómo todos sabemos en la vida del trader rentable la Psicología juega un papel muy importante por lo cual sea a creado este espacio para ayudarlos a fortalecer y encontrar su camino es este aspecto tan fundamental en la vida del trader rentable.',
            'link_imagen' => 'imagenes/new_1575656963.png',
            'link' => NULL,
        ),
        17 => 
        array (
            'id' => 118,
            'titulo' => 'PsychologySpace 001',
            'contenido' => NULL,
            'link_imagen' => 'imagenes/new_1575668043.png',
            'link' => NULL,
        ),
        18 => 
        array (
            'id' => 121,
            'titulo' => 'GBPCAD',
            'contenido' => 'Oportunidad de entrada en corto buscando el retroceso de la tendencia de timeframe mayores Riesgo recomendado 3% maximo. Los detalles los encontrarán en su pestaña de Profitcalls.',
            'link_imagen' => 'imagenes/new_1575904951.png',
            'link' => NULL,
        ),
        19 => 
        array (
            'id' => 122,
            'titulo' => 'USDCAD',
            'contenido' => 'Oportunidad de ventas para el par USDCAD buscando continuación de la tendencia bajista teniendo niveles de TP en @1.3115 y en @1.3060 con SL en @1.3275 arriesgando 35 pips. Precio de entrada @1.3240 ',
            'link_imagen' => '',
            'link' => '',
        ),
    ));
        
        
    }
}