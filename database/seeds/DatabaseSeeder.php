<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(RoleSeeder::class);
//        $this->call(RoleUserSeeder::class);
        $this->call(ServicioSeeder::class);
        $this->call(CategoriasSeeder::class);
        $this->call(VideosTableSeeder::class);
        //$this->call(UsuarioServicioSeeder::class);
        $this->call(UsersServiciosTableSeeder::class);
        $this->call(FlipbookTableSeeder::class);
        $this->call(RoleUserTableSeeder::class);
        $this->call(CotizacionTableSeeder::class);
        $this->call(NoticiasTableSeeder::class);
        $this->call(TestimoniosTableSeeder::class);
        $this->call(AlarmasTableSeeder::class);
        $this->call(MensajesTableSeeder::class);
        $this->call(OperacionTableSeeder::class);
    }
}
