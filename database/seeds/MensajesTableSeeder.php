<?php

use Illuminate\Database\Seeder;

class MensajesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void_id
     */
    public function run()
    {
        

        \DB::table('mensajes')->delete();
        
        \DB::table('mensajes')->insert(array (
            0 => 
            array (
                'id_mensajes' => 103,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Saliendo a ganacia en el par:XAUUSD',
                'link_imagen' => NULL,
                'created_at' => '2019-08-21 17:29:00',
                'updated_at' => '2019-08-21 17:29:00',
            ),
            1 => 
            array (
                'id_mensajes' => 120,
                'titulo' => 'Profit4Life',
                'mensaje' => 'GBPNZD Se mueve stop loss a: 1.89985000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-22 03:57:00',
                'updated_at' => '2019-08-22 03:57:00',
            ),
            2 => 
            array (
                'id_mensajes' => 121,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Saliendo a ganacia en el par:GBPNZD',
                'link_imagen' => NULL,
                'created_at' => '2019-08-22 03:58:00',
                'updated_at' => '2019-08-22 03:58:00',
            ),
            3 => 
            array (
                'id_mensajes' => 123,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Saliendo por stop loss en el par:XAUUSD',
                'link_imagen' => NULL,
                'created_at' => '2019-08-22 10:03:00',
                'updated_at' => '2019-08-22 10:03:00',
            ),
            4 => 
            array (
                'id_mensajes' => 125,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Saliendo por stop loss en el par:ETHUSD',
                'link_imagen' => NULL,
                'created_at' => '2019-08-22 22:35:00',
                'updated_at' => '2019-08-22 22:35:00',
            ),
            5 => 
            array (
                'id_mensajes' => 126,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD',
                'link_imagen' => NULL,
                'created_at' => '2019-08-23 05:28:00',
                'updated_at' => '2019-08-23 05:28:00',
            ),
            6 => 
            array (
                'id_mensajes' => 127,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD',
                'link_imagen' => NULL,
                'created_at' => '2019-08-23 17:39:00',
                'updated_at' => '2019-08-23 17:39:00',
            ),
            7 => 
            array (
                'id_mensajes' => 128,
                'titulo' => 'Profit4Life',
                'mensaje' => 'GBPNZD Se mueve stop loss a: 1.92164000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-23 18:59:00',
                'updated_at' => '2019-08-23 18:59:00',
            ),
            8 => 
            array (
                'id_mensajes' => 129,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Saliendo a ganacia en el par:GBPNZD',
                'link_imagen' => NULL,
                'created_at' => '2019-08-23 19:00:00',
                'updated_at' => '2019-08-23 19:00:00',
            ),
            9 => 
            array (
                'id_mensajes' => 131,
                'titulo' => 'profit4life',
                'mensaje' => 'Bienvenid_mensajesos a Profit4Life',
                'link_imagen' => NULL,
                'created_at' => '2019-08-24 01:51:00',
                'updated_at' => '2019-08-24 01:51:00',
            ),
            10 => 
            array (
                'id_mensajes' => 132,
                'titulo' => 'ETHUSD',
                'mensaje' => 'En espera de recuperacion par ETHUSD',
                'link_imagen' => NULL,
                'created_at' => '2019-08-24 05:11:00',
                'updated_at' => '2019-08-24 05:11:00',
            ),
            11 => 
            array (
                'id_mensajes' => 135,
                'titulo' => 'Profit4Life',
                'mensaje' => 'El futuro ya esta aqui',
                'link_imagen' => NULL,
                'created_at' => '2019-08-24 05:16:00',
                'updated_at' => '2019-08-24 05:16:00',
            ),
            12 => 
            array (
                'id_mensajes' => 137,
                'titulo' => 'Profit4Life',
                'mensaje' => 'ETHUSD Se mueve stop loss a: 190.81000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-24 07:29:00',
                'updated_at' => '2019-08-24 07:29:00',
            ),
            13 => 
            array (
                'id_mensajes' => 138,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Saliendo por stop loss en el par:ETHUSD',
                'link_imagen' => NULL,
                'created_at' => '2019-08-24 08:23:00',
                'updated_at' => '2019-08-24 08:23:00',
            ),
            14 => 
            array (
                'id_mensajes' => 139,
                'titulo' => 'XRPUSD',
                'mensaje' => 'Nuestra profit call de XRPUSD ya tiene 20 pips de profit',
                'link_imagen' => NULL,
                'created_at' => '2019-08-24 19:11:00',
                'updated_at' => '2019-08-24 19:11:00',
            ),
            15 => 
            array (
                'id_mensajes' => 140,
                'titulo' => 'XRPUSD',
                'mensaje' => 'Seguimos Adelante!!! Si lo imaginas es posible',
                'link_imagen' => NULL,
                'created_at' => '2019-08-24 19:13:00',
                'updated_at' => '2019-08-24 19:13:00',
            ),
            16 => 
            array (
                'id_mensajes' => 141,
                'titulo' => 'XRPUSD',
                'mensaje' => 'Ripple es la crypto con la velocid_mensajesad mas alta de transaccion',
                'link_imagen' => NULL,
                'created_at' => '2019-08-24 19:18:00',
                'updated_at' => '2019-08-24 19:18:00',
            ),
            17 => 
            array (
                'id_mensajes' => 142,
                'titulo' => 'Noticias',
                'mensaje' => 'Esperamos por google play para tener de vuelta la app para android_mensajes y ios simultaneamente',
                'link_imagen' => NULL,
                'created_at' => '2019-08-24 20:04:00',
                'updated_at' => '2019-08-24 20:04:00',
            ),
            18 => 
            array (
                'id_mensajes' => 143,
                'titulo' => 'Profit4life',
                'mensaje' => 'La paciencia rinde frutos y es la virtud y escencia del trader',
                'link_imagen' => NULL,
                'created_at' => '2019-08-24 20:05:00',
                'updated_at' => '2019-08-24 20:05:00',
            ),
            19 => 
            array (
                'id_mensajes' => 144,
                'titulo' => '123',
                'mensaje' => 'probando',
                'link_imagen' => NULL,
                'created_at' => '2019-08-24 21:09:00',
                'updated_at' => '2019-08-24 21:09:00',
            ),
            20 => 
            array (
                'id_mensajes' => 145,
                'titulo' => 'XrpUsd',
                'mensaje' => 'mover SL a punto de entrada, Muy poco Volumen',
                'link_imagen' => NULL,
                'created_at' => '2019-08-24 22:06:00',
                'updated_at' => '2019-08-24 22:06:00',
            ),
            21 => 
            array (
                'id_mensajes' => 146,
                'titulo' => 'XrpUsd',
                'mensaje' => 'recuperando pips en el push Alcista.',
                'link_imagen' => NULL,
                'created_at' => '2019-08-24 23:01:00',
                'updated_at' => '2019-08-24 23:01:00',
            ),
            22 => 
            array (
                'id_mensajes' => 147,
                'titulo' => 'atencion',
                'mensaje' => 'La App estar&aacute; al 100% el lunes lo que la tienen tendr&aacute;n que actualizarla el lunes',
                'link_imagen' => NULL,
                'created_at' => '2019-08-25 00:23:00',
                'updated_at' => '2019-08-25 00:23:00',
            ),
            23 => 
            array (
                'id_mensajes' => 148,
                'titulo' => 'Profit4Life',
                'mensaje' => 'El futuro esta aqui',
                'link_imagen' => NULL,
                'created_at' => '2019-08-25 02:35:00',
                'updated_at' => '2019-08-25 02:35:00',
            ),
            24 => 
            array (
                'id_mensajes' => 149,
                'titulo' => 'XRPUSD',
                'mensaje' => 'Operaci&oacute;n XRPUSD con 20 pips positivos',
                'link_imagen' => NULL,
                'created_at' => '2019-08-25 07:27:00',
                'updated_at' => '2019-08-25 07:27:00',
            ),
            25 => 
            array (
                'id_mensajes' => 151,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XRPUSD Se mueve stop loss a: 0.27301000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-25 07:31:00',
                'updated_at' => '2019-08-25 07:31:00',
            ),
            26 => 
            array (
                'id_mensajes' => 152,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XRPUSD Se mueve stop loss a: 0.27417000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-25 07:32:00',
                'updated_at' => '2019-08-25 07:32:00',
            ),
            27 => 
            array (
                'id_mensajes' => 155,
                'titulo' => 'XRPUSD',
                'mensaje' => 'Operación XRPUSD llegando a los 60 pips',
                'link_imagen' => NULL,
                'created_at' => '2019-08-25 07:48:00',
                'updated_at' => '2019-08-25 07:48:00',
            ),
            28 => 
            array (
                'id_mensajes' => 156,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Saliendo a ganacia en el par:XRPUSD',
                'link_imagen' => NULL,
                'created_at' => '2019-08-25 08:04:00',
                'updated_at' => '2019-08-25 08:04:00',
            ),
            29 => 
            array (
                'id_mensajes' => 157,
                'titulo' => 'xrpusd',
                'mensaje' => 'cerrar Posición con Ganancias',
                'link_imagen' => NULL,
                'created_at' => '2019-08-25 08:04:00',
                'updated_at' => '2019-08-25 08:04:00',
            ),
            30 => 
            array (
                'id_mensajes' => 158,
                'titulo' => 'XRPUSD',
                'mensaje' => 'Operación con 42.8 PIPS ganancia!!',
                'link_imagen' => NULL,
                'created_at' => '2019-08-25 08:05:00',
                'updated_at' => '2019-08-25 08:05:00',
            ),
            31 => 
            array (
                'id_mensajes' => 159,
                'titulo' => '123',
                'mensaje' => 'probando en Android_mensajes',
                'link_imagen' => NULL,
                'created_at' => '2019-08-25 16:50:00',
                'updated_at' => '2019-08-25 16:50:00',
            ),
            32 => 
            array (
                'id_mensajes' => 162,
                'titulo' => 'prueba',
                'mensaje' => '% de riesgo mínimo, PIPS   = $$$$$',
                'link_imagen' => NULL,
                'created_at' => '2019-08-25 16:55:00',
                'updated_at' => '2019-08-25 16:55:00',
            ),
            33 => 
            array (
                'id_mensajes' => 163,
                'titulo' => 'Profit4Life',
                'mensaje' => 'El futuro esta aquí',
                'link_imagen' => NULL,
                'created_at' => '2019-08-25 16:56:00',
                'updated_at' => '2019-08-25 16:56:00',
            ),
            34 => 
            array (
                'id_mensajes' => 164,
                'titulo' => 'prueba',
                'mensaje' => 'gracias a todos por su paciencia..... falta muy poco para tener la versión final en Android_mensajes y apple',
                'link_imagen' => NULL,
                'created_at' => '2019-08-25 17:07:00',
                'updated_at' => '2019-08-25 17:07:00',
            ),
            35 => 
            array (
                'id_mensajes' => 165,
                'titulo' => '1234',
                'mensaje' => 'ya falta poco',
                'link_imagen' => NULL,
                'created_at' => '2019-08-25 18:15:00',
                'updated_at' => '2019-08-25 18:15:00',
            ),
            36 => 
            array (
                'id_mensajes' => 166,
                'titulo' => '1234',
                'mensaje' => 'ya falta poco',
                'link_imagen' => NULL,
                'created_at' => '2019-08-25 18:20:00',
                'updated_at' => '2019-08-25 18:20:00',
            ),
            37 => 
            array (
                'id_mensajes' => 167,
                'titulo' => 'Profit4Life',
                'mensaje' => 'ETHUSD Se mueve stop loss a: 184.73000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-25 19:25:00',
                'updated_at' => '2019-08-25 19:25:00',
            ),
            38 => 
            array (
                'id_mensajes' => 168,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Saliendo a ganacia en el par:ETHUSD',
                'link_imagen' => NULL,
                'created_at' => '2019-08-25 19:25:00',
                'updated_at' => '2019-08-25 19:25:00',
            ),
            39 => 
            array (
                'id_mensajes' => 169,
                'titulo' => 'ETHUSD',
                'mensaje' => 'Otra profit call efectiva ganancia de 24.3 pips!!',
                'link_imagen' => NULL,
                'created_at' => '2019-08-25 19:27:00',
                'updated_at' => '2019-08-25 19:27:00',
            ),
            40 => 
            array (
                'id_mensajes' => 175,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Saliendo por stop loss en el par:GBPAUD',
                'link_imagen' => NULL,
                'created_at' => '2019-08-26 06:56:00',
                'updated_at' => '2019-08-26 06:56:00',
            ),
            41 => 
            array (
                'id_mensajes' => 182,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Si lo Imaginas se puede',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 05:34:00',
                'updated_at' => '2019-08-27 05:34:00',
            ),
            42 => 
            array (
                'id_mensajes' => 183,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Bienvenid_mensajesos al next level',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 05:39:00',
                'updated_at' => '2019-08-27 05:39:00',
            ),
            43 => 
            array (
                'id_mensajes' => 185,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Bienvenid_mensajesos al next level',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 05:47:00',
                'updated_at' => '2019-08-27 05:47:00',
            ),
            44 => 
            array (
                'id_mensajes' => 186,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Invierte en tu futuro',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 05:59:00',
                'updated_at' => '2019-08-27 05:59:00',
            ),
            45 => 
            array (
                'id_mensajes' => 187,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Profit4Life en esta academia tenemos todo!!',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 05:59:00',
                'updated_at' => '2019-08-27 05:59:00',
            ),
            46 => 
            array (
                'id_mensajes' => 188,
                'titulo' => 'XAAUSD',
                'mensaje' => 'Estamos viendo una alza en el oro cheers!!',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 06:12:00',
                'updated_at' => '2019-08-27 06:12:00',
            ),
            47 => 
            array (
                'id_mensajes' => 189,
                'titulo' => 'GBPNZD',
                'mensaje' => 'pueden asegurar Mitad de la posición y mover SL a punto de entrada!!',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 06:51:00',
                'updated_at' => '2019-08-27 06:51:00',
            ),
            48 => 
            array (
                'id_mensajes' => 190,
                'titulo' => 'Profit4Life',
                'mensaje' => 'GBPNZD Se mueve stop loss a: 1.92087000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 06:53:00',
                'updated_at' => '2019-08-27 06:53:00',
            ),
            49 => 
            array (
                'id_mensajes' => 192,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Saliendo a ganacia en el par:GBPNZD',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 06:57:00',
                'updated_at' => '2019-08-27 06:57:00',
            ),
            50 => 
            array (
                'id_mensajes' => 193,
                'titulo' => 'US30',
                'mensaje' => NULL,
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:00:00',
                'updated_at' => '2019-08-27 15:00:00',
            ),
            51 => 
            array (
                'id_mensajes' => 194,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1538.68000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:11:00',
                'updated_at' => '2019-08-27 15:11:00',
            ),
            52 => 
            array (
                'id_mensajes' => 195,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.74000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:19:00',
                'updated_at' => '2019-08-27 15:19:00',
            ),
            53 => 
            array (
                'id_mensajes' => 196,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.77000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:19:00',
                'updated_at' => '2019-08-27 15:19:00',
            ),
            54 => 
            array (
                'id_mensajes' => 197,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.79000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:19:00',
                'updated_at' => '2019-08-27 15:19:00',
            ),
            55 => 
            array (
                'id_mensajes' => 198,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.65000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:19:00',
                'updated_at' => '2019-08-27 15:19:00',
            ),
            56 => 
            array (
                'id_mensajes' => 199,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.62000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:19:00',
                'updated_at' => '2019-08-27 15:19:00',
            ),
            57 => 
            array (
                'id_mensajes' => 200,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.53000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:19:00',
                'updated_at' => '2019-08-27 15:19:00',
            ),
            58 => 
            array (
                'id_mensajes' => 201,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.76000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:19:00',
                'updated_at' => '2019-08-27 15:19:00',
            ),
            59 => 
            array (
                'id_mensajes' => 202,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.79000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:19:00',
                'updated_at' => '2019-08-27 15:19:00',
            ),
            60 => 
            array (
                'id_mensajes' => 203,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.79000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:19:00',
                'updated_at' => '2019-08-27 15:19:00',
            ),
            61 => 
            array (
                'id_mensajes' => 204,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.54000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:20:00',
                'updated_at' => '2019-08-27 15:20:00',
            ),
            62 => 
            array (
                'id_mensajes' => 205,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.68000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:20:00',
                'updated_at' => '2019-08-27 15:20:00',
            ),
            63 => 
            array (
                'id_mensajes' => 206,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.94000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:20:00',
                'updated_at' => '2019-08-27 15:20:00',
            ),
            64 => 
            array (
                'id_mensajes' => 207,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.96000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:20:00',
                'updated_at' => '2019-08-27 15:20:00',
            ),
            65 => 
            array (
                'id_mensajes' => 208,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.81000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:20:00',
                'updated_at' => '2019-08-27 15:20:00',
            ),
            66 => 
            array (
                'id_mensajes' => 209,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.74000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:20:00',
                'updated_at' => '2019-08-27 15:20:00',
            ),
            67 => 
            array (
                'id_mensajes' => 210,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.65000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:20:00',
                'updated_at' => '2019-08-27 15:20:00',
            ),
            68 => 
            array (
                'id_mensajes' => 211,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.65000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:20:00',
                'updated_at' => '2019-08-27 15:20:00',
            ),
            69 => 
            array (
                'id_mensajes' => 212,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.46000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:20:00',
                'updated_at' => '2019-08-27 15:20:00',
            ),
            70 => 
            array (
                'id_mensajes' => 213,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.57000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:20:00',
                'updated_at' => '2019-08-27 15:20:00',
            ),
            71 => 
            array (
                'id_mensajes' => 214,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.45000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:20:00',
                'updated_at' => '2019-08-27 15:20:00',
            ),
            72 => 
            array (
                'id_mensajes' => 215,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.63000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:20:00',
                'updated_at' => '2019-08-27 15:20:00',
            ),
            73 => 
            array (
                'id_mensajes' => 216,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.43000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:20:00',
                'updated_at' => '2019-08-27 15:20:00',
            ),
            74 => 
            array (
                'id_mensajes' => 217,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.45000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:20:00',
                'updated_at' => '2019-08-27 15:20:00',
            ),
            75 => 
            array (
                'id_mensajes' => 218,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.60000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:20:00',
                'updated_at' => '2019-08-27 15:20:00',
            ),
            76 => 
            array (
                'id_mensajes' => 219,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.62000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:20:00',
                'updated_at' => '2019-08-27 15:20:00',
            ),
            77 => 
            array (
                'id_mensajes' => 220,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.72000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:20:00',
                'updated_at' => '2019-08-27 15:20:00',
            ),
            78 => 
            array (
                'id_mensajes' => 221,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.64000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:20:00',
                'updated_at' => '2019-08-27 15:20:00',
            ),
            79 => 
            array (
                'id_mensajes' => 222,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.59000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:20:00',
                'updated_at' => '2019-08-27 15:20:00',
            ),
            80 => 
            array (
                'id_mensajes' => 223,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.62000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:21:00',
                'updated_at' => '2019-08-27 15:21:00',
            ),
            81 => 
            array (
                'id_mensajes' => 224,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.63000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:21:00',
                'updated_at' => '2019-08-27 15:21:00',
            ),
            82 => 
            array (
                'id_mensajes' => 225,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.76000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:21:00',
                'updated_at' => '2019-08-27 15:21:00',
            ),
            83 => 
            array (
                'id_mensajes' => 226,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.81000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:21:00',
                'updated_at' => '2019-08-27 15:21:00',
            ),
            84 => 
            array (
                'id_mensajes' => 227,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.83000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:21:00',
                'updated_at' => '2019-08-27 15:21:00',
            ),
            85 => 
            array (
                'id_mensajes' => 228,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.72000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:21:00',
                'updated_at' => '2019-08-27 15:21:00',
            ),
            86 => 
            array (
                'id_mensajes' => 229,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.74000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:21:00',
                'updated_at' => '2019-08-27 15:21:00',
            ),
            87 => 
            array (
                'id_mensajes' => 230,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.42000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:21:00',
                'updated_at' => '2019-08-27 15:21:00',
            ),
            88 => 
            array (
                'id_mensajes' => 231,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.63000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:21:00',
                'updated_at' => '2019-08-27 15:21:00',
            ),
            89 => 
            array (
                'id_mensajes' => 232,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.54000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:21:00',
                'updated_at' => '2019-08-27 15:21:00',
            ),
            90 => 
            array (
                'id_mensajes' => 233,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.55000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:21:00',
                'updated_at' => '2019-08-27 15:21:00',
            ),
            91 => 
            array (
                'id_mensajes' => 234,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.56000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:21:00',
                'updated_at' => '2019-08-27 15:21:00',
            ),
            92 => 
            array (
                'id_mensajes' => 235,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.52000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:21:00',
                'updated_at' => '2019-08-27 15:21:00',
            ),
            93 => 
            array (
                'id_mensajes' => 236,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.44000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:21:00',
                'updated_at' => '2019-08-27 15:21:00',
            ),
            94 => 
            array (
                'id_mensajes' => 237,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.40000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:21:00',
                'updated_at' => '2019-08-27 15:21:00',
            ),
            95 => 
            array (
                'id_mensajes' => 238,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.44000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:21:00',
                'updated_at' => '2019-08-27 15:21:00',
            ),
            96 => 
            array (
                'id_mensajes' => 239,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.50000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:21:00',
                'updated_at' => '2019-08-27 15:21:00',
            ),
            97 => 
            array (
                'id_mensajes' => 240,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.52000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:21:00',
                'updated_at' => '2019-08-27 15:21:00',
            ),
            98 => 
            array (
                'id_mensajes' => 241,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.42000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:21:00',
                'updated_at' => '2019-08-27 15:21:00',
            ),
            99 => 
            array (
                'id_mensajes' => 242,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.39000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:21:00',
                'updated_at' => '2019-08-27 15:21:00',
            ),
            100 => 
            array (
                'id_mensajes' => 243,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.23000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:21:00',
                'updated_at' => '2019-08-27 15:21:00',
            ),
            101 => 
            array (
                'id_mensajes' => 244,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.25000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:21:00',
                'updated_at' => '2019-08-27 15:21:00',
            ),
            102 => 
            array (
                'id_mensajes' => 245,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.30000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:21:00',
                'updated_at' => '2019-08-27 15:21:00',
            ),
            103 => 
            array (
                'id_mensajes' => 246,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.32000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:21:00',
                'updated_at' => '2019-08-27 15:21:00',
            ),
            104 => 
            array (
                'id_mensajes' => 247,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.09000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:21:00',
                'updated_at' => '2019-08-27 15:21:00',
            ),
            105 => 
            array (
                'id_mensajes' => 248,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.01000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:22:00',
                'updated_at' => '2019-08-27 15:22:00',
            ),
            106 => 
            array (
                'id_mensajes' => 249,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.18000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:22:00',
                'updated_at' => '2019-08-27 15:22:00',
            ),
            107 => 
            array (
                'id_mensajes' => 250,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.10000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:22:00',
                'updated_at' => '2019-08-27 15:22:00',
            ),
            108 => 
            array (
                'id_mensajes' => 251,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.04000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:22:00',
                'updated_at' => '2019-08-27 15:22:00',
            ),
            109 => 
            array (
                'id_mensajes' => 252,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.02000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:22:00',
                'updated_at' => '2019-08-27 15:22:00',
            ),
            110 => 
            array (
                'id_mensajes' => 253,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.20000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:22:00',
                'updated_at' => '2019-08-27 15:22:00',
            ),
            111 => 
            array (
                'id_mensajes' => 254,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.26000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:22:00',
                'updated_at' => '2019-08-27 15:22:00',
            ),
            112 => 
            array (
                'id_mensajes' => 255,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.27000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:22:00',
                'updated_at' => '2019-08-27 15:22:00',
            ),
            113 => 
            array (
                'id_mensajes' => 256,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.03000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:22:00',
                'updated_at' => '2019-08-27 15:22:00',
            ),
            114 => 
            array (
                'id_mensajes' => 257,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.06000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:22:00',
                'updated_at' => '2019-08-27 15:22:00',
            ),
            115 => 
            array (
                'id_mensajes' => 258,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.11000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:22:00',
                'updated_at' => '2019-08-27 15:22:00',
            ),
            116 => 
            array (
                'id_mensajes' => 259,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.28000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:22:00',
                'updated_at' => '2019-08-27 15:22:00',
            ),
            117 => 
            array (
                'id_mensajes' => 260,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.48000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:22:00',
                'updated_at' => '2019-08-27 15:22:00',
            ),
            118 => 
            array (
                'id_mensajes' => 261,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.52000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:22:00',
                'updated_at' => '2019-08-27 15:22:00',
            ),
            119 => 
            array (
                'id_mensajes' => 262,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.44000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:22:00',
                'updated_at' => '2019-08-27 15:22:00',
            ),
            120 => 
            array (
                'id_mensajes' => 263,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.33000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:22:00',
                'updated_at' => '2019-08-27 15:22:00',
            ),
            121 => 
            array (
                'id_mensajes' => 264,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.23000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:22:00',
                'updated_at' => '2019-08-27 15:22:00',
            ),
            122 => 
            array (
                'id_mensajes' => 265,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.12000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:22:00',
                'updated_at' => '2019-08-27 15:22:00',
            ),
            123 => 
            array (
                'id_mensajes' => 266,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.07000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:22:00',
                'updated_at' => '2019-08-27 15:22:00',
            ),
            124 => 
            array (
                'id_mensajes' => 267,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.04000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:22:00',
                'updated_at' => '2019-08-27 15:22:00',
            ),
            125 => 
            array (
                'id_mensajes' => 268,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.02000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:23:00',
                'updated_at' => '2019-08-27 15:23:00',
            ),
            126 => 
            array (
                'id_mensajes' => 269,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.16000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:23:00',
                'updated_at' => '2019-08-27 15:23:00',
            ),
            127 => 
            array (
                'id_mensajes' => 270,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.16000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:23:00',
                'updated_at' => '2019-08-27 15:23:00',
            ),
            128 => 
            array (
                'id_mensajes' => 271,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.11000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:23:00',
                'updated_at' => '2019-08-27 15:23:00',
            ),
            129 => 
            array (
                'id_mensajes' => 272,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.15000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:23:00',
                'updated_at' => '2019-08-27 15:23:00',
            ),
            130 => 
            array (
                'id_mensajes' => 273,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.08000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:23:00',
                'updated_at' => '2019-08-27 15:23:00',
            ),
            131 => 
            array (
                'id_mensajes' => 274,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.17000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:23:00',
                'updated_at' => '2019-08-27 15:23:00',
            ),
            132 => 
            array (
                'id_mensajes' => 275,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.11000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:23:00',
                'updated_at' => '2019-08-27 15:23:00',
            ),
            133 => 
            array (
                'id_mensajes' => 276,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.06000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:23:00',
                'updated_at' => '2019-08-27 15:23:00',
            ),
            134 => 
            array (
                'id_mensajes' => 277,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1538.88000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:23:00',
                'updated_at' => '2019-08-27 15:23:00',
            ),
            135 => 
            array (
                'id_mensajes' => 278,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1538.83000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:23:00',
                'updated_at' => '2019-08-27 15:23:00',
            ),
            136 => 
            array (
                'id_mensajes' => 279,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1538.73000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:23:00',
                'updated_at' => '2019-08-27 15:23:00',
            ),
            137 => 
            array (
                'id_mensajes' => 280,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1538.74000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:23:00',
                'updated_at' => '2019-08-27 15:23:00',
            ),
            138 => 
            array (
                'id_mensajes' => 281,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1538.71000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:23:00',
                'updated_at' => '2019-08-27 15:23:00',
            ),
            139 => 
            array (
                'id_mensajes' => 282,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1538.72000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:23:00',
                'updated_at' => '2019-08-27 15:23:00',
            ),
            140 => 
            array (
                'id_mensajes' => 283,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1538.82000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:23:00',
                'updated_at' => '2019-08-27 15:23:00',
            ),
            141 => 
            array (
                'id_mensajes' => 284,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1538.83000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:23:00',
                'updated_at' => '2019-08-27 15:23:00',
            ),
            142 => 
            array (
                'id_mensajes' => 285,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1538.94000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:24:00',
                'updated_at' => '2019-08-27 15:24:00',
            ),
            143 => 
            array (
                'id_mensajes' => 286,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.10000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:24:00',
                'updated_at' => '2019-08-27 15:24:00',
            ),
            144 => 
            array (
                'id_mensajes' => 287,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.36000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:24:00',
                'updated_at' => '2019-08-27 15:24:00',
            ),
            145 => 
            array (
                'id_mensajes' => 288,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.44000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:24:00',
                'updated_at' => '2019-08-27 15:24:00',
            ),
            146 => 
            array (
                'id_mensajes' => 289,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.37000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:24:00',
                'updated_at' => '2019-08-27 15:24:00',
            ),
            147 => 
            array (
                'id_mensajes' => 290,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.45000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:24:00',
                'updated_at' => '2019-08-27 15:24:00',
            ),
            148 => 
            array (
                'id_mensajes' => 291,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.34000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:24:00',
                'updated_at' => '2019-08-27 15:24:00',
            ),
            149 => 
            array (
                'id_mensajes' => 292,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.36000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:24:00',
                'updated_at' => '2019-08-27 15:24:00',
            ),
            150 => 
            array (
                'id_mensajes' => 293,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.36000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:24:00',
                'updated_at' => '2019-08-27 15:24:00',
            ),
            151 => 
            array (
                'id_mensajes' => 294,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.37000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:24:00',
                'updated_at' => '2019-08-27 15:24:00',
            ),
            152 => 
            array (
                'id_mensajes' => 295,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.35000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:24:00',
                'updated_at' => '2019-08-27 15:24:00',
            ),
            153 => 
            array (
                'id_mensajes' => 296,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.48000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:24:00',
                'updated_at' => '2019-08-27 15:24:00',
            ),
            154 => 
            array (
                'id_mensajes' => 297,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.45000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:24:00',
                'updated_at' => '2019-08-27 15:24:00',
            ),
            155 => 
            array (
                'id_mensajes' => 298,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.37000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:25:00',
                'updated_at' => '2019-08-27 15:25:00',
            ),
            156 => 
            array (
                'id_mensajes' => 299,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.41000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:25:00',
                'updated_at' => '2019-08-27 15:25:00',
            ),
            157 => 
            array (
                'id_mensajes' => 300,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.34000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:25:00',
                'updated_at' => '2019-08-27 15:25:00',
            ),
            158 => 
            array (
                'id_mensajes' => 301,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.53000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:25:00',
                'updated_at' => '2019-08-27 15:25:00',
            ),
            159 => 
            array (
                'id_mensajes' => 302,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.46000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:25:00',
                'updated_at' => '2019-08-27 15:25:00',
            ),
            160 => 
            array (
                'id_mensajes' => 303,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.06000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:25:00',
                'updated_at' => '2019-08-27 15:25:00',
            ),
            161 => 
            array (
                'id_mensajes' => 304,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.43000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:25:00',
                'updated_at' => '2019-08-27 15:25:00',
            ),
            162 => 
            array (
                'id_mensajes' => 305,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.55000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:25:00',
                'updated_at' => '2019-08-27 15:25:00',
            ),
            163 => 
            array (
                'id_mensajes' => 306,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.61000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:25:00',
                'updated_at' => '2019-08-27 15:25:00',
            ),
            164 => 
            array (
                'id_mensajes' => 307,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.69000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:25:00',
                'updated_at' => '2019-08-27 15:25:00',
            ),
            165 => 
            array (
                'id_mensajes' => 308,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.60000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:25:00',
                'updated_at' => '2019-08-27 15:25:00',
            ),
            166 => 
            array (
                'id_mensajes' => 309,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.51000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:25:00',
                'updated_at' => '2019-08-27 15:25:00',
            ),
            167 => 
            array (
                'id_mensajes' => 310,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.44000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:25:00',
                'updated_at' => '2019-08-27 15:25:00',
            ),
            168 => 
            array (
                'id_mensajes' => 311,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.57000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:25:00',
                'updated_at' => '2019-08-27 15:25:00',
            ),
            169 => 
            array (
                'id_mensajes' => 312,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.53000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:26:00',
                'updated_at' => '2019-08-27 15:26:00',
            ),
            170 => 
            array (
                'id_mensajes' => 313,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.54000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:26:00',
                'updated_at' => '2019-08-27 15:26:00',
            ),
            171 => 
            array (
                'id_mensajes' => 314,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.65000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:26:00',
                'updated_at' => '2019-08-27 15:26:00',
            ),
            172 => 
            array (
                'id_mensajes' => 315,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.64000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:26:00',
                'updated_at' => '2019-08-27 15:26:00',
            ),
            173 => 
            array (
                'id_mensajes' => 316,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.66000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:26:00',
                'updated_at' => '2019-08-27 15:26:00',
            ),
            174 => 
            array (
                'id_mensajes' => 317,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.64000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:26:00',
                'updated_at' => '2019-08-27 15:26:00',
            ),
            175 => 
            array (
                'id_mensajes' => 318,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.62000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:26:00',
                'updated_at' => '2019-08-27 15:26:00',
            ),
            176 => 
            array (
                'id_mensajes' => 319,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.60000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:26:00',
                'updated_at' => '2019-08-27 15:26:00',
            ),
            177 => 
            array (
                'id_mensajes' => 325,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.59000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:27:00',
                'updated_at' => '2019-08-27 15:27:00',
            ),
            178 => 
            array (
                'id_mensajes' => 328,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1539.95000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:27:00',
                'updated_at' => '2019-08-27 15:27:00',
            ),
            179 => 
            array (
                'id_mensajes' => 329,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Saliendo a ganacia en el par:XAUUSD',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:27:00',
                'updated_at' => '2019-08-27 15:27:00',
            ),
            180 => 
            array (
                'id_mensajes' => 330,
                'titulo' => 'Profit4Life',
                'mensaje' => 'El futuro esta aquí',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:33:00',
                'updated_at' => '2019-08-27 15:33:00',
            ),
            181 => 
            array (
                'id_mensajes' => 331,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Si lo Imaginas se puede',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 15:33:00',
                'updated_at' => '2019-08-27 15:33:00',
            ),
            182 => 
            array (
                'id_mensajes' => 332,
                'titulo' => 'US30',
                'mensaje' => NULL,
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 16:00:00',
                'updated_at' => '2019-08-27 16:00:00',
            ),
            183 => 
            array (
                'id_mensajes' => 333,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Pips del Take Profit:2310.00000000 Pips del Stop Loss:2310.00000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 16:00:00',
                'updated_at' => '2019-08-27 16:00:00',
            ),
            184 => 
            array (
                'id_mensajes' => 334,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Saliendo a ganacia en el par:US30',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 16:03:00',
                'updated_at' => '2019-08-27 16:03:00',
            ),
            185 => 
            array (
                'id_mensajes' => 335,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Saliendo a ganacia en el par:US30',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 16:05:00',
                'updated_at' => '2019-08-27 16:05:00',
            ),
            186 => 
            array (
                'id_mensajes' => 336,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Conocimiento es poder, hoy llevamos 980pips de ganancias!!',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 19:11:00',
                'updated_at' => '2019-08-27 19:11:00',
            ),
            187 => 
            array (
                'id_mensajes' => 337,
                'titulo' => 'Profit4life',
                'mensaje' => 'Hoy ha sid_mensajeso un gran día 4/4!!',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 20:02:00',
                'updated_at' => '2019-08-27 20:02:00',
            ),
            188 => 
            array (
                'id_mensajes' => 338,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Bienvenid_mensajesos al next level',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 22:37:00',
                'updated_at' => '2019-08-27 22:37:00',
            ),
            189 => 
            array (
                'id_mensajes' => 339,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Paciencia es la virtud del trader!!',
                'link_imagen' => NULL,
                'created_at' => '2019-08-27 22:44:00',
                'updated_at' => '2019-08-27 22:44:00',
            ),
            190 => 
            array (
                'id_mensajes' => 340,
                'titulo' => 'Recordatorio',
                'mensaje' => 'Recuerda el manejo de riesgo es fundamental',
                'link_imagen' => NULL,
                'created_at' => '2019-08-28 12:22:00',
                'updated_at' => '2019-08-28 12:22:00',
            ),
            191 => 
            array (
                'id_mensajes' => 341,
                'titulo' => 'Recordatorio',
                'mensaje' => 'El manejo de riesgo es fundamental',
                'link_imagen' => NULL,
                'created_at' => '2019-08-28 12:23:00',
                'updated_at' => '2019-08-28 12:23:00',
            ),
            192 => 
            array (
                'id_mensajes' => 342,
                'titulo' => 'Nuevo dia de Oportunid_mensajesades',
                'mensaje' => 'Comenzamos el dia en positivo',
                'link_imagen' => NULL,
                'created_at' => '2019-08-28 12:31:00',
                'updated_at' => '2019-08-28 12:31:00',
            ),
            193 => 
            array (
                'id_mensajes' => 343,
                'titulo' => 'Oportunid_mensajesades',
                'mensaje' => 'Comenzamos el dia en positivo',
                'link_imagen' => NULL,
                'created_at' => '2019-08-28 12:40:00',
                'updated_at' => '2019-08-28 12:40:00',
            ),
            194 => 
            array (
                'id_mensajes' => 344,
                'titulo' => 'Oportunid_mensajesades',
                'mensaje' => 'Comenzamos el dia en positivo',
                'link_imagen' => NULL,
                'created_at' => '2019-08-28 12:42:00',
                'updated_at' => '2019-08-28 12:42:00',
            ),
            195 => 
            array (
                'id_mensajes' => 345,
                'titulo' => 'Oportunid_mensajesades',
                'mensaje' => 'Comenzamos el dia en positivo',
                'link_imagen' => NULL,
                'created_at' => '2019-08-28 14:11:00',
                'updated_at' => '2019-08-28 14:11:00',
            ),
            196 => 
            array (
                'id_mensajes' => 346,
                'titulo' => 'Oportunid_mensajesades',
                'mensaje' => 'Comenzamos el dia en positivo',
                'link_imagen' => NULL,
                'created_at' => '2019-08-28 14:19:00',
                'updated_at' => '2019-08-28 14:19:00',
            ),
            197 => 
            array (
                'id_mensajes' => 347,
                'titulo' => 'Oportunid_mensajesades',
                'mensaje' => 'Comenzamos el dia en positivo',
                'link_imagen' => NULL,
                'created_at' => '2019-08-28 14:23:00',
                'updated_at' => '2019-08-28 14:23:00',
            ),
            198 => 
            array (
                'id_mensajes' => 348,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Pips del Take Profit:169.40000000 Pips del Stop Loss:169.40000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-28 16:02:00',
                'updated_at' => '2019-08-28 16:02:00',
            ),
            199 => 
            array (
                'id_mensajes' => 349,
                'titulo' => 'GBPNZD',
                'mensaje' => NULL,
                'link_imagen' => NULL,
                'created_at' => '2019-08-28 16:04:00',
                'updated_at' => '2019-08-28 16:04:00',
            ),
            200 => 
            array (
                'id_mensajes' => 351,
                'titulo' => 'GBPNZD',
                'mensaje' => 'Long',
                'link_imagen' => NULL,
                'created_at' => '2019-08-28 16:08:00',
                'updated_at' => '2019-08-28 16:08:00',
            ),
            201 => 
            array (
                'id_mensajes' => 354,
                'titulo' => 'GBPNZD',
                'mensaje' => 'Long TP:1.94988 SL:1.91600',
                'link_imagen' => NULL,
                'created_at' => '2019-08-28 16:18:00',
                'updated_at' => '2019-08-28 16:18:00',
            ),
            202 => 
            array (
                'id_mensajes' => 355,
                'titulo' => 'Rentabilid_mensajesad',
                'mensaje' => 'La rentabilid_mensajesad y el manejo del Riesgo van de la mano',
                'link_imagen' => NULL,
                'created_at' => '2019-08-28 17:04:00',
                'updated_at' => '2019-08-28 17:04:00',
            ),
            203 => 
            array (
                'id_mensajes' => 356,
                'titulo' => 'Profitcalls',
                'mensaje' => 'mientras mas alto sea el SL menor es el lotaje',
                'link_imagen' => NULL,
                'created_at' => '2019-08-28 19:08:00',
                'updated_at' => '2019-08-28 19:08:00',
            ),
            204 => 
            array (
                'id_mensajes' => 357,
                'titulo' => 'Profit4Life',
                'mensaje' => 'GBPNZD Se mueve stop loss a: 1.92744000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-28 20:24:00',
                'updated_at' => '2019-08-28 20:24:00',
            ),
            205 => 
            array (
                'id_mensajes' => 358,
                'titulo' => 'Profit4life',
                'mensaje' => 'La calculadora de pips te ayuda a calcular el lotaje para cada trade',
                'link_imagen' => NULL,
                'created_at' => '2019-08-28 20:40:00',
                'updated_at' => '2019-08-28 20:40:00',
            ),
            206 => 
            array (
                'id_mensajes' => 359,
                'titulo' => 'GBPNZD',
                'mensaje' => 'Ya estamos en zona positiva!!',
                'link_imagen' => NULL,
                'created_at' => '2019-08-29 01:45:00',
                'updated_at' => '2019-08-29 01:45:00',
            ),
            207 => 
            array (
                'id_mensajes' => 360,
                'titulo' => 'Profit4life',
                'mensaje' => 'El dinero nunca duerme',
                'link_imagen' => NULL,
                'created_at' => '2019-08-29 05:36:00',
                'updated_at' => '2019-08-29 05:36:00',
            ),
            208 => 
            array (
                'id_mensajes' => 361,
                'titulo' => 'GBPNZD',
                'mensaje' => NULL,
                'link_imagen' => NULL,
                'created_at' => '2019-08-29 12:01:00',
                'updated_at' => '2019-08-29 12:01:00',
            ),
            209 => 
            array (
                'id_mensajes' => 362,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Pips del Take Profit:100.00000000 Pips del Stop Loss:69.20000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-29 12:01:00',
                'updated_at' => '2019-08-29 12:01:00',
            ),
            210 => 
            array (
                'id_mensajes' => 363,
                'titulo' => 'GBPNZD',
                'mensaje' => '50pips de la ultima operacion podriamos empezar a hacer trailing',
                'link_imagen' => NULL,
                'created_at' => '2019-08-29 14:55:00',
                'updated_at' => '2019-08-29 14:55:00',
            ),
            211 => 
            array (
                'id_mensajes' => 364,
                'titulo' => 'Profit4Life',
                'mensaje' => 'GBPNZD Se mueve stop loss a: 1.93087000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-29 14:55:00',
                'updated_at' => '2019-08-29 14:55:00',
            ),
            212 => 
            array (
                'id_mensajes' => 365,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Saliendo a ganacia en el par:GBPNZD',
                'link_imagen' => NULL,
                'created_at' => '2019-08-29 15:00:00',
                'updated_at' => '2019-08-29 15:00:00',
            ),
            213 => 
            array (
                'id_mensajes' => 366,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Hemos salid_mensajeso de la operacion con 45.6pips de ganancia=GBPNZD',
                'link_imagen' => NULL,
                'created_at' => '2019-08-29 15:02:00',
                'updated_at' => '2019-08-29 15:02:00',
            ),
            214 => 
            array (
                'id_mensajes' => 367,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Pips del Take Profit:150.00000000 Pips del Stop Loss:90.80000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-29 20:02:00',
                'updated_at' => '2019-08-29 20:02:00',
            ),
            215 => 
            array (
                'id_mensajes' => 369,
                'titulo' => 'XAAUSD',
                'mensaje' => 'XAAUSD Entrada:1528.64, TP:1543.64, SL:1519.56 Long',
                'link_imagen' => NULL,
                'created_at' => '2019-08-29 20:06:00',
                'updated_at' => '2019-08-29 20:06:00',
            ),
            216 => 
            array (
                'id_mensajes' => 370,
                'titulo' => 'XAAUSD',
                'mensaje' => 'XAAUSD Entrada:1528.64, TP:1543.64, SL:1519.56 Long, Pips TP:150, Pips SL:90.8',
                'link_imagen' => NULL,
                'created_at' => '2019-08-29 20:09:00',
                'updated_at' => '2019-08-29 20:09:00',
            ),
            217 => 
            array (
                'id_mensajes' => 371,
                'titulo' => 'Profit4Life',
                'mensaje' => 'El esfuerzo y la constancia tienen premio',
                'link_imagen' => NULL,
                'created_at' => '2019-08-29 20:36:00',
                'updated_at' => '2019-08-29 20:36:00',
            ),
            218 => 
            array (
                'id_mensajes' => 372,
                'titulo' => 'Profit4life',
                'mensaje' => 'Tenemos dos ordenes abiertas',
                'link_imagen' => NULL,
                'created_at' => '2019-08-29 21:18:00',
                'updated_at' => '2019-08-29 21:18:00',
            ),
            219 => 
            array (
                'id_mensajes' => 373,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Long XAUUSD Entrada:1528.97000000 TP:1543.97000000 SL:1519.56000000Pips del TP:150.00000000 Pips SL:94.10000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-30 00:00:00',
                'updated_at' => '2019-08-30 00:00:00',
            ),
            220 => 
            array (
                'id_mensajes' => 375,
                'titulo' => 'GBPNZD',
                'mensaje' => 'Atentos a este par podriamos empezar a hacer trailing pronto.',
                'link_imagen' => NULL,
                'created_at' => '2019-08-30 02:23:00',
                'updated_at' => '2019-08-30 02:23:00',
            ),
            221 => 
            array (
                'id_mensajes' => 376,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Long XAUUSD Entrada:1529.82000000 TP:1544.82000000 SL:1522.85000000Pips del TP:150.00000000 Pips SL:69.70000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-30 06:00:00',
                'updated_at' => '2019-08-30 06:00:00',
            ),
            222 => 
            array (
                'id_mensajes' => 377,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Lotaje sugerid_mensajeso por cada 10.000$: 0.28694405',
                'link_imagen' => NULL,
                'created_at' => '2019-08-30 06:04:00',
                'updated_at' => '2019-08-30 06:04:00',
            ),
            223 => 
            array (
                'id_mensajes' => 378,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Saliendo por stop loss en el par:XAUUSD',
                'link_imagen' => NULL,
                'created_at' => '2019-08-30 08:00:00',
                'updated_at' => '2019-08-30 08:00:00',
            ),
            224 => 
            array (
                'id_mensajes' => 379,
                'titulo' => 'Profit4life',
                'mensaje' => 'Buen dia a todos!! mas cerca de las metas hoy!!',
                'link_imagen' => NULL,
                'created_at' => '2019-08-30 13:06:00',
                'updated_at' => '2019-08-30 13:06:00',
            ),
            225 => 
            array (
                'id_mensajes' => 380,
                'titulo' => 'Profit4life',
                'mensaje' => 'Las pérdid_mensajesas son Controladas. primera pérdid_mensajesa en Oro.',
                'link_imagen' => NULL,
                'created_at' => '2019-08-30 13:33:00',
                'updated_at' => '2019-08-30 13:33:00',
            ),
            226 => 
            array (
                'id_mensajes' => 381,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Saliendo por stop loss en el par:XAUUSD',
                'link_imagen' => NULL,
                'created_at' => '2019-08-30 17:31:00',
                'updated_at' => '2019-08-30 17:31:00',
            ),
            227 => 
            array (
                'id_mensajes' => 382,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Saliendo por stop loss en el par:XAUUSD',
                'link_imagen' => NULL,
                'created_at' => '2019-08-30 17:31:00',
                'updated_at' => '2019-08-30 17:31:00',
            ),
            228 => 
            array (
                'id_mensajes' => 383,
                'titulo' => 'Profit4life',
                'mensaje' => 'Tenemos aun GBPNZD corriendo',
                'link_imagen' => NULL,
                'created_at' => '2019-08-30 17:38:00',
                'updated_at' => '2019-08-30 17:38:00',
            ),
            229 => 
            array (
                'id_mensajes' => 384,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Long XAUUSD Entrada:1525.56000000 TP:1540.56000000 SL:1516.96000000Pips del TP:150.00000000 Pips SL:86.00000000',
                'link_imagen' => NULL,
                'created_at' => '2019-08-30 19:00:00',
                'updated_at' => '2019-08-30 19:00:00',
            ),
            230 => 
            array (
                'id_mensajes' => 385,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Lotaje sugerid_mensajeso por cada 10.000$: 0.13333333',
                'link_imagen' => NULL,
                'created_at' => '2019-08-30 19:00:00',
                'updated_at' => '2019-08-30 19:00:00',
            ),
            231 => 
            array (
                'id_mensajes' => 386,
                'titulo' => 'Profit4life',
                'mensaje' => 'cerramos la primera semana 769 pips Positivos esperamos superar los 1000 pips la próxima semana.',
                'link_imagen' => NULL,
                'created_at' => '2019-08-31 02:36:00',
                'updated_at' => '2019-08-31 02:36:00',
            ),
            232 => 
            array (
                'id_mensajes' => 387,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Saliendo a ganacia en el par:XAUUSD',
                'link_imagen' => NULL,
                'created_at' => '2019-09-01 22:25:00',
                'updated_at' => '2019-09-01 22:25:00',
            ),
            233 => 
            array (
                'id_mensajes' => 388,
                'titulo' => 'Profitcall',
                'mensaje' => 'cerramos Posición del oro con ganancia 45 pips',
                'link_imagen' => NULL,
                'created_at' => '2019-09-01 22:26:00',
                'updated_at' => '2019-09-01 22:26:00',
            ),
            234 => 
            array (
                'id_mensajes' => 389,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Comenzamos la semana cerrado en ganacia XAUUSD Feliz inicio de semana de trading',
                'link_imagen' => NULL,
                'created_at' => '2019-09-01 23:14:00',
                'updated_at' => '2019-09-01 23:14:00',
            ),
            235 => 
            array (
                'id_mensajes' => 390,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Saliendo por stop loss en el par:GBPNZD',
                'link_imagen' => NULL,
                'created_at' => '2019-09-02 08:59:00',
                'updated_at' => '2019-09-02 08:59:00',
            ),
            236 => 
            array (
                'id_mensajes' => 391,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Long XAUUSD Entrada:1525.84000000 TP:1540.84000000 SL:1519.36000000Pips del TP:150.00000000 Pips SL:64.80000000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-02 10:04:00',
                'updated_at' => '2019-09-02 10:04:00',
            ),
            237 => 
            array (
                'id_mensajes' => 392,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Lotaje sugerid_mensajeso por cada 10.000$: 0.13333333',
                'link_imagen' => NULL,
                'created_at' => '2019-09-02 10:04:00',
                'updated_at' => '2019-09-02 10:04:00',
            ),
            238 => 
            array (
                'id_mensajes' => 393,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Long GBPAUD Entrada:1.79822000 TP:1.81322000 SL:1.79213000Pips del TP:150.00000000 Pips SL:60.90000000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-02 17:04:00',
                'updated_at' => '2019-09-02 17:04:00',
            ),
            239 => 
            array (
                'id_mensajes' => 394,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Lotaje sugerid_mensajeso por cada 10.000$: 0.20000000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-02 17:04:00',
                'updated_at' => '2019-09-02 17:04:00',
            ),
            240 => 
            array (
                'id_mensajes' => 395,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Lotaje sugerid_mensajeso por cada 10.000$: 0.30000000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-03 01:00:00',
                'updated_at' => '2019-09-03 01:00:00',
            ),
            241 => 
            array (
                'id_mensajes' => 396,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Long GBPNZD Entrada:1.91561000 TP:1.92561000 SL:1.91045000Pips del TP:100.00000000 Pips SL:51.60000000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-03 01:04:00',
                'updated_at' => '2019-09-03 01:04:00',
            ),
            242 => 
            array (
                'id_mensajes' => 397,
                'titulo' => 'Profit4life',
                'mensaje' => 'tenemos 3 operaciones abiertas, no olvid_mensajesen el manejo del Riesgo',
                'link_imagen' => NULL,
                'created_at' => '2019-09-03 01:21:00',
                'updated_at' => '2019-09-03 01:21:00',
            ),
            243 => 
            array (
                'id_mensajes' => 398,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Saliendo por stop loss en el par:GBPAUD',
                'link_imagen' => NULL,
                'created_at' => '2019-09-03 05:12:00',
                'updated_at' => '2019-09-03 05:12:00',
            ),
            244 => 
            array (
                'id_mensajes' => 399,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Saliendo por stop loss en el par:GBPNZD',
                'link_imagen' => NULL,
                'created_at' => '2019-09-03 05:42:00',
                'updated_at' => '2019-09-03 05:42:00',
            ),
            245 => 
            array (
                'id_mensajes' => 400,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Long XAUUSD Entrada:1526.75000000 TP:1541.75000000 SL:1520.11000000Pips del TP:150.00000000 Pips SL:66.40000000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-03 06:00:00',
                'updated_at' => '2019-09-03 06:00:00',
            ),
            246 => 
            array (
                'id_mensajes' => 401,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Lotaje sugerid_mensajeso por cada 10.000$: 0.13333333',
                'link_imagen' => NULL,
                'created_at' => '2019-09-03 06:00:00',
                'updated_at' => '2019-09-03 06:00:00',
            ),
            247 => 
            array (
                'id_mensajes' => 402,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1532.47000000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-03 09:52:00',
                'updated_at' => '2019-09-03 09:52:00',
            ),
            248 => 
            array (
                'id_mensajes' => 403,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Saliendo a ganacia en el par:XAUUSD',
                'link_imagen' => NULL,
                'created_at' => '2019-09-03 10:05:00',
                'updated_at' => '2019-09-03 10:05:00',
            ),
            249 => 
            array (
                'id_mensajes' => 404,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1533.31000000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-03 12:25:00',
                'updated_at' => '2019-09-03 12:25:00',
            ),
            250 => 
            array (
                'id_mensajes' => 405,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Long XRPUSD Entrada:0.26138000 TP:0.27338000 SL:0.25831000Pips del TP:120.00000000 Pips SL:30.70000000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-03 12:30:00',
                'updated_at' => '2019-09-03 12:30:00',
            ),
            251 => 
            array (
                'id_mensajes' => 406,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Lotaje sugerid_mensajeso por cada 10.000$: 0.25000000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-03 12:30:00',
                'updated_at' => '2019-09-03 12:30:00',
            ),
            252 => 
            array (
                'id_mensajes' => 407,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Saliendo a ganacia en el par:XAUUSD',
                'link_imagen' => NULL,
                'created_at' => '2019-09-03 12:33:00',
                'updated_at' => '2019-09-03 12:33:00',
            ),
            253 => 
            array (
                'id_mensajes' => 408,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Long ETHUSD Entrada:179.36000000 TP:184.84000000 SL:173.88000000Pips del TP:54.80000000 Pips SL:54.80000000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-03 13:00:00',
                'updated_at' => '2019-09-03 13:00:00',
            ),
            254 => 
            array (
                'id_mensajes' => 409,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Lotaje sugerid_mensajeso por cada 10.000$: 0.91240876',
                'link_imagen' => NULL,
                'created_at' => '2019-09-03 13:00:00',
                'updated_at' => '2019-09-03 13:00:00',
            ),
            255 => 
            array (
                'id_mensajes' => 410,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XRPUSD Se mueve stop loss a: 0.26556000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-03 13:40:00',
                'updated_at' => '2019-09-03 13:40:00',
            ),
            256 => 
            array (
                'id_mensajes' => 411,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Saliendo a ganacia en el par:XRPUSD',
                'link_imagen' => NULL,
                'created_at' => '2019-09-03 13:40:00',
                'updated_at' => '2019-09-03 13:40:00',
            ),
            257 => 
            array (
                'id_mensajes' => 412,
                'titulo' => 'Profit4Life',
                'mensaje' => 'ETHUSD Se mueve stop loss a: 181.72000000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-03 14:24:00',
                'updated_at' => '2019-09-03 14:24:00',
            ),
            258 => 
            array (
                'id_mensajes' => 413,
                'titulo' => 'Profit4Life',
                'mensaje' => 'ETHUSD Se mueve stop loss a: 182.30000000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-03 14:24:00',
                'updated_at' => '2019-09-03 14:24:00',
            ),
            259 => 
            array (
                'id_mensajes' => 414,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Saliendo a ganacia en el par:ETHUSD',
                'link_imagen' => NULL,
                'created_at' => '2019-09-03 14:24:00',
                'updated_at' => '2019-09-03 14:24:00',
            ),
            260 => 
            array (
                'id_mensajes' => 415,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Long GBPNZD Entrada:1.91359000 TP:1.92770000 SL:1.89948000Pips del TP:141.10000000 Pips SL:141.10000000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-03 15:00:00',
                'updated_at' => '2019-09-03 15:00:00',
            ),
            261 => 
            array (
                'id_mensajes' => 416,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Long GBPAUD Entrada:1.78996000 TP:1.80496000 SL:1.77926000Pips del TP:150.00000000 Pips SL:107.00000000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-03 15:00:00',
                'updated_at' => '2019-09-03 15:00:00',
            ),
            262 => 
            array (
                'id_mensajes' => 417,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Lotaje sugerid_mensajeso por cada 10.000$: 0.21261517',
                'link_imagen' => NULL,
                'created_at' => '2019-09-03 15:00:00',
                'updated_at' => '2019-09-03 15:00:00',
            ),
            263 => 
            array (
                'id_mensajes' => 418,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Lotaje sugerid_mensajeso por cada 10.000$: 0.20000000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-03 15:00:00',
                'updated_at' => '2019-09-03 15:00:00',
            ),
            264 => 
            array (
                'id_mensajes' => 419,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Long GBPNZD Entrada:1.90931000 TP:1.91931000 SL:1.90251000Pips del TP:100.00000000 Pips SL:68.00000000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-04 01:00:00',
                'updated_at' => '2019-09-04 01:00:00',
            ),
            265 => 
            array (
                'id_mensajes' => 420,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Long US30 Entrada:26208.14000000 TP:26438.91000000 SL:25977.37000000Pips del TP:2307.70000000 Pips SL:2307.70000000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-04 02:00:00',
                'updated_at' => '2019-09-04 02:00:00',
            ),
            266 => 
            array (
                'id_mensajes' => 421,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Lotaje sugerid_mensajeso por cada 10.000$: 0.00013000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-04 02:00:00',
                'updated_at' => '2019-09-04 02:00:00',
            ),
            267 => 
            array (
                'id_mensajes' => 422,
                'titulo' => 'Profitcall',
                'mensaje' => 'si tú cuenta es menor de $1000 no recomendamos tomar entradas de Us30',
                'link_imagen' => NULL,
                'created_at' => '2019-09-04 02:02:00',
                'updated_at' => '2019-09-04 02:02:00',
            ),
            268 => 
            array (
                'id_mensajes' => 423,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Long ETHUSD Entrada:180.16000000 TP:185.16000000 SL:177.63000000Pips del TP:50.00000000 Pips SL:25.30000000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-04 04:00:00',
                'updated_at' => '2019-09-04 04:00:00',
            ),
            269 => 
            array (
                'id_mensajes' => 424,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Lotaje sugerid_mensajeso por cada 10.000$: 1.00000000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-04 04:00:00',
                'updated_at' => '2019-09-04 04:00:00',
            ),
            270 => 
            array (
                'id_mensajes' => 425,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Saliendo a ganacia en el par:US30',
                'link_imagen' => NULL,
                'created_at' => '2019-09-04 04:41:00',
                'updated_at' => '2019-09-04 04:41:00',
            ),
            271 => 
            array (
                'id_mensajes' => 426,
                'titulo' => 'Profit4Life',
                'mensaje' => 'GBPNZD Se mueve stop loss a: 1.91384000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-04 06:38:00',
                'updated_at' => '2019-09-04 06:38:00',
            ),
            272 => 
            array (
                'id_mensajes' => 427,
                'titulo' => 'GNPNZD',
                'mensaje' => 'Se mueve en la posicion q tiene mas de 50pips de ganancia',
                'link_imagen' => NULL,
                'created_at' => '2019-09-04 06:39:00',
                'updated_at' => '2019-09-04 06:39:00',
            ),
            273 => 
            array (
                'id_mensajes' => 428,
                'titulo' => 'Profit4Life',
                'mensaje' => 'GBPNZD Se mueve stop loss a: 1.91434000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-04 06:40:00',
                'updated_at' => '2019-09-04 06:40:00',
            ),
            274 => 
            array (
                'id_mensajes' => 429,
                'titulo' => 'Profit4Life',
                'mensaje' => 'GBPNZD Se mueve stop loss a: 1.91435000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-04 06:41:00',
                'updated_at' => '2019-09-04 06:41:00',
            ),
            275 => 
            array (
                'id_mensajes' => 430,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Saliendo a ganacia en el par:GBPNZD',
                'link_imagen' => NULL,
                'created_at' => '2019-09-04 06:41:00',
                'updated_at' => '2019-09-04 06:41:00',
            ),
            276 => 
            array (
                'id_mensajes' => 431,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Long GBPAUD Entrada:1.79066000 TP:1.80566000 SL:1.78337000Pips del TP:150.00000000 Pips SL:72.90000000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-04 07:00:00',
                'updated_at' => '2019-09-04 07:00:00',
            ),
            277 => 
            array (
                'id_mensajes' => 432,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Lotaje sugerid_mensajeso por cada 10.000$: 0.20000000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-04 07:00:00',
                'updated_at' => '2019-09-04 07:00:00',
            ),
            278 => 
            array (
                'id_mensajes' => 433,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Saliendo por stop loss en el par:ETHUSD',
                'link_imagen' => NULL,
                'created_at' => '2019-09-04 08:12:00',
                'updated_at' => '2019-09-04 08:12:00',
            ),
            279 => 
            array (
                'id_mensajes' => 434,
                'titulo' => 'Profit4Life',
                'mensaje' => 'GBPNZD Se mueve stop loss a: 1.91811000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-04 09:59:00',
                'updated_at' => '2019-09-04 09:59:00',
            ),
            280 => 
            array (
                'id_mensajes' => 435,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Saliendo a ganacia en el par:GBPNZD',
                'link_imagen' => NULL,
                'created_at' => '2019-09-04 10:00:00',
                'updated_at' => '2019-09-04 10:00:00',
            ),
            281 => 
            array (
                'id_mensajes' => 436,
                'titulo' => 'Profit4Life',
                'mensaje' => 'GBPAUD Se mueve stop loss a: 1.79648000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-04 10:58:00',
                'updated_at' => '2019-09-04 10:58:00',
            ),
            282 => 
            array (
                'id_mensajes' => 437,
                'titulo' => 'Profit4Life',
                'mensaje' => 'GBPAUD Se mueve stop loss a: 1.79724000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-04 10:58:00',
                'updated_at' => '2019-09-04 10:58:00',
            ),
            283 => 
            array (
                'id_mensajes' => 438,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Saliendo a ganacia en el par:GBPAUD',
                'link_imagen' => NULL,
                'created_at' => '2019-09-04 11:02:00',
                'updated_at' => '2019-09-04 11:02:00',
            ),
            284 => 
            array (
                'id_mensajes' => 439,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Saliendo a ganacia en el par:GBPAUD',
                'link_imagen' => NULL,
                'created_at' => '2019-09-04 11:03:00',
                'updated_at' => '2019-09-04 11:03:00',
            ),
            285 => 
            array (
                'id_mensajes' => 440,
                'titulo' => 'Profit4Life',
                'mensaje' => '24 horas de trading intensas, nos dejaron 8 operaciones buenas 3 malas y 841 pips de ganancias',
                'link_imagen' => NULL,
                'created_at' => '2019-09-04 12:56:00',
                'updated_at' => '2019-09-04 12:56:00',
            ),
            286 => 
            array (
                'id_mensajes' => 441,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Long XAUUSD Entrada:1541.88000000 TP:1556.88000000 SL:1533.88000000Pips del TP:150.00000000 Pips SL:80.00000000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-04 13:00:00',
                'updated_at' => '2019-09-04 13:00:00',
            ),
            287 => 
            array (
                'id_mensajes' => 442,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Lotaje sugerid_mensajeso por cada 10.000$: 0.13333333',
                'link_imagen' => NULL,
                'created_at' => '2019-09-04 13:00:00',
                'updated_at' => '2019-09-04 13:00:00',
            ),
            288 => 
            array (
                'id_mensajes' => 443,
                'titulo' => 'XAAUSD',
                'mensaje' => 'Atentos a los movimientos que estamos en 60pips de profit',
                'link_imagen' => NULL,
                'created_at' => '2019-09-04 14:11:00',
                'updated_at' => '2019-09-04 14:11:00',
            ),
            289 => 
            array (
                'id_mensajes' => 444,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Saliendo a ganacia en el par:XAUUSD',
                'link_imagen' => NULL,
                'created_at' => '2019-09-04 14:18:00',
                'updated_at' => '2019-09-04 14:18:00',
            ),
            290 => 
            array (
                'id_mensajes' => 445,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Long GBPNZD Entrada:1.92105000 TP:1.93105000 SL:1.91511000Pips del TP:100.00000000 Pips SL:59.40000000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-05 08:00:00',
                'updated_at' => '2019-09-05 08:00:00',
            ),
            291 => 
            array (
                'id_mensajes' => 446,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Lotaje sugerid_mensajeso por cada 10.000$: 0.30000000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-05 08:00:00',
                'updated_at' => '2019-09-05 08:00:00',
            ),
            292 => 
            array (
                'id_mensajes' => 447,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Long GBPAUD Entrada:1.79845000 TP:1.81345000 SL:1.79243000Pips del TP:150.00000000 Pips SL:60.20000000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-05 08:00:00',
                'updated_at' => '2019-09-05 08:00:00',
            ),
            293 => 
            array (
                'id_mensajes' => 448,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Lotaje sugerid_mensajeso por cada 10.000$: 0.20000000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-05 08:00:00',
                'updated_at' => '2019-09-05 08:00:00',
            ),
            294 => 
            array (
                'id_mensajes' => 449,
                'titulo' => 'Profit4Life',
                'mensaje' => 'GBPNZD Se mueve stop loss a: 1.92562000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-05 08:37:00',
                'updated_at' => '2019-09-05 08:37:00',
            ),
            295 => 
            array (
                'id_mensajes' => 450,
                'titulo' => 'Profit4Life',
                'mensaje' => 'GBPNZD Se mueve stop loss a: 1.92613000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-05 08:38:00',
                'updated_at' => '2019-09-05 08:38:00',
            ),
            296 => 
            array (
                'id_mensajes' => 451,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Saliendo a ganacia en el par:GBPNZD',
                'link_imagen' => NULL,
                'created_at' => '2019-09-05 08:38:00',
                'updated_at' => '2019-09-05 08:38:00',
            ),
            297 => 
            array (
                'id_mensajes' => 452,
                'titulo' => 'Profit4Life',
                'mensaje' => 'GBPAUD Se mueve stop loss a: 1.80501000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-05 09:52:00',
                'updated_at' => '2019-09-05 09:52:00',
            ),
            298 => 
            array (
                'id_mensajes' => 453,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Saliendo a ganacia en el par:GBPAUD',
                'link_imagen' => NULL,
                'created_at' => '2019-09-05 09:56:00',
                'updated_at' => '2019-09-05 09:56:00',
            ),
            299 => 
            array (
                'id_mensajes' => 454,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Long XAUUSD Entrada:1516.36000000 TP:1531.36000000 SL:1502.40000000Pips del TP:150.00000000 Pips SL:139.60000000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-06 13:00:00',
                'updated_at' => '2019-09-06 13:00:00',
            ),
            300 => 
            array (
                'id_mensajes' => 455,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Lotaje sugerid_mensajeso por cada 10.000$: 0.13333333',
                'link_imagen' => NULL,
                'created_at' => '2019-09-06 13:00:00',
                'updated_at' => '2019-09-06 13:00:00',
            ),
            301 => 
            array (
                'id_mensajes' => 456,
                'titulo' => 'XAUUSD',
                'mensaje' => 'Atentos a los movimientos',
                'link_imagen' => NULL,
                'created_at' => '2019-09-06 13:13:00',
                'updated_at' => '2019-09-06 13:13:00',
            ),
            302 => 
            array (
                'id_mensajes' => 457,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1523.09000000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-06 13:13:00',
                'updated_at' => '2019-09-06 13:13:00',
            ),
            303 => 
            array (
                'id_mensajes' => 458,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Saliendo a ganacia en el par:XAUUSD',
                'link_imagen' => NULL,
                'created_at' => '2019-09-06 13:14:00',
                'updated_at' => '2019-09-06 13:14:00',
            ),
            304 => 
            array (
                'id_mensajes' => 459,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Cada día más cerca de la libertad financiera',
                'link_imagen' => NULL,
                'created_at' => '2019-09-06 13:23:00',
                'updated_at' => '2019-09-06 13:23:00',
            ),
            305 => 
            array (
                'id_mensajes' => 460,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Short ETHUSD Entrada:176.53000000 TP:171.11000000 SL:178.15000000Pips del TP:50.00000000 Pips SL:20.40000000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-06 13:30:00',
                'updated_at' => '2019-09-06 13:30:00',
            ),
            306 => 
            array (
                'id_mensajes' => 461,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Lotaje sugerid_mensajeso por cada 10.000$: 1.00000000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-06 13:30:00',
                'updated_at' => '2019-09-06 13:30:00',
            ),
            307 => 
            array (
                'id_mensajes' => 462,
                'titulo' => 'Profit4Life',
                'mensaje' => 'ETHUSD Se mueve stop loss a: 174.13000000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-06 17:40:00',
                'updated_at' => '2019-09-06 17:40:00',
            ),
            308 => 
            array (
                'id_mensajes' => 463,
                'titulo' => 'Profit4Life',
                'mensaje' => 'ETHUSD Se mueve stop loss a: 173.51000000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-06 17:40:00',
                'updated_at' => '2019-09-06 17:40:00',
            ),
            309 => 
            array (
                'id_mensajes' => 464,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Saliendo a ganacia en el par:ETHUSD',
                'link_imagen' => NULL,
                'created_at' => '2019-09-06 17:40:00',
                'updated_at' => '2019-09-06 17:40:00',
            ),
            310 => 
            array (
                'id_mensajes' => 465,
                'titulo' => 'ETHUSD',
                'mensaje' => 'La operacion alcanzo TP y mucho mas!! mas de 70 pips',
                'link_imagen' => NULL,
                'created_at' => '2019-09-06 17:48:00',
                'updated_at' => '2019-09-06 17:48:00',
            ),
            311 => 
            array (
                'id_mensajes' => 467,
                'titulo' => 'Profit4life',
                'mensaje' => 'Feliz viernes a todos 222 pips hoy para cerrar una excelente semana.',
                'link_imagen' => NULL,
                'created_at' => '2019-09-06 18:40:00',
                'updated_at' => '2019-09-06 18:40:00',
            ),
            312 => 
            array (
                'id_mensajes' => 468,
                'titulo' => 'Profit4life',
                'mensaje' => 'Los fin de semana estamos pendiente de las criptomonedas. Forex e indices cerrado',
                'link_imagen' => NULL,
                'created_at' => '2019-09-07 05:05:00',
                'updated_at' => '2019-09-07 05:05:00',
            ),
            313 => 
            array (
                'id_mensajes' => 470,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Es una prueba de mensajeria',
                'link_imagen' => NULL,
                'created_at' => '2019-09-07 13:45:00',
                'updated_at' => '2019-09-07 13:45:00',
            ),
            314 => 
            array (
                'id_mensajes' => 471,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Long XRPUSD Entrada:0.26448000 TP:0.27648000 SL:0.25630000Pips del TP:120.00000000 Pips SL:81.80000000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-08 15:00:00',
                'updated_at' => '2019-09-08 15:00:00',
            ),
            315 => 
            array (
                'id_mensajes' => 472,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Lotaje sugerid_mensajeso por cada 10.000$: 0.25000000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-08 15:00:00',
                'updated_at' => '2019-09-08 15:00:00',
            ),
            316 => 
            array (
                'id_mensajes' => 473,
                'titulo' => 'Profit4life',
                'mensaje' => 'Todos los mercados abiertos esperamos tener una semana llena de Profits !!',
                'link_imagen' => NULL,
                'created_at' => '2019-09-08 22:26:00',
                'updated_at' => '2019-09-08 22:26:00',
            ),
            317 => 
            array (
                'id_mensajes' => 474,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Long GBPNZD Entrada:1.91315000 TP:1.92315000 SL:1.90555000Pips del TP:100.00000000 Pips SL:76.00000000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-09 08:00:00',
                'updated_at' => '2019-09-09 08:00:00',
            ),
            318 => 
            array (
                'id_mensajes' => 475,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Lotaje sugerid_mensajeso por cada 10.000$: 0.30000000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-09 08:00:00',
                'updated_at' => '2019-09-09 08:00:00',
            ),
            319 => 
            array (
                'id_mensajes' => 476,
                'titulo' => 'Profit4Life',
                'mensaje' => 'GBPNZD Se mueve stop loss a: 1.91766000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-09 09:35:00',
                'updated_at' => '2019-09-09 09:35:00',
            ),
            320 => 
            array (
                'id_mensajes' => 477,
                'titulo' => 'Profit4Life',
                'mensaje' => 'GBPNZD Se mueve stop loss a: 1.91822000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-09 09:35:00',
                'updated_at' => '2019-09-09 09:35:00',
            ),
            321 => 
            array (
                'id_mensajes' => 478,
                'titulo' => 'Profit4Life',
                'mensaje' => 'GBPNZD Se mueve stop loss a: 1.91947000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-09 09:35:00',
                'updated_at' => '2019-09-09 09:35:00',
            ),
            322 => 
            array (
                'id_mensajes' => 479,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Saliendo a ganacia en el par:GBPNZD',
                'link_imagen' => NULL,
                'created_at' => '2019-09-09 09:35:00',
                'updated_at' => '2019-09-09 09:35:00',
            ),
            323 => 
            array (
                'id_mensajes' => 480,
                'titulo' => 'Profit4life',
                'mensaje' => 'Seguimos en XRPUSD, recuerden el manejo del riesgo',
                'link_imagen' => NULL,
                'created_at' => '2019-09-09 21:48:00',
                'updated_at' => '2019-09-09 21:48:00',
            ),
            324 => 
            array (
                'id_mensajes' => 481,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Long ETHUSD Entrada:182.58000000 TP:187.58000000 SL:178.80000000Pips del TP:50.00000000 Pips SL:37.80000000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-09 23:00:00',
                'updated_at' => '2019-09-09 23:00:00',
            ),
            325 => 
            array (
                'id_mensajes' => 482,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Lotaje sugerid_mensajeso por cada 10.000$: 1.00000000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-09 23:00:00',
                'updated_at' => '2019-09-09 23:00:00',
            ),
            326 => 
            array (
                'id_mensajes' => 483,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Long GBPNZD Entrada:1.92192000 TP:1.93192000 SL:1.91490000Pips del TP:100.00000000 Pips SL:70.20000000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-10 11:00:00',
                'updated_at' => '2019-09-10 11:00:00',
            ),
            327 => 
            array (
                'id_mensajes' => 484,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Lotaje sugerid_mensajeso por cada 10.000$: 0.30000000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-10 11:00:00',
                'updated_at' => '2019-09-10 11:00:00',
            ),
            328 => 
            array (
                'id_mensajes' => 485,
                'titulo' => 'Profit4Life',
                'mensaje' => 'GBPNZD Se mueve stop loss a: 1.92643000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-10 13:28:00',
                'updated_at' => '2019-09-10 13:28:00',
            ),
            329 => 
            array (
                'id_mensajes' => 486,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Saliendo a ganacia en el par:GBPNZD',
                'link_imagen' => NULL,
                'created_at' => '2019-09-10 13:31:00',
                'updated_at' => '2019-09-10 13:31:00',
            ),
            330 => 
            array (
                'id_mensajes' => 487,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Saliendo por stop loss en el par:ETHUSD',
                'link_imagen' => NULL,
                'created_at' => '2019-09-10 14:20:00',
                'updated_at' => '2019-09-10 14:20:00',
            ),
            331 => 
            array (
                'id_mensajes' => 488,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Short XAUUSD Entrada:1491.32000000 TP:1475.78000000 SL:1500.75000000Pips del TP:150.00000000 Pips SL:99.70000000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-10 19:00:00',
                'updated_at' => '2019-09-10 19:00:00',
            ),
            332 => 
            array (
                'id_mensajes' => 489,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Lotaje sugerid_mensajeso por cada 10.000$: 0.13333333',
                'link_imagen' => NULL,
                'created_at' => '2019-09-10 19:00:00',
                'updated_at' => '2019-09-10 19:00:00',
            ),
            333 => 
            array (
                'id_mensajes' => 490,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Saliendo por stop loss en el par:XRPUSD',
                'link_imagen' => NULL,
                'created_at' => '2019-09-10 19:36:00',
                'updated_at' => '2019-09-10 19:36:00',
            ),
            334 => 
            array (
                'id_mensajes' => 491,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Short ETHUSD Entrada:180.20000000 TP:174.79000000 SL:182.62000000Pips del TP:50.00000000 Pips SL:28.30000000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-11 04:00:00',
                'updated_at' => '2019-09-11 04:00:00',
            ),
            335 => 
            array (
                'id_mensajes' => 492,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Lotaje sugerid_mensajeso por cada 10.000$: 1.00000000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-11 04:00:00',
                'updated_at' => '2019-09-11 04:00:00',
            ),
            336 => 
            array (
                'id_mensajes' => 493,
                'titulo' => 'Profit4Life',
                'mensaje' => 'ETHUSD Se mueve stop loss a: 177.67000000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-11 06:51:00',
                'updated_at' => '2019-09-11 06:51:00',
            ),
            337 => 
            array (
                'id_mensajes' => 494,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Saliendo a ganacia en el par:ETHUSD',
                'link_imagen' => NULL,
                'created_at' => '2019-09-11 06:52:00',
                'updated_at' => '2019-09-11 06:52:00',
            ),
            338 => 
            array (
                'id_mensajes' => 495,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Short XAUUSD Entrada:1490.32000000 TP:1474.77000000 SL:1494.80000000Pips del TP:150.00000000 Pips SL:50.30000000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-11 08:00:00',
                'updated_at' => '2019-09-11 08:00:00',
            ),
            339 => 
            array (
                'id_mensajes' => 496,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Lotaje sugerid_mensajeso por cada 10.000$: 0.13333333',
                'link_imagen' => NULL,
                'created_at' => '2019-09-11 08:00:00',
                'updated_at' => '2019-09-11 08:00:00',
            ),
            340 => 
            array (
                'id_mensajes' => 497,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Long GBPNZD Entrada:1.92630000 TP:1.93630000 SL:1.91908000Pips del TP:100.00000000 Pips SL:72.20000000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-11 08:00:00',
                'updated_at' => '2019-09-11 08:00:00',
            ),
            341 => 
            array (
                'id_mensajes' => 498,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Lotaje sugerid_mensajeso por cada 10.000$: 0.30000000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-11 08:00:00',
                'updated_at' => '2019-09-11 08:00:00',
            ),
            342 => 
            array (
                'id_mensajes' => 499,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Saliendo por stop loss en el par:XAUUSD',
                'link_imagen' => NULL,
                'created_at' => '2019-09-11 09:00:00',
                'updated_at' => '2019-09-11 09:00:00',
            ),
            343 => 
            array (
                'id_mensajes' => 500,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Long GBPNZD Entrada:1.92499000 TP:1.93499000 SL:1.91908000Pips del TP:100.00000000 Pips SL:59.10000000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-11 15:00:00',
                'updated_at' => '2019-09-11 15:00:00',
            ),
            344 => 
            array (
                'id_mensajes' => 501,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Lotaje sugerid_mensajeso por cada 10.000$: 0.30000000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-11 15:00:00',
                'updated_at' => '2019-09-11 15:00:00',
            ),
            345 => 
            array (
                'id_mensajes' => 502,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Saliendo por stop loss en el par:GBPNZD',
                'link_imagen' => NULL,
                'created_at' => '2019-09-12 00:06:00',
                'updated_at' => '2019-09-12 00:06:00',
            ),
            346 => 
            array (
                'id_mensajes' => 503,
                'titulo' => 'Profit4life',
                'mensaje' => 'Esperamos recuperar los pips perdid_mensajesos en las próximas entradas, el manejo del riesgo y la paciencia son fundamentales',
                'link_imagen' => NULL,
                'created_at' => '2019-09-12 01:02:00',
                'updated_at' => '2019-09-12 01:02:00',
            ),
            347 => 
            array (
                'id_mensajes' => 504,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Saliendo por stop loss en el par:XAUUSD',
                'link_imagen' => NULL,
                'created_at' => '2019-09-12 06:56:00',
                'updated_at' => '2019-09-12 06:56:00',
            ),
            348 => 
            array (
                'id_mensajes' => 505,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Long GBPNZD Entrada:1.92183000 TP:1.93237000 SL:1.91129000Pips del TP:105.40000000 Pips SL:105.40000000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-12 14:00:00',
                'updated_at' => '2019-09-12 14:00:00',
            ),
            349 => 
            array (
                'id_mensajes' => 506,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Short XAUUSD Entrada:1503.38000000 TP:1482.08000000 SL:1524.08000000Pips del TP:210.00000000 Pips SL:210.00000000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-12 16:00:00',
                'updated_at' => '2019-09-12 16:00:00',
            ),
            350 => 
            array (
                'id_mensajes' => 507,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Short ETHUSD Entrada:179.41000000 TP:173.98000000 SL:180.62000000Pips del TP:50.00000000 Pips SL:16.40000000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-12 17:32:00',
                'updated_at' => '2019-09-12 17:32:00',
            ),
            351 => 
            array (
                'id_mensajes' => 508,
                'titulo' => 'Profit4Life',
                'mensaje' => 'GBPNZD Se mueve stop loss a: 1.92637000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-12 18:50:00',
                'updated_at' => '2019-09-12 18:50:00',
            ),
            352 => 
            array (
                'id_mensajes' => 509,
                'titulo' => 'Profit4Life',
                'mensaje' => 'GBPNZD Se mueve stop loss a: 1.92689000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-12 18:55:00',
                'updated_at' => '2019-09-12 18:55:00',
            ),
            353 => 
            array (
                'id_mensajes' => 510,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Saliendo a ganacia en el par:GBPNZD',
                'link_imagen' => NULL,
                'created_at' => '2019-09-12 18:57:00',
                'updated_at' => '2019-09-12 18:57:00',
            ),
            354 => 
            array (
                'id_mensajes' => 511,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Saliendo por stop loss en el par:ETHUSD',
                'link_imagen' => NULL,
                'created_at' => '2019-09-12 20:38:00',
                'updated_at' => '2019-09-12 20:38:00',
            ),
            355 => 
            array (
                'id_mensajes' => 512,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Saliendo a ganacia en el par:XAUUSD',
                'link_imagen' => NULL,
                'created_at' => '2019-09-12 20:39:00',
                'updated_at' => '2019-09-12 20:39:00',
            ),
            356 => 
            array (
                'id_mensajes' => 513,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Short GBPAUD Entrada:1.81606000 TP:1.80081000 SL:1.82022000Pips del TP:150.00000000 Pips SL:44.10000000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-15 23:00:00',
                'updated_at' => '2019-09-15 23:00:00',
            ),
            357 => 
            array (
                'id_mensajes' => 514,
                'titulo' => 'Profit4Life',
                'mensaje' => 'GBPAUD Se mueve stop loss a: 1.80928000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-16 08:31:00',
                'updated_at' => '2019-09-16 08:31:00',
            ),
            358 => 
            array (
                'id_mensajes' => 515,
                'titulo' => 'Profit4Life',
                'mensaje' => 'GBPAUD Se mueve stop loss a: 1.80823000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-16 08:37:00',
                'updated_at' => '2019-09-16 08:37:00',
            ),
            359 => 
            array (
                'id_mensajes' => 516,
                'titulo' => 'Profit4Life',
                'mensaje' => 'GBPAUD Se mueve stop loss a: 1.80696000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-16 08:58:00',
                'updated_at' => '2019-09-16 08:58:00',
            ),
            360 => 
            array (
                'id_mensajes' => 517,
                'titulo' => 'Profit4Life',
                'mensaje' => 'GBPAUD Se mueve stop loss a: 1.80696000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-16 08:58:00',
                'updated_at' => '2019-09-16 08:58:00',
            ),
            361 => 
            array (
                'id_mensajes' => 518,
                'titulo' => 'Profit4Life',
                'mensaje' => 'GBPAUD Se mueve stop loss a: 1.80692000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-16 08:58:00',
                'updated_at' => '2019-09-16 08:58:00',
            ),
            362 => 
            array (
                'id_mensajes' => 519,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Saliendo a ganacia en el par:GBPAUD',
                'link_imagen' => NULL,
                'created_at' => '2019-09-16 09:05:00',
                'updated_at' => '2019-09-16 09:05:00',
            ),
            363 => 
            array (
                'id_mensajes' => 520,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Long GBPNZD Entrada:1.95232000 TP:1.96232000 SL:1.94527000Pips del TP:100.00000000 Pips SL:70.50000000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-16 11:00:00',
                'updated_at' => '2019-09-16 11:00:00',
            ),
            364 => 
            array (
                'id_mensajes' => 521,
                'titulo' => 'Profit4Life',
                'mensaje' => 'GBPNZD Se mueve stop loss a: 1.95685000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-16 12:13:00',
                'updated_at' => '2019-09-16 12:13:00',
            ),
            365 => 
            array (
                'id_mensajes' => 522,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Saliendo a ganacia en el par:GBPNZD',
                'link_imagen' => NULL,
                'created_at' => '2019-09-16 12:13:00',
                'updated_at' => '2019-09-16 12:13:00',
            ),
            366 => 
            array (
                'id_mensajes' => 523,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Long XRPUSD Entrada:0.29309000 TP:0.30509000 SL:0.28468000Pips del TP:120.00000000 Pips SL:84.10000000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-18 00:30:00',
                'updated_at' => '2019-09-18 00:30:00',
            ),
            367 => 
            array (
                'id_mensajes' => 524,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XRPUSD Se mueve stop loss a: 0.29599000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-18 00:51:00',
                'updated_at' => '2019-09-18 00:51:00',
            ),
            368 => 
            array (
                'id_mensajes' => 525,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Saliendo a ganacia en el par:XRPUSD',
                'link_imagen' => NULL,
                'created_at' => '2019-09-18 00:52:00',
                'updated_at' => '2019-09-18 00:52:00',
            ),
            369 => 
            array (
                'id_mensajes' => 526,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Long XRPUSD Entrada:0.30970000 TP:0.32170000 SL:0.30246000Pips del TP:120.00000000 Pips SL:72.40000000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-18 10:30:00',
                'updated_at' => '2019-09-18 10:30:00',
            ),
            370 => 
            array (
                'id_mensajes' => 527,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XRPUSD Se mueve stop loss a: 0.31270000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-18 12:59:00',
                'updated_at' => '2019-09-18 12:59:00',
            ),
            371 => 
            array (
                'id_mensajes' => 528,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Saliendo a ganacia en el par:XRPUSD',
                'link_imagen' => NULL,
                'created_at' => '2019-09-18 13:00:00',
                'updated_at' => '2019-09-18 13:00:00',
            ),
            372 => 
            array (
                'id_mensajes' => 529,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Long XRPUSD Entrada:0.31674000 TP:0.32874000 SL:0.31119000Pips del TP:120.00000000 Pips SL:55.50000000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-18 21:30:00',
                'updated_at' => '2019-09-18 21:30:00',
            ),
            373 => 
            array (
                'id_mensajes' => 530,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Saliendo por stop loss en el par:XRPUSD',
                'link_imagen' => NULL,
                'created_at' => '2019-09-19 00:11:00',
                'updated_at' => '2019-09-19 00:11:00',
            ),
            374 => 
            array (
                'id_mensajes' => 531,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Long XRPUSD Entrada:0.30502000 TP:0.31702000 SL:0.29691000Pips del TP:120.00000000 Pips SL:81.10000000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-19 02:30:00',
                'updated_at' => '2019-09-19 02:30:00',
            ),
            375 => 
            array (
                'id_mensajes' => 532,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Saliendo por stop loss en el par:XRPUSD',
                'link_imagen' => NULL,
                'created_at' => '2019-09-19 03:03:00',
                'updated_at' => '2019-09-19 03:03:00',
            ),
            376 => 
            array (
                'id_mensajes' => 533,
                'titulo' => 'Profit4life',
                'mensaje' => 'Recuerden el Manejo del Riesgo es Muy importante eso hará que las pérdid_mensajesas no afecten nuestro balance ganador',
                'link_imagen' => NULL,
                'created_at' => '2019-09-19 03:12:00',
                'updated_at' => '2019-09-19 03:12:00',
            ),
            377 => 
            array (
                'id_mensajes' => 534,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Long ETHUSD Entrada:205.55000000 TP:210.55000000 SL:203.46000000Pips del TP:50.00000000 Pips SL:20.90000000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-19 05:30:00',
                'updated_at' => '2019-09-19 05:30:00',
            ),
            378 => 
            array (
                'id_mensajes' => 535,
                'titulo' => 'Profit4Life',
                'mensaje' => 'ETHUSD Se mueve stop loss a: 207.55000000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-19 08:44:00',
                'updated_at' => '2019-09-19 08:44:00',
            ),
            379 => 
            array (
                'id_mensajes' => 536,
                'titulo' => 'Profit4Life',
                'mensaje' => 'ETHUSD Se mueve stop loss a: 208.11000000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-19 08:46:00',
                'updated_at' => '2019-09-19 08:46:00',
            ),
            380 => 
            array (
                'id_mensajes' => 537,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Saliendo a ganacia en el par:ETHUSD',
                'link_imagen' => NULL,
                'created_at' => '2019-09-19 08:46:00',
                'updated_at' => '2019-09-19 08:46:00',
            ),
            381 => 
            array (
                'id_mensajes' => 538,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Short XAUUSD Entrada:1498.47000000 TP:1483.04000000 SL:1504.29000000Pips del TP:150.00000000 Pips SL:62.50000000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-19 23:00:00',
                'updated_at' => '2019-09-19 23:00:00',
            ),
            382 => 
            array (
                'id_mensajes' => 539,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Saliendo por stop loss en el par:XAUUSD',
                'link_imagen' => NULL,
                'created_at' => '2019-09-20 03:18:00',
                'updated_at' => '2019-09-20 03:18:00',
            ),
            383 => 
            array (
                'id_mensajes' => 540,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Long US30 Entrada:27141.84000000 TP:27291.84000000 SL:27059.67000000Pips del TP:1500.00000000 Pips SL:821.70000000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-20 10:00:00',
                'updated_at' => '2019-09-20 10:00:00',
            ),
            384 => 
            array (
                'id_mensajes' => 541,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Long ETHUSD Entrada:219.26000000 TP:224.26000000 SL:215.26000000Pips del TP:50.00000000 Pips SL:40.00000000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-20 12:00:00',
                'updated_at' => '2019-09-20 12:00:00',
            ),
            385 => 
            array (
                'id_mensajes' => 542,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Long GBPAUD Entrada:1.84559000 TP:1.86059000 SL:1.83552000Pips del TP:150.00000000 Pips SL:100.70000000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-20 15:00:00',
                'updated_at' => '2019-09-20 15:00:00',
            ),
            386 => 
            array (
                'id_mensajes' => 543,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Saliendo por stop loss en el par:ETHUSD',
                'link_imagen' => NULL,
                'created_at' => '2019-09-20 16:59:00',
                'updated_at' => '2019-09-20 16:59:00',
            ),
            387 => 
            array (
                'id_mensajes' => 544,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Saliendo por stop loss en el par:US30',
                'link_imagen' => NULL,
                'created_at' => '2019-09-20 17:13:00',
                'updated_at' => '2019-09-20 17:13:00',
            ),
            388 => 
            array (
                'id_mensajes' => 545,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Long ETHUSD Entrada:217.56000000 TP:222.91000000 SL:212.21000000Pips del TP:53.50000000 Pips SL:53.50000000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-20 20:00:00',
                'updated_at' => '2019-09-20 20:00:00',
            ),
            389 => 
            array (
                'id_mensajes' => 546,
                'titulo' => 'asd',
                'mensaje' => 'asdasd',
                'link_imagen' => NULL,
                'created_at' => '2019-09-21 00:04:00',
                'updated_at' => '2019-09-21 00:04:00',
            ),
            390 => 
            array (
                'id_mensajes' => 547,
                'titulo' => 'Profit4Life',
                'mensaje' => 'ETHUSD Se mueve stop loss a: 219.56000000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-21 01:03:00',
                'updated_at' => '2019-09-21 01:03:00',
            ),
            391 => 
            array (
                'id_mensajes' => 548,
                'titulo' => 'Profit4Life',
                'mensaje' => 'ETHUSD Se mueve stop loss a: 220.09000000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-21 01:03:00',
                'updated_at' => '2019-09-21 01:03:00',
            ),
            392 => 
            array (
                'id_mensajes' => 549,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Saliendo a ganacia en el par:ETHUSD',
                'link_imagen' => NULL,
                'created_at' => '2019-09-21 01:03:00',
                'updated_at' => '2019-09-21 01:03:00',
            ),
            393 => 
            array (
                'id_mensajes' => 550,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Short XRPUSD Entrada:0.29108000 TP:0.27825000 SL:0.29398000Pips del TP:120.00000000 Pips SL:37.30000000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-21 19:30:00',
                'updated_at' => '2019-09-21 19:30:00',
            ),
            394 => 
            array (
                'id_mensajes' => 551,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XRPUSD Se mueve stop loss a: 0.28520000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-22 01:17:00',
                'updated_at' => '2019-09-22 01:17:00',
            ),
            395 => 
            array (
                'id_mensajes' => 552,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XRPUSD Se mueve stop loss a: 0.28409000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-22 01:18:00',
                'updated_at' => '2019-09-22 01:18:00',
            ),
            396 => 
            array (
                'id_mensajes' => 553,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Saliendo a ganacia en el par:XRPUSD',
                'link_imagen' => NULL,
                'created_at' => '2019-09-22 01:19:00',
                'updated_at' => '2019-09-22 01:19:00',
            ),
            397 => 
            array (
                'id_mensajes' => 554,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Short ETHUSD Entrada:210.81000000 TP:205.23000000 SL:212.41000000Pips del TP:50.00000000 Pips SL:21.80000000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-22 11:00:00',
                'updated_at' => '2019-09-22 11:00:00',
            ),
            398 => 
            array (
                'id_mensajes' => 555,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Saliendo por stop loss en el par:ETHUSD',
                'link_imagen' => NULL,
                'created_at' => '2019-09-22 12:09:00',
                'updated_at' => '2019-09-22 12:09:00',
            ),
            399 => 
            array (
                'id_mensajes' => 556,
                'titulo' => 'Noticias',
                'mensaje' => 'Ordenes Pendientes',
                'link_imagen' => NULL,
                'created_at' => '2019-09-23 04:00:00',
                'updated_at' => '2019-09-23 04:00:00',
            ),
            400 => 
            array (
                'id_mensajes' => 558,
                'titulo' => 'Noticias',
                'mensaje' => 'Importante',
                'link_imagen' => NULL,
                'created_at' => '2019-09-23 05:54:00',
                'updated_at' => '2019-09-23 05:54:00',
            ),
            401 => 
            array (
                'id_mensajes' => 559,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Saliendo por stop loss en el par:GBPAUD',
                'link_imagen' => NULL,
                'created_at' => '2019-09-23 10:34:00',
                'updated_at' => '2019-09-23 10:34:00',
            ),
            402 => 
            array (
                'id_mensajes' => 560,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Long US30 Entrada:26958.34000000 TP:27108.34000000 SL:26820.67000000Pips del TP:1500.00000000 Pips SL:1376.70000000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-23 16:00:00',
                'updated_at' => '2019-09-23 16:00:00',
            ),
            403 => 
            array (
                'id_mensajes' => 561,
                'titulo' => 'Risk',
                'mensaje' => 'Recuerden ser cuid_mensajesadosos con las entradas en us30 el manejo del Riesgo es Muy importante.',
                'link_imagen' => NULL,
                'created_at' => '2019-09-23 16:02:00',
                'updated_at' => '2019-09-23 16:02:00',
            ),
            404 => 
            array (
                'id_mensajes' => 562,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Saliendo a ganacia en el par:US30',
                'link_imagen' => NULL,
                'created_at' => '2019-09-23 22:49:00',
                'updated_at' => '2019-09-23 22:49:00',
            ),
            405 => 
            array (
                'id_mensajes' => 563,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Short XRPUSD Entrada:0.26850000 TP:0.25570000 SL:0.27278000Pips del TP:120.00000000 Pips SL:50.80000000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-24 08:30:00',
                'updated_at' => '2019-09-24 08:30:00',
            ),
            406 => 
            array (
                'id_mensajes' => 564,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Short ZECUSD Entrada:44.21000000 TP:40.08000000 SL:45.91000000Pips del TP:40.00000000 Pips SL:18.30000000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-24 14:00:00',
                'updated_at' => '2019-09-24 14:00:00',
            ),
            407 => 
            array (
                'id_mensajes' => 565,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XRPUSD Se mueve stop loss a: 0.26152000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-24 14:21:00',
                'updated_at' => '2019-09-24 14:21:00',
            ),
            408 => 
            array (
                'id_mensajes' => 566,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XRPUSD Se mueve stop loss a: 0.26049000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-24 14:21:00',
                'updated_at' => '2019-09-24 14:21:00',
            ),
            409 => 
            array (
                'id_mensajes' => 567,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XRPUSD Se mueve stop loss a: 0.26042000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-24 14:21:00',
                'updated_at' => '2019-09-24 14:21:00',
            ),
            410 => 
            array (
                'id_mensajes' => 568,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Saliendo a ganacia en el par:XRPUSD',
                'link_imagen' => NULL,
                'created_at' => '2019-09-24 14:22:00',
                'updated_at' => '2019-09-24 14:22:00',
            ),
            411 => 
            array (
                'id_mensajes' => 569,
                'titulo' => 'Profit4Life',
                'mensaje' => 'ZECUSD Se mueve stop loss a: 43.38000000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-24 15:14:00',
                'updated_at' => '2019-09-24 15:14:00',
            ),
            412 => 
            array (
                'id_mensajes' => 570,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Saliendo a ganacia en el par:ZECUSD',
                'link_imagen' => NULL,
                'created_at' => '2019-09-24 15:17:00',
                'updated_at' => '2019-09-24 15:17:00',
            ),
            413 => 
            array (
                'id_mensajes' => 571,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Short ETHUSD Entrada:168.44000000 TP:159.96000000 SL:175.21000000Pips del TP:80.00000000 Pips SL:72.50000000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-25 04:30:00',
                'updated_at' => '2019-09-25 04:30:00',
            ),
            414 => 
            array (
                'id_mensajes' => 572,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Short ZECUSD Entrada:35.53000000 TP:31.28000000 SL:39.24000000Pips del TP:40.00000000 Pips SL:39.60000000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-25 04:30:00',
                'updated_at' => '2019-09-25 04:30:00',
            ),
            415 => 
            array (
                'id_mensajes' => 573,
                'titulo' => 'Profit4Life',
                'mensaje' => 'En la sección de Home podrán encontrar la Primera orden pendiente de',
                'link_imagen' => NULL,
                'created_at' => '2019-09-25 05:21:00',
                'updated_at' => '2019-09-25 05:21:00',
            ),
            416 => 
            array (
                'id_mensajes' => 574,
                'titulo' => 'Profit4Life',
                'mensaje' => 'ZECUSD Se mueve stop loss a: 34.42000000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-25 07:27:00',
                'updated_at' => '2019-09-25 07:27:00',
            ),
            417 => 
            array (
                'id_mensajes' => 575,
                'titulo' => 'Profit4Life',
                'mensaje' => 'ZECUSD Se mueve stop loss a: 34.12000000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-25 07:43:00',
                'updated_at' => '2019-09-25 07:43:00',
            ),
            418 => 
            array (
                'id_mensajes' => 576,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Saliendo a ganacia en el par:ZECUSD',
                'link_imagen' => NULL,
                'created_at' => '2019-09-25 07:44:00',
                'updated_at' => '2019-09-25 07:44:00',
            ),
            419 => 
            array (
                'id_mensajes' => 577,
                'titulo' => 'Profit4Life',
                'mensaje' => 'ETHUSD Se mueve stop loss a: 164.21000000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-25 08:02:00',
                'updated_at' => '2019-09-25 08:02:00',
            ),
            420 => 
            array (
                'id_mensajes' => 578,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Saliendo a ganacia en el par:ETHUSD',
                'link_imagen' => NULL,
                'created_at' => '2019-09-25 08:03:00',
                'updated_at' => '2019-09-25 08:03:00',
            ),
            421 => 
            array (
                'id_mensajes' => 579,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Long US30 Entrada:26949.34000000 TP:27164.51000000 SL:26734.17000000Pips del TP:2151.70000000 Pips SL:2151.70000000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-25 15:00:00',
                'updated_at' => '2019-09-25 15:00:00',
            ),
            422 => 
            array (
                'id_mensajes' => 580,
                'titulo' => 'profit4life',
                'mensaje' => 'La paciencia Paga',
                'link_imagen' => NULL,
                'created_at' => '2019-09-26 01:19:00',
                'updated_at' => '2019-09-26 01:19:00',
            ),
            423 => 
            array (
                'id_mensajes' => 581,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Short BTCUSD Entrada:8386.82000000 TP:7868.28000000 SL:8670.15000000Pips del TP:5000.00000000 Pips SL:3018.70000000',
                'link_imagen' => NULL,
                'created_at' => '2019-09-26 02:00:00',
                'updated_at' => '2019-09-26 02:00:00',
            ),
            424 => 
            array (
                'id_mensajes' => 582,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Saliendo por stop loss en el par:US30',
                'link_imagen' => NULL,
                'created_at' => '2019-09-26 06:57:00',
                'updated_at' => '2019-09-26 06:57:00',
            ),
            425 => 
            array (
                'id_mensajes' => 584,
                'titulo' => 'Orden Pendiente',
            'mensaje' => 'Nueva orden pendiente en su pestaña de Home 3)',
            'link_imagen' => NULL,
            'created_at' => '2019-09-26 08:05:00',
            'updated_at' => '2019-09-26 08:05:00',
        ),
        426 => 
        array (
            'id_mensajes' => 585,
            'titulo' => 'Profit4Life',
            'mensaje' => 'BTCUSD Se mueve stop loss a: 8311.58000000',
            'link_imagen' => NULL,
            'created_at' => '2019-09-26 13:32:00',
            'updated_at' => '2019-09-26 13:32:00',
        ),
        427 => 
        array (
            'id_mensajes' => 586,
            'titulo' => 'Profit4Life',
            'mensaje' => 'BTCUSD Se mueve stop loss a: 8242.21000000',
            'link_imagen' => NULL,
            'created_at' => '2019-09-26 13:32:00',
            'updated_at' => '2019-09-26 13:32:00',
        ),
        428 => 
        array (
            'id_mensajes' => 587,
            'titulo' => 'Profit4Life',
            'mensaje' => 'BTCUSD Se mueve stop loss a: 8248.71000000',
            'link_imagen' => NULL,
            'created_at' => '2019-09-26 13:32:00',
            'updated_at' => '2019-09-26 13:32:00',
        ),
        429 => 
        array (
            'id_mensajes' => 588,
            'titulo' => 'Profit4Life',
            'mensaje' => 'BTCUSD Se mueve stop loss a: 8256.22000000',
            'link_imagen' => NULL,
            'created_at' => '2019-09-26 13:32:00',
            'updated_at' => '2019-09-26 13:32:00',
        ),
        430 => 
        array (
            'id_mensajes' => 589,
            'titulo' => 'Profit4Life',
            'mensaje' => 'BTCUSD Se mueve stop loss a: 8261.33000000',
            'link_imagen' => NULL,
            'created_at' => '2019-09-26 13:32:00',
            'updated_at' => '2019-09-26 13:32:00',
        ),
        431 => 
        array (
            'id_mensajes' => 590,
            'titulo' => 'Profit4Life',
            'mensaje' => 'BTCUSD Se mueve stop loss a: 8249.61000000',
            'link_imagen' => NULL,
            'created_at' => '2019-09-26 13:32:00',
            'updated_at' => '2019-09-26 13:32:00',
        ),
        432 => 
        array (
            'id_mensajes' => 591,
            'titulo' => 'Profit4Life',
            'mensaje' => 'BTCUSD Se mueve stop loss a: 8235.40000000',
            'link_imagen' => NULL,
            'created_at' => '2019-09-26 13:32:00',
            'updated_at' => '2019-09-26 13:32:00',
        ),
        433 => 
        array (
            'id_mensajes' => 592,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Saliendo a ganacia en el par:BTCUSD',
            'link_imagen' => NULL,
            'created_at' => '2019-09-26 13:32:00',
            'updated_at' => '2019-09-26 13:32:00',
        ),
        434 => 
        array (
            'id_mensajes' => 593,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Long GBPNZD Entrada:1.95716000 TP:1.96716000 SL:1.95149000Pips del TP:100.00000000 Pips SL:56.70000000',
            'link_imagen' => NULL,
            'created_at' => '2019-09-26 15:00:00',
            'updated_at' => '2019-09-26 15:00:00',
        ),
        435 => 
        array (
            'id_mensajes' => 594,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Long US30 Entrada:26905.34000000 TP:27055.34000000 SL:26804.67000000Pips del TP:1500.00000000 Pips SL:1006.70000000',
            'link_imagen' => NULL,
            'created_at' => '2019-09-26 17:00:00',
            'updated_at' => '2019-09-26 17:00:00',
        ),
        436 => 
        array (
            'id_mensajes' => 595,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Saliendo a ganacia en el par:US30',
            'link_imagen' => NULL,
            'created_at' => '2019-09-26 19:33:00',
            'updated_at' => '2019-09-26 19:33:00',
        ),
        437 => 
        array (
            'id_mensajes' => 596,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Short XRPUSD Entrada:0.24240000 TP:0.22972000 SL:0.24671000Pips del TP:120.00000000 Pips SL:49.90000000',
            'link_imagen' => NULL,
            'created_at' => '2019-09-27 00:30:00',
            'updated_at' => '2019-09-27 00:30:00',
        ),
        438 => 
        array (
            'id_mensajes' => 597,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Short ETHUSD Entrada:165.09000000 TP:156.65000000 SL:168.13000000Pips del TP:80.00000000 Pips SL:34.80000000',
            'link_imagen' => NULL,
            'created_at' => '2019-09-27 01:00:00',
            'updated_at' => '2019-09-27 01:00:00',
        ),
        439 => 
        array (
            'id_mensajes' => 598,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Saliendo por stop loss en el par:GBPNZD',
            'link_imagen' => NULL,
            'created_at' => '2019-09-27 07:09:00',
            'updated_at' => '2019-09-27 07:09:00',
        ),
        440 => 
        array (
            'id_mensajes' => 599,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Saliendo por stop loss en el par:ETHUSD',
            'link_imagen' => NULL,
            'created_at' => '2019-09-27 07:37:00',
            'updated_at' => '2019-09-27 07:37:00',
        ),
        441 => 
        array (
            'id_mensajes' => 600,
            'titulo' => 'Profit4Life',
            'mensaje' => 'XRPUSD Se mueve stop loss a: 0.23672000',
            'link_imagen' => NULL,
            'created_at' => '2019-09-27 15:27:00',
            'updated_at' => '2019-09-27 15:27:00',
        ),
        442 => 
        array (
            'id_mensajes' => 601,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Saliendo a ganacia en el par:XRPUSD',
            'link_imagen' => NULL,
            'created_at' => '2019-09-27 15:28:00',
            'updated_at' => '2019-09-27 15:28:00',
        ),
        443 => 
        array (
            'id_mensajes' => 602,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Long GBPNZD Entrada:1.95590000 TP:1.96590000 SL:1.95057000Pips del TP:100.00000000 Pips SL:53.30000000',
            'link_imagen' => NULL,
            'created_at' => '2019-09-27 16:00:00',
            'updated_at' => '2019-09-27 16:00:00',
        ),
        444 => 
        array (
            'id_mensajes' => 603,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Long XAUUSD Entrada:1502.84000000 TP:1519.07000000 SL:1486.61000000Pips del TP:162.30000000 Pips SL:162.30000000',
            'link_imagen' => NULL,
            'created_at' => '2019-09-27 17:00:00',
            'updated_at' => '2019-09-27 17:00:00',
        ),
        445 => 
        array (
            'id_mensajes' => 604,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Long US30 Entrada:26817.74000000 TP:26967.74000000 SL:26712.17000000Pips del TP:1500.00000000 Pips SL:1055.70000000',
            'link_imagen' => NULL,
            'created_at' => '2019-09-27 20:00:00',
            'updated_at' => '2019-09-27 20:00:00',
        ),
        446 => 
        array (
            'id_mensajes' => 605,
            'titulo' => 'OrdenPendiente',
        'mensaje' => 'orden pendiente 4)USD/CHF cerrada con 50 pips de Ganancia TP Alcanzado',
        'link_imagen' => NULL,
        'created_at' => '2019-09-27 20:17:00',
        'updated_at' => '2019-09-27 20:17:00',
    ),
    447 => 
    array (
        'id_mensajes' => 606,
        'titulo' => 'Noticias',
        'mensaje' => 'USDCHF',
        'link_imagen' => NULL,
        'created_at' => '2019-09-27 20:19:00',
        'updated_at' => '2019-09-27 20:19:00',
    ),
    448 => 
    array (
        'id_mensajes' => 607,
        'titulo' => 'Ordenes pendientes',
        'mensaje' => 'recuerden las ordenes pendientes se encuentran en su pestaña de Home',
        'link_imagen' => NULL,
        'created_at' => '2019-09-27 20:26:00',
        'updated_at' => '2019-09-27 20:26:00',
    ),
    449 => 
    array (
        'id_mensajes' => 608,
        'titulo' => 'Profit4Life',
        'mensaje' => 'Long ETHUSD Entrada:170.44000000 TP:178.44000000 SL:164.57000000Pips del TP:80.00000000 Pips SL:58.70000000',
        'link_imagen' => NULL,
        'created_at' => '2019-09-29 17:00:00',
        'updated_at' => '2019-09-29 17:00:00',
    ),
    450 => 
    array (
        'id_mensajes' => 609,
        'titulo' => 'Atencion',
        'mensaje' => 'Hacer caso omiso a la anterior notificacion, FUE UN MENSAJE VACIO',
        'link_imagen' => NULL,
        'created_at' => '2019-09-29 21:51:00',
        'updated_at' => '2019-09-29 21:51:00',
    ),
    451 => 
    array (
        'id_mensajes' => 610,
        'titulo' => 'Profit4Life',
        'mensaje' => 'US30 Se mueve stop loss a: 26919.27000000',
        'link_imagen' => NULL,
        'created_at' => '2019-09-29 22:02:00',
        'updated_at' => '2019-09-29 22:02:00',
    ),
    452 => 
    array (
        'id_mensajes' => 611,
        'titulo' => 'Profit4Life',
        'mensaje' => 'Saliendo a ganacia en el par:US30',
        'link_imagen' => NULL,
        'created_at' => '2019-09-29 22:03:00',
        'updated_at' => '2019-09-29 22:03:00',
    ),
    453 => 
    array (
        'id_mensajes' => 612,
        'titulo' => 'Profit4Life',
        'mensaje' => 'Feliz Inicio de semana comenzando con 1000pips de ganancia en us30!!',
        'link_imagen' => NULL,
        'created_at' => '2019-09-29 22:12:00',
        'updated_at' => '2019-09-29 22:12:00',
    ),
    454 => 
    array (
        'id_mensajes' => 613,
        'titulo' => 'Profit4Life',
        'mensaje' => 'Recuerden siempre el manejo de riesgo!!',
        'link_imagen' => NULL,
        'created_at' => '2019-09-29 22:12:00',
        'updated_at' => '2019-09-29 22:12:00',
    ),
    455 => 
    array (
        'id_mensajes' => 614,
        'titulo' => 'Profit4Life',
        'mensaje' => 'Short ETHUSD Entrada:169.82000000 TP:161.42000000 SL:172.28000000Pips del TP:80.00000000 Pips SL:28.60000000',
        'link_imagen' => NULL,
        'created_at' => '2019-09-30 00:30:00',
        'updated_at' => '2019-09-30 00:30:00',
    ),
    456 => 
    array (
        'id_mensajes' => 615,
        'titulo' => 'Profit4Life',
        'mensaje' => 'Saliendo por stop loss en el par:ETHUSD',
        'link_imagen' => NULL,
        'created_at' => '2019-09-30 00:32:00',
        'updated_at' => '2019-09-30 00:32:00',
    ),
    457 => 
    array (
        'id_mensajes' => 616,
        'titulo' => 'Profit4Life',
        'mensaje' => 'GBPNZD Se mueve stop loss a: 1.96096000',
        'link_imagen' => NULL,
        'created_at' => '2019-09-30 01:26:00',
        'updated_at' => '2019-09-30 01:26:00',
    ),
    458 => 
    array (
        'id_mensajes' => 618,
        'titulo' => 'GBPNZD',
        'mensaje' => 'cerrar operación con Ganancias',
        'link_imagen' => NULL,
        'created_at' => '2019-09-30 01:29:00',
        'updated_at' => '2019-09-30 01:29:00',
    ),
    459 => 
    array (
        'id_mensajes' => 619,
        'titulo' => 'Profit4Life',
        'mensaje' => 'Saliendo por stop loss en el par:XAUUSD',
        'link_imagen' => NULL,
        'created_at' => '2019-09-30 09:10:00',
        'updated_at' => '2019-09-30 09:10:00',
    ),
    460 => 
    array (
        'id_mensajes' => 620,
        'titulo' => 'Profit4Life',
        'mensaje' => 'Saliendo por stop loss en el par:ETHUSD',
        'link_imagen' => NULL,
        'created_at' => '2019-09-30 09:49:00',
        'updated_at' => '2019-09-30 09:49:00',
    ),
    461 => 
    array (
        'id_mensajes' => 621,
        'titulo' => 'Profit4Life',
        'mensaje' => 'Long XRPUSD Entrada:0.25754000 TP:0.26954000 SL:0.25098000Pips del TP:120.00000000 Pips SL:65.60000000',
        'link_imagen' => NULL,
        'created_at' => '2019-09-30 22:00:00',
        'updated_at' => '2019-09-30 22:00:00',
    ),
    462 => 
    array (
        'id_mensajes' => 622,
        'titulo' => 'Profit4Life',
        'mensaje' => 'Long GBPNZD Entrada:1.96477000 TP:1.97477000 SL:1.96053000Pips del TP:100.00000000 Pips SL:42.40000000',
        'link_imagen' => NULL,
        'created_at' => '2019-09-30 22:00:00',
        'updated_at' => '2019-09-30 22:00:00',
    ),
    463 => 
    array (
        'id_mensajes' => 624,
        'titulo' => 'Profit4Life',
        'mensaje' => 'Long ETHUSD Entrada:179.27000000 TP:187.27000000 SL:176.23000000Pips del TP:80.00000000 Pips SL:30.40000000',
        'link_imagen' => NULL,
        'created_at' => '2019-09-30 23:30:00',
        'updated_at' => '2019-09-30 23:30:00',
    ),
    464 => 
    array (
        'id_mensajes' => 625,
        'titulo' => 'Profit4Life',
        'mensaje' => 'Long GBPAUD Entrada:1.82141000 TP:1.83641000 SL:1.81782000Pips del TP:150.00000000 Pips SL:35.90000000',
        'link_imagen' => NULL,
        'created_at' => '2019-10-01 02:00:00',
        'updated_at' => '2019-10-01 02:00:00',
    ),
    465 => 
    array (
        'id_mensajes' => 626,
        'titulo' => 'Profit4Life',
        'mensaje' => 'ETHUSD Se mueve stop loss a: 182.87000000',
        'link_imagen' => NULL,
        'created_at' => '2019-10-01 02:00:00',
        'updated_at' => '2019-10-01 02:00:00',
    ),
    466 => 
    array (
        'id_mensajes' => 627,
        'titulo' => 'Profit4Life',
        'mensaje' => 'Saliendo a ganacia en el par:ETHUSD',
        'link_imagen' => NULL,
        'created_at' => '2019-10-01 02:01:00',
        'updated_at' => '2019-10-01 02:01:00',
    ),
    467 => 
    array (
        'id_mensajes' => 628,
        'titulo' => 'Profit4Life',
        'mensaje' => 'Saliendo por stop loss en el par:GBPAUD',
        'link_imagen' => NULL,
        'created_at' => '2019-10-01 04:30:00',
        'updated_at' => '2019-10-01 04:30:00',
    ),
    468 => 
    array (
        'id_mensajes' => 629,
        'titulo' => 'Profit4Life',
        'mensaje' => 'Saliendo por stop loss en el par:GBPNZD',
        'link_imagen' => NULL,
        'created_at' => '2019-10-01 04:31:00',
        'updated_at' => '2019-10-01 04:31:00',
    ),
    469 => 
    array (
        'id_mensajes' => 630,
        'titulo' => 'Profit4Life',
        'mensaje' => 'Saliendo a ganacia en el par:XRPUSD',
        'link_imagen' => NULL,
        'created_at' => '2019-10-01 04:39:00',
        'updated_at' => '2019-10-01 04:39:00',
    ),
    470 => 
    array (
        'id_mensajes' => 631,
        'titulo' => 'Profit4life',
        'mensaje' => 'Es importante nunca olvid_mensajesen colocar su SL en momentos de alta volatilid_mensajesad protegen nuestras cuentas y nos aseguran un buen manejo del Riesgo',
        'link_imagen' => NULL,
        'created_at' => '2019-10-01 04:40:00',
        'updated_at' => '2019-10-01 04:40:00',
    ),
    471 => 
    array (
        'id_mensajes' => 632,
        'titulo' => 'Noticias',
    'mensaje' => '5)Usd/Chf Sell limit',
    'link_imagen' => NULL,
    'created_at' => '2019-10-01 07:15:00',
    'updated_at' => '2019-10-01 07:15:00',
),
472 => 
array (
    'id_mensajes' => 633,
    'titulo' => 'Profit4Life',
    'mensaje' => 'Long ETHUSD Entrada:181.42000000 TP:189.42000000 SL:178.09000000Pips del TP:80.00000000 Pips SL:33.30000000',
    'link_imagen' => NULL,
    'created_at' => '2019-10-01 11:30:00',
    'updated_at' => '2019-10-01 11:30:00',
),
473 => 
array (
    'id_mensajes' => 634,
    'titulo' => 'Profit4Life',
    'mensaje' => 'Saliendo por stop loss en el par:ETHUSD',
    'link_imagen' => NULL,
    'created_at' => '2019-10-01 13:04:00',
    'updated_at' => '2019-10-01 13:04:00',
),
474 => 
array (
    'id_mensajes' => 635,
    'titulo' => 'Profit4Life',
    'mensaje' => 'Long GBPNZD Entrada:1.97272000 TP:1.98578000 SL:1.95966000Pips del TP:130.60000000 Pips SL:130.60000000',
    'link_imagen' => NULL,
    'created_at' => '2019-10-01 17:00:00',
    'updated_at' => '2019-10-01 17:00:00',
),
475 => 
array (
    'id_mensajes' => 636,
    'titulo' => 'Profit4Life',
    'mensaje' => 'Long XRPUSD Entrada:0.24987000 TP:0.26187000 SL:0.24566000Pips del TP:120.00000000 Pips SL:42.10000000',
    'link_imagen' => NULL,
    'created_at' => '2019-10-01 20:00:00',
    'updated_at' => '2019-10-01 20:00:00',
),
476 => 
array (
    'id_mensajes' => 637,
    'titulo' => 'Profit4Life',
    'mensaje' => 'Short XAUUSD Entrada:1478.55000000 TP:1462.00000000 SL:1482.27000000Pips del TP:160.00000000 Pips SL:42.70000000',
    'link_imagen' => NULL,
    'created_at' => '2019-10-02 02:00:00',
    'updated_at' => '2019-10-02 02:00:00',
),
477 => 
array (
    'id_mensajes' => 638,
    'titulo' => 'Profit4Life',
    'mensaje' => 'Saliendo por stop loss en el par:XRPUSD',
    'link_imagen' => NULL,
    'created_at' => '2019-10-02 03:10:00',
    'updated_at' => '2019-10-02 03:10:00',
),
478 => 
array (
    'id_mensajes' => 639,
    'titulo' => 'orden pendiente GBP/USD',
    'mensaje' => 'pueden encontrar la nueva orden pendiente en su pestaña de Home',
    'link_imagen' => NULL,
    'created_at' => '2019-10-02 05:08:00',
    'updated_at' => '2019-10-02 05:08:00',
),
479 => 
array (
    'id_mensajes' => 640,
    'titulo' => 'Profit4Life',
    'mensaje' => 'Saliendo por stop loss en el par:XAUUSD',
    'link_imagen' => NULL,
    'created_at' => '2019-10-02 07:22:00',
    'updated_at' => '2019-10-02 07:22:00',
),
480 => 
array (
    'id_mensajes' => 641,
    'titulo' => 'Profit4Life',
    'mensaje' => 'Long ETHUSD Entrada:176.51000000 TP:184.51000000 SL:174.16000000Pips del TP:80.00000000 Pips SL:23.50000000',
    'link_imagen' => NULL,
    'created_at' => '2019-10-02 07:30:00',
    'updated_at' => '2019-10-02 07:30:00',
),
481 => 
array (
    'id_mensajes' => 642,
    'titulo' => 'GBPUSD',
    'mensaje' => 'eliminar orden, orden no activada',
    'link_imagen' => NULL,
    'created_at' => '2019-10-02 08:57:00',
    'updated_at' => '2019-10-02 08:57:00',
),
482 => 
array (
    'id_mensajes' => 643,
    'titulo' => 'Profit4life',
    'mensaje' => 'Nuestras últimas 4 entradas han llegado a SL esperamos recuperar y mejorar para el cierre de semana. Es importante el manejo del riesgo Recuerden nuestros resultados positivos son en base a promedios mensuales',
    'link_imagen' => NULL,
    'created_at' => '2019-10-02 09:01:00',
    'updated_at' => '2019-10-02 09:01:00',
),
483 => 
array (
    'id_mensajes' => 644,
    'titulo' => 'Profit4Life',
    'mensaje' => 'Short XAUUSD Entrada:1498.87000000 TP:1482.26000000 SL:1504.74000000Pips del TP:160.00000000 Pips SL:64.80000000',
    'link_imagen' => NULL,
    'created_at' => '2019-10-02 19:00:00',
    'updated_at' => '2019-10-02 19:00:00',
),
484 => 
array (
    'id_mensajes' => 645,
    'titulo' => 'Profit4Life',
    'mensaje' => 'Saliendo por stop loss en el par:GBPNZD',
    'link_imagen' => NULL,
    'created_at' => '2019-10-02 21:14:00',
    'updated_at' => '2019-10-02 21:14:00',
),
485 => 
array (
    'id_mensajes' => 646,
    'titulo' => 'Profit4Life',
    'mensaje' => 'Saliendo a ganacia en el par:ETHUSD',
    'link_imagen' => NULL,
    'created_at' => '2019-10-02 21:17:00',
    'updated_at' => '2019-10-02 21:17:00',
),
486 => 
array (
    'id_mensajes' => 647,
    'titulo' => 'Profit4Life',
    'mensaje' => 'Short XRPUSD Entrada:0.25177000 TP:0.23908000 SL:0.25450000Pips del TP:120.00000000 Pips SL:34.20000000',
    'link_imagen' => NULL,
    'created_at' => '2019-10-03 04:00:00',
    'updated_at' => '2019-10-03 04:00:00',
),
487 => 
array (
    'id_mensajes' => 648,
    'titulo' => 'Profit4Life',
    'mensaje' => 'Saliendo por stop loss en el par:XAUUSD',
    'link_imagen' => NULL,
    'created_at' => '2019-10-03 11:36:00',
    'updated_at' => '2019-10-03 11:36:00',
),
488 => 
array (
    'id_mensajes' => 649,
    'titulo' => 'Profit4Life',
    'mensaje' => 'Long GBPAUD Entrada:1.83363000 TP:1.84863000 SL:1.82790000Pips del TP:150.00000000 Pips SL:57.30000000',
    'link_imagen' => NULL,
    'created_at' => '2019-10-03 12:00:00',
    'updated_at' => '2019-10-03 12:00:00',
),
489 => 
array (
    'id_mensajes' => 650,
    'titulo' => 'Profit4Life',
    'mensaje' => 'Long GBPNZD Entrada:1.95950000 TP:1.96950000 SL:1.95342000Pips del TP:100.00000000 Pips SL:60.80000000',
    'link_imagen' => NULL,
    'created_at' => '2019-10-03 12:00:00',
    'updated_at' => '2019-10-03 12:00:00',
),
490 => 
array (
    'id_mensajes' => 651,
    'titulo' => 'Profit4Life',
    'mensaje' => 'GBPNZD Se mueve stop loss a: 1.96427000',
    'link_imagen' => NULL,
    'created_at' => '2019-10-03 12:29:00',
    'updated_at' => '2019-10-03 12:29:00',
),
491 => 
array (
    'id_mensajes' => 652,
    'titulo' => 'Profit4Life',
    'mensaje' => 'Saliendo a ganacia en el par:GBPNZD',
    'link_imagen' => NULL,
    'created_at' => '2019-10-03 12:30:00',
    'updated_at' => '2019-10-03 12:30:00',
),
492 => 
array (
    'id_mensajes' => 653,
    'titulo' => 'Profit4Life',
    'mensaje' => 'Saliendo a ganacia en el par:GBPAUD',
    'link_imagen' => NULL,
    'created_at' => '2019-10-03 12:46:00',
    'updated_at' => '2019-10-03 12:46:00',
),
493 => 
array (
    'id_mensajes' => 654,
    'titulo' => 'Profit4Life',
    'mensaje' => 'XRPUSD Se mueve stop loss a: 0.24520000',
    'link_imagen' => NULL,
    'created_at' => '2019-10-03 14:22:00',
    'updated_at' => '2019-10-03 14:22:00',
),
494 => 
array (
    'id_mensajes' => 655,
    'titulo' => 'Profit4Life',
    'mensaje' => 'XRPUSD Se mueve stop loss a: 0.24409000',
    'link_imagen' => NULL,
    'created_at' => '2019-10-03 14:22:00',
    'updated_at' => '2019-10-03 14:22:00',
),
495 => 
array (
    'id_mensajes' => 656,
    'titulo' => 'Profit4Life',
    'mensaje' => 'Saliendo a ganacia en el par:XRPUSD',
    'link_imagen' => NULL,
    'created_at' => '2019-10-03 14:22:00',
    'updated_at' => '2019-10-03 14:22:00',
),
496 => 
array (
    'id_mensajes' => 657,
    'titulo' => 'Profit4Life',
    'mensaje' => 'Short XAUUSD Entrada:1505.51000000 TP:1489.41000000 SL:1519.63000000Pips del TP:160.00000000 Pips SL:142.20000000',
    'link_imagen' => NULL,
    'created_at' => '2019-10-03 20:00:00',
    'updated_at' => '2019-10-03 20:00:00',
),
497 => 
array (
    'id_mensajes' => 658,
    'titulo' => 'Profit4Life',
    'mensaje' => 'Short XRPUSD Entrada:0.24697000 TP:0.23430000 SL:0.25007000Pips del TP:120.00000000 Pips SL:37.70000000',
    'link_imagen' => NULL,
    'created_at' => '2019-10-04 05:00:00',
    'updated_at' => '2019-10-04 05:00:00',
),
498 => 
array (
    'id_mensajes' => 659,
    'titulo' => 'Noticias',
    'mensaje' => 'Usd/Cad Sell Stop',
    'link_imagen' => NULL,
    'created_at' => '2019-10-04 05:04:00',
    'updated_at' => '2019-10-04 05:04:00',
),
499 => 
array (
    'id_mensajes' => 660,
    'titulo' => 'Profit4Life',
    'mensaje' => 'Saliendo por stop loss en el par:XRPUSD',
    'link_imagen' => NULL,
    'created_at' => '2019-10-04 07:23:00',
    'updated_at' => '2019-10-04 07:23:00',
),
));
        \DB::table('mensajes')->insert(array (
            0 => 
            array (
                'id_mensajes' => 661,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1498.41000000',
                'link_imagen' => NULL,
                'created_at' => '2019-10-04 12:59:00',
                'updated_at' => '2019-10-04 12:59:00',
            ),
            1 => 
            array (
                'id_mensajes' => 662,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1497.26000000',
                'link_imagen' => NULL,
                'created_at' => '2019-10-04 12:59:00',
                'updated_at' => '2019-10-04 12:59:00',
            ),
            2 => 
            array (
                'id_mensajes' => 663,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1497.34000000',
                'link_imagen' => NULL,
                'created_at' => '2019-10-04 12:59:00',
                'updated_at' => '2019-10-04 12:59:00',
            ),
            3 => 
            array (
                'id_mensajes' => 664,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1497.41000000',
                'link_imagen' => NULL,
                'created_at' => '2019-10-04 12:59:00',
                'updated_at' => '2019-10-04 12:59:00',
            ),
            4 => 
            array (
                'id_mensajes' => 665,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1497.38000000',
                'link_imagen' => NULL,
                'created_at' => '2019-10-04 12:59:00',
                'updated_at' => '2019-10-04 12:59:00',
            ),
            5 => 
            array (
                'id_mensajes' => 666,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Saliendo a ganacia en el par:XAUUSD',
                'link_imagen' => NULL,
                'created_at' => '2019-10-04 13:00:00',
                'updated_at' => '2019-10-04 13:00:00',
            ),
            6 => 
            array (
                'id_mensajes' => 667,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Long GBPNZD Entrada:1.94961000 TP:1.95961000 SL:1.94040000Pips del TP:100.00000000 Pips SL:92.10000000',
                'link_imagen' => NULL,
                'created_at' => '2019-10-04 16:00:00',
                'updated_at' => '2019-10-04 16:00:00',
            ),
            7 => 
            array (
                'id_mensajes' => 668,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Saliendo a ganacia en el par:GBPNZD',
                'link_imagen' => NULL,
                'created_at' => '2019-10-04 19:41:00',
                'updated_at' => '2019-10-04 19:41:00',
            ),
            8 => 
            array (
                'id_mensajes' => 669,
                'titulo' => 'Profitcalls',
                'mensaje' => 'cerrando extra 35 pips con Gbpnzd, Disfruten su fin de Semana',
                'link_imagen' => NULL,
                'created_at' => '2019-10-04 19:42:00',
                'updated_at' => '2019-10-04 19:42:00',
            ),
            9 => 
            array (
                'id_mensajes' => 670,
                'titulo' => 'Noticias',
                'mensaje' => 'USD/Cad',
                'link_imagen' => NULL,
                'created_at' => '2019-10-04 20:51:00',
                'updated_at' => '2019-10-04 20:51:00',
            ),
            10 => 
            array (
                'id_mensajes' => 671,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Long GBPAUD Entrada:1.82248000 TP:1.83748000 SL:1.81510000Pips del TP:150.00000000 Pips SL:73.80000000',
                'link_imagen' => NULL,
                'created_at' => '2019-10-06 22:00:00',
                'updated_at' => '2019-10-06 22:00:00',
            ),
            11 => 
            array (
                'id_mensajes' => 672,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Short US30 Entrada:26476.74000000 TP:26322.27000000 SL:26589.67000000Pips del TP:1500.00000000 Pips SL:1174.00000000',
                'link_imagen' => NULL,
                'created_at' => '2019-10-07 00:00:00',
                'updated_at' => '2019-10-07 00:00:00',
            ),
            12 => 
            array (
                'id_mensajes' => 673,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Short XAUUSD Entrada:1505.87000000 TP:1489.77000000 SL:1509.33000000Pips del TP:160.00000000 Pips SL:35.60000000',
                'link_imagen' => NULL,
                'created_at' => '2019-10-07 05:00:00',
                'updated_at' => '2019-10-07 05:00:00',
            ),
            13 => 
            array (
                'id_mensajes' => 674,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Short GBPNZD Entrada:1.95005000 TP:1.93957000 SL:1.95470000Pips del TP:100.00000000 Pips SL:51.30000000',
                'link_imagen' => NULL,
                'created_at' => '2019-10-07 06:00:00',
                'updated_at' => '2019-10-07 06:00:00',
            ),
            14 => 
            array (
                'id_mensajes' => 675,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Saliendo por stop loss en el par:GBPNZD',
                'link_imagen' => NULL,
                'created_at' => '2019-10-07 08:40:00',
                'updated_at' => '2019-10-07 08:40:00',
            ),
            15 => 
            array (
                'id_mensajes' => 676,
                'titulo' => 'Profit4Life',
                'mensaje' => 'GBPAUD Se mueve stop loss a: 1.82899000',
                'link_imagen' => NULL,
                'created_at' => '2019-10-07 10:44:00',
                'updated_at' => '2019-10-07 10:44:00',
            ),
            16 => 
            array (
                'id_mensajes' => 677,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Saliendo a ganacia en el par:GBPAUD',
                'link_imagen' => NULL,
                'created_at' => '2019-10-07 10:46:00',
                'updated_at' => '2019-10-07 10:46:00',
            ),
            17 => 
            array (
                'id_mensajes' => 678,
                'titulo' => 'Profit4Life',
                'mensaje' => 'XAUUSD Se mueve stop loss a: 1498.57000000',
                'link_imagen' => NULL,
                'created_at' => '2019-10-07 11:06:00',
                'updated_at' => '2019-10-07 11:06:00',
            ),
            18 => 
            array (
                'id_mensajes' => 679,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Saliendo a ganacia en el par:XAUUSD',
                'link_imagen' => NULL,
                'created_at' => '2019-10-07 11:07:00',
                'updated_at' => '2019-10-07 11:07:00',
            ),
            19 => 
            array (
                'id_mensajes' => 680,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Saliendo por stop loss en el par:US30',
                'link_imagen' => NULL,
                'created_at' => '2019-10-07 15:35:00',
                'updated_at' => '2019-10-07 15:35:00',
            ),
            20 => 
            array (
                'id_mensajes' => 681,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Short GBPNZD Entrada:1.95458000 TP:1.94414000 SL:1.96171000Pips del TP:100.00000000 Pips SL:75.70000000',
                'link_imagen' => NULL,
                'created_at' => '2019-10-07 18:00:00',
                'updated_at' => '2019-10-07 18:00:00',
            ),
            21 => 
            array (
                'id_mensajes' => 682,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Short US30 Entrada:26524.34000000 TP:26372.67000000 SL:26656.17000000Pips del TP:1500.00000000 Pips SL:1335.00000000',
                'link_imagen' => NULL,
                'created_at' => '2019-10-07 19:00:00',
                'updated_at' => '2019-10-07 19:00:00',
            ),
            22 => 
            array (
                'id_mensajes' => 683,
                'titulo' => 'Profit4Life',
                'mensaje' => 'GBPNZD Se mueve stop loss a: 1.94957000',
                'link_imagen' => NULL,
                'created_at' => '2019-10-08 01:53:00',
                'updated_at' => '2019-10-08 01:53:00',
            ),
            23 => 
            array (
                'id_mensajes' => 684,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Saliendo a ganacia en el par:GBPNZD',
                'link_imagen' => NULL,
                'created_at' => '2019-10-08 01:55:00',
                'updated_at' => '2019-10-08 01:55:00',
            ),
            24 => 
            array (
                'id_mensajes' => 685,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Long GBPAUD Entrada:1.82146000 TP:1.83646000 SL:1.81689000Pips del TP:150.00000000 Pips SL:45.70000000',
                'link_imagen' => NULL,
                'created_at' => '2019-10-08 08:00:00',
                'updated_at' => '2019-10-08 08:00:00',
            ),
            25 => 
            array (
                'id_mensajes' => 686,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Saliendo por stop loss en el par:GBPAUD',
                'link_imagen' => NULL,
                'created_at' => '2019-10-08 08:25:00',
                'updated_at' => '2019-10-08 08:25:00',
            ),
            26 => 
            array (
                'id_mensajes' => 687,
                'titulo' => 'Profit4Life',
                'mensaje' => 'US30 Se mueve stop loss a: 26427.84000000',
                'link_imagen' => NULL,
                'created_at' => '2019-10-08 09:30:00',
                'updated_at' => '2019-10-08 09:30:00',
            ),
            27 => 
            array (
                'id_mensajes' => 688,
                'titulo' => 'Profit4Life',
                'mensaje' => 'US30 Se mueve stop loss a: 26418.84000000',
                'link_imagen' => NULL,
                'created_at' => '2019-10-08 09:30:00',
                'updated_at' => '2019-10-08 09:30:00',
            ),
            28 => 
            array (
                'id_mensajes' => 689,
                'titulo' => 'Profit4Life',
                'mensaje' => 'US30 Se mueve stop loss a: 26410.84000000',
                'link_imagen' => NULL,
                'created_at' => '2019-10-08 09:30:00',
                'updated_at' => '2019-10-08 09:30:00',
            ),
            29 => 
            array (
                'id_mensajes' => 690,
                'titulo' => 'Profit4Life',
                'mensaje' => 'US30 Se mueve stop loss a: 26405.84000000',
                'link_imagen' => NULL,
                'created_at' => '2019-10-08 09:30:00',
                'updated_at' => '2019-10-08 09:30:00',
            ),
            30 => 
            array (
                'id_mensajes' => 691,
                'titulo' => 'Profit4Life',
                'mensaje' => 'US30 Se mueve stop loss a: 26405.84000000',
                'link_imagen' => NULL,
                'created_at' => '2019-10-08 09:30:00',
                'updated_at' => '2019-10-08 09:30:00',
            ),
            31 => 
            array (
                'id_mensajes' => 692,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Saliendo a ganacia en el par:US30',
                'link_imagen' => NULL,
                'created_at' => '2019-10-08 09:31:00',
                'updated_at' => '2019-10-08 09:31:00',
            ),
            32 => 
            array (
                'id_mensajes' => 693,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Short XAUUSD Entrada:1502.11000000 TP:1486.01000000 SL:1508.78000000Pips del TP:160.00000000 Pips SL:67.70000000',
                'link_imagen' => NULL,
                'created_at' => '2019-10-08 15:00:00',
                'updated_at' => '2019-10-08 15:00:00',
            ),
            33 => 
            array (
                'id_mensajes' => 694,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Saliendo por stop loss en el par:XAUUSD',
                'link_imagen' => NULL,
                'created_at' => '2019-10-08 18:58:00',
                'updated_at' => '2019-10-08 18:58:00',
            ),
            34 => 
            array (
                'id_mensajes' => 695,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Short XAUUSD Entrada:1504.95000000 TP:1488.76000000 SL:1509.31000000Pips del TP:160.00000000 Pips SL:45.50000000',
                'link_imagen' => NULL,
                'created_at' => '2019-10-09 17:00:00',
                'updated_at' => '2019-10-09 17:00:00',
            ),
            35 => 
            array (
                'id_mensajes' => 696,
                'titulo' => 'Profit4Life',
                'mensaje' => 'Saliendo por stop loss en el par:XAUUSD',
                'link_imagen' => NULL,
                'created_at' => '2019-10-09 19:43:00',
                'updated_at' => '2019-10-09 19:43:00',
            ),
            36 => 
            array (
                'id_mensajes' => 697,
                'titulo' => 'Noticias',
            'mensaje' => '8)Eur/Usd',
            'link_imagen' => NULL,
            'created_at' => '2019-10-10 01:33:00',
            'updated_at' => '2019-10-10 01:33:00',
        ),
        37 => 
        array (
            'id_mensajes' => 698,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Short GBPNZD Entrada:1.93729000 TP:1.92706000 SL:1.94552000Pips del TP:100.00000000 Pips SL:84.60000000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-10 03:00:00',
            'updated_at' => '2019-10-10 03:00:00',
        ),
        38 => 
        array (
            'id_mensajes' => 699,
            'titulo' => 'Profit4Life',
            'mensaje' => 'GBPNZD Se mueve stop loss a: 1.93238000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-10 12:07:00',
            'updated_at' => '2019-10-10 12:07:00',
        ),
        39 => 
        array (
            'id_mensajes' => 700,
            'titulo' => 'Profit4Life',
            'mensaje' => 'GBPNZD Se mueve stop loss a: 1.93179000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-10 12:07:00',
            'updated_at' => '2019-10-10 12:07:00',
        ),
        40 => 
        array (
            'id_mensajes' => 701,
            'titulo' => 'Profit4Life',
            'mensaje' => 'GBPNZD Se mueve stop loss a: 1.93179000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-10 12:07:00',
            'updated_at' => '2019-10-10 12:07:00',
        ),
        41 => 
        array (
            'id_mensajes' => 702,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Saliendo a ganacia en el par:GBPNZD',
            'link_imagen' => NULL,
            'created_at' => '2019-10-10 12:09:00',
            'updated_at' => '2019-10-10 12:09:00',
        ),
        42 => 
        array (
            'id_mensajes' => 703,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Short XAUUSD Entrada:1495.24000000 TP:1479.14000000 SL:1503.21000000Pips del TP:160.00000000 Pips SL:80.70000000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-11 10:00:00',
            'updated_at' => '2019-10-11 10:00:00',
        ),
        43 => 
        array (
            'id_mensajes' => 704,
            'titulo' => 'Profit4Life',
            'mensaje' => 'XAUUSD Se mueve stop loss a: 1488.00000000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-11 11:34:00',
            'updated_at' => '2019-10-11 11:34:00',
        ),
        44 => 
        array (
            'id_mensajes' => 705,
            'titulo' => 'Profit4Life',
            'mensaje' => 'XAUUSD Se mueve stop loss a: 1486.95000000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-11 11:39:00',
            'updated_at' => '2019-10-11 11:39:00',
        ),
        45 => 
        array (
            'id_mensajes' => 706,
            'titulo' => 'Profit4Life',
            'mensaje' => 'XAUUSD Se mueve stop loss a: 1486.98000000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-11 11:39:00',
            'updated_at' => '2019-10-11 11:39:00',
        ),
        46 => 
        array (
            'id_mensajes' => 707,
            'titulo' => 'Profit4Life',
            'mensaje' => 'XAUUSD Se mueve stop loss a: 1486.99000000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-11 11:39:00',
            'updated_at' => '2019-10-11 11:39:00',
        ),
        47 => 
        array (
            'id_mensajes' => 708,
            'titulo' => 'Profit4Life',
            'mensaje' => 'XAUUSD Se mueve stop loss a: 1486.92000000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-11 11:39:00',
            'updated_at' => '2019-10-11 11:39:00',
        ),
        48 => 
        array (
            'id_mensajes' => 709,
            'titulo' => 'Profit4Life',
            'mensaje' => 'XAUUSD Se mueve stop loss a: 1486.94000000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-11 11:39:00',
            'updated_at' => '2019-10-11 11:39:00',
        ),
        49 => 
        array (
            'id_mensajes' => 710,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Saliendo a ganacia en el par:XAUUSD',
            'link_imagen' => NULL,
            'created_at' => '2019-10-11 11:40:00',
            'updated_at' => '2019-10-11 11:40:00',
        ),
        50 => 
        array (
            'id_mensajes' => 711,
            'titulo' => 'Profitcall',
            'mensaje' => 'Feliz fin de semana para todos, seguimos trabajando en actualizaciones y mejoras para la App y los Profitcalls que a todos les encantarán y mejorarán los resultados',
            'link_imagen' => NULL,
            'created_at' => '2019-10-12 01:29:00',
            'updated_at' => '2019-10-12 01:29:00',
        ),
        51 => 
        array (
            'id_mensajes' => 712,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Short GBPAUD Entrada:1.86154000 TP:1.84455000 SL:1.86475000Pips del TP:150.00000000 Pips SL:52.00000000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-13 21:05:00',
            'updated_at' => '2019-10-13 21:05:00',
        ),
        52 => 
        array (
            'id_mensajes' => 713,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Saliendo por stop loss en el par:GBPAUD',
            'link_imagen' => NULL,
            'created_at' => '2019-10-14 13:48:00',
            'updated_at' => '2019-10-14 13:48:00',
        ),
        53 => 
        array (
            'id_mensajes' => 714,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Long XRPUSD Entrada:0.29094000 TP:0.30294000 SL:0.28613000Pips del TP:120.00000000 Pips SL:48.10000000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-14 14:00:00',
            'updated_at' => '2019-10-14 14:00:00',
        ),
        54 => 
        array (
            'id_mensajes' => 715,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Short GBPNZD Entrada:1.99456000 TP:1.97560000 SL:2.01288000Pips del TP:186.40000000 Pips SL:186.40000000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-14 20:00:00',
            'updated_at' => '2019-10-14 20:00:00',
        ),
        55 => 
        array (
            'id_mensajes' => 716,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Long GBPAUD Entrada:1.86245000 TP:1.87745000 SL:1.85241000Pips del TP:150.00000000 Pips SL:100.40000000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-14 21:05:00',
            'updated_at' => '2019-10-14 21:05:00',
        ),
        56 => 
        array (
            'id_mensajes' => 717,
            'titulo' => 'Profit4Life',
            'mensaje' => 'XRPUSD Se mueve stop loss a: 0.29595000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-14 23:29:00',
            'updated_at' => '2019-10-14 23:29:00',
        ),
        57 => 
        array (
            'id_mensajes' => 718,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Saliendo a ganacia en el par:XRPUSD',
            'link_imagen' => NULL,
            'created_at' => '2019-10-15 00:07:00',
            'updated_at' => '2019-10-15 00:07:00',
        ),
        58 => 
        array (
            'id_mensajes' => 719,
            'titulo' => 'Profit4Life',
            'mensaje' => 'GBPAUD Se mueve stop loss a: 1.86915000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-15 06:30:00',
            'updated_at' => '2019-10-15 06:30:00',
        ),
        59 => 
        array (
            'id_mensajes' => 720,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Saliendo por stop loss en el par:GBPNZD',
            'link_imagen' => NULL,
            'created_at' => '2019-10-15 06:31:00',
            'updated_at' => '2019-10-15 06:31:00',
        ),
        60 => 
        array (
            'id_mensajes' => 721,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Saliendo a ganacia en el par:GBPAUD',
            'link_imagen' => NULL,
            'created_at' => '2019-10-15 06:33:00',
            'updated_at' => '2019-10-15 06:33:00',
        ),
        61 => 
        array (
            'id_mensajes' => 722,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Long XRPUSD Entrada:0.29463000 TP:0.30663000 SL:0.28834000Pips del TP:120.00000000 Pips SL:62.90000000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-15 12:30:00',
            'updated_at' => '2019-10-15 12:30:00',
        ),
        62 => 
        array (
            'id_mensajes' => 723,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Saliendo por stop loss en el par:XRPUSD',
            'link_imagen' => NULL,
            'created_at' => '2019-10-15 16:13:00',
            'updated_at' => '2019-10-15 16:13:00',
        ),
        63 => 
        array (
            'id_mensajes' => 724,
            'titulo' => 'Noticias',
            'mensaje' => 'Criptomonedas',
            'link_imagen' => NULL,
            'created_at' => '2019-10-15 17:52:00',
            'updated_at' => '2019-10-15 17:52:00',
        ),
        64 => 
        array (
            'id_mensajes' => 725,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Long GBPNZD Entrada:2.03122000 TP:2.04441000 SL:2.01803000Pips del TP:131.90000000 Pips SL:131.90000000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-16 09:00:00',
            'updated_at' => '2019-10-16 09:00:00',
        ),
        65 => 
        array (
            'id_mensajes' => 726,
            'titulo' => 'Profit4Life',
            'mensaje' => 'GBPNZD Se mueve stop loss a: 2.03584000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-16 09:56:00',
            'updated_at' => '2019-10-16 09:56:00',
        ),
        66 => 
        array (
            'id_mensajes' => 727,
            'titulo' => 'Profit4Life',
            'mensaje' => 'GBPNZD Se mueve stop loss a: 2.03651000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-16 09:56:00',
            'updated_at' => '2019-10-16 09:56:00',
        ),
        67 => 
        array (
            'id_mensajes' => 728,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Saliendo a ganacia en el par:GBPNZD',
            'link_imagen' => NULL,
            'created_at' => '2019-10-16 09:57:00',
            'updated_at' => '2019-10-16 09:57:00',
        ),
        68 => 
        array (
            'id_mensajes' => 729,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Long XRPUSD Entrada:0.28482000 TP:0.29682000 SL:0.27888000Pips del TP:120.00000000 Pips SL:59.40000000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-16 16:30:00',
            'updated_at' => '2019-10-16 16:30:00',
        ),
        69 => 
        array (
            'id_mensajes' => 730,
            'titulo' => 'Noticias',
            'mensaje' => 'ProfitAdvice',
            'link_imagen' => NULL,
            'created_at' => '2019-10-16 17:36:00',
            'updated_at' => '2019-10-16 17:36:00',
        ),
        70 => 
        array (
            'id_mensajes' => 731,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Saliendo a ganacia en el par:XRPUSD',
            'link_imagen' => NULL,
            'created_at' => '2019-10-16 23:19:00',
            'updated_at' => '2019-10-16 23:19:00',
        ),
        71 => 
        array (
            'id_mensajes' => 732,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Short XRPUSD Entrada:0.29861000 TP:0.28569000 SL:0.30409000Pips del TP:120.00000000 Pips SL:64.00000000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-17 17:00:00',
            'updated_at' => '2019-10-17 17:00:00',
        ),
        72 => 
        array (
            'id_mensajes' => 733,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Short ETHUSD Entrada:177.65000000 TP:169.19000000 SL:179.58000000Pips del TP:80.00000000 Pips SL:23.90000000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-17 19:30:00',
            'updated_at' => '2019-10-17 19:30:00',
        ),
        73 => 
        array (
            'id_mensajes' => 734,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Long GBPNZD Entrada:2.03197000 TP:2.04621000 SL:2.01773000Pips del TP:142.40000000 Pips SL:142.40000000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-17 21:05:00',
            'updated_at' => '2019-10-17 21:05:00',
        ),
        74 => 
        array (
            'id_mensajes' => 735,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Saliendo por stop loss en el par:XRPUSD',
            'link_imagen' => NULL,
            'created_at' => '2019-10-17 23:15:00',
            'updated_at' => '2019-10-17 23:15:00',
        ),
        75 => 
        array (
            'id_mensajes' => 736,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Saliendo por stop loss en el par:GBPNZD',
            'link_imagen' => NULL,
            'created_at' => '2019-10-18 01:21:00',
            'updated_at' => '2019-10-18 01:21:00',
        ),
        76 => 
        array (
            'id_mensajes' => 737,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Long GBPNZD Entrada:2.02190000 TP:2.03190000 SL:2.01574000Pips del TP:100.00000000 Pips SL:61.60000000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-18 07:00:00',
            'updated_at' => '2019-10-18 07:00:00',
        ),
        77 => 
        array (
            'id_mensajes' => 738,
            'titulo' => 'Profit4Life',
            'mensaje' => 'ETHUSD Se mueve stop loss a: 174.10000000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-18 08:04:00',
            'updated_at' => '2019-10-18 08:04:00',
        ),
        78 => 
        array (
            'id_mensajes' => 739,
            'titulo' => 'Profit4Life',
            'mensaje' => 'ETHUSD Se mueve stop loss a: 173.47000000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-18 08:07:00',
            'updated_at' => '2019-10-18 08:07:00',
        ),
        79 => 
        array (
            'id_mensajes' => 740,
            'titulo' => 'Profit4Life',
            'mensaje' => 'ETHUSD Se mueve stop loss a: 173.47000000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-18 08:07:00',
            'updated_at' => '2019-10-18 08:07:00',
        ),
        80 => 
        array (
            'id_mensajes' => 741,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Saliendo a ganacia en el par:ETHUSD',
            'link_imagen' => NULL,
            'created_at' => '2019-10-18 08:08:00',
            'updated_at' => '2019-10-18 08:08:00',
        ),
        81 => 
        array (
            'id_mensajes' => 742,
            'titulo' => 'Profit4Life',
            'mensaje' => 'GBPNZD Se mueve stop loss a: 2.02646000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-18 08:36:00',
            'updated_at' => '2019-10-18 08:36:00',
        ),
        82 => 
        array (
            'id_mensajes' => 743,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Saliendo a ganacia en el par:GBPNZD',
            'link_imagen' => NULL,
            'created_at' => '2019-10-18 08:37:00',
            'updated_at' => '2019-10-18 08:37:00',
        ),
        83 => 
        array (
            'id_mensajes' => 744,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Short XRPUSD Entrada:0.28976000 TP:0.27710000 SL:0.29188000Pips del TP:120.00000000 Pips SL:27.80000000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-20 10:30:00',
            'updated_at' => '2019-10-20 10:30:00',
        ),
        84 => 
        array (
            'id_mensajes' => 745,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Saliendo por stop loss en el par:XRPUSD',
            'link_imagen' => NULL,
            'created_at' => '2019-10-20 14:12:00',
            'updated_at' => '2019-10-20 14:12:00',
        ),
        85 => 
        array (
            'id_mensajes' => 746,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Long US30 Entrada:26849.74000000 TP:26999.74000000 SL:26755.77000000Pips del TP:1500.00000000 Pips SL:939.70000000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-21 01:00:00',
            'updated_at' => '2019-10-21 01:00:00',
        ),
        86 => 
        array (
            'id_mensajes' => 747,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Long GBPNZD Entrada:2.02345000 TP:2.03345000 SL:2.01530000Pips del TP:100.00000000 Pips SL:81.50000000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-21 07:00:00',
            'updated_at' => '2019-10-21 07:00:00',
        ),
        87 => 
        array (
            'id_mensajes' => 748,
            'titulo' => 'Profit4Life',
            'mensaje' => 'GBPNZD Se mueve stop loss a: 2.02796000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-21 08:13:00',
            'updated_at' => '2019-10-21 08:13:00',
        ),
        88 => 
        array (
            'id_mensajes' => 749,
            'titulo' => 'Profit4Life',
            'mensaje' => 'GBPNZD Se mueve stop loss a: 2.02848000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-21 08:14:00',
            'updated_at' => '2019-10-21 08:14:00',
        ),
        89 => 
        array (
            'id_mensajes' => 750,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Saliendo a ganacia en el par:GBPNZD',
            'link_imagen' => NULL,
            'created_at' => '2019-10-21 08:15:00',
            'updated_at' => '2019-10-21 08:15:00',
        ),
        90 => 
        array (
            'id_mensajes' => 751,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Saliendo por stop loss en el par:US30',
            'link_imagen' => NULL,
            'created_at' => '2019-10-21 14:06:00',
            'updated_at' => '2019-10-21 14:06:00',
        ),
        91 => 
        array (
            'id_mensajes' => 752,
            'titulo' => 'Profit4Life',
            'mensaje' => 'GBPNZD Se mueve stop loss a: 2.02506000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-22 18:19:00',
            'updated_at' => '2019-10-22 18:19:00',
        ),
        92 => 
        array (
            'id_mensajes' => 753,
            'titulo' => 'Profit4Life',
            'mensaje' => 'GBPNZD Se mueve stop loss a: 2.02568000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-22 18:19:00',
            'updated_at' => '2019-10-22 18:19:00',
        ),
        93 => 
        array (
            'id_mensajes' => 754,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Saliendo a ganacia en el par:GBPNZD',
            'link_imagen' => NULL,
            'created_at' => '2019-10-22 18:20:00',
            'updated_at' => '2019-10-22 18:20:00',
        ),
        94 => 
        array (
            'id_mensajes' => 755,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Long GBPNZD Entrada:2.01246000 TP:2.02246000 SL:2.00800000Pips del TP:100.00000000 Pips SL:44.60000000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-22 23:00:00',
            'updated_at' => '2019-10-22 23:00:00',
        ),
        95 => 
        array (
            'id_mensajes' => 756,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Saliendo por stop loss en el par:GBPNZD',
            'link_imagen' => NULL,
            'created_at' => '2019-10-23 00:28:00',
            'updated_at' => '2019-10-23 00:28:00',
        ),
        96 => 
        array (
            'id_mensajes' => 757,
            'titulo' => 'GBPUSD',
            'mensaje' => 'Activando GBPUSD próximamente posibles entradas en Venta.',
            'link_imagen' => NULL,
            'created_at' => '2019-10-23 21:33:00',
            'updated_at' => '2019-10-23 21:33:00',
        ),
        97 => 
        array (
            'id_mensajes' => 758,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Short ETHUSD Entrada:161.09000000 TP:152.70000000 SL:163.25000000Pips del TP:80.00000000 Pips SL:25.50000000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-24 02:30:00',
            'updated_at' => '2019-10-24 02:30:00',
        ),
        98 => 
        array (
            'id_mensajes' => 759,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Short ETHUSD Entrada:161.50000000 TP:153.05000000 SL:163.25000000Pips del TP:80.00000000 Pips SL:22.00000000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-24 06:00:00',
            'updated_at' => '2019-10-24 06:00:00',
        ),
        99 => 
        array (
            'id_mensajes' => 760,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Saliendo por stop loss en el par:ETHUSD',
            'link_imagen' => NULL,
            'created_at' => '2019-10-24 14:02:00',
            'updated_at' => '2019-10-24 14:02:00',
        ),
        100 => 
        array (
            'id_mensajes' => 761,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Long GBPAUD Entrada:1.88591000 TP:1.90091000 SL:1.87685000Pips del TP:150.00000000 Pips SL:90.60000000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-24 19:00:00',
            'updated_at' => '2019-10-24 19:00:00',
        ),
        101 => 
        array (
            'id_mensajes' => 762,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Long GBPNZD Entrada:2.01695000 TP:2.03495000 SL:2.00576000Pips del TP:180.00000000 Pips SL:111.90000000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-24 19:00:00',
            'updated_at' => '2019-10-24 19:00:00',
        ),
        102 => 
        array (
            'id_mensajes' => 763,
            'titulo' => 'Profit4Life',
            'mensaje' => 'US30 Se mueve stop loss a: 26779.17000000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-24 19:00:00',
            'updated_at' => '2019-10-24 19:00:00',
        ),
        103 => 
        array (
            'id_mensajes' => 764,
            'titulo' => 'Profit4Life',
            'mensaje' => 'US30 Se mueve stop loss a: 26780.17000000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-24 19:00:00',
            'updated_at' => '2019-10-24 19:00:00',
        ),
        104 => 
        array (
            'id_mensajes' => 765,
            'titulo' => 'Profit4Life',
            'mensaje' => 'US30 Se mueve stop loss a: 26781.67000000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-24 19:01:00',
            'updated_at' => '2019-10-24 19:01:00',
        ),
        105 => 
        array (
            'id_mensajes' => 766,
            'titulo' => 'Profit4Life',
            'mensaje' => 'US30 Se mueve stop loss a: 26783.67000000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-24 19:01:00',
            'updated_at' => '2019-10-24 19:01:00',
        ),
        106 => 
        array (
            'id_mensajes' => 767,
            'titulo' => 'Profit4Life',
            'mensaje' => 'US30 Se mueve stop loss a: 26784.67000000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-24 19:01:00',
            'updated_at' => '2019-10-24 19:01:00',
        ),
        107 => 
        array (
            'id_mensajes' => 768,
            'titulo' => 'Profit4Life',
            'mensaje' => 'US30 Se mueve stop loss a: 26785.67000000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-24 19:02:00',
            'updated_at' => '2019-10-24 19:02:00',
        ),
        108 => 
        array (
            'id_mensajes' => 769,
            'titulo' => 'Ignorar',
            'mensaje' => 'Ignorar Mensajes de Us30',
            'link_imagen' => NULL,
            'created_at' => '2019-10-24 19:02:00',
            'updated_at' => '2019-10-24 19:02:00',
        ),
        109 => 
        array (
            'id_mensajes' => 770,
            'titulo' => 'Profit4Life',
            'mensaje' => 'US30 Se mueve stop loss a: 26786.67000000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-24 19:03:00',
            'updated_at' => '2019-10-24 19:03:00',
        ),
        110 => 
        array (
            'id_mensajes' => 771,
            'titulo' => 'Profit4Life',
            'mensaje' => 'US30 Se mueve stop loss a: 26787.17000000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-24 19:03:00',
            'updated_at' => '2019-10-24 19:03:00',
        ),
        111 => 
        array (
            'id_mensajes' => 772,
            'titulo' => 'Profit4Life',
            'mensaje' => 'US30 Se mueve stop loss a: 26787.67000000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-24 19:03:00',
            'updated_at' => '2019-10-24 19:03:00',
        ),
        112 => 
        array (
            'id_mensajes' => 773,
            'titulo' => 'Profit4Life',
            'mensaje' => 'US30 Se mueve stop loss a: 26788.67000000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-24 19:04:00',
            'updated_at' => '2019-10-24 19:04:00',
        ),
        113 => 
        array (
            'id_mensajes' => 774,
            'titulo' => 'Profit4Life',
            'mensaje' => 'US30 Se mueve stop loss a: 26789.17000000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-24 19:04:00',
            'updated_at' => '2019-10-24 19:04:00',
        ),
        114 => 
        array (
            'id_mensajes' => 775,
            'titulo' => 'Profit4Life',
            'mensaje' => 'US30 Se mueve stop loss a: 26789.67000000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-24 19:04:00',
            'updated_at' => '2019-10-24 19:04:00',
        ),
        115 => 
        array (
            'id_mensajes' => 776,
            'titulo' => 'Profit4Life',
            'mensaje' => 'US30 Se mueve stop loss a: 26790.17000000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-24 19:04:00',
            'updated_at' => '2019-10-24 19:04:00',
        ),
        116 => 
        array (
            'id_mensajes' => 777,
            'titulo' => 'Profit4Life',
            'mensaje' => 'US30 Se mueve stop loss a: 26790.67000000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-24 19:04:00',
            'updated_at' => '2019-10-24 19:04:00',
        ),
        117 => 
        array (
            'id_mensajes' => 778,
            'titulo' => 'Profit4Life',
            'mensaje' => 'US30 Se mueve stop loss a: 26791.67000000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-24 19:06:00',
            'updated_at' => '2019-10-24 19:06:00',
        ),
        118 => 
        array (
            'id_mensajes' => 779,
            'titulo' => 'Profit4Life',
            'mensaje' => 'US30 Se mueve stop loss a: 26792.67000000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-24 19:08:00',
            'updated_at' => '2019-10-24 19:08:00',
        ),
        119 => 
        array (
            'id_mensajes' => 780,
            'titulo' => 'Profit4Life',
            'mensaje' => 'US30 Se mueve stop loss a: 26793.67000000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-24 19:11:00',
            'updated_at' => '2019-10-24 19:11:00',
        ),
        120 => 
        array (
            'id_mensajes' => 781,
            'titulo' => 'Profit4Life',
            'mensaje' => 'US30 Se mueve stop loss a: 26794.17000000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-24 19:11:00',
            'updated_at' => '2019-10-24 19:11:00',
        ),
        121 => 
        array (
            'id_mensajes' => 782,
            'titulo' => 'Profit4Life',
            'mensaje' => 'US30 Se mueve stop loss a: 26795.67000000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-24 19:11:00',
            'updated_at' => '2019-10-24 19:11:00',
        ),
        122 => 
        array (
            'id_mensajes' => 783,
            'titulo' => 'Update',
            'mensaje' => 'Estamos trabajando en Actualizaciones para Mejor los resultados y el servicio',
            'link_imagen' => NULL,
            'created_at' => '2019-10-24 19:14:00',
            'updated_at' => '2019-10-24 19:14:00',
        ),
        123 => 
        array (
            'id_mensajes' => 784,
            'titulo' => 'Profit4Life',
            'mensaje' => 'GBPUSD Se mueve stop loss a: 1.28524000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-25 00:03:00',
            'updated_at' => '2019-10-25 00:03:00',
        ),
        124 => 
        array (
            'id_mensajes' => 785,
            'titulo' => 'Profit4Life',
            'mensaje' => 'GBPUSD Se mueve stop loss a: 1.28525000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-25 00:03:00',
            'updated_at' => '2019-10-25 00:03:00',
        ),
        125 => 
        array (
            'id_mensajes' => 786,
            'titulo' => 'Profit4Life',
            'mensaje' => 'GBPUSD Se mueve stop loss a: 1.28526000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-25 00:03:00',
            'updated_at' => '2019-10-25 00:03:00',
        ),
        126 => 
        array (
            'id_mensajes' => 787,
            'titulo' => 'Profit4Life',
            'mensaje' => 'GBPUSD Se mueve stop loss a: 1.28522000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-25 00:03:00',
            'updated_at' => '2019-10-25 00:03:00',
        ),
        127 => 
        array (
            'id_mensajes' => 788,
            'titulo' => 'Profit4Life',
            'mensaje' => 'GBPUSD Se mueve stop loss a: 1.28526000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-25 00:03:00',
            'updated_at' => '2019-10-25 00:03:00',
        ),
        128 => 
        array (
            'id_mensajes' => 789,
            'titulo' => 'Profit4Life',
            'mensaje' => 'GBPUSD Se mueve stop loss a: 1.28527000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-25 00:03:00',
            'updated_at' => '2019-10-25 00:03:00',
        ),
        129 => 
        array (
            'id_mensajes' => 790,
            'titulo' => 'Profit4Life',
            'mensaje' => 'GBPUSD Se mueve stop loss a: 1.28528000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-25 00:03:00',
            'updated_at' => '2019-10-25 00:03:00',
        ),
        130 => 
        array (
            'id_mensajes' => 791,
            'titulo' => 'Profit4Life',
            'mensaje' => 'GBPUSD Se mueve stop loss a: 1.28527000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-25 00:03:00',
            'updated_at' => '2019-10-25 00:03:00',
        ),
        131 => 
        array (
            'id_mensajes' => 792,
            'titulo' => 'Profit4Life',
            'mensaje' => 'GBPUSD Se mueve stop loss a: 1.28527000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-25 00:03:00',
            'updated_at' => '2019-10-25 00:03:00',
        ),
        132 => 
        array (
            'id_mensajes' => 793,
            'titulo' => 'Profit4Life',
            'mensaje' => 'GBPUSD Se mueve stop loss a: 1.28527000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-25 00:03:00',
            'updated_at' => '2019-10-25 00:03:00',
        ),
        133 => 
        array (
            'id_mensajes' => 794,
            'titulo' => 'Profit4Life',
            'mensaje' => 'GBPUSD Se mueve stop loss a: 1.28539000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-25 00:03:00',
            'updated_at' => '2019-10-25 00:03:00',
        ),
        134 => 
        array (
            'id_mensajes' => 795,
            'titulo' => 'Profit4Life',
            'mensaje' => 'GBPUSD Se mueve stop loss a: 1.28537000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-25 00:04:00',
            'updated_at' => '2019-10-25 00:04:00',
        ),
        135 => 
        array (
            'id_mensajes' => 796,
            'titulo' => 'Profit4Life',
            'mensaje' => 'GBPUSD Se mueve stop loss a: 1.28538000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-25 00:04:00',
            'updated_at' => '2019-10-25 00:04:00',
        ),
        136 => 
        array (
            'id_mensajes' => 797,
            'titulo' => 'Profit4Life',
            'mensaje' => 'GBPUSD Se mueve stop loss a: 1.28527000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-25 00:04:00',
            'updated_at' => '2019-10-25 00:04:00',
        ),
        137 => 
        array (
            'id_mensajes' => 798,
            'titulo' => 'Profit4Life',
            'mensaje' => 'GBPUSD Se mueve stop loss a: 1.28526000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-25 00:04:00',
            'updated_at' => '2019-10-25 00:04:00',
        ),
        138 => 
        array (
            'id_mensajes' => 799,
            'titulo' => 'Profit4Life',
            'mensaje' => 'GBPUSD Se mueve stop loss a: 1.28526000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-25 00:04:00',
            'updated_at' => '2019-10-25 00:04:00',
        ),
        139 => 
        array (
            'id_mensajes' => 800,
            'titulo' => 'Profit4Life',
            'mensaje' => 'GBPUSD Se mueve stop loss a: 1.28525000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-25 00:04:00',
            'updated_at' => '2019-10-25 00:04:00',
        ),
        140 => 
        array (
            'id_mensajes' => 801,
            'titulo' => 'Profit4Life',
            'mensaje' => 'GBPUSD Se mueve stop loss a: 1.28534000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-25 00:04:00',
            'updated_at' => '2019-10-25 00:04:00',
        ),
        141 => 
        array (
            'id_mensajes' => 802,
            'titulo' => 'Profit4Life',
            'mensaje' => 'GBPUSD Se mueve stop loss a: 1.28534000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-25 00:04:00',
            'updated_at' => '2019-10-25 00:04:00',
        ),
        142 => 
        array (
            'id_mensajes' => 803,
            'titulo' => 'Profit4Life',
            'mensaje' => 'GBPUSD Se mueve stop loss a: 1.28530000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-25 00:04:00',
            'updated_at' => '2019-10-25 00:04:00',
        ),
        143 => 
        array (
            'id_mensajes' => 804,
            'titulo' => 'Profit4Life',
            'mensaje' => 'GBPUSD Se mueve stop loss a: 1.28530000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-25 00:04:00',
            'updated_at' => '2019-10-25 00:04:00',
        ),
        144 => 
        array (
            'id_mensajes' => 805,
            'titulo' => 'Profit4Life',
            'mensaje' => 'GBPUSD Se mueve stop loss a: 1.28531000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-25 00:04:00',
            'updated_at' => '2019-10-25 00:04:00',
        ),
        145 => 
        array (
            'id_mensajes' => 806,
            'titulo' => 'Profit4Life',
            'mensaje' => 'GBPUSD Se mueve stop loss a: 1.28531000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-25 00:04:00',
            'updated_at' => '2019-10-25 00:04:00',
        ),
        146 => 
        array (
            'id_mensajes' => 807,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Saliendo por stop loss en el par:GBPAUD',
            'link_imagen' => NULL,
            'created_at' => '2019-10-25 13:03:00',
            'updated_at' => '2019-10-25 13:03:00',
        ),
        147 => 
        array (
            'id_mensajes' => 808,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Short XAUUSD Entrada:1504.02000000 TP:1487.92000000 SL:1517.97000000Pips del TP:160.00000000 Pips SL:140.50000000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-25 16:00:00',
            'updated_at' => '2019-10-25 16:00:00',
        ),
        148 => 
        array (
            'id_mensajes' => 809,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Saliendo a ganacia en el par:GBPNZD',
            'link_imagen' => NULL,
            'created_at' => '2019-10-25 18:33:00',
            'updated_at' => '2019-10-25 18:33:00',
        ),
        149 => 
        array (
            'id_mensajes' => 810,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Long ETHUSD Entrada:183.10000000 TP:191.10000000 SL:176.95000000Pips del TP:80.00000000 Pips SL:61.50000000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-26 10:30:00',
            'updated_at' => '2019-10-26 10:30:00',
        ),
        150 => 
        array (
            'id_mensajes' => 811,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Short BTCUSD Entrada:9167.78000000 TP:8835.16000000 SL:9454.48000000Pips del TP:3096.60000000 Pips SL:3096.60000000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-26 14:00:00',
            'updated_at' => '2019-10-26 14:00:00',
        ),
        151 => 
        array (
            'id_mensajes' => 812,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Saliendo por stop loss en el par:ETHUSD',
            'link_imagen' => NULL,
            'created_at' => '2019-10-26 17:52:00',
            'updated_at' => '2019-10-26 17:52:00',
        ),
        152 => 
        array (
            'id_mensajes' => 813,
            'titulo' => 'BTCUSD',
            'mensaje' => 'cerrar operación con 150 pips de Ganancia',
            'link_imagen' => NULL,
            'created_at' => '2019-10-26 19:43:00',
            'updated_at' => '2019-10-26 19:43:00',
        ),
        153 => 
        array (
            'id_mensajes' => 814,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Long BTCUSD Entrada:9265.09000000 TP:9628.49000000 SL:8901.69000000Pips del TP:3634.00000000 Pips SL:3634.00000000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-26 22:00:00',
            'updated_at' => '2019-10-26 22:00:00',
        ),
        154 => 
        array (
            'id_mensajes' => 815,
            'titulo' => 'Profit4Life',
            'mensaje' => 'BTCUSD Se mueve stop loss a: 9265.59000000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-26 22:12:00',
            'updated_at' => '2019-10-26 22:12:00',
        ),
        155 => 
        array (
            'id_mensajes' => 816,
            'titulo' => 'Profit4Life',
            'mensaje' => 'BTCUSD Se mueve stop loss a: 9268.59000000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-26 22:12:00',
            'updated_at' => '2019-10-26 22:12:00',
        ),
        156 => 
        array (
            'id_mensajes' => 817,
            'titulo' => 'Profit4Life',
            'mensaje' => 'BTCUSD Se mueve stop loss a sl Alto: 57.10000000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-26 22:12:00',
            'updated_at' => '2019-10-26 22:12:00',
        ),
        157 => 
        array (
            'id_mensajes' => 818,
            'titulo' => 'Profit4Life',
            'mensaje' => 'BTCUSD Se mueve stop loss a: 9274.68000000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-26 22:12:00',
            'updated_at' => '2019-10-26 22:12:00',
        ),
        158 => 
        array (
            'id_mensajes' => 819,
            'titulo' => 'Profit4Life',
            'mensaje' => 'BTCUSD Se mueve stop loss a: 9274.78000000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-26 22:12:00',
            'updated_at' => '2019-10-26 22:12:00',
        ),
        159 => 
        array (
            'id_mensajes' => 820,
            'titulo' => 'Profit4Life',
            'mensaje' => 'BTCUSD Se mueve stop loss a: 9276.68000000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-26 22:12:00',
            'updated_at' => '2019-10-26 22:12:00',
        ),
        160 => 
        array (
            'id_mensajes' => 821,
            'titulo' => 'Profit4Life',
            'mensaje' => 'BTCUSD Se mueve stop loss a: 9277.48000000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-26 22:12:00',
            'updated_at' => '2019-10-26 22:12:00',
        ),
        161 => 
        array (
            'id_mensajes' => 822,
            'titulo' => 'Profit4Life',
            'mensaje' => 'BTCUSD Se mueve stop loss a: 9270.59000000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-26 22:12:00',
            'updated_at' => '2019-10-26 22:12:00',
        ),
        162 => 
        array (
            'id_mensajes' => 823,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Saliendo a ganacia en el par:BTCUSD',
            'link_imagen' => NULL,
            'created_at' => '2019-10-26 22:12:00',
            'updated_at' => '2019-10-26 22:12:00',
        ),
        163 => 
        array (
            'id_mensajes' => 824,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Long XRPUSD Entrada:0.29792000 TP:0.30992000 SL:0.29252000Pips del TP:120.00000000 Pips SL:54.00000000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-27 14:00:00',
            'updated_at' => '2019-10-27 14:00:00',
        ),
        164 => 
        array (
            'id_mensajes' => 825,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Long XRPUSD Entrada:0.30052000 TP:0.31252000 SL:0.29585000Pips del TP:120.00000000 Pips SL:46.70000000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-28 00:30:00',
            'updated_at' => '2019-10-28 00:30:00',
        ),
        165 => 
        array (
            'id_mensajes' => 826,
            'titulo' => 'Profit4Life',
            'mensaje' => 'XRPUSD Se mueve stop loss a: 0.30292000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-28 00:43:00',
            'updated_at' => '2019-10-28 00:43:00',
        ),
        166 => 
        array (
            'id_mensajes' => 827,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Saliendo a ganacia en el par:XRPUSD',
            'link_imagen' => NULL,
            'created_at' => '2019-10-28 00:46:00',
            'updated_at' => '2019-10-28 00:46:00',
        ),
        167 => 
        array (
            'id_mensajes' => 828,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Saliendo a ganacia en el par:XRPUSD',
            'link_imagen' => NULL,
            'created_at' => '2019-10-28 00:57:00',
            'updated_at' => '2019-10-28 00:57:00',
        ),
        168 => 
        array (
            'id_mensajes' => 829,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Long ETHUSD Entrada:184.23000000 TP:192.23000000 SL:181.51000000Pips del TP:80.00000000 Pips SL:27.20000000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-28 09:30:00',
            'updated_at' => '2019-10-28 09:30:00',
        ),
        169 => 
        array (
            'id_mensajes' => 830,
            'titulo' => 'XAUUSD',
            'mensaje' => 'cerrar oro con 50 pips de Ganancia',
            'link_imagen' => NULL,
            'created_at' => '2019-10-28 13:05:00',
            'updated_at' => '2019-10-28 13:05:00',
        ),
        170 => 
        array (
            'id_mensajes' => 831,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Long BTCUSD Entrada:9430.07000000 TP:9532.92000000 SL:9327.22000000Pips del TP:1028.50000000 Pips SL:1028.50000000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-28 16:00:00',
            'updated_at' => '2019-10-28 16:00:00',
        ),
        171 => 
        array (
            'id_mensajes' => 832,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Saliendo por stop loss en el par:ETHUSD',
            'link_imagen' => NULL,
            'created_at' => '2019-10-28 17:07:00',
            'updated_at' => '2019-10-28 17:07:00',
        ),
        172 => 
        array (
            'id_mensajes' => 833,
            'titulo' => 'Profit4Life',
            'mensaje' => 'BTCUSD Se mueve stop loss a: 9430.01000000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-28 18:12:00',
            'updated_at' => '2019-10-28 18:12:00',
        ),
        173 => 
        array (
            'id_mensajes' => 834,
            'titulo' => 'Profit4Life',
            'mensaje' => 'BTCUSD Se mueve stop loss a: 9431.01000000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-28 18:12:00',
            'updated_at' => '2019-10-28 18:12:00',
        ),
        174 => 
        array (
            'id_mensajes' => 835,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Saliendo a ganacia en el par:BTCUSD',
            'link_imagen' => NULL,
            'created_at' => '2019-10-28 18:13:00',
            'updated_at' => '2019-10-28 18:13:00',
        ),
        175 => 
        array (
            'id_mensajes' => 836,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Long GBPNZD Entrada:2.01943000 TP:2.03743000 SL:2.01560000Pips del TP:180.00000000 Pips SL:38.30000000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-29 05:00:00',
            'updated_at' => '2019-10-29 05:00:00',
        ),
        176 => 
        array (
            'id_mensajes' => 837,
            'titulo' => 'Profit4Life',
            'mensaje' => 'GBPNZD Se mueve stop loss a: 2.02511000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-29 10:55:00',
            'updated_at' => '2019-10-29 10:55:00',
        ),
        177 => 
        array (
            'id_mensajes' => 838,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Saliendo a ganacia en el par:GBPNZD',
            'link_imagen' => NULL,
            'created_at' => '2019-10-29 10:55:00',
            'updated_at' => '2019-10-29 10:55:00',
        ),
        178 => 
        array (
            'id_mensajes' => 839,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Long ETHUSD Entrada:187.55000000 TP:195.55000000 SL:184.26000000Pips del TP:80.00000000 Pips SL:32.90000000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-29 15:00:00',
            'updated_at' => '2019-10-29 15:00:00',
        ),
        179 => 
        array (
            'id_mensajes' => 840,
            'titulo' => 'Profit4Life',
            'mensaje' => 'ETHUSD Se mueve stop loss a: 190.59000000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-29 20:06:00',
            'updated_at' => '2019-10-29 20:06:00',
        ),
        180 => 
        array (
            'id_mensajes' => 841,
            'titulo' => 'Profit4Life',
            'mensaje' => 'ETHUSD Se mueve stop loss a: 191.11000000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-29 20:06:00',
            'updated_at' => '2019-10-29 20:06:00',
        ),
        181 => 
        array (
            'id_mensajes' => 842,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Saliendo a ganacia en el par:ETHUSD',
            'link_imagen' => NULL,
            'created_at' => '2019-10-29 20:07:00',
            'updated_at' => '2019-10-29 20:07:00',
        ),
        182 => 
        array (
            'id_mensajes' => 843,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Long GBPNZD Entrada:2.02702000 TP:2.04502000 SL:2.02019000Pips del TP:180.00000000 Pips SL:68.30000000',
            'link_imagen' => NULL,
            'created_at' => '2019-10-30 06:00:00',
            'updated_at' => '2019-10-30 06:00:00',
        ),
        183 => 
        array (
            'id_mensajes' => 844,
            'titulo' => 'Noticias',
            'mensaje' => 'Update',
            'link_imagen' => NULL,
            'created_at' => '2019-11-04 18:01:00',
            'updated_at' => '2019-11-04 18:01:00',
        ),
        184 => 
        array (
            'id_mensajes' => 845,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Long XRPUSD Entrada:0.30200000 TP:0.31400000 SL:0.29578000Pips del TP:120.00000000 Pips SL:62.20000000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-05 09:30:00',
            'updated_at' => '2019-11-05 09:30:00',
        ),
        185 => 
        array (
            'id_mensajes' => 846,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Long GBPNZD Entrada:2.01157000 TP:2.02957000 SL:2.00414000Pips del TP:180.00000000 Pips SL:74.30000000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-05 11:00:00',
            'updated_at' => '2019-11-05 11:00:00',
        ),
        186 => 
        array (
            'id_mensajes' => 847,
            'titulo' => 'Profit4Life',
            'mensaje' => 'GBPNZD Se mueve stop loss a: 2.01713000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-05 15:11:00',
            'updated_at' => '2019-11-05 15:11:00',
        ),
        187 => 
        array (
            'id_mensajes' => 848,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Saliendo a ganacia en el par:GBPNZD',
            'link_imagen' => NULL,
            'created_at' => '2019-11-05 15:11:00',
            'updated_at' => '2019-11-05 15:11:00',
        ),
        188 => 
        array (
            'id_mensajes' => 849,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Short GBPNZD Entrada:2.01978000 TP:2.00156000 SL:2.02419000Pips del TP:180.00000000 Pips SL:46.30000000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-06 09:00:00',
            'updated_at' => '2019-11-06 09:00:00',
        ),
        189 => 
        array (
            'id_mensajes' => 850,
            'titulo' => 'Profit4Life',
            'mensaje' => 'XRPUSD Se mueve stop loss a: 0.30740000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-06 21:37:00',
            'updated_at' => '2019-11-06 21:37:00',
        ),
        190 => 
        array (
            'id_mensajes' => 851,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Saliendo a ganacia en el par:XRPUSD',
            'link_imagen' => NULL,
            'created_at' => '2019-11-06 21:39:00',
            'updated_at' => '2019-11-06 21:39:00',
        ),
        191 => 
        array (
            'id_mensajes' => 852,
            'titulo' => 'Profit4Life',
            'mensaje' => 'GBPNZD Se mueve stop loss a: 2.01404000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-07 12:05:00',
            'updated_at' => '2019-11-07 12:05:00',
        ),
        192 => 
        array (
            'id_mensajes' => 853,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Saliendo a ganacia en el par:GBPNZD',
            'link_imagen' => NULL,
            'created_at' => '2019-11-07 12:07:00',
            'updated_at' => '2019-11-07 12:07:00',
        ),
        193 => 
        array (
            'id_mensajes' => 854,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Short GBPAUD Entrada:1.86515000 TP:1.84995000 SL:1.87103000Pips del TP:150.00000000 Pips SL:60.80000000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-08 16:00:00',
            'updated_at' => '2019-11-08 16:00:00',
        ),
        194 => 
        array (
            'id_mensajes' => 855,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Short GBPNZD Entrada:2.01936000 TP:2.00119000 SL:2.02562000Pips del TP:180.00000000 Pips SL:64.30000000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-08 16:00:00',
            'updated_at' => '2019-11-08 16:00:00',
        ),
        195 => 
        array (
            'id_mensajes' => 857,
            'titulo' => 'Update',
            'mensaje' => 'Estamos Trabajando en La nueva actualización muy pronto podrán Disfrutarla.',
            'link_imagen' => NULL,
            'created_at' => '2019-11-09 18:56:00',
            'updated_at' => '2019-11-09 18:56:00',
        ),
        196 => 
        array (
            'id_mensajes' => 858,
            'titulo' => 'Update',
            'mensaje' => 'Todos los usuarios con Android_mensajes pueden actualizar su App. IOS en un par de días.',
            'link_imagen' => NULL,
            'created_at' => '2019-11-10 17:15:00',
            'updated_at' => '2019-11-10 17:15:00',
        ),
        197 => 
        array (
            'id_mensajes' => 859,
            'titulo' => 'Profit4Life',
            'mensaje' => 'GBPNZD Se mueve stop loss a: 2.01366000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-11 09:15:00',
            'updated_at' => '2019-11-11 09:15:00',
        ),
        198 => 
        array (
            'id_mensajes' => 860,
            'titulo' => 'Profit4Life',
            'mensaje' => 'GBPNZD Se mueve stop loss a: 2.01311000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-11 09:16:00',
            'updated_at' => '2019-11-11 09:16:00',
        ),
        199 => 
        array (
            'id_mensajes' => 861,
            'titulo' => 'Profit4Life',
            'mensaje' => 'GBPNZD Se mueve stop loss a: 2.01311000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-11 09:16:00',
            'updated_at' => '2019-11-11 09:16:00',
        ),
        200 => 
        array (
            'id_mensajes' => 862,
            'titulo' => 'Profit4Life',
            'mensaje' => 'GBPNZD Se mueve stop loss a: 2.01206000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-11 09:17:00',
            'updated_at' => '2019-11-11 09:17:00',
        ),
        201 => 
        array (
            'id_mensajes' => 863,
            'titulo' => 'Profit4Life',
            'mensaje' => 'GBPNZD Se mueve stop loss a: 2.01206000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-11 09:17:00',
            'updated_at' => '2019-11-11 09:17:00',
        ),
        202 => 
        array (
            'id_mensajes' => 864,
            'titulo' => 'Profit4Life',
            'mensaje' => 'GBPNZD Se mueve stop loss a: 2.01202000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-11 09:17:00',
            'updated_at' => '2019-11-11 09:17:00',
        ),
        203 => 
        array (
            'id_mensajes' => 865,
            'titulo' => 'Profit4Life',
            'mensaje' => 'GBPNZD Se mueve stop loss a: 2.01202000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-11 09:17:00',
            'updated_at' => '2019-11-11 09:17:00',
        ),
        204 => 
        array (
            'id_mensajes' => 866,
            'titulo' => 'Profit4Life',
            'mensaje' => 'GBPNZD Se mueve stop loss a: 2.01191000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-11 09:17:00',
            'updated_at' => '2019-11-11 09:17:00',
        ),
        205 => 
        array (
            'id_mensajes' => 867,
            'titulo' => 'Profit4Life',
            'mensaje' => 'GBPNZD Se mueve stop loss a: 2.01191000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-11 09:17:00',
            'updated_at' => '2019-11-11 09:17:00',
        ),
        206 => 
        array (
            'id_mensajes' => 868,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Saliendo a ganacia en el par:GBPNZD',
            'link_imagen' => NULL,
            'created_at' => '2019-11-11 09:17:00',
            'updated_at' => '2019-11-11 09:17:00',
        ),
        207 => 
        array (
            'id_mensajes' => 869,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Saliendo por stop loss en el par:GBPAUD',
            'link_imagen' => NULL,
            'created_at' => '2019-11-11 11:50:00',
            'updated_at' => '2019-11-11 11:50:00',
        ),
        208 => 
        array (
            'id_mensajes' => 870,
            'titulo' => 'Nueva Orden Pendiente',
            'mensaje' => 'Orden Pendiente en la pestaña de Profitcalls, por ahora solo para Android_mensajese',
            'link_imagen' => NULL,
            'created_at' => '2019-11-11 15:30:00',
            'updated_at' => '2019-11-11 15:30:00',
        ),
        209 => 
        array (
            'id_mensajes' => 871,
            'titulo' => 'Noticias',
            'mensaje' => 'EURUSD',
            'link_imagen' => NULL,
            'created_at' => '2019-11-11 15:50:00',
            'updated_at' => '2019-11-11 15:50:00',
        ),
        210 => 
        array (
            'id_mensajes' => 872,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Short GBPNZD Entrada:2.02140000 TP:2.00316000 SL:2.02566000Pips del TP:180.00000000 Pips SL:45.00000000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-11 17:00:00',
            'updated_at' => '2019-11-11 17:00:00',
        ),
        211 => 
        array (
            'id_mensajes' => 873,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Long ETHUSD Entrada:186.18000000 TP:194.18000000 SL:183.98000000Pips del TP:80.00000000 Pips SL:22.00000000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-12 01:00:00',
            'updated_at' => '2019-11-12 01:00:00',
        ),
        212 => 
        array (
            'id_mensajes' => 874,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Saliendo por stop loss en el par:GBPNZD',
            'link_imagen' => NULL,
            'created_at' => '2019-11-12 02:00:00',
            'updated_at' => '2019-11-12 02:00:00',
        ),
        213 => 
        array (
            'id_mensajes' => 875,
            'titulo' => 'Update',
            'mensaje' => 'usuarios de Android_mensajese vuelvan a actualizar',
            'link_imagen' => NULL,
            'created_at' => '2019-11-12 05:18:00',
            'updated_at' => '2019-11-12 05:18:00',
        ),
        214 => 
        array (
            'id_mensajes' => 876,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Saliendo por stop loss en el par:ETHUSD',
            'link_imagen' => NULL,
            'created_at' => '2019-11-12 13:58:00',
            'updated_at' => '2019-11-12 13:58:00',
        ),
        215 => 
        array (
            'id_mensajes' => 877,
            'titulo' => 'Noticias',
            'mensaje' => 'USDCAD',
            'link_imagen' => NULL,
            'created_at' => '2019-11-12 22:20:00',
            'updated_at' => '2019-11-12 22:20:00',
        ),
        216 => 
        array (
            'id_mensajes' => 878,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Long GBPNZD Entrada:2.01042000 TP:2.02842000 SL:2.00413000Pips del TP:180.00000000 Pips SL:62.90000000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-13 04:00:00',
            'updated_at' => '2019-11-13 04:00:00',
        ),
        217 => 
        array (
            'id_mensajes' => 879,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Short GBPAUD Entrada:1.87776000 TP:1.86255000 SL:1.88120000Pips del TP:150.00000000 Pips SL:36.50000000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-13 05:00:00',
            'updated_at' => '2019-11-13 05:00:00',
        ),
        218 => 
        array (
            'id_mensajes' => 880,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Saliendo por stop loss en el par:GBPNZD',
            'link_imagen' => NULL,
            'created_at' => '2019-11-13 08:30:00',
            'updated_at' => '2019-11-13 08:30:00',
        ),
        219 => 
        array (
            'id_mensajes' => 881,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Short XRPUSD Entrada:0.27273000 TP:0.26011000 SL:0.27554000Pips del TP:120.00000000 Pips SL:34.30000000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-13 09:00:00',
            'updated_at' => '2019-11-13 09:00:00',
        ),
        220 => 
        array (
            'id_mensajes' => 882,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Saliendo por stop loss en el par:GBPAUD',
            'link_imagen' => NULL,
            'created_at' => '2019-11-13 09:43:00',
            'updated_at' => '2019-11-13 09:43:00',
        ),
        221 => 
        array (
            'id_mensajes' => 883,
            'titulo' => 'Profit4Life',
            'mensaje' => 'XRPUSD Se mueve stop loss a: 0.26567000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-14 08:01:00',
            'updated_at' => '2019-11-14 08:01:00',
        ),
        222 => 
        array (
            'id_mensajes' => 884,
            'titulo' => 'Profit4Life',
            'mensaje' => 'XRPUSD Se mueve stop loss a: 0.26673000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-14 08:02:00',
            'updated_at' => '2019-11-14 08:02:00',
        ),
        223 => 
        array (
            'id_mensajes' => 885,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Saliendo a ganacia en el par:XRPUSD',
            'link_imagen' => NULL,
            'created_at' => '2019-11-14 08:02:00',
            'updated_at' => '2019-11-14 08:02:00',
        ),
        224 => 
        array (
            'id_mensajes' => 886,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Short GBPNZD Entrada:2.01885000 TP:2.00032000 SL:2.02541000Pips del TP:180.00000000 Pips SL:70.90000000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-14 22:05:00',
            'updated_at' => '2019-11-14 22:05:00',
        ),
        225 => 
        array (
            'id_mensajes' => 887,
            'titulo' => 'Noticias',
            'mensaje' => 'Update Iphone',
            'link_imagen' => NULL,
            'created_at' => '2019-11-15 01:35:00',
            'updated_at' => '2019-11-15 01:35:00',
        ),
        226 => 
        array (
            'id_mensajes' => 888,
            'titulo' => 'update iphone',
            'mensaje' => 'https://testflight.apple.com/join/SL9Q42Ow',
            'link_imagen' => NULL,
            'created_at' => '2019-11-15 01:37:00',
            'updated_at' => '2019-11-15 01:37:00',
        ),
        227 => 
        array (
            'id_mensajes' => 889,
            'titulo' => 'Update ios',
            'mensaje' => 'El link lo pueden buscar en mi última historia en @emiletrader en IG',
            'link_imagen' => NULL,
            'created_at' => '2019-11-15 01:48:00',
            'updated_at' => '2019-11-15 01:48:00',
        ),
        228 => 
        array (
            'id_mensajes' => 890,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Saliendo a ganacia en el par:GBPNZD',
            'link_imagen' => NULL,
            'created_at' => '2019-11-15 21:21:00',
            'updated_at' => '2019-11-15 21:21:00',
        ),
        229 => 
        array (
            'id_mensajes' => 891,
            'titulo' => 'Feliz fin de semana',
            'mensaje' => 'Esperamos hayan podid_mensajeso tomar ventaja de los análisis de su pestaña de Home.',
            'link_imagen' => NULL,
            'created_at' => '2019-11-16 00:11:00',
            'updated_at' => '2019-11-16 00:11:00',
        ),
        230 => 
        array (
            'id_mensajes' => 892,
            'titulo' => 'Profit4Life',
            'mensaje' => 'ETHUSD Se mueve stop loss a: 182.82000000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-17 03:05:00',
            'updated_at' => '2019-11-17 03:05:00',
        ),
        231 => 
        array (
            'id_mensajes' => 893,
            'titulo' => 'Profit4Life',
            'mensaje' => 'ETHUSD Se mueve stop loss a: 182.82000000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-17 03:05:00',
            'updated_at' => '2019-11-17 03:05:00',
        ),
        232 => 
        array (
            'id_mensajes' => 894,
            'titulo' => 'Ethusd',
            'mensaje' => 'obviar mensaje de Ethusd',
            'link_imagen' => NULL,
            'created_at' => '2019-11-17 03:06:00',
            'updated_at' => '2019-11-17 03:06:00',
        ),
        233 => 
        array (
            'id_mensajes' => 895,
            'titulo' => 'Noticias',
            'mensaje' => 'BTCUSD',
            'link_imagen' => NULL,
            'created_at' => '2019-11-17 23:22:00',
            'updated_at' => '2019-11-17 23:22:00',
        ),
        234 => 
        array (
            'id_mensajes' => 896,
            'titulo' => 'BTCUSD',
            'mensaje' => 'Tienen un mensaje en su pestaña de Home',
            'link_imagen' => NULL,
            'created_at' => '2019-11-18 00:13:00',
            'updated_at' => '2019-11-18 00:13:00',
        ),
        235 => 
        array (
            'id_mensajes' => 897,
            'titulo' => 'Home',
            'mensaje' => 'Las proyecciones de EURUSD y USDCAD siguen siendo válid_mensajesas',
            'link_imagen' => NULL,
            'created_at' => '2019-11-18 17:32:00',
            'updated_at' => '2019-11-18 17:32:00',
        ),
        236 => 
        array (
            'id_mensajes' => 898,
            'titulo' => 'Home',
            'mensaje' => 'La entrada en venta de BTCUSD también sigue abierta y con 100 pips',
            'link_imagen' => NULL,
            'created_at' => '2019-11-18 17:33:00',
            'updated_at' => '2019-11-18 17:33:00',
        ),
        237 => 
        array (
            'id_mensajes' => 899,
            'titulo' => 'Noticias',
            'mensaje' => 'BTCUSD',
            'link_imagen' => NULL,
            'created_at' => '2019-11-18 18:38:00',
            'updated_at' => '2019-11-18 18:38:00',
        ),
        238 => 
        array (
            'id_mensajes' => 900,
            'titulo' => 'Noticias',
            'mensaje' => 'Gold',
            'link_imagen' => NULL,
            'created_at' => '2019-11-19 05:12:00',
            'updated_at' => '2019-11-19 05:12:00',
        ),
        239 => 
        array (
            'id_mensajes' => 901,
            'titulo' => 'Profit4Life',
            'mensaje' => 'XAUUSD Se mueve stop loss a: 1470.45000000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-19 10:29:00',
            'updated_at' => '2019-11-19 10:29:00',
        ),
        240 => 
        array (
            'id_mensajes' => 902,
            'titulo' => 'Profit4Life',
            'mensaje' => 'XAUUSD Se mueve stop loss a: 1470.29000000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-19 10:29:00',
            'updated_at' => '2019-11-19 10:29:00',
        ),
        241 => 
        array (
            'id_mensajes' => 903,
            'titulo' => 'Profit4Life',
            'mensaje' => 'XAUUSD Se mueve stop loss a: 1470.26000000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-19 10:29:00',
            'updated_at' => '2019-11-19 10:29:00',
        ),
        242 => 
        array (
            'id_mensajes' => 904,
            'titulo' => 'Profit4Life',
            'mensaje' => 'XAUUSD Se mueve stop loss a: 1470.25000000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-19 10:29:00',
            'updated_at' => '2019-11-19 10:29:00',
        ),
        243 => 
        array (
            'id_mensajes' => 905,
            'titulo' => 'Profit4Life',
            'mensaje' => 'XAUUSD Se mueve stop loss a: 1470.31000000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-19 10:29:00',
            'updated_at' => '2019-11-19 10:29:00',
        ),
        244 => 
        array (
            'id_mensajes' => 906,
            'titulo' => 'Profit4Life',
            'mensaje' => 'XAUUSD Se mueve stop loss a: 1470.33000000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-19 10:29:00',
            'updated_at' => '2019-11-19 10:29:00',
        ),
        245 => 
        array (
            'id_mensajes' => 907,
            'titulo' => 'Profit4Life',
            'mensaje' => 'XAUUSD Se mueve stop loss a: 1470.33000000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-19 10:30:00',
            'updated_at' => '2019-11-19 10:30:00',
        ),
        246 => 
        array (
            'id_mensajes' => 908,
            'titulo' => 'Profit4Life',
            'mensaje' => 'XAUUSD Se mueve stop loss a: 1470.32000000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-19 10:30:00',
            'updated_at' => '2019-11-19 10:30:00',
        ),
        247 => 
        array (
            'id_mensajes' => 909,
            'titulo' => 'Profit4Life',
            'mensaje' => 'XAUUSD Se mueve stop loss a: 1470.23000000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-19 10:30:00',
            'updated_at' => '2019-11-19 10:30:00',
        ),
        248 => 
        array (
            'id_mensajes' => 910,
            'titulo' => 'Profit4Life',
            'mensaje' => 'XAUUSD Se mueve stop loss a: 1470.10000000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-19 10:30:00',
            'updated_at' => '2019-11-19 10:30:00',
        ),
        249 => 
        array (
            'id_mensajes' => 911,
            'titulo' => 'Profit4Life',
            'mensaje' => 'XAUUSD Se mueve stop loss a: 1470.23000000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-19 10:30:00',
            'updated_at' => '2019-11-19 10:30:00',
        ),
        250 => 
        array (
            'id_mensajes' => 912,
            'titulo' => 'Profit4Life',
            'mensaje' => 'XAUUSD Se mueve stop loss a: 1470.24000000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-19 10:30:00',
            'updated_at' => '2019-11-19 10:30:00',
        ),
        251 => 
        array (
            'id_mensajes' => 913,
            'titulo' => 'Profit4Life',
            'mensaje' => 'XAUUSD Se mueve stop loss a: 1470.24000000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-19 10:30:00',
            'updated_at' => '2019-11-19 10:30:00',
        ),
        252 => 
        array (
            'id_mensajes' => 914,
            'titulo' => 'Profit4Life',
            'mensaje' => 'XAUUSD Se mueve stop loss a: 1470.23000000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-19 10:30:00',
            'updated_at' => '2019-11-19 10:30:00',
        ),
        253 => 
        array (
            'id_mensajes' => 915,
            'titulo' => 'Profit4Life',
            'mensaje' => 'XAUUSD Se mueve stop loss a: 1470.24000000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-19 10:30:00',
            'updated_at' => '2019-11-19 10:30:00',
        ),
        254 => 
        array (
            'id_mensajes' => 916,
            'titulo' => 'Profit4Life',
            'mensaje' => 'XAUUSD Se mueve stop loss a: 1470.25000000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-19 10:30:00',
            'updated_at' => '2019-11-19 10:30:00',
        ),
        255 => 
        array (
            'id_mensajes' => 917,
            'titulo' => 'Profit4Life',
            'mensaje' => 'XAUUSD Se mueve stop loss a: 1470.35000000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-19 10:30:00',
            'updated_at' => '2019-11-19 10:30:00',
        ),
        256 => 
        array (
            'id_mensajes' => 918,
            'titulo' => 'Profit4Life',
            'mensaje' => 'XAUUSD Se mueve stop loss a: 1470.30000000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-19 10:30:00',
            'updated_at' => '2019-11-19 10:30:00',
        ),
        257 => 
        array (
            'id_mensajes' => 919,
            'titulo' => 'Profit4Life',
            'mensaje' => 'XAUUSD Se mueve stop loss a: 1470.29000000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-19 10:30:00',
            'updated_at' => '2019-11-19 10:30:00',
        ),
        258 => 
        array (
            'id_mensajes' => 920,
            'titulo' => 'Profit4Life',
            'mensaje' => 'XAUUSD Se mueve stop loss a: 1470.30000000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-19 10:30:00',
            'updated_at' => '2019-11-19 10:30:00',
        ),
        259 => 
        array (
            'id_mensajes' => 921,
            'titulo' => 'Profit4Life',
            'mensaje' => 'XAUUSD Se mueve stop loss a: 1470.35000000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-19 10:30:00',
            'updated_at' => '2019-11-19 10:30:00',
        ),
        260 => 
        array (
            'id_mensajes' => 922,
            'titulo' => 'Profit4Life',
            'mensaje' => 'XAUUSD Se mueve stop loss a: 1470.37000000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-19 10:30:00',
            'updated_at' => '2019-11-19 10:30:00',
        ),
        261 => 
        array (
            'id_mensajes' => 923,
            'titulo' => 'Profit4Life',
            'mensaje' => 'XAUUSD Se mueve stop loss a: 1470.30000000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-19 10:30:00',
            'updated_at' => '2019-11-19 10:30:00',
        ),
        262 => 
        array (
            'id_mensajes' => 924,
            'titulo' => 'Profit4Life',
            'mensaje' => 'XAUUSD Se mueve stop loss a: 1470.22000000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-19 10:30:00',
            'updated_at' => '2019-11-19 10:30:00',
        ),
        263 => 
        array (
            'id_mensajes' => 925,
            'titulo' => 'Profit4Life',
            'mensaje' => 'XAUUSD Se mueve stop loss a: 1470.20000000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-19 10:30:00',
            'updated_at' => '2019-11-19 10:30:00',
        ),
        264 => 
        array (
            'id_mensajes' => 926,
            'titulo' => 'Profit4Life',
            'mensaje' => 'XAUUSD Se mueve stop loss a: 1470.21000000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-19 10:30:00',
            'updated_at' => '2019-11-19 10:30:00',
        ),
        265 => 
        array (
            'id_mensajes' => 927,
            'titulo' => 'Profit4Life',
            'mensaje' => 'XAUUSD Se mueve stop loss a: 1470.26000000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-19 10:30:00',
            'updated_at' => '2019-11-19 10:30:00',
        ),
        266 => 
        array (
            'id_mensajes' => 928,
            'titulo' => 'Profit4Life',
            'mensaje' => 'XAUUSD Se mueve stop loss a: 1470.26000000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-19 10:30:00',
            'updated_at' => '2019-11-19 10:30:00',
        ),
        267 => 
        array (
            'id_mensajes' => 929,
            'titulo' => 'Profit4Life',
            'mensaje' => 'XAUUSD Se mueve stop loss a: 1470.26000000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-19 10:30:00',
            'updated_at' => '2019-11-19 10:30:00',
        ),
        268 => 
        array (
            'id_mensajes' => 930,
            'titulo' => 'Profit4Life',
            'mensaje' => 'XAUUSD Se mueve stop loss a: 1470.30000000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-19 10:30:00',
            'updated_at' => '2019-11-19 10:30:00',
        ),
        269 => 
        array (
            'id_mensajes' => 931,
            'titulo' => 'Profit4Life',
            'mensaje' => 'XAUUSD Se mueve stop loss a: 1470.24000000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-19 10:31:00',
            'updated_at' => '2019-11-19 10:31:00',
        ),
        270 => 
        array (
            'id_mensajes' => 932,
            'titulo' => 'Profit4Life',
            'mensaje' => 'XAUUSD Se mueve stop loss a: 1470.25000000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-19 10:31:00',
            'updated_at' => '2019-11-19 10:31:00',
        ),
        271 => 
        array (
            'id_mensajes' => 933,
            'titulo' => 'Profit4Life',
            'mensaje' => 'XAUUSD Se mueve stop loss a: 1470.32000000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-19 10:31:00',
            'updated_at' => '2019-11-19 10:31:00',
        ),
        272 => 
        array (
            'id_mensajes' => 934,
            'titulo' => 'Profit4Life',
            'mensaje' => 'XAUUSD Se mueve stop loss a: 1470.31000000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-19 10:31:00',
            'updated_at' => '2019-11-19 10:31:00',
        ),
        273 => 
        array (
            'id_mensajes' => 935,
            'titulo' => 'Profit4Life',
            'mensaje' => 'XAUUSD Se mueve stop loss a: 1470.27000000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-19 10:31:00',
            'updated_at' => '2019-11-19 10:31:00',
        ),
        274 => 
        array (
            'id_mensajes' => 936,
            'titulo' => 'Profit4Life',
            'mensaje' => 'XAUUSD Se mueve stop loss a: 1470.35000000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-19 10:31:00',
            'updated_at' => '2019-11-19 10:31:00',
        ),
        275 => 
        array (
            'id_mensajes' => 937,
            'titulo' => 'Profit4Life',
            'mensaje' => 'XAUUSD Se mueve stop loss a: 1470.36000000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-19 10:31:00',
            'updated_at' => '2019-11-19 10:31:00',
        ),
        276 => 
        array (
            'id_mensajes' => 938,
            'titulo' => 'Profit4Life',
            'mensaje' => 'XAUUSD Se mueve stop loss a: 1470.36000000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-19 10:31:00',
            'updated_at' => '2019-11-19 10:31:00',
        ),
        277 => 
        array (
            'id_mensajes' => 939,
            'titulo' => 'Profit4Life',
            'mensaje' => 'XAUUSD Se mueve stop loss a: 1470.41000000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-19 10:31:00',
            'updated_at' => '2019-11-19 10:31:00',
        ),
        278 => 
        array (
            'id_mensajes' => 940,
            'titulo' => 'Profit4Life',
            'mensaje' => 'XAUUSD Se mueve stop loss a: 1470.40000000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-19 10:31:00',
            'updated_at' => '2019-11-19 10:31:00',
        ),
        279 => 
        array (
            'id_mensajes' => 941,
            'titulo' => 'Profit4Life',
            'mensaje' => 'XAUUSD Se mueve stop loss a: 1470.38000000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-19 10:31:00',
            'updated_at' => '2019-11-19 10:31:00',
        ),
        280 => 
        array (
            'id_mensajes' => 942,
            'titulo' => 'Profit4Life',
            'mensaje' => 'XAUUSD Se mueve stop loss a: 1470.24000000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-19 10:31:00',
            'updated_at' => '2019-11-19 10:31:00',
        ),
        281 => 
        array (
            'id_mensajes' => 943,
            'titulo' => 'Profit4Life',
            'mensaje' => 'XAUUSD Se mueve stop loss a: 1470.22000000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-19 10:31:00',
            'updated_at' => '2019-11-19 10:31:00',
        ),
        282 => 
        array (
            'id_mensajes' => 944,
            'titulo' => 'Profit4Life',
            'mensaje' => 'XAUUSD Se mueve stop loss a: 1470.20000000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-19 10:31:00',
            'updated_at' => '2019-11-19 10:31:00',
        ),
        283 => 
        array (
            'id_mensajes' => 945,
            'titulo' => 'Profit4Life',
            'mensaje' => 'XAUUSD Se mueve stop loss a: 1469.91000000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-19 10:31:00',
            'updated_at' => '2019-11-19 10:31:00',
        ),
        284 => 
        array (
            'id_mensajes' => 946,
            'titulo' => 'Profit4Life',
            'mensaje' => 'XAUUSD Se mueve stop loss a: 1470.03000000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-19 10:31:00',
            'updated_at' => '2019-11-19 10:31:00',
        ),
        285 => 
        array (
            'id_mensajes' => 947,
            'titulo' => 'Profit4Life',
            'mensaje' => 'XAUUSD Se mueve stop loss a: 1470.06000000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-19 10:31:00',
            'updated_at' => '2019-11-19 10:31:00',
        ),
        286 => 
        array (
            'id_mensajes' => 948,
            'titulo' => 'Profit4Life',
            'mensaje' => 'XAUUSD Se mueve stop loss a: 1470.03000000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-19 10:31:00',
            'updated_at' => '2019-11-19 10:31:00',
        ),
        287 => 
        array (
            'id_mensajes' => 949,
            'titulo' => 'Profit4Life',
            'mensaje' => 'XAUUSD Se mueve stop loss a: 1470.13000000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-19 10:32:00',
            'updated_at' => '2019-11-19 10:32:00',
        ),
        288 => 
        array (
            'id_mensajes' => 950,
            'titulo' => 'Profit4Life',
            'mensaje' => 'XAUUSD Se mueve stop loss a: 1470.17000000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-19 10:32:00',
            'updated_at' => '2019-11-19 10:32:00',
        ),
        289 => 
        array (
            'id_mensajes' => 951,
            'titulo' => 'Profit4Life',
            'mensaje' => 'XAUUSD Se mueve stop loss a: 1470.13000000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-19 10:32:00',
            'updated_at' => '2019-11-19 10:32:00',
        ),
        290 => 
        array (
            'id_mensajes' => 952,
            'titulo' => 'Profit4Life',
            'mensaje' => 'XAUUSD Se mueve stop loss a: 1470.29000000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-19 10:32:00',
            'updated_at' => '2019-11-19 10:32:00',
        ),
        291 => 
        array (
            'id_mensajes' => 953,
            'titulo' => 'Profit4Life',
            'mensaje' => 'XAUUSD Se mueve stop loss a: 1470.26000000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-19 10:32:00',
            'updated_at' => '2019-11-19 10:32:00',
        ),
        292 => 
        array (
            'id_mensajes' => 954,
            'titulo' => 'Profit4Life',
            'mensaje' => 'XAUUSD Se mueve stop loss a: 1470.25000000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-19 10:32:00',
            'updated_at' => '2019-11-19 10:32:00',
        ),
        293 => 
        array (
            'id_mensajes' => 955,
            'titulo' => 'Profit4Life',
            'mensaje' => 'XAUUSD Se mueve stop loss a: 1470.31000000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-19 10:32:00',
            'updated_at' => '2019-11-19 10:32:00',
        ),
        294 => 
        array (
            'id_mensajes' => 956,
            'titulo' => 'Profit4Life',
            'mensaje' => 'XAUUSD Se mueve stop loss a: 1470.31000000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-19 10:32:00',
            'updated_at' => '2019-11-19 10:32:00',
        ),
        295 => 
        array (
            'id_mensajes' => 957,
            'titulo' => 'Profit4Life',
            'mensaje' => 'XAUUSD Se mueve stop loss a: 1470.35000000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-19 10:32:00',
            'updated_at' => '2019-11-19 10:32:00',
        ),
        296 => 
        array (
            'id_mensajes' => 958,
            'titulo' => 'Profit4Life',
            'mensaje' => 'XAUUSD Se mueve stop loss a: 1470.36000000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-19 10:32:00',
            'updated_at' => '2019-11-19 10:32:00',
        ),
        297 => 
        array (
            'id_mensajes' => 959,
            'titulo' => 'Profit4Life',
            'mensaje' => 'XAUUSD Se mueve stop loss a: 1470.46000000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-19 10:32:00',
            'updated_at' => '2019-11-19 10:32:00',
        ),
        298 => 
        array (
            'id_mensajes' => 960,
            'titulo' => 'Profit4Life',
            'mensaje' => 'XAUUSD Se mueve stop loss a: 1470.45000000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-19 10:32:00',
            'updated_at' => '2019-11-19 10:32:00',
        ),
        299 => 
        array (
            'id_mensajes' => 961,
            'titulo' => 'Profit4Life',
            'mensaje' => 'XAUUSD Se mueve stop loss a: 1470.50000000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-19 10:32:00',
            'updated_at' => '2019-11-19 10:32:00',
        ),
        300 => 
        array (
            'id_mensajes' => 962,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Long GBPNZD Entrada:2.01144000 TP:2.02944000 SL:2.00807000Pips del TP:180.00000000 Pips SL:33.70000000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-19 21:00:00',
            'updated_at' => '2019-11-19 21:00:00',
        ),
        301 => 
        array (
            'id_mensajes' => 963,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Saliendo por stop loss en el par:GBPNZD',
            'link_imagen' => NULL,
            'created_at' => '2019-11-20 00:13:00',
            'updated_at' => '2019-11-20 00:13:00',
        ),
        302 => 
        array (
            'id_mensajes' => 964,
            'titulo' => 'XRPUSD',
            'mensaje' => 'nueva Profitcall revisar su pestaña de Profitcalls',
            'link_imagen' => NULL,
            'created_at' => '2019-11-20 04:02:00',
            'updated_at' => '2019-11-20 04:02:00',
        ),
        303 => 
        array (
            'id_mensajes' => 965,
            'titulo' => 'XRPUSD',
            'mensaje' => 'cerrar Venta en XRPUSD',
            'link_imagen' => NULL,
            'created_at' => '2019-11-20 15:56:00',
            'updated_at' => '2019-11-20 15:56:00',
        ),
        304 => 
        array (
            'id_mensajes' => 966,
            'titulo' => 'Profit4Life',
            'mensaje' => 'XRPUSD Se mueve stop loss a: 0.25238000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-20 16:39:00',
            'updated_at' => '2019-11-20 16:39:00',
        ),
        305 => 
        array (
            'id_mensajes' => 967,
            'titulo' => 'Profit4Life',
            'mensaje' => 'XRPUSD Se mueve stop loss a: 0.25247000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-20 16:39:00',
            'updated_at' => '2019-11-20 16:39:00',
        ),
        306 => 
        array (
            'id_mensajes' => 968,
            'titulo' => 'Profit4Life',
            'mensaje' => 'XRPUSD Se mueve stop loss a: 0.25246000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-20 16:39:00',
            'updated_at' => '2019-11-20 16:39:00',
        ),
        307 => 
        array (
            'id_mensajes' => 969,
            'titulo' => 'Profit4Life',
            'mensaje' => 'XRPUSD Se mueve stop loss a: 0.25243000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-20 16:39:00',
            'updated_at' => '2019-11-20 16:39:00',
        ),
        308 => 
        array (
            'id_mensajes' => 970,
            'titulo' => 'Profit4Life',
            'mensaje' => 'XRPUSD Se mueve stop loss a: 0.25240000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-20 16:39:00',
            'updated_at' => '2019-11-20 16:39:00',
        ),
        309 => 
        array (
            'id_mensajes' => 971,
            'titulo' => 'Ignorar',
            'mensaje' => 'ignorar mensaje de Xrpusd de mover SL',
            'link_imagen' => NULL,
            'created_at' => '2019-11-20 16:40:00',
            'updated_at' => '2019-11-20 16:40:00',
        ),
        310 => 
        array (
            'id_mensajes' => 972,
            'titulo' => 'Noticias',
            'mensaje' => 'USDJPY',
            'link_imagen' => NULL,
            'created_at' => '2019-11-20 22:44:00',
            'updated_at' => '2019-11-20 22:44:00',
        ),
        311 => 
        array (
            'id_mensajes' => 973,
            'titulo' => 'UPDATE',
            'mensaje' => 'todos los usuarios IOS actualizar su app desde la AppStore',
            'link_imagen' => NULL,
            'created_at' => '2019-11-21 21:32:00',
            'updated_at' => '2019-11-21 21:32:00',
        ),
        312 => 
        array (
            'id_mensajes' => 974,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Short ETHUSD Entrada:161.86000000 TP:153.49000000 SL:163.21000000Pips del TP:80.00000000 Pips SL:17.20000000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-22 03:30:00',
            'updated_at' => '2019-11-22 03:30:00',
        ),
        313 => 
        array (
            'id_mensajes' => 975,
            'titulo' => 'Profit4Life',
            'mensaje' => 'ETHUSD Se mueve stop loss a: 157.91000000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-22 06:49:00',
            'updated_at' => '2019-11-22 06:49:00',
        ),
        314 => 
        array (
            'id_mensajes' => 976,
            'titulo' => 'Profit4Life',
            'mensaje' => 'ETHUSD Se mueve stop loss a: 157.41000000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-22 06:51:00',
            'updated_at' => '2019-11-22 06:51:00',
        ),
        315 => 
        array (
            'id_mensajes' => 977,
            'titulo' => 'Profit4Life',
            'mensaje' => 'ETHUSD Se mueve stop loss a: 157.41000000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-22 06:51:00',
            'updated_at' => '2019-11-22 06:51:00',
        ),
        316 => 
        array (
            'id_mensajes' => 978,
            'titulo' => 'Profit4Life',
            'mensaje' => 'ETHUSD Se mueve stop loss a: 156.39000000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-22 06:53:00',
            'updated_at' => '2019-11-22 06:53:00',
        ),
        317 => 
        array (
            'id_mensajes' => 979,
            'titulo' => 'Profit4Life',
            'mensaje' => 'ETHUSD Se mueve stop loss a: 156.39000000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-22 06:53:00',
            'updated_at' => '2019-11-22 06:53:00',
        ),
        318 => 
        array (
            'id_mensajes' => 980,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Saliendo a ganacia en el par:ETHUSD',
            'link_imagen' => NULL,
            'created_at' => '2019-11-22 06:53:00',
            'updated_at' => '2019-11-22 06:53:00',
        ),
        319 => 
        array (
            'id_mensajes' => 981,
            'titulo' => 'USDJPY',
            'mensaje' => 'operación aún abierta sólo recuerden el SWAP es negativo.',
            'link_imagen' => NULL,
            'created_at' => '2019-11-22 21:00:00',
            'updated_at' => '2019-11-22 21:00:00',
        ),
        320 => 
        array (
            'id_mensajes' => 982,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Short ETHUSD Entrada:150.25000000 TP:141.90000000 SL:155.26000000Pips del TP:80.00000000 Pips SL:53.60000000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-22 23:00:00',
            'updated_at' => '2019-11-22 23:00:00',
        ),
        321 => 
        array (
            'id_mensajes' => 983,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Short XRPUSD Entrada:0.23052000 TP:0.21785000 SL:0.23262000Pips del TP:120.00000000 Pips SL:27.70000000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-24 13:30:00',
            'updated_at' => '2019-11-24 13:30:00',
        ),
        322 => 
        array (
            'id_mensajes' => 984,
            'titulo' => 'Profit4Life',
            'mensaje' => 'ETHUSD Se mueve stop loss a: 146.35000000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-24 14:50:00',
            'updated_at' => '2019-11-24 14:50:00',
        ),
        323 => 
        array (
            'id_mensajes' => 985,
            'titulo' => 'Profit4Life',
            'mensaje' => 'ETHUSD Se mueve stop loss a: 145.85000000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-24 14:50:00',
            'updated_at' => '2019-11-24 14:50:00',
        ),
        324 => 
        array (
            'id_mensajes' => 986,
            'titulo' => 'Profit4Life',
            'mensaje' => 'ETHUSD Se mueve stop loss a: 145.85000000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-24 14:50:00',
            'updated_at' => '2019-11-24 14:50:00',
        ),
        325 => 
        array (
            'id_mensajes' => 987,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Saliendo a ganacia en el par:ETHUSD',
            'link_imagen' => NULL,
            'created_at' => '2019-11-24 14:51:00',
            'updated_at' => '2019-11-24 14:51:00',
        ),
        326 => 
        array (
            'id_mensajes' => 988,
            'titulo' => 'Profit4Life',
            'mensaje' => 'XRPUSD Se mueve stop loss a: 0.22480000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-24 16:15:00',
            'updated_at' => '2019-11-24 16:15:00',
        ),
        327 => 
        array (
            'id_mensajes' => 989,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Saliendo a ganacia en el par:XRPUSD',
            'link_imagen' => NULL,
            'created_at' => '2019-11-24 16:18:00',
            'updated_at' => '2019-11-24 16:18:00',
        ),
        328 => 
        array (
            'id_mensajes' => 990,
            'titulo' => 'Noticias',
            'mensaje' => 'USDCAD',
            'link_imagen' => NULL,
            'created_at' => '2019-11-25 07:23:00',
            'updated_at' => '2019-11-25 07:23:00',
        ),
        329 => 
        array (
            'id_mensajes' => 991,
            'titulo' => 'Noticias',
            'mensaje' => 'Criptomonedas',
            'link_imagen' => NULL,
            'created_at' => '2019-11-25 14:29:00',
            'updated_at' => '2019-11-25 14:29:00',
        ),
        330 => 
        array (
            'id_mensajes' => 992,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Short ETHUSD Entrada:148.09000000 TP:137.64000000 SL:152.48000000Pips del TP:100.00000000 Pips SL:48.40000000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-25 20:00:00',
            'updated_at' => '2019-11-25 20:00:00',
        ),
        331 => 
        array (
            'id_mensajes' => 993,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Short ETHUSD Entrada:148.55000000 TP:138.19000000 SL:150.68000000Pips del TP:100.00000000 Pips SL:24.90000000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-26 07:00:00',
            'updated_at' => '2019-11-26 07:00:00',
        ),
        332 => 
        array (
            'id_mensajes' => 994,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Saliendo a ganacia en el par:ETHUSD',
            'link_imagen' => NULL,
            'created_at' => '2019-11-26 17:59:00',
            'updated_at' => '2019-11-26 17:59:00',
        ),
        333 => 
        array (
            'id_mensajes' => 995,
            'titulo' => 'ETHUSD',
            'mensaje' => 'Mover SL a 147',
            'link_imagen' => NULL,
            'created_at' => '2019-11-26 18:01:00',
            'updated_at' => '2019-11-26 18:01:00',
        ),
        334 => 
        array (
            'id_mensajes' => 996,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Short XRPUSD Entrada:0.22069000 TP:0.20805000 SL:0.22468000Pips del TP:120.00000000 Pips SL:46.30000000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-27 04:00:00',
            'updated_at' => '2019-11-27 04:00:00',
        ),
        335 => 
        array (
            'id_mensajes' => 997,
            'titulo' => 'ETHUSD',
            'mensaje' => 'cerrar Operación con Ganancia',
            'link_imagen' => NULL,
            'created_at' => '2019-11-27 04:20:00',
            'updated_at' => '2019-11-27 04:20:00',
        ),
        336 => 
        array (
            'id_mensajes' => 998,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Long GBPAUD Entrada:1.89745000 TP:1.91245000 SL:1.89198000Pips del TP:150.00000000 Pips SL:54.70000000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-27 05:00:00',
            'updated_at' => '2019-11-27 05:00:00',
        ),
        337 => 
        array (
            'id_mensajes' => 999,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Saliendo por stop loss en el par:XRPUSD',
            'link_imagen' => NULL,
            'created_at' => '2019-11-27 16:49:00',
            'updated_at' => '2019-11-27 16:49:00',
        ),
        338 => 
        array (
            'id_mensajes' => 1000,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Saliendo a ganacia en el par:GBPAUD',
            'link_imagen' => NULL,
            'created_at' => '2019-11-27 18:44:00',
            'updated_at' => '2019-11-27 18:44:00',
        ),
        339 => 
        array (
            'id_mensajes' => 1001,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Short GBPNZD Entrada:2.01136000 TP:1.99317000 SL:2.02090000Pips del TP:180.00000000 Pips SL:97.30000000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-28 01:00:00',
            'updated_at' => '2019-11-28 01:00:00',
        ),
        340 => 
        array (
            'id_mensajes' => 1002,
            'titulo' => 'USDCAD',
            'mensaje' => 'Cerrar orden pendiente',
            'link_imagen' => NULL,
            'created_at' => '2019-11-28 04:33:00',
            'updated_at' => '2019-11-28 04:33:00',
        ),
        341 => 
        array (
            'id_mensajes' => 1003,
            'titulo' => 'Profit4Life',
            'mensaje' => 'GBPNZD Se mueve stop loss a: 2.00547000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-29 09:06:00',
            'updated_at' => '2019-11-29 09:06:00',
        ),
        342 => 
        array (
            'id_mensajes' => 1004,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Saliendo a ganacia en el par:GBPNZD',
            'link_imagen' => NULL,
            'created_at' => '2019-11-29 09:09:00',
            'updated_at' => '2019-11-29 09:09:00',
        ),
        343 => 
        array (
            'id_mensajes' => 1005,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Long GBPAUD Entrada:1.90538000 TP:1.92038000 SL:1.90029000Pips del TP:150.00000000 Pips SL:50.90000000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-29 14:00:00',
            'updated_at' => '2019-11-29 14:00:00',
        ),
        344 => 
        array (
            'id_mensajes' => 1006,
            'titulo' => 'Profit4Life',
            'mensaje' => 'GBPAUD Se mueve stop loss a: 1.91144000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-29 16:01:00',
            'updated_at' => '2019-11-29 16:01:00',
        ),
        345 => 
        array (
            'id_mensajes' => 1007,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Saliendo a ganacia en el par:GBPAUD',
            'link_imagen' => NULL,
            'created_at' => '2019-11-29 16:04:00',
            'updated_at' => '2019-11-29 16:04:00',
        ),
        346 => 
        array (
            'id_mensajes' => 1008,
            'titulo' => 'Feliz Viernes A todos',
            'mensaje' => 'Cerrando más de 100 pips Positivos Feliz Viernes a Todos.',
            'link_imagen' => NULL,
            'created_at' => '2019-11-29 16:06:00',
            'updated_at' => '2019-11-29 16:06:00',
        ),
        347 => 
        array (
            'id_mensajes' => 1009,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Short XAUUSD Entrada:1464.22000000 TP:1443.97000000 SL:1466.55000000Pips del TP:200.00000000 Pips SL:25.80000000',
            'link_imagen' => NULL,
            'created_at' => '2019-11-29 18:00:00',
            'updated_at' => '2019-11-29 18:00:00',
        ),
        348 => 
        array (
            'id_mensajes' => 1010,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Short XRPUSD Entrada:0.22354000 TP:0.21088000 SL:0.22615000Pips del TP:120.00000000 Pips SL:32.70000000',
            'link_imagen' => NULL,
            'created_at' => '2019-12-01 15:30:00',
            'updated_at' => '2019-12-01 15:30:00',
        ),
        349 => 
        array (
            'id_mensajes' => 1011,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Saliendo por stop loss en el par:XRPUSD',
            'link_imagen' => NULL,
            'created_at' => '2019-12-01 22:01:00',
            'updated_at' => '2019-12-01 22:01:00',
        ),
        350 => 
        array (
            'id_mensajes' => 1012,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Short GBPNZD Entrada:2.00540000 TP:1.98715000 SL:2.01793000Pips del TP:180.00000000 Pips SL:127.80000000',
            'link_imagen' => NULL,
            'created_at' => '2019-12-02 01:00:00',
            'updated_at' => '2019-12-02 01:00:00',
        ),
        351 => 
        array (
            'id_mensajes' => 1013,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Saliendo a ganacia en el par:XAUUSD',
            'link_imagen' => NULL,
            'created_at' => '2019-12-02 02:32:00',
            'updated_at' => '2019-12-02 02:32:00',
        ),
        352 => 
        array (
            'id_mensajes' => 1014,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Saliendo a ganacia en el par:GBPNZD',
            'link_imagen' => NULL,
            'created_at' => '2019-12-02 08:09:00',
            'updated_at' => '2019-12-02 08:09:00',
        ),
        353 => 
        array (
            'id_mensajes' => 1015,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Saliendo por stop loss en el par:GBPAUD',
            'link_imagen' => NULL,
            'created_at' => '2019-12-02 15:20:00',
            'updated_at' => '2019-12-02 15:20:00',
        ),
        354 => 
        array (
            'id_mensajes' => 1016,
            'titulo' => 'Noticias',
            'mensaje' => 'EURUSD BUY',
            'link_imagen' => NULL,
            'created_at' => '2019-12-04 23:16:00',
            'updated_at' => '2019-12-04 23:16:00',
        ),
        355 => 
        array (
            'id_mensajes' => 1017,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Short GBPNZD Entrada:2.00242000 TP:1.98410000 SL:2.00844000Pips del TP:180.00000000 Pips SL:63.40000000',
            'link_imagen' => NULL,
            'created_at' => '2019-12-05 00:00:00',
            'updated_at' => '2019-12-05 00:00:00',
        ),
        356 => 
        array (
            'id_mensajes' => 1018,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Saliendo por stop loss en el par:GBPNZD',
            'link_imagen' => NULL,
            'created_at' => '2019-12-05 07:06:00',
            'updated_at' => '2019-12-05 07:06:00',
        ),
        357 => 
        array (
            'id_mensajes' => 1019,
            'titulo' => 'EURUSD',
            'mensaje' => 'Cerrar Con Ganancia',
            'link_imagen' => NULL,
            'created_at' => '2019-12-05 16:51:00',
            'updated_at' => '2019-12-05 16:51:00',
        ),
        358 => 
        array (
            'id_mensajes' => 1020,
            'titulo' => 'Viernes de NFP',
            'mensaje' => 'recuerden mañana es Viernes de NFP los pares USD se verán Afectados',
            'link_imagen' => NULL,
            'created_at' => '2019-12-05 21:27:00',
            'updated_at' => '2019-12-05 21:27:00',
        ),
        359 => 
        array (
            'id_mensajes' => 1021,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Long GBPUSD Entrada:1.31260000 TP:1.32760000 SL:1.31002000Pips del TP:150.00000000 Pips SL:25.80000000',
            'link_imagen' => NULL,
            'created_at' => '2019-12-06 17:00:00',
            'updated_at' => '2019-12-06 17:00:00',
        ),
        360 => 
        array (
            'id_mensajes' => 1022,
            'titulo' => 'Noticias',
            'mensaje' => 'Nuevo espacio de Psicología',
            'link_imagen' => NULL,
            'created_at' => '2019-12-06 18:29:00',
            'updated_at' => '2019-12-06 18:29:00',
        ),
        361 => 
        array (
            'id_mensajes' => 1023,
            'titulo' => 'Noticias',
            'mensaje' => 'PsychologySpace 001',
            'link_imagen' => NULL,
            'created_at' => '2019-12-06 21:34:00',
            'updated_at' => '2019-12-06 21:34:00',
        ),
        362 => 
        array (
            'id_mensajes' => 1024,
            'titulo' => 'Profit4Life',
            'mensaje' => 'GBPUSD Se mueve stop loss a: 1.31661000',
            'link_imagen' => NULL,
            'created_at' => '2019-12-09 06:31:00',
            'updated_at' => '2019-12-09 06:31:00',
        ),
        363 => 
        array (
            'id_mensajes' => 1025,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Saliendo a ganacia en el par:GBPUSD',
            'link_imagen' => NULL,
            'created_at' => '2019-12-09 07:08:00',
            'updated_at' => '2019-12-09 07:08:00',
        ),
        364 => 
        array (
            'id_mensajes' => 1026,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Short GBPNZD Entrada:2.00430000 TP:1.98598000 SL:2.01002000Pips del TP:180.00000000 Pips SL:60.40000000',
            'link_imagen' => NULL,
            'created_at' => '2019-12-09 15:00:00',
            'updated_at' => '2019-12-09 15:00:00',
        ),
        365 => 
        array (
            'id_mensajes' => 1027,
            'titulo' => 'Noticias',
            'mensaje' => 'USDCAD',
            'link_imagen' => NULL,
            'created_at' => '2019-12-09 15:03:00',
            'updated_at' => '2019-12-09 15:03:00',
        ),
        366 => 
        array (
            'id_mensajes' => 1028,
            'titulo' => 'Noticias',
            'mensaje' => 'USDCAD',
            'link_imagen' => NULL,
            'created_at' => '2019-12-09 15:09:00',
            'updated_at' => '2019-12-09 15:09:00',
        ),
        367 => 
        array (
            'id_mensajes' => 1029,
            'titulo' => 'Noticias',
            'mensaje' => 'GBPCAD',
            'link_imagen' => NULL,
            'created_at' => '2019-12-09 15:22:00',
            'updated_at' => '2019-12-09 15:22:00',
        ),
        368 => 
        array (
            'id_mensajes' => 1030,
            'titulo' => 'Noticias',
            'mensaje' => 'USDCAD',
            'link_imagen' => NULL,
            'created_at' => '2019-12-09 15:45:00',
            'updated_at' => '2019-12-09 15:45:00',
        ),
        369 => 
        array (
            'id_mensajes' => 1031,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Saliendo por stop loss en el par:GBPNZD',
            'link_imagen' => NULL,
            'created_at' => '2019-12-10 09:01:00',
            'updated_at' => '2019-12-10 09:01:00',
        ),
        370 => 
        array (
            'id_mensajes' => 1032,
            'titulo' => 'GBPCAD',
            'mensaje' => 'orden pendiente Activada',
            'link_imagen' => NULL,
            'created_at' => '2019-12-10 13:31:00',
            'updated_at' => '2019-12-10 13:31:00',
        ),
        371 => 
        array (
            'id_mensajes' => 1033,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Short GBPNZD Entrada:2.01216000 TP:1.99391000 SL:2.01776000Pips del TP:180.00000000 Pips SL:58.50000000',
            'link_imagen' => NULL,
            'created_at' => '2019-12-10 14:00:00',
            'updated_at' => '2019-12-10 14:00:00',
        ),
        372 => 
        array (
            'id_mensajes' => 1034,
            'titulo' => 'Profit4Life',
            'mensaje' => 'Saliendo por stop loss en el par:GBPNZD',
            'link_imagen' => NULL,
            'created_at' => '2019-12-10 19:53:00',
            'updated_at' => '2019-12-10 19:53:00',
        ),
    ));
        
        
    }
}