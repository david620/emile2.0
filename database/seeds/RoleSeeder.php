<?php

use Illuminate\Database\Seeder;
use App\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create([
            'name' => 'ADMINISTRADOR',
            'display_name' => 'Administrador',
            'description'     => 'Administrador del Sistema'
        ]);
        Role::create([
            'name' => 'USUARIO',
            'display_name' => 'Usuario',
            'description'     => 'Usuario del Sistema'
        ]);
        Role::create([
            'name' => 'Vendedor',
            'display_name' => 'Vendedor',
            'description'     => 'Vendedor del Sistema'
        ]);
    }
}
