<?php

use Illuminate\Database\Seeder;

class FlipbookTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('flipbook')->delete();
        
        \DB::table('flipbook')->insert(array (
            0 => 
            array (
                'id' => 4,
                'name' => 'Scalping con Profit 4 Life',
                'desc' => 'Scalping con Profit 4 Life',
                'content' => 'flipbook/fbook/pics/1532805644_1.jpg,flipbook/fbook/pics/1532805644_2.jpg,flipbook/fbook/pics/1532805644_3.jpg,flipbook/fbook/pics/1532805644_4.jpg,flipbook/fbook/pics/1532805938_1.jpg,flipbook/fbook/pics/1532805938_2.jpg,flipbook/fbook/pics/1532805938_3.jpg,flipbook/fbook/pics/1532805938_4.jpg,flipbook/fbook/pics/1532805938_5.jpg,flipbook/fbook/pics/1532805938_6.jpg,flipbook/fbook/pics/1532805938_7.jpg,flipbook/fbook/pics/1532805938_8.jpg,flipbook/fbook/pics/1532809790_1.jpg,flipbook/fbook/pics/1532809790_2.jpg,flipbook/fbook/pics/1532809790_3.jpg,flipbook/fbook/pics/1532809790_4.jpg,flipbook/fbook/pics/1532809790_5.jpg,flipbook/fbook/pics/1532809790_6.jpg,flipbook/fbook/pics/1532809790_7.jpg,flipbook/fbook/pics/1532809790_8.jpg,flipbook/fbook/pics/1532815840_1.jpg,flipbook/fbook/pics/1532815840_2.jpg,flipbook/fbook/pics/1532815840_3.jpg,flipbook/fbook/pics/1532815840_4.jpg,flipbook/fbook/pics/1532815840_5.jpg,flipbook/fbook/pics/1532815840_6.jpg,flipbook/fbook/pics/1532815840_7.jpg,flipbook/fbook/pics/1532815840_8.jpg,flipbook/fbook/pics/1532820566_2.jpg,flipbook/fbook/pics/1532820566_3.jpg,flipbook/fbook/pics/1532820566_4.jpg,flipbook/fbook/pics/1532820566_5.jpg,flipbook/fbook/pics/1532820566_6.jpg,flipbook/fbook/pics/1532820566_7.jpg,flipbook/fbook/pics/1532820566_8.jpg,flipbook/fbook/pics/1532820566_9.jpg,flipbook/fbook/pics/1532820566_10.jpg,flipbook/fbook/pics/1532820566_11.jpg,flipbook/fbook/pics/blanco.jpg,flipbook/fbook/pics/1532820566_12.jpg',
                'habilitado' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 5,
                'name' => 'Cripto4Life 1.0',
                'desc' => 'Cripto4Life 1.0',
                'content' => 'flipbook/fbook/pics2/cript.jpg,flipbook/fbook/pics2/cripto.jpg,flipbook/fbook/pics2/cripto0.jpg,flipbook/fbook/pics2/cripto1.jpg,flipbook/fbook/pics2/cripto2.jpg,flipbook/fbook/pics2/cripto3.jpg,flipbook/fbook/pics2/cripto4.jpg,flipbook/fbook/pics2/cripto5.jpg,flipbook/fbook/pics2/cripto6.jpg,flipbook/fbook/pics2/cripto7.jpg,flipbook/fbook/pics2/cripto8.jpg,flipbook/fbook/pics2/cripto9.jpg,flipbook/fbook/pics2/cripto10.jpg,flipbook/fbook/pics2/cripto11.jpg,flipbook/fbook/pics2/cripto12.jpg,flipbook/fbook/pics2/cripto13.jpg,flipbook/fbook/pics2/cripto14.jpg,flipbook/fbook/pics2/cripto15.jpg,flipbook/fbook/pics2/cripto16.jpg,flipbook/fbook/pics2/cripto17.jpg,flipbook/fbook/pics2/cripto18.jpg,flipbook/fbook/pics2/cripto19.jpg,flipbook/fbook/pics2/cripto20.jpg,flipbook/fbook/pics2/cripto21.jpg,flipbook/fbook/pics2/cripto22.jpg,flipbook/fbook/pics2/cripto23.jpg,flipbook/fbook/pics2/cripto24.jpg',
                'habilitado' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}