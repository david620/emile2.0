<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersConcursoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_concurso', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre')->nullable();            
            $table->string('apellido')->nullable();  
            $table->string('email')->nullable();            
            $table->string('telefono')->nullable();  
            $table->string('pais')->nullable();  
            $table->unsignedInteger('concurso_id')->nullable();
            $table->foreign('concurso_id')->references('id')->on('concursos')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_concurso');
    }
}
