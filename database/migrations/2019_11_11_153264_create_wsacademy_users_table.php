<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWsacademyUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wsacademy_users', function (Blueprint $table) {
            $table->increments('id');
            $table->double('monto_wsacademy')->nullable();            
            $table->string('mail_wsacademy')->nullable();  
            $table->dateTime('date_wsacademy')->nullable();            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wsacademy_users');
    }
}
