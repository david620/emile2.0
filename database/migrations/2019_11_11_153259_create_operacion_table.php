<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOperacionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('operacion', function (Blueprint $table) {
            $table->increments('id');
            $table->string('symbol')->nullable();            
            $table->double('price')->nullable();
            $table->string('tipo')->nullable();
            $table->double('close')->nullable();
            $table->string('status')->nullable();
            $table->dateTime('fecha_inicio')->nullable();
            $table->dateTime('fecha_fin')->nullable();
            $table->double('sl')->nullable();
            $table->double('tp')->nullable();
            $table->double('price_cierre')->nullable();
            $table->bigInteger('ticket')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('operacion');
    }
}
