<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersCursoVivoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_curso_vivo', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre')->nullable();            
            $table->string('apellido')->nullable();  
            $table->string('email')->nullable();            
            $table->string('telefono')->nullable();    
            $table->integer('modalidad')->nullable();          
            $table->integer('curso')->nullable();
            $table->integer('task_id')->nullable();          
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_curso_vivo');
    }
}
