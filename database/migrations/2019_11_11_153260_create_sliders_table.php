<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSlidersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sliders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('sliders_ruta_imagen')->nullable();            
            $table->tinyInteger('plataforma')->nullable();
            $table->string('sliders_descripcion')->nullable();
            $table->string('sliders_descripcion_2')->nullable();
            $table->string('sliders_descripcion_3')->nullable();
            $table->string('sliders_descripcion_4')->nullable();
            $table->string('sliders_descripcion_5')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sliders');
    }
}
