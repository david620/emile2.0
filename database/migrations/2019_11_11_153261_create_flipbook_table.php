<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFlipbookTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('flipbook', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();            
            $table->string('desc')->nullable();  
            $table->text('content')->nullable();          
            $table->tinyInteger('habilitado')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('flipbook');
    }
}
