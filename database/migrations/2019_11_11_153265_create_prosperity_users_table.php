<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProsperityUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prosperity_users', function (Blueprint $table) {
            $table->increments('id');
            $table->double('monto_prosperity')->nullable();            
            $table->string('mail_prosperity')->nullable();  
            $table->dateTime('date_prosperity')->nullable();            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prosperity_users');
    }
}
