<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlarmasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alarmas', function (Blueprint $table) {
            $table->increments('id_alarmas');
            $table->integer('id_cotizacion')->nullable();
            $table->double('precio')->nullable();
            $table->double('inicio')->nullable();
            $table->integer('id_usuario')->nullable();
            $table->dateTime('fecha')->nullable();
            $table->tinyInteger('ejecutado')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alarmas');
    }
}
