<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre', 50)->nullable();
            $table->string('apellido', 50)->nullable();
            $table->string('nickname')->nullable();
            $table->string('pais')->nullable();
            $table->string('phone')->nullable();
            $table->string('email')->unique();
            $table->text('token')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->date('fecha_duracion_app')->nullable();
            $table->tinyInteger('habilitado')->nullable();
            $table->tinyInteger('habilitado_app')->nullable();
            $table->integer('plataforma')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
