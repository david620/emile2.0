<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserCursoVivo extends Model {
	protected $table = 'users_curso_vivo';
	protected $primaryKey = 'id';
	public $timestamps = true;
    public $incrementing = true;

	
	protected $fillable = [
		'*',
	];

}