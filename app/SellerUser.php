<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SellerUser extends Model {
	protected $table = 'seller_user';
	protected $primaryKey = 'id';
    public $incrementing = true;
	
	protected $fillable = [
		'*',
	];

}