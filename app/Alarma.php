<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Alarma extends Model {

	protected $table = 'alarmas';
	protected $primaryKey = 'id_alarmas';
    public $incrementing = true;
	public $timestamps = false;
	
	protected $fillable = [
		'*',
	];

}