<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProsperityUser extends Model {
	protected $table = 'prosperity_users';
	protected $primaryKey = 'id';
	
    public $incrementing = true;
	
	protected $fillable = [
		'*',
	];

}