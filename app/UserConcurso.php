<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserConcurso extends Model {
	protected $table = 'users_concurso';
	protected $primaryKey = 'id';
    public $incrementing = true;

	
	protected $fillable = [
		'*',
	];

}