<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Operacion extends Model {
	protected $table = 'operacion';
	protected $primaryKey = 'id';
    public $incrementing = true;	
    
	protected $fillable = [
		'*',
	];

}