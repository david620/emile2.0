<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Flipbook extends Model {
	protected $table = 'flipbook';
	protected $primaryKey = 'id';
	public $timestamps = true;
    public $incrementing = true;

	protected $fillable = [
		'*',
	];

}