<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    //
	protected $fillable = ['title', 'price', 'payment_habilitado', 'user_id'];

    public function getPaidAttribute() {
    	if ($this->payment_habilitado == 'Invalid') {
    		return false;
    	}
    	return true;
    }
}
