<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppleCode extends Model {
	protected $table = 'apple_codes';
	protected $primaryKey = 'id';
	
    public $incrementing = true;
	
	protected $fillable = [
		'*',
	];

}