<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PagoApp extends Model {
	protected $table = 'pagos_app';
	protected $primaryKey = 'id';
    public $incrementing = true;
	
	protected $fillable = [
		'*',
	];

}