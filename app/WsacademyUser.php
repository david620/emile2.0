<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WsacademyUser extends Model {
	protected $table = 'wsacademy_users';
	protected $primaryKey = 'id';
	
    public $incrementing = true;
	
	protected $fillable = [
		'*',
	];

}