<?php

namespace App\Http\Middleware;

use Closure;
use App\Servicio;
use DB;
use Auth;

class CheckService
{
    public function handle($request, Closure $next, $servicio)
    {
                $services = DB::table('servicios')
                    ->join('users_servicios', 'users_servicios.servicio_id', '=', 'servicios.id')
                    ->join('users', 'users.id', '=', 'users_servicios.user_id')
                    ->where('users_servicios.user_id', '=', Auth::user()->id)
                    ->select('servicios.*')            
                    ->distinct()            
                    ->get();
                    dd($services);

        foreach ($services as $service) {
            if (($service->id == $servicio) ) {
                dd($servicio);
                return redirect('login');
            }
        }

        return $next($request);
    }
}
