<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Task;
use App\User;
use App\PagosApp;
use App\Servicio;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Redirect;
use Response;
use Carbon\Carbon;

class WsAcademyController extends Controller
{
    protected $urlgetopab = null;
    protected $urlgetopce = null;
    protected $urlmarket = null;
    protected $urlsavetoken = null;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function app_index(Request $request){
            $servicio = Servicio::all();
            return view('wsacademy.app', compact('servicio'));            
    }


    public function index_wsacademy(Request $request){
            $users = DB::table('wsacademy_users')
                ->select('wsacademy_users.*')
                ->orderBy('date_wsacademy', 'DESC')                
                ->get();

        return view('app.index_wsacademy', compact('users'));          
    }

    public function index_users_wsacademy()
    {
            $users = DB::table('wsacademy_users')
                ->select('wsacademy_users.*')
                ->get();

            return response()->json([
                'success' => true,
                'users' => $users
            ]);  
    }


    public function wsacademy_store(Request $request){

        $arreglo = ['email' => $request->email, 'password' => $request->password, 'name' => $request->name];

        $request->session()->put('wsacademy.app.data', $arreglo); // guardamos usuario en session

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,"https://prosperitytrader.net/api/auth/login");
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, 'email='.$request->email.'&password='.$request->password.'&tokenapn=fdsgdf');         
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $remote_server_output = curl_exec ($ch);         
        curl_close ($ch);
        $respuesta = json_decode($remote_server_output);
        

        if($respuesta->acceso == true){
            return redirect()->route('paypal.express-checkout-wsacademy');
        }

        if($respuesta->acceso == false){
            return redirect()->route('paypal.express-checkout-wsacademy-new');
        }

        abort(404);

    }
 

}
