<?php
  
namespace App\Http\Controllers;
  
use Illuminate\Http\Request;
use Srmklive\PayPal\Services\ExpressCheckout;
use App\Http\Requests;
use Redirect;
use Session;  
use DB;  
use App\User;
use App\Servicio;
use Carbon\Carbon;
use App\Invoice;


class PayPalAppController extends Controller
{
	protected $provider;

	public function __construct() {
	    $this->provider = new ExpressCheckout();
	}


    public function payment()
    {
        $servicio = Servicio::all();
        $invoice = new Invoice();
        $invoice->save();

            $data['items'] = [
                [
                    'name' => 'Profit Calls',
                    'price' => $servicio[2]->precio_servicio,
                    'desc'  => 'Servicios Aplicación Profit 4 Life',
                    'qty' => 1
                ]
            ];
            $data['invoice_id'] = $invoice->id;
            $data['invoice_description'] = "Order #{$data['invoice_id']} Invoice";
            $data['return_url'] = route('payment.success-app');
            $data['cancel_url'] = route('payment.cancel-app');
            $data['total'] = $servicio[2]->precio_servicio;
  
        $response =  $this->provider->setExpressCheckout($data);
        $response =  $this->provider->setExpressCheckout($data, true);
  
        return redirect($response['paypal_link']);
    }
   
    /**
     * Responds with a welcome message with instructions
     *
     * @return \Illuminate\Http\Response
     */
    public function cancel()
    {
       	return view('errors.cancel_paypal');
    }
  
    /**
     * Responds with a welcome message with instructions
     *
     * @return \Illuminate\Http\Response
     */
    public function success(Request $request)
    {
        $mytime = Carbon::now();
        $datetime = $mytime->addMonth();
        $app_sesion = Session::get('profit.app.data');

        $user = DB::table('users')
            ->select('users.*')
            ->where('email', '=', $app_sesion['email'])
            ->get();


        $response =  $this->provider->getExpressCheckoutDetails($request->token);
  
        if (in_array(strtoupper($response['ACK']), ['SUCCESS', 'SUCCESSWITHWARNING'])) {

                        if(count($user) != 0){
                            $usuario = User::find($user[0]->id);
                            $usuario->habilitado_app = 1;
                            $usuario->fecha_duracion_app = $datetime;
                            $usuario->save();
                            DB::insert('insert into users_servicios (servicio_id, user_id) values(?,?)', [3, $usuario->id]);
                        }
                        else{
                            $new_user = new User();
                            $new_user->nombre = $app_sesion['nombre'];
                            $new_user->apellido = $app_sesion['apellido'];
                            $new_user->phone = $app_sesion['phone'];
                            $new_user->email = $app_sesion['email'];
                            $new_user->password = $app_sesion['password'];
                            $new_user->habilitado = 1;
                            $new_user->habilitado_app = 1;
                            $new_user->fecha_duracion_app = $datetime;
                            $new_user->save();
                            DB::insert('insert into role_user (user_id, role_id) values(?,?)', [$new_user->id, 2]);
                            DB::insert('insert into users_servicios (servicio_id, user_id) values(?,?)', [3, $new_user->id]);
                        }
            Session::flush();        
        	return view('errors.success_paypal');
        }
        	return view('errors.error_paypal');
    }
}