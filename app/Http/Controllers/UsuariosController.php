<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Servicio;
use DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Redirect;
use App\Exports\UsersExport;
use Maatwebsite\Excel\Facades\Excel;
use Carbon\Carbon;

class UsuariosController extends Controller
{


    public function index()
    {
        //$videos = Video::orderBy('id', 'DESC')->get();
        $usuarios = User::where('id', '!=' , Auth::id())->orderBy('id', 'DESC')->get();
        //$usuarios = DB::table('users')
        //    ->join('users_servicios', 'users_servicios.user_id', '=', 'users.id')
        //    ->join('servicios', 'servicios.id', '=', 'users_servicios.servicio_id')
        //    ->select('users.*', 'servicios.*')
        //    ->get();

        return view('usuarios.index', compact('usuarios'));
    }

    public function intermedia()
    {
        $usuarios = User::All();
        $mytime = Carbon::now();
        $datetime = $mytime->addMonth();
/*
        for($i=0; $i<count($usuarios);$i++){
            $usuarios[$i]->habilitado_app = 0;
            $usuarios[$i]->fecha_duracion_app = "2019-12-15";
            $usuarios[$i]->save();
        }
*/


        for($i=count($usuarios) - 100; $i<count($usuarios);$i++){
            $usuarios[$i]->habilitado_app = 1;
            $usuarios[$i]->fecha_duracion_app = "2019-12-15";
            $usuarios[$i]->save();
            DB::insert('insert into users_servicios (servicio_id, user_id) values(?,?)', [3, $usuarios[$i]->id]);
        }



        dd("fin");
    }


    public function create()
    {
        $servicios = Servicio::pluck('nombre_servicio', 'id');
        return view('usuarios.create', compact('servicios'));
    }


    public function edit($id)
    {
        $usuario = User::find($id);
        $servicios = Servicio::pluck('nombre_servicio', 'id');
        $servicio = DB::table('servicios')
                    ->join('users_servicios', 'users_servicios.servicio_id', '=', 'servicios.id')
                    ->join('users', 'users.id', '=', 'users_servicios.user_id')
                    ->where('users.id', '=', $usuario->id)
                    ->select('servicios.id')
                    ->pluck('id');

        $servicios_pendientes = DB::table('users_servicios_pending')
                    ->where('users_servicios_pending.user_id', '=', $usuario->id)
                    ->select('users_servicios_pending.*')
                    ->get();

        $bandera = 0;

        if(count($servicios_pendientes)){
            $bandera = 1; 
        }

        return view('usuarios.edit', compact('usuario', 'servicios', 'servicio', 'bandera'));
    }

    public function store(Request $request)
    {
        $mytime = Carbon::now();
        $datetime = $mytime->addMonth();

        $this->validate($request, [
            'nombre' => 'required',
            'apellido' => 'required',
            'phone' => 'required',
            'email' => 'required|unique:users',
            'password' => 'required',
            'services' => 'required',

        ]);

        $user = new User();
        $user->nombre = $request->input('nombre');
        $user->apellido = $request->input('apellido');
        $user->phone = $request->input('phone');
        $user->email = $request->input('email');
        $user->password = bcrypt($request->password);
        $user->habilitado = 1;        
        $user->habilitado_app = 0;

        $user->save();

        DB::insert('insert into role_user (user_id, role_id) values(?,?)', [$user->id, 2]);
        foreach ($request->services as $servicio) {
            DB::insert('insert into users_servicios (servicio_id, user_id) values(?,?)', [$servicio, $user->id]);
            if($servicio == 3){
                $user->fecha_duracion_app = $datetime;
                $user->habilitado_app = 1;
                $user->save();

            }
        }

        return redirect()->route('usuarios.index')
                        ->with('success','Usuario creado exitosamente');
    }


    public function update(Request $request, $id)
    {
        $mytime = Carbon::now();
        $datetime = $mytime->addMonth();


        $this->validate($request, [
            'nombre' => 'required',
            'apellido' => 'required',
            'phone' => 'required',
            'services' => 'required',

        ]);

        $user = User::find($id);
        $user->nombre = $request->input('nombre');
        $user->apellido = $request->input('apellido');
        $user->phone = $request->input('phone');
        $user->email = $request->input('email');
        if (!empty($request->password)) {
            $user->password = bcrypt($request->password);
        }
        $user->habilitado = 1;
        $user->update();

        foreach ($request->services as $servicio) {
            DB::delete('delete from users_servicios where user_id = :id', ['id' => $user->id]);
            if($servicio == 3){
                $user->habilitado_app = 0;
                $user->update();
            }
        }
        foreach ($request->services as $servicio) {
            DB::insert('insert into users_servicios (servicio_id, user_id) values(?,?)', [$servicio, $user->id]);
            if($servicio == 3){
                $user->fecha_duracion_app = $datetime;
                $user->habilitado_app = 1;
                $user->update();

            }
        }


        return redirect()->route('usuarios.index')
                        ->with('success','Usuario creado exitosamente');
    }



    public function destroy($id)
    {
        DB::table("users")->where('id',$id)->delete();
        return redirect()->route('usuarios.index')
                        ->with('success','Usuario Eliminado con Éxito');
    }

    public function export() 
    {
        return Excel::download(new UsersExport, 'users_list.xlsx');
    }

    public function modificaEstado($id)
    {
        $user = User::findOrfail($id);

        if ($user->habilitado == '0') {
            $user->habilitado = '1';
            $user->save();
        }
        else{
            $user->habilitado = '0';
            $user->save();           
        }

        return redirect()->route('usuarios.index')->with('success', 'Estado Usuario Cambiado Exitosamente');
    }


    public function habilitarServicios($id)
    {
        $mytime = Carbon::now();
        $datetime = $mytime->addMonth();

        $servicios_pendientes = DB::table('users_servicios_pending')
                    ->where('users_servicios_pending.user_id', '=', $id)
                    ->select('users_servicios_pending.*')
                    ->get();

        foreach ($servicios_pendientes as $key => $value) {
            DB::insert('insert into users_servicios (servicio_id, user_id) values (?, ?)', [$value->servicio_id, $value->user_id]);
                                if($value->servicio_id == 3){
                                    $user = User::find($id);
                                    $user->habilitado_app = 1;
                                    $user->fecha_duracion_app = $datetime;
                                    $user->update();
                                }
        }

        foreach ($servicios_pendientes as $key => $value) {
            DB::delete('delete from users_servicios_pending where user_id = :id', ['id' =>$value->user_id]);
        }

        return redirect()->route('usuarios.index')->with('success', 'Servicios Habilitados Exitosamente');
    }

}
