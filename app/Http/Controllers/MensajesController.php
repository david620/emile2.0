<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mensaje;
use DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Redirect;

class MensajesController extends Controller
{


    public function index()
    {
        $messages = Mensaje::orderBy('id_mensajes', 'DESC')->get();

            return response()->json([
                'success' => true,
                'mensajes' => $messages
            ]);  
    }

    public function store(Request $request)
    {
        $messages= Mensaje::create($request->all());

            return response()->json([
                'success' => true,
                'mensaje' => $messages
            ]);   
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Mensaje $messages)
    {
        return $messages;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    public function destroy($id)
    {
        $mensaje = Mensaje::findOrfail($id);
        $mensaje->delete();
            return response()->json([
                'success' => true,
            ]);   

    }



}
