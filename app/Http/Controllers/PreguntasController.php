<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Video;
use App\Categoria;
use App\Pregunta;
use DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Redirect;
use Crypt;

class PreguntasController extends Controller
{


    public function index()
    {
        $preguntas = Pregunta::orderBy('created_at', 'DESC')->get();

        return view('preguntas.index', compact('preguntas'));
    }


    public function store(Request $request)
    {

        $this->validate($request, [
            'pregunta' => 'required',
        ]);

        $pregunta = new Pregunta();
        $pregunta->pregunta = $request->input('pregunta');
        $pregunta->user_id = Auth::user()->id;
        $pregunta->video_id = $request->input('video_id');
        $pregunta->save();



        return redirect()->route('home.video_detalles', Crypt::encrypt($request->video_id))
                        ->with('success','Pregunta creada exitosamente');
    }


    public function edit($id)
    {
        $pregunta = Pregunta::find($id);

        return view('preguntas.edit', compact('pregunta'));
    }


    public function answer(Request $request, $id)
    {


        $pregunta = Pregunta::find($id);
        $pregunta->respuesta = $request->input('respuesta');
        $pregunta->save();


        return redirect()->route('preguntas.index', Crypt::encrypt($request->video_id))
                        ->with('success','Respuesta creada exitosamente');
    }



    public function destroy($id)
    {
        DB::table("preguntas")->where('id',$id)->delete();
        return redirect()->route('preguntas.index')
                        ->with('success','Pregunta Eliminada con Éxito');
    }



}
