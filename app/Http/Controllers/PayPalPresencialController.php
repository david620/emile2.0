<?php
  
namespace App\Http\Controllers;
  
use Illuminate\Http\Request;
use Srmklive\PayPal\Services\ExpressCheckout;
use App\Http\Requests;
use Redirect;
use Session;  
use DB;  
use App\User;
use App\Invoice;
use App\Servicio;
use App\UserCursoVivo;
use App\Task;
use Carbon\Carbon;

class PayPalPresencialController extends Controller
{
	protected $provider;

	public function __construct() {
	    $this->provider = new ExpressCheckout();
	}


    public function payment()
    {
        $user_sesion = Session::get('profit.presencial.data');
        $invoice = new Invoice();
        $invoice->save();
        $data = [];

        if($user_sesion['modalidad'] == 1 ){
             $data['items'] = [
                [
                    'name' => 'Curso de 5 días en Persona',
                    'price' => 1500,
                    'desc'  => 'Curso de 5 días en Persona',
                    'qty' => 1
                ]
            ];
            $data['invoice_id'] = $invoice->id;
            $data['invoice_description'] = "Order #{$data['invoice_id']} Invoice";
            $data['return_url'] = route('payment.success-presencial');
            $data['cancel_url'] = route('payment.cancel-presencial');
            $data['total'] = 1500;
        }
        if($user_sesion['modalidad'] == 2 ){
             $data['items'] = [
                [
                    'name' => 'Curso de 5 días en Persona + Membresia Premium.',
                    'price' => 2000,
                    'desc'  => 'Curso de 5 días en Persona + Membresia Premium.',
                    'qty' => 1
                ]
            ];
            $data['invoice_id'] = $invoice->id;
            $data['invoice_description'] = "Order #{$data['invoice_id']} Invoice";
            $data['return_url'] = route('payment.success-presencial');
            $data['cancel_url'] = route('payment.cancel-presencial');
            $data['total'] = 2000;
        }
        if($user_sesion['modalidad'] == 3 ){
             $data['items'] = [
                [
                    'name' => 'Curso de 5 días Versión Online',
                    'price' => 1500,
                    'desc'  => 'Curso de 5 días Versión Online',
                    'qty' => 1
                ]
            ];
            $data['invoice_id'] = $invoice->id;
            $data['invoice_description'] = "Order #{$data['invoice_id']} Invoice";
            $data['return_url'] = route('payment.success-presencial');
            $data['cancel_url'] = route('payment.cancel-presencial');
            $data['total'] = 1500;
        }
        if($user_sesion['modalidad'] == 4 ){
             $data['items'] = [
                [
                    'name' => 'Curso de 5 días Versión Online + Membresía Premium',
                    'price' => 2000,
                    'desc'  => 'Curso de 5 días Versión Online + Membresía Premium',
                    'qty' => 1
                ]
            ];
            $data['invoice_id'] = $invoice->id;
            $data['invoice_description'] = "Order #{$data['invoice_id']} Invoice";
            $data['return_url'] = route('payment.success-presencial');
            $data['cancel_url'] = route('payment.cancel-presencial');
            $data['total'] = 2000;
        }

  
        $response =  $this->provider->setExpressCheckout($data);
        $response =  $this->provider->setExpressCheckout($data, true);
  
        return redirect($response['paypal_link']);
    }
   
    /**
     * Responds with a welcome message with instructions
     *
     * @return \Illuminate\Http\Response
     */
    public function cancel()
    {
       	return view('errors.cancel_paypal');
    }
  
    /**
     * Responds with a welcome message with instructions
     *
     * @return \Illuminate\Http\Response
     */
    public function success(Request $request)
    {
        $response =  $this->provider->getExpressCheckoutDetails($request->token);
  
        if (in_array(strtoupper($response['ACK']), ['SUCCESS', 'SUCCESSWITHWARNING'])) {

            $user_sesion = Session::get('profit.presencial.data');

                 $user = new UserCursoVivo;

                 $user->nombre = $user_sesion['name'];
                 $user->apellido = $user_sesion['apellido'];
                 $user->email = $user_sesion['email'];
                 $user->telefono = $user_sesion['phone'];
                 $user->curso = $user_sesion['curso'];
                 $user->modalidad = $user_sesion['modalidad'];
                 $user->save();

                $task = Task::find($user_sesion['curso']);
                $task->disponibilidad = $task->disponibilidad - 1;
                $task->save();



            Session::forget('profit.presencial.data');  

            return view('errors.success_paypal');

        }
        	return view('errors.error_paypal');
    }
}