<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Slider;
use App\Task;
use App\Servicio;
use App\Correo;
use App\Concurso;
use Auth; 
use Mail;
use DB;
use Redirect;
use App\InvoiceVivo;
use App\UserCursoVivo;
use Illuminate\Support\Facades\Log;

class ConcursoRifaController extends Controller {

    public function generar_ganador($id) {
        $concurso = Concurso::where('id', '=', $id)->first();
        $servicios = Servicio::all();

        $ganador = DB::table('users_concurso')->where('concurso_id', '=', $concurso->id)->inRandomOrder()->first();


        return view('concurso.generar_ganador_concurso', compact('servicios', 'concurso','ganador', 'id'));

    }



    public function ganador($id) {
        $concurso = Concurso::where('id', '=', $id)->first();
        $servicios = Servicio::all();
        $ganador = DB::table('users_concurso')->where('concurso_id', '=', $concurso->id)->inRandomOrder()->first();

        return view('concurso.ganador', compact('servicios', 'concurso', 'ganador'));
    }



    public function store(Request $request) {


        $this->validate($request, [
            'nombre' => 'required',
            'apellido' => 'required',
            'email' => 'required',
            'telefono' => 'required',
            'tickets' => 'required|min:1',
            'pais' => 'required',
        ]);


        $arreglo = ['nombre' => $request->nombre, 'apellido' => $request->apellido, 'email' => $request->email, 'telefono' => $request->telefono, 'tickets' => $request->tickets, 'pais' => $request->pais, 'concurso' => $request->concurso];

        
        $request->session()->put('concurso.user.data', $arreglo); // guardamos usuario en session


        return redirect()->route('paypal.express-checkout-concurso');

    }

    public function store_concurso(Request $request) {

        $concurso = new Concurso();
        $concurso->premio_semana = $request->premio_semana;
        $concurso->fecha_vencimiento = $request->fecha_vencimiento;
        $concurso->habilitado = 0;
        $concurso->save();


        return redirect()->route('listado_concursos');

    }    


    public function update_concurso(Request $request, $id) {

        $concurso = Concurso::findOrfail($id);

        $concurso->premio_semana = $request->premio_semana;
        $concurso->save();

        return redirect()->route('listado_concursos');

    }   


    public function listadoConcurso(Request $request, $id) {


        $users = DB::table('users_concurso')
            ->where('users_concurso.concurso_id', '=', $id)
            ->select('users_concurso.*')->get();

        return view('concurso.listado_usuarios_concurso', compact('users'));
    }


    public function listadoConcursos(Request $request) {


        $concursos = Concurso::all();
        return view('concurso.index', compact('concursos'));
    }

    public function editarlistadoConcursos(Request $request, $id) {

        $concurso = Concurso::findOrfail($id);
        $concursos = Concurso::all();

        return view('concurso.edit', compact('concursos', 'concurso'));
    }


    public function destroy(Request $request, $id)
    {
        DB::table("concursos")->where('id',$id)->delete();
        return redirect()->route('listado_concursos');
    }


    public function modificaEstado(Request $request, $id)
    {
        $concurso = Concurso::findOrfail($id);

        if ($concurso->habilitado == '0') {
            $concurso->habilitado = '1';
            $concurso->save();
        }
        else{
            $concurso->habilitado = '0';
            $concurso->save();           
        }

        return redirect()->route('listado_concursos')->with('success', 'Estado Concurso Cambiado Exitosamente');
    }


}

