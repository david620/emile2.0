<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Categoria;
use App\Servicio;
use App\Archivo;
use DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Redirect;
use File;

class ArchivosController extends Controller
{

    public function archivos_lista(){
            $services = DB::table('servicios')
                                ->join('users_servicios', 'users_servicios.servicio_id', '=', 'servicios.id')
                                ->join('users', 'users.id', '=', 'users_servicios.user_id')
                                ->where('users_servicios.user_id', '=', Auth::id())
                                ->select('servicios.*')            
                                ->distinct()            
                                ->get(); 
        $archivos = Archivo::orderBy('id', 'DESC')->get();
        $servicios = Servicio::all();
        return view('archivos_lista', compact('archivos', 'services', 'servicios'));
    }

    public function index(){
        $archivos = Archivo::orderBy('id', 'DESC')->get();
        return view('archivos.index', compact('archivos'));
    }

    public function create(){
        return view('archivos.create');
    }

    public function store(Request $request){
        $this->validate($request, [
            'name' => 'required',
            'url_archivo' => 'required',
            'plataforma_id' => 'required',
        ]);

        DB::beginTransaction();

        $archivo = new Archivo;
        $archivo->nombre_archivo = $request->name;
        $archivo->plataforma_id = $request->plataforma_id;
        $archivo->url_archivo = "";
        $archivo->save();

        if (!$archivo) {
            DB::rollBack();
            return redirect()->route('archivos.create')->with('alert-danger', 'Error Creando Archivo');
        }

        $archivo1 = Archivo::find($archivo->id);

        $filename = 'archivos/'.$archivo1->id.'_'.$request->url_archivo->getClientOriginalName();

        Storage::disk('archivoss')->put($filename, File::get($request->url_archivo));

        $route = 'archivos/'.$filename;

        $archivo1->url_archivo = $route;

        $archivo1->save();

        if (!$archivo1) {
            DB::rollBack();
            return redirect()->route('archivos.create')->with('alert-danger', 'Error Creando Archivo');
        }

        DB::commit();

        return redirect()->route('archivos.index')
                        ->with('success','Archivo creado exitosamente');
    }

    public function destroy($id){
        DB::table("archivos")->where('id',$id)->delete();
        return redirect()->route('archivos.index')
                        ->with('success','Archivo Eliminado con Éxito');
    }
}
