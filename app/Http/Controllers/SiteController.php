<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Redirect;
use Session;
use App\Servicio;
use App\Contact;
use Crypt;
use DB;
use File;
use Illuminate\Contracts\Encryption\DecryptException;
use App\Task;
use App\Correo;
use App\User;
use App\Concurso;
use App\Video;
use App\Pregunta;
use Auth; 
use Mail;
use App\InvoiceVivo;
use App\UserCursoVivo;
use Carbon\Carbon;

class SiteController extends Controller
{


    public function home()
    {
        $servicios = Servicio::all();
        $usuarios_totales = User::all();
        $cuenta_usuarios = count($usuarios_totales);

        $usuarios_profit = DB::table('users')
            ->join('users_servicios', 'users_servicios.user_id', '=', 'users.id')
            ->join('servicios', 'servicios.id', '=', 'users_servicios.servicio_id')
            ->where('servicios.id', '=', 1)
            ->select('users.*')            
            ->distinct()            
            ->get();

        $cuenta_usuarios_profit = count($usuarios_profit);

        $usuarios_cripto = DB::table('users')
            ->join('users_servicios', 'users_servicios.user_id', '=', 'users.id')
            ->join('servicios', 'servicios.id', '=', 'users_servicios.servicio_id')
            ->where('servicios.id', '=', 2)
            ->select('users.*')            
            ->distinct()            
            ->get();

        $cuenta_usuarios_cripto = count($usuarios_cripto);


        return view('index', compact('servicios', 'cuenta_usuarios', 'cuenta_usuarios_profit','cuenta_usuarios_cripto'));
    }

    public function home_administrador()
    {
        return view('administrador.home_administrador');
    }

    public function forex()
    {
        $servicios = Servicio::all();
        return view('forex', compact('servicios'));
    }

    public function servicios()
    {
        $servicios = Servicio::all();

        return view('servicios', compact('servicios'));
    }

    public function presencial()
    {
        $servicios = Servicio::all();
        $tasks = Task::all();

        return view('presencial', compact('servicios', 'tasks'));
    }

    public function concurso()
    {
        $concurso = Concurso::where('habilitado', '=', 1)->first();
        $cuenta = count(Concurso::where('habilitado', '=', 1)->get());
        $servicios = Servicio::all();
        if(isset($concurso->fecha_vencimiento)){
            $date = new \DateTime(($concurso->fecha_vencimiento));
            $minuto =  $date->format('i');
            $dia =  $date->format('d');
            $mes =  $date->format('m');
            $anio =  $date->format('Y');
            $hora = $date->format('H');
            $local = new \DateTime("now", new \DateTimeZone('America/La_Paz'));


            $minutol =  $local->format('i');
            $dial =  $local->format('d');
            $mesl =  $local->format('m');
            $aniol =  $local->format('Y');
            $horal = $local->format('H');

            $bandera = 0;

            if( ($dial >= $dia) && ($mesl == $mes) && ($aniol == $anio) && ($horal >= $hora) && ($minutol >= $minuto)){
                $bandera = 1;
            }

            $ganador = DB::table('users_concurso')->where('concurso_id', '=', isset($concurso->id_concursos))->inRandomOrder()->first();

            return view('concurso', compact('concurso', 'dia', 'mes', 'anio', 'hora', 'bandera', 'minuto', 'servicios', 'cuenta'));
        }
            return view('concurso', compact('concurso', 'servicios', 'cuenta'));

    }

    public function producto($id)
    {
        try {
            $d = Crypt::decrypt($id);
        } catch (DecryptException $e) {
            abort(404);
        }

        $servicio = Servicio::find($d);
        $servicios = Servicio::all();
        $servs = Servicio::where('id', '!=', $servicio->id)->where('id', '!=', 6)->get();

        return view('producto', compact('servicio', 'servicios', 'servs'));
    }

    public function carrito()
    {
        $servicios = Servicio::all();
        $carrito = Session::get('cart');                
        $suma = 0;
        $carritos_ids = [];
        $servicios_filtrados = [];
        $promo_premium = 0;
        $promo_combo = 0;
        if(Session::has('cart')){     
            foreach ($carrito as $key => $value) {
                $car = $value->id;
                array_push($carritos_ids, $car);
            }
        }        
        $carritos_id = array_unique($carritos_ids);        


        foreach ($carritos_id as $key => $value) {
            if($value == 1 || $value == 2 || $value == 4 || $value == 5){
                $promo_premium = $promo_premium + 1;
            }
            if($value == 1 || $value == 2){
                $promo_combo = $promo_combo + 1;
            }

            $servicio_filtrado = Servicio::find($value);
            array_push($servicios_filtrados, $servicio_filtrado);
            $suma = $suma + $servicio_filtrado->precio_servicio;
        }

        if($promo_premium == 4){
            $suma = $suma - 950;
            return view('carrito', compact('servicios', 'suma', 'servicios_filtrados', 'promo_premium', 'promo_combo'));
        }

        if($promo_combo == 2){
            $suma = $suma - 400;
            return view('carrito', compact('servicios', 'suma', 'servicios_filtrados', 'promo_premium', 'promo_combo'));
        }

        return view('carrito', compact('servicios', 'suma', 'servicios_filtrados', 'promo_premium', 'promo_combo'));
    }

    public function agregar_carrito(Request $request, $id)
    {
        try {
            $d = Crypt::decrypt($id);
        } catch (DecryptException $e) {
            abort(404);
        }

        $servicio = Servicio::find($d);

        Session::push('cart', $servicio);
        $carrito = Session::get('cart');                
        $servicios = Servicio::all();
        $suma = 0;
        $carritos_ids = [];
        $servicios_filtrados = [];
        $promo_premium = 0;
        $promo_combo = 0;
        foreach ($carrito as $key => $value) {
            $car = $value->id;
            array_push($carritos_ids, $car);
        }

        $carritos_id = array_unique($carritos_ids);        


        foreach ($carritos_id as $key => $value) {
            if($value == 1 || $value == 2 || $value == 4 || $value == 5){
                $promo_premium = $promo_premium + 1;
            }
            if($value == 1 || $value == 2){
                $promo_combo = $promo_combo + 1;
            }

            $servicio_filtrado = Servicio::find($value);
            array_push($servicios_filtrados, $servicio_filtrado);
            $suma = $suma + $servicio_filtrado->precio_servicio;
        }

        Session::push('cart_filtrado', $servicio);

        if($promo_premium == 4){
            $suma = $suma - 950;
            return view('carrito', compact('servicios', 'suma', 'servicios_filtrados', 'promo_premium', 'promo_combo'));
        }

        if($promo_combo == 2){
            $suma = $suma - 400;
            return view('carrito', compact('servicios', 'suma', 'servicios_filtrados', 'promo_premium', 'promo_combo'));
        }

        return view('carrito', compact('servicios', 'suma', 'servicios_filtrados', 'promo_premium', 'promo_combo'));
    }

    public function remover_carrito(Request $request)
    {
        Session::flush();        
        $servicios = Servicio::all();
        $carrito = Session::get('cart');                
        $suma = 0;
        $carritos_ids = [];
        $servicios_filtrados = [];
        $promo_premium = 0;
        $promo_combo = 0;
        if(Session::has('cart')){     
            foreach ($carrito as $key => $value) {
                $car = $value->id;
                array_push($carritos_ids, $car);
            }
        }        
        $carritos_id = array_unique($carritos_ids);        


        foreach ($carritos_id as $key => $value) {
            if($value == 1 || $value == 2 || $value == 4 || $value == 5){
                $promo_premium = $promo_premium + 1;
            }
            if($value == 1 || $value == 2){
                $promo_combo = $promo_combo + 1;
            }

            $servicio_filtrado = Servicio::find($value);
            array_push($servicios_filtrados, $servicio_filtrado);
            $suma = $suma + $servicio_filtrado->precio_servicio;
        }

        if($promo_premium == 4){
            $suma = $suma - 950;
            return view('carrito', compact('servicios', 'suma', 'servicios_filtrados', 'promo_premium', 'promo_combo'));
        }

        if($promo_combo == 2){
            $suma = $suma - 400;
            return view('carrito', compact('servicios', 'suma', 'servicios_filtrados', 'promo_premium', 'promo_combo'));
        }

        return view('carrito', compact('servicios', 'suma', 'servicios_filtrados', 'promo_premium', 'promo_combo'));           
    }


    public function pasarela()
    {
        $suma = 0;
        $promo_premium = 0;
        $promo_combo = 0;

        if(Session::has('cart_filtrado')){
            $carrito = Session::get('cart_filtrado');
            foreach ($carrito as $key => $value) {
                if($value->id == 1 || $value->id == 2 || $value->id == 4 || $value->id == 5){
                    $promo_premium = $promo_premium + 1;
                }
                if($value->id == 1 || $value->id == 2){
                    $promo_combo = $promo_combo + 1;
                }
                $suma = $suma + $value->precio_servicio;
            }
        }

         if($promo_combo == 2){
            $servicios = Servicio::all();
            $suma = $suma - 400;
            return view('pasarela', compact('servicios', 'suma', 'promo_premium', 'promo_combo'));
        }

        if($promo_premium == 4){
            $servicios = Servicio::all();
            $suma = $suma - 950;
            return view('pasarela', compact('servicios', 'suma', 'promo_premium', 'promo_combo'));
        }


        $servicios = Servicio::all();
            return view('pasarela', compact('servicios', 'suma', 'promo_premium', 'promo_combo'));
    }


    public function store_app(Request $request)
    {
        $this->validate($request, [
            'email' => 'required',
            'password' => 'required',
        ]);

            $app_data = ['nombre' => $request->nombre, 'apellido' => $request->apellido, 'phone' => $request->phone, 'nickname' => $request->nickname, 'email' => $request->email, 'password' => bcrypt($request->password)];
                $request->session()->put('profit.app.data', $app_data);            
                return redirect()->route('paypal.payment-app');
    }


    public function store_carrito(Request $request)
    {
        $carrito = Session::get('cart_filtrado');
        $mytime = Carbon::now();
        $datetime = $mytime->addMonth();
        $servicios_id = [];
        $servicios_ids = [];

        $this->validate($request, [
            'nombre' => 'required',
            'apellido' => 'required',
            'phone' => 'required',
            'password' => 'required',
            'pais' => 'required',
        ]);


        if($request->metodo_pago == 0){
            $user_data = ['nombre' => $request->nombre, 'apellido' => $request->apellido, 'pais' => $request->pais, 'phone' => $request->phone, 'nickname' => $request->nickname, 'email' => $request->email, 'password' => bcrypt($request->password)];
                $request->session()->put('profit.user.data', $user_data);            
                return redirect()->route('paypal.payment');
        }

        $user = DB::table('users')
            ->select('users.*')
            ->where('email', '=', $request->email)
            ->get();


        if(count($user) != 0){
            $services = DB::table('servicios')
                                ->join('users_servicios', 'users_servicios.servicio_id', '=', 'servicios.id')
                                ->join('users', 'users.id', '=', 'users_servicios.user_id')
                                ->where('users_servicios.user_id', '=', $user[0]->id)
                                ->select('servicios.*')            
                                ->distinct()            
                                ->get();            
        foreach ($services as $key => $value) {
            $service = $value->id;
            array_push($servicios_ids, $service);
        }
        
        $servicios_id = array_unique($servicios_ids);

        }

                        if(count($user) != 0){
                            foreach ($carrito as $key => $value) {
                                if((array_search($value->id, $servicios_id)) == false){
                                    DB::insert('insert into users_servicios_pending (servicio_id, user_id) values(?,?)', [$value->id, $user[0]->id]);
                                }
                            }
                        }
                        else{
                            $new_user = new User();
                            $new_user->nombre = $request->nombre;
                            $new_user->apellido = $request->apellido;
                            $new_user->phone = $request->phone;
                            $new_user->pais = $request->pais;
                            $new_user->email = $request->email;
                            $new_user->password = bcrypt($request->password);
                            $new_user->habilitado = 0;
                            $new_user->save();
                            DB::insert('insert into role_user (user_id, role_id) values(?,?)', [$new_user->id, 2]);
                            foreach ($carrito as $key => $value) {
                                DB::insert('insert into users_servicios (servicio_id, user_id) values(?,?)', [$value->id, $new_user->id]);
                                if($value->id == 3){
                                    $new_user->habilitado_app = 1;
                                    $new_user->fecha_duracion_app = $datetime;
                                    $new_user->save();
                                }
                            }
                        }
            return view('errors.success');
    }


    public function terminos()
    {
        $servicios = Servicio::all();
        return view('terminos', compact('servicios'));
    }

    public function recuperar()
    {
        $servicios = Servicio::all();
        return view('recuperar', compact('servicios'));
    }

    public function app()
    {
        $servicio = Servicio::all();
        return view('app', compact('servicio'));
    }

    public function prosperity()
    {
        return view('app/prosperity');
    }

    public function wsacademy()
    {
        return view('app/wsacademy');
    }


    public function video_categorias($id)
    {

            $services = DB::table('servicios')
                ->join('users_servicios', 'users_servicios.servicio_id', '=', 'servicios.id')
                ->join('users', 'users.id', '=', 'users_servicios.user_id')
                ->where('users_servicios.user_id', '=', Auth::user()->id)
                ->select('servicios.*')            
                ->distinct()            
                ->get();
            $servicios = Servicio::all();

                $videos = DB::table('videos')
                    ->join('categorias', 'categorias.id', '=', 'videos.categoria_id')
                    ->join('servicios', 'servicios.id', '=', 'categorias.plataforma_categoría')
                    ->where('categorias.id', '=', $id)
                    ->select('videos.*')            
                    ->distinct()            
                    ->orderBy('videos.posicion_video', 'ASC')
                    ->paginate(5);

        return view('video_lista', compact('servicios', 'services', 'videos'));
    }



    public function video_lista($id)
    {
        try {
            $d = Crypt::decrypt($id);
        } catch (DecryptException $e) {
            abort(404);
        }
            $services = DB::table('servicios')
                ->join('users_servicios', 'users_servicios.servicio_id', '=', 'servicios.id')
                ->join('users', 'users.id', '=', 'users_servicios.user_id')
                ->where('users_servicios.user_id', '=', Auth::user()->id)
                ->select('servicios.*')            
                ->distinct()            
                ->get();
            $servicios = Servicio::all();

            $service = DB::table('servicios')
                ->join('users_servicios', 'users_servicios.servicio_id', '=', 'servicios.id')
                ->join('users', 'users.id', '=', 'users_servicios.user_id')
                ->where('users_servicios.user_id', '=', Auth::user()->id)
                ->where('servicios.id', '=', $d)
                ->select('servicios.*')            
                ->distinct()            
                ->get();

                if(count($service) == 0){
                    return view('perfil', compact('services', 'servicios'));
                }

            if(($d == 1) || ($d == 2)){
                $videos = DB::table('videos')
                    ->join('categorias', 'categorias.id', '=', 'videos.categoria_id')
                    ->join('servicios', 'servicios.id', '=', 'categorias.plataforma_categoría')
                    ->where('servicios.id', '=', $d)
                    ->select('videos.*')            
                    ->distinct()            
                    ->paginate(5);

            return redirect()->route('categorias.categorias_lista', $d);


     //           return view('video_lista', compact('servicios', 'services', 'videos'));
            }
            if(($d == 3)){
                return redirect()->route('home.app');
            }

            if(($d == 4) || ($d == 5)){
                $flipbook = DB::table('flipbook')->where('id', $d)->get()[0];
                $content = explode(",",$flipbook->content);
            return view('rudrarajiv.flipbooklaravel.libro',compact('flipbook','content', 'd', 'services', 'servicios'));
            }
    }

    public function video_detalles($id)
    {
        try {
            $d = Crypt::decrypt($id);
        } catch (DecryptException $e) {
            abort(404);
        }

        $video = Video::find($d);
        $servicios = Servicio::all();
        $services = DB::table('servicios')
            ->join('users_servicios', 'users_servicios.servicio_id', '=', 'servicios.id')
            ->join('users', 'users.id', '=', 'users_servicios.user_id')
            ->where('users_servicios.user_id', '=', Auth::user()->id)
            ->select('servicios.*')            
            ->distinct()            
            ->get();

        $preguntas = Pregunta::orderBy('created_at', 'DESC')->where('video_id', '=', $video->id )->get();
        return view('video_detalles', compact('servicios', 'video', 'services', 'preguntas'));
    }

    public function perfil()
    {
        $services = DB::table('servicios')
            ->join('users_servicios', 'users_servicios.servicio_id', '=', 'servicios.id')
            ->join('users', 'users.id', '=', 'users_servicios.user_id')
            ->where('users_servicios.user_id', '=', Auth::user()->id)
            ->select('servicios.*')            
            ->distinct()            
            ->get();

        $servicios = Servicio::all();

        return view('perfil', compact('services', 'servicios'));
    }

    public function contact_store(Request $request)
    {


        $this->validate($request, [
            'name' => 'required',
            'email' => 'required',
            'subject' => 'required',
            'message' => 'required',
        ]);

        DB::beginTransaction();

        $contact = New Contact();
        $contact->name = $request->name;
        $contact->email = $request->email;
        $contact->subject = $request->subject;
        $contact->message = $request->message;
        $contact->save();

        if (!$contact) {
            DB::rollBack();
            return redirect()->route('home.index');
        }

        DB::commit();
            return redirect()->route('home.index');
    }


}
