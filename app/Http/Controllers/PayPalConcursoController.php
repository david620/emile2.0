<?php
  
namespace App\Http\Controllers;
  
use Illuminate\Http\Request;
use Srmklive\PayPal\Services\ExpressCheckout;
use App\Http\Requests;
use Redirect;
use Session;  
use DB;  
use App\User;
use App\Invoice;
use App\Servicio;
use Carbon\Carbon;

class PayPalConcursoController extends Controller
{
	protected $provider;

	public function __construct() {
	    $this->provider = new ExpressCheckout();
	}


    public function payment()
    {
        $user_sesion = Session::get('concurso.user.data');
        $invoice = new Invoice();
        $invoice->save();

             $data['items'] = [
                [
                    'name' => 'Concurso Profit 4 Life',
                    'price' => $user_sesion['tickets'],
                    'desc'  => 'Concurso Profit 4 Life',
                    'qty' => 1
                ]
            ];
            $data['invoice_id'] = $invoice->id;
            $data['invoice_description'] = "Order #{$data['invoice_id']} Invoice";
            $data['return_url'] = route('payment.success-rifa');
            $data['cancel_url'] = route('payment.cancel-rifa');
            $data['total'] = $user_sesion['tickets'];
  

        $response =  $this->provider->setExpressCheckout($data);
        $response =  $this->provider->setExpressCheckout($data, true);
  
        return redirect($response['paypal_link']);
    }
   
    /**
     * Responds with a welcome message with instructions
     *
     * @return \Illuminate\Http\Response
     */
    public function cancel()
    {
       	return view('errors.cancel_paypal');
    }
  
    /**
     * Responds with a welcome message with instructions
     *
     * @return \Illuminate\Http\Response
     */
    public function success(Request $request)
    {
        $response =  $this->provider->getExpressCheckoutDetails($request->token);
  
        if (in_array(strtoupper($response['ACK']), ['SUCCESS', 'SUCCESSWITHWARNING'])) {

            $user_sesion = Session::get('concurso.user.data');

                $ticket = $user_sesion['tickets'];

                $entradas = 0;

                if($ticket == '15'){
                    $entradas = '1';
                }

                if($ticket == '25'){
                    $entradas = '2';
                }


                if($ticket == '40'){
                    $entradas = '4';
                }


                if($ticket == '65'){
                    $entradas = '8';
                }

                if($ticket == '150'){
                    $entradas = '20';
                }


                $algo = json_decode($user_sesion['concurso']);


                for ($i=1; $i <= $entradas; $i++) { 
                    DB::insert('insert into users_concurso (nombre, apellido, email, telefono, pais, concurso_id) values(?,?,?,?,?,?)', [$user_sesion['nombre'], $user_sesion['apellido'], $user_sesion['email'], $user_sesion['telefono'], $user_sesion['pais'], $algo->id]);
                }

                $request->session()->forget('concurso.user.data');  



            return view('errors.success_paypal');

        }
        	return view('errors.error_paypal');
    }
}