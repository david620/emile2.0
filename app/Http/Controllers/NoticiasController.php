<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Noticia;
use DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Redirect;

class NoticiasController extends Controller
{


    public function index()
    {
        $news = Noticia::orderBy('id', 'DESC')->get();

            return response()->json([
                'success' => true,
                'noticias' => $news
            ]);  
    }

    public function store(Request $request)
    {
        //hacer para que reciba imagenes

        DB::beginTransaction();

        $news = new Noticia;

        $news->titulo = $request->titulo;
        $news->contenido = $request->contenido;
        $news->link = $request->link;
        $news->link_imagen = "";
        $news->save();

        if (!$news) {
            DB::rollBack();
            return response()->json([
                'success' => false,
            ]);  
        }

        $news1 = Noticia::find($news->id);


	   $file_data = $request->link_imagen; 
	   $file_name = 'new_'.time().'.png'; //generating unique file name; 
	   @list($type, $file_data) = explode(';', $file_data);
	   @list(, $file_data) = explode(',', $file_data); 
	   if($file_data!=""){ // storing image in storage/app/public Folder 
	          \Storage::disk('imagenes')->put($file_name,base64_decode($file_data)); 
	    } 

        $route = 'imagenes/'.$file_name;

        $news1->link_imagen = $route;

        $news1->save();

        if (!$news1) {
            DB::rollBack();
            return response()->json([
                'success' => false,
            ]); 
        }

        DB::commit();

            return response()->json([
                'success' => true,
                'noticia' => $news1
            ]);   
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Noticia $news)
    {
        return $news;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    public function destroy($id)
    {
        $noticia = Noticia::findOrfail($id);
        $noticia->delete();
            return response()->json([
                'success' => true,
            ]);   

    }



}
