<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\News;
use App\Testimonio;
use DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Redirect;

class TestimoniosController extends Controller
{


    public function index()
    {
        $testimonios = Testimonio::orderBy('id', 'DESC')->get();

            return response()->json([
                'success' => true,
                'testimonios' => $testimonios
            ]);  
    }

    public function store(Request $request)
    {
        //hacer para que reciba imagenes

        DB::beginTransaction();

        $testimonio = new Testimonio;

        $testimonio->texto_descriptivo = $request->texto_descriptivo;
        $testimonio->user_id = $request->user_id;
        $testimonio->url_imagen = "";
        $testimonio->save();

        if (!$testimonio) {
            DB::rollBack();
            return response()->json([
                'success' => false,
            ]);  
        }

        $testimonio1 = Testimonio::find($testimonio->id);


	   $file_data = $request->url_imagen; 
	   $file_name = 'testimonio'.time().'.png'; //generating unique file name; 
	   @list($type, $file_data) = explode(';', $file_data);
	   @list(, $file_data) = explode(',', $file_data); 
	   if($file_data!=""){ // storing image in storage/app/public Folder 
	          \Storage::disk('imagenes')->put($file_name,base64_decode($file_data)); 
	    } 

        $route = 'imagenes/'.$file_name;

        $testimonio1->url_imagen = $route;

        $testimonio1->save();

        if (!$testimonio1) {
            DB::rollBack();
            return response()->json([
                'success' => false,
            ]); 
        }

        DB::commit();

            return response()->json([
                'success' => true,
                'testimonio' => $testimonio1
            ]);   
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Testimonio $testimonios)
    {
        return $testimonios;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    public function destroy($id)
    {
        $testimonio = Testimonio::findOrfail($id);
        $testimonio->delete();
            return response()->json([
                'success' => true,
            ]);   

    }



}
