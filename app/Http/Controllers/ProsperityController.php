<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Task;
use App\User;
use App\PagosApp;
use App\Servicio;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Redirect;
use Response;
use Carbon\Carbon;

class ProsperityController extends Controller
{
    protected $urlgetopab = null;
    protected $urlgetopce = null;
    protected $urlmarket = null;
    protected $urlsavetoken = null;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function app_index(Request $request){
            $servicio = Servicio::all();
        return view('app/prosperity', compact('servicio'));          
    }

    public function index_prosperity(Request $request){
            $users = DB::table('prosperity_users')
                ->select('prosperity_users.*')
                ->orderBy('date_prosperity', 'DESC')                
                ->get();

        return view('app.index_prosperity', compact('users'));          
    }

    public function index_users_prosperity()
    {
            $users = DB::table('prosperity_users')
                ->select('prosperity_users.*')
                ->get();

            return response()->json([
                'success' => true,
                'users' => $users
            ]);  
    }

    public function prosperity_store(Request $request){



        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,"https://prosperitytrader.net/api/auth/login");
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, 'email='.$request->email.'&password='.$request->password.'&tokenapn=fdsgdf');         
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $remote_server_output = curl_exec ($ch);         
        curl_close ($ch);
        $respuesta = json_decode($remote_server_output);
        
        $arreglo = ['email' => $request->email, 'password' => $request->password, 'name' => $request->name, 'respuesta' => $respuesta->acceso ];
        $request->session()->put('prosperity.app.data', $arreglo);

        return redirect()->route('paypal.express-checkout-prosperity');
    }


    public function index()
    {
        $users = User::all();
        foreach ($users as $user) {
            $user->fecha_duracion_app = '2019-10-31 00:00:00';
            $user->save();
        }

        dd("Todo Listo");

    }

    public function index_users_token()
    {
            $users = DB::table('users')
                ->where('token', '!=', null)
                ->where('token', '!=', '')
                ->select('users.fecha_duracion_app', 'users.id', 'users.nombre','users.apellido','users.email','users.email','users.pais','users.role','users.email', 'users.habilitado','users.habilitado_app', 'users.token')
                ->get();

            return response()->json([
                'success' => true,
                'users' => $users
            ]);  
    }


    public function app_login(Request $request)
    {
        $token = $request->token;       

        if((Auth::attempt(['email'=>$request->email,'password'=>$request->password])) && ($token === 'Profit4Life')){
            $user = DB::table('users')
                ->where('email', $request->email)
                ->select('users.fecha_duracion_app', 'users.id', 'users.nombre','users.apellido','users.email','users.email','users.pais','users.role','users.email', 'users.habilitado','users.habilitado_app', 'users.token')
                ->first();

            if($request->tokenapn == 'Null' || $request->tokenapn == null || $request->tokenapn == '' ||$request->tokenapn == 'null' || $request->tokenapn == NULL || $request->tokenapn == 'NULL'){
                return response()->json([
                    'success' => true,
                    'usuario' => $user
                ]);   
            }
            else{
                DB::update('update users set token = :token where id = :id ', ['token' => $request->tokenapn, 'id' => $user->id]);
                return response()->json([
                    'success' => true,
                    'usuario' => $user
                ]);                
            }
        }
        else{
            return response()->json([
                'success' => false,
            ]);
        }
    }

}
