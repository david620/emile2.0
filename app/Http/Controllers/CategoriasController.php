<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Categoria;
use App\Servicio;
use DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Redirect;

class CategoriasController extends Controller
{


    public function categoria_lista($id){
            $services = DB::table('servicios')
                                ->join('users_servicios', 'users_servicios.servicio_id', '=', 'servicios.id')
                                ->join('users', 'users.id', '=', 'users_servicios.user_id')
                                ->where('users_servicios.user_id', '=', Auth::id())
                                ->select('servicios.*')            
                                ->distinct()            
                                ->get(); 
        $categorias = Categoria::orderBy('id', 'ASC')->where('plataforma_categoría', '=', $id)->get();

        $servicios = Servicio::all();
        return view('categorias_lista', compact('categorias', 'services', 'servicios'));
    }


    public function index()
    {
        $categorias = Categoria::orderBy('id', 'DESC')->get();
        return view('videos.categorias.index', compact('categorias'));
    }

    public function create()
    {
        $categorias = Categoria::all();
        $servicios = Servicio::all();

        return view('videos.categorias.create', compact('categorias', 'servicios'));
    }

    public function store(Request $request)
    {

        $this->validate($request, [
            'nombre_categoria' => 'required',
            'plataforma_categoría' => 'required',
        ]);

        $categoria = new Categoria();
        $categoria->nombre_categoria = $request->input('nombre_categoria');
        $categoria->plataforma_categoría = $request->input('plataforma_categoría');
        $categoria->save();

        return redirect()->route('categorias.index')
                        ->with('success','Categoria creada exitosamente');
    }



    public function destroy($id)
    {
        DB::table("categorias")->where('id',$id)->delete();
        return redirect()->route('categorias.index')
                        ->with('success','Categoria Eliminada con Éxito');
    }



}
