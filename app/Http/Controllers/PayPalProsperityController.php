<?php
  
namespace App\Http\Controllers;
  
use Illuminate\Http\Request;
use Srmklive\PayPal\Services\ExpressCheckout;
use App\Http\Requests;
use Redirect;
use Session;  
use DB;  
use App\User;
use App\Invoice;
use App\Servicio;
use Carbon\Carbon;

class PayPalProsperityController extends Controller
{
	protected $provider;

	public function __construct() {
	    $this->provider = new ExpressCheckout();
	}


    public function payment()
    {
        $servicio = Servicio::all();
        $invoice = new Invoice();
        $invoice->save();

             $data['items'] = [
                [
                    'name' => 'Techinical Prosperity App',
                    'price' => $servicio[5]->precio_servicio,
                    'desc'  => 'Techinical Prosperity App',
                    'qty' => 1
                ]
            ];
            $data['invoice_id'] = $invoice->id;
            $data['invoice_description'] = "Order #{$data['invoice_id']} Invoice";
            $data['return_url'] = route('payment.success-prosperity');
            $data['cancel_url'] = route('payment.cancel-prosperity');
            $data['total'] = $servicio[5]->precio_servicio;
  

        $response =  $this->provider->setExpressCheckout($data);
        $response =  $this->provider->setExpressCheckout($data, true);
  
        return redirect($response['paypal_link']);
    }
   
    /**
     * Responds with a welcome message with instructions
     *
     * @return \Illuminate\Http\Response
     */
    public function cancel()
    {
       	return view('errors.cancel_paypal');
    }
  
    /**
     * Responds with a welcome message with instructions
     *
     * @return \Illuminate\Http\Response
     */
    public function success(Request $request)
    {
        $servicio = Servicio::all();
        $response =  $this->provider->getExpressCheckoutDetails($request->token);
  
        if (in_array(strtoupper($response['ACK']), ['SUCCESS', 'SUCCESSWITHWARNING'])) {

                $user_sesion = $request->session()->get('prosperity.app.data');

                if($user_sesion['respuesta'] == true){
                    DB::insert('insert into prosperity_users (monto_prosperity, mail_prosperity, date_prosperity) values(?,?,?)', [ $servicio[5]->precio_servicio, $user_sesion['email'], Carbon::now()->toDateTimeString() ]);
                    $request->session()->forget('prosperity.app.data');  
                    return view('errors.success_paypal');
                }
                if($user_sesion['respuesta'] == false){
                    $user_sesion = $request->session()->get('prosperity.app.data');
                    $user_name = $user_sesion['name'];
                    $user_email = $user_sesion['email'];
                    $user_password = $user_sesion['password'];

                        $ch2 = curl_init();
                        curl_setopt($ch2, CURLOPT_URL,"http://prosperitytrader.net/api/auth/signup");
                        curl_setopt($ch2, CURLOPT_POST, TRUE);
                        curl_setopt($ch2, CURLOPT_POSTFIELDS, "name=$user_name&email=$user_email&password=$user_password&password_confirmation=$user_password");

                        curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
                        $remote_server_output2 = curl_exec ($ch2);         
                        curl_close ($ch2);
                        $respuesta2 = json_decode($remote_server_output2);


                    DB::insert('insert into prosperity_users (monto_prosperity, mail_prosperity, date_prosperity) values(?,?,?)', [$servicio[5]->precio_servicio, $user_sesion['email'], Carbon::now()->toDateTimeString() ]);

                    $request->session()->forget('prosperity.app.data');    


                    return view('errors.success_paypal');
                }

        }
        	return view('errors.error_paypal');
    }
}