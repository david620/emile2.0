<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Video;
use App\Categoria;
use App\Servicio;
use DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Redirect;

class ServiciosController extends Controller
{


    public function index()
    {
        //$videos = Video::orderBy('id', 'DESC')->get();
        $servicios = Servicio::all();
        return view('servicios.index', compact('servicios'));
    }

    public function edit($id)
    {
        //$videos = Video::orderBy('id', 'DESC')->get();
        $servicio = Servicio::find($id);
        return view('servicios.edit', compact('servicio'));
    }
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nombre_servicio' => 'required',
            'descripcion_servicio' => 'required',
            'precio_servicio' => 'required',
        ]);


        $servicio = Servicio::find($id);
        $servicio->nombre_servicio = $request->input('nombre_servicio');
        $servicio->descripcion_servicio = $request->input('descripcion_servicio');
        $servicio->precio_servicio = $request->input('precio_servicio');
        $servicio->update();

        return redirect()->route('servicios.index')
                        ->with('success','Servicio Actualizado con Exito');
    }


    public function destroy($id)
    {
        DB::table("servicios")->where('id',$id)->delete();
        return redirect()->route('servicios.index')
                        ->with('success','Servicio Eliminado con Éxito');
    }



}
