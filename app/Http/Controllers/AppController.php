<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Task;
use App\User;
use App\PagosApp;
use App\Servicio;
use App\Alarma;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Redirect;
use Response;
use Carbon\Carbon;
use Mail;

class AppController extends Controller
{
    protected $urlgetopab = null;
    protected $urlgetopce = null;
    protected $urlmarket = null;
    protected $urlsavetoken = null;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function pago_app_register(Request $request)
    {
        //$pagos = PagosApp::all()->paginate(10);
        $pagos = new PagosApp();
        $pagos->id_user = $request->id_user;
        $pagos->monto = $request->monto;
        $pagos->fecha_pago = $request->fecha_pago;
        $pagos->save();

        return response()->json([
            'success' => true,
            'pagos' => $pagos
        ]);  

    }

    public function app_register_pasarela(Request $request)
    {

        $user = DB::table('users')
            ->select('users.*')
            ->where('email', '=', $request->email)
            ->get();

        if ($user->count() != 0){
            return response()->json([
                'success' => false,
                'message' => 'Usuario ya existe',

            ]); 
        }
        else{
                Validator::make($request->all(), [
                        'nombre'   => 'required|max:50|regex:[[a-zA-Z]{1,} ?[a-zA-Z]{0,}]',
                        'apellido' => 'required|regex:[[a-zA-Z]{1,} ?[a-zA-Z]{0,}]|max:50',
                        'nicknombre' => 'required|alpha_num|max:191',
                        'telefono'      => 'max:16',
                        'edad'     => 'numeric|between:0,120|nullable|present',
                        'email' => 'email|unique:users,email',
                        'password' => 'required|string|min:5|max:191',
                    ])->validate();         
        }

        $mytime = Carbon::now();
        $month = $mytime->addMonth();
        $servicio = Servicio::all();

        DB::beginTransaction();

        $usuario = new User();
        $usuario->nombre   = $request->nombre;
        $usuario->apellido = $request->apellido;
        $usuario->edad     = $request->edad;
        $usuario->telefono = $request->telefono;
        $usuario->nicknombre = $request->nicknombre;
        $usuario->role = 'USUARIO';
        $usuario->habilitado = 1;
        $usuario->habilitado_app = 1;
        $usuario->bandera_plataforma = 7;
        $usuario->bandera_app = 1;
        $usuario->fecha_duracion_app = $month->toDateTimeString();
        $usuario->email = $request->email;
        if (!empty($request->password)) {
            $usuario->password = bcrypt($request->password);
        }
        $usuario->foto = 'images/user.png';
        $usuario->save();


        $pagos = new PagosApp();
        $pagos->id_user = $usuario->id;
        $pagos->monto = $servicio[0]->servicios_precio7;
        $pagos->fecha_pago = $mytime->toDateTimeString();
        $pagos->save();



        DB::commit();
        if (!$usuario) {
            DB::rollBack();
            return response()->json([
                'success' => false,
            ]); 
        }


            return response()->json([
                'success' => true,
                'id_user' => $usuario->id
            ]);  

    }


    public function app_register(Request $request)
    {

        $user = DB::table('users')
            ->select('users.*')
            ->where('email', '=', $request->email)
            ->get();

        if ($user->count() != 0){
                Validator::make($request->all(), [
                        'email' => 'email|unique:users,email',
                    ])->validate();
        }
        else{
                Validator::make($request->all(), [
                        'nombre'   => 'required|max:50|regex:[[a-zA-Z]{1,} ?[a-zA-Z]{0,}]',
                        'apellido' => 'required|regex:[[a-zA-Z]{1,} ?[a-zA-Z]{0,}]|max:50',
                        'nicknombre' => 'required|alpha_num|max:191',
                        'telefono'      => 'max:16',
                        'edad'     => 'numeric|between:0,120|nullable|present',
                        'email' => 'email|unique:users,email',
                        'password' => 'required|string|min:5|max:191',
                    ])->validate();         
        }

        $mytime = Carbon::now();
        $month = $mytime->addMonth();
        $servicio = Servicio::all();

        DB::beginTransaction();

        $usuario = new User();
        $usuario->nombre   = $request->nombre;
        $usuario->apellido = $request->apellido;
        $usuario->edad     = $request->edad;
        $usuario->telefono = $request->telefono;
        $usuario->nicknombre = $request->nicknombre;
        $usuario->role = 'USUARIO';
        $usuario->habilitado = 1;
        $usuario->habilitado_app = 1;
        $usuario->bandera_plataforma = 7;
        $usuario->bandera_app = 1;
        $usuario->fecha_duracion_app = $month->toDateTimeString();
        $usuario->email = $request->email;
        if (!empty($request->password)) {
            $usuario->password = bcrypt($request->password);
        }
        $usuario->foto = 'images/user.png';
        $usuario->save();


        $pagos = new PagosApp();
        $pagos->id_user = $usuario->id;
        $pagos->monto = $servicio[0]->servicios_precio7;
        $pagos->fecha_pago = $mytime->toDateTimeString();
        $pagos->save();



        DB::commit();
        if (!$usuario) {
            DB::rollBack();
            return response()->json([
                'success' => false,
            ]); 
        }


            return response()->json([
                'success' => true,
                'id_user' => $usuario->id
            ]);  

    }

    public function monto_app(){
        $servicio = Servicio::all();

            return response()->json([
                'success' => true,
                'pago' => $servicio[2]->precio_servicio
            ]);  
    }

    public function PagosApp($id_user)
    {
        $pagos = DB::table('pagos_app')
            ->select('pagos_app.*')
            ->where('id_user', '=', $id_user)
            ->get();

        //$pagos = PagosApp::all()->paginate(10);

            return response()->json([
                'success' => true,
                'pagos' => $pagos
            ]);  
    }


    public function UsersAppProsperity(Request $request)
    {
        $users = DB::table('prosperity_users')
            ->select('prosperity_users.*')
            ->get();

        //$pagos = PagosApp::all()->paginate(10);

            return response()->json([
                'success' => true,
                'users' => $users
            ]);  
    }

    public function registro_app(Request $request){

        $user = DB::table('users')
            ->select('users.*')
            ->where('email', '=', $request->email)
            ->get();

        if ($user->count() != 0){
                Validator::make($request->all(), [
                        'email' => 'email|unique:users,email',
                    ])->validate();
        }
        else{
                Validator::make($request->all(), [
                        'nombre'   => 'required|max:50|regex:[[a-zA-Z]{1,} ?[a-zA-Z]{0,}]',
                        'apellido' => 'required|regex:[[a-zA-Z]{1,} ?[a-zA-Z]{0,}]|max:50',
                        'nicknombre' => 'required|alpha_num|max:191',
                        'telefono'      => 'max:16',
                        'edad'     => 'numeric|between:0,120|nullable|present',
                        'email' => 'email|unique:users,email',
                        'password' => 'required|string|min:5|max:191',
                    ])->validate();         
        }

        $arreglo = ['nombre' => $request->nombre, 'apellido' => $request->apellido, 'edad' => $request->edad, 'telefono' => $request->tel, 'nicknombre' => $request->nicknombre, 'role' => 'USUARIO', 'habilitado' => 0, 'email' => $request->email, 'foto' => 'images/user.png', 'password' => bcrypt($request->password), 'habilitado_app' => 0];

            $request->session()->put('profit.app.data', $arreglo); // guardamos usuario en session

        if ($request->tipo_pago == "APPLE") {
                $mytime = Carbon::now();
                $month = $mytime->addMonth();

                $usuario = new User();
                $usuario->nombre   = $arreglo['nombre'];
                $usuario->apellido = $arreglo['apellido'];
                $usuario->edad     = $arreglo['edad'];
                $usuario->telefono = $arreglo['telefono'];
                $usuario->nicknombre = $arreglo['nicknombre'];
                $usuario->role = 'USUARIO';
                $usuario->habilitado = 1;
                $usuario->habilitado_app = 1;
                $usuario->transaction_id = $request->transaction_id;
                $usuario->bandera_app = 1;
                $usuario->email = $arreglo['email'];
                $usuario->fecha_duracion_app = $month->toDateTimeString();
                if (!empty($arreglo['password'])) {
                    $usuario->password = bcrypt($arreglo['password']);
                }
                $usuario->foto = 'images/user.png';
                $usuario->save();


                    $arreglo = ['email' => $usuario->email];

                    Mail::send('emails.welcome_app', $arreglo, function($message) use($arreglo){
                        $message->to($arreglo['email']);
                        $message->subject('Gracias por Adquirir Profit Calls');
                    });

        return redirect()->route('login');
        }

        return redirect()->route('paypal.express-checkout-app');

    }

    public function registro_app_seller(Request $request){

        $user = DB::table('users')
            ->select('users.*')
            ->where('email', '=', $request->email)
            ->get();

        if ($user->count() != 0){
                Validator::make($request->all(), [
                        'email' => 'email|unique:users,email',
                    ])->validate();
        }
        else{
                Validator::make($request->all(), [
                        'nombre'   => 'required|max:50|regex:[[a-zA-Z]{1,} ?[a-zA-Z]{0,}]',
                        'apellido' => 'required|regex:[[a-zA-Z]{1,} ?[a-zA-Z]{0,}]|max:50',
                        'nicknombre' => 'required|alpha_num|max:191',
                        'telefono'      => 'max:16',
                        'edad'     => 'numeric|between:0,120|nullable|present',
                        'email' => 'email|unique:users,email',
                        'password' => 'required|string|min:5|max:191',
                    ])->validate();         
        }

        $arreglo = ['nombre' => $request->nombre, 'apellido' => $request->apellido, 'edad' => $request->edad, 'telefono' => $request->telefono, 'nicknombre' => $request->nicknombre, 'role' => 'USUARIO', 'habilitado' => 0, 'email' => $request->email, 'foto' => 'images/user.png', 'password' => bcrypt($request->password), 'habilitado_app' => 0, 'id_seller' => $request->id_seller];

        $request->session()->put('profit.app.data', $arreglo); // guardamos usuario en session

        return redirect()->route('paypal.express-checkout-app-seller');
    }


    public function index()
    {
        $users = User::all();
        foreach ($users as $user) {
            $user->fecha_duracion_app = '2019-10-15 00:00:00';
            $user->save();
        }

        dd("Todo Listo");

    }

    public function index_users_token()
    {
            $users = DB::table('users')
                ->where('token', '!=', null)
                ->where('token', '!=', '')
                ->select('users.fecha_duracion_app', 'users.id', 'users.nombre','users.apellido','users.email','users.email','users.pais','users.email', 'users.habilitado','users.habilitado_app', 'users.token')
                ->get();

            return response()->json([
                'success' => true,
                'users' => $users
            ]);  
    }


    public function app_login(Request $request)
    {
        $token = $request->token;       

        if((Auth::attempt(['email'=>$request->email,'password'=>$request->password])) && ($token === 'Profit4Life')){
            $user = DB::table('users')
                ->join('role_user', 'role_user.user_id', '=', 'users.id')
                ->join('roles', 'roles.id', '=', 'role_user.role_id')
                ->where('users.email', $request->email)
                ->select('users.fecha_duracion_app', 'users.id', 'users.nombre','users.apellido','users.email','users.email','users.pais','users.email', 'users.habilitado','users.habilitado_app', 'users.token', 'roles.name as role')
                ->first();


            if($request->tokenapn == 'Null' || $request->tokenapn == null || $request->tokenapn == '' ||$request->tokenapn == 'null' || $request->tokenapn == NULL || $request->tokenapn == 'NULL'){
                return response()->json([
                    'success' => true,
                    'usuario' => $user
                ]);   
            }
            else{
                DB::update('update users set token = :token where id = :id ', ['token' => $request->tokenapn, 'id' => $user->id]);
                return response()->json([
                    'success' => true,
                    'usuario' => $user
                ]);                
            }
        }
        else{
            return response()->json([
                'success' => false,
            ]);
        }
    }

    public function redeem_code(Request $request)
    {
        $this->validate($request, [
            'token' => 'required',
            'apple_code' => 'required|unique:apple_codes',
        ]);

        $token = $request->token;       

        if(($token === 'Profit4life') && $request->apple_code != ''){
            DB::insert('insert into apple_codes (apple_code, flag) values (?, ?)', [$request->apple_code, true]);                
                return response()->json([
                    'success' => true,
                ]);   
        }
        else{
            return response()->json([
                'success' => false,
            ]);
        }
    }


    public function switch_app(Request $request)
    {

        switch ($request->opcion)
        {
            case "mensajemasivo":
                $mensaje=$this->checkget("mensaje");
                $this->sendGCM($mensaje,"/topics/trading","Profit4Life");            
                //echo "true";
                break;
            case "precios":
                $symbol=$this->checkget("symbol");
                $ask=$this->checkget("ask");
                $bid=$this->checkget("bid");
                $result = DB::table('cotizacion')
                    ->where('symbol', $symbol)
                    ->get();
                if (count($result)>0){
                    $id=$result[0]->id;
                    DB::update('update cotizacion set ask = :ask, bid = :bid where id = :id ', ['ask' => $ask, 'bid' => $bid, 'id' => $id]);
                    $this->CheckAlarms($id,$symbol,$ask);

                }else{
                    DB::insert('insert into cotizacion (ask,bid,symbol,decimales,factor) values (?,?, ?, ?,?)', [$ask, $bid, $symbol, 1, 1]);
                }
                break;
            case "abriroperacion":
                $symbol=$this->checkget("symbol");
                $price=$this->checkget('price');
                $tp=$this->checkget('tp');
                $sl=$this->checkget('sl');
                $tipo=$this->checkget('tipo');
                $ticket=$this->checkget('ticket');
                $tipo=strtolower($tipo);
                $fecha_inicio = date('Y-m-d H:i:s');
                $ticket=strtolower($ticket);

                if($tipo === ""){
                    //id   symbol price  tipo   close  status fecha_inicio   fecha_fin  sl tp
                    DB::insert('insert into operacion (symbol, price, tipo, fecha_inicio, sl, tp, status, ticket, close) values (?, ?, ?, ?, ?, ?, ?, ?, ?)', [$symbol, $price, 'Long', $fecha_inicio, $sl, $tp, 'open', $ticket, 0]);
                    break;
                }
                else{
                    //id   symbol price  tipo   close  status fecha_inicio   fecha_fin  sl tp
                    DB::insert('insert into operacion (symbol, price, tipo, fecha_inicio, sl, tp, status, ticket, close) values (?, ?, ?, ?, ?, ?, ?, ?, ?)', [$symbol, $price, $tipo, $fecha_inicio, $sl, $tp, 'open', $ticket, 0]);
                    break;
                }
            case "abrirpendiente":
                $symbol=$this->checkget("symbol");
                $price=$this->checkget('price');
                $tp=$this->checkget('tp');
                $sl=$this->checkget('sl');
                $tipo=$this->checkget('tipo');
                $ticket=$this->checkget('ticket');
                $tipo=strtolower($tipo);
                $fecha_inicio = date('Y-m-d H:i:s');
                $ticket=strtolower($ticket);

                if($tipo === ""){
                    //id   symbol price  tipo   close  status fecha_inicio   fecha_fin  sl tp
                    DB::insert('insert into operacion (symbol, price, tipo, fecha_inicio, sl, tp, status, ticket, close) values (?, ?, ?, ?, ?, ?, ?, ?, ?)', [$symbol, $price, 'Long', $fecha_inicio, $sl, $tp, 'pending', $ticket, 0]);
                    break;
                }
                else{
                    //id   symbol price  tipo   close  status fecha_inicio   fecha_fin  sl tp
                    DB::insert('insert into operacion (symbol, price, tipo, fecha_inicio, sl, tp, status, ticket, close) values (?, ?, ?, ?, ?, ?, ?, ?, ?)', [$symbol, $price, $tipo, $fecha_inicio, $sl, $tp, 'pending', $ticket, 0]);
                    break;
                }
            case "cerraroperacion":
                $symbol=$this->checkget("symbol");
                $price=$this->checkget('price');
                $ticket=$this->checkget('ticket');
                $fecha_fin = date('Y-m-d H:i:s');
                $ticket=strtolower($ticket);

                //id   symbol price  tipo   close  status fecha_inicio   fecha_fin  sl tp
                DB::update('update operacion set close = :price, status = :closed, fecha_fin = :fecha_fin where ticket = :ticket ', ['price' => $price, 'closed' => 'closed', 'fecha_fin' => $fecha_fin, 'ticket' => $ticket]);
                break;

            case "veroperacionesabiertas":
                $result = DB::table('operacion')
                    ->leftjoin('cotizacion', 'cotizacion.symbol', '=', 'operacion.symbol')
                    ->where('operacion.status', '=', 'open')
                    ->select('operacion.id', 'operacion.ticket', 'operacion.symbol', 'operacion.price','operacion.tipo', 'operacion.close', 'operacion.status', 'operacion.fecha_inicio','operacion.fecha_fin', 'operacion.sl', 'operacion.tp', 'cotizacion.ask','cotizacion.bid', 'cotizacion.decimales')
                    ->get();

                $rows=array();
                //var_dump($result);
                if (count($result) > 0) {
                    foreach($result as $r){
                        if (is_null($r->ask)) $r->ask=0;
                        if (is_null($r->bid)) $r->bid=0;
                        $r->tipo=strtolower($r->tipo);
                        $cor=10**($r->decimales);
                        $r->tipo=strtolower($r->tipo);
                        $tipo=$r->tipo;
                        //echo "tipo:$tipo\n Cor:$cor\n";
                        if ($r->tipo=='long'){

                            $r->pips=($r->bid-$r->price)*$cor;
                        }
                        else {
                            $r->pips=(-$r->ask+$r->price)*$cor;
                        }
                        $fechaD = date("d/m/Y",strtotime($r->fecha_inicio));
                        $horaD = date("H:i:s",strtotime($r->fecha_inicio));


//                        $r['fechaD']=date("d/m/Y",strtotime($r->fecha_inicio));
//                        $r['horaD']=

                        $rows[] = $r;
//                        $rows[] = $fechaD;
//                        $rows[] = $horaD;

                    }
                }
                //url="https://midasbottraders.com/messagetprofit4l.php?tipo="+tipo+"&entrada="+DoubleToStr(precio)+"&tp="+DoubleToStr(tp)+"&sl="+DoubleToStr(sl)+"&simbolo="+par+"&riesgo="+riesgo;
                $json['data']=$rows;
                echo json_encode($json);
                break;

            case "veroperacionespendientes":
                $result = DB::table('operacion')
                    ->leftjoin('cotizacion', 'cotizacion.symbol', '=', 'operacion.symbol')
                    ->where('operacion.status', '=', 'pending')
                    ->select('operacion.id', 'operacion.ticket', 'operacion.symbol', 'operacion.price','operacion.tipo', 'operacion.close', 'operacion.status', 'operacion.fecha_inicio','operacion.fecha_fin', 'operacion.sl', 'operacion.tp', 'cotizacion.ask','cotizacion.bid', 'cotizacion.decimales')
                    ->get();

                $rows=array();
                //var_dump($result);
                if (count($result) > 0) {
                    foreach($result as $r){
                        if (is_null($r->ask)) $r->ask=0;
                        if (is_null($r->bid)) $r->bid=0;
                        $r->tipo=strtolower($r->tipo);
                        $cor=10**($r->decimales);
                        $r->tipo=strtolower($r->tipo);
                        $tipo=$r->tipo;
                        //echo "tipo:$tipo\n Cor:$cor\n";
                        if ($r->tipo=='long'){

                            $r->pips=($r->bid-$r->price)*$cor;
                        }
                        else {
                            $r->pips=(-$r->ask+$r->price)*$cor;
                        }
                        $fechaD = date("d/m/Y",strtotime($r->fecha_inicio));
                        $horaD = date("H:i:s",strtotime($r->fecha_inicio));


//                        $r['fechaD']=date("d/m/Y",strtotime($r->fecha_inicio));
//                        $r['horaD']=

                        $rows[] = $r;
//                        $rows[] = $fechaD;
//                        $rows[] = $horaD;

                    }
                }
                //url="https://midasbottraders.com/messagetprofit4l.php?tipo="+tipo+"&entrada="+DoubleToStr(precio)+"&tp="+DoubleToStr(tp)+"&sl="+DoubleToStr(sl)+"&simbolo="+par+"&riesgo="+riesgo;
                $json['data']=$rows;
                echo json_encode($json);
                break;

            case "veroperacionescerradas":
                $result = DB::table('operacion')
                    ->leftjoin('cotizacion', 'cotizacion.symbol', '=', 'operacion.symbol')
                    ->where('operacion.status', '=', 'closed')
                    ->select('operacion.id', 'operacion.ticket', 'operacion.symbol', 'operacion.price','operacion.tipo', 'operacion.close', 'operacion.status', 'operacion.fecha_inicio','operacion.fecha_fin', 'operacion.sl', 'operacion.tp', 'cotizacion.ask','cotizacion.bid', 'cotizacion.decimales')
                    ->orderBy('fecha_inicio', 'DESC')
                    ->get();

                $rows=array();
                //var_dump($result);
                $ganadas=0;
                $perdidas=0;
                $pipsganados=0;
                $pipsperdidos=0;
                if ($result) {
                    if (count($result) > 0) {
                        foreach($result as $r){
                            $cor = 1 / (10 ** $r->decimales);
                            $r->tipo = strtolower($r->tipo);
                            $rows[] = $r;
                        }
                    }
                }
                $json['data']['dataresumen']['pipsganados']=$pipsganados;
                $json['data']['dataresumen']['pipsperdidos']=$pipsperdidos;


                //url="https://midasbottraders.com/messagetprofit4l.php?tipo="+tipo+"&entrada="+DoubleToStr(precio)+"&tp="+DoubleToStr(tp)+"&sl="+DoubleToStr(sl)+"&simbolo="+par+"&riesgo="+riesgo;
                $json['data']['datos']=$rows;
                echo json_encode($json);
                break;


            case "vermercado":

                $result = DB::table('cotizacion')
                    ->select('cotizacion.*')
                    ->get();

                //return this.http.get(this.urlbase+'?opcion=agregarcita&dr=jlld&fecha='+cita.fecha+'&nombre='+cita.nombre+'&telefono='+cita.telefono+'&patologia='+cita.patologia+'&cedula='+cita.cedula);

                $json['exito']=true;
                $json['data']=$result;

                echo json_encode($json);

                break;

            case "tokensave":
                $token=$this->checkget("token");
                $fecha = date('Y-m-d H:i:s');
                $user=$this->checkget("user");
                DB::insert('insert into tokens (token,fecha,user) values (?, ?, ?)', [$token, $fecha, $user]);

                //echo $sql;
                $json['exito']=false;

                echo json_encode($json);
                break;

            case "insertaralarma":
                $id_par=$this->checkget("id_par");
                $fecha = date('Y-m-d H:i:s');
                $id_usuario=$this->checkget("id_usuario");

                $result = DB::table('cotizacion')
                    ->where('id', '=', $id_par)
                    ->select('cotizacion.*')
                    ->first();

                    $inicio= null;

                if ($result)
                {
                    $inicio=$result->ask;
                }

                $precio=$this->checkget("precio");

                $alarma = new Alarma();
                $alarma->id_cotizacion =  $id_par;
                $alarma->id_usuario =  $id_usuario;
                $alarma->precio =  $precio;
                $alarma->inicio =  $inicio;
                $alarma->save();

//                DB::insert('insert into alarmas (id_cotizacion,fecha,id_usuario,precio,inicio) values (?, ?, ?, ?, ?)', [$id_par, $fecha, $id_usuario, $precio, $inicio]);

                //echo $sql;
                $json['exito']=false;
                if ($result)
                {
                    $json['exito']=true;
                }
                echo json_encode($json);
                break;

            case "veralarmas":
                $id_usuario=$this->checkget("id_usuario");
                $result = DB::table('alarmas')
                    ->leftjoin('cotizacion', 'cotizacion.id', '=', 'alarmas.id_cotizacion')
                    ->where('alarmas.id_usuario', '=', $id_usuario)
                    ->where('alarmas.ejecutado', '=', 0)
                    ->select('alarmas.precio', 'alarmas.fecha','cotizacion.symbol', 'alarmas.id_alarmas')
                    ->get();

                //return this.http.get(this.urlbase+'?opcion=agregarcita&dr=jlld&fecha='+cita.fecha+'&nombre='+cita.nombre+'&telefono='+cita.telefono+'&patologia='+cita.patologia+'&cedula='+cita.cedula);
                $rows = [];
                if ($result) {
                    if (count($result) > 0) {
                        foreach($result as $r){
                            $rows[] = $r;
                        }
                    }
                }
                $json['exito']=true;
                $json['data']=$rows;
                if ($result)
                {
                    $json['exito']=false;
                }
                echo json_encode($json);

                break;

            case "borraralarma":
                $id=$this->checkget("id");
                DB::delete('delete from alarmas where id_alarmas = '.$id);
                //return this.http.get(this.urlbase+'?opcion=agregarcita&dr=jlld&fecha='+cita.fecha+'&nombre='+cita.nombre+'&telefono='+cita.telefono+'&patologia='+cita.patologia+'&cedula='+cita.cedula);

                $json['exito']=true;
                echo json_encode($json);
                break;
            case "borraroperacion":
                $id=$this->checkget("id");
                DB::update('update operacion set status = :status where id = :id ', ['status' => 'deleted', 'id' => $id]);

//                DB::delete('delete from operacion where id = '.$id);
                $json['exito']=true;
                echo json_encode($json);
                break;
        }


    }

    public function CheckAlarms($id,$symbol,$ask){
        $result = DB::table('alarmas')
            ->leftjoin('users', 'users.id', '=', 'alarmas.id_usuario')
            ->where('alarmas.id_cotizacion', '=', $id)
            ->where('alarmas.ejecutado', '=', 0)
            ->select('alarmas.precio', 'alarmas.inicio','alarmas.id_alarmas', 'users.token')
            ->get();
        $rows=array();
        //var_dump($result);
        if (count($result) > 0) {
            foreach($result as $r){
                $precio=$r->precio;
                $inicio=$r->inicio;
                $id2=$r->id_alarmas;
                $token=$r->token;
                if ($inicio<$precio){
                    if ($ask>=$precio){
                        $this->sendGCM("Precio superado para el par $symbol->$ask",$token,"Alarma");
                        DB::update('update alarmas set ejecutado = 1 where id_alarmas = :id ', ['id' => $id2]);
                    }
                }
                if ($inicio>$precio){

                    if ($ask<=$precio){
                        $this->sendGCM("Precio superado para el par $symbol->$ask",$token,"Alarma");
                        DB::update('update alarmas set ejecutado = 1 where id_alarmas = :id ', ['id' => $id2]);
                    }
                }
            }
        }

        $result2 = DB::table('operacion')
            ->where('operacion.status', '=', 'pending')
            ->select('operacion.*')
            ->get();

        if (count($result2) > 0) {
            foreach($result2 as $r){
                $precio = $r->precio;
                $inicio = $r->price_cierre;
                $id2 = $r->id;
                if ($inicio < $precio) {
                    if ($ask >= $precio) {
                        //sendGCM("Precio superado para el par \n$symbol->$ask",$token,"Alarma");
                        $this->sendGCM("Orden pendiente en el par $symbol ejecutada","/topics/grupopago",$symbol);
                        DB::update('update operacion set status = open where id = :id ', ['id' => $id2]);

                    }
                }
                if ($inicio > $precio) {
                    if ($ask <= $precio) {
                        //sendGCM("Precio superado para el par $symbol \n->$ask",$token,"Alarma");
                        $this->sendGCM("Orden pendiente en el par $symbol ejecutada","/topics/grupopago",$symbol);
                        DB::update('update operacion set status = open where id = :id ', ['id' => $id2]);
                    }

                }
            }
        }

    }

    public function checkget($var)
    {
        $ret="";
        if (isset($_GET[$var]))
        {
            $ret=$_GET[$var];
        }
        return $ret;
    }

    public function sendGCM($message, $id,$titulo) {
        $url = 'https://fcm.googleapis.com/fcm/send';
        $fields['notification']['title']=$titulo;
        $fields['notification']['body']=htmlentities($message);
        $fields['notification']['sound']="default";
        $fields['to']=$id;
        $fields['priority']="high";
        $fields['restricted_package_nombre']="";


        $fields = json_encode ( $fields );

        $headers = array (
            'Authorization: key=' . "AAAAqGeXYkY:APA91bGqS-gdEjpWl36Ofj8orb3d2Hb6SkU3jcJeFRtHr1G7qrmWe1gvd50VhWGIqDD6T8mUzbNy9WZo5KkZotsWd0lhH46e54pkZNJ_xlbicFXmCuRp2av_WmimQmhTxKZFswSn2L7d",
            'Content-Type: application/json'
        );

        $ch = curl_init ();
        curl_setopt ( $ch, CURLOPT_URL, $url );
        curl_setopt ( $ch, CURLOPT_POST, true );
        curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
        curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt ( $ch, CURLOPT_POSTFIELDS, $fields );

        $result = curl_exec ( $ch );
        echo $result;
        curl_close ( $ch );
    }
}
