<?php
  
namespace App\Http\Controllers;
  
use Illuminate\Http\Request;
use Srmklive\PayPal\Services\ExpressCheckout;
use App\Http\Requests;
use Redirect;
use Session;  
use DB;  
use App\User;
use Carbon\Carbon;
use App\Invoice;

class PayPalController extends Controller
{
	protected $provider;

	public function __construct() {
	    $this->provider = new ExpressCheckout();
	}


    public function payment()
    {
        $invoice = new Invoice();
        $invoice->save();
        $suma = 0;
        $promo_premium = 0;
        $promo_combo = 0;
        $data = [];

        if(Session::has('cart_filtrado')){
            $carrito = Session::get('cart_filtrado');
            foreach ($carrito as $key => $value) {
                if($value->id == 1 || $value->id == 2 || $value->id == 4 || $value->id == 5){
                    $promo_premium = $promo_premium + 1;
                }
                if($value->id == 1 || $value->id == 2){
                    $promo_combo = $promo_combo + 1;
                }
                $suma = $suma + $value->precio_servicio;
            }
        }


         if($promo_combo == 2){
            $suma = $suma - 400;
            $data['items'] = [
                [
                    'name' => 'Servicios Profit 4 Life',
                    'price' => $suma,
                    'desc'  => 'Servicios Profit 4 Life',
                    'qty' => 1
                ]
            ];
            $data['invoice_id'] = $invoice->id;
            $data['invoice_description'] = "Order #{$data['invoice_id']} Invoice";
            $data['return_url'] = route('payment.success');
            $data['cancel_url'] = route('payment.cancel');
            $data['total'] = $suma;

        $response =  $this->provider->setExpressCheckout($data);
        $response =  $this->provider->setExpressCheckout($data, true);
  
        return redirect($response['paypal_link']);

        }

        if($promo_premium == 4){
            $suma = $suma - 950;
            $data['items'] = [
                [
                    'name' => 'Servicios Profit 4 Life',
                    'price' => $suma,
                    'desc'  => 'Servicios Profit 4 Life',
                    'qty' => 1
                ]
            ];
            $data['invoice_id'] = $invoice->id;
            $data['invoice_description'] = "Order #{$data['invoice_id']} Invoice";
            $data['return_url'] = route('payment.success');
            $data['cancel_url'] = route('payment.cancel');
            $data['total'] = $suma;
        $response =  $this->provider->setExpressCheckout($data);
        $response =  $this->provider->setExpressCheckout($data, true);
  
        return redirect($response['paypal_link']);

        }

            $data['items'] = [
                [
                    'name' => 'Servicios Profit 4 Life',
                    'price' => $suma,
                    'desc'  => 'Servicios Profit 4 Life',
                    'qty' => 1
                ]
            ];
            $data['invoice_id'] = $invoice->id;
            $data['invoice_description'] = "Order #{$data['invoice_id']} Invoice";
            $data['return_url'] = route('payment.success');
            $data['cancel_url'] = route('payment.cancel');
            $data['total'] = $suma;
  
        $response =  $this->provider->setExpressCheckout($data);
        $response =  $this->provider->setExpressCheckout($data, true);
  
        return redirect($response['paypal_link']);
    }
   
    /**
     * Responds with a welcome message with instructions
     *
     * @return \Illuminate\Http\Response
     */
    public function cancel()
    {
       	return view('errors.cancel_paypal');
    }
  
    /**
     * Responds with a welcome message with instructions
     *
     * @return \Illuminate\Http\Response
     */
    public function success(Request $request)
    {
        $usuario = Session::get('profit.user.data');
        $carrito = Session::get('cart');
        $suma = 0;
        $mytime = Carbon::now();
        $datetime = $mytime->addMonth();

        $user = DB::table('users')
            ->select('users.*')
            ->where('email', '=', $usuario['email'])
            ->get();
        $services = null;
        $servicios_ids = [];
        $carritos_ids = [];
        $servicios_id = [];
        if(count($user) != 0){
            $services = DB::table('servicios')
                                ->join('users_servicios', 'users_servicios.servicio_id', '=', 'servicios.id')
                                ->join('users', 'users.id', '=', 'users_servicios.user_id')
                                ->where('users_servicios.user_id', '=', $user[0]->id)
                                ->select('servicios.*')            
                                ->distinct()            
                                ->get();            
        foreach ($services as $key => $value) {
            $service = $value->id;
            array_push($servicios_ids, $service);
        }
        
        $servicios_id = array_unique($servicios_ids);

        }



        foreach ($carrito as $key => $value) {
            $car = $value->id;
            array_push($carritos_ids, $car);
        }

        $carritos_id = array_unique($carritos_ids);

        $response =  $this->provider->getExpressCheckoutDetails($request->token);
  
        if (in_array(strtoupper($response['ACK']), ['SUCCESS', 'SUCCESSWITHWARNING'])) {

                        if(count($user) != 0){
                            for($i = 0; $i < count($carritos_id); $i++){
                                if((array_search($carritos_id[$i],$servicios_id)) == false){
//                                    DB::insert('insert into role_user (user_id, role_id) values(?,?)', [$user[0]->id, 2]);
                                    DB::insert('insert into users_servicios (servicio_id, user_id) values(?,?)', [$carritos_id[$i], $user[0]->id]);
                                }
                            }
                        }
                        else{
                            $new_user = new User();
                            $new_user->nombre = $usuario['nombre'];
                            $new_user->apellido = $usuario['apellido'];
                            $new_user->phone = $usuario['phone'];
                            $new_user->pais = $usuario['pais'];
                            $new_user->email = $usuario['email'];
                            $new_user->password = $usuario['password'];
                            $new_user->habilitado = 1;
                            $new_user->save();
                            DB::insert('insert into role_user (user_id, role_id) values(?,?)', [$new_user->id, 2]);
                            for($i = 0; $i < count($carritos_id); $i++){
                                DB::insert('insert into users_servicios (servicio_id, user_id) values(?,?)', [$carritos_id[$i], $new_user->id]);
                                if($carritos_id[$i] == 3){
                                    $new_user->habilitado_app = 1;
                                    $new_user->fecha_duracion_app = $datetime;
                                    $new_user->save();
                                }
                            }
                        }
            Session::flush();        
        	return view('errors.success_paypal');
        }
        	return view('errors.error_paypal');
    }
}