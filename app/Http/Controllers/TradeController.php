<?php

namespace App\Http\Controllers;
use Redirect;
use Illuminate\Http\Request;
use App\Trade;

class TradeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
    	$token = $request->token;

    	if($token === 'Profit4Life'){
	    	return Trade::all();
    	}
    	else{
    		abort(404);
    	}
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    	$token = $request->token;

    	if($token === 'Profit4Life'){
	        $trade = new Trade();
	        $trade->par = 			$request->par;
	        $trade->fecha = 		$request->fecha;
	        $trade->rentabilidad =  $request->rentabilidad;
	        $trade->FechaCierre = 	$request->FechaCierre;
	        $trade->save();

	        return $trade;    		
    	}
    	else{
    		abort(404);
    	}
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Trade::findOrfail($id)->first();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    	$token = $request->token;

    	if($token === 'Profit4Life'){
	        $trade = Trade::find($id);
	        $trade->par 			= 	$request->par;
	        $trade->fecha 			= 	$request->fecha;
	        $trade->rentabilidad 	= 	$request->rentabilidad;
	        $trade->FechaCierre 	= 	$request->FechaCierre;
	        $trade->update();

	    return $trade;
    	}
    	else{
    		abort(404);
    	}

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
    	$token = $request->token;

    	if($token === 'Profit4Life'){
	        $trade = Trade::findOrfail($id);
	        $trade->delete();    
        return $id;
    	}
    	else{
    		abort(404);
    	}
    }
}
