<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Crypt;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use App\Servicio;

class FlipBookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    use AuthenticatesUsers;




    public function index2()
    {
        $flipbook_cripto = DB::table('flipbook')->get();
        $flipbook_profit = DB::table('flipbook')->get();
        $algo = explode(",",$flipbook_profit[0]->content);
        $algo2 = explode(",",$flipbook_cripto[1]->content);
        $retornar = $algo[0];
        $retornar2 = $algo2[0];
        $flipbooks = DB::table('flipbook')->get();

        return view('rudrarajiv.flipbooklaravel.index',compact('flipbooks','flipbook_cripto','retornar', 'flipbook_profit', 'retornar2'));
    }





    public function index(Request $request, $id)
    {

        //
       // echo Carbon::now($timezone)->toDateTimeString();
       // $current_time = ($timezone) ? Carbon::now(str_replace('-', '/', $timezone)) : Carbon::now();
        //echo $time->toDateTimeString();
      // return view('flipbook::time', compact('current_time'));

        try {
            $d = Crypt::decrypt($id);
        } catch (DecryptException $e) {
            abort(404);
        }

        $flipbook = DB::table('flipbook')->where('id', $d)->get()[0];
        $algo = explode(",",$flipbook->content);
        $retornar = $algo[0];

        return view('rudrarajiv.flipbooklaravel.bookindex',compact('flipbook', 'd', 'retornar'));
    }


    public function libro_nuevo($id)
    {
        try {
            $d = Crypt::decrypt($id);
        } catch (DecryptException $e) {
            abort(404);
        }

        $flipbook = DB::table('flipbook')->where('id', $d)->get()[0];
       //dd($flipbook);
        $content = explode(",",$flipbook->content);

            $services = DB::table('servicios')
                ->join('users_servicios', 'users_servicios.servicio_id', '=', 'servicios.id')
                ->join('users', 'users.id', '=', 'users_servicios.user_id')
                ->where('users_servicios.user_id', '=', Auth::user()->id)
                ->select('servicios.*')            
                ->distinct()            
                ->get();

            $servicios = Servicio::all();

        return view('rudrarajiv.flipbooklaravel.libro',compact('flipbook','content', 'd', 'services', 'servicios'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('rudrarajiv.flipbooklaravel.bookcreater');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $input = $request->all();
      //  return dd($input);

        $content = "";
        $i = 1;
        foreach($request->files as $uploadedFile )
        //if($request->hasFile('flip_img'))
        {
           // echo "hi";
            // $request->file()
           // $name = $uploadedFile->getClientOriginalName();

            //$image =  $request->file('flip_img');
            $filename  = time().'_'. $i . '.' . $uploadedFile->getClientOriginalExtension();
            $i++;
            $file = $uploadedFile->move(public_path('rudra/fbook/pics/'), $filename);
           //$path = public_path('rudra/flipbook/pics/' . $filename);
            $path = 'rudra/fbook/pics/' . $filename;

           // Image::make($image->getRealPath())->resize(200, 200)->save($path);
            $content .= $path .",";

        }

        $input['content'] = rtrim($content,",");
        $input['habilitado'] = 1;
        $input['name'] = $input['book_name'];
       // $input['urlname'] = strtolower(preg_replace('/[ ,]+/', '-', trim($input['name'])));
        Flipbook::create($input);
        //dd($input);
        //exit;
       // Subcategory::create($input);
        return Redirect::route('rudra.flipbook.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $d = Crypt::decrypt($id);
        } catch (DecryptException $e) {
            abort(404);
        }

        $flipbook = DB::table('flipbook')->where('id', $d)->get()[0];
       //dd($flipbook);
        $content = explode(",",$flipbook->content);
            return view('rudrarajiv.flipbooklaravel.showbook',compact('flipbook','content'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $flipbook = DB::table('flipbook')->where('id', $id)->get()[0];
        $content = explode(",",$flipbook->content);
        return view('rudrarajiv.flipbooklaravel.editbook',compact('flipbook','content'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $fb = Flipbook::find($id);
        $input = $request->all();
        //  return dd($input);

        $fb->content .= ",";
        $i = 1;
        foreach($request->files as $uploadedFile )
            //if($request->hasFile('flip_img'))
        {
            // echo "hi";
            // $request->file()
            // $name = $uploadedFile->getClientOriginalName();

            //$image =  $request->file('flip_img');
            $filename  = time().'_'. $i . '.' . $uploadedFile->getClientOriginalExtension();
            $i++;
            $file = $uploadedFile->move(public_path('rudra/fbook/pics/'), $filename);
            //$path = public_path('rudra/flipbook/pics/' . $filename);
            $path = 'rudra/fbook/pics/' . $filename;

            // Image::make($image->getRealPath())->resize(200, 200)->save($path);
            $fb->content .= $path .",";

        }

        $fb->content = rtrim($fb->content,",");

        $fb->name = $input['book_name'];
        $fb->desc = $input['desc'];
        // $input['urlname'] = strtolower(preg_replace('/[ ,]+/', '-', trim($input['name'])));
        $fb->save();
        //dd($input);
        //exit;
        // Subcategory::create($input);
        return Redirect::route('flipbook.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $fb = Flipbook::find($id);
        $imgArr = explode(",",$fb->content);
        foreach($imgArr as $img)
        {
            $image = public_path($img);
        if (file_exists($image)) {
            unlink($image);
        }
        }
        $fb->delete();
        return Redirect::route('flipbook.index');
    }
}
