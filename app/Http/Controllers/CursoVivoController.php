<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Slider;
use App\Task;
use App\Servicio;
use App\User;
use Session;
use Auth; 
use Mail;
use DB;
use Redirect;
use App\Invoice;
use App\UserCursoVivo;
use Illuminate\Support\Facades\Log;

class CursoVivoController extends Controller {


    public function index() {
        $cursos = Task::all();
        return view('presencial.index', compact('cursos'));
    }

    public function create() {
        return view('presencial.create');
    }


	public function store(Request $request) {

        $curso = new Task();
        $curso->name = $request->input('name');
        $curso->description = $request->input('description');
        $curso->task_date = $request->input('task_date');
        $curso->task_date2 = $request->input('task_date2');
        $curso->disponibilidad = $request->input('disponibilidad');
        $curso->save();


        return redirect()->route('presencial.index')->with('success', 'Curso Creado Exitosamente');
	}


    public function store_user(Request $request) {

        $user_data = ['name' => $request->name, 'apellido' => $request->apellido, 'phone' => $request->phone, 'email' => $request->email, 'curso' => $request->curso, 'modalidad' => $request->modalidad ];

        Session::put('profit.presencial.data', $user_data);            

        return redirect()->route('paypal.express-checkout-presencial');
    }



    public function destroy($id)
    {
        $task = Task::findOrfail($id);
        $task->delete();
        return redirect()->route('presencial.index')->with('success', 'Curso Eliminado Exitosamente');
    }

}

