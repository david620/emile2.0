<?php
namespace App\Http\Controllers;

use App\Http\Requests;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use Auth;

class ImageController extends Controller {

    public function __construct()
   {
        $this->middleware('auth');
   } 
    public function getImage($filename) {
       $path = public_path().'/flipbook/fbook/pics/'.$filename;
       $type = "image/jpeg";
       header('Content-Type:'.$type);
       readfile($path);

    }
    public function getImage2($filename) {
       $path =  public_path().'/flipbook/fbook/pics2/'.$filename;
       $type = "image/jpeg";
       header('Content-Type:'.$type);
       readfile($path);

    }
 }