<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Video;
use App\Categoria;
use DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Redirect;

class VideosController extends Controller
{


    public function index()
    {
        //$videos = Video::orderBy('id', 'DESC')->get();
        $videos = DB::table('videos')
            ->join('categorias', 'categorias.id', '=', 'videos.categoria_id')
            ->join('servicios', 'servicios.id', '=', 'categorias.plataforma_categoría')
            ->select('videos.nombre_video', 'videos.descripcion_video', 'videos.iframe_video','categorias.nombre_categoria', 'videos.id', 'servicios.nombre_servicio')
            ->orderBy('videos.id', 'asc')
            ->get();
        return view('videos.index', compact('videos'));
    }

    public function create()
    {
        //$videos = Video::orderBy('id', 'DESC')->get();
        $categorias = Categoria::pluck('nombre_categoria','id');
        return view('videos.create', compact('categorias'));
    }

    public function edit($id)
    {
        $video = Video::find($id);
        $categoria = Categoria::where('id','=', $video->categoria_id)->first();
//        $categorias = Categoria::where('id','=', $video->categoria_id)->pluck('nombre_categoria','id');

        $categorias = DB::table('categorias')
                    ->join('videos', 'videos.categoria_id', '=', 'categorias.id')
                    ->join('servicios', 'servicios.id', '=', 'categorias.plataforma_categoría')
                    ->where('servicios.id', '=', $categoria->plataforma_categoría)
                    ->select('categorias.*')
                    ->pluck('nombre_categoria','id');

        $videos = DB::table('videos')
                    ->join('categorias', 'categorias.id', '=', 'videos.categoria_id')
                    ->where('categorias.id', '=', $video->categoria_id)
                    ->select('videos.*')
                    ->get();
        $posiciones = count($videos);

        return view('videos.edit', compact('categorias', 'categoria', 'video', 'posiciones'));
    }

    public function store(Request $request)
    {

        $this->validate($request, [
            'nombre_video' => 'required',
            'descripcion_video' => 'required',
            'iframe_video' => 'required',
            'categoria_id' => 'required',
        ]);

        $video = new Video();
        $video->nombre_video = $request->input('nombre_video');
        $video->descripcion_video = $request->input('descripcion_video');
        $video->iframe_video = $request->input('iframe_video');
        $video->categoria_id = $request->input('categoria_id');
        $video->save();

        return redirect()->route('videos.index')
                        ->with('success','Video creado exitosamente');
    }


    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'nombre_video' => 'required',
            'descripcion_video' => 'required',
            'iframe_video' => 'required',
            'categoria_id' => 'required',
        ]);

        $video = Video::find($id);
        $video->nombre_video = $request->input('nombre_video');
        $video->descripcion_video = $request->input('descripcion_video');
        $video->iframe_video = $request->input('iframe_video');
        $video->categoria_id = $request->input('categoria_id');
        $video->posicion_video = $request->input('posicion');
        $video->save();

        return redirect()->route('videos.index')
                        ->with('success','Video Editado exitosamente');
    }



    public function destroy($id)
    {
        DB::table("videos")->where('id',$id)->delete();
        return redirect()->route('videos.index')
                        ->with('success','Video Eliminado con Éxito');
    }



}
