<?php

namespace App\Exports;

use App\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

class UsersExport implements FromCollection, ShouldAutoSize, WithHeadings, WithEvents
{
    public function collection()
    {
        return User::all();
    }

    public function headings(): array
    {
        return [
            '#Identificador',
            'Nombre',
            'Apellido',
            'Nickname',
            'País',
            'Teléfono',
            'Correo',
            'token',
            'Email Verificado',
            'Password',
            'Fecha Duración App',
            'Estado',
            'Estado App',
            'Plataforma',
            'remember_token',
            'Created at',
            'Updated at'
        ];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $cellRange = 'A1:Q1'; // 
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(14);
            },
        ];
    }


}