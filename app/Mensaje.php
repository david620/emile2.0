<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mensaje extends Model {
	protected $primaryKey = 'id_mensajes';

	protected $fillable = [
		'titulo', 'mensaje', 'link_imagen',
	];

}