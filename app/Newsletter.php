<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Newsletter extends Model {
	protected $table = 'newsletters';
	protected $primaryKey = 'id';
	public $timestamps = true;
    public $incrementing = true;

	protected $fillable = [
		'*',
	];

}