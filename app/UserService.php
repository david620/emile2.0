<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserService extends Model {
	protected $table = 'users_servicios';
	protected $primaryKey = 'id';
	
    public $incrementing = true;
	
	protected $fillable = [
		'*',
	];

}