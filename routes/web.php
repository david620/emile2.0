<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('users/intermedia', ['as' => 'intermedia', 'uses' => 'UsuariosController@intermedia']);

///////////////////----------     Inicio Rutas de Paypal     ----------///////////////////

Route::get('payment', ['as'   => 'paypal.payment', 'uses'   => 'PayPalController@payment']);
Route::get('cancel', 'PayPalController@cancel')->name('payment.cancel');
Route::get('payment/success', 'PayPalController@success')->name('payment.success');

///////////////////----------     Inicio Rutas de Paypal  App   ----------///////////////////

Route::get('paypal_app/payment', ['as'   => 'paypal.payment-app', 'uses'   => 'PayPalAppController@payment']);
Route::get('paypal_app/cancel', 'PayPalAppController@cancel')->name('payment.cancel-app');
Route::get('paypal_app/payment/success', 'PayPalAppController@success')->name('payment.success-app');

///////////////////----------     Inicio Rutas de Paypal Prosperity     ----------///////////////////

Route::get('paypal_prosperity/payment', ['as'   => 'paypal.express-checkout-prosperity', 'uses'   => 'PaypalProsperityController@payment']);
Route::get('paypal_prosperity/cancel', 'PaypalProsperityController@cancel')->name('payment.cancel-prosperity');
Route::get('paypal_prosperity/success', 'PaypalProsperityController@success')->name('payment.success-prosperity');


///////////////////----------     Inicio Rutas de Paypal Rifa     ----------///////////////////

Route::get('paypal_rifa/payment', ['as'   => 'paypal.express-checkout-concurso', 'uses'   => 'PayPalConcursoController@payment']);
Route::get('paypal_rifa/cancel', 'PayPalConcursoController@cancel')->name('payment.cancel-rifa');
Route::get('paypal_rifa/success', 'PayPalConcursoController@success')->name('payment.success-rifa');


///////////////////----------     Inicio Rutas de Paypal Presencial     ----------///////////////////

Route::get('paypal_presencial/payment', ['as'   => 'paypal.express-checkout-presencial', 'uses'   => 'PayPalPresencialController@payment']);
Route::get('paypal_presencial/cancel', 'PayPalPresencialController@cancel')->name('payment.cancel-presencial');
Route::get('paypal_presencial/success', 'PayPalPresencialController@success')->name('payment.success-presencial');

///////////////////----------     Fin Rutas de Paypal     ----------///////////////////

///////////////////----------     Inicio Rutas de VISTAS     ----------///////////////////

Route::post('contact/send/message', ['as'   => 'contact.store', 'uses'   => 'SiteController@contact_store']);

Route::get('/', ['as' => 'home.index', 'uses' => 'SiteController@home']);

Route::get('forex', ['as' => 'home.forex', 'uses' => 'SiteController@forex']);

Route::get('servicios', ['as' => 'home.servicios', 'uses' => 'SiteController@servicios']);

Route::get('presencial/curso', ['as' => 'home.presencial', 'uses' => 'SiteController@presencial']);

Route::post('presencial/create/usuario/curso', ['as' => 'usuario-presencial.store', 'uses' => 'CursoVivoController@store_user']);


Route::get('concurso_rifa/sorteo/{id}', ['as' => 'concurso_rifa_ganador', 'uses' => 'ConcursoRifaController@generar_ganador']);

Route::get('concurso', ['as' => 'home.concurso', 'uses' => 'SiteController@concurso']);

Route::get('producto/{id}', ['as' => 'home.producto', 'uses' => 'SiteController@producto']);

Route::get('carrito', ['as' => 'home.carrito', 'uses' => 'SiteController@carrito']);

Route::get('carrito/agregar/{id}', ['as' => 'home.agregar_carrito', 'uses' => 'SiteController@agregar_carrito']);

Route::get('carrito/remover', ['as' => 'home.remover_carrito', 'uses' => 'SiteController@remover_carrito']);

Route::get('pasarela', ['as' => 'home.pasarela', 'uses' => 'SiteController@pasarela']);

Route::get('terminos', ['as' => 'home.terminos', 'uses' => 'SiteController@terminos']);

Route::get('recuperar', ['as' => 'home.recuperar', 'uses' => 'SiteController@recuperar']);

Route::get('/profitcalls/buy/app', ['as' => 'home.app', 'uses' => 'SiteController@app']);

Route::get('/prosperity/buy/app', ['as' => 'home.prosperity', 'uses' => 'ProsperityController@app_index']);
Route::post('prosperity/buy', ['as' => 'prosperity_store', 'uses' => 'ProsperityController@prosperity_store']);

Route::get('wsacademy', ['as' => 'home.wsacademy', 'uses' => 'SiteController@wsacademy']);


Route::post('store/carrito', ['as' => 'store.carrito', 'uses' => 'SiteController@store_carrito']);

Route::post('store/app', ['as' => 'store.app', 'uses' => 'SiteController@store_app']);

///////////////////----------     Fin Rutas de VISTAS     ----------///////////////////


///////////////////----------     Inicio Rutas de COCURSO     ----------///////////////////

Route::get('/concurso_rifa/ganador/{id}', ['as' => 'concurso_ganador', 'uses' => 'ConcursoRifaController@ganador']);

Route::post('concurso_rifa/store', ['as' => 'concurso_rifa_store', 'uses' => 'ConcursoRifaController@store']);







///////////////////----------     Fin Rutas de COCURSO     ----------///////////////////





Route::group(['middleware' => 'auth'], function () {

Route::get('lista/categorias/{id}', ['as' => 'categorias.categorias_lista', 'uses' => 'CategoriasController@categoria_lista'])->middleware('auth', 'role:USUARIO');

Route::get('lista/videos/{id}', ['as' => 'video_categorias', 'uses' => 'SiteController@video_categorias'])->middleware('auth', 'role:USUARIO');


Route::get('lista/archivos', ['as' => 'archivos.archivos_lista', 'uses' => 'ArchivosController@archivos_lista'])->middleware('auth', 'role:USUARIO');

Route::get('users/prosperity', ['as' => 'users_prosperity.index', 'uses' => 'ProsperityController@index_prosperity'])->middleware('auth', 'role:ADMINISTRADOR');
Route::get('users/wsacademy', ['as' => 'users_wsacademy.index', 'uses' => 'WsAcademyController@index_wsacademy'])->middleware('auth', 'role:ADMINISTRADOR');

Route::get('video_detalles/{id}', ['as' => 'home.video_detalles', 'uses' => 'SiteController@video_detalles'])->middleware('auth', 'role:USUARIO');

Route::get('servicio/{id}', ['as' => 'home.video_lista', 'uses' => 'SiteController@video_lista'])->middleware('auth', 'role:USUARIO');

Route::get('perfil', ['as' => 'home.perfil', 'uses' => 'SiteController@perfil'])->middleware('auth', 'role:USUARIO');

Route::get('inicio', ['as' => 'home.administrador', 'uses' => 'SiteController@home_ADMINISTRADOR'])->middleware('auth', 'role:ADMINISTRADOR');

Route::get('contacto/', ['as' => 'contacto.index', 'uses' => 'ContactController@index'])->middleware('auth', 'role:ADMINISTRADOR');
Route::delete('contacto/delete/{id}', ['as' => 'mails.destroy', 'uses'   => 'ContactController@destroy'])->middleware('auth', 'role:ADMINISTRADOR');

Route::get('categorias/', ['as' => 'categorias.index', 'uses' => 'CategoriasController@index'])->middleware('auth', 'role:ADMINISTRADOR');
Route::get('categorias/create/', ['as' => 'categorias.create', 'uses' => 'CategoriasController@create'])->middleware('auth', 'role:ADMINISTRADOR');
Route::post('categorias/create/', ['as' => 'categorias.store', 'uses' => 'CategoriasController@store'])->middleware('auth', 'role:ADMINISTRADOR');
Route::delete('categorias/delete/{id}', ['as' => 'categorias.destroy', 'uses'   => 'CategoriasController@destroy'])->middleware('auth', 'role:ADMINISTRADOR');


Route::get('archivos/index', ['as' => 'archivos.index', 'uses' => 'ArchivosController@index'])->middleware('auth', 'role:ADMINISTRADOR');
Route::get('archivos/create/', ['as' => 'archivos.create', 'uses' => 'ArchivosController@create'])->middleware('auth', 'role:ADMINISTRADOR');
Route::post('archivos/create/', ['as' => 'archivos.store', 'uses' => 'ArchivosController@store'])->middleware('auth', 'role:ADMINISTRADOR');
Route::delete('archivos/delete/{id}', ['as' => 'archivos.destroy', 'uses'   => 'ArchivosController@destroy'])->middleware('auth', 'role:ADMINISTRADOR');

Route::get('videos/', ['as' => 'videos.index', 'uses' => 'VideosController@index'])->middleware('auth', 'role:ADMINISTRADOR');
Route::get('videos/create/', ['as' => 'videos.create', 'uses' => 'VideosController@create'])->middleware('auth', 'role:ADMINISTRADOR');
Route::get('videos/edit/{id}', ['as' => 'videos.edit', 'uses' => 'VideosController@edit'])->middleware('auth', 'role:ADMINISTRADOR');
Route::post('videos/create/', ['as' => 'videos.store', 'uses' => 'VideosController@store'])->middleware('auth', 'role:ADMINISTRADOR');
Route::patch('videos/edit/{id}', ['as' => 'videos.update', 'uses' => 'VideosController@update'])->middleware('auth', 'role:ADMINISTRADOR');

Route::delete('videos/delete/{id}', ['as' => 'videos.destroy', 'uses'   => 'VideosController@destroy'])->middleware('auth', 'role:ADMINISTRADOR');


Route::get('presencial/', ['as' => 'presencial.index', 'uses' => 'CursoVivoController@index'])->middleware('auth', 'role:ADMINISTRADOR');
Route::get('presencial/create', ['as' => 'presencial.create', 'uses' => 'CursoVivoController@create'])->middleware('auth', 'role:ADMINISTRADOR');
Route::post('presencial/create', ['as' => 'presencial.store', 'uses' => 'CursoVivoController@store'])->middleware('auth', 'role:ADMINISTRADOR');
Route::delete('presencial/delete/{id}', ['as' => 'presencial.destroy', 'uses'   => 'CursoVivoController@destroy'])->middleware('auth', 'role:ADMINISTRADOR');

Route::get('preguntas/', ['as' => 'preguntas.index', 'uses' => 'PreguntasController@index'])->middleware('auth', 'role:ADMINISTRADOR');
Route::get('preguntas/responder/{id}', ['as' => 'preguntas.edit', 'uses' => 'PreguntasController@edit'])->middleware('auth', 'role:ADMINISTRADOR');
Route::post('preguntas/store', ['as' => 'preguntas.store', 'uses' => 'PreguntasController@store'])->middleware('auth', 'role:USUARIO');
Route::post('preguntas/answer/{id}', ['as' => 'preguntas.answer', 'uses' => 'PreguntasController@answer'])->middleware('auth', 'role:ADMINISTRADOR');
Route::delete('preguntas/delete/{id}', ['as' => 'preguntas.destroy', 'uses'   => 'PreguntasController@destroy'])->middleware('auth', 'role:ADMINISTRADOR');

Route::get('usuarios/', ['as' => 'usuarios.index', 'uses' => 'UsuariosController@index'])->middleware('auth', 'role:ADMINISTRADOR');
Route::get('usuarios/create/', ['as' => 'usuarios.create', 'uses' => 'UsuariosController@create'])->middleware('auth', 'role:ADMINISTRADOR');
Route::post('usuarios/create/', ['as' => 'usuarios.store', 'uses' => 'UsuariosController@store'])->middleware('auth', 'role:ADMINISTRADOR');
Route::get('usuarios/edit/{id}', ['as' => 'usuarios.edit', 'uses' => 'UsuariosController@edit'])->middleware('auth', 'role:ADMINISTRADOR');
Route::patch('usuarios/edit/{id}', ['as' => 'usuarios.update', 'uses' => 'UsuariosController@update'])->middleware('auth', 'role:ADMINISTRADOR');
Route::delete('usuarios/delete/{id}', ['as' => 'usuarios.destroy', 'uses'   => 'UsuariosController@destroy'])->middleware('auth', 'role:ADMINISTRADOR');
Route::get('usuarios/modificaEstado/{id}', ['as' => 'usuarios.modificaEstado', 'uses'      => 'UsuariosController@modificaEstado']);
Route::get('usuarios/habilitarServicios/{id}', ['as' => 'usuarios.habilitarServicios', 'uses'      => 'UsuariosController@habilitarServicios']);

Route::get('servicios/index', ['as' => 'servicios.index', 'uses' => 'ServiciosController@index'])->middleware('auth', 'role:ADMINISTRADOR');
Route::get('servicios/edit/{id}', ['as' => 'servicios.edit', 'uses' => 'ServiciosController@edit'])->middleware('auth', 'role:ADMINISTRADOR');
Route::patch('servicios/edit/{id}', ['as' => 'servicios.update', 'uses' => 'ServiciosController@update'])->middleware('auth', 'role:ADMINISTRADOR');
Route::delete('servicios/delete/{id}', ['as' => 'servicios.destroy', 'uses'   => 'ServiciosController@destroy'])->middleware('auth', 'role:ADMINISTRADOR');

Route::get('listado/concursos', ['as' => 'listado_concursos', 'uses' => 'ConcursoRifaController@listadoConcursos'])->middleware('auth', 'role:ADMINISTRADOR');
Route::get('listado/concursos/{id}', ['as'   => 'concursos.destroy', 'uses'   => 'ConcursoRifaController@destroy'])->middleware('auth', 'role:ADMINISTRADOR');
Route::get('listado/concursos/{id}/tmodificaEstado/', ['as' => 'concursos.modificaEstado', 'uses'      => 'ConcursoRifaController@modificaEstado'])->middleware('auth', 'role:ADMINISTRADOR');
Route::get('listado/concursos/edit/{id}', ['as' => 'listado_concursos_editar', 'uses' => 'ConcursoRifaController@editarlistadoConcursos'])->middleware('auth', 'role:ADMINISTRADOR');
Route::post('listado/concursos', ['as' => 'concursos.store', 'uses' => 'ConcursoRifaController@store_concurso'])->middleware('auth', 'role:ADMINISTRADOR');
Route::patch('listado/concursos/{id}', ['as' => 'concursos.update', 'uses' => 'ConcursoRifaController@update_concurso'])->middleware('auth', 'role:ADMINISTRADOR');
Route::get('listado/usuarios/concurso/{id}', ['as' => 'listado_usuarios_concurso', 'uses' => 'ConcursoRifaController@listadoConcurso'])->middleware('auth', 'role:ADMINISTRADOR');


Route::get('export-users', ['as' => 'users.export', 'uses' => 'UsuariosController@export'])->middleware('auth', 'role:ADMINISTRADOR');



Route::get('flipbook', ['as' => 'flipbook.index', 'uses' => 'FlipBookController@index'])->middleware('auth', 'role:USUARIO');

Route::post('flipbook', ['as' => 'flipbook.store', 'uses' => 'FlipBookController@store'])->middleware('auth', 'role:ADMINISTRADOR');

Route::get('/flipbook/create', ['as' => 'flipbook.create', 'uses' => 'FlipBookController@create'])->middleware('auth', 'role:ADMINISTRADOR');


Route::patch('/flipbook/{flipbook}', ['as' => 'flipbook.update', 'uses' => 'FlipBookController@update'])->middleware('auth', 'role:USUARIO');

Route::delete('/flipbook/{flipbook}', ['as' => 'flipbook.destroy', 'uses' => 'FlipBookController@destroy'])->middleware('auth', 'role:USUARIO');

Route::get('/flipbook/{flipbook}/edit', ['as' => 'flipbook.edit', 'uses' => 'FlipBookController@edit'])->middleware('auth', 'role:USUARIO');

Route::get('usuario_libro', ['as' => 'usuario_libro.index', 'uses' => 'FlipBookController@index2'])->middleware('auth', 'role:USUARIO');

Route::get('usuario_libro/libro/choose/{id}', ['as' => 'usuario_libro.index_choose', 'uses' => 'FlipBookController@index'])->middleware('auth', 'role:USUARIO');


Route::get('/flipbook/{flipbook}', ['as' => 'flipbook.show', 'uses' => 'FlipBookController@show'])->middleware('auth', 'role:USUARIO');

Route::get('/flipbook_libro/{flipbook}', ['as' => 'flipbook.libro', 'uses' => 'FlipBookController@libro_nuevo'])->middleware('auth', 'role:USUARIO');


Route::get('/images/profit/{file}','ImageController@getImage')->middleware('auth', 'role:USUARIO');
Route::get('/images/cripto/{file}','ImageController@getImage2')->middleware('auth', 'role:USUARIO');

});
