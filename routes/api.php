<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|

*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('profit4life/trades/index', ['as' => 'trades_index_new', 'uses' => 'TradeController@index']);

Route::get('profit4life/trades/show/{id}', ['as' => 'trades_show', 'uses' => 'TradeController@show']);

Route::post('profit4life/trades/store', ['as' => 'trades_store', 'uses' => 'TradeController@store']);

Route::post('profit4life/pago/app', ['as' => 'pago_app', 'uses' => 'AppController@pago_app_register']);

Route::post('profit4life/pago/app/register', ['as' => 'pago_app_registers', 'uses' => 'AppController@app_register']);

Route::get('profit4life/pasarela/app/register', ['as' => 'pago_app_registers', 'uses' => 'AppController@app_register_pasarela']);


Route::patch('profit4life/trades/update/{id}', ['as' => 'trades_update', 'uses' => 'TradeController@update']);

Route::delete('profit4life/trades/delete/{id}', ['as' => 'trades_delete', 'uses' => 'TradeController@destroy']);

//Login App
Route::get('profit4life/app/auth', ['as' => 'app_auth', 'uses' => 'AppController@app_login']);

Route::get('profit4life/app/switch_app', ['as' => 'switch_app', 'uses' => 'AppController@switch_app']);



Route::get('profit4life/testimonios/all', ['as' => 'testimonios_paginated', 'uses' => 'TestimoniosController@index']);

Route::post('profit4life/testimonios/store', ['as' => 'testimonios_store', 'uses' => 'TestimoniosController@store']);

Route::get('profit4life/testimonios/show/{testimonios}', ['as' => 'testimonios_show', 'uses' => 'TestimoniosController@show']);

Route::get('profit4life/testimonios/delete/{id}', ['as' => 'testimonios_delete', 'uses' => 'TestimoniosController@destroy']);


Route::get('profit4life/news/all', ['as' => 'news_paginated', 'uses' => 'NoticiasController@index']);

Route::post('profit4life/news/store', ['as' => 'news_store', 'uses' => 'NoticiasController@store']);

Route::get('profit4life/news/show/{news}', ['as' => 'news_show', 'uses' => 'NoticiasController@show']);

Route::get('profit4life/news/delete/{id}', ['as' => 'news_delete', 'uses' => 'NoticiasController@destroy']);


Route::get('profit4life/pagos/monto', ['as' => 'monto_pago', 'uses' => 'AppController@monto_app']);

Route::get('profit4life/pagos/all/{id}', ['as' => 'pagos_paginated', 'uses' => 'AppController@PagosApp']);

Route::post('profit4life/messages/store', ['as' => 'messages_store', 'uses' => 'MensajesController@store']);

Route::get('profit4life/messages/show/{messages}', ['as' => 'messages_show', 'uses' => 'MensajesController@show']);

Route::get('profit4life/messages/delete/{id}', ['as' => 'messages_delete', 'uses' => 'MensajesController@destroy']);

Route::get('profit4life/messages/all', ['as' => 'messages_paginated', 'uses' => 'MensajesController@index']);

Route::get('profit4life/users/all_token', ['as' => 'users_token', 'uses' => 'AppController@index_users_token']);

Route::get('profit4life/users/all/wsacademy', ['as' => 'users_wsacademy', 'uses' => 'WsAcademyController@index_users_wsacademy']);

Route::get('profit4life/users/all/prosperity', ['as' => 'users_prosperity', 'uses' => 'ProsperityController@index_users_prosperity']);

Route::get('profit4life/app/redeem/code', ['as' => 'redeem_code', 'uses' => 'AppController@redeem_code']);



Route::group(['prefix' => 'v1','middleware' => 'auth:api'], function () {
    //    Route::resource('task', 'TasksController');

    //Please do not remove this if you want adminlte:route and adminlte:link commands to works correctly.
    #adminlte_api_routes
});
