<!doctype html>
<html class="no-js" lang="es">
@include('layouts.head')
<body>

<!-- Page Wrapper -->
<div id="wrap"> 
  
  <!-- Header -->
@include('layouts.header')
  <!-- End Header --> 
  
  <!--======= HOME MAIN SLIDER =========-->
  <section class="home-slider">
    <div class="tp-banner-container">
      <div class="tp-banner">
        <ul>
          <!-- SLIDE  -->
          <li data-transition="random" data-slotamount="7" data-masterspeed="300"  data-saveperformance="off" > 
            <!-- MAIN IMAGE --> 
            <img src="{{URL::asset('img/sliders/slider1.png')}}"  alt="slider"  data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat"> 
            <!-- LAYERS --> 
            
            <!-- LAYER NR. 1 -->
            <div class="tp-caption biglargetext sfr tp-resizeme" 
                data-x="center" data-hoffset="0" 
                data-y="center" data-voffset="-90" 
                data-speed="500" 
                data-start="1000" 
                data-easing="Power3.easeInOut" 
                data-splitin="chars" 
                data-splitout="none" 
                data-elementdelay="0.06" 
                data-endelementdelay="0.1" 
                data-endspeed="300"
                style="z-index: 5; max-width: auto; max-height: auto; white-space: nowrap;"> </div>
            
            <!-- LAYER NR. 2 -->
            <div class="tp-caption verybiglargetext sfr tp-resizeme" 
                data-x="center" data-hoffset="0" 
                data-y="center" data-voffset="40" 
                data-speed="500" 
                data-start="1500" 
                data-easing="Power3.easeInOut" 
                data-splitin="chars" 
                data-splitout="none" 
                data-elementdelay="0.07" 
                data-endelementdelay="0.1" 
                data-endspeed="300" 
                style="z-index: 6; max-width: auto; max-height: auto; white-space: nowrap;"> </div>
            
            <!-- LAYER NR. 3 -->
            <div class="tp-caption font-crimson font-italic sfb tp-resizeme" 
                data-x="center" data-hoffset="0" 
                data-y="center" data-voffset="140" 
                data-speed="500" 
                data-start="1900" 
                data-easing="Power3.easeInOut" 
                data-splitin="none" 
                data-splitout="none" 
                data-elementdelay="0.1" 
                data-endelementdelay="0.1" 
                data-endspeed="300" 
                style="z-index: 7; font-size:18px; color:#fff; max-width: auto; max-height: auto; white-space: nowrap;"> </div>
            
            <!-- LAYER NR. 4 -->
            <div class="tp-caption lfb tp-scrollbelowslider" 
                data-x="center" data-hoffset="0" 
                data-y="bottom" data-voffset="-50" 
                data-speed="500" 
                data-start="2400" 
                data-easing="Power3.easeInOut" 
                data-elementdelay="0.1" 
                data-endelementdelay="0.1" 
                data-endspeed="300" 
                data-scrolloffset="0"
                style="z-index: 8;"> </div>
          </li>
          <!-- SLIDE  -->
          <li data-transition="random" data-slotamount="7" data-masterspeed="300"  data-saveperformance="off" > 
            <!-- MAIN IMAGE --> 
            <img src="{{URL::asset('img/sliders/slider2.png')}}"  alt="home_slider2"  data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat"> 
            <!-- LAYERS --> 
            
            <!-- LAYER NR. 1 -->
            <div class="tp-caption sfb tp-resizeme" 
                data-x="center" data-hoffset="0" 
                data-y="center" data-voffset="-170" 
                data-speed="600" 
                data-start="1000" 
                data-easing="Power3.easeInOut" 
                data-splitin="chars" 
                data-splitout="none" 
                data-elementdelay="0.07" 
                data-endelementdelay="0.1" 
                data-endspeed="300" 
                style="z-index: 5; font-size:40px; color:#fff; max-width: auto; max-height: auto; white-space: nowrap;"></div>
            
            <!-- LAYER NR. 2 -->
            <div class="tp-caption font-crimson font-italic sfb tp-resizeme" 
                data-x="center" data-hoffset="0" 
                data-y="center" data-voffset="-100" 
                data-speed="600" 
                data-start="2500" 
                data-easing="Power3.easeInOut" 
                data-splitin="none" 
                data-splitout="none" 
                data-elementdelay="0.1" 
                data-endelementdelay="0.1" 
                data-endspeed="300" 
                style="z-index: 6; font-size:24px; color:#fff; max-width: auto; max-height: auto; white-space: nowrap;"> </div>
            
            <!-- LAYER NR. 4 -->
            <div class="tp-caption sfb" 
                data-x="center" data-hoffset="0" 
                data-y="center" data-voffset="150" 
                data-speed="500" 
                data-start="3300" 
                data-easing="Power3.easeInOut" 
                data-elementdelay="0.1" 
                data-endelementdelay="0.1" 
                data-endspeed="300" 
                style="z-index: 8;"></div>
            
            <!-- LAYER NR. 6 -->
            <div class="tp-caption skewfromleft tp-resizeme" 
                data-x="center" data-hoffset="0" 
                data-y="bottom" data-voffset="20" 
                data-speed="400" 
                data-start="4100" 
                data-easing="Power3.easeInOut" 
                data-splitin="none" 
                data-splitout="none" 
                data-elementdelay="0.1" 
                data-endelementdelay="0.1" 
                data-endspeed="300" 
                style="z-index: 10; max-width: auto; max-height: auto; white-space: nowrap;"></div>
          </li>
        </ul>
      </div>
    </div>
  </section>
  
  <!-- Content -->
  <div id="content"> 
    <div class="tradingview-widget-container">
  <div class="tradingview-widget-container__widget"></div>
  <script type="text/javascript" src="https://s3.tradingview.com/external-embedding/embed-widget-ticker-tape.js" async>
  {
  "symbols": [
    {
      "title": "S&P 500",
      "proName": "INDEX:SPX"
    },
    {
      "title": "Shangai Composite",
      "proName": "INDEX:XLY0"
    },
    {
      "title": "EUR/USD",
      "proName": "FX_IDC:EURUSD"
    },
    {
      "title": "BTC/USD",
      "proName": "BITFINEX:BTCUSD"
    },
    {
      "title": "ETH/USD",
      "proName": "BITFINEX:ETHUSD"
    }
  ],
  "theme": "light",
  "isTransparent": false,
  "displayMode": "adaptive",
  "locale": "es"
}
  </script>
</div>
    <!-- Welcome -->
    <section class="welcome padding-top-80">
      <div class="container">
        <div class="row"> 
          
          <!-- Intro Image -->
          <div class="col-md-3 text-center"> <a href="#"> <img class="img-responsive" src="{{URL::asset('img/intro/intro.png')}}" alt="Intro Image" /> </a> </div>
          
          <!-- Intro Text -->
          <div class="col-md-9">
            <div class="heading-block no-margin"> 
              <h2 class="margin-bottom-30">Bienvenidos</h2>
            </div>
            <p align="justify">Los saluda Emile Machado, CEO de Profit4Life, la primera academia de Forex online en español. Aquí aprenderás y desarrollarás habilidades para hacer trading en el mercado Forex.</p> 
            <br> 
            <p align="justify"> Los precios de los activos finacieros cambian frecuentemente, permitiendo comprar y vender de forma lucrativa acciones, oro, divisas, índices y recursos energéticos. Gracias al internet éste mundo de las finanzas puede estar al alcance de las manos.</p>
            
            <!-- Icon Row -->
            <div class="row margin-top-30"> 
              
              <!-- Icon -->
              <div class="col-md-4">
                <div class="icon-box ib-style-1 ib-circle ib-bordered ib-bordered-light ib-small">
                  <div class="ib-icon"> <i class="fa fa-instagram"></i> </div>
                  <div class="ib-info">
                    <a href="https://www.instagram.com/emiletrader/"><h6>emiletrader</h6></a>
                  </div>
                </div>
              </div>
              
              <!-- Icon -->
              <div class="col-md-4">
                <div class="icon-box ib-style-1 ib-circle ib-bordered ib-bordered-light ib-small">
                  <div class="ib-icon"> <i class="fa fa-youtube-play"></i> </div>
                  <div class="ib-info">
                    <a href="https://www.youtube.com/channel/UCpF5xy-jW3rhqN8uElpVYVw"><h6>EmileTraderFx</h6></a>
                  </div>
                </div>
              </div>
              
              <!-- Icon -->
              <div class="col-md-4">
                <div class="icon-box ib-style-1 ib-circle ib-bordered ib-bordered-light ib-small">
                  <div class="ib-icon"> <i class="fa fa-facebook-square"></i> </div>
                  <div class="ib-info">
                    <a href="https://www.facebook.com/Profit4lifevzla/"><h6>Profit4life</h6></a>
                  </div>
                </div>
              </div>
            </div>
            <!-- Icon --> 
            <a href="{{route('home.servicios')}}" class="btn btn-large dark-border font-normal margin-top-80 letter-space-1">Más Servicios</a> </div>
        </div>
      </div>
    </section>
    
    <!-- PORTFOLIO -->
    <section class="portfolio port-wrap padding-top-80">
      
      
      <!-- PORTFOLIO ITEMS -->
      <div class="items row col-4"> 
        
        <!-- ITEM -->
        <article class="portfolio-item pf-branding-design pf-web-design">
          <div class="portfolio-image"><img alt="Open Imagination" src="{{URL::asset('/img/pictures/1.jpg')}}"></div>
          
        </article>
        
        <!-- ITEM -->
        <article class="portfolio-item pf-branding-design pf-web-design">
          <div class="portfolio-image"><img alt="Open Imagination" src="{{URL::asset('/img/pictures/2.jpg')}}"></div>
          
        </article>
        
        <!-- ITEM -->
        <article class="portfolio-item pf-branding-design pf-web-design">
          <div class="portfolio-image"><img alt="Open Imagination" src="{{URL::asset('/img/pictures/3.jpg')}}"></div>
          
        </article>
        
        <!-- ITEM -->
        <article class="portfolio-item pf-branding-design pf-web-design">
          <div class="portfolio-image"><img alt="Open Imagination" src="{{URL::asset('/img/pictures/4.png')}}"></div>
          
        </article>
        
        <!-- ITEM -->
        <article class="portfolio-item pf-branding-design pf-web-design">
          <div class="portfolio-image"><img alt="Open Imagination" src="{{URL::asset('/img/pictures/5.jpg')}}"></div>
          
        </article>
        
        <!-- ITEM -->
        <article class="portfolio-item pf-branding-design pf-web-design">
          <div class="portfolio-image"><img alt="Open Imagination" src="{{URL::asset('/img/pictures/6.png')}}"></div>
          
        </article>
        
        <!-- ITEM -->
        <article class="portfolio-item pf-branding-design pf-web-design">
          <div class="portfolio-image"><img alt="Open Imagination" src="{{URL::asset('/img/pictures/7.jpg')}}"></div>
          
        </article>
        
        <!-- ITEM -->
        <article class="portfolio-item pf-branding-design pf-web-design">
          <div class="portfolio-image"><img alt="Open Imagination" src="{{URL::asset('/img/pictures/8.jpg')}}"></div>
          
        </article>
      </div>
    </section>
    
    <!-- Why Choose Us -->
    <section class="why-choose padding-top-80 padding-bottom-60">
      <div class="container"> 
        
        <!-- Main Heading -->
        <div class="heading-block text-center">
          <h3>CONÓCENOS</h3>
          <div class="divider divider-short divider-center"><img class="i-div" src="{{URL::asset('img/logo/logo_dark.png')}}" alt="Divider Icon Image"/></div>
          <span>Familia Profit 4 Life</span> </div>
        <!-- Why Choose Us  ROW-->
        <div class="row">
          <div class="col-md-7 margin-top-50"> 
            
            <!-- Tittle -->
            <div class="heading-block margin-bottom-20"> 
              <h4>Acerca de mí</h4>
            </div>
            <p align="justify">Mi nombre es Emile Machado, tengo 23 años. Nací en Venezuela. Fuí estudiante de Ingeniería Mecánica en la Facultad de Ingeniería de la Universidad de los Andes, llegando hasta un séptimo semestre de la carrera. Actualmente estoy desarrollando mi visión y meta de ser un emprendedor.</p>
            <br>
            <p align="justify">Creo fuertemente que hay un gran potencial viviendo fuera del sistema de trabajar 8 horas o más diarias durante cinco o más días a la semana. Vivir un estilo de vida que te permita tener más tiempo libre para desarrollar las actividades que realmente te gustarían hacer, y no estar trabajando solo por un cheque que pague tus cuentas.</p>
            <div class="ultra-ser">
              <hr>
              </div>
          </div>
          
          <!-- Image -->
          <div class="col-md-5 text-right"> <img class="img-responsive" src="{{URL::asset('img/pictures/9.jpg')}}" alt="Why Choose Us Image" /> </div>
        </div>
       	<hr>	       	
        <!-- Services ROW -->
        <div class="row margin-top-60">
        <div class="heading-block text-center">
          <h3>NUESTROS PILARES</h3>
          <div class="divider divider-short divider-center"><img class="i-div" src="{{URL::asset('img/logo/logo_dark.png')}}" alt="Divider Icon Image"/></div>
           </div>
            </div>
            
          
          <!-- Services -->
          <div class="col-md-4">
            <div class="clearfix icon-box ib-style-2 i-large i-top padding-bottom-15 border-light">
              <div class="ib-icon"> <i class="fa fa-tencent-weibo"></i> </div>
              <div class="ib-info">
                <h4 class="h6">¿QUÉ?</h4>
                <p align="justify">La Academia tiene como objetivo ayudar y dar a conocer a muchas personas el mundo del Forex y guíarlos a través de él con una serie de video-tutoriales que les permitiran manejar este tipo de inversiones y oportunidades.</p>
              </div>
            </div>
          </div>
          
          <!-- Services -->
          <div class="col-md-4">
            <div class="clearfix icon-box ib-style-2 i-large i-top padding-bottom-15 border-light">
              <div class="ib-icon"> <i class="fa fa-sliders"></i> </div>
              <div class="ib-info">
                <h4 class="h6">¿COMO?</h4>
                <p align="justify">Contarás con un portal virtual donde podrás encontrar todo el material necesario para la formación individual como trader, que va desde textos, material de psicología de trading, gráficos,algunos archivos multimedia descargables y por último pero no menos importante, los Video-Tutoriales, pilares de la plataforma PROFIT 4 LIFE.</p>
              </div>
            </div>
          </div>
          
          <!-- Services -->
          <div class="col-md-4 margin-bottom-30">
            <div class="clearfix icon-box ib-style-2 i-large i-top padding-bottom-15 border-light">
              <div class="ib-icon"> <i class="fa fa-paper-plane-o"></i> </div>
              <div class="ib-info">
                <h4 class="h6">¿PARA QUÉ?</h4>
                <p align="justify">Basadas en las experiencias de nuestro CEO como trader, se decidió crear la Academia Profit 4 Life la cual contará con mentoria personalizada para que nuestros usuarios comiencen a ver rentabilidad en el mercado, estudiando por el tiempo necesario hasta finalmente desarrollar y encontrar su estilo de Trading.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    
      <!-- FACTS -->
    <section class="facts padding-top-90 padding-bottom-90 margin-bottom-80" style="background-image: url('img/contador/fondo.png')">
      <div class="container">
        <div class="row counter"> 
          <!-- Team Member -->
          <div class="col-md-4 margin-bottom-30">
            <div class="c-style-4 c-text-white"> <span class="number"> <span class="timer" data-speed="2000" data-refresh-interval="100" data-to="{{$cuenta_usuarios}}" data-from="0">{{$cuenta_usuarios}}</span> </span>
              <h5>Usuarios en la Plataforma</h5>
            </div>
          </div>
          
          <!-- Line Of Codes -->
          <div class="col-md-4 margin-bottom-30">
            <div class="counter c-style-4 c-text-white"> <span class="number"> <span class="timer" data-speed="2000" data-refresh-interval="100" data-to="{{$cuenta_usuarios_profit}}" data-from="0">{{$cuenta_usuarios_profit}}</span> </span>
              <h5>Usuarios Profit 4 Life</h5>
            </div>
          </div>
          
          <!-- Satisfied Client -->
          <div class="col-md-4 columns margin-bottom-30">
            <div class="counter c-style-4 c-text-white"> <span class="number"> <span class="timer" data-speed="2000" data-refresh-interval="100" data-to="{{$cuenta_usuarios_cripto}}" data-from="0">{{$cuenta_usuarios_cripto}}</span> </span>
              <h5>Usuarios Cripto 4 Life</h5>
            </div>
          </div>

        </div>
      </div>
    </section>
    
    <!-- TEAM MEMBER -->
    <section class="our-team">
      <div class="container"> 
        
        <!-- Main Heading -->
        <div class="heading-block text-center">
          <h3>POSIBILIDADES DEL FOREX</h3>
          <div class="divider divider-short divider-center"><img class="i-div" src="{{URL::asset('img/logo/logo_dark.png')}}" alt="Divider Icon Image"/></div>
           </div>
        
        <!-- Team Row -->
        <div class="row margin-bottom-20"> 
          
          <!-- Team Member -->
          <div class="col-md-4 margin-bottom-30">
            <div class="team style-1">
              <div class="team-image"> <a href="#."> <img alt="Our Team Thumbnail Image" src="{{URL::asset('img/pictures/10.jpg')}}"/> </a> </div>
              <div class="team-desc">
                <div class="team-title">
                  <h4><a href="#">DESCUBRE</a></h4>
                   <span>La gama de conocimientos necesarios para que puedas incursionar dentro del mercado de forex y ser tu mismo analista de mercado.</span> </div>
              </div>
            </div>
          </div>
          <!-- Team Member -->
          <div class="col-md-4 margin-bottom-30">
            <div class="team style-1">
              <div class="team-image"> <a href="#."> <img alt="Our Team Thumbnail Image" src="{{URL::asset('img/pictures/11.jpg')}}"/> </a> </div>
              <div class="team-desc">
                <div class="team-title">
                  <h4><a href="#">INVIERTE</a></h4>
                  <span>De manera eficiente para obtener excelentes resultados haciendo producir tu dinero “que trabaje para ti y no al contrario”.</span> </div>
              </div>
            </div>
          </div>
          <!-- Team Member -->
          <div class="col-md-4 margin-bottom-30">
            <div class="team style-1">
              <div class="team-image"> <a href="#."> <img alt="Our Team Thumbnail Image" src="{{URL::asset('img/pictures/12.jpg')}}"/> </a> </div>
              <div class="team-desc">
                <div class="team-title">
                  <h4><a href="#">CREA</a></h4>
                  <span>LA VIDA QUE ESTA DISPONIBLE PARA LOS QUE DAN EL PASO PARA SER RENTABLES EN UN MERCADO DE  MAS DE 3 TRILLONES DE DOLARES AL DIA.</span> </div>
              </div>
            </div>
          </div>
        </div>
        <!-- Team Member -->
           <br>	
	
	<hr>	       
	<br><br>
      </div>
   
    </section>


    <!-- Pricing Table -->
    <section class="pricing padding-bottom-100">
      <div class="container"> 
        
        <!-- Heading Blog -->
        <div class="heading-block text-center">
          <h3>Nuestros Precios</h3>
          <div class="divider divider-short divider-center"><img class="i-div" src="{{URL::asset('img/logo/logo_dark.png')}}" alt="Divider Icon Image"/></div>
          <span>Invierte en tu Futuro</span> </div>
        
        <!-- Pricing Table Row -->
        
        <div class="row"> 
         <div class="col-md-1 margin-bottom-30">
            
          </div>

          <!-- Basic Plan -->
          <div class="col-md-2 margin-bottom-30">
            <ul class="pricing-table style-1 hover-up hover-shadow">
              <li class="title">Profit 4 Life</li>
              <li class="bullet-item">Acceso a Cursos</li>
              <li class="bullet-item">Acceso a Grupos</i></li>
              <li class="bullet-item">Mentoría en Vivo</li>
              <li class="bullet-item">Discord Chat</li>
              <li class="bullet-item">Webinars </li>
              <li class="bullet-item">Soporte las 24 horas<i class="fa fa-check text-color-primary"></i></li>
              <li class="price"><span class="currency">$</span>{{$servicios[0]->precio_servicio}}<span class="period"></span></li>
              <li class="cta-button"><a class="btn dark"  href="{{route('home.agregar_carrito', Crypt::encrypt($servicios[0]->id))}}">Comprar</a></li>
            </ul>
          </div>
          
          <!-- Business Plan -->
          <div class="col-md-2 margin-bottom-30">
            <ul class="pricing-table style-1 hover-up hover-shadow">
              <li class="title">Cripto 4 Life</li>
              <li class="bullet-item">Acceso a Contenido</li>
              <li class="bullet-item">Acceso a Grupos</i></li>
              <li class="bullet-item">Mentoría en Vivo</li>
              <li class="bullet-item">Chats Privados </li>
              <li class="bullet-item">Seguimiento</li>
              <li class="bullet-item">Soporte las 24 horas<i class="fa fa-check text-color-primary"></i></li>
              <li class="price"><span class="currency">$</span>{{$servicios[1]->precio_servicio}}<span class="period"></span></li>
              <li class="cta-button"><a class="btn dark"  href="{{route('home.agregar_carrito', Crypt::encrypt($servicios[1]->id))}}">Comprar</a></li>
            </ul>
          </div>
          
          <!-- Premium Plan -->
          <div class="col-md-2 margin-bottom-30">
            <ul class="pricing-table style-1 hover-up hover-shadow">
              <li class="title">Profit Calls</li>
              <li class="bullet-item">Profit Calls</li>
              <li class="bullet-item">Mercados en Vivo</i></li>
              <li class="bullet-item">Resumen de Profits</li>
              <li class="bullet-item">Calculadora de Pips</li>
              <li class="bullet-item">Noticias y Análisis </li>
              <li class="bullet-item">Soporte las 24 horas<i class="fa fa-check text-color-primary"></i></li>
              <li class="price"><span class="currency">$</span>{{$servicios[2]->precio_servicio}}<span class="period"></span></li>
              <li class="cta-button"><a class="btn dark"  href="{{route('home.app')}}">Comprar</a></li>
            </ul>
          </div>
          
          <!-- Ultimate Plan -->
          <div class="col-md-2 margin-bottom-30">
            <ul class="pricing-table style-1 hover-up hover-shadow">
              <li class="title">Libro Profit</li>
              <li class="bullet-item"><br></li>
              <li class="bullet-item"><br></li>
              <li class="bullet-item">Acceso a Libro Digital</li>
              <li class="bullet-item">Soporte las 24 horas<i class="fa fa-check text-color-primary"></i></li>
              <li class="bullet-item"><br></li>
              <li class="bullet-item"><br></li>
              <li class="price"><span class="currency">$</span>{{$servicios[3]->precio_servicio}}<span class="period"></span></li>
              <li class="cta-button"><a class="btn dark"  href="{{route('home.agregar_carrito', Crypt::encrypt($servicios[3]->id))}}">Comprar</a></li>
            </ul>
          </div>

          <!-- Plus Plan -->
          <div class="col-md-2 margin-bottom-30">
            <ul class="pricing-table style-1 hover-up hover-shadow">
              <li class="title">Libro Cripto</li>
              <li class="bullet-item"><br></li>
              <li class="bullet-item"><br></li>
              <li class="bullet-item">Acceso a Libro Digital</li>
              <li class="bullet-item">Soporte las 24 horas<i class="fa fa-check text-color-primary"></i></li>
              <li class="bullet-item"><br></li>
              <li class="bullet-item"><br></li>
              <li class="price"><span class="currency">$</span>{{$servicios[4]->precio_servicio}}<span class="period"></span></li>
              <li class="cta-button"><a class="btn dark"  href="{{route('home.agregar_carrito', Crypt::encrypt($servicios[4]->id))}}">Comprar</a></li>
            </ul>
          </div>

          <div class="col-md-1 margin-bottom-30">
            
          </div>

        </div>
      </div>
    </section>
    
    <section class="contact">
      
      
      <!-- Contact  -->
      <div class="padding-top-80 padding-bottom-80 bg-parallax" style="background-image: url('img/contador/fond.png')">
        <div class="container">
          <div class="row"> 
            
           
            
            <!-- Form  -->
            <div class="col-md-12 ">
              <div class="contact-form"> 
                <div class="heading-bloc text-center">
              <div class="divider divider-short divider-center"><img class="i-div" src="{{URL::asset('img/logo/logo_ligth.png')}}" alt="Divider Icon Image"/></div>
              <h3 >CONTÁCTANOS</h3>
              <br>

              
               </div>
                
                <!-- FORM -->
                    {!! Form::open(array('route' => 'contact.store','method' => 'POST', 'class' => 'contact-form')) !!}
                    {{ csrf_field() }}

                  <ul class="row">
                    <li class="col-sm-4">
                      <label>*<span> NOMBRE</span>
                        {!! Form::text('nombre', null, array('placeholder' => 'Nombre: ','class' => 'form-control', 'required' => 'required')) !!}
                      </label>
                    </li>
                    <li class="col-sm-4">
                      <label>* <span>EMAIL</span>
                        {!! Form::email('email', null, array('placeholder' => 'E-Mail: ','class' => 'form-control', 'required' => 'required')) !!}
                      </label>
                    </li>
                    <li class="col-sm-4">
                      <label>* <span>ASUNTO</span>
                        {!! Form::text('subject', null, array('placeholder' => 'Asunto: ','class' => 'form-control', 'required' => 'required')) !!}
                      </label>
                    </li>
                    <li class="col-sm-12 margin-top-20">
                      <label> <span>MENSAJE</span>
                        {!! Form::textarea('message', null, array('placeholder' => 'Mensaje: ','class' => 'form-control', 'required' => 'required')) !!}
                      </label>
                    </li>
                    <li class="col-sm-12">
                      <button type="submit" value="submit">ENVIAR</button>
                    </li>
                  </ul>
                    {!! Form::close() !!}
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    
    <!-- Parthner -->
    
        <!-- Why Choose Us -->
    <section class="why-choose padding-top-60 padding-bottom-20">
      <div class="container"> 
        
        <!-- Main Heading -->
        <div class="heading-block text-center">
          
          <div class="divider divider-short divider-center"><img class="i-div" src="{{URL::asset('img/logo/logo_dark.png')}}" alt="Divider Icon Image"/>
          <span>Familia Profit 4 Life</span> </div>
          
        <!-- Why Choose Us  ROW-->
        
            
          
      </div>
    </section>


  </div>
  <!-- End Content --> 


  
@include('layouts.footer')
</body>
</html>