@extends('adminlte::layouts.app')

@section('htmlheader_title')
	Usuarios
@endsection
@section('contentheader_title') {{-- TITULO DEL CONTENIDO DE LA VISTA --}}

@stop


@section('main-content')

<div class="container-fluid">
    <div class="row">
        <div class="col-md-3 col-md-offset-1">
            <div class="box box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Acciones</h3>
                </div>
                <div class="box-body">
 	                <center>
					<a class="btn btn-primary btn-block " href="{{route('usuarios.create')}}">Crear Usuario</a>	
					<a class="btn btn-primary btn-block " href="{{route('users.export')}}">Exportar a Excel</a>						
					</center>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
	    	@if ($message = Session::get('success'))
				<div class="alert alert-success">
					<p>{{ $message }}</p>
				</div>
			@endif
          <div class="box">
			<div class="box-body table-responsive-dt">
	            <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    	<tr class="header">
							<th>Nombre del Usuario</th>
							<th>Apellido del Usuario</th>
							<th>Pais del Usuario</th>
							<th>Teléfono del Usuario</th>
							<th>Correo del Usuario</th>
							<th>Acción</th>
						</tr>
                    </thead>
                    <tbody>
						@foreach ($usuarios as $usuario)
						<tr>
							<td>{{ $usuario->nombre }}</td>
							<td>{{ $usuario->apellido }}</td>
							<td>{{ $usuario->pais }}</td>
							<td> 
								{{$usuario->phone}}
							</td>
							<td> 
								{{$usuario->email}}
							</td>
							<td><center>
                                    @if($usuario->habilitado == '0')
                                        {!!link_to_route('usuarios.modificaEstado', $title = 'Activar',
                                        $parameters = ['id' => $usuario->id],
                                        $attributes = ['class'=>'btn btn-success','style'=>'display:inline']);!!}
                                    @endif
                                    @if($usuario->habilitado == '1')
	                                    {!!link_to_route('usuarios.modificaEstado', $title = 'Desactivar',
	                                    $parameters = ['id' => $usuario->id],
	                                    $attributes = ['class'=>'btn btn-danger','style'=>'display:inline']);!!}
                                    @endif
								<br>
								<br>
								<a class="btn btn-primary" href="{{ route('usuarios.edit',$usuario->id) }}">Editar</a>
								<br>
								<br>
								{!! Form::open(['method' => 'DELETE','route' => ['usuarios.destroy', $usuario->id],'style'=>'display:inline']) !!}
					            {!! Form::submit('Eliminar', ['class' => 'btn btn-danger']) !!}
					        	{!! Form::close() !!}
							</center></td>
						</tr>
						@endforeach 
                       
                    </tbody>
                </table>
            </div>
		  </div>
        </div>
    </div>
@endsection
