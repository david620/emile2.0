@extends('adminlte::layouts.app')

@section('htmlheader_title')
	Crear Usuario
@endsection
@section('contentheader_title') {{-- TITULO DEL CONTENIDO DE LA VISTA --}}

@stop

@section('main-content')

	@if (count($errors) > 0)
		<div class="alert alert-danger">
			<h4><i class="icon fa fa-ban"></i> ¡Uy!, ha ocurrido un problema</h4>
			<ul>
				@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
	@endif

          <div class="row">
              <div class="col-md-3 col-md-offset-1">
                  <div class="box box-solid">
                      <div class="box-header with-border">
                          <h3 class="box-title">Acciones</h3>
                      </div>
                      <div class="box-body">
                          <center>
                            <a class="btn btn-primary btn-block " href="{{ route('usuarios.index')}}">Regresar</a>   
                          </center>
                      </div>
                  </div>
              </div>
          </div>


	{!! Form::open(array('route' => 'usuarios.store','method'=>'POST')) !!}
    {{ csrf_field() }}
          <div class="row">
        <div class="col-md-9 col-md-offset-1">
         <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Crear Usuario</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form">
              <div class="box-body">
                <div class="form-group">
                  <label for="nombre">Nombre del Usuario:</label>
		            {!! Form::text('nombre', null, array('placeholder' => 'Nombre del Usuario: ','class' => 'form-control', 'required' => 'required')) !!}
                </div>
                <div class="form-group">
                  <label for="apellido">Apellido del Usuario:</label>
                {!! Form::text('apellido', null, array('placeholder' => 'Apellido del Usuario: ','class' => 'form-control', 'required' => 'required')) !!}
                </div>
                <div class="form-group">
                  <label for="phone">Teléfono del Usuario:</label>
                  {!! Form::text('phone', null, array('placeholder' => 'Teléfono del Usuario: ','class' => 'form-control', 'required' => 'required')) !!}
                </div>
                <div class="form-group">
                  <label for="email">Email del Usuario:</label>
                  {!! Form::text('email', null, array('placeholder' => 'Email del Usuario: ','class' => 'form-control', 'required' => 'required')) !!}
                </div>
                <div class="form-group">
                  <label for="password">Contraseña del Usuario:</label>
                  {!! Form::password('password', null, array('placeholder' => 'Contraseña del Usuario: ','class' => 'form-control', 'required' => 'required')) !!}
                </div>
                <div class="form-group">
                  <label for="services">Servicios del Usuario:</label>
                    {!! Form::select('services[]', $servicios, '', array('class' => 'form-control', 'required' => 'required', 'multiple' => 'multiple')) !!}
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Crear</button>
              </div>
            </form>
          </div>
        </div>
        </div>
	{!! Form::close() !!}
@endsection
