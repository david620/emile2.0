<!-- REQUIRED JS SCRIPTS -->

<!-- JQuery and bootstrap are required by Laravel 5.3 in resources/assets/js/bootstrap.js-->
<!-- Laravel App -->
<script src="{{ url (mix('/js/app.js')) }}" type="text/javascript"></script>
{{--  datatables  --}}
<script src="{{ asset('plugins/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
 
<script>
      $(".alert").delay(6000).slideUp(200, function() {
        $(this).alert('close');
      });


  $(function () {
    $('#example1').DataTable({
          "order": [],
          "language": {
            "lengthMenu": "Ver _MENU_ registros por página",
            "zeroRecords": "No se encontraron resultados",
            "info": "Ver _PAGE_ de _PAGES_",
            "infoEmpty": "Sin Registros",
            "infoFiltered": "(filtered from _MAX_ total records)",
            "paginate": {
                  "first":      "Primero",
                  "last":       "Último",
                  "next":       "Siguiente",
                  "previous":   "Atras"
            },
            "search":         "Buscar:"
        }
    })
    $('#example2').DataTable({
          "order": [],
          "language": {
            "lengthMenu": "Ver _MENU_ registros por página",
            "zeroRecords": "No se encontraron resultados",
            "info": "Ver _PAGE_ de _PAGES_",
            "infoEmpty": "Sin Registros",
            "infoFiltered": "(filtered from _MAX_ total records)",
            "paginate": {
                  "first":      "Primero",
                  "last":       "Último",
                  "next":       "Siguiente",
                  "previous":   "Atras"
            },
            "search":         "Buscar:"
        }
    })
  })

</script>
@yield('js')