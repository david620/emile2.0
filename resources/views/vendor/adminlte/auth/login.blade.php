<!DOCTYPE html>
<html lang="en">
<head>
    <title>Login - Profit 4 Life</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->  
    <link rel="icon" type="image/png" href="{{URL::asset('assets_login/img/icons/fav.png')}}"/>
<!--===============================================================================================-->

    <link rel="stylesheet" type="text/css" href="{{URL::asset('assets_login/css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('assets_login/css/material-design-iconic-font.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('assets_login/css/util.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('assets_login/css/main.css')}}">
<!--===============================================================================================-->
    
</head>
<body>
<div class="limiter">
    <div  class="container-login100" style="background-image: url({{URL::asset('/img/sliders/slider1.png')}});">
                <div class="wrap-login100">
                    <form class="login100-form validate-form"  method="POST" action="{{ route('login') }}">
                        @csrf
                        <span class="login100-form-logo">
                            <a href="{{route('home.index')}}"><img src="{{URL::asset('assets_login/img/logo_ligth.png')}}" alt=""></a>
                        </span>
                        <span class="login100-form-title p-b-34 p-t-27">
                            Iniciar Sesión 
                        </span>
                        <br>
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            Hubo un problema al Iniciar Sesión<br><br>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
<br><br>
                @if ($message = Session::get('success'))
                        <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-check"></i> Uy!</h4>
                        {{ $message }}
                    </div>
                @endif
                        <div class="wrap-input100 validate-input" data-validate = "Enter username">
                            <input class="input100" placeholder="Usuario" id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>
                            <span class="focus-input100" data-placeholder="&#xf207;"></span>

                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="wrap-input100 validate-input" data-validate="Enter password">
                            <input class="input100" placeholder="Contraseña" id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                            <span class="focus-input100" data-placeholder="&#xf191;"></span>

                            @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                                            
                        <div class="contact100-form-checkbox">
                            <input class="input-checkbox100" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                            <label class="label-checkbox100" for="remember">
                                {{ __('Recordarme') }}
                            </label>
                        </div>

                        <div class="container-login100-form-btn">
                            <button type="submit" class="login100-form-btn">
                                {{ __('Entrar') }}
                            </button>                           
                        </div>
                        <br>
                        <div class="text-center p-t-20 p-b-30">
                            @if (Route::has('password.request'))
                                <a class="txt1" href="{{ route('password.request') }}">
                                    {{ __('¿Olvidaste Contraseña?') }}
                                </a>
                            @endif
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="dropDownSelect1"></div>
<!--===============================================================================================-->
    <script src="{{URL::asset('assets_login/js/jquery-3.2.1.min.js')}}"></script>
<!--===============================================================================================-->
    <script src="{{URL::asset('assets_login/js/popper.js')}}"></script>
    <script src="{{URL::asset('assets_login/js/bootstrap.min.js')}}"></script>
<!--===============================================================================================-->
    <script src="{{URL::asset('assets_login/js/main.js')}}"></script>

</body>
</html>