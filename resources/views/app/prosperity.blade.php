<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="keywords" content="HTML5 Template" />
    <meta name="description" content="Appino - Responsive App Landing Page" />
    <meta name="author" content="iqonicthemes.in" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <title>Technical Prosperity - APP</title>  
    <!-- Favicon -->
    <link rel="shortcut icon" href="{{URL::asset('assets_apps/img/logo_prosperity.png')}}" />
    
    <!-- Google Fonts -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700&amp;Raleway:300,400,500,600,700,800,900">
    
    <!-- Bootstrap -->
    <link rel="stylesheet" href="{{URL::asset('assets_apps/css/bootstrap.min.css')}}">
    
    <!-- owl-carousel -->
    <link rel="stylesheet" type="text/css" href="{{URL::asset('assets_apps/css/owl.carousel.css')}}" />
    
    <!-- Font Awesome -->
    <link rel="stylesheet" type="text/css" href="{{URL::asset('assets_apps/css/font-awesome.css')}}" />
    
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{URL::asset('assets_apps/css/ionicons.min.css')}}">
    
    <!-- Style -->
    <link rel="stylesheet" href="{{URL::asset('assets_apps/css/style_red.css')}}">

    <!-- Responsive -->
    <link rel="stylesheet" href="{{URL::asset('assets_apps/css/responsive.css')}}">
</head>

<body>
    
    <!-- loading -->
    <div id="loading" class="green-bg">
        <div id="loading-center">
            <div class="boxLoading"></div>
        </div>
    </div>
    <!-- loading End -->

    <!-- Header -->
    <header id="header-wrap" data-spy="affix" data-offset-top="55">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <nav class="navbar navbar-default">
                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                            
                            <a class="navbar-brand" href="javascript:void(0)">
                                <img src="{{URL::asset('assets_apps/img/logo_prosperity.png')}}" alt="logo">
                            </a>
                        </div>
                        <!-- Collect the nav links, forms, and other content for toggling -->
                        
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <!-- Header End -->

    <!-- Banner -->
    <section id="iq-home" class="banner-03 iq-bg iq-bg-fixed iq-box-shadow iq-over-black-80" style=" background: url(assets_apps/img/fondos.png);">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h1 class="iq-font-white iq-tw-8 text-center">TECHNICAL PROSPERITY</h1>
                    <div class="iq-mobile-app text-center">
                        <div class="iq-mobile-box">
                            <img class="iq-mobile-img" src="{{URL::asset('assets_apps/img/phone14.png')}}"" alt="#">
                            <span data-depth="0.8" class="layer iq-mobile-icon icon-01" data-bottom="transform:translateX(50px)" data-top="transform:translateX(-100px);">
                                <img src="{{URL::asset('assets_apps/img/04.png')}}" alt="#">
                            </span>
                            <span data-depth="0.8" class="layer iq-mobile-icon icon-02" data-bottom="transform:translateX(100px)" data-top="transform:translateX(0px);">
                                <img src="{{URL::asset('assets_apps/img/05.png')}}" alt="#">
                            </span>
                            <span data-depth="0.8" class="layer iq-mobile-icon icon-03" data-bottom="transform:translateX(-50px)" data-top="transform:translateX(40px);">
                                <img src="{{URL::asset('assets_apps/img/06.png')}}" alt="#">
                            </span>
                            <span data-depth="0.8" class="layer iq-mobile-icon icon-04" data-bottom="transform:translateX(-30px)" data-top="transform:translateX(0px);">
                                <img src="{{URL::asset('assets_apps/img/07.png')}}" alt="#">
                            </span>
                            <span data-depth="0.8" class="layer iq-mobile-icon icon-05" data-bottom="transform:translateX(0px)" data-top="transform:translateX(-50px);">
                                <img src="{{URL::asset('assets_apps/img/08.png')}}" alt="#">
                            </span>
                        </div>
                    </div>
                    <div class="link">
                        <h5 class="iq-font-white" data-animation="animated fadeInLeft">Download Here</h5>
                        <ul class="list-inline" data-animation="animated fadeInUp">
                            <li><a href="javascript:void(0)"><i class="ion-social-android"></i></a></li>
                            <li><a href="javascript:void(0)"><i class="ion-social-apple"></i></a></li>
                        </ul>
                        <p class="iq-font-white iq-mt-10">We are pround to present our new App <br> Download for the system of your Preference.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Banner End -->


<div class="main-content">
    <!-- Feature -->
    <section id="about-us" class="overview-block-ptb iq-mt-50">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center">
                    <div class="heading-title iq-mb-20">
                        <h2 class="title iq-tw-6">About Our App</h2>
                        <div class="divider"></div>    
                    </div>
                <p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
                <div class="btn-group iq-mt-40" role="group" aria-label="">
                  <a class="button iq-mr-15" href="# "> <i class="ion-social-apple"></i> App Store</a>
                  <a class="button iq-mr-15" href="# "> <i class="ion-social-android"></i> Play Store</a>
                </div>
            </div>
        </div>
        </div>
    </section>
    <!-- Feature END -->

<hr>

    <!-- Special Features -->
    <section id="features" class="overview-block-ptb iq-amazing-tab white-bg">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="heading-title">
                        <h2 class="title iq-tw-6">Prosperity Benefits</h2>
                        <div class="divider"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 col-md-4 col-lg-4">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active wow fadeInLeft" data-wow-duration="1s">
                            <a class="round-right" href="#design" aria-controls="design" role="tab" data-toggle="tab">
                                <div class="iq-fancy-box-01 text-right">
                                    <i aria-hidden="true" class="ion-ios-checkmark-outline"></i>
                                    <h4 class="iq-tw-6">TECHNICAL PROSPERITY</h4>
                                    <div class="fancy-content-01">
                                        <p>Be part of the evolution with Prosperity.</p>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li role="presentation" class="wow fadeInLeft" data-wow-duration="1s" >
                            <a class="round-right" href="#resolution" aria-controls="resolution" role="tab" data-toggle="tab">
                                <div class="iq-fancy-box-01 text-right">
                                    <i aria-hidden="true" class="ion-ios-checkmark-outline"></i>
                                    <h4 class="iq-tw-6">LIVE MARKETS</h4>
                                    <div class="fancy-content-01">
                                        <p>Stay in line with Live Markets.</p>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li role="presentation" class="wow fadeInLeft" data-wow-duration="1s">
                            <a class="round-right" href="#ready" aria-controls="ready" role="tab" data-toggle="tab">
                                <div class="iq-fancy-box-01 text-right">
                                    <i aria-hidden="true" class="ion-ios-checkmark-outline"></i>
                                    <h4 class="iq-tw-6">PROFITS SUMMARY</h4>
                                    <div class="fancy-content-01">
                                        <p>Keep a history of Profits to complement the study and improve your trading.Keep a history of Profits to complement the study and improve your trading.</p>
                                    </div>
                                </div>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-4 text-center hidden-sm hidden-xs">
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="design"><img src="{{URL::asset('assets_apps/img/app.png')}}" class="img-responsive center-block" alt="#"></div>
                        
                    </div>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-4">
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="wow fadeInRight" data-wow-duration="1s">
                            <a href="#fertures" aria-controls="fertures" role="tab" data-toggle="tab">
                                <div class="iq-fancy-box-01">
                                    <i aria-hidden="true" class="ion-ios-checkmark-outline"></i>
                                    <h4 class="iq-tw-6">PIPS CALCULATOR</h4>
                                    <div class="fancy-content-01">
                                        <p>Use our Pips Calculator to complement your trading.</p>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li role="presentation" class="wow fadeInRight" data-wow-duration="1s">
                            <a href="#face" aria-controls="face" role="tab" data-toggle="tab">
                                <div class="iq-fancy-box-01">
                                    <i aria-hidden="true" class="ion-ios-checkmark-outline"></i>
                                    <h4 class="iq-tw-6">PRICE ALARMS</h4>
                                    <div class="fancy-content-01">
                                        <p>Set your alarms to know the latest prices.</p>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li role="presentation" class="wow fadeInRight" data-wow-duration="1s">
                            <a href="#codes" aria-controls="codes" role="tab" data-toggle="tab">
                                <div class="iq-fancy-box-01">
                                    <i aria-hidden="true" class="ion-ios-checkmark-outline"></i>
                                    <h4 class="iq-tw-6">NEWS AND TECHNICAL ANALYSIS</h4>
                                    <div class="fancy-content-01">
                                        <p>Keep up to date with the most relevant news from Prosperity.</p>
                                    </div>
                                </div>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <!-- Special Features END -->

    <section class="overview-block-ptb iq-bg iq-bg-fixed iq-over-black-80" style="background: url(assets_apps/img/fondos.png);" >
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="heading-title iq-mb-40">
                        <h2 class="title iq-tw-6 iq-font-white">Download App</h2>
                        <div class="divider white"></div>
                        <p class="iq-font-white">Join the evolution with technical prosperity and bring your trading to the next level.</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 text-center">
                    <a class="button button-icon iq-mr-15" href="# "> <i class="ion-social-apple"></i> App Store</a>
                    <a class="button button-icon iq-mr-15 re4-mt-20" href="# "> <i class="ion-social-android"></i> Google Play</a>      
                </div>
            </div>
        </div>
    </section>

    <!-- App Screenshots -->
    <section id="screenshots" class="iq-app iq-bg iq-bg-fixed iq-font-white" style="background: url(assets_apps/img/fondos.png);>
        <div class="container-fluid">
            <div class="row row-eq-height">
                <div class="col-md-6 text-left iq-ptb-80 green-bg">
                <div class="iq-app-info">
                    <h2 class="heading-left iq-font-white white iq-tw-6 ">App Screenshots</h2>
                    <div class="lead iq-tw-6 iq-mb-20">Appino is here with its best screenshot, from this photo gallery you can get idea about this application.</div>
                    <h4 class="iq-mt-50 iq-font-white iq-tw-6 iq-mb-15">Awesome Design</h4>
                    <p class="">Simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, Lorem Ipsum is simply dummy text of the printing and typesetting indus</p>
                </div>
                </div>
                <div class="col-md-6 iq-app-screen iq-ptb-80">
                    <div class="home-screen-slide">
                        <div class="owl-carousel popup-gallery" data-autoplay="true" data-loop="true" data-nav="false" data-dots="false" data-items="3" data-items-laptop="2" data-items-tab="2" data-items-mobile="1" data-items-mobile-sm="1" data-margin="15">
                            <div class="item"><a href="#" class="popup-img"><img class="img-responsive" src="{{URL::asset('assets_apps/img/app.png')}}" alt="#"></a></div>
                            <div class="item"><a href="#" class="popup-img"><img class="img-responsive" src="{{URL::asset('assets_apps/img/app1.png')}}" alt="#"></a></div>
                            <div class="item"><a href="#" class="popup-img"><img class="img-responsive" src="{{URL::asset('assets_apps/img/app2.png')}}" alt="#"></a></div>
                            <div class="item"><a href="#" class="popup-img"><img class="img-responsive" src="{{URL::asset('assets_apps/img/app3.png')}}" alt="#"></a></div>                           
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>
    <!-- App Screenshots END -->

    <section id="pricing" class="overview-block-ptb grey-bg iq-price-table">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="heading-title">
                        <h2 class="title iq-tw-6">Our Benefits</h2>
                        <div class="divider"></div>
                        <p>We're here with best pricing offers. Get start your future with our awesome pricing plan. Here is affordable prices available. Go with your choice and enjoy the services.</p>
                    </div>
                    <div>
                    <div class="iq-pricing text-center">
                        <div class="price-title green-bg ">
                            <h1 class="iq-font-white iq-tw-7"><small>$</small>{{$servicio[5]->precio_servicio}}<small></small></h1>
                        </div>
                        <ul>
                            <li>Live Markets</li>
                            <li>Profits Summary</li>
                            <li>Pips Calculator</li>
                            <li>Price Alarms</li>
                            <li>News and technical analysis</li>
                        </ul>
                    </div>
                </div>
                </div>
            </div>            
        </div>
    </section>

    <section id="features" class="overview-block-ptb iq-amazing-tab white-bg">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="heading-title">
                        <h2 class="title iq-tw-6">If You Are A New User, Join The Evolution With Prosperity</h2>
                        <div class="divider"></div>
                    </div>
                    {!! Form::open(array('route' => 'prosperity_store','method'=>'POST', 'enctype' => 'multipart/form-data')) !!}
                    {{ csrf_field() }}
                            <div class="contact-form">
                                <div class="col-sm-6">
                                    <div class="section-field">
                                      <input id="name" type="text" placeholder="Name*" class="form-control" name="name" require autofocus required="required">
                                      @if ($errors->has('name'))
                                        <span class="help-block">
                                          <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                      @endif  
                                    </div>
                                    <div class="section-field">
                                      <input id="email" type="email" placeholder="E-mail*" class="form-control" name="email" require autofocus required="required">
                                      @if ($errors->has('email'))
                                        <span class="help-block">
                                          <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                      @endif
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="section-field">
                                      <input id="password" type="password" placeholder="password*" class="form-control" name="password" require autofocus required="required">
                                      @if ($errors->has('password'))
                                        <span class="help-block">
                                          <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                      @endif 
                                    </div>
                                    <div class="section-field">
                                      <input id="password_confirmation" type="password" placeholder="password confirmation*" class="form-control" name="password_confirmation" require autofocus required="required">
                                      @if ($errors->has('password_confirmation'))
                                        <span class="help-block">
                                          <strong>{{ $errors->first('password_confirmation') }}</strong>
                                        </span>
                                      @endif
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                     <button id="submit" name="submit" type="submit" class="button"><span> Join Us </span> <i class="fa fa-paper-plane"></i></button>
                                </div>
                            </div>
                          {!! Form::close() !!}
                </div>
            </div>
        </div>
    </section>
</div>


<div class="footer">
    <!-- Subscribe Our Newsletter -->
    <div class="iq-ptb-60 green-bg iq-subscribe">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center">
                    {!! Form::open(array('route' => 'prosperity_store','method'=>'POST', 'enctype' => 'multipart/form-data', 'class' => 'form-inline')) !!}
                    {{ csrf_field() }}
                    	<div class="heading-titles">
                        <h2 class="title iq-tw-6 iq-font-white">Join The Evolution With Prosperity</h2>
                        <div class="divider white"></div>
                    	</div>
                        <div class="form-group">                      
                                      <input id="email" type="email" placeholder="E-mail*" class="form-control" name="email" require autofocus required="required">
                                      @if ($errors->has('email'))
                                        <span class="help-block">
                                          <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                      @endif
                                      <input id="password" type="password" placeholder="password*" class="form-control" name="password" require autofocus required="required">
                                      @if ($errors->has('password'))
                                        <span class="help-block">
                                          <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                      @endif
                        </div>
                                <div class="col-sm-12">
                                     <button id="submit" name="submit" type="submit" class="button"><span> Join Us </span> <i class="fa fa-paper-plane"></i></button>
                                </div>
                  {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <!-- Footer -->
    <footer class="iq-footer-03 white-bg iq-ptb-20">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center">
                    <div class="footer-copyright iq-pt-10">Profit4Lifevzla © Copyright is reserved. Developed by <a target="_blank" href="https://iqonicthemes.com/">Átomo Tecnologías Virtuales</a>.</div>
                </div>
            </div>
        </div>
    </footer>
    <!--  END -->
</div>
    
    <!-- back-to-top -->
    <div id="back-to-top">
        <a class="top" id="top" href="#top"> <i class="ion-ios-upload-outline"></i> </a>
    </div>

    <!-- jQuery -->
    <script src="{{URL::asset('assets_apps/js/jquery.min.js')}}"></script>

    <!-- owl-carousel -->
    <script src="{{URL::asset('assets_apps/js/owl-carousel/owl.carousel.min.js')}}"></script>

    <!-- Counter -->
    <script src="{{URL::asset('assets_apps/js/counter/jquery.countTo.js')}}"></script>

    <!-- Jquery Appear -->
    <script src="{{URL::asset('assets_apps/js/jquery.appear.js')}}"></script>

    <!-- Magnific Popup --> 
    <script src="{{URL::asset('assets_apps/js/magnific-popup/jquery.magnific-popup.min.js')}}"></script>

    <!-- Retina -->
    <script src="{{URL::asset('assets_apps/js/retina.min.js')}}"></script>

    <!-- wow -->
    <script src="{{URL::asset('assets_apps/js/wow.min.js')}}"></script>

    <!-- Skrollr --> 
    <script src="{{URL::asset('assets_apps/js/skrollr.min.js')}}"></script>

    <!-- Countdown -->
    <script src="{{URL::asset('assets_apps/js/jquery.countdown.min.js')}}"></script>

    <!-- bootstrap -->
    <script src="{{URL::asset('assets_apps/js/bootstrap.min.js')}}"></script>

    <!-- Custom -->
    <script src="{{URL::asset('assets_apps/js/custom.js')}}"></script>
</body>
</html>

