<!doctype html>
<!--[if lt IE 7 ]> <html lang="en" class="ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<head>
<meta name="viewport" content="width = 1050, user-scalable = yes" />
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script type="text/javascript" src="{{ asset('extras/modernizr.2.5.3.min.js') }}"></script>
<script src="{{ asset('js/jquery.mlens-1.7.min.js') }}"></script>
<style>
    body{
    background-color: transparent;
    background-image: url('{{asset('img/image10.png')}}');
    background-repeat: no-repeat;
    background-position: center top;
    background-attachment: fixed;
    backgroudn-size: contain;
    }

</style>
</head>
<body>
    <?php
        $id = Crypt::encrypt($flipbook[0]->id);
        $id2 = Crypt::encrypt($flipbook[1]->id);
        $cadena = substr($retornar, 20);
        $cadena2 = substr($retornar2, 21);

    ?>

<div class="zoom-viewport">
  <div class="flipbook-viewport">

	 <div class="container">
		  <div id="flipbook" class="flipbook">
    		@foreach($content as $page)
		        $cadena = substr($page, 17);
  	   		<div style="background-image:url({{ "images/profit/$cadena" }})"></div>
         @endforeach
  		</div>
  	</div>

  </div>
</div>

<script type="text/javascript">







function loadApp() {

	// Create the flipbook


	$('.flipbook').turn({
			// Width

			width:922,

			// Height

			height:600,

			// Elevation

			elevation: 50,

			// Enable gradients

			gradients: true,

			// Auto center this flipbook

			autoCenter: true

	});


}

// Load the HTML4 version if there's not CSS transform

yepnope({
	test : Modernizr.csstransforms,
	yep: ['{{ asset('lib/turn.js') }}'],
	nope: ['{{ asset('lib/turn.html4.min.js') }}'],
	both: ['{{ asset('lib/zoom.min.js') }}', '{{ asset('css/basic.css') }}'],
	complete: loadApp
});



</script>


</body>
</html>