<!doctype html>
<html class="no-js" lang="es">
@include('layouts.layouts_user.head')
<body>

<!-- Page Wrapper -->
<div id="wrap"> 
  
  <!-- Header -->
@include('layouts.layouts_user.header')
  <!-- End Header --> 
  
  <!--======= HOME MAIN SLIDER =========-->
  <section class="home-slider">
    <div class="tp-banner-container">
      <div class="tp-banner">
        <ul>
          <!-- SLIDE  -->
          <li data-transition="random" data-slotamount="7" data-masterspeed="300"  data-saveperformance="off" > 
            <!-- MAIN IMAGE --> 
            <img src="{{URL::asset('img/contador/fondo7.png')}}"  alt="slider"  data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat"> 
            <!-- LAYERS --> 
            <div class="sub-banner">
              <div class="position-center-center">
                <img src="{{URL::asset('img/logo/logo_ligth.png')}}" alt="" width="200px" height="200px" />
                <h2>BIENVENIDO A PROFIT 4 LIFE</h2>
                
              </div>
            </div>
            
            <!-- LAYER NR. 1 -->
            <div class="tp-caption biglargetext sfr tp-resizeme" 
                data-x="center" data-hoffset="0" 
                data-y="center" data-voffset="-90" 
                data-speed="500" 
                data-start="1000" 
                data-easing="Power3.easeInOut" 
                data-splitin="chars" 
                data-splitout="none" 
                data-elementdelay="0.06" 
                data-endelementdelay="0.1" 
                data-endspeed="300"
                style="z-index: 5; max-width: auto; max-height: auto; white-space: nowrap;"> </div>
            
            <!-- LAYER NR. 2 -->
            <div class="tp-caption verybiglargetext sfr tp-resizeme" 
                data-x="center" data-hoffset="0" 
                data-y="center" data-voffset="40" 
                data-speed="500" 
                data-start="1500" 
                data-easing="Power3.easeInOut" 
                data-splitin="chars" 
                data-splitout="none" 
                data-elementdelay="0.07" 
                data-endelementdelay="0.1" 
                data-endspeed="300" 
                style="z-index: 6; max-width: auto; max-height: auto; white-space: nowrap;"> </div>
            
            <!-- LAYER NR. 3 -->
            <div class="tp-caption font-crimson font-italic sfb tp-resizeme" 
                data-x="center" data-hoffset="0" 
                data-y="center" data-voffset="140" 
                data-speed="500" 
                data-start="1900" 
                data-easing="Power3.easeInOut" 
                data-splitin="none" 
                data-splitout="none" 
                data-elementdelay="0.1" 
                data-endelementdelay="0.1" 
                data-endspeed="300" 
                style="z-index: 7; font-size:18px; color:#fff; max-width: auto; max-height: auto; white-space: nowrap;"> </div>
            
            <!-- LAYER NR. 4 -->
            <div class="tp-caption lfb tp-scrollbelowslider" 
                data-x="center" data-hoffset="0" 
                data-y="bottom" data-voffset="-50" 
                data-speed="500" 
                data-start="2400" 
                data-easing="Power3.easeInOut" 
                data-elementdelay="0.1" 
                data-endelementdelay="0.1" 
                data-endspeed="300" 
                data-scrolloffset="0"
                style="z-index: 8;"> </div>
          </li>
          
        </ul>
      </div>
    </div>
  </section>
  
  <!-- Content -->
  <div id="content"> 
    <div class="tradingview-widget-container">
  <div class="tradingview-widget-container__widget"></div>
  <script type="text/javascript" src="https://s3.tradingview.com/external-embedding/embed-widget-ticker-tape.js" async>
  {
  "symbols": [
    {
      "title": "S&P 500",
      "proName": "INDEX:SPX"
    },
    {
      "title": "Shangai Composite",
      "proName": "INDEX:XLY0"
    },
    {
      "title": "EUR/USD",
      "proName": "FX_IDC:EURUSD"
    },
    {
      "title": "BTC/USD",
      "proName": "BITFINEX:BTCUSD"
    },
    {
      "title": "ETH/USD",
      "proName": "BITFINEX:ETHUSD"
    }
  ],
  "theme": "light",
  "isTransparent": false,
  "displayMode": "adaptive",
  "locale": "es"
}
  </script>
</div>

    <!-- Why Choose Us -->
    <section class="why-choose padding-top-20 ">
      <div class="container"> 
        <!-- Main Heading -->
        <div class="heading-block text-center ">
          <h3>PERFIL</h3>
          <div class="divider divider-short divider-center"><img class="i-div" src="{{URL::asset('img/logo/logo_dark.png')}}" alt="Divider Icon Image"/>
          <span>Familia Profit 4 Life</span></div>
        </div>
      </div>
    </section>
  <!-- Services Intro -->
   <section class="padding-top-20">
      <div class="container">
        <div class="row">

          <div class="col-md-6">
            <div class="col-md-10">
            <div class="icon-box ib-style-1 ib-circle ib-bordered ib-bordered-light ib-small">
              <div class="ib-icon"> <i class="fa fa-user"></i> </div>
              <div class="ib-info">
                <h4 class="h4">{{Auth::user()->nombre}} {{Auth::user()->apellido}}</h4>
              </div>
            </div>
            <br>
            <div class="icon-box ib-style-1 ib-circle ib-bordered ib-bordered-light ib-small">
              <div class="ib-icon"> <i class="fa fa-map-marker"></i> </div>
              <div class="ib-info">
                <h4 class="h4">{{Auth::user()->pais}}</h4>
              </div>
            </div>
            <br>
            <div class="icon-box ib-style-1 ib-circle ib-bordered ib-bordered-light ib-small">
              <div class="ib-icon"> <i class="fa fa-envelope "></i> </div>
              <div class="ib-info">
                <h4 class="h4">{{Auth::user()->email}}</h4>
              </div>
            </div>
            <br>
            <div class="icon-box ib-style-1 ib-circle ib-bordered ib-bordered-light ib-small">
              <div class="ib-icon"> <i class="fa fa-phone"></i> </div>
              <div class="ib-info">
                <h4 class="h4">{{Auth::user()->phone}}</h4>
              </div>
            </div>
          </div>
            </div> <br>
          <div class="col-md-6"> <center><img  class="img-responsive" src="{{URL::asset('img/logo/logo_dark.png')}}" alt="" width="200px" height ="200px"></center> </div>
        </div>
      </div>
    </section>
    <br><br><br>
    
    <!-- Skills -->
    <section class="gray-bg margin-bottom-80">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-6 no-padding"> <img class="width-100" src="{{URL::asset('img/pictures/7.jpg')}}" alt=""> </div>
          <div class="col-md-6 no-padding">
            <div class="padding-left-50 padding-bottom-50 padding-top-50 have-sikills">
              <div class="heading-block style-4 text-left margin-bottom-10 width-100">
                <h4>PROGRESOS </h4>
              </div>
              <div class="progress-bars style-2 margin-top-30"> 
                <div class="bar">

                  <span class="progress-bar-tooltip caret-down">75%</span>
                  <div class="progress" data-percent="75%">
                    <div class="progress-bar progress-bar-primary" style="width: 75%;"> </div>
                  </div>
                </div>
                <br><br>
                <center>
                <a href="#." class="btn btn-large dark-border">CONTINUAR EL PROGRESO</a>
                </center>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class="why-choose padding-top-50">
      <div class="container"> 
        <!-- Main Heading -->
        <div class="heading-block text-center ">
          <h3>PERFIL</h3> 
          <div class="divider divider-short divider-center"><img class="i-div" src="{{URL::asset('img/logo/logo_dark.png')}}" alt="Divider Icon Image"/>
          <span>Familia Profit 4 Life</span> </div>
        </div>
      </div>
    </section>

   
    

  </div>
  <!-- End Content --> 


  
@include('layouts.footer')
</body>
</html>