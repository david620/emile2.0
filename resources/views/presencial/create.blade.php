@extends('adminlte::layouts.app')

@section('htmlheader_title')
	Crear Curso Presencial
@endsection
@section('contentheader_title') {{-- TITULO DEL CONTENIDO DE LA VISTA --}}

@stop

@section('main-content')

	@if (count($errors) > 0)
		<div class="alert alert-danger">
			<h4><i class="icon fa fa-ban"></i> ¡Uy!, ha ocurrido un problema</h4>
			<ul>
				@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
	@endif

          <div class="row">
              <div class="col-md-3 col-md-offset-1">
                  <div class="box box-solid">
                      <div class="box-header with-border">
                          <h3 class="box-title">Acciones</h3>
                      </div>
                      <div class="box-body">
                          <center>
                            <a class="btn btn-primary btn-block " href="{{ route('presencial.index')}}">Regresar</a>   
                          </center>
                      </div>
                  </div>
              </div>
          </div>


	{!! Form::open(array('route' => 'presencial.store','method'=>'POST')) !!}
    {{ csrf_field() }}
          <div class="row">
        <div class="col-md-9 col-md-offset-1">
         <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Crear Curso Presencial</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form">
              <div class="box-body">
                <div class="form-group">
                  <label for="name">Nombre del Curso:</label>
		            {!! Form::text('name', null, array('placeholder' => 'Nombre del Usuario: ','class' => 'form-control', 'required' => 'required')) !!}
                </div>
                <div class="form-group">
                  <label for="description">Descripción del Curso:</label>
                {!! Form::text('description', null, array('placeholder' => 'Descripción del Curso: ','class' => 'form-control', 'required' => 'required')) !!}
                </div>
                <div class="form-group">
                  <label for="task_date">Fecha Inicio:</label>
                  {!! Form::date('task_date', null, array('placeholder' => 'Fecha Inicio: ','class' => 'form-control', 'required' => 'required')) !!}
                </div>
                <div class="form-group">
                  <label for="task_date2">Fecha Final:</label>
                  {!! Form::date('task_date2', null, array('placeholder' => 'Fecha Final: ','class' => 'form-control', 'required' => 'required')) !!}
                </div>
                <div class="form-group">
                  <label for="disponibilidad">Disponibilidad del Curso:</label>
                {!! Form::text('disponibilidad', null, array('placeholder' => 'Descripción del Curso: ','class' => 'form-control', 'required' => 'required')) !!}
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Crear</button>
              </div>
            </form>
          </div>
        </div>
        </div>
	{!! Form::close() !!}
@endsection
