<!doctype html>
<html class="no-js" lang="es">
@include('layouts.layouts_user.head')
<body>

<!-- Page Wrapper -->
<div id="wrap"> 
  
  <!-- Header -->
@include('layouts.layouts_user.header')
  <!-- End Header --> 
  
  <!--======= HOME MAIN SLIDER =========-->
  <section class="home-slider">
    <div class="tp-banner-container">
      <div class="tp-banner">
        <ul>
          <!-- SLIDE  -->
          <li data-transition="random" data-slotamount="7" data-masterspeed="300"  data-saveperformance="off" > 
            <!-- MAIN IMAGE --> 
            <img src="{{URL::asset('img/contador/fondo6.png')}}"  alt="slider"  data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat"> 
            <!-- LAYERS --> 
            <div class="sub-banner">
              <div class="position-center-center">
                <img src="{{URL::asset('img/logo/logo_ligth.png')}}" alt="" width="200px" height="200px" />
                <h2>{{$video->nombre_video}}</h2>
                <!--======= Breadcrumb =========-->
                
              </div>
            </div>
            
            <!-- LAYER NR. 1 -->
            <div class="tp-caption biglargetext sfr tp-resizeme" 
                data-x="center" data-hoffset="0" 
                data-y="center" data-voffset="-90" 
                data-speed="500" 
                data-start="1000" 
                data-easing="Power3.easeInOut" 
                data-splitin="chars" 
                data-splitout="none" 
                data-elementdelay="0.06" 
                data-endelementdelay="0.1" 
                data-endspeed="300"
                style="z-index: 5; max-width: auto; max-height: auto; white-space: nowrap;"> </div>
            
            <!-- LAYER NR. 2 -->
            <div class="tp-caption verybiglargetext sfr tp-resizeme" 
                data-x="center" data-hoffset="0" 
                data-y="center" data-voffset="40" 
                data-speed="500" 
                data-start="1500" 
                data-easing="Power3.easeInOut" 
                data-splitin="chars" 
                data-splitout="none" 
                data-elementdelay="0.07" 
                data-endelementdelay="0.1" 
                data-endspeed="300" 
                style="z-index: 6; max-width: auto; max-height: auto; white-space: nowrap;"> </div>
            
            <!-- LAYER NR. 3 -->
            <div class="tp-caption font-crimson font-italic sfb tp-resizeme" 
                data-x="center" data-hoffset="0" 
                data-y="center" data-voffset="140" 
                data-speed="500" 
                data-start="1900" 
                data-easing="Power3.easeInOut" 
                data-splitin="none" 
                data-splitout="none" 
                data-elementdelay="0.1" 
                data-endelementdelay="0.1" 
                data-endspeed="300" 
                style="z-index: 7; font-size:18px; color:#fff; max-width: auto; max-height: auto; white-space: nowrap;"> </div>
            
            <!-- LAYER NR. 4 -->
            <div class="tp-caption lfb tp-scrollbelowslider" 
                data-x="center" data-hoffset="0" 
                data-y="bottom" data-voffset="-50" 
                data-speed="500" 
                data-start="2400" 
                data-easing="Power3.easeInOut" 
                data-elementdelay="0.1" 
                data-endelementdelay="0.1" 
                data-endspeed="300" 
                data-scrolloffset="0"
                style="z-index: 8;"> </div>
          </li>
          
        </ul>
      </div>
    </div>
  </section>
  
  <!-- Content -->
  <div id="content"> 
    <div class="tradingview-widget-container">
  <div class="tradingview-widget-container__widget"></div>
  <script type="text/javascript" src="https://s3.tradingview.com/external-embedding/embed-widget-ticker-tape.js" async>
  {
  "symbols": [
    {
      "title": "S&P 500",
      "proName": "INDEX:SPX"
    },
    {
      "title": "Shangai Composite",
      "proName": "INDEX:XLY0"
    },
    {
      "title": "EUR/USD",
      "proName": "FX_IDC:EURUSD"
    },
    {
      "title": "BTC/USD",
      "proName": "BITFINEX:BTCUSD"
    },
    {
      "title": "ETH/USD",
      "proName": "BITFINEX:ETHUSD"
    }
  ],
  "theme": "light",
  "isTransparent": false,
  "displayMode": "adaptive",
  "locale": "es"
}
  </script>
</div>

    <!-- Why Choose Us -->
    <section class="why-choose padding-top-20 ">
      <div class="container"> 
        <!-- Main Heading -->
        <div class="heading-block text-center ">
          <h3>VIDEOS</h3>
          <div class="divider divider-short divider-center"><img class="i-div" src="{{URL::asset('img/logo/logo_dark.png')}}" alt="Divider Icon Image"/>
          <span>Familia Profit 4 Life</span></div>
        </div>
      </div>
    </section>
  <!-- Services Intro -->
   <section class="blog blog-detail padding-bottom-80"> 
      <!-- TITTLE -->
      <div class="container"> 
        
        <!-- BLOG ROW -->
        <div class="row">
          <div class="col-md-12"> 
            
            <!-- BLOG POST -->
            <article class="blog-post width-100">
              <div class="embed-responsive embed-responsive-16by9">
                {!!$video->iframe_video!!}
              </div>
              <br><br>
              <h4 class="font-normal">{{$video->nombre_video}}</h4>
              <span class="post-bt">por <span class="text-color-primary">Emile Machado</span></span>
              <ul class="post-info">

              </ul>
              <p align="justify">{{$video->descripcion_video}}</p>
              
              <div class="auther-name text-right">
                <h6 class="font-12px margin-top-20 margin-bottom-10">EMILE MACHADO</h6>
                <span class="font-16px font-crimson font-italic">CEO/Fundador Profit 4 Life</span> 
                
                <!-- Social Icons -->
                <ul class="social-icons">
                  <li><a href="https://www.instagram.com/emiletrader/"><i class="fa fa-instagram"></i></a></li>
                  <li><a href="https://www.facebook.com/Profit4lifevzla/"><i class="fa fa-facebook"></i></a></li>
                  <li><a href="https://www.youtube.com/channel/UCpF5xy-jW3rhqN8uElpVYVw""><i class="fa fa-youtube"></i></a></li>
                </ul>
              </div>
            </article>

            
            <!-- Comments -->
            <div class="comments"> 
              
              <!-- Main Heading -->
              <div class="heading-side-bar margin-bottom-50 margin-top-100">
                <h4>Preguntas</h4>
              </div>
              <ul>
                
                @foreach($preguntas as $pregunta)

                <li class="margin-bottom-30">
                  <div class="media">
                    <div class="media-left">
                      <div class="avatar"></div>
                    </div>
                    <div class="media-body">
                      <div class="a-com"> <span class="a-name text-color-primary">{{(\App\User::where('id', '=',$pregunta->user_id)->first())->name}} {{(\App\User::where('id', '=',$pregunta->user_id)->first())->apellido}} </span><span class="date">{{$pregunta->created_at}}</span>
                        <p class="margin-top-20">{{$pregunta->pregunta}}</p>
                      </div>
                    </div>
                  </div>
                </li>
                
                @if($pregunta->respuesta != null)
                <li class="com-reply">
                  <div class="media">
                    <div class="media-left">
                      <div class="avatar"></div>
                    </div>
                    <div class="media-body">
                      <div class="a-com"> <span class="a-name text-color-primary">Emile Machado </span><span class="date">{{$pregunta->updated_at}}</span>
                        <p class="margin-top-20">{{$pregunta->respuesta}}</p>
                      </div>
                    </div>
                  </div>
                </li>
                @endif
                <br>
                <br>
                @endforeach
              </ul>
              
              <!-- Comments Form -->
              <div class="comment-form"> 
                <!-- Main Heading -->
                <div class="heading-side-bar margin-bottom-50 margin-top-80">
                  <h4>Enviar Pregunta</h4>
                </div>
                  {!! Form::open(array('route' => 'preguntas.store','method'=>'POST')) !!}
                  {{ csrf_field() }}
                    <ul class="row">
                      <li class="col-sm-6">
                        <label>*NOMBRE
                          <input class="form-control" type="text" placeholder="" value="{{Auth::user()->name}}">
                        </label>
                      </li>
                      <li class="col-sm-6">
                        <label>*EMAIL
                          <input class="form-control" type="text" placeholder="" value="{{Auth::user()->email}}">
                        </label>
                      </li>
                      <li class="col-sm-12">
                        <textarea name="pregunta" placeholder="PREGUNTA"></textarea>
                      </li>
                      <input type="hidden" name="video_id" value="{{$video->id}}">
                      <li class="col-sm-12">
                        <button type="submit" class="btn btn-dark">ENVIAR PREGUNTA </button>
                      </li>
                    </ul>
                    {!! Form::close() !!}
              </div>
            </div>
          </div>
          
          <!-- Side Bar -->
          
        </div>
      </div>
    </section>
<!-- Why Choose Us -->
    <section class="why-choose">
      <div class="container"> 
        <!-- Main Heading -->
        <div class="heading-block text-center ">
          <h3>VIDEOS</h3> 
          <div class="divider divider-short divider-center"><img class="i-div" src="{{URL::asset('img/logo/logo_dark.png')}}" alt="Divider Icon Image"/>
          <span>Familia Profit 4 Life</span> </div>
        </div>
      </div>
    </section>

   
    

  </div>
  <!-- End Content --> 


  
@include('layouts.footer')
</body>
</html>