<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="keywords" content="HTML5 Template" />
    <meta name="description" content="Appino - Responsive App Landing Page" />
    <meta name="author" content="iqonicthemes.in" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <title>Profit 4 Life - APP</title>  
    <!-- Favicon -->
    <link rel="shortcut icon" href="{{URL::asset('assets_apps/img/logo.png')}}" />
    
    <!-- Google Fonts -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700&amp;Raleway:300,400,500,600,700,800,900">
    
    <!-- Bootstrap -->
    <link rel="stylesheet" href="{{URL::asset('assets_apps/css/bootstrap.min.css')}}">
    
    <!-- owl-carousel -->
    <link rel="stylesheet" type="text/css" href="{{URL::asset('assets_apps/css/owl.carousel.css')}}" />
    
    <!-- Font Awesome -->
    <link rel="stylesheet" type="text/css" href="{{URL::asset('assets_apps/css/font-awesome.css')}}" />
    
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{URL::asset('assets_apps/css/ionicons.min.css')}}">
    
    <!-- Style -->
    <link rel="stylesheet" href="{{URL::asset('assets_apps/css/style_black.css')}}">

    <!-- Responsive -->
    <link rel="stylesheet" href="{{URL::asset('assets_apps/css/responsive.css')}}">
</head>

<body>
    
    <!-- loading -->
    <div id="loading" class="green-bg">
        <div id="loading-center">
            <div class="boxLoading"></div>
        </div>
    </div>
    <!-- loading End -->

    <!-- Header -->
    <header id="header-wrap" data-spy="affix" data-offset-top="55">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <nav class="navbar navbar-default">
                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                            
                            <a class="navbar-brand" href="{{route('home.index')}}">
                                <img src="{{URL::asset('assets_apps/img/logo_ligth.png')}}" alt="logo">
                            </a>
                        </div>
                        <!-- Collect the nav links, forms, and other content for toggling -->
                        
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <!-- Header End -->

    <!-- Banner -->
    <section id="iq-home" class="banner-03 iq-bg iq-bg-fixed iq-box-shadow iq-over-black-80" style=" background: url(assets_apps/img/fondos.png);">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h1 class="iq-font-white iq-tw-8 text-center">PROFIT CALLS</h1>
                    <div class="iq-mobile-app text-center">
                        <div class="iq-mobile-box">
                            <img class="iq-mobile-img" src="{{URL::asset('assets_apps/img/phone15.png')}}" alt="#">
                            <span data-depth="0.8" class="layer iq-mobile-icon icon-01" data-bottom="transform:translateX(50px)" data-top="transform:translateX(-100px);">
                                <img src="{{URL::asset('assets_apps/img/04.png')}}" alt="#">
                            </span>
                            <span data-depth="0.8" class="layer iq-mobile-icon icon-02" data-bottom="transform:translateX(100px)" data-top="transform:translateX(0px);">
                                <img src="{{URL::asset('assets_apps/img/05.png')}}" alt="#">
                            </span>
                            <span data-depth="0.8" class="layer iq-mobile-icon icon-03" data-bottom="transform:translateX(-50px)" data-top="transform:translateX(40px);">
                                <img src="{{URL::asset('assets_apps/img/06.png')}}" alt="#">
                            </span>
                            <span data-depth="0.8" class="layer iq-mobile-icon icon-04" data-bottom="transform:translateX(-30px)" data-top="transform:translateX(0px);">
                                <img src="{{URL::asset('assets_apps/img/07.png')}}" alt="#">
                            </span>
                            <span data-depth="0.8" class="layer iq-mobile-icon icon-05" data-bottom="transform:translateX(0px)" data-top="transform:translateX(-50px);">
                                <img src="{{URL::asset('assets_apps/img/08.png')}}" alt="#">
                            </span>
                        </div>
                    </div>
                     <div class="link">
                        <h5 class="iq-font-white" data-animation="animated fadeInLeft">Descargar Aqui</h5>
                        <ul class="list-inline" data-animation="animated fadeInUp">
                            <li><a href="https://play.google.com/store/apps/details?id=profit4life.app"><i class="ion-social-android"></i></a></li>
                            <li><a href="https://apps.apple.com/us/app/profit4life/id1473356954?l=es&ls=1"><i class="ion-social-apple"></i></a></li>
                        </ul>
                        <p class="iq-font-white iq-mt-10">Estamos orgullosos de presentar nuestra App <br> Descarga para el sistema de tu preferencia.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Banner End -->


<div class="main-content">
    <!-- Feature -->
    <section id="about-us" class="overview-block-ptb iq-mt-50">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center">
                    <div class="heading-title iq-mb-20">
                        <h2 class="title iq-tw-6">Profit Calls</h2>
                        <div class="divider"></div>    
                    </div>
					  <div class="container">
					            <center>
					              <video style="width:80%;height:80%;" id="player1" poster="{{URL::asset('assets_apps/video/poster.PNG')}}" controls preload="none">
					                <source type="video/mp4" src="{{URL::asset('assets_apps/video/call.mp4')}}" />
					              </video>
					            </center>
					    </div>>
                <div class="btn-group iq-mt-40" role="group" aria-label="">
                  <a class="button iq-mr-15" href="https://apps.apple.com/us/app/profit4life/id1473356954?l=es&ls=1"> <i class="ion-social-apple"></i> App Store</a>
                  <a class="button iq-mr-15" href="https://play.google.com/store/apps/details?id=profit4life.app"> <i class="ion-social-android"></i> Play Store</a>
                </div>
            </div>
        </div>
        </div>
    </section>
    <!-- Feature END -->

<hr>

    <!-- Special Features -->
    <section id="features" class="overview-block-ptb iq-amazing-tab white-bg">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="heading-title">
                        <h2 class="title iq-tw-6">Beneficios de Profit Calls</h2>
                        <p>Es Un Producto De Emile Trader Con Características Para Consolidar Tus Conocimientos De Trading.</p>
                        <div class="divider"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 col-md-4 col-lg-4">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active wow fadeInLeft" data-wow-duration="1s">
                            <a class="round-right" href="#design" aria-controls="design" role="tab" data-toggle="tab">
                                <div class="iq-fancy-box-01 text-right">
                                    <i aria-hidden="true" class="ion-ios-checkmark-outline"></i>
                                    <h4 class="iq-tw-6">PROFIT CALLS</h4>
                                    <div class="fancy-content-01">
                                        <p>Sé parte de la evolución de Profit 4 Life con nuestras Profit Calls.</p>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li role="presentation" class="wow fadeInLeft" data-wow-duration="1s" >
                            <a class="round-right" href="#resolution" aria-controls="resolution" role="tab" data-toggle="tab">
                                <div class="iq-fancy-box-01 text-right">
                                    <i aria-hidden="true" class="ion-ios-checkmark-outline"></i>
                                    <h4 class="iq-tw-6">MERCADOS EN VIVO</h4>
                                    <div class="fancy-content-01">
                                        <p>Mantente en línea con los Mercados en Vivo.</p>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li role="presentation" class="wow fadeInLeft" data-wow-duration="1s">
                            <a class="round-right" href="#ready" aria-controls="ready" role="tab" data-toggle="tab">
                                <div class="iq-fancy-box-01 text-right">
                                    <i aria-hidden="true" class="ion-ios-checkmark-outline"></i>
                                    <h4 class="iq-tw-6">RESUMEN DE PROFITS</h4>
                                    <div class="fancy-content-01">
                                        <p>Manten un historial de Profits para complementar el estudio y mejorar tu trading.</p>
                                    </div>
                                </div>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-4 text-center hidden-sm hidden-xs">
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="design"><img src="{{URL::asset('assets_apps/img/app6.png')}}" class="img-responsive center-block" alt="#"></div>
                        
                    </div>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-4">
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="wow fadeInRight" data-wow-duration="1s">
                            <a href="#fertures" aria-controls="fertures" role="tab" data-toggle="tab">
                                <div class="iq-fancy-box-01">
                                    <i aria-hidden="true" class="ion-ios-checkmark-outline"></i>
                                    <h4 class="iq-tw-6">CALCULADORA DE PIPS</h4>
                                    <div class="fancy-content-01">
                                        <p>Utiliza nuestra Calculadora de Pips para complementar tu trading.

</p>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li role="presentation" class="wow fadeInRight" data-wow-duration="1s">
                            <a href="#face" aria-controls="face" role="tab" data-toggle="tab">
                                <div class="iq-fancy-box-01">
                                    <i aria-hidden="true" class="ion-ios-checkmark-outline"></i>
                                    <h4 class="iq-tw-6">ALARMAS DE PRECIOS</h4>
                                    <div class="fancy-content-01">
                                        <p>Configura tus alarmas para saber los precios más recientes.</p>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li role="presentation" class="wow fadeInRight" data-wow-duration="1s">
                            <a href="#codes" aria-controls="codes" role="tab" data-toggle="tab">
                                <div class="iq-fancy-box-01">
                                    <i aria-hidden="true" class="ion-ios-checkmark-outline"></i>
                                    <h4 class="iq-tw-6">NOTICIAS Y ANÁLISIS</h4>
                                    <div class="fancy-content-01">
                                        <p>Mantente al día de las noticias más relevantes de la mano de Profit 4 Life.</p>
                                    </div>
                                </div>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <!-- Special Features END -->

    <section class="overview-block-ptb iq-bg iq-bg-fixed iq-over-black-80" style="background: url(assets_apps/img/fondos.png);" >
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="heading-title iq-mb-40">
                        <h2 class="title iq-tw-6 iq-font-white">Descarga la App</h2>
                        <div class="divider white"></div>
                        <p class="iq-font-white">ÚNETE A LA EVOLUCIÓN DE PROFIT 4 LIFE CON PROFIT CALLS Y LLEVA TU TRADING AL SIGUIENTE NIVEL.</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 text-center">
                    <a class="button button-icon iq-mr-15" href="https://apps.apple.com/us/app/profit4life/id1473356954?l=es&ls=1"> <i class="ion-social-apple"></i> App Store</a>
                    <a class="button button-icon iq-mr-15 re4-mt-20" href="https://play.google.com/store/apps/details?id=profit4life.app"> <i class="ion-social-android"></i> Google Play</a>      
                </div>
            </div>
        </div>
    </section>

    <!-- App Screenshots -->
    <section id="screenshots" class="iq-app iq-bg iq-bg-fixed iq-font-white" style="background: url(assets_apps/img/fondos.png);>
        <div class="container-fluid">
            <div class="row row-eq-height">
                <div class="col-md-6 text-left iq-ptb-80 green-bg">
                <div class="iq-app-info">
                    <h2 class="heading-left iq-font-white white iq-tw-6 ">Profit Calls</h2>
                    <div class="lead iq-tw-6 iq-mb-20"><p align="justify">Estamos aquí con las mejores ofertas de precios. Comience su futuro con nuestro increíble plan de precios. Aquí hay precios asequibles disponibles. Vaya con su elección y disfrute de los servicios.</p></div>
                </div>
                </div>
                <div class="col-md-6 iq-app-screen iq-ptb-80">
                    <div class="home-screen-slide">
                        <div class="owl-carousel popup-gallery" data-autoplay="true" data-loop="true" data-nav="false" data-dots="false" data-items="3" data-items-laptop="2" data-items-tab="2" data-items-mobile="1" data-items-mobile-sm="1" data-margin="15">
                            <div class="item"><img class="img-responsive" src="{{URL::asset('assets_apps/img/app6.png')}}" alt="#"></div>
                            <div class="item"><img class="img-responsive" src="{{URL::asset('assets_apps/img/22.png')}}" alt="#"></div>
                            <div class="item"><img class="img-responsive" src="{{URL::asset('assets_apps/img/23.png')}}" alt="#"></div>
                            <div class="item"><img class="img-responsive" src="{{URL::asset('assets_apps/img/24.png')}}" alt="#"></div>
                            <div class="item"><img class="img-responsive" src="{{URL::asset('assets_apps/img/25.png')}}" alt="#"></div>
                            <div class="item"><img class="img-responsive" src="{{URL::asset('assets_apps/img/26.png')}}" alt="#"></div>
                            
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>
    <!-- App Screenshots END -->

    <section id="pricing" class="overview-block-ptb grey-bg iq-price-table">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="heading-title">
                        <h2 class="title iq-tw-6">Nuesros Beneficios</h2>
                        <div class="divider"></div>
                        <p>Estamos aquí con las mejores ofertas de precios. Comience su futuro con nuestro increíble plan de precios. Aquí hay precios asequibles disponibles. Vaya con su elección y disfrute de los servicios.</p>
                    </div>
                    <div>
                    <div class="iq-pricing text-center">
                        <div class="price-title green-bg ">
                            <h1 class="iq-font-white iq-tw-7"><small>$</small>{{$servicio[2]->precio_servicio}}<small></small></h1>
                        </div>
                        <ul>
                            <li>Profit Calls</li>
                            <li>MErcados en Vivo</li>
                            <li>Resumen de Profits</li>
                            <li>Calculadora de Pips</li>
                            <li>Alarma de Precios</li>
                            <li>Noticias y Análisis Técnicos</li>
                        </ul>
                        <div class="price-footer">
                            <a class="button" href="# ">Comprar</a>
                        </div>
                    </div>
                </div>
                </div>
            </div>            
        </div>
    </section>
    <section id="features" class="overview-block-ptb iq-amazing-tab white-bg">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="heading-title">
                        <h2 class="title iq-tw-6">Si eres un nuevo Usuario, Únete A La Evolución con Profit 4 Life</h2>
                        <div class="divider"></div>
                    </div>
                        {!! Form::open(array('route' => 'store.app','method'=>'POST')) !!}
                        {{ csrf_field() }}    
                            <div class="contact-form">
                                <div class="col-sm-6">
                                    <div class="section-field">
                                        {!! Form::text('nombre', null, array('placeholder' => 'Nombre del Usuario','class' => 'form-control', 'required' => 'required')) !!}
                                    </div>
                                    <div class="section-field">
                                      {!! Form::text('phone', null, array('placeholder' => 'Teléfono del Usuario','class' => 'form-control', 'required' => 'required')) !!}
                                    </div>

                                </div>
                                <div class="col-sm-6">
                                    <div class="section-field">
                                        {!! Form::text('apellido', null, array('placeholder' => 'Apellido del Usuario','class' => 'form-control', 'required' => 'required')) !!}
                                    </div>
                                    <div class="section-field">
                                        {!! Form::text('nickname', null, array('placeholder' => 'Nickname del Usuario','class' => 'form-control', 'required' => 'required')) !!}
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="section-field">
                                        {!! Form::text('email', null, array('placeholder' => 'Email del Usuario','class' => 'form-control', 'required' => 'required')) !!}
                                    </div>
                                    <div class="section-field">
                                        {!! Form::text('password', null, array('placeholder' => 'Confirmar Contraseña del Usuario: ','class' => 'form-control', 'required' => 'required')) !!}
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="section-field">
                                        {!! Form::text('password', null, array('placeholder' => 'Contraseña del Usuario: ','class' => 'form-control', 'required' => 'required')) !!}
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <input type="hidden" name="action" value="sendEmail" />
                                    <button id="submit" name="submit" type="submit" value="Send" class="button pull-right iq-mt-40">JOIN US</button>
                                </div>
                            </div>
                        {!! Form::close() !!}
                </div>
            </div>
        </div>
    </section>
</div>


<div class="footer">
    <!-- Subscribe Our Newsletter -->
    <div class="iq-ptb-60 green-bg iq-subscribe">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center">
                        {!! Form::open(array('route' => 'store.app','method'=>'POST')) !!}
                        {{ csrf_field() }}    
                    	<div class="heading-titles">
                        <h2 class="title iq-tw-6 iq-font-white">Renueva La Evolución De Profit 4 Life</h2>
                        <div class="divider white"></div>
                    	</div>
                        <div class="form-group">                      
                            {!! Form::text('email', null, array('placeholder' => 'Email del Usuario','class' => 'form-control', 'required' => 'required')) !!} {!! Form::text('password', null, array('placeholder' => 'Contraseña del Usuario: ','class' => 'form-control', 'required' => 'required')) !!}
                        </div>
                        <button id="submit" name="submit" type="submit" value="Send" class="button bt-white re-mt-30">Renovar</button>
                        {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <!-- Footer -->
    <footer class="iq-footer-03 white-bg iq-ptb-20">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center">
                    <div class="footer-copyright iq-pt-10">Profit4Lifevzla © Copyright is reserved. Developed by <a target="_blank" href="https://iqonicthemes.com/">Átomo Tecnologías Virtuales</a>.</div>
                </div>
            </div>
        </div>
    </footer>
    <!--  END -->
</div>
    
   <!-- back-to-top -->
    <div id="back-to-top">
        <a class="top" id="top" href="#top"> <i class="ion-ios-upload-outline"></i> </a>
    </div>

    <!-- jQuery -->
    <script src="{{URL::asset('assets_apps/js/jquery.min.js')}}"></script>

    <!-- owl-carousel -->
    <script src="{{URL::asset('assets_apps/js/owl-carousel/owl.carousel.min.js')}}"></script>

    <!-- Counter -->
    <script src="{{URL::asset('assets_apps/js/counter/jquery.countTo.js')}}"></script>

    <!-- Jquery Appear -->
    <script src="{{URL::asset('assets_apps/js/jquery.appear.js')}}"></script>

    <!-- Magnific Popup --> 
    <script src="{{URL::asset('assets_apps/js/magnific-popup/jquery.magnific-popup.min.js')}}"></script>

    <!-- Retina -->
    <script src="{{URL::asset('assets_apps/js/retina.min.js')}}"></script>

    <!-- wow -->
    <script src="{{URL::asset('assets_apps/js/wow.min.js')}}"></script>

    <!-- Skrollr --> 
    <script src="{{URL::asset('assets_apps/js/skrollr.min.js')}}"></script>

    <!-- Countdown -->
    <script src="{{URL::asset('assets_apps/js/jquery.countdown.min.js')}}"></script>

    <!-- bootstrap -->
    <script src="{{URL::asset('assets_apps/js/bootstrap.min.js')}}"></script>

    <!-- Custom -->
    <script src="{{URL::asset('assets_apps/js/custom.js')}}"></script>
</body>
</html>