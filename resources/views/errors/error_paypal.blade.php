<!doctype html>
<html class="no-js" lang="es">
@include('layouts.head')
<body>

@include('layouts.header')
<!-- Page Wrapper -->
<div id="wrap"> 

  
  <!--======= HOME MAIN SLIDER =========-->
  <section class="home-slider">
    <div class="tp-banner-container">
      <div class="tp-banner">
        <ul>
          <!-- SLIDE  -->
          <li data-transition="random" data-slotamount="7" data-masterspeed="300"  data-saveperformance="off" > 
            <!-- MAIN IMAGE --> 
            <img src="{{URL::asset('img/contador/fondos.png')}}"  alt="slider"  data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat"> 
            <!-- LAYERS --> 
            <div class="sub-banner">
              <div class="position-center-center">
                <img src="{{URL::asset('img/logo/logo_ligth.png')}}" alt="" width="200px" height="200px" />
                <h2>Ha ocurrido un problema con su pago</h2><br>
                <p style="color: white;">Por favor verifique su método de pago e intente de nuevo.</p>
                <!--======= Breadcrumb =========-->
                
              </div>
            </div>
            
            <!-- LAYER NR. 1 -->
            <div class="tp-caption biglargetext sfr tp-resizeme" 
                data-x="center" data-hoffset="0" 
                data-y="center" data-voffset="-90" 
                data-speed="500" 
                data-start="1000" 
                data-easing="Power3.easeInOut" 
                data-splitin="chars" 
                data-splitout="none" 
                data-elementdelay="0.06" 
                data-endelementdelay="0.1" 
                data-endspeed="300"
                style="z-index: 5; max-width: auto; max-height: auto; white-space: nowrap;"> </div>
            
            <!-- LAYER NR. 2 -->
            <div class="tp-caption verybiglargetext sfr tp-resizeme" 
                data-x="center" data-hoffset="0" 
                data-y="center" data-voffset="40" 
                data-speed="500" 
                data-start="1500" 
                data-easing="Power3.easeInOut" 
                data-splitin="chars" 
                data-splitout="none" 
                data-elementdelay="0.07" 
                data-endelementdelay="0.1" 
                data-endspeed="300" 
                style="z-index: 6; max-width: auto; max-height: auto; white-space: nowrap;"> </div>
            
            <!-- LAYER NR. 3 -->
            <div class="tp-caption font-crimson font-italic sfb tp-resizeme" 
                data-x="center" data-hoffset="0" 
                data-y="center" data-voffset="140" 
                data-speed="500" 
                data-start="1900" 
                data-easing="Power3.easeInOut" 
                data-splitin="none" 
                data-splitout="none" 
                data-elementdelay="0.1" 
                data-endelementdelay="0.1" 
                data-endspeed="300" 
                style="z-index: 7; font-size:18px; color:#fff; max-width: auto; max-height: auto; white-space: nowrap;"> </div>
            
            <!-- LAYER NR. 4 -->
            <div class="tp-caption lfb tp-scrollbelowslider" 
                data-x="center" data-hoffset="0" 
                data-y="bottom" data-voffset="-50" 
                data-speed="500" 
                data-start="2400" 
                data-easing="Power3.easeInOut" 
                data-elementdelay="0.1" 
                data-endelementdelay="0.1" 
                data-endspeed="300" 
                data-scrolloffset="0"
                style="z-index: 8;"> </div>
          </li>
         
        </ul>
      </div>
    </div>
  </section>

  </div>
  <!-- End Content --> 


  

<!-- JavaScripts --> 
<script src="{{URL::asset('assets/js/vendors/jquery/jquery.min.js')}}"></script> 
<script src="{{URL::asset('assets/js/vendors/wow.min.js')}}"></script> 
<script src="{{URL::asset('assets/js/vendors/bootstrap.min.js')}}"></script> 
<script src="{{URL::asset('assets/js/vendors/own-menu.js')}}"></script> 
<script src="{{URL::asset('assets/js/vendors/flexslider/jquery.flexslider-min.js')}}"></script> 
<script src="{{URL::asset('assets/js/vendors/jquery.countTo.js')}}"></script> 
<script src="{{URL::asset('assets/js/vendors/jquery.isotope.min.js')}}"></script> 
<script src="{{URL::asset('assets/js/vendors/jquery.bxslider.min.js')}}"></script> 
<script src="{{URL::asset('assets/js/vendors/owl.carousel.min.js')}}"></script> 
<script src="{{URL::asset('assets/js/vendors/jquery.sticky.js')}}"></script> 
<script src="{{URL::asset('assets/js/vendors/color-switcher.js')}}"></script>

<!-- SLIDER REVOLUTION 4.x SCRIPTS  --> 
<script type="text/javascript" src="{{URL::asset('assets/rs-plugin/js/jquery.themepunch.tools.min.js')}}"></script> 
<script type="text/javascript" src="{{URL::asset('assets/rs-plugin/js/jquery.themepunch.revolution.min.js')}}"></script> 
<script src="{{URL::asset('assets/js/zap.js')}}"></script>

</body>
</html>