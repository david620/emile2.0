<!doctype html>
<html class="no-js" lang="es">
@include('layouts.head')
<body>

<!-- Page Wrapper -->
<div id="wrap"> 
  
  <!-- Header -->
@include('layouts.header')
  <!-- End Header --> 
  
  <!--======= HOME MAIN SLIDER =========-->
  <section class="home-slider">
    <div class="tp-banner-container">
      <div class="tp-banner">
        <ul>
          <!-- SLIDE  -->
          <li data-transition="random" data-slotamount="7" data-masterspeed="300"  data-saveperformance="off" > 
            <!-- MAIN IMAGE --> 
            <img src="{{URL::asset('img/contador/fondo2.png')}}"  alt="slider"  data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat"> 
            <!-- LAYERS --> 
            <div class="sub-banner">
              <div class="position-center-center">
                <img src="{{URL::asset('img/logo/logo_ligth.png')}}" alt="" width="200px" height="200px" />
                <h2>CONOCE NUESTROS SERVICIOS</h2>
                <!--======= Breadcrumb =========-->
                
              </div>
            </div>
            
            <!-- LAYER NR. 1 -->
            <div class="tp-caption biglargetext sfr tp-resizeme" 
                data-x="center" data-hoffset="0" 
                data-y="center" data-voffset="-90" 
                data-speed="500" 
                data-start="1000" 
                data-easing="Power3.easeInOut" 
                data-splitin="chars" 
                data-splitout="none" 
                data-elementdelay="0.06" 
                data-endelementdelay="0.1" 
                data-endspeed="300"
                style="z-index: 5; max-width: auto; max-height: auto; white-space: nowrap;"> </div>
            
            <!-- LAYER NR. 2 -->
            <div class="tp-caption verybiglargetext sfr tp-resizeme" 
                data-x="center" data-hoffset="0" 
                data-y="center" data-voffset="40" 
                data-speed="500" 
                data-start="1500" 
                data-easing="Power3.easeInOut" 
                data-splitin="chars" 
                data-splitout="none" 
                data-elementdelay="0.07" 
                data-endelementdelay="0.1" 
                data-endspeed="300" 
                style="z-index: 6; max-width: auto; max-height: auto; white-space: nowrap;"> </div>
            
            <!-- LAYER NR. 3 -->
            <div class="tp-caption font-crimson font-italic sfb tp-resizeme" 
                data-x="center" data-hoffset="0" 
                data-y="center" data-voffset="140" 
                data-speed="500" 
                data-start="1900" 
                data-easing="Power3.easeInOut" 
                data-splitin="none" 
                data-splitout="none" 
                data-elementdelay="0.1" 
                data-endelementdelay="0.1" 
                data-endspeed="300" 
                style="z-index: 7; font-size:18px; color:#fff; max-width: auto; max-height: auto; white-space: nowrap;"> </div>
            
            <!-- LAYER NR. 4 -->
            <div class="tp-caption lfb tp-scrollbelowslider" 
                data-x="center" data-hoffset="0" 
                data-y="bottom" data-voffset="-50" 
                data-speed="500" 
                data-start="2400" 
                data-easing="Power3.easeInOut" 
                data-elementdelay="0.1" 
                data-endelementdelay="0.1" 
                data-endspeed="300" 
                data-scrolloffset="0"
                style="z-index: 8;"> </div>
          </li>
          
        </ul>
      </div>
    </div>
  </section>
  
  <!-- Content -->
  <div id="content"> 
    <div class="tradingview-widget-container">
  <div class="tradingview-widget-container__widget"></div>
  <script type="text/javascript" src="https://s3.tradingview.com/external-embedding/embed-widget-ticker-tape.js" async>
  {
  "symbols": [
    {
      "title": "S&P 500",
      "proName": "INDEX:SPX"
    },
    {
      "title": "Shangai Composite",
      "proName": "INDEX:XLY0"
    },
    {
      "title": "EUR/USD",
      "proName": "FX_IDC:EURUSD"
    },
    {
      "title": "BTC/USD",
      "proName": "BITFINEX:BTCUSD"
    },
    {
      "title": "ETH/USD",
      "proName": "BITFINEX:ETHUSD"
    }
  ],
  "theme": "light",
  "isTransparent": false,
  "displayMode": "adaptive",
  "locale": "es"
}
  </script>
</div>

    <!-- Why Choose Us -->
    <section class="why-choose padding-top-20 ">
      <div class="container"> 
        <!-- Main Heading -->
        <div class="heading-block text-center ">
          <h3>SERVICIOS</h3>
          <div class="divider divider-short divider-center"><img class="i-div" src="{{URL::asset('img/logo/logo_dark.png')}}" alt="Divider Icon Image"/>
          <span>Disfruta de los Servicios</span></div>
        </div>
      </div>
    </section>
  <!-- Services Intro -->
    <section class=" padding-bottom-80">
      <div class="container"> 
        
        <!-- UNIQUE DESIGN & FULLY RESPONSIVE -->
        <div class="row">
          <div class="col-md-6"> 
            <!-- Heading -->
            <div class="heading-block no-margin-bottom margin-top-30"> <span class="margin-bottom-15 margin-top-0"></span>
              <h2 class="margin-bottom-20 margin-top-1 letter-space-2 text-center">{{$servicios[0]->nombre_servicio}}</h2>
            </div>
            <p align="justify">{{$servicios[0]->descripcion_servicio}}</p>
            <br>
            <li class="cta-button text-center"><a class="btn dark"  href="{{route('home.producto', Crypt::encrypt($servicios[0]->id))}}">Comprar</a></li>
          </div><br>
          <!-- Image -->
          <div class="col-md-6 text-center"> <img class="img-responsive" src="{{URL::asset($servicios[0]->imagen_servicio)}}" alt=""> </div>
        </div>
        <br><br><br>
        <hr>

        
        <!-- BEST PSD TEMPLATE -->
        <div class="row margin-top-50">
          <div class="col-md-5 text-center"> <img class="img-responsive" src="{{URL::asset($servicios[1]->imagen_servicio)}}" alt=""> </div>
          <div class="col-md-7"> 
            <!-- Heading -->
            <div class="heading-block no-margin-bottom margin-top-30"> <span class="margin-bottom-15 margin-top-0"></span>
              <h2 class="margin-bottom-20 margin-top-1 letter-space-2 text-center">{{$servicios[1]->nombre_servicio}}</h2>
            </div>
            <p align="justify">{{$servicios[1]->descripcion_servicio}}</p>
            <br>
            <li class="cta-button text-center"><a class="btn dark"  href="{{route('home.producto', Crypt::encrypt($servicios[1]->id))}}">Comprar</a></li>
             </div>
        </div>
         <br><br><br>
        <hr>
        <div class="row margin-top-50">
          <div class="col-md-6"> 
            <!-- Heading -->
            <div class="heading-block no-margin-bottom margin-top-30"> <span class="margin-bottom-15 margin-top-0"></span>
              <h2 class="margin-bottom-20 margin-top-1 letter-space-2 text-center">{{$servicios[2]->nombre_servicio}}</h2>
            </div>
            <p align="justify">{{$servicios[2]->descripcion_servicio}}</p>
             <ul class="list-style list-w-bullets text-uppercase">
                <li><p>Mercado en vivo</p></li>
                <li><p>Resumen de Profits</p></li>
                <li><p>Calculadora de Pips</p></li>
                <li><p>Alarma de precios</p></li>
                <li><p>Noticias y análisis técnicos</p></li>
            </ul>
            <br>
            <li class="cta-button text-center"><a class="btn dark"  href="{{route('home.producto', Crypt::encrypt($servicios[2]->id))}}">Comprar</a></li>
          </div><br>
          <!-- Image -->
          <div class="col-md-6 text-center"> <img class="img-responsive" src="{{URL::asset($servicios[2]->imagen_servicio)}}" alt=""> </div>
        </div>
        <br><br><br>
        <hr>

        
        <!-- BEST PSD TEMPLATE -->
        <div class="row margin-top-50">
          <div class="col-md-5 text-center"> <img class="img-responsive" src="{{URL::asset($servicios[3]->imagen_servicio)}}" alt=""> </div>
          <div class="col-md-7"> 
            <!-- Heading -->
            <div class="heading-block no-margin-bottom margin-top-30"> <span class="margin-bottom-15 margin-top-0"></span>
              <h2 class="margin-bottom-20 margin-top-1 letter-space-2 text-center">{{$servicios[3]->nombre_servicio}}</h2>
            </div>
            <p align="justify">{{$servicios[3]->descripcion_servicio}}</p>
            <br>
            <li class="cta-button text-center"><a class="btn dark"  href="{{route('home.producto', Crypt::encrypt($servicios[3]->id))}}">Comprar</a></li>
             </div>
        </div>
         <br><br><br>
        <hr>
        <div class="row margin-top-50">
          <div class="col-md-6"> 
            <!-- Heading -->
            <div class="heading-block no-margin-bottom margin-top-30"> <span class="margin-bottom-15 margin-top-0"></span>
              <h2 class="margin-bottom-20 margin-top-1 letter-space-2 text-center">{{$servicios[4]->nombre_servicio}}</h2>
            </div>
            <p align="justify">{{$servicios[4]->descripcion_servicio}}</p>
            <br>
            <li class="cta-button text-center"><a class="btn dark"  href="{{route('home.producto', Crypt::encrypt($servicios[4]->id))}}">Comprar</a></li>
          </div><br>
          <!-- Image -->
          <div class="col-md-6 text-center"> <img class="img-responsive" src="{{URL::asset($servicios[4]->imagen_servicio)}}" alt=""> </div>
        </div>

        
      </div>
    </section>
<!-- Why Choose Us -->
    <section class="why-choose padding-top-20 ">
      <div class="container"> 
        <!-- Main Heading -->
        <div class="heading-block text-center ">
          <h3>SERVICIOS</h3> 
          <div class="divider divider-short divider-center"><img class="i-div" src="{{URL::asset('img/logo/logo_dark.png')}}" alt="Divider Icon Image"/>
          <span>Disfruta de los Servicios</span> </div>
        </div>
      </div>
    </section>

   
    

  </div>
  <!-- End Content --> 


  
@include('layouts.footer')
</body>
</html>