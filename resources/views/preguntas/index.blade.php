@extends('adminlte::layouts.app')

@section('htmlheader_title')
	Preguntas
@endsection
@section('contentheader_title') {{-- TITULO DEL CONTENIDO DE LA VISTA --}}

@stop


@section('main-content')

<div class="container-fluid">
    <div class="row">
        <div class="col-md-3 col-md-offset-1">
            <div class="box box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Acciones</h3>
                </div>
                <div class="box-body">
 	                <center>
					</center>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
	    	@if ($message = Session::get('success'))
				<div class="alert alert-success">
					<p>{{ $message }}</p>
				</div>
			@endif
          <div class="box">
			<div class="box-body table-responsive-dt">
	            <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    	<tr class="header">
							<th>Pregunta del Usuario</th>
							<th>Respuesta</th>
							<th>Nombre del Usuario</th>
							<th>Acción</th>
						</tr>
                    </thead>
                    <tbody>
						@foreach ($preguntas as $pregunta)
						<tr>
							<td>{{ $pregunta->pregunta }}</td>
							<td>{{ $pregunta->respuesta }}</td>
							<td>
								{{(\App\User::where('id', '=',$pregunta->user_id)->first()['nombre'])}}
							</td>
							<td><center>
								<a class="btn btn-primary" href="{{ route('preguntas.edit',$pregunta->id) }}">Responder Pregunta</a>
								<br>
								<br>
								{!! Form::open(['method' => 'DELETE','route' => ['preguntas.destroy', $pregunta->id],'style'=>'display:inline']) !!}
					            {!! Form::submit('Eliminar', ['class' => 'btn btn-danger']) !!}
					        	{!! Form::close() !!}
							</center></td>
						</tr>
						@endforeach 
                       
                    </tbody>
                </table>
            </div>
		  </div>
        </div>
    </div>
@endsection
