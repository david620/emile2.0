@extends('adminlte::layouts.app')

@section('htmlheader_title')
Responder Pregunta
@endsection
@section('contentheader_title') {{-- TITULO DEL CONTENIDO DE LA VISTA --}}

@stop

@section('main-content')

	@if (count($errors) > 0)
		<div class="alert alert-danger">
			<h4><i class="icon fa fa-ban"></i> ¡Uy!, ha ocurrido un problema</h4>
			<ul>
				@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
	@endif

          <div class="row">
              <div class="col-md-3 col-md-offset-1">
                  <div class="box box-solid">
                      <div class="box-header with-border">
                          <h3 class="box-title">Acciones</h3>
                      </div>
                      <div class="box-body">
                          <center>
                            <a class="btn btn-primary btn-block " href="{{ route('preguntas.index')}}">Regresar</a>   
                          </center>
                      </div>
                  </div>
              </div>
          </div>


  {!! Form::open(array('route' => ['preguntas.answer', $pregunta->id],'method'=>'POST')) !!}
    {{ csrf_field() }}
          <div class="row">
        <div class="col-md-9 col-md-offset-1">
         <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Responder Pregunta</h3>

            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form">
              <div class="box-body">
              <h3 class="box-title">{{$pregunta->pregunta}}</h3>
                <div class="form-group">
                  <label for="respuesta">Respuesta:</label>
		            {!! Form::textarea('respuesta', $pregunta->respuesta, array('placeholder' => 'Respuesta: ','class' => 'form-control', 'required' => 'required')) !!}
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <div class="row">
                      <div class="col-md-6 col-md-offset-1">
                          <button type="submit" class="btn btn-primary">Editar</button>
                      </div>
                </div>
              </div>
            </form>
          </div>
        </div>
        </div>
	{!! Form::close() !!}
@endsection
