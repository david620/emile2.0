<!doctype html>
<html class="no-js" lang="es">

@include('layouts.head')
<body>

<!-- Page Wrapper -->
<div id="wrap"> 
  
  <!-- Header -->
@include('layouts.header')
<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.1.0/fullcalendar.min.css' />
  <!-- End Header --> 
  
  <!--======= HOME MAIN SLIDER =========-->
  <section class="home-slider">
    <div class="tp-banner-container">
      <div class="tp-banner">
        <ul>
          <!-- SLIDE  -->
          <li data-transition="random" data-slotamount="7" data-masterspeed="300"  data-saveperformance="off" > 
            <!-- MAIN IMAGE --> 
            <img src="{{URL::asset('img/contador/fondo1.png')}}"  alt="slider"  data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat"> 
            <!-- LAYERS --> 
            <div class="sub-banner">
              <div class="position-center-center">
                <img src="{{URL::asset('img/logo/logo_ligth.png')}}" alt="" width="200px" height="200px" />
                <h2>EXPERIENCIA EN VIVO</h2>
                <!--======= Breadcrumb =========-->
                
              </div>
            </div>
            
            <!-- LAYER NR. 1 -->
            <div class="tp-caption biglargetext sfr tp-resizeme" 
                data-x="center" data-hoffset="0" 
                data-y="center" data-voffset="-90" 
                data-speed="500" 
                data-start="1000" 
                data-easing="Power3.easeInOut" 
                data-splitin="chars" 
                data-splitout="none" 
                data-elementdelay="0.06" 
                data-endelementdelay="0.1" 
                data-endspeed="300"
                style="z-index: 5; max-width: auto; max-height: auto; white-space: nowrap;"> </div>
            
            <!-- LAYER NR. 2 -->
            <div class="tp-caption verybiglargetext sfr tp-resizeme" 
                data-x="center" data-hoffset="0" 
                data-y="center" data-voffset="40" 
                data-speed="500" 
                data-start="1500" 
                data-easing="Power3.easeInOut" 
                data-splitin="chars" 
                data-splitout="none" 
                data-elementdelay="0.07" 
                data-endelementdelay="0.1" 
                data-endspeed="300" 
                style="z-index: 6; max-width: auto; max-height: auto; white-space: nowrap;"> </div>
            
            <!-- LAYER NR. 3 -->
            <div class="tp-caption font-crimson font-italic sfb tp-resizeme" 
                data-x="center" data-hoffset="0" 
                data-y="center" data-voffset="140" 
                data-speed="500" 
                data-start="1900" 
                data-easing="Power3.easeInOut" 
                data-splitin="none" 
                data-splitout="none" 
                data-elementdelay="0.1" 
                data-endelementdelay="0.1" 
                data-endspeed="300" 
                style="z-index: 7; font-size:18px; color:#fff; max-width: auto; max-height: auto; white-space: nowrap;"> </div>
            
            <!-- LAYER NR. 4 -->
            <div class="tp-caption lfb tp-scrollbelowslider" 
                data-x="center" data-hoffset="0" 
                data-y="bottom" data-voffset="-50" 
                data-speed="500" 
                data-start="2400" 
                data-easing="Power3.easeInOut" 
                data-elementdelay="0.1" 
                data-endelementdelay="0.1" 
                data-endspeed="300" 
                data-scrolloffset="0"
                style="z-index: 8;"> </div>
          </li>
          
        </ul>
      </div>
    </div>
  </section>
  
  <!-- Content -->
  <div id="content"> 
    <div class="tradingview-widget-container">
  <div class="tradingview-widget-container__widget"></div>
  <script type="text/javascript" src="https://s3.tradingview.com/external-embedding/embed-widget-ticker-tape.js" async>
  {
  "symbols": [
    {
      "title": "S&P 500",
      "proName": "INDEX:SPX"
    },
    {
      "title": "Shangai Composite",
      "proName": "INDEX:XLY0"
    },
    {
      "title": "EUR/USD",
      "proName": "FX_IDC:EURUSD"
    },
    {
      "title": "BTC/USD",
      "proName": "BITFINEX:BTCUSD"
    },
    {
      "title": "ETH/USD",
      "proName": "BITFINEX:ETHUSD"
    }
  ],
  "theme": "light",
  "isTransparent": false,
  "displayMode": "adaptive",
  "locale": "es"
}
  </script>
</div>

    <!-- Why Choose Us -->
    <section class="why-choose padding-top-20 ">
      <div class="container"> 
        <!-- Main Heading -->
        <div class="heading-block text-center ">
          <h3>PRESENCIAL</h3>
          <div class="divider divider-short divider-center"><img class="i-div" src="{{URL::asset('img/logo/logo_dark.png')}}" alt="Divider Icon Image"/>
          <span>Familia Profit 4 Life</span></div>
        </div>
      </div>
    </section>
  <!-- Services Intro -->
    <section class=" padding-bottom-80">
      <div class="container"> 
        
        <!-- UNIQUE DESIGN & FULLY RESPONSIVE -->
        <div class="heading-block style-6 text-center">
          <h4>SOMOS PROFIT 4 LIFE Y TE INVITAMOS A PARTICIPAR EN NUESTROS CURSOS EN VIVO. REVISA EL CALENDARIO Y REGISTRATE AL FINAL SEGUN LA DISPONIBILIDAD</h4>
        </div>
        <div class="heading-block style-3 text-center margin-bottom-80">
          <h2 style="color: #61C2C3;">CALENDARIO</h2>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 mb-60">
           <div id="calendar">

          </div>
        </div>
        <br><br><br>
        <hr>
      </div>
    </section>

    <section class="padding-bottom-80 pages-in chart-page">
      <div class="container"> 
        
        <!-- Payments Steps -->
        <div class="shopping-cart"> 
          
          <!-- SHOPPING INFORMATION -->
          <div class="cart-ship-info">
            <div class="row"> 
                <div class="heading-block style-6 text-center">
          <h4>NO TE PIERDAS NUESTROS CURSOS EN VIVO. ¡ÚNETE AHORA!</h4>
        </div>
        <div class="heading-block style-3 text-center">
          <h2 style="color: #61C2C3;">REGISTRARSE EN EL CURSO</h2>
        </div>
              <!-- ESTIMATE SHIPPING & TAX -->
              <div class="col-sm-12">
              {!! Form::open(array('route' => 'usuario-presencial.store','method'=>'POST')) !!}
                {{ csrf_field() }}
                  <ul class="row">
                    <li class="col-md-6"> 
                      <!-- ADDRESS -->
                      <label>
                        <input type="text" name="nombre" value="" placeholder="NOMBRE" required="required">
                      </label>
                    </li>
                    <li class="col-md-6"> 
                      <!-- ADDRESS -->
                      <label>
                        <input type="text" name="apellido" value="" placeholder="APELLIDO" required="required">
                      </label>
                    </li>
                    <li class="col-md-6"> 
                      <!-- ADDRESS -->
                      <label>
                        <input type="text" name="email" value="" placeholder="EMAIL" required="required">
                      </label>
                    </li>
                    <li class="col-md-6"> 
                      <!-- ADDRESS -->
                      <label>
                        <input type="text" name="phone" value="" placeholder="TELÉFONO" required="required">
                      </label>
                    </li>
                    <!-- *COUNTRY -->
                    <li class="col-md-6" >
                      <label> 
                      <select class="selectpicker" id="curso" name="curso" required="required" >
                        @foreach($tasks as $task)
                          @if($task->disponibilidad > 0)
                              <option value="{!!$task->id!!}">{!!$task->name!!}&nbsp&nbsp Disponibilidad: &nbsp&nbsp{!!$task->disponibilidad!!}&nbsp Cupos </option>
                          @else
                          @endif
                        @endforeach
                      </select>
                      </label>
                    </li>
                    <li class="col-md-6">
                      <label>
                        <select class="selectpicker" required="required" name="modalidad"> 
                                        <option value="">MODALIDAD</option>
                                        <option value="1">Curso de 5 días en Persona. Precio: 1500$</option>
                                        <option value="2">Curso de 5 días en Persona + Membresia Premium. Precio: 2000$</option>
                                        <option value="3">Curso de 5 días Versión Online. Precio: 1500$</option>
                                        <option value="4">Curso de 5 días Versión Online + Membresía Premium. Precio: 1750$</option>
                        </select>
                      </label>
                    </li>
                
                    <li class="col-md-12">
                      <div class="checkbox">
                        <input id="checkbox1" class="styled" type="checkbox" required="required">
                        <label for="checkbox1"> ACEPTAR LAS CONDICIONES </label>
                      </div>
                    </li>
                    
                
                        <button type="submit" class="cta-button  text-center btn dark">
                            <span>Registrar</span>
                            <i class="fa fa-check"></i>
                        </button>

                  </ul>
            {!! Form::close() !!}
              </div>
              
              <!-- SUB TOTAL -->
            
            </div>
          </div>
        </div>
        <br><br>        
      </div>
    </section>

    
    


<!-- Why Choose Us -->
    <section class="why-choose">
      <div class="container"> 

        <!-- Main Heading -->
        <div class="heading-block text-center ">
          <h3>PRESENCIAL</h3>
          <div class="divider divider-short divider-center"><img class="i-div" src="{{URL::asset('img/logo/logo_dark.png')}}" alt="Divider Icon Image"/>
          <span>Familia Profit 4 Life</span></div>
        </div>
      </div>
    </section>

   
    

  </div>
  <!-- End Content --> 


  
@include('layouts.footer')
<script src='https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.1.0/fullcalendar.min.js'></script>
<script>
    $(document).ready(function() {
        // page is now ready, initialize the calendar...
        $('#calendar').fullCalendar({
            // put your options and callbacks here
            events : [
                @foreach($tasks as $task)
                {
                    title : '{{ $task->name }}',
                    start : '{{ $task->task_date }}',
                    end : '{{ $task->task_date2 }}'
                },
                @endforeach
            ]
        })
    });
</script>
</body>
</html>
