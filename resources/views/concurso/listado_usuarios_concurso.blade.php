@extends('adminlte::layouts.app')

@section('htmlheader_title')
	Concursos (Rifa)
@endsection
@section('contentheader_title') {{-- TITULO DEL CONTENIDO DE LA VISTA --}}

@stop


@section('main-content')

<div class="container-fluid">

    <div class="row">
        <div class="col-md-3 col-md-offset-1">
                	<br>
            <div class="box box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Acciones</h3>
                </div>
                <div class="box-body">
                    <a class="btn btn-primary btn-block" href="{{ route('listado_concursos')}}">Regresar</a>
                </div>
            </div>
        </div>
    </div>

 

    <div class="row">
        <div class="col-md-10 col-md-offset-1">
	    	@if ($message = Session::get('success'))
				<div class="alert alert-success">
					<p>{{ $message }}</p>
				</div>
			@endif
          <div class="box">
			<div class="box-body">
	            <table id="example2" class="table table-bordered table-hover">
                    <thead>
                    	<tr class="header">
					      <th align="center">Nombre</th>
					      <th align="center">Apellido</th>
					      <th align="center">E-mail</th>
					      <th align="center">Teléfono</th>
					      <th align="center">País</th>
						</tr>
                    </thead>
                    <tbody>
					    @foreach($users as $user)
					      <tbody>
					        <td>{{$user->nombre}}</td>
					        <td>{{$user->apellido}}</td>
					        <td>{{$user->email}}</td>
					        <td>{{$user->telefono}}</td>
					        <td>{{$user->pais}}</td>
					      </tbody>
					    @endforeach
                       
                    </tbody>
                </table>
            </div>
		  </div>
        </div>
    </div>
@endsection
