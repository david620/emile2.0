@extends('adminlte::layouts.app')

@section('htmlheader_title')
	Concursos (Rifa)
@endsection
@section('contentheader_title') {{-- TITULO DEL CONTENIDO DE LA VISTA --}}

@stop


@section('main-content')

<div class="container-fluid">

    <div class="row">
        <div class="col-md-3 col-md-offset-1">
                	<br>
            <div class="box box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Acciones</h3>
                </div>
                <div class="box-body">
                    <a class="btn btn-primary btn-block" href="{{ route('listado_concursos')}}">Regresar</a>
                </div>
            </div>
        </div>
        <div class="col-md-7 col-md">
            <div class="box box-primary">
			  {!! Form::model($concurso, ['method' => 'PATCH','route' => ['concursos.update', $concurso->id]]) !!}
			    {{ csrf_field() }}


			         <div class="box box-primary">
			            <div class="box-header with-border">
			              <h3 class="box-title">Editar Concurso</h3>
			            </div>
			            <!-- /.box-header -->
			            <!-- form start -->
			            <form role="form">
			              <div class="box-body">
			                <div class="form-group">
	                          <label for="premio_semana">Premio de la Semana:</label>
		                      {!! Form::text('premio_semana', $concurso->premio_semana, array('placeholder' => 'Premio: ','class' => 'form-control',  'required' => 'required')) !!}
			                </div>
			              </div>
			              <!-- /.box-body -->
			              <div class="box-footer">
			                <button type="submit" class="btn btn-primary">Editar Concurso</button>
			              </div>
			            </form>
			          </div>

				{!! Form::close() !!}
		    </div>
	    </div>
    </div>

 

    <div class="row">
        <div class="col-md-10 col-md-offset-1">
	    	@if ($message = Session::get('success'))
				<div class="alert alert-success">
					<p>{{ $message }}</p>
				</div>
			@endif
          <div class="box">
			<div class="box-body">
	            <table id="example2" class="table table-bordered table-hover">
                    <thead>
                    	<tr class="header">
					      <th align="center">Fecha Inicio Concurso</th>
					      <th align="center">Premio Semana</th>
					      <th align="center">Estado</th>
					      <th align="center">Acciones</th>
						</tr>
                    </thead>
                    <tbody>
					    @foreach($concursos as $concurso)
					      <tbody>
					        <td>{{$concurso->fecha_vencimiento}}</td>
					        <td>{{$concurso->premio_semana}}</td>
					        <td>
					            @if($concurso->habilitado == 0)
					                Inactivo
					            @endif
					            @if($concurso->habilitado == 1)
					                Activo
					            @endif
					        </td>
					        <td>
					                                        {!!link_to_route('listado_usuarios_concurso', $title = 'Listado Usuarios',
					                                        $parameters = ['id' => $concurso->id],
					                                        $attributes = ['class'=>'btn btn-success','style'=>'display:inline']);!!}
					                                    @if($concurso->habilitado == '0')
					                                        {!!link_to_route('concursos.modificaEstado', $title = 'Activar',
					                                        $parameters = ['id' => $concurso->id],
					                                        $attributes = ['class'=>'btn btn-success','style'=>'display:inline']);!!}
					                                    @endif
					                                    @if($concurso->habilitado == '1')
					                                      {!!link_to_route('concursos.modificaEstado', $title = 'Desactivar',
					                                      $parameters = ['id' => $concurso->id],
					                                      $attributes = ['class'=>'btn btn-danger','style'=>'display:inline']);!!}
					                                    @endif
					                                        {!!link_to_route('listado_concursos_editar', $title = 'Editar',
					                                        $parameters = ['id' => $concurso->id],
					                                        $attributes = ['class'=>'btn btn-warning','style'=>'display:inline']);!!}
					                              {!!link_to_route('concursos.destroy', $title = 'Eliminar',
					                              $parameters = ['id' => $concurso->id],
					                              $attributes = ['class'=>'btn btn bg-red']);!!}  

					                              <a class="btn btn bg-success" href="{{route('concurso_rifa_ganador', ($concurso->id))}}"> Generar Ganador</a>

					        </td>
					      </tbody>
					    @endforeach
                       
                    </tbody>
                </table>
            </div>
		  </div>
        </div>
    </div>
@endsection
