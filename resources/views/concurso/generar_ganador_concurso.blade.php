<!doctype html>
<html class="no-js" lang="es">
@include('layouts.head')
<body>

<!-- Page Wrapper -->
<div id="wrap"> 
  
  <!-- Header -->
@include('layouts.header')
  <!-- End Header --> 
  
  <!--======= HOME MAIN SLIDER =========-->
  <section class="home-slider">
    <div class="tp-banner-container">
      <div class="tp-banner">
        <ul>
          <!-- SLIDE  -->
          <li data-transition="random" data-slotamount="7" data-masterspeed="300"  data-saveperformance="off" > 
            <!-- MAIN IMAGE --> 
            <img src="{{URL::asset('img/contador/fondo3.png')}}"  alt="slider"  data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat"> 
            <!-- LAYERS --> 
            <div class="sub-banner">
              <div class="position-center-center">
                <img src="{{URL::asset('img/logo/logo_ligth.png')}}" alt="" width="200px" height="200px" />
                <h2>APROVECHA LA RIFA</h2>
                <!--======= Breadcrumb =========-->
                
              </div>
            </div>
            
            <!-- LAYER NR. 1 -->
            <div class="tp-caption biglargetext sfr tp-resizeme" 
                data-x="center" data-hoffset="0" 
                data-y="center" data-voffset="-90" 
                data-speed="500" 
                data-start="1000" 
                data-easing="Power3.easeInOut" 
                data-splitin="chars" 
                data-splitout="none" 
                data-elementdelay="0.06" 
                data-endelementdelay="0.1" 
                data-endspeed="300"
                style="z-index: 5; max-width: auto; max-height: auto; white-space: nowrap;"> </div>
            
            <!-- LAYER NR. 2 -->
            <div class="tp-caption verybiglargetext sfr tp-resizeme" 
                data-x="center" data-hoffset="0" 
                data-y="center" data-voffset="40" 
                data-speed="500" 
                data-start="1500" 
                data-easing="Power3.easeInOut" 
                data-splitin="chars" 
                data-splitout="none" 
                data-elementdelay="0.07" 
                data-endelementdelay="0.1" 
                data-endspeed="300" 
                style="z-index: 6; max-width: auto; max-height: auto; white-space: nowrap;"> </div>
            
            <!-- LAYER NR. 3 -->
            <div class="tp-caption font-crimson font-italic sfb tp-resizeme" 
                data-x="center" data-hoffset="0" 
                data-y="center" data-voffset="140" 
                data-speed="500" 
                data-start="1900" 
                data-easing="Power3.easeInOut" 
                data-splitin="none" 
                data-splitout="none" 
                data-elementdelay="0.1" 
                data-endelementdelay="0.1" 
                data-endspeed="300" 
                style="z-index: 7; font-size:18px; color:#fff; max-width: auto; max-height: auto; white-space: nowrap;"> </div>
            
            <!-- LAYER NR. 4 -->
            <div class="tp-caption lfb tp-scrollbelowslider" 
                data-x="center" data-hoffset="0" 
                data-y="bottom" data-voffset="-50" 
                data-speed="500" 
                data-start="2400" 
                data-easing="Power3.easeInOut" 
                data-elementdelay="0.1" 
                data-endelementdelay="0.1" 
                data-endspeed="300" 
                data-scrolloffset="0"
                style="z-index: 8;"> </div>
          </li>
          
        </ul>
      </div>
    </div>
  </section>
  
  <!-- Content -->
  <div id="content"> 
    <div class="tradingview-widget-container">
  <div class="tradingview-widget-container__widget"></div>
  <script type="text/javascript" src="https://s3.tradingview.com/external-embedding/embed-widget-ticker-tape.js" async>
  {
  "symbols": [
    {
      "title": "S&P 500",
      "proName": "INDEX:SPX"
    },
    {
      "title": "Shangai Composite",
      "proName": "INDEX:XLY0"
    },
    {
      "title": "EUR/USD",
      "proName": "FX_IDC:EURUSD"
    },
    {
      "title": "BTC/USD",
      "proName": "BITFINEX:BTCUSD"
    },
    {
      "title": "ETH/USD",
      "proName": "BITFINEX:ETHUSD"
    }
  ],
  "theme": "light",
  "isTransparent": false,
  "displayMode": "adaptive",
  "locale": "es"
}
  </script>
</div>

    <!-- Why Choose Us -->
    <section class="why-choose padding-top-20 ">
      <div class="container"> 
        <!-- Main Heading -->
        <div class="heading-block text-center ">
          <h3>RIFA</h3>
          <div class="divider divider-short divider-center"><img class="i-div" src="{{URL::asset('img/logo/logo_dark.png')}}" alt="Divider Icon Image"/>
          <span>Familia Profit 4 Life</span></div>
        </div>
      </div>
    </section>
 
    <section class="padding-bottom-50 pages-in chart-page">
      <div class="container"> 
        
        <!-- Payments Steps -->
        <div class="shopping-cart"> 
          
          <!-- SHOPPING INFORMATION -->
          <div class="cart-ship-info">
            <div class="row"> 

                  <li class="col-md-12 text-center">
                    <a href="{{route('concurso_ganador', $id)}}" class="btn dark">
                      GENERAR GANADOR <i class="fa fa-paper-plane"></i>
                    </a>
                  </li>

            </div>
          </div>
        </div>
        <br><br>        
      </div>
      <div class="heading-block style-3 text-center"> 
          <br><br>
          <h4>EL PREMIO DE ESTA SEMANA ES:<h4 style="color: #61C2C3;"> 
            @if(isset($concurso->premio_semana))
              {{$concurso->premio_semana}}
            @endif
            </h4> ¡Mientras más tickets adquieras, más oportunidades tendrás de ganar!</h4>
        </div>
    </section>    

<!-- Why Choose Us -->
    <section class="why-choose">
      <div class="container"> 

        <!-- Main Heading -->
        <div class="heading-block text-center ">
          <h3>RIFA</h3>
          <div class="divider divider-short divider-center"><img class="i-div" src="{{URL::asset('img/logo/logo_dark.png')}}" alt="Divider Icon Image"/>
          <span>Familia Profit 4 Life</span></div>
        </div>
      </div>
    </section>

   
    

  </div>
  <!-- End Content --> 


  
@include('layouts.footer')
@section('js')
  <script src="{{ asset('counter/countdowntime/moment.min.js') }}"></script>
  <script src="{{ asset('counter/countdowntime/moment-timezone.min.js') }}"></script>
  <script src="{{ asset('counter/countdowntime/moment-timezone-with-data.min.js') }}"></script>
  <script src="{{ asset('counter/countdowntime/countdowntime.js') }}"></script>
  <script>
    $('.cd100').countdown100({
      /*Set Endtime here*/
      /*Endtime must be > current time*/
      endtimeYear: {{isset($anio)}},
      endtimeMonth: {{isset($mes)}},
      endtimeDate: {{isset($dia)}},
      endtimeHours: {{isset($hora)}},
      endtimeMinutes: 0,
      endtimeSeconds: 0,
      timeZone: "" 
      // ex:  timeZone: "America/New_York"
      //go to " http://momentjs.com/timezone/ " to get timezone
    });
  </script>

@stop


</body>
</html>
