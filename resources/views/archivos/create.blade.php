@extends('adminlte::layouts.app')

@section('htmlheader_title')
	Crear Archivo
@endsection
@section('contentheader_title') {{-- TITULO DEL CONTENIDO DE LA VISTA --}}

@stop

@section('main-content')

	@if (count($errors) > 0)
		<div class="alert alert-danger">
			<h4><i class="icon fa fa-ban"></i> ¡Uy!, ha ocurrido un problema</h4>
			<ul>
				@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
	@endif

          <div class="row">
              <div class="col-md-3 col-md-offset-1">
                  <div class="box box-solid">
                      <div class="box-header with-border">
                          <h3 class="box-title">Acciones</h3>
                      </div>
                      <div class="box-body">
                          <center>
                            <a class="btn btn-primary btn-block " href="{{ route('archivos.index')}}">Regresar</a>   
                          </center>
                      </div>
                  </div>
              </div>
          </div>

    {!! Form::open(array('route' => 'archivos.store','method'=>'POST', 'enctype' => 'multipart/form-data')) !!}
    {{ csrf_field() }}
          <div class="row">
        <div class="col-md-9 col-md-offset-1">
         <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Crear Archivo</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form">
              <div class="box-body">
                <div class="form-group">
                  <label for="name">Nombre del Archivo:</label>
		            {!! Form::text('name', null, array('placeholder' => 'Nombre del Archivo: ','class' => 'form-control', 'required' => 'required')) !!}
                </div>
                <div class="form-group">
                  <label for="url_archivo">Archivo:</label>
                {!! Form::file('url_archivo', null, array('placeholder' => 'Archivo: ','class' => 'form-control', 'required' => 'required')) !!}
                </div>
                <div class="form-group">
                  <label for="plataforma_id">Servicio Asociado:</label>
                  {!! Form::select('plataforma_id',['1' => 'Profit 4 Life', '2' => 'Cripto 4 Life',], '', array('class' => 'form-control')) !!}
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Crear</button>
              </div>
            </form>
          </div>
        </div>
        </div>
	{!! Form::close() !!}
@endsection
