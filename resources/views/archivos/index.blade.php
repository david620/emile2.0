@extends('adminlte::layouts.app')

@section('htmlheader_title')
	Archivos
@endsection
@section('contentheader_title') {{-- TITULO DEL CONTENIDO DE LA VISTA --}}

@stop


@section('main-content')

<div class="container-fluid">
    <div class="row">
        <div class="col-md-3 col-md-offset-1">
            <div class="box box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Acciones</h3>
                </div>
                <div class="box-body">
 	                <center>
					<a class="btn btn-primary btn-block " href="{{route('archivos.create')}}">Crear Archivo</a>					
					</center>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
	    	@if ($message = Session::get('success'))
				<div class="alert alert-success">
					<p>{{ $message }}</p>
				</div>
			@endif
          <div class="box">
			<div class="box-body table-responsive-dt">
	            <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    	<tr class="header">
							<th>Nombre del Archivo</th>
							<th>Url del Archivo</th>
							<th>Plataforma</th>
							<th>Acción</th>
						</tr>
                    </thead>
                    <tbody>
						@foreach ($archivos as $archivo)
						<tr>
							<td>{{ $archivo->nombre_archivo }}</td>
							<td>
								<font color=blue><a style="color:#0000ff" href="{{URL::to($archivo->url_archivo)}}" target="_blank">{{$archivo->nombre_archivo}}</a></font>
							</td>
							<td>{{ $archivo->plataforma_id }}</td>
							<td><center>
								{!! Form::open(['method' => 'DELETE','route' => ['archivos.destroy', $archivo->id],'style'=>'display:inline']) !!}
					            {!! Form::submit('Eliminar', ['class' => 'btn btn-danger']) !!}
					        	{!! Form::close() !!}
							</center></td>
						</tr>
						@endforeach 
                       
                    </tbody>
                </table>
            </div>
		  </div>
        </div>
    </div>
@endsection
