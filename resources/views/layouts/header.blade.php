  <header class="header">
    <div class="sticky">
      <div class="container">
        <div class="logo"> <a href="{{route('home.index')}}"><img src="{{URL::asset('img/logo/logo.png')}}" alt=""></a> </div>
        
        <!-- Nav -->
        <nav>
          <ul id="ownmenu" class="ownmenu">
            <li><a class="border-center" href="{{route('home.index')}}">PRINCIPAL</a>
            </li>
            <li><a href="{{route('home.forex')}}">FOREX</a>
            </li>
            <li><a href="https://www.wsatraining.com/">ENGLISH</a>
            </li>
            <li><a href="{{route('home.servicios')}}">SERVICIOS</a>
            </li>
            <li><a href="{{route('home.app')}}">PROFIT CALLS</a>
            </li>
            <li><a href="{{route('home.presencial')}}">PRESENCIAL</a>
            </li>
            <li><a href="{{route('home.concurso')}}">RIFA</a>
            </li>
            @if(Auth::check())
              <li><a href="{{url('home')}}">{{ Auth::user()->nombre }}</a>
              <ul class="dropdown animated-3s fadeInUp">

                                    <a href="{{ url('/logout') }}" class="border-center" id="logout"
                                       onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                        Cerrar Sesión
                                    </a>

                                    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                        <input type="submit" value="logout" style="display: none;">
                                    </form>
              </ul>
              </li>
            @else
              <li><a href="{{route('login')}}">ENTRAR</a>
              </li>
            @endif
            <li class="shop-cart right"><a href="{{route('home.carrito')}}"><i class="fa fa-shopping-cart"></i></a>
            </li>
          </ul>
        </nav>
      </div>
    </div>
  </header>