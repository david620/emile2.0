<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="author" content="Profit 4 Life" />
<!-- Document Title -->
<title>Profit 4 Life</title>

<!-- Favicon -->
<link rel="shortcut icon" href="{{URL::asset('img/fav/fav.png')}}" type="image/x-icon">
<link rel="icon" href="{{URL::asset('img/fav/fav.png')}}" type="image/x-icon">

<!-- Favicon -->
<link rel="shortcut icon" href="{{URL::asset('img/fav/fav.png')}}" type="image/x-icon">
<link rel="icon" href="{{URL::asset('img/fav/fav.png')}}" type="image/x-icon">


<!-- FontsOnline -->
<link href='https://fonts.googleapis.com/css?family=Montserrat:200,300,400,500,600,700,800,900' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Crimson+Text:400,400italic,600,600italic,700,700italic' rel='stylesheet' type='text/css'>

<!-- StyleSheets -->
<link rel="stylesheet" href="{{URL::asset('assets/css/ionicons.min.css')}}">
<link rel="stylesheet" href="{{URL::asset('assets/css/bootstrap/bootstrap.min.css')}}">
<link rel="stylesheet" href="{{URL::asset('assets/css/font-awesome.min.css')}}">
<link rel="stylesheet" href="{{URL::asset('assets/css/main.css')}}">
<link rel="stylesheet" href="{{URL::asset('assets/css/style.css')}}">
<link rel="stylesheet" href="{{URL::asset('assets/css/responsive.css')}}">

<!-- SLIDER REVOLUTION 4.x CSS SETTINGS -->
<link rel="stylesheet" type="text/css" href="{{URL::asset('assets/rs-plugin/css/settings.css')}}" media="screen" />

<!-- COLORS -->
<link rel="stylesheet" id="color" href="{{URL::asset('assets/css/default.css')}}">

<!-- JavaScripts -->
<script src="{{URL::asset('assets/js/vendors/modernizr.js')}}"></script>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
<!-- Analytics -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-98667359-1', 'auto');
  ga('send', 'pageview');

</script>   
</head>