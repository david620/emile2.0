  <!-- Footer -->
  <footer id="footer">
    <div class="footer-wrapper"> 
      
      <!-- Footer Top -->
      <div class="footer-top">
        <div class="footer-top-wrapper">
          <div class="container">
            <div class="row"> 
              <!-- About Block -->
              <div class="col-md-6">
                <div class="block block-about">
                  <h3 class="block-title"><span>Profit 4 Life</span></h3>
                  <div class="block-content">
                    <p align="justify">La vida como emprendedor en los mercados de criptomonedas no es nada sencilla, pero cada segundo invertido para cambiar tu vida vale la pena. ¿Qué esperas? ¡Únete a nuestra familia de PROFIT 4 LIFE!</p>
                    <img class="footer-logo" src="{{URL::asset('/img/intro/intr.png')}}" alt="" /> </div>
                </div>
              </div>
              <!-- End About Block --> 
              
              <!-- Footer Links Block -->
              <div class="col-md-2">
                <div class="block block-links">
                  <h3 class="block-title"><span>Enlaces</span></h3>
                  <div class="block-content">
                    <ul>
                      <li><a href="{{route('home.forex')}}">Forex</a></li>
                      <li><a href="{{route('home.servicios')}}">Servicios</a></li>
                      <li><a href="{{route('home.presencial')}}">Presencial</a></li>
                      <li><a href="https://www.wsatraining.com/">English</a></li>
                      <li><a href="{{route('login')}}">Entrar</a></li>                      
                    </ul>
                  </div>
                </div>
              </div>
              <!-- End Footer Links Block --> 
              
              
              <!-- Footer Links Block -->
              <div class="col-md-4">
                <div class="block block-links">
                  <h3 class="block-title"><span>Servicios</span></h3>
                  <div class="block-content">
                    <ul>
                      <li><a href="{{route('home.producto', Crypt::encrypt($servicios[0]->id))}}">Profit 4 Life</a></li>
                      <li><a href="{{route('home.producto', Crypt::encrypt($servicios[1]->id))}}">Cripto 4 Life</a></li>
                      <li><a href="{{route('home.producto', Crypt::encrypt($servicios[2]->id))}}">Aplicación Movil</a></li>
                      <li><a href="{{route('home.producto', Crypt::encrypt($servicios[3]->id))}}">Libro Profit</a></li>
                      <li><a href="{{route('home.producto', Crypt::encrypt($servicios[4]->id))}}">Libro Cripto</a></li>
                    </ul>
                  </div>
                </div>
              </div>
              <!-- End Footer Links Block -->               
            </div>
          </div>
        </div>
      </div>
      <!-- End Footer Top --> 
      
      <!-- Footer Bottom -->
      <div class="footer-bottom">
        <div class="footer-bottom-wrapper">
          <div class="container">
            <div class="row">
              <div class="col-md-10 copyright">
                <p>Profit4Lifevzla &copy; Reserva Derechos de Autor.<span id="copyright">
            Desarrollado con ♥ por: <a href="http://atomovirtual.ddns.net/atomo/">Átomo Tecnologías Virtuales © 2019</a></p>
              </div>
              <div class="col-md-2 social-links">
                <ul class="right">
                  <li><a href="https://www.instagram.com/emiletrader/"><i class="fa fa-instagram"></i></a></li>
                  <li><a href="https://www.youtube.com/channel/UCpF5xy-jW3rhqN8uElpVYVw"><i class="fa fa-youtube-play"></i></a></li>
                  <li><a href="https://www.facebook.com/Profit4lifevzla/"><i class="fa fa-facebook"></i></a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- End Footer Bottom --> 
    </div>
  </footer>
  <!-- End Footer --> 
  
  <!-- GO TO TOP --> 
    <a href="#" class="cd-top"><i class="fa fa-angle-up"></i></a> 
  <!-- GO TO TOP End --> 
</div>
<!-- End Page Wrapper --> 


<!-- JavaScripts --> 
<script src="{{URL::asset('assets/js/vendors/jquery/jquery.min.js')}}"></script> 
<script src="{{URL::asset('assets/js/vendors/wow.min.js')}}"></script> 
<script src="{{URL::asset('assets/js/vendors/bootstrap.min.js')}}"></script> 
<script src="{{URL::asset('assets/js/vendors/own-menu.js')}}"></script> 
<script src="{{URL::asset('assets/js/vendors/flexslider/jquery.flexslider-min.js')}}"></script> 
<script src="{{URL::asset('assets/js/vendors/jquery.countTo.js')}}"></script> 
<script src="{{URL::asset('assets/js/vendors/jquery.isotope.min.js')}}"></script> 
<script src="{{URL::asset('assets/js/vendors/jquery.bxslider.min.js')}}"></script> 
<script src="{{URL::asset('assets/js/vendors/owl.carousel.min.js')}}"></script> 
<script src="{{URL::asset('assets/js/vendors/jquery.sticky.js')}}"></script> 
<script src="{{URL::asset('assets/js/vendors/color-switcher.js')}}"></script>

<!-- SLIDER REVOLUTION 4.x SCRIPTS  --> 
<script type="text/javascript" src="{{URL::asset('assets/rs-plugin/js/jquery.themepunch.tools.min.js')}}"></script> 
<script type="text/javascript" src="{{URL::asset('assets/rs-plugin/js/jquery.themepunch.revolution.min.js')}}"></script> 
<script src="{{URL::asset('assets/js/zap.js')}}"></script>
<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5ad2b179d7591465c709828e/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->