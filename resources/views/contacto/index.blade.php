@extends('adminlte::layouts.app')

@section('htmlheader_title')
	Correos
@endsection
@section('contentheader_title') {{-- TITULO DEL CONTENIDO DE LA VISTA --}}

@stop


@section('main-content')

<div class="container-fluid">
    <div class="row">
        <div class="col-md-3 col-md-offset-1">
            <div class="box box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Acciones</h3>
                </div>
                <div class="box-body">
 	                <center>					
					</center>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
	    	@if ($message = Session::get('success'))
				<div class="alert alert-success">
					<p>{{ $message }}</p>
				</div>
			@endif
          <div class="box">
			<div class="box-body table-responsive-dt">
	            <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    	<tr class="header">
							<th>Nombre</th>
							<th>Mail</th>
							<th>Asunto</th>
							<th>Mensaje</th>
							<th>Acción</th>
						</tr>
                    </thead>
                    <tbody>
						@foreach ($mails as $mail)
						<tr>
							<td>{{ $mail->name }}</td>
							<td>{{ $mail->email }}</td>
							<td>{{ $mail->subject }}</td>
							<td>{{ $mail->message }}</td>
							<td><center>
								{!! Form::open(['method' => 'DELETE','route' => ['mails.destroy', $mail->id],'style'=>'display:inline']) !!}
					            {!! Form::submit('Eliminar', ['class' => 'btn btn-danger']) !!}
					        	{!! Form::close() !!}
							</center></td>
						</tr>
						@endforeach 
                       
                    </tbody>
                </table>
            </div>
		  </div>
        </div>
    </div>
@endsection
