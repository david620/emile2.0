<!doctype html>
<html class="no-js" lang="es">
@include('layouts.layouts_user.head')
<body>

<div id="wrap"> 
  
@include('layouts.layouts_user.header')
  <section class="home-slider">
    <div class="tp-banner-container">
      <div class="tp-banner">
        <ul>
          <li data-transition="random" data-slotamount="7" data-masterspeed="300"  data-saveperformance="off" > 
            <img src="{{URL::asset('img/contador/fondo6.png')}}"  alt="slider"  data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat"> 
            <div class="sub-banner">
              <div class="position-center-center">
                <img src="{{URL::asset('img/logo/logo_ligth.png')}}" alt="" width="200px" height="200px" />
                <h2>CATEGORIAS</h2>
                
              </div>
            </div>
            
            <div class="tp-caption biglargetext sfr tp-resizeme" 
                data-x="center" data-hoffset="0" 
                data-y="center" data-voffset="-90" 
                data-speed="500" 
                data-start="1000" 
                data-easing="Power3.easeInOut" 
                data-splitin="chars" 
                data-splitout="none" 
                data-elementdelay="0.06" 
                data-endelementdelay="0.1" 
                data-endspeed="300"
                style="z-index: 5; max-width: auto; max-height: auto; white-space: nowrap;"> </div>
            
            <div class="tp-caption verybiglargetext sfr tp-resizeme" 
                data-x="center" data-hoffset="0" 
                data-y="center" data-voffset="40" 
                data-speed="500" 
                data-start="1500" 
                data-easing="Power3.easeInOut" 
                data-splitin="chars" 
                data-splitout="none" 
                data-elementdelay="0.07" 
                data-endelementdelay="0.1" 
                data-endspeed="300" 
                style="z-index: 6; max-width: auto; max-height: auto; white-space: nowrap;"> </div>
            
            <div class="tp-caption font-crimson font-italic sfb tp-resizeme" 
                data-x="center" data-hoffset="0" 
                data-y="center" data-voffset="140" 
                data-speed="500" 
                data-start="1900" 
                data-easing="Power3.easeInOut" 
                data-splitin="none" 
                data-splitout="none" 
                data-elementdelay="0.1" 
                data-endelementdelay="0.1" 
                data-endspeed="300" 
                style="z-index: 7; font-size:18px; color:#fff; max-width: auto; max-height: auto; white-space: nowrap;"> </div>
            
            <div class="tp-caption lfb tp-scrollbelowslider" 
                data-x="center" data-hoffset="0" 
                data-y="bottom" data-voffset="-50" 
                data-speed="500" 
                data-start="2400" 
                data-easing="Power3.easeInOut" 
                data-elementdelay="0.1" 
                data-endelementdelay="0.1" 
                data-endspeed="300" 
                data-scrolloffset="0"
                style="z-index: 8;"> </div>
          </li>
          
        </ul>
      </div>
    </div>
  </section>
  
  <div id="content"> 
    <div class="tradingview-widget-container">
  <div class="tradingview-widget-container__widget"></div>
  <script type="text/javascript" src="https://s3.tradingview.com/external-embedding/embed-widget-ticker-tape.js" async>
  {
  "symbols": [
    {
      "title": "S&P 500",
      "proName": "INDEX:SPX"
    },
    {
      "title": "Shangai Composite",
      "proName": "INDEX:XLY0"
    },
    {
      "title": "EUR/USD",
      "proName": "FX_IDC:EURUSD"
    },
    {
      "title": "BTC/USD",
      "proName": "BITFINEX:BTCUSD"
    },
    {
      "title": "ETH/USD",
      "proName": "BITFINEX:ETHUSD"
    }
  ],
  "theme": "light",
  "isTransparent": false,
  "displayMode": "adaptive",
  "locale": "es"
}
  </script>
</div>

    <section class="why-choose padding-top-20 ">
      <div class="container"> 
        <div class="heading-block text-center ">
          <h3>CATEGORIAS</h3>
          <div class="divider divider-short divider-center"><img class="i-div" src="{{URL::asset('img/logo/logo_dark.png')}}" alt="Divider Icon Image"/>
          <span>Familia Profit 4 Life</span></div>
        </div>
      </div>
    </section>

    <section class="blog split-post padding-top-20 padding-bottom-80"> 
      <div class="container">         
        @foreach($categorias as $categoria)
        <article class="blog-post">
          <div class="row"> 
            <div class="col-md-12"> <a href="{{route('video_categorias', $categoria->id)}}" class="tittle-post"> {{$categoria->nombre_categoria}} </a> 
            	<span class="post-bt">por <span class="text-color-primary">Emile Machado</span></span>
            </div>
          </div>
        </article>
        @endforeach
      </div>
    </section>
    <section class="why-choose">
      <div class="container"> 
        <div class="heading-block text-center ">
          <h3>CATEGORIAS</h3> 
          <div class="divider divider-short divider-center"><img class="i-div" src="{{URL::asset('img/logo/logo_dark.png')}}" alt="Divider Icon Image"/>
          <span>Familia Profit 4 Life</span> </div>
        </div>
      </div>
    </section>

   
    

  </div>

@include('layouts.layouts_user.footer')
</body>
</html>