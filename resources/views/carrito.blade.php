<!doctype html>
<html class="no-js" lang="es">
@include('layouts.head')
<body>

<!-- Page Wrapper -->
<div id="wrap"> 
  
  <!-- Header -->
@include('layouts.header')
  <!-- End Header --> 
  
  <!--======= HOME MAIN SLIDER =========-->
  <section class="home-slider">
    <div class="tp-banner-container">
      <div class="tp-banner">
        <ul>
          <!-- SLIDE  -->
          <li data-transition="random" data-slotamount="7" data-masterspeed="300"  data-saveperformance="off" > 
            <!-- MAIN IMAGE --> 
            <img src="{{URL::asset('img/contador/fondo4.png')}}"  alt="slider"  data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat"> 
            <!-- LAYERS --> 
            <div class="sub-banner">
              <div class="position-center-center">
                <img src="{{URL::asset('img/logo/logo_ligth.png')}}" alt="" width="200px" height="200px" />
                <h2>Carrito de Compras</h2>
                <!--======= Breadcrumb =========-->
                
              </div>
            </div>
            
            <!-- LAYER NR. 1 -->
            <div class="tp-caption biglargetext sfr tp-resizeme" 
                data-x="center" data-hoffset="0" 
                data-y="center" data-voffset="-90" 
                data-speed="500" 
                data-start="1000" 
                data-easing="Power3.easeInOut" 
                data-splitin="chars" 
                data-splitout="none" 
                data-elementdelay="0.06" 
                data-endelementdelay="0.1" 
                data-endspeed="300"
                style="z-index: 5; max-width: auto; max-height: auto; white-space: nowrap;"> </div>
            
            <!-- LAYER NR. 2 -->
            <div class="tp-caption verybiglargetext sfr tp-resizeme" 
                data-x="center" data-hoffset="0" 
                data-y="center" data-voffset="40" 
                data-speed="500" 
                data-start="1500" 
                data-easing="Power3.easeInOut" 
                data-splitin="chars" 
                data-splitout="none" 
                data-elementdelay="0.07" 
                data-endelementdelay="0.1" 
                data-endspeed="300" 
                style="z-index: 6; max-width: auto; max-height: auto; white-space: nowrap;"> </div>
            
            <!-- LAYER NR. 3 -->
            <div class="tp-caption font-crimson font-italic sfb tp-resizeme" 
                data-x="center" data-hoffset="0" 
                data-y="center" data-voffset="140" 
                data-speed="500" 
                data-start="1900" 
                data-easing="Power3.easeInOut" 
                data-splitin="none" 
                data-splitout="none" 
                data-elementdelay="0.1" 
                data-endelementdelay="0.1" 
                data-endspeed="300" 
                style="z-index: 7; font-size:18px; color:#fff; max-width: auto; max-height: auto; white-space: nowrap;"> </div>
            
            <!-- LAYER NR. 4 -->
            <div class="tp-caption lfb tp-scrollbelowslider" 
                data-x="center" data-hoffset="0" 
                data-y="bottom" data-voffset="-50" 
                data-speed="500" 
                data-start="2400" 
                data-easing="Power3.easeInOut" 
                data-elementdelay="0.1" 
                data-endelementdelay="0.1" 
                data-endspeed="300" 
                data-scrolloffset="0"
                style="z-index: 8;"> </div>
          </li>
          
        </ul>
      </div>
    </div>
  </section>
  
  <!-- Content -->
  <div id="content"> 

   


   <section class="padding-top-80 padding-bottom-80 pages-in chart-page">
      <div class="container"> 
        
        <!-- Payments Steps -->
        <div class="shopping-cart text-center">
          <div class="cart-head">
            <ul class="row">
              <!-- PRODUCTS -->
              <li class="col-sm-4">
                <h6>PRODUCTO</h6>
              </li>
              <!-- NAME -->
              <li class="col-sm-4">
                <h6>NOMBRE</h6>
              </li>
              <!-- QTY -->
             
              <!-- PRICE -->
              <li class="col-sm-3">
                <h6>PRECIO</h6>
              </li>
              <!-- TOTAL PRICE -->
              <li class="col-sm-1"> </li>
            </ul>
          </div>

          @foreach($servicios_filtrados as $servicio)          
              <ul class="row cart-details">
                <li class="col-sm-10">
                  <div class="media"> 
                    <!-- Media Image -->
                    <div class="media-left media-middle"> <a href="#." class="item-img"> <img class="media-object" src="{{URL::asset($servicio->imagen_servicio)}}" alt=""> </a> </div>
                    
                    <!-- Item Name -->
                    <div class="media-body">
                      <div class="position-center-center">
                        <h6>{{$servicio->nombre_servicio}}</h6>
                      </div>
                    </div>
                  </div>
                </li>
                
                <li class="col-sm-1">
                  <div class="position-center-center"> <span class="font-18px font-crimson font-italic">${{$servicio->precio_servicio}}</span> </div>
                </li>
                
                <!-- EDIT AND REMOVE -->
                <li class="col-sm-1">

                </li>
              </ul>
          @endforeach

          <div class="btn-sec"> 
                        
            <a href="{{route('home.remover_carrito')}}" class="btn gray-border"> VACIAR CARRITO </a> 
            
            <!-- CONTINUE SHOPPING --> 
            <a href="#" class="btn gray-border right-btn"> </a> </div>
          
          <!-- SHOPPING INFORMATION -->
          <div class="cart-ship-info">
            <div class="row"> 
              
              <!-- DISCOUNT CODE -->
              <div class="col-sm-6">
              </div>
              
              
              <!-- SUB TOTAL -->
              <div class="col-sm-6">
                <h6>CARRITO TOTAL</h6>
                @if(Session::has('cart'))
                    <div class="grand-total"> 
                      <h4>TOTAL: 
                        <span class="font-crimson font-italic font-normal"> 
                          ${{$suma}}
                        </span>
                      </h4>
                      <hr>
                      <a href="{{route('home.pasarela')}}" class="btn">COMPRAR</a> 
                    </div>
                @endif
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

<!-- Why Choose Us -->
    <section class="why-choose">
      <div class="container"> 

        <!-- Main Heading -->
        <div class="heading-block text-center ">
          <h3>CARRITO DE COMPRAS</h3>
          <div class="divider divider-short divider-center"><img class="i-div" src="{{URL::asset('img/logo/logo_dark.png')}}" alt="Divider Icon Image"/>
          <span>Familia Profit 4 Life</span></div>
        </div>
      </div>
    </section>

   
    

  </div>
  <!-- End Content --> 


  
@include('layouts.footer')
</body>
</html>
