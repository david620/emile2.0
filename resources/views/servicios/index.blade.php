@extends('adminlte::layouts.app')

@section('htmlheader_title')
	Servicios
@endsection
@section('contentheader_title') {{-- TITULO DEL CONTENIDO DE LA VISTA --}}

@stop


@section('main-content')

<div class="container-fluid">
    <div class="row">
        <div class="col-md-3 col-md-offset-1">
            <div class="box box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Acciones</h3>
                </div>
                <div class="box-body">
 	                <center>	
					</center>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
	    	@if ($message = Session::get('success'))
				<div class="alert alert-success">
					<p>{{ $message }}</p>
				</div>
			@endif
          <div class="box">
			<div class="box-body table-responsive-dt">
	            <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    	<tr class="header">
							<th>Nombre del Servicio</th>
							<th>Descripción del Servicio</th>
							<th>Precio del Servicio</th>
							<th>Acción</th>
						</tr>
                    </thead>
                    <tbody>
						@foreach ($servicios as $servicio)
						<tr>
							<td>{{ $servicio->nombre_servicio }}</td>
							<td>{{ $servicio->descripcion_servicio }}</td>
							<td>${{ $servicio->precio_servicio }}</td>
							<td><center>								
								<a class="btn btn-primary" href="{{ route('servicios.edit',$servicio->id) }}">Editar</a>
								{!! Form::open(['method' => 'DELETE','route' => ['servicios.destroy', $servicio->id],'style'=>'display:inline']) !!}
					            {!! Form::submit('Eliminar', ['class' => 'btn btn-danger']) !!}
					        	{!! Form::close() !!}
							</center></td>
						</tr>
						@endforeach 
                       
                    </tbody>
                </table>
            </div>
		  </div>
        </div>
    </div>
@endsection
