<!doctype html>
<html class="no-js" lang="es">
@include('layouts.head')
<body>

<!-- Page Wrapper -->
<div id="wrap"> 
  
  <!-- Header -->
@include('layouts.header')
  <!-- End Header --> 
  
  <!--======= HOME MAIN SLIDER =========-->
  <section class="home-slider">
    <div class="tp-banner-container">
      <div class="tp-banner">
        <ul>
          <!-- SLIDE  -->
          <li data-transition="random" data-slotamount="7" data-masterspeed="300"  data-saveperformance="off" > 
            <!-- MAIN IMAGE --> 
            <img src="{{URL::asset('img/contador/fond.png')}}"  alt="slider"  data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat"> 
            <!-- LAYERS --> 
            <div class="sub-banner">
              <div class="position-center-center">
                <img src="{{URL::asset('img/logo/logo_ligth.png')}}" alt="" width="200px" height="200px" />
                <h2>CONOCE SOBRE FOREX</h2>
                <!--======= Breadcrumb =========-->
                
              </div>
            </div>
            
            <!-- LAYER NR. 1 -->
            <div class="tp-caption biglargetext sfr tp-resizeme" 
                data-x="center" data-hoffset="0" 
                data-y="center" data-voffset="-90" 
                data-speed="500" 
                data-start="1000" 
                data-easing="Power3.easeInOut" 
                data-splitin="chars" 
                data-splitout="none" 
                data-elementdelay="0.06" 
                data-endelementdelay="0.1" 
                data-endspeed="300"
                style="z-index: 5; max-width: auto; max-height: auto; white-space: nowrap;"> </div>
            
            <!-- LAYER NR. 2 -->
            <div class="tp-caption verybiglargetext sfr tp-resizeme" 
                data-x="center" data-hoffset="0" 
                data-y="center" data-voffset="40" 
                data-speed="500" 
                data-start="1500" 
                data-easing="Power3.easeInOut" 
                data-splitin="chars" 
                data-splitout="none" 
                data-elementdelay="0.07" 
                data-endelementdelay="0.1" 
                data-endspeed="300" 
                style="z-index: 6; max-width: auto; max-height: auto; white-space: nowrap;"> </div>
            
            <!-- LAYER NR. 3 -->
            <div class="tp-caption font-crimson font-italic sfb tp-resizeme" 
                data-x="center" data-hoffset="0" 
                data-y="center" data-voffset="140" 
                data-speed="500" 
                data-start="1900" 
                data-easing="Power3.easeInOut" 
                data-splitin="none" 
                data-splitout="none" 
                data-elementdelay="0.1" 
                data-endelementdelay="0.1" 
                data-endspeed="300" 
                style="z-index: 7; font-size:18px; color:#fff; max-width: auto; max-height: auto; white-space: nowrap;"> </div>
            
            <!-- LAYER NR. 4 -->
            <div class="tp-caption lfb tp-scrollbelowslider" 
                data-x="center" data-hoffset="0" 
                data-y="bottom" data-voffset="-50" 
                data-speed="500" 
                data-start="2400" 
                data-easing="Power3.easeInOut" 
                data-elementdelay="0.1" 
                data-endelementdelay="0.1" 
                data-endspeed="300" 
                data-scrolloffset="0"
                style="z-index: 8;"> </div>
          </li>
          
        </ul>
      </div>
    </div>
  </section>
  
  <!-- Content -->
  <div id="content"> 
    <div class="tradingview-widget-container">
  <div class="tradingview-widget-container__widget"></div>
  <script type="text/javascript" src="https://s3.tradingview.com/external-embedding/embed-widget-ticker-tape.js" async>
  {
  "symbols": [
    {
      "title": "S&P 500",
      "proName": "INDEX:SPX"
    },
    {
      "title": "Shangai Composite",
      "proName": "INDEX:XLY0"
    },
    {
      "title": "EUR/USD",
      "proName": "FX_IDC:EURUSD"
    },
    {
      "title": "BTC/USD",
      "proName": "BITFINEX:BTCUSD"
    },
    {
      "title": "ETH/USD",
      "proName": "BITFINEX:ETHUSD"
    }
  ],
  "theme": "light",
  "isTransparent": false,
  "displayMode": "adaptive",
  "locale": "es"
}
  </script>
</div>

    <!-- Why Choose Us -->
    <section class="why-choose padding-top-20 ">
      <div class="container"> 
        <!-- Main Heading -->
        <div class="heading-block text-center ">
          <h3>FOREX</h3>
          <div class="divider divider-short divider-center"><img class="i-div" src="{{URL::asset('img/logo/logo_dark.png')}}" alt="Divider Icon Image"/>
          <span>Aprende de este Mercado</span></div>
        </div>
      </div>
    </section>
  <!-- Services Intro -->
    <section class=" padding-bottom-80">
      <div class="container"> 
        
        <div class="row">
          <div class="col-md-3">
          </div>  
          <div class="col-md-6 text-center img-responsive"> <iframe width="560" height="315" src="https://www.youtube.com/embed/0tlksunyN-8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
          <div class="col-md-3">
          </div>  
        </div>
        <br><br><br>
        <hr>



        <div class="row">
          <div class="col-md-6"> 
            <!-- Heading -->
            <div class="heading-block no-margin-bottom margin-top-30"> <span class="margin-bottom-15 margin-top-0"></span>
              <h2 class="margin-bottom-20 margin-top-1 letter-space-2 text-center">¿Qué es Forex?</h2>
            </div>
            <p align="justify">El mercado forex es el mercado internacional de divisas. Forex es acrónimo de Foreign Exchange market. Es el mercado financiero más grande del mundo, con un volumen de negocio superior a los 4 trillones de dólares diarios.</p>
          </div><br>
          <!-- Image -->
          <div class="col-md-6 text-center"> <img class="img-responsive" src="{{URL::asset('img/forex/1.png')}}" alt=""> </div>
        </div>
        <br><br><br>
        <hr>

        
        <!-- BEST PSD TEMPLATE -->
        <div class="row margin-top-50">
          <div class="col-md-5 text-center"> <img class="img-responsive" src="{{URL::asset('img/forex/2.png')}}" alt=""> </div>
          <div class="col-md-7"> 
            <!-- Heading -->
            <div class="heading-block no-margin-bottom margin-top-30"> <span class="margin-bottom-15 margin-top-0"></span>
              <h2 class="margin-bottom-20 margin-top-1 letter-space-2 text-center">¿A qué hora opera este mercado?</h2>
            </div>
            <p align="justify">Forex está abierto todo el día, y cierra los fines de semana. Es por ello que se puede operar en la madrugada, al medio día, por las tardes, en cualquier tiempo libre.</p>
             </div>
        </div>
         <br><br><br>
        <hr>
        <div class="row margin-top-50">
          <div class="col-md-6"> 
            <!-- Heading -->
            <div class="heading-block no-margin-bottom margin-top-30"> <span class="margin-bottom-15 margin-top-0"></span>
              <h2 class="margin-bottom-20 margin-top-1 letter-space-2 text-center">¿Dónde puedo hacer trading?</h2>
            </div>
            <p align="justify">Uno de los mayores avances del mercado de forex es la posibilidad de poder hacer trading desde cualquier lugar del mundo, con disponer de un ordenador, tablet o dispositivo móvil inteligente con conexion a internet “practico y simple”.</p>
          </div><br>
          <!-- Image -->
          <div class="col-md-6 text-center"> <img class="img-responsive" src="{{URL::asset('img/forex/3.png')}}" alt=""> </div>
        </div>
        <br><br><br>
        <hr>

        
        <!-- BEST PSD TEMPLATE -->
        <div class="row margin-top-50">
          <div class="col-md-5 text-center"> <img class="img-responsive" src="{{URL::asset('img/forex/4.png')}}" alt=""> </div>
          <div class="col-md-7"> 
            <!-- Heading -->
            <div class="heading-block no-margin-bottom margin-top-30"> <span class="margin-bottom-15 margin-top-0"></span>
              <h2 class="margin-bottom-20 margin-top-1 letter-space-2 text-center">¿Cómo gano dinero?</h2>
            </div>
            <p align="justify">Supongamos que el Euro el día de hoy cotiza con respecto al Dolar (USD) a un valor de $1.3207, y pienso que va a subir su paridad, por lo tanto compro euros. Efectivamente sube el Euro y cierra en $1.3287, donde los vendo y compro dólares. Por las variaciones tan pequeñas se compran mínimo 10,000 dolares.</p>
             </div>
        </div>
         <br><br><br>
        <hr>
        <div class="row margin-top-50">
          <div class="col-md-6"> 
            <!-- Heading -->
            <div class="heading-block no-margin-bottom margin-top-30"> <span class="margin-bottom-15 margin-top-0"></span>
              <h2 class="margin-bottom-20 margin-top-1 letter-space-2 text-center">¿10,000 mil dolares? No tengo tanto dinero ahorrado!!</h2>
            </div>
            <p align="justify">Aquí es donde entra lo que se llama apalancamiento. Hay brokers que permiten abrir cuentas con solo 100 dolares o menos, y le prestan a uno el resto. En este caso, si yo tengo 100 dolares y necesito comprar 10,000, me prestan el resto y entonces se dice que tengo un apalancamiento es de 1 a 100. Esta información es el gancho, y toda es verdad. Puedo operar en cualquier momento, con poca inversión, y tener buenas utilidades.</p>
          </div><br>
          <!-- Image -->
          <div class="col-md-6 text-center"> <img class="img-responsive" src="{{URL::asset('img/forex/5.png')}}" alt=""> </div>
        </div>

        
      </div>
    </section>
<!-- Why Choose Us -->
    <section class="why-choose padding-top-20 ">
      <div class="container"> 
        <!-- Main Heading -->
        <div class="heading-block text-center ">
          <h3>FOREX</h3>
          <div class="divider divider-short divider-center"><img class="i-div" src="{{URL::asset('img/logo/logo_dark.png')}}" alt="Divider Icon Image"/>
          <span>Aprende de este Mercado</span> </div>
        </div>
      </div>
    </section>

   
    

  </div>
  <!-- End Content --> 


  
@include('layouts.footer')
</body>
</html>