<!doctype html>
<html class="no-js" lang="es">
@include('layouts.head')
<body>

<!-- Page Wrapper -->
<div id="wrap"> 
  
  <!-- Header -->
@include('layouts.header')
  <!-- End Header --> 
  
  <!--======= HOME MAIN SLIDER =========-->
  <section class="home-slider">
    <div class="tp-banner-container">
      <div class="tp-banner">
        <ul>
          <!-- SLIDE  -->
          <li data-transition="random" data-slotamount="7" data-masterspeed="300"  data-saveperformance="off" > 
            <!-- MAIN IMAGE --> 
            <img src="{{URL::asset('img/contador/fondo4.png')}}"  alt="slider"  data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat"> 
            <!-- LAYERS --> 
            <div class="sub-banner">
              <div class="position-center-center">
                <img src="{{URL::asset('img/logo/logo_ligth.png')}}" alt="" width="200px" height="200px" />
                <h2>Términos y Condiciones</h2>
                <!--======= Breadcrumb =========-->
                
              </div>
            </div>
            
            <!-- LAYER NR. 1 -->
            <div class="tp-caption biglargetext sfr tp-resizeme" 
                data-x="center" data-hoffset="0" 
                data-y="center" data-voffset="-90" 
                data-speed="500" 
                data-start="1000" 
                data-easing="Power3.easeInOut" 
                data-splitin="chars" 
                data-splitout="none" 
                data-elementdelay="0.06" 
                data-endelementdelay="0.1" 
                data-endspeed="300"
                style="z-index: 5; max-width: auto; max-height: auto; white-space: nowrap;"> </div>
            
            <!-- LAYER NR. 2 -->
            <div class="tp-caption verybiglargetext sfr tp-resizeme" 
                data-x="center" data-hoffset="0" 
                data-y="center" data-voffset="40" 
                data-speed="500" 
                data-start="1500" 
                data-easing="Power3.easeInOut" 
                data-splitin="chars" 
                data-splitout="none" 
                data-elementdelay="0.07" 
                data-endelementdelay="0.1" 
                data-endspeed="300" 
                style="z-index: 6; max-width: auto; max-height: auto; white-space: nowrap;"> </div>
            
            <!-- LAYER NR. 3 -->
            <div class="tp-caption font-crimson font-italic sfb tp-resizeme" 
                data-x="center" data-hoffset="0" 
                data-y="center" data-voffset="140" 
                data-speed="500" 
                data-start="1900" 
                data-easing="Power3.easeInOut" 
                data-splitin="none" 
                data-splitout="none" 
                data-elementdelay="0.1" 
                data-endelementdelay="0.1" 
                data-endspeed="300" 
                style="z-index: 7; font-size:18px; color:#fff; max-width: auto; max-height: auto; white-space: nowrap;"> </div>
            
            <!-- LAYER NR. 4 -->
            <div class="tp-caption lfb tp-scrollbelowslider" 
                data-x="center" data-hoffset="0" 
                data-y="bottom" data-voffset="-50" 
                data-speed="500" 
                data-start="2400" 
                data-easing="Power3.easeInOut" 
                data-elementdelay="0.1" 
                data-endelementdelay="0.1" 
                data-endspeed="300" 
                data-scrolloffset="0"
                style="z-index: 8;"> </div>
          </li>
          
        </ul>
      </div>
    </div>
  </section>
  
  <!-- Content -->
  <div id="content"> 

   

  <section class="padding-top-80 padding-bottom-80">
      <div class="container"> 
        <!-- Content -->
        <div class="relative">
          <div class="heading-block no-margin-bottom"> <span  class="margin-bottom-10">Bienvenidos a Profit 4 Life</span>
            <h2 class="margin-bottom-20 margin-top-0 letter-space-1">Términos y Condiciones</h2>
          </div>
          	<p align="justify">Debido a que nuestros servicios y educación son un servicio digital y en vivo que se puede copiar y guardar fácilmente, no permitimos ni proporcionamos ningún reembolso por ningún motivo. Usted acepta esta política y acepta que no realizaremos ningún reembolso, sin importar cuál sea la condición.
			<br><br>
			Cuando compra nuestro servicio educativo, acepta esta política y respetará la perspectiva de www.profit4lifevzla.com (Profit4lifevzla INC) sobre estos temas para obtener reembolsos. No garantizamos la exactitud, integridad o puntualidad de ninguna discusión o educación intercambiada a través de nuestro sitio o servicios de chat, y la información debe ser confirmada por otras fuentes. Todas las proyecciones son solo estimaciones. El material en el sitio o sala de chat no incluye: Una oferta o invitación de www.profit4lifevzla.com ; o Una recomendación de valores de Profit4lifevzla INC o cualquier empleado, propietario o contratista independiente de Profit4lifevzla INC para comprar o vender valores o cualquier otro producto financiero, moneda o producto básico.
			<br><br>
			Nada en este sitio o salas de chat constituye o sugiere asesoramiento financiero personal. El material, la discusión y la educación en este sitio o sala de chat no tienen en cuenta las necesidades personales de ninguna persona. Antes de tomar decisiones de inversión, debe contactar a un asesor financiero con licencia profesional. A excepción de la responsabilidad legal que no puede excluirse, Profit4lifevzla INC y cualquier empleado, propietario o contratista independiente excluye toda responsabilidad (incluida la responsabilidad por negligencia) que surja del uso de cualquier servicio en este sitio o salas de chat. La responsabilidad que legalmente no puede excluirse se limita al máximo posible.
			<br><br>
			Este sitio no debe usarse como un sustituto del asesoramiento profesional bajo ninguna circunstancia. A excepción de la responsabilidad que legalmente no puede excluirse, Profit4lifevzla INC y cualquier empleado, propietario o contratista independiente de Profit4lifevzla INC, excluye toda responsabilidad (incluida la responsabilidad por negligencia) que surja del uso de datos, información, discusión, contribución, u otro, proporcionado por proveedores externos al sitio o sala de chat. La responsabilidad que legalmente no puede excluirse se limita al máximo posible. La educación con consejos generales o cualquier consejo o información en este sitio web es solo un consejo general, no considera sus circunstancias personales. Cualquier consejo general dado no debe tomarse como consejo profesional bajo ninguna circunstancia. Cualquier consejo general no debe tomarse como asesoramiento financiero específico bajo ninguna circunstancia. Cualquier consejo general no representa el deseo o la intención de Profit4lifevzla INC de proporcionarle asesoría u orientación financiera.
			<br><br>
			No intercambie ni invierta solo en base a la información provista dentro del sitio www.profit4lifevzla.com, o los chats asociados de Profit4lifevzla INC. Al ver la información dentro del sitio web y las salas de chat en www.profit4lifevzla.com y las salas de chat asociadas, usted acepta que se trata de educación general y discusión general, y no hará responsable a Profit4lifevzla INC ni a ninguna persona afiliada a Profit4lifeVzla INC de CUALQUIER pérdida o daño resultante de la información o el asesoramiento general proporcionado aquí por Profit4lifevzla INC o sus empleados, propietarios o contratistas independientes. Las operaciones de cambio tienen grandes recompensas potenciales, pero también tienen factores de riesgo grandes y extremos. Debe ser consciente de los riesgos y estar dispuesto a aceptarlos para invertir en el mercado de divisas. Al firmar este descargo de responsabilidad, usted acepta tener una comprensión firme de los riesgos asociados con el mercado de divisas.
			<br><br>
			No negocies con dinero que no puedas permitirte perder. Este sitio web y las salas de chat asociadas no son una solicitud ni una oferta para comprar / vender divisas spot o cualquier otro producto financiero, moneda o producto básico. No se está haciendo ninguna representación de que cualquier tipo de cuenta, ya sea una cuenta real o una cuenta demo, logre ganancias o pérdidas como las discutidas en cualquier material en el sitio web, discusión de la sala de chat pasada, presente o futura o en cualquier fuente asociada de redes sociales. Los resultados anteriores y el rendimiento de cualquier sistema de negociación, comentario de mercado, discusión de chat, interacción con redes sociales u otra fuente, NO son garantía de resultados futuros y deben ser utilizados bajo su propio riesgo. Advertencia de alto riesgo: el alto grado de apalancamiento en las operaciones de cambio implica un riesgo extremo de pérdida y no es adecuado para todos los inversores. Al firmar este descargo de responsabilidad, usted comprende que su apalancamiento utilizado en los mercados de divisas se correlaciona directamente con su riesgo, y comprende y acepta que las cuentas de alto apalancamiento arrojan un mayor riesgo. Al firmar este descargo de responsabilidad, reconoce que Profit4lifevzla INC no es responsable ni tiene influencia en ninguna decisión tomada con respecto al apalancamiento O cualquier otra decisión relacionada con la moneda.
			<br><br>
			Recuerde que los resultados anteriores de cualquier sistema de comercio o información no están garantizados de los resultados futuros de cualquier cuenta en vivo o demo. Al comprar nuestros servicios, usted acepta indemnizar y mantener a las subsidiarias y afiliadas de Profit4lifevzla INC, y a cada uno de sus directores, funcionarios, agentes, contratistas, socios y empleados, u otros inofensivos y no culpables en el tribunal de justicia de y contra cualquier pérdida, responsabilidad, reclamo, demanda, daños, costos y gastos, incluidos los honorarios de abogados. Al firmar este descargo de responsabilidad, usted comprende y acepta que Profit4lifevzla INC NO es un grupo "proveedor de señales", y NO ofrece NINGUNA asesoría u orientación financiera. Al firmar esta cláusula de exención de responsabilidad, usted comprende y reconoce su comprensión de que DEBE consultar a un asesor de asesoría financiera con licencia antes de tomar decisiones financieras o de inversión educadas.
			<br><br>
			Ganancias y resultados. El descargo de responsabilidad en cuanto a los resultados comerciales pasados o las ganancias pasadas en una cuenta real o cuenta demo no deben tomarse como una garantía, promesa o sugerencia de que puede lograr los mismos resultados en el futuro. La evidencia escrita o audiovisual de ganancias anteriores de una cuenta real o cuenta demo no es una garantía o promesa de que una persona o entidad alcanzará los mismos resultados o el mismo rendimiento cuando coloque operaciones en una cuenta real o en una cuenta demo. Al firmar esta cláusula de exención de responsabilidad, también reconoce que Profit4lifevzla INC no pretende representar una cuenta real o una cuenta demo en ninguna circunstancia específica.
			<br><br>
			Las estrategias de negociación e información discutidas en el sitio web, salas de chat u otras fuentes, se han utilizado para operar en condiciones de mercado en vivo y demostración, pero esto no garantiza ni promete ganancias o pérdidas futuras. Si utiliza las estrategias / estrategias discutidas en cualquier plataforma de Profit4lifevzla INC para realizar operaciones o decisiones de inversión, lo hace bajo su propio riesgo. Profit4lifevzla INC y sus empleados, propietarios, contratistas independientes y otros, han utilizado en el pasado las estrategias de negociación discutidas en el sitio web, la sala de chat u otras fuentes para lograr tanto operaciones ganadoras como perdedoras.
			<br><br>
			Al utilizar www.profit4lifevzla.com, sus salas de chat o cualquier otra fuente, y las estrategias que se enseñan, muestran o analizan en su interior, se basan en su propia decisión y son su responsabilidad por completo. Profit4lifevzla INC no es responsable en ningún caso de las pérdidas o daños causados por sus decisiones personales, acciones o intercambios, debe ponerse en contacto con un asesor profesional de inversiones antes de invertir en el mercado forex o en cualquier otro mercado financiero. En ningún momento se ha garantizado o prometido que las ganancias o los ingresos se obtendrán siguiendo / aplicando la educación, la discusión o la exhibición de divisas que tenga lugar en www.profit4lifevzla.com o en cualquier sala de chat asociada u otra fuente. No se garantiza que se produzcan resultados similares en una cuenta real con dinero real o cuenta demo con dinero falso.
			<br><br>
			Cualquier Divulgación de la información y material Perteneciente a Profit4lifevzla INC, Será visto Como robo Intelectual Y se Procederá a Una demanda Legal, Donde el usuario no solo será Expulsado de la academia si no que también asume que tendrá que Pagar a Profit4lifevzla INC una cantidad Inicial de diez mil Dolares Americanos la cual Puede Aumentar.
			<br><br>
			La interpretación juega un papel en los métodos / estrategias comerciales, entonces lo que usted cree que es una oportunidad comercial puede ser diferente a la visión de otra persona de una oportunidad comercial, tenga en cuenta que una estrategia comercial sin reglas mecánicas es interpretable, y Profit4lifevzla.</p>
          <hr>
          <div class="row">
            <div class="col-md-12">
              <p class="font-crimson font-16px font-italic">“La vida como emprendedor en los mercados de criptomonedas no es nada sencilla, pero cada segundo invertido para cambiar tu vida vale la pena.”</p>
              <h6>EMILE MACHADO - <span class="font-crimson font-italic">CEO/FUNDADOR PROFIT 4 LIFE</span></h6>
            </div>
          </div>
        </div>
      </div>
    </section>
<!-- Why Choose Us -->
    <section class="why-choose">
      <div class="container"> 

        <!-- Main Heading -->
        <div class="heading-block text-center ">
          <h3>TÉRMINOS Y CONDICIONES</h3>
          <div class="divider divider-short divider-center"><img class="i-div" src="{{URL::asset('img/logo/logo_dark.png')}}" alt="Divider Icon Image"/>
          <span>Familia Profit 4 Life</span></div>
        </div>
      </div>
    </section>

   
    

  </div>
  <!-- End Content --> 


  
@include('layouts.footer')
@section('js')
<script>
    $(document).ready(function() {
        // page is now ready, initialize the calendar...
        $('#calendar').fullCalendar({
            // put your options and callbacks here
            events : [
                {
                    title : 'fecha1',
                    start : '2019-11-05',
                    end : '2019-11-09'
                },
            ]
        })
    });
</script>
@stop


</body>
</html>
