@extends('adminlte::layouts.app')

@section('htmlheader_title')
	Categorias
@endsection
@section('contentheader_title') {{-- TITULO DEL CONTENIDO DE LA VISTA --}}

@stop


@section('main-content')

<div class="container-fluid">
    <div class="row">
        <div class="col-md-3 col-md-offset-1">
            <div class="box box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Acciones</h3>
                </div>
                <div class="box-body">
 	                <center>
					<a class="btn btn-primary btn-block " href="{{ route('categorias.create')}}">Crear Categoría</a>		
					</center>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
	    	@if ($message = Session::get('success'))
				<div class="alert alert-success">
					<p>{{ $message }}</p>
				</div>
			@endif
          <div class="box">
			<div class="box-body table-responsive-dt">
	            <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    	<tr class="header">
							<th>Nombre Categoría</th>
							<th>Plataforma Asociada</th>
							<th>Acción</th>
						</tr>
                    </thead>
                    <tbody>
						@foreach ($categorias as $categoria)
						<tr>
							<td>{{ $categoria->nombre_categoria }}</td>
							<td>
								@if($categoria->plataforma_categoría == 1)
									Profit 4 Life
								@endif
								@if($categoria->plataforma_categoría == 2)
									Cripto 4 Life
								@endif
							</td>
							<td><center>
								{!! Form::open(['method' => 'DELETE','route' => ['categorias.destroy', $categoria->id],'style'=>'display:inline']) !!}
					            {!! Form::submit('Eliminar', ['class' => 'btn btn-danger']) !!}
					        	{!! Form::close() !!}
							</center></td>
						</tr>
						@endforeach 
                       
                    </tbody>
                </table>
            </div>
		  </div>
        </div>
    </div>
@endsection
