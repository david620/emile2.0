@extends('adminlte::layouts.app')

@section('htmlheader_title')
	Videos
@endsection
@section('contentheader_title') {{-- TITULO DEL CONTENIDO DE LA VISTA --}}

@stop


@section('main-content')

<div class="container-fluid">
    <div class="row">
        <div class="col-md-3 col-md-offset-1">
            <div class="box box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Acciones</h3>
                </div>
                <div class="box-body">
 	                <center>
					<a class="btn btn-primary btn-block " href="{{route('videos.create')}}">Crear Video</a>		
					</center>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
	    	@if ($message = Session::get('success'))
				<div class="alert alert-success">
					<p>{{ $message }}</p>
				</div>
			@endif
          <div class="box">
			<div class="box-body table-responsive-dt">
	            <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    	<tr class="header">
							<th>Nombre del Video</th>
							<th>Descripción del Video</th>
							<th>Iframe del Video</th>
							<th>Categoría del Video</th>
							<th>Servicio del Video</th>
							<th>Acción</th>
						</tr>
                    </thead>
                    <tbody>
						@foreach ($videos as $video)
						<tr>
							<td>{{ $video->nombre_video }}</td>
							<td>{{ $video->descripcion_video }}</td>
							<td>{{ $video->iframe_video }}</td>
							<td> 
								{{$video->nombre_categoria}}
							</td>
							<td> 
								{{$video->nombre_servicio}}
							</td>
							<td><center>
								<a class="btn btn-primary" href="{{ route('videos.edit',$video->id) }}">Editar</a>
								{!! Form::open(['method' => 'DELETE','route' => ['videos.destroy', $video->id],'style'=>'display:inline']) !!}
					            {!! Form::submit('Eliminar', ['class' => 'btn btn-danger']) !!}
					        	{!! Form::close() !!}
							</center></td>
						</tr>
						@endforeach 
                       
                    </tbody>
                </table>
            </div>
		  </div>
        </div>
    </div>
@endsection
