@extends('adminlte::layouts.app')

@section('htmlheader_title')
	Editar Video
@endsection
@section('contentheader_title') {{-- TITULO DEL CONTENIDO DE LA VISTA --}}

@stop

@section('main-content')

	@if (count($errors) > 0)
		<div class="alert alert-danger">
			<h4><i class="icon fa fa-ban"></i> ¡Uy!, ha ocurrido un problema</h4>
			<ul>
				@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
	@endif

          <div class="row">
              <div class="col-md-3 col-md-offset-1">
                  <div class="box box-solid">
                      <div class="box-header with-border">
                          <h3 class="box-title">Acciones</h3>
                      </div>
                      <div class="box-body">
                          <center>
                            <a class="btn btn-primary btn-block " href="{{ route('videos.index')}}">Regresar</a>   
                          </center>
                      </div>
                  </div>
              </div>
          </div>


  {!! Form::model($video, ['method' => 'PATCH', 'enctype' => 'multipart/form-data', 'route' => ['videos.update', $video->id]]) !!}
    {{ csrf_field() }}
          <div class="row">
        <div class="col-md-9 col-md-offset-1">
         <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Editar Video</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form">
              <div class="box-body">
                <div class="form-group">
                  <label for="nombre_video">Nombre del Video:</label>
		            {!! Form::text('nombre_video', $video->nombre_video, array('placeholder' => 'Nombre del Video: ','class' => 'form-control', 'required' => 'required')) !!}
                </div>
                <div class="form-group">
                  <label for="descripcion_video">Descripción del video:</label>
                  {!! Form::textarea('descripcion_video', $video->descripcion_video, array('placeholder' => 'Descripción del Video: ','class' => 'form-control', 'required' => 'required')) !!}
                </div>
                <div class="form-group">
                  <label for="iframe_video">Iframe del video (VIMEO):</label>
                  {!! Form::textarea('iframe_video', $video->iframe_video, array('placeholder' => 'Iframe del Video: ','class' => 'form-control', 'required' => 'required')) !!}
                </div>
                <div class="form-group">
                  <label for="categoria_id">Categoría del video:</label>
                  {!! Form::select('categoria_id',$categorias, $categoria->id, array('class' => 'form-control')) !!}
                </div>
                <div class="form-group">
                  <label for="posicion">Posición del video:</label>
                  <select class="form-control" name="posicion">
                    <option value="" selected="selected">Seleccione una Posición</option>
                    @for($i=1; $i <= $posiciones; $i++)
                        @if($i == $video->posicion_video)
                          <option value="{{$i}}" selected="selected">Posición: {{$i}}</option>
                        @else
                          <option value="{{$i}}">Posición: {{$i}}</option>
                        @endif
                    @endfor
                  </select>
                </div>

              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Editar</button>
              </div>
            </form>
          </div>
        </div>
        </div>
	{!! Form::close() !!}
@endsection
