@extends('adminlte::layouts.app')

@section('htmlheader_title')
	Crear Video
@endsection
@section('contentheader_title') {{-- TITULO DEL CONTENIDO DE LA VISTA --}}

@stop

@section('main-content')

	@if (count($errors) > 0)
		<div class="alert alert-danger">
			<h4><i class="icon fa fa-ban"></i> ¡Uy!, ha ocurrido un problema</h4>
			<ul>
				@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
	@endif

          <div class="row">
              <div class="col-md-3 col-md-offset-1">
                  <div class="box box-solid">
                      <div class="box-header with-border">
                          <h3 class="box-title">Acciones</h3>
                      </div>
                      <div class="box-body">
                          <center>
                            <a class="btn btn-primary btn-block " href="{{ route('videos.index')}}">Regresar</a>   
                          </center>
                      </div>
                  </div>
              </div>
          </div>


	{!! Form::open(array('route' => 'videos.store','method'=>'POST')) !!}
    {{ csrf_field() }}
          <div class="row">
        <div class="col-md-9 col-md-offset-1">
         <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Crear Video</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form">
              <div class="box-body">
                <div class="form-group">
                  <label for="nombre_video">Nombre del Video:</label>
		            {!! Form::text('nombre_video', null, array('placeholder' => 'Nombre del Video: ','class' => 'form-control', 'required' => 'required')) !!}
                </div>
                <div class="form-group">
                  <label for="descripcion_video">Descripción del video:</label>
                  {!! Form::textarea('descripcion_video', null, array('placeholder' => 'Descripción del Video: ','class' => 'form-control', 'required' => 'required')) !!}
                </div>
                <div class="form-group">
                  <label for="iframe_video">Iframe del video (VIMEO):</label>
                  {!! Form::textarea('iframe_video', null, array('placeholder' => 'Iframe del Video: ','class' => 'form-control', 'required' => 'required')) !!}
                </div>
                <div class="form-group">
                  <label for="categoria_id">Categoría del video:</label>
                  {!! Form::select('categoria_id',$categorias, '', array('class' => 'form-control')) !!}
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Crear</button>
              </div>
            </form>
          </div>
        </div>
        </div>
	{!! Form::close() !!}
@endsection
