<!doctype html>
<html class="no-js" lang="es">
@include('layouts.head')
<body>

<!-- Page Wrapper -->
<div id="wrap"> 
  
  <!-- Header -->
@include('layouts.header')
  <!-- End Header --> 
  
  <!--======= HOME MAIN SLIDER =========-->
  <section class="home-slider">
    <div class="tp-banner-container">
      <div class="tp-banner">
        <ul>
          <!-- SLIDE  -->
          <li data-transition="random" data-slotamount="7" data-masterspeed="300"  data-saveperformance="off" > 
            <!-- MAIN IMAGE --> 
            <img src="{{URL::asset('img/contador/fondo5.png')}}"  alt="slider"  data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat"> 
            <!-- LAYERS --> 
            <div class="sub-banner">
              <div class="position-center-center">
                <img src="{{URL::asset('img/logo/logo_ligth.png')}}" alt="" width="200px" height="200px" />
                <h2>{{$servicio->nombre_servicio}}</h2>
                <!--======= Breadcrumb =========-->
                
              </div>
            </div>
            
            <!-- LAYER NR. 1 -->
            <div class="tp-caption biglargetext sfr tp-resizeme" 
                data-x="center" data-hoffset="0" 
                data-y="center" data-voffset="-90" 
                data-speed="500" 
                data-start="1000" 
                data-easing="Power3.easeInOut" 
                data-splitin="chars" 
                data-splitout="none" 
                data-elementdelay="0.06" 
                data-endelementdelay="0.1" 
                data-endspeed="300"
                style="z-index: 5; max-width: auto; max-height: auto; white-space: nowrap;"> </div>
            
            <!-- LAYER NR. 2 -->
            <div class="tp-caption verybiglargetext sfr tp-resizeme" 
                data-x="center" data-hoffset="0" 
                data-y="center" data-voffset="40" 
                data-speed="500" 
                data-start="1500" 
                data-easing="Power3.easeInOut" 
                data-splitin="chars" 
                data-splitout="none" 
                data-elementdelay="0.07" 
                data-endelementdelay="0.1" 
                data-endspeed="300" 
                style="z-index: 6; max-width: auto; max-height: auto; white-space: nowrap;"> </div>
            
            <!-- LAYER NR. 3 -->
            <div class="tp-caption font-crimson font-italic sfb tp-resizeme" 
                data-x="center" data-hoffset="0" 
                data-y="center" data-voffset="140" 
                data-speed="500" 
                data-start="1900" 
                data-easing="Power3.easeInOut" 
                data-splitin="none" 
                data-splitout="none" 
                data-elementdelay="0.1" 
                data-endelementdelay="0.1" 
                data-endspeed="300" 
                style="z-index: 7; font-size:18px; color:#fff; max-width: auto; max-height: auto; white-space: nowrap;"> </div>
            
            <!-- LAYER NR. 4 -->
            <div class="tp-caption lfb tp-scrollbelowslider" 
                data-x="center" data-hoffset="0" 
                data-y="bottom" data-voffset="-50" 
                data-speed="500" 
                data-start="2400" 
                data-easing="Power3.easeInOut" 
                data-elementdelay="0.1" 
                data-endelementdelay="0.1" 
                data-endspeed="300" 
                data-scrolloffset="0"
                style="z-index: 8;"> </div>
          </li>
          
        </ul>
      </div>
    </div>
  </section>
  
  <!-- Content -->
  <div id="content"> 

   


   <section class="item-detail-page padding-top-80 padding-bottom-80"> 
      
      <!--======= PAGES INNER =========-->
      <section class="section-p-30px pages-in">
        <div class="container">
          <div class="row"> 
            
            <!--======= IMAGES SLIDER =========-->
            <div class="col-sm-6 large-detail">
              <div class="images-slider">
                <ul class="slides">
                  <li data-thumb="images/shop/item-detail-img-1.jpg"> <img class="img-responsive" src="{{URL::asset($servicio->imagen_servicio)}}"  alt=""> </li>
                </ul>
              </div>
            </div>
            <div class="col-sm-6 large-detail">
              <h2 class="margin-top-0 margin-bottom-20">{{$servicio->nombre_servicio}}</h2>
              <p align="justify">{{$servicio->descripcion_servicio}}</p>
              <hr>
              <div class="price"> <span class="text-strike font-crimson">${{$servicio->precio_servicio + 400}}</span> <span class="font-crimson">${{$servicio->precio_servicio}}</span> </div>
              
              @if($servicio->id == 3)             
                <a href="{{route('home.app')}}" class="btn btn-med btn-color">COMPRAR</a>
              @else
                <a href="{{route('home.agregar_carrito', Crypt::encrypt($servicio->id))}}" class="btn btn-med btn-color">AÑADIR AL CARRITO</a>
              @endif
              <div class="clearfix"></div>
              
              <ul class="social-icons">
                <li><a href="https://www.instagram.com/emiletrader/"><i class="fa fa-instagram"></i></a></li>
                <li><a href="https://www.facebook.com/Profit4lifevzla/"><i class="fa fa-facebook"></i></a></li>
                <li><a href="https://www.youtube.com/channel/UCpF5xy-jW3rhqN8uElpVYVw"><i class="fa fa-youtube"></i></a></li>
                
              </ul>
            </div>
          </div>
          

          <br>
          <hr>
          <!--======= Products =========-->
          <div class="popurlar_product margin-bottom-80">
            <ul class="row">
              
              @foreach($servs as $servi)
              <li class="col-sm-3">
                <div class="items-in"> 
                  <!-- Image --> 
                  <img height="250px" width="400px" src="{{URL::asset($servi->imagen_servicio)}}" alt=""> 
                  <!-- Hover Details -->
                  @if($servi->id == 3)
                    <div class="over-item"> <a href="{{route('home.app')}}" class="btn">COMPRAR</a> </div>
                  @else
                    <div class="over-item"> <a href="{{route('home.agregar_carrito', Crypt::encrypt($servi->id))}}" class="btn">AÑADIR AL CARRITO</a> </div>
                  @endif
                </div>

                <div class="details-sec"> <a href="#.">{{$servi->nombre_servicio}}</a> <span class="font-crimson font-italic font-18px">${{$servi->precio_servicio}} <span class="text-strike font-crimson font-italic font-16px margin-left-20">${{$servi->precio_servicio + 400}}</span> </span> </div>
              </li>
              @endforeach
            </ul>
          </div>
        </div>
      </section>
    </section>
<!-- Why Choose Us -->
    <section class="why-choose">
      <div class="container"> 

        <!-- Main Heading -->
        <div class="heading-block text-center ">
          <h3>{{$servicio->nombre_servicio}}</h3>
          <div class="divider divider-short divider-center"><img class="i-div" src="{{URL::asset('img/logo/logo_dark.png')}}" alt="Divider Icon Image"/>
          <span>Familia Profit 4 Life</span></div>
        </div>
      </div>
    </section>

   
    

  </div>
  <!-- End Content --> 


  
@include('layouts.footer')
</body>
</html>
